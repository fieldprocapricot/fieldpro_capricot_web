<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function web_inputNode($array)
{
	$q=array();
	for($i=0; $i < count($array); $i++): $q[] = '?';  endfor; 
	return $inputResult = implode(',',$q);
}

function adminEmail()
{
	$data = 'hwsupport@capricot.com';
	return $data;
}
function adminPhone()
{
	$data = '1800-270-3142';
	return $data;
}
function adminFromName()
{
	$data = 'Capricot';
	return $data;
}

function web_DateTime($datetime)
{
	$time = strtotime($datetime);
	$taskDateTime = date('d-m-Y h:i:s A', $time);	
	return $taskDateTime;
}
function web_Date($datetime)
{
	$time = strtotime($datetime);
	$taskDateTime = date('d-m-Y', $time);	//don't change the format bcz mobile app used this format
	return $taskDateTime;
}
function web_dbDateTime($datetime)
{
	$time = strtotime($datetime);
	$taskDateTime = date('Y-m-d h:i:s', $time);	//don't change the format bcz mobile app used this format
	return $taskDateTime;
}
function web_dbDate($date)
{
	$date = strtotime($date);
	$date = date('Y-m-d', $date);	//don't change the format bcz mobile app used this format
	return $date;
}
function web_time($time)
{
	$time = strtotime($time);
	$array = date('h.i a', $time);
	return $array;
}
function price_format($price)
{
	return number_format(doubleval($price),2);
}



?>