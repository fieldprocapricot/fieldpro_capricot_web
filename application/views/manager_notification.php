<!DOCTYPE html>
<html lang="en">

<head>
          <?php include "assets/lib/cssscript.php"?>
		 </head>
         <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
 <!-- BEGIN CONTAINER -->
 <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/manager_header.php"?>
               <!-- END HEADER -->
              <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
               <div class="page-content-wrapper">
                  <div class="page-content">


                  
<!--dhanu-->
<div class="row"> 
                      <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Notifications</span>
                                                    </div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#tab_1_1" data-toggle="tab"> Tickets </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_2" data-toggle="tab"> Users </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_3" data-toggle="tab"> Spares </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_4" data-toggle="tab"> Training </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_5" data-toggle="tab"> SLA Mapping </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1_1">
                                        <div class="scroller" style="height: 320px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                                                <ul class="feeds">
                                                                <li>
                                                                <?php foreach ($new_tickets as $row){?>  
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-sm label-success">
                                                                                        <i class="fa fa-bell-o"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                                    <div class="desc"> A Ticket with Ticket ID - <strong><?php echo $row['ticket_id'];?></strong> has been raised.
                                                                                    <span class="label label-sm label-info"> New Ticket
                                                                                    
                                                                                        </span>
                                                                                      
                                                                                      
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date">
                                                                            <?php  
$datetime1 = new DateTime(date("Y-m-d H:i:s"));
$datetime2 = new DateTime($row['raised_time']);
$interval = $datetime1->diff($datetime2);
$hour= $interval->format('%H');
$day= $interval->format('%d');
$month= $interval->format('%m');
$min= $interval->format('%i');
$sec= $interval->format('%s');
$year= $interval->format('%Y');
$number=0;
                                                                                     
       if($year == 0 && $month != 0){
           
          $number=$interval->format('%m');
           $var='Months';
        }
        else if($year == 0 && $month == 0 && $day != 0){
           $number=$interval->format('%d');
           $var='Days';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour != 0){
            $number=$interval->format('%H');
            $var='Hours';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min != 0){
            $number=$interval->format('%i');
            $var='Minutes';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min == 0 && $sec != 0){
           $number=$interval->format('%i');
           $var='Seconds';
        }
        else{
           $number=$interval->format('%Y');
           $var='Years';
           
        }
        switch($number){
            case 1:
            $new=rtrim($var,'s');
            echo  $number.'&nbsp'.$new.'&nbsp'.'ago';
            break;

            default:
            echo  $number.'&nbsp'.$var.'&nbsp'.'ago';
            break;
        }

       
        
        ?>



                                                                                     
                                                                                   
                                                                             </div>
                                                                        </div>
                                                                <?php } ?>
                                                                       
                                                                    </li>
                                                            
                                                                    <li>
                                                                <?php foreach ($escalated_tickets as $row){?>  
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-sm label-success">
                                                                                        <i class="fa fa-bell-o"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                                    <div class="desc"> A Ticket with Ticket ID - <strong><?php echo $row['ticket_id'];?></strong> has been escalated.
                                                                                    <span class="label label-sm label-warning"> Escalated Ticket
                                                                                    
                                                                                        </span>
                                                                                      
                                                                                      
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date">
                                                                            <?php  
$datetime1 = new DateTime(date("Y-m-d H:i:s"));
$datetime2 = new DateTime($row['last_update']);
$interval = $datetime1->diff($datetime2);
$hour= $interval->format('%H');
$day= $interval->format('%d');
$month= $interval->format('%m');
$min= $interval->format('%i');
$sec= $interval->format('%s');
$year= $interval->format('%Y');
$number=0;
                                                                                     
//echo $interval->format('%Y years %m months %d days %H hours %i minutes %s seconds');
       if($year == 0 && $month != 0){
           
          $number=$interval->format('%m');
           $var='Months';
        }
        else if($year == 0 && $month == 0 && $day != 0){
           $number=$interval->format('%d');
           $var='Days';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour != 0){
            $number=$interval->format('%H');
            $var='Hours';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min != 0){
            $number=$interval->format('%i');
            $var='Minutes';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min == 0 && $sec != 0){
           $number=$interval->format('%i');
           $var='Seconds';
        }
        else{
           $number=$interval->format('%Y');
           $var='Years';
           
        }
        switch($number){
            case 1:
            $new=rtrim($var,'s');
            echo  $number.'&nbsp'.$new.'&nbsp'.'ago';
            break;

            default:
            echo  $number.'&nbsp'.$var.'&nbsp'.'ago';
            break;
        }

       
        
        ?>


   

                                                                                     
                                                                                   
                                                                             </div>
                                                                        </div>
                                                                <?php } ?>
                                                                       
                                                                    </li>
                                                                    <li>
                                                                <?php foreach ($completed_tickets as $row){?>  
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-sm label-success">
                                                                                        <i class="fa fa-bell-o"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                                    <div class="desc"> A Ticket with Ticket ID - <strong><?php echo $row['ticket_id'];?></strong> has been completed.
                                                                                    <span class="label label-sm label-success" style="background-color: green";> Completed Ticket
                                                                                    
                                                                                        </span>
                                                                                      
                                                                                      
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date">
                                                                            <?php  
$datetime1 = new DateTime(date("Y-m-d H:i:s"));
$datetime2 = new DateTime($row['last_update']);
$interval = $datetime1->diff($datetime2);
$hour= $interval->format('%H');
$day= $interval->format('%d');
$month= $interval->format('%m');
$min= $interval->format('%i');
$sec= $interval->format('%s');
$year= $interval->format('%Y');
$number=0;
                                                                                     
//echo $interval->format('%Y years %m months %d days %H hours %i minutes %s seconds');
       if($year == 0 && $month != 0){
           
          $number=$interval->format('%m');
           $var='Months';
        }
        else if($year == 0 && $month == 0 && $day != 0){
           $number=$interval->format('%d');
           $var='Days';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour != 0){
            $number=$interval->format('%H');
            $var='Hours';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min != 0){
            $number=$interval->format('%i');
            $var='Minutes';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min == 0 && $sec != 0){
           $number=$interval->format('%i');
           $var='Seconds';
        }
        else{
           $number=$interval->format('%Y');
           $var='Years';
           
        }
        switch($number){
            case 1:
            $new=rtrim($var,'s');
            echo  $number.'&nbsp'.$new.'&nbsp'.'ago';
            break;

            default:
            echo  $number.'&nbsp'.$var.'&nbsp'.'ago';
            break;
        }

       
        
        ?>



                                                                                     
                                                                                   
                                                                             </div>
                                                                        </div>
                                                                <?php } ?>
                                                                       
                                                                    </li>
                                                                   
                                                                    <li>
                                                                <?php foreach ($spare_request as $row){?>  
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-sm label-success">
                                                                                        <i class="fa fa-bell-o"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                                    <div class="desc"> A Ticket with Ticket ID - <strong><?php echo $row['ticket_id'];?></strong> has been raised.
                                                                                    <span class="label label-sm label-info"> New Spare
                                                                                    
                                                                                        </span>
                                                                                      
                                                                                      
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date">
                                                                            <?php  
$datetime1 = new DateTime(date("Y-m-d H:i:s"));
$datetime2 = new DateTime($row['raised_time']);
$interval = $datetime1->diff($datetime2);
$hour= $interval->format('%H');
$day= $interval->format('%d');
$month= $interval->format('%m');
$min= $interval->format('%i');
$sec= $interval->format('%s');
$year= $interval->format('%Y');
$number=0;
                                                                                     
       if($year == 0 && $month != 0){
           
          $number=$interval->format('%m');
           $var='Months';
        }
        else if($year == 0 && $month == 0 && $day != 0){
           $number=$interval->format('%d');
           $var='Days';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour != 0){
            $number=$interval->format('%H');
            $var='Hours';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min != 0){
            $number=$interval->format('%i');
            $var='Minutes';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min == 0 && $sec != 0){
           $number=$interval->format('%i');
           $var='Seconds';
        }
        else{
           $number=$interval->format('%Y');
           $var='Years';
           
        }
        switch($number){
            case 1:
            $new=rtrim($var,'s');
            echo  $number.'&nbsp'.$new.'&nbsp'.'ago';
            break;

            default:
            echo  $number.'&nbsp'.$var.'&nbsp'.'ago';
            break;
        }

       
        
        ?>



                                                                                     
                                                                                   
                                                                             </div>
                                                                        </div>
                                                                <?php } ?>
                                                                       
                                                                    </li>

                                                                    


                                                                    
                                                                       
                                                                       
                                                                   
                                                                </ul>
                                                            </div>

                                        </div>
                                        <div class="tab-pane" id="tab_1_2">
                                        <div class="scroller" style="height: 320px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                                                <ul class="feeds">
                                                                <li>
                                                                    
                                                                <?php foreach ($new_users as $row){?>  
                                                                <?php //echo($row['role']); ?>
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-sm label-success">
                                                                                        <i class="fa fa-bell-o"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                               
                                                                               
                                                                                    <div class="desc">
                                                                                    <?php if($row['role']=='Service_Desk'){ ?>
                                                                                     A Service Desk Operator with User ID - <strong><?php echo $row['userfname'];?></strong> has been Added.
                                                                                     <?php } elseif($row['role']=='Manager' || $row['role']=='Service_Manager'){ ?>
                                                                                     A Service Manager Operator with User ID - <strong><?php echo $row['userfname'];?></strong> has been Added.
                                                                                     <?php } elseif($row['role']=='Admin'){ ?>
                                                                                     An Admin with User ID - <strong><?php echo $row['userfname'];?></strong> has been Added.
                                                                                     <?php } elseif($row['role']=='Call-coordinator'){ ?>
                                                                                     A Call-coordinator with User ID - <strong><?php echo $row['userfname'];?></strong> has been Added.
                                                                                     
                                                                                     <?php } elseif($row['role']=='Technician'){ ?>
                                                                                     A Technician with Technician ID - <strong><?php echo $row['useremployeeid'];?></strong> has been Added.
                                                                                     <?php } ?>

                                                                                    </div>
                                                                                
                                     
                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date">
                                                                            <?php  
$datetime1 = new DateTime(date("Y-m-d H:i:s"));
$datetime2 = new DateTime($row['last_update']);
$interval = $datetime1->diff($datetime2);
$hour= $interval->format('%H');
$day= $interval->format('%d');
$month= $interval->format('%m');
$min= $interval->format('%i');
$sec= $interval->format('%s');
$year= $interval->format('%Y');
$number=0;
                                                                                     
       if($year == 0 && $month != 0){
           
          $number=$interval->format('%m');
           $var='Months';
        }
        else if($year == 0 && $month == 0 && $day != 0){
           $number=$interval->format('%d');
           $var='Days';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour != 0){
            $number=$interval->format('%H');
            $var='Hours';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min != 0){
            $number=$interval->format('%i');
            $var='Minutes';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min == 0 && $sec != 0){
           $number=$interval->format('%i');
           $var='Seconds';
        }
        else{
           $number=$interval->format('%Y');
           $var='Years';
           
        }
        switch($number){
            case 1:
            $new=rtrim($var,'s');
            echo  $number.'&nbsp'.$new.'&nbsp'.'ago';
            break;

            default:
            echo  $number.'&nbsp'.$var.'&nbsp'.'ago';
            break;
        }

       
        
        ?>



                                                                                     
                                                                                   
                                                                             </div>
                                                                        </div>
                                                                <?php } ?>
                                                                       
                                                                    </li>
                                                                    
                                                               
                                                                  
                                                                </ul>
                                                            </div>

                                        </div>
                                        <div class="tab-pane" id="tab_1_3">
                                        <div class="scroller" style="height: 320px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                                                <ul class="feeds">
                                                                <li>
                                                                    
                                                                <?php foreach ($spares as $row){?>  
                                                                <?php //echo($row['role']); ?>
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-sm label-success">
                                                                                        <i class="fa fa-bell-o"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                               
                                                                               
                                                                                    <div class="desc">
                                                                                    
                                                                                    A spare with spare code <strong><?php echo $row['spare_name'];?></strong> has been Added.
                                                                           
                                                                                    </div>
                                                                                
                                     
                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date">
                                                                            <?php  
$datetime1 = new DateTime(date("Y-m-d H:i:s"));
$datetime2 = new DateTime($row['last_update']);
$interval = $datetime1->diff($datetime2);
$hour= $interval->format('%H');
$day= $interval->format('%d');
$month= $interval->format('%m');
$min= $interval->format('%i');
$sec= $interval->format('%s');
$year= $interval->format('%Y');
$number=0;
                                                                                     
       if($year == 0 && $month != 0){
           
          $number=$interval->format('%m');
           $var='Months';
        }
        else if($year == 0 && $month == 0 && $day != 0){
           $number=$interval->format('%d');
           $var='Days';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour != 0){
            $number=$interval->format('%H');
            $var='Hours';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min != 0){
            $number=$interval->format('%i');
            $var='Minutes';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min == 0 && $sec != 0){
           $number=$interval->format('%i');
           $var='Seconds';
        }
        else{
           $number=$interval->format('%Y');
           $var='Years';
           
        }
        switch($number){
            case 1:
            $new=rtrim($var,'s');
            echo  $number.'&nbsp'.$new.'&nbsp'.'ago';
            break;

            default:
            echo  $number.'&nbsp'.$var.'&nbsp'.'ago';
            break;
        }

       
        
        ?>



                                                                                     
                                                                                   
                                                                             </div>
                                                                        </div>
                                                                <?php } ?>
                                                                       
                                                                    </li>
                                                                    
                                                               
                                                                  
                                                                </ul>
                                                            </div>

                                        </div>
<!-- Spares end-->
<!--Training start-->
<div class="tab-pane" id="tab_1_4">
                                        <div class="scroller" style="height: 320px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                                                <ul class="feeds">
                                                                <li>
                                                                    
                                                                <?php foreach ($training as $row){?>  
                                                                <?php //echo($row['role']); ?>
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-sm label-success">
                                                                                        <i class="fa fa-bell-o"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                               
                                                                               
                                                                                    <div class="desc">
                                                                                    
                                                                                    A Training with training Id <strong><?php echo $row['title'];?></strong> has been Added.
                                                                           
                                                                                    </div>
                                                                                
                                     
                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date">
                                                                            <?php  
$datetime1 = new DateTime(date("Y-m-d H:i:s"));
$datetime2 = new DateTime($row['last_update']);
$interval = $datetime1->diff($datetime2);
$hour= $interval->format('%H');
$day= $interval->format('%d');
$month= $interval->format('%m');
$min= $interval->format('%i');
$sec= $interval->format('%s');
$year= $interval->format('%Y');
$number=0;
                                                                                     
       if($year == 0 && $month != 0){
           
          $number=$interval->format('%m');
           $var='Months';
        }
        else if($year == 0 && $month == 0 && $day != 0){
           $number=$interval->format('%d');
           $var='Days';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour != 0){
            $number=$interval->format('%H');
            $var='Hours';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min != 0){
            $number=$interval->format('%i');
            $var='Minutes';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min == 0 && $sec != 0){
           $number=$interval->format('%i');
           $var='Seconds';
        }
        else{
           $number=$interval->format('%Y');
           $var='Years';
           
        }
        switch($number){
            case 1:
            $new=rtrim($var,'s');
            echo  $number.'&nbsp'.$new.'&nbsp'.'ago';
            break;

            default:
            echo  $number.'&nbsp'.$var.'&nbsp'.'ago';
            break;
        }

       
        
        ?>



                                                                                     
                                                                                   
                                                                             </div>
                                                                        </div>
                                                                <?php } ?>
                                                                       
                                                                    </li>
                                                                    
                                                               
                                                                  
                                                                </ul>
                                                            </div>

                                        </div>



                                        <div class="tab-pane" id="tab_1_5">
                                        <div class="scroller" style="height: 320px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                                                <ul class="feeds">
                                                                <li>
                                                                    
                                                                <?php foreach ($slamapping as $row){?>  
                                                                <?php //echo($row['role']); ?>
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-sm label-success">
                                                                                        <i class="fa fa-bell-o"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                               
                                                                               
                                                                                    <div class="desc">
                                                                                    
                                                                                <?php if($row['product']=="all" && $row['category']=="all"){ ?>
                                                                                    SLA mapping is done for the company <strong><?php echo $row['company_name'];?></strong>
                                                                                 <?php } else if($row['product']!="all" && $row['category']=="all"){ ?>

                                                                                    SLA mapping is done for the product with product id <strong><?php echo $row['product_name'];?></strong>
                                                                                 <?php } else if($row['product']!="all" && $row['category']!="all"){ ?>
                                                                                     SLA mapping is done for the sub category with category id   <strong><?php echo $row['category'];?></strong>
                                                                                 <?php } ?>   

                                                                                    </div>
                                                                                
                                     
                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date">
                                                                            <?php  
$datetime1 = new DateTime(date("Y-m-d H:i:s"));
$datetime2 = new DateTime($row['last_update']);
$interval = $datetime1->diff($datetime2);
$hour= $interval->format('%H');
$day= $interval->format('%d');
$month= $interval->format('%m');
$min= $interval->format('%i');
$sec= $interval->format('%s');
$year= $interval->format('%Y');
$number=0;
                                                                                     
       if($year == 0 && $month != 0){
           
          $number=$interval->format('%m');
           $var='Months';
        }
        else if($year == 0 && $month == 0 && $day != 0){
           $number=$interval->format('%d');
           $var='Days';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour != 0){
            $number=$interval->format('%H');
            $var='Hours';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min != 0){
            $number=$interval->format('%i');
            $var='Minutes';
        }
        else if($year == 0 && $month == 0 && $day == 0 && $hour == 0 && $min == 0 && $sec != 0){
           $number=$interval->format('%i');
           $var='Seconds';
        }
        else{
           $number=$interval->format('%Y');
           $var='Years';
           
        }
        switch($number){
            case 1:
            $new=rtrim($var,'s');
            echo  $number.'&nbsp'.$new.'&nbsp'.'ago';
            break;

            default:
            echo  $number.'&nbsp'.$var.'&nbsp'.'ago';
            break;
        }

       
        
        ?>



                                                                                     
                                                                                   
                                                                             </div>
                                                                        </div>
                                                                <?php } ?>
                                                                       
                                                                    </li>
                                                                    
                                                               
                                                                  
                                                                </ul>
                                                            </div>

                                        </div>
<!--SLA mapping end -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
</div>
                                            <!--dhanu-->
                                            


                  </div>
                  </div>
               </div>
               <?php include 'assets/lib/footer.php'?>
            </div>
            <?php include 'assets/lib/javascript.php'?>


         </body>
      </html>
      <script>
      </script>