<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<?php
 		$company_id=$this->session->userdata('companyid');
         include 'assets/lib/cssscript.php'?>
		 
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo">
    <!-- BEGIN CONTAINER -->
    <div class="wrapper">
        <!-- BEGIN HEADER -->
        <?php include "assets/lib/manager_header.php"?>
        <!-- END HEADER -->
        <div class="container-fluid">
            <div class="page-content">
                <!-- BEGIN BREADCRUMBS -->
                <div class="breadcrumbs">
                    <h1>FieldPro Service</h1>
                </div>
                <!-- END BREADCRUMBS -->
                <!-- BEGIN PAGE BASE CONTENT -->
                <div class="page-content-container">
                    <div class="page-content-row">
                        <div class="page-content-col">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box dark">
                                <div class="portlet-title">

                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                    </div>
									
									<div class="x_content">
						<div class="form-group row">
						  <label  class="col-xs-3 col-form-label">Average Calls/Day</label>
						  <div class="col-xs-8">
							<input class="form-control" type="number" value="" id="avgcall" name="avgcall" >
						  </div>
						</div>
						<div class="form-group row">
						  <label  class="col-xs-3 col-form-label">First-time Fix Rate	</label>
						  <div class="col-xs-8">
							<input class="form-control" type="number" value="" id="first_time" name="first_time" >
						  </div>
						</div>
						<div class="form-group row">
						  <label  class="col-xs-3 col-form-label">Customer Feed Back</label>
						  <div class="col-xs-8">
							<input class="form-control" type="number" value="" id="cus_feed" name="cus_feed" >
						  </div>
						</div>
						<div class="form-group row">
						  <label  class="col-xs-3 col-form-label">Service Contract Revenue</label>
						  <div class="col-xs-8">
							<input class="form-control" type="number" value="" id="service_revev" name="service_revev" >
						  </div>
						</div>
					
						<div class="form-group row">
						  <label  class="col-xs-3 col-form-label">Customer Rating	</label>
						  <div class="col-xs-8">
							<input class="form-control" type="number" value="" id="custo_rat" name="custo_rat" >
						  </div>
						</div>
						<div class="form-group row">
						  <label  class="col-xs-3 col-form-label">Contribution to KB	</label>
						  <div class="col-xs-8">
							<input class="form-control" type="number" value="" id="cont_kb" name="cont_kb" >
						  </div>
						 
						</div>
						<div class="form-group row">
						<label  class="col-xs-3 col-form-label">Training & Certification</label>
						 <div class="col-xs-8">
							<input class="form-control" type="number" value="" id="training" name="training" >
						  </div>
						</div>
						<div class="form-group row">
						  <label  class="col-xs-3 col-form-label">SLA adherence </label>
						 <div class="col-xs-8">
							<input class="form-control" type="number" value="" id="sla_adher" name="sla_adher">
						  </div>
						</div>
						<div class="form-group row">
							<div class="col-md-6 col-md-offset-5">
							  <button type="submit" class="btn btn-primary">Cancel</button>
							  <button id="send" type="button" onclick="add_score();" class="btn btn-success">Submit</button>
							</div>
						</div>
					</div>
									
									
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
    </div>
	<?php include 'assets/lib/footer.php';?>
	<?php include 'assets/lib/javascript.php';?>
        <script>
            var resizefunc = [];
        </script>
<script>
        		
       		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
        	$('#man_serv').addClass('open');
		
</script>
        <script type="text/javascript">window.history.forward();
            $(document).ready(function() {
                $('#datatable').dataTable();
            } );
			
	function add_score()
		{
		var avgcall=$('#avgcall').val();
		var first_time=$('#first_time').val();
		var cus_feed=$('#cus_feed').val();
		var service_revev=$('#service_revev').val();
		var custo_rat=$('#custo_rat').val();
		var cont_kb=$('#cont_kb').val();
		var training=$('#training').val();
		var sla_adher=$('#sla_adher').val();
var company_id="<?php echo $company_id;?>";
		   $.ajax({
				url: "<?php echo base_url();?>" + "index.php?/Controller_admin/add_lead",
				type: 'POST',
				data: {'avgcall':avgcall,'first_time':first_time,'cus_feed':cus_feed,'service_revev':service_revev,'custo_rat':custo_rat,'cont_kb':cont_kb,'training':training,'sla_adher':sla_adher,'company_id':company_id},
				success: function(data) {
								alert(data);
window.location.reload();
								}
	});
	}
 </script>
	</body>
</html>