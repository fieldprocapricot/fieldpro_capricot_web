<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<?php
 		$company_id=$this->session->userdata('companyid');
         include 'assets/lib/cssscript.php'?>
		 
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo">
    <!-- BEGIN CONTAINER -->
    <div class="wrapper">
        <!-- BEGIN HEADER -->
        <?php include "assets/lib/header_service.php"?>
        <!-- END HEADER -->
        <div class="container-fluid">
            <div class="page-content">
                <!-- BEGIN BREADCRUMBS -->
                <div class="breadcrumbs">
                    <h1>FieldPro Service</h1>
                    <ol class="breadcrumb">
                        <li>
                            <a href="#">Dashboard</a>
                        </li>
                        <li class="active">Impreset Spare</li>
                    </ol>
                </div>
                <!-- END BREADCRUMBS -->
                <!-- BEGIN PAGE BASE CONTENT -->
                <div class="page-content-container">
                    <div class="page-content-row">
                        <div class="page-content-col">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box dark">
                                <div class="portlet-title">

                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                    </div>
<button id="sample_editable_1_new" class="btn btn-circle green btn-outline" data-toggle="modal" data-target="#myModal25" data-backdrop="static" data-keyboard="false"> Bulk Upload
															<i class="fa fa-upload"></i>
														</button>
                                     <div class="tab-pane active" id="Accepted">
                                            <div class="table-responsive">

                                                <table class="table table-hover" id="sample_2">
                                                    <thead>
                                                       <tr>
													<th style="text-align:center">Technician ID</th>
													<th style="text-align:center">Technician Name</th>
													<th style="text-align:center">Spare Required</th>
    												<th style="text-align:center">Status</th>
													</tr>
                                                    </thead>
                                                        <tbody id="tbdy_attendance">
						 <?php  							
						foreach ($records as $row) {
							$string  = json_encode(json_decode($row['spare_array']));
								?>
								<tr>
								  
									<td style="text-align:center" id="<?php echo $row['employee_id']; ?>" onclick="hover_tech(this.id,'<?php echo $row['first_name']; ?>','<?php echo $row['skill_level']; ?>','<?php echo $row['contact_number']; ?>','<?php echo $row['location']; ?>','<?php echo $row['product_name']; ?>','<?php echo $row['cat_name']; ?>')";><a><?php echo $row['employee_id']; ?></a></td>
<td style="text-align:center"><?php echo $row['first_name']; ?></td>
									<td style="text-align:center">
									<button class="btn btn-circle coral btn-outline btn-sm" id="<?php echo $row['employee_id']; ?>" onclick=hover_spare(this.id,'<?php echo $string; ?>');>View</button>
									</td>
									 <td style="text-align:center">
                      <?php if ($row['status_comment']==0){echo 'In-Progress';}  else{echo 'Delivered';}?>
                                                                        </td>
													</tr>
													<?php } ?>
												</tbody>
                                                </table>
<div class="row">
												   <div class="col-lg-12">
													<button type="button" class="btn blue btn-outline btn-circle btn-md pull-right" id="imprest_download_spareform">Download</button>     
													</div>
													</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
    </div>
 <div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
				<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Details</h5>
      </div>
      <div class="modal-body"id='modal_display' style="text-align:center !important">
		<div class="table-responsive">
			<table class="table table-hover ">
				<thead>
					<td>Spare code</td>
					<td>Spare Required</td>
					<td>Spare Location</td>
				</thead>
				<tbody id="spare_details">
				</tbody>
			</table>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
            </div>

        </div>

			<div id="myModal1" class="modal fade" role="dialog">
	  <div class="modal-dialog">
				<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Details</h5>
      </div>
      <div class="modal-body"id='modal_tech'>
		<form class="form-horizontal" role="form" >
                 
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
            </div>

        </div>
			<div id="myModa2" class="modal fade" role="dialog">
	  <div class="modal-dialog">
				<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Details</h5>
      </div>
      <div class="modal-body"id='modal_tic'>
		<form class="form-horizontal" role="form" >
                 
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
            </div>

        </div>
     <div id="myModal25" class="modal fade" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Bulk Upload</h4>
                    </div>
                    <div class="modal-body">
                    	<form action="#" class="form-horizontal form-bordered">
                            <div class="form-body row">
                            	<div class="form-group col-md-4 col-sm-12 text-center">
                                	<?php $fname='customer.xlsx'; ?>
                                    <a class="btn btn-circle purple-sharp btn-outline sbold uppercase" href="<?php echo base_url(); ?>index.php?/controller_admin/download_sampletemplate/<?php echo $fname;?>">Sample Template</a>
                                </div>
                                <div class="form-group col-md-6 col-sm-12">
                                    <div class="col-md-3">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select file </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="add_excel" id="add_excel"> </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                              </div>
                            </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-circle dark btn-outline" id="bulkupload" name="bulkupload"><i class="fa fa-upload"></i> Upload</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>	

        <!-- END wrapper -->
<script>                
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
            $('#imprest').addClass('open');
        </script>
<?php include 'assets/lib/javascript.php'?>

	<script>window.history.forward();
		$(document).ready(function(){
			$(".datatable").DataTable({
				dom: "Bfrtip",
				buttons: [
					{
					  extend: "copy",
					  className: "btn-sm"
					},
					{
					  extend: "csv",
					  className: "btn-sm"
					},
					{
					  extend: "excel",
					  className: "btn-sm"
					},
					{
					  extend: "pdfHtml5",
					  className: "btn-sm"
					},
					{
					  extend: "print",
					  className: "btn-sm"
					}
				]
			});
		});
	</script>
	
   
        <script>
            var resizefunc = [];
        </script>
<!--For Spare form download-->
		<script>
			$('#imprest_download_spareform').click(function(){
				var data1 = [{a:1,b:10},{a:2,b:20}];
				var data2 = [{a:100,b:10},{a:200,b:20}];
				var opts = [{sheetid:'One',header:true},{sheetid:'Two',header:false}];				
				var company_id="<?php echo $this->session->userdata('companyid')?>";
				$.ajax({
					url			:	 "<?php echo base_url();?>" + "index.php?/controller_service/imprest_download_spareform",
					type		: 	 'POST',
					data		: 	 {'company_id':company_id},
					dataType	:	 "JSON",
					success		: 	function(data) {
									//var parsed = JSON.parse(data);
									//console.log(data);				
									saveAsXlsx1(name,data);
									//var res = alasql('SELECT * INTO XLSX("imprest_spareform.xlsx",?) FROM ?',[parsed]);
								}
				});
			});
			function saveAsXlsx1(name,test){	
				//alasql('SELECT * INTO XLSX("Storemaster.XLSX",{headers:false}) FROM ?',[test]);
				alasql('SELECT * INTO XLSX("Imprest_Storemaster.XLSX",{headers:true}) FROM ?',[test]);

			}
		</script>
	  <!--For Spare form download-->

        <script type="text/javascript">
            $(document).ready(function() {
                //$('#datatable').DataTable();
                /* $('#datatable-keytable').DataTable( { keys: true } );
                $('#datatable-responsive').DataTable();
                $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } ); */
            } );
            //TableManageButtons.init();

        </script>

<script>
		function accepts(id){
            $.ajax({
               url: "<?php echo base_url();?>" + "index.php?/controller_manager/impresed_status",
               type: 'POST',
               data: {'id':id},
               //dataType: "json",
               success: function(data) {
                  alert(data);
                   //location.reload();
               }
            });
         };
         function Rejects(id){
            $.ajax({
               url: "<?php echo base_url();?>" + "index.php?/controller_manager/impressed_accept_Spare",
               type: 'POST',
               data: {'id':id},
               //dataType: "json",
               success: function(data) {
                     
                  alert(data);   
                   location.reload();
               }
            });
         }
		    
        function hover_tech(tech_id,first_name,Skill_level,contact_number,location,Product_name,cat_name)
	  {
		//cat_name = cat_name.replace(/\:/g," "); 
		  $('#modal_tech form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Technician ID</label><div class="col-sm-6 control-label">'+tech_id+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Name</label><div class="col-sm-6 control-label">'+first_name+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Skill Level</label><div class="col-sm-6 control-label">'+Skill_level+'</div></div><div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Contact Number</label><div class="col-sm-6 control-label">'+contact_number+'</div></div><div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Location</label><div class="col-sm-6 control-label">'+location+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Product Category </label><div class="col-sm-6 control-label">'+Product_name+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Sub Category</label><div class="col-sm-6 control-label">'+cat_name+'</div></div>');
		  $('#myModal1').modal('show');
	  }
		  function hover_tic(ticket_id,cus_id,cus_name,amc_id,call_type,model)
	  {
		 
		  $('#modal_tic form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Ticket ID</label><div class="col-sm-6 control-label">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Customer Id</label><div class="col-sm-6 control-label">'+cus_id+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Customer Name</label><div class="col-sm-6 control-label">'+cus_name+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >AMC Id </label><div class="col-sm-6 control-label">'+amc_id+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Contract Type</label><div class="col-sm-6 control-label">'+call_type+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Model</label><div class="col-sm-6 control-label">'+model+'</div></div>');
		  $('#myModa2').modal('show');
	  }	  function hover_spare(ticket_id,spare_array)
	{
         $('#spare_details').empty(); 
            var data = spare_array;
            console.log(data);
			 
			  if(data == "null"){
                                $.dialogbox({
      							type:'msg',
      							content:'No Data',
      							closeBtn:true,
      							btn:['Ok.'],
      							call:[
      								function(){
      									$.dialogbox.close();
      								}
      							]
      
      						});  
                        }
                else{
                var data = JSON.parse(spare_array);
				for(i=0; i<data.length; i++)
				{
				$('#spare_details').append('<tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr>');
				} 
						                             //console.log(data);
                                                      $('#myModal').modal('show');
                                                        }
       
                          
                                }
 function hover_spare_accept(ticket_id,spare_array)
	  {
		  $('#spare_details').empty(); 
		 var data = JSON.parse(spare_array);
		// console.log(data);

		for(i=0; i<data.length; i++)
		 {
			 $('#tbdy_attendance').append('<tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr>');
		   
		 }
		 /*$('#modal_display').html('<table><thead><th>Spare Code</th><th>Spare Required</th><th>Spare Source</th></thead>');
		 for(i=0; i<data.length; i++)
		 {
			 $('#modal_display').append('<tbody><tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr></tbody>');
		   
		 }
		$('#modal_display').append('</table>');*/
		  $('#myModal').modal('show');
		
	  }
	  function hover_ticket(ticket_id,customer_name,location,product_name,cat_name,problem,priority)
	  {
		  var replaceSpace=problem;
		problem = problem.replace(/\:/g," ");
		var location=location;
		location= location.replace(/\:/g," ");
		var product_name=product_name;
		product_name= product_name.replace(/\:/g," ");
		var cat_name=cat_name;
		cat_name= cat_name.replace(/\:/g," ");	
		  $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 control-label"for="inputEmail3">Ticket ID</label><div class="col-sm-4 control-label">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Customer Name</label><div class="col-sm-4 control-label">'+customer_name+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Location</label><div class="col-sm-4 control-label">'+location+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >-Category</label><div class="col-sm-4 control-label">'+product_name+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Sub-Category</label><div class="col-sm-4 control-label">'+cat_name+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Problem</label><div class="col-sm-4 control-label">'+problem+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Priority</label><div class="col-sm-4 control-label">'+priority+'</div></div></div>');
		  $('#myModal').modal('show');
	  }
	  
        var company_id="<?php echo $company_id;?>";
	   
		
		function viewimage(id){	
			$.ajax({
				url: "<?php echo base_url();?>" + "index.php?/controller_manager/view",
				type: 'POST',
				data: {'id':id},
				//dataType: "JSON",
				success: function(data) {
					var data=JSON.parse(data);
					//console.log(data['view']);	
					$("#token").attr('src', data['view']);
					$("#myModal").modal('show');
					
				}
			});
		}
		 $('#bulkupload').click(function(){
				var ext = $('#add_excel').val().toString().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['xls', 'xlsx', 'csv']) == -1) {
                    alert('Please upload Excel file');
                    return false;
                }else {
                    var file_data = $('#add_excel').prop('files')[0];   
                    var form_data = new FormData();
                    form_data.append('add_excel', file_data);
                    $.ajax({                
                        type:'POST',
                        url:'<?php echo base_url(); ?>index.php?/controller_service/bulk_impreset',
                        contentType:false,
                        processData: false,
                        cache:false,
                        data:form_data,         
                        success: function (data) {
                           alert(data);
                            /*bootbox.alert({
                                message: data,
                                callback: function () {
                                    location.reload();
                                }
                            })*/  
                        },
                    });         
                }
            })
		</script>
		 <script type="text/javascript">			
          //$('.datatable').dataTable();
          $('#datatable-keytable').DataTable( { keys: true } );
          $('#datatable-responsive').DataTable();
          $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
          var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } ); 
    </script>
	</body>
</html>