<!DOCTYPE html>
<html><head>
<meta chartset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
<title>Service Call Report</title>
<style>
.page-container {
max-width:650px;
margin:0 auto;
border: 2px solid #000;
}
	
.address-right { /*float:left; */padding: 8px }
.address-right p{ line-height:6px; }
.comp-name { font-weight:bold; font-size:22px; margin: 0px; }
.pageclear {clear:both; display:block; }
/*.logo-right { float:right; }*/
.logo-top { font-size:32px;font-weight: bold;padding: 50px 20px; }
.logo-top span { color:#0099CC; }
.page-divide { border-bottom: 2px solid #000; }
.id-date div { /*float:left; width:310px;*/ font-weight:bold; position:relative; top:-15px;/* padding: 0 8px; */}
.service-report { text-align:center; font-size:16px; }
/*.date { padding-left: 28%;} */
.id-date p { /* height:20px; */ }
/*@media screen and (min-width:992px) {
.col-6 { width:50%; float:left; position:relative; }
.col-3 { width:33%; float:left; position:relative; } }*/
.cus-address { text-decoration: underline; }
.address-content { height: 130px;  padding: 6px; }
.address-content{line-height: 6px;}
.address-left { border-left: 1px solid #000; }
.product-title { padding: 6px; }
.summary { height: 80px; padding: 6px; }
.summary-title { text-decoration: underline; font-weight: bold; }
.input-fields { padding: 10px; }
.input-fields2 { padding: 10px; border-left:1px solid #000; }
.input-data { border:none; border-bottom:1px solid #000; width: 56%; margin-left: 5px; }
.input-form {  padding: 12px 0; }
.date input { width:82%; }
</style>
</head><body>
<?php
if($case_id!='')
{
	$csid=$case_id;
}
else
{
	$csid='None';
}

if($service_id == 9)
{
	$company_email = 'sysadmin@capricot.com';
}
else
{
	$company_email = 'hwsupport@arkance.world';
}


?>
<div class="page-container">
<table>
<tr>
<td width="350">
<div class="address-right">
<h4 class="comp-name"><?php echo $company_name; ?></h4>
<p><?php echo wordwrap($company_dno.','.$company_address,23, "<br><br><br>", TRUE); ?></p>
<p><?php echo $company_city.' -'. $company_pincode; ?></p>
<p><b>Ph :</b> <?php echo $company_contact; ?></p>
<p><b>Email ID :</b> <?php echo $company_email; ?></p>
</div>
</td>
<td width="100">
<div class="logo-right" > <div class="logo-top" align="right"><img src ="<?=$company_logo?>" alt="Company_logo" height="100" width="150"></div> </div>
</td>
</tr>
</table>
<div class="page-divide"></div>
<div class="id-date">
<table>
<tr>
<td width="100"><div><p>Call Id: <b><?=$ticket_id?></b> </p> </div> </td>
<td width="250"><div><p class="service-report"><?=$service_group_name?> report  </p> </div></td>
<td width="150"><div><p class="date">Date: <b><?=$ticket_date?></b> </p> </div> </td>
</tr>
</table>
</div>
<span class="pageclear"></span>
<div class="page-divide"></div>
<table width="100%" border="1" cellpadding="2" cellspacing="2">
<tr>
<td >
<div class="col-12">
<div class="address-content">
	<div class="cus-address"><b>Customer Contact Details :</b></div>
<p>Mr/Mrs. <?php echo $customer_name; ?> </p>
<p><?php echo $customer_door_no.','.$cust_address; ?></p>
<p><?php echo $cust_town.','.$cust_city.'-'.$cus_pincode; ?></p>
<p><b>Email ID :</b> <?=$cust_email?></p>
<p><b>Ph :</b> <?=$cust_mobile?></p>
</div>
</div>
</td>
</tr>
<tr>
<table width="100%">
<tr>
<td >
<div class="col-3">
<div class="product-title">Product: <b><?=$product_id?></b> </div>
</div>
</td>
<td >
<div class="col-3">
<div class="product-title">Model No: <b> <?=$model?></b></div>
</div>
</td>
<td >
<div class="col-3">
<div class="product-title">Case ID:  <b><?=$csid?></b></div>
</div>
</td>
<td>
<div class="col-3">
<div class="product-title">Serial No:  <b><?=$serial_no?></b></div>
</div>
</td>
</tr>
</table>
</tr>
</table>	
<span class="pageclear"></span>
<div class="page-divide"></div>
<div class="summary">
<div class="summary-title"> Problem Description: </div>
<?php echo $prob_descip; ?>
</div>
<span class="pageclear"></span>
<div class="page-divide"></div>	
<div class="summary">
<div class="summary-title"> Resolution Summary: </div>
<?php echo $rsummary; ?>
</div>
<?php
if($service_id!='' || $service_id!='1')
{
?>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
	<tr>
		<th width="10%">#</th>
		<th width="40%">Check Points</th>
		<th width="20%">Yes / No</th>
		<th width="30%">Remarks</th>
	</tr>
<?php
$i=1;
foreach($activities as $row)
{
?>
<tr>
	<td width="10%"><?php echo $i; ?></td>
	<td width="40%"><?php echo $row['description']; ?></td>
	<td width="20%"><?php 
if($row['ap_value']=='0')
{
echo "No";
}
elseif($row['ap_value']=='1')
{
echo "Yes";
}
else
{
echo $row['ap_value'];
}
?></td>
<td width="30%"><?php echo $row['remarks']; ?></td>
</tr>	
<?php
$i++;
  }
?>
</table>
<?php
}
else
{
}
?>
<span class="pageclear"></span>
<div class="page-divide"></div>
<div class="product-title">Call status: <b><?=$status_name?></b></div>
<span class="pageclear"></span>
<div class="page-divide"></div>
<table>
<tr>
<td width="230">
<div class="col-6">
<div class="input-fields">
<div class="input-form">
<label>Engineer Name: </label>
<span><?=$technician_name?></span>

</div>
<span class="pageclear"></span>
<div class="input-form">
<label>Signature / Date: </label>
<?=$ticket_date?>
</div>
<!-- Service image -->
<label class="service_img">
<?php
if($service_image != '' && $service_image != null)
{
	echo "Service Image :";
}
?>
</label>
<div class="input-form">    
<span><img src ="<?=$service_image?>" alt="service_image" height="250" width="250"></span>
</div>

<!-- Service image -->
</div>
</div>
</td>
<td width="230">
<div class="col-6">
<div class="input-fields2">
<div class="input-form">
<label>Customer Name: </label>

<span><?=$customer_name?></span>
</div>
<span class="pageclear"></span>
 <div class="input-form">
<!--<label>Signature: </label>-->
<span><img src ="<?=$signature?>" alt="Company_logo" height="80" width="100"></span>
</div>
<div class="input-form date">
<label>Date: </label>
<?=$ticket_date?>
</div>
</div>
</div>
</td>
</tr>
</table>
<span class="pageclear"></span>
</div>
</body></html>
