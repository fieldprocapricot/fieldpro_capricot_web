<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php 
               $company_id=$this->session->userdata('companyid');
               $region=$user['region'];$area=$user['area'];$location=$user['location'];
               include 'assets/lib/cssscript.php'?>
               <link href="assets/global/plugins/chart-graph/nv.d3.css" rel="stylesheet" type="text/css">
            
            <style>
               /* html, body, #chart1, svg {
               margin: 0px !important;
               padding: 0px !important;
               height: 100% !important;
               width: 100% !important;
               }*/
               .highcharts-button-symbol {
               display: none;
               }
               .highcharts-credits {
               display: none;
               }
            </style>
            <style>
               .amcharts-chart-div a {
               display: none !important;
               }
               .piechart5 {
               margin: -12% 8%;
               }
        .highcharts-button-box {
    display: none;
}
               @media screen and (min-width:1600px) and (max-width:2100px)
               {
               .piechart5 {
               margin: -10% 20%;
               }  
               }
            </style>
         </head>
         <!-- END HEAD -->
          <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header_service.php"?>
               <!-- END HEADER -->
               <div class="page-container">
         <div class="page-sidebar-wrapper">
         <?php include "assets/lib/service_sidebar.php"?>
         </div>
         <div class="page-content-wrapper">
                  <div class="page-content">
                    
                     <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mobile-chart1">
                           <div class="portlet light bg-inverse col-md-12" style="height:504px">
                              <div class="portlet-title">
                                 <div class="caption font-black">
                                    <span class="caption-subject bold uppercase">Current Tickets</span>
                                 </div>
                                 <div class="actions">
                                 <button class="btn btn-circle green btn-outline btn-sm" id="graph1export">
                                            <i class="fa fa-pencil"></i> Export </button>



                                        <button class="btn btn-circle green btn-outline btn-sm" id="graph1print">
                                            <i class="fa fa-print"></i> Print </button>
                                 </div>
                              </div>

                              <!--new-->
                              <div class="portlet-body">
                                 <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  pull-right">
                                       <select class="form-control" id="current_tickets" onChange="current_tickets();" name="role1">
                                          <option value='today' selected>Today</option>
                                          <option value='week'>This Week</option>
                                          <option value='month'>This Month</option>
                                          <option value='year' >This Year</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>

                              <!--<div style="height:360px">
                                 <svg id="test1" class="mypiechart"></svg>
                                 </div>-->
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <h5><b>Fresh:<span id='fresh_tic'></span></b></h5>
                                 <br>
                                 <div id="morris-bar-total" style="min-width: 270px; height: 300px; max-width: 400px; margin: 0 auto"></div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <h5><b>Carry Forward:<span id='carry_tic'></span></b></h5>
                                 <br>
                                 <div id="morris-bar-carry" style="min-width: 270px; height: 300px; max-width: 400px; margin: 0 auto"></div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                           <div class="portlet light bg-inverse col-md-12 mobile-chart2" style="height: 504px;">
                              <div class="portlet-title">
                                 <div class="caption font-black">
                                    <span class="caption-subject bold uppercase">Ticket Distribution</span>
                                 </div>
                                 <div class="actions">
                                   <button class="btn btn-circle green btn-outline btn-sm" id="graph2export">
                                            <i class="fa fa-pencil"></i> Export </button>



                                        <button class="btn btn-circle green btn-outline btn-sm" id="graph2print">
                                            <i class="fa fa-print"></i> Print </button>
                                 </div>
                              </div>
                              <div class="portlet-body">
                                 <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  pull-right">
                                       <select class="form-control" id="call_distribution" onChange="call_distribution();" name="role1">
                                          <option value='today' selected>Today</option>
                                          <option value='week'>This Week</option>
                                          <option value='month'>This Month</option>
                                          <option value='year' >This Year</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div id="chart1">
                                    <svg></svg>
                                 </div><br><br><br>
                              </div>
                           </div>
                        </div>  
  </div>
                        <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                           <div class="portlet light bg-inverse col-md-12">
                              <div class="portlet-title">
                                 <div class="caption font-black">
                                    <span class="caption-subject bold uppercase">Ticket Insights</span>
                                 </div>
                 <div class="actions">
                                         <button class="btn btn-circle green btn-outline btn-sm" id="graph3export">
                                            <i class="fa fa-pencil"></i> Export </button>



                                        <button class="btn btn-circle green btn-outline btn-sm" id="graph3print">
                                            <i class="fa fa-print"></i> Print </button>
                                    </div>
                              </div>
                              <div class="portlet-body">
                                 <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  pull-right">
                                       <select class="form-control" id="ticket_insight" onChange="ticket_insight();" name="role1">
                                          <option value='today' selected>Today</option>
                                          <option value='week'>This Week</option>
                                          <option value='month'>This Month</option>
                                          <option value='year' >This Year</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  pull-right">
                                       <div id="ticket_insight_graph" style="height: 345px;  position: relative;" class="m-t-10"> </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                           <div class="portlet light bg-inverse col-md-12" style="min-height:475px;">
                              <div class="portlet-title">
                                 <div class="caption font-black">
                                    <span class="caption-subject bold uppercase">C-SAT Insights(In Percentage)</span>
                                 </div>
                 <div class="actions">
<button class="btn btn-circle green btn-outline btn-sm" id="graph4export">
                                            <i class="fa fa-pencil"></i> Export </button>



                                        <button class="btn btn-circle green btn-outline btn-sm" id="graph4print">
                                            <i class="fa fa-print"></i> Print </button>                                   
    </div>
                              </div>
                              <div class="portlet-body">
                                 <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  pull-right">
                                       <select class="form-control" id="csat_insight" onChange="csat_insight();" name="role1">
                                          <option value='today' selected>Today</option>
                                          <option value='week'>This Week</option>
                                          <option value='month'>This Month</option>
                                          <option value='year' >This Year</option>
                                       </select>
                                    </div>
                                    <!--div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  pull-left">
                                       <select class="form-control" id="product_id" onChange="csat_insight();" name="role1">

                                       </select>
                                    </div-->
                                 </div>
                                 <div class="row">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12  pull-right">
                                       <!--<div id="csat_insight_graph" style="height: 345px;  position: relative;" class="m-t-10"> </div>-->
                                       <div id="csat_insight_graph" style="min-width: 250px; max-width: 600px; height: 325px; margin-left:18px;"></div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12  pull-right">
                                       <div id="morris-rat-total" style="min-width: 250px; max-width: 400px; height: 300px; margin-left:-20px;"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                      </div>
                </div>
<!-- BEGIN FOOTER -->
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
               </div>

            <!--Modal Starts-->
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Bulk Upload</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id='modal_display'>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" data-dismiss="modal">OK!</button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END QUICK SIDEBAR -->

            <!--loading model-->
	 <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->



            <?php include 'assets/lib/javascript.php'?> 
       

<script src="assets/global/plugins/chart-graph/d3.min.js" charset="utf-8"></script>
<script src="assets/global/plugins/chart-graph/nv.d3.js"></script>
<script src="assets/global/plugins/chart-graph/stream_layers.js"></script>
<!-- C-sat Insight Plugin-->
<script src="assets/global/plugins/chart-graph/highcharts/highcharts.js"></script>
<script src="assets/global/plugins/chart-graph/highcharts/exporting.js"></script>
            <script>
               $(document).ready(function() {
           $('.nav.navbar-nav').find('.open').removeClass( 'open' );
          $('#dashboard').addClass('open');
          });
            </script>
       <script>
               $(document).ready(function() {
                 $('#button').click(function () {
                     $('#todo').append("<ul>" + $("input[name=task]").val() + " <a href='#' class='close' aria-hidden='true'>&times;</a></ul>");
               $('#task').val('');
                 });
                 $("body").on('click', '#todo a', function () {
                     $(this).closest("ul").remove();
                 });
 var chart5="";
               var company_id="<?php echo $company_id;?>";
               var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="<?php echo $location;?>";
               //$("#status").fadeIn();
               //$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');
                      $.ajax({
                             url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
                             type: 'POST',
                             data: {'company_id':company_id},
                             dataType: "json",
                             success: function(data) {
                             $('#product_id').html(' <option selected value="">Product-Category</option>');
                             console.log(data);
                             for(i=0;i<data.length;i++)
                             {
                              $('#product_id').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
                             }
               
               // //$("#status").fadeOut();
               ////$("#preloader").fadeOut();
                             }
                             });

             



               var company_id="<?php echo $company_id;?>";
               var filter=$('#day').val();
               var fresh_result;
               var carry_result;
var chart10="";
               //$("#status").fadeIn();
               //$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');
               $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/tickets_count",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                    // $('#Searching_Modal').modal('hide');

                    console.log(data);
                    $('#morris-bar-total').empty();
                    $('#morris-bar-carry').empty();
                    var total=parseInt(data[2].assigned) + parseInt(data[3].accepted)+parseInt(data[5].escalated)+parseInt(data[6].spare_requested)+parseInt(data[7].work_in_progress)+ parseInt(data[8].unassigned)+ parseInt(data[9].deferred);
               if(total<10)
               {
               total='0'+total;
               }
                    $('#total_tic').html(total);
               
                    var fresh=parseInt(data[2].assigned) + parseInt(data[3].accepted)+ parseInt(data[8].unassigned)+ parseInt(data[9].deferred);
               if(fresh<10)
               {
               fresh='0'+fresh;
               }
                    var carry=parseInt(data[5].escalated) + parseInt(data[6].spare_requested)+ parseInt(data[7].work_in_progress);
                    if(carry<10)
               {
               carry='0'+carry;
               }
               $('#fresh_tic').html(fresh);
                    $('#carry_tic').html(carry);
                    fresh_result=[
                      { name: 'Assigned', y:  data[2].assigned},
                      { name: 'Accepted', y:  data[3].accepted},
                      { name: 'Unassigned', y:  data[8].unassigned},
                      { name: 'Deferred Support', y:  data[9].deferred}
                      ];
                      carry_result=[
                      { name: 'Escalated', y:  data[5].escalated},
                      { name: 'Spare Requested', y:  data[6].spare_requested},
                      { name: 'In progress', y:  data[7].work_in_progress}
                      ];
                    if(fresh==0)
               {
               $('#morris-bar-total').html('<div style="height: 260px; margin: 45% 13% !important; position: relative;"><h3>No Data Available</h3></div>');
               }
               else{
                chart10 = Highcharts.chart('morris-bar-total', {
               chart: {
               plotBackgroundColor: null,
               plotBorderWidth: null,
               plotShadow: false,
               backgroundColor: null,
               type: 'pie'
               },
               title: {
               text: ''
               },
               plotOptions: {
               pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
               }
               },"colors":["#337ab7","#d93d5e","#45b6af","#754DEB"],
               series: [{
               name: 'Tickets',
               colorByPoint: true,
               data: fresh_result
               }]
               });
               
               }if(carry==0)
               {
               $('#morris-bar-carry').html('<div style="height: 260px; margin: 45% 13% !important; position: relative;"><h3>No Data Available</h3></div>');
               }
               else{
               var chart = Highcharts.chart('morris-bar-carry', {
               chart: {
               plotBackgroundColor: null,
               plotBorderWidth: null,
               plotShadow: false,
               backgroundColor: null,
               type: 'pie'
               },
               title: {
               text: ''
               },
               plotOptions: {
               pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
               }
               },"colors":["#337ab7","#d93d5e","#45b6af","#754DEB"],
               series: [{
               name: 'Tickets',
               colorByPoint: true,
               data: carry_result
               }]
               });
               
               
                    
                  } 
               }
               });
                $("#graph1print").on('click', function (event) {

        chart10.print();

    });

    $("#graph1export").on('click', function (event) {
        chart10.exportChart({
            type: "image/jpeg"
        });
    });
               ////$("#status").fadeOut();
               ////$("#preloader").fadeOut();
               var filter=$('#call_distribution').val();
               var result= new Array();
               var res= new Array();
               //    //$("#status").fadeIn();
               ////$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');

               $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/ticket_distribution",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                     //$('#Searching_Modal').modal('hide');

                    //$('#chart1').empty();
                  //  $('#chart1').html('');
                    res=new Array(data[0]);var result= new Array();
                    $.each(res[0], function(key, value) {
                      result.push("{'label': '"+key+"','value': '"+value+"'}");
                    });
                    result=JSON.stringify(result);
                    result = result.replace(/\"/g,'');
                    result = result.replace(/\\/g,'');
                    result = result.replace(/\ /g,'');
                    result = result.replace(/\'/g,'"');
                    result=JSON.parse(result);
                    console.log(result);
                    //$('#chart1').empty();
                            historicalBarChart = [
               {
               key: "No. of Tickets",
               values:result
               }
               ];
               
               nv.addGraph(function() {
               var chart = nv.models.discreteBarChart()
               .x(function(d) { return d.label })
               .y(function(d) { return d.value }).valueFormat(d3.format('d'))
               .staggerLabels(true)
               //.staggerLabels(historicalBarChart[0].values.length > 8)
               .showValues(true)
               .duration(250)
               ;
               
               d3.select('#chart1 svg')
               .datum(historicalBarChart)
               .call(chart);
               
               //nv.utils.windowResize(chart.update);
               return chart;
               });
                   
               ////$("#status").fadeOut();
               ////$("#preloader").fadeOut();
               
               }
               });
                    
               var filter=$('#ticket_insight').val();
               var comprehensive= new Array();
               var labour = new Array();
               var demand= new Array();
               var part_only= new Array();
               var warranty = new Array();
               var product = new Array();
               var total = new Array();
               //  //$("#status").fadeIn();
               ////$("#preloader").fadeIn();
            /*   $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/ticket_insight",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                    //$('#ticket_insight_graph').empty();
               for(i=0;i<data.length;i++)
               {
                    console.log(data[i]);
               product.push(data[i]['product']);
               comprehensive.push(parseInt(data[i]['Comprehensive Support']));
               labour.push(parseInt(data[i]['Labour Support']));
               demand.push(parseInt(data[i]['On-Demand']));
               part_only.push(parseInt(data[i]['Part_only']));
               warranty.push(parseInt(data[i]['Warranty Support']));
               total.push(parseInt(data[i]['Total']));
               }
             chart5=        Highcharts.chart('ticket_insight_graph', {
               chart: {
               backgroundColor: (Highcharts.theme && Highcharts.theme.chart) || '#f1f4f7'
               },
               title: {
               text: ' '
               },
               xAxis: {
               categories: product
               },yAxis: [{
               title: {
                text: 'No. of Tickets'
               }
               }],
               labels: {
               items: [{
               html: '',
               style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
               }
               }]
               },
               series: [{
               type: 'column',
               name: 'Comprehensive Support',
               data: comprehensive
               }, {
               type: 'column',
               name: 'Labour Support',
               data: labour
               }, {
               type: 'column',
               name: 'On Demand',
               data: demand
               }, {
               type: 'column',
               name: 'Part Only',
               data: part_only
               },  {
               type: 'column',
               name: 'Warranty Support',
               data: warranty
               }, {
               type: 'spline',
               name: 'Total',
               data: total,
               marker: {
               lineWidth: 2,
               lineColor: Highcharts.getOptions().colors[3],
               fillColor: 'white'
               }
               }, {
               type: 'pie',
               name: 'Total consumption',
               
               center: [100, 80],
               size: 100,
               showInLegend: false,
               dataLabels: {
               enabled: false
               }
               }]
               });
               
                 
               ////$("#status").fadeOut();
               ////$("#preloader").fadeOut(); 
               }
               });*/
               
               
               var filter=$('#revenue_insight').val();
               var product2= new Array();
               var contract= new Array();
               var amc= new Array();
               var call= new Array();
               var spare= new Array();
               //   //$("#status").fadeIn();
               ////$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');

               $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/revenue_insight",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                     //$('#Searching_Modal').modal('hide');

                    $('#revenue_insight_graph').empty();
                    console.log(data);
                    for(i=0;i<data.length;i++)
               {
                    console.log(data[i]);
               product2.push(data[i]['Product']);
               contract.push(parseInt(data[i]['Contract Renewal']));
               amc.push(parseInt(data[i]['New AMC']));
               call.push(parseInt(data[i]['Per Call']));
               spare.push(parseInt(data[i]['Spare Sale']));
               }
               Highcharts.chart('revenue_insight_graph', {
               chart: {
               backgroundColor: (Highcharts.theme && Highcharts.theme.chart) || '#f1f4f7'
               },
               title: {
               text: ' '
               },
               xAxis: {
               categories: product2
               },
               labels: {
               items: [{
               html: '',
               style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
               }
               }]
               },
               series: [{
               type: 'column',
               name: 'New AMC',
               data: amc
               }, {
               type: 'column',
               name: 'Per Call',
               data: call
               }, {
               type: 'column',
               name: 'Contract Renewal',
               data: contract
               }, {
               type: 'column',
               name: 'Spare Sale',
               data: spare
               }]
               });
                     
               ////$("#status").fadeOut();
               ////$("#preloader").fadeOut();
               }
               });
               
               var filter=$('#spare_insight').val();
               var amc1= new Array();
               var warranty1= new Array();
               var product1= new Array();
               //    //$("#status").fadeIn();
               ////$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');

               $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/spare_insight",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                     //$('#Searching_Modal').modal('hide');

                    $('#spare_insight_graph').empty();
                    console.log(data);
                    for(i=0;i<data.length;i++)
               {
                    console.log(data[i]);
               product1.push(data[i]['Product']);
               amc1.push(parseInt(data[i]['AMC Support']));
               warranty1.push(parseInt(data[i]['Warranty Support']));
               }
               Highcharts.chart('spare_insight_graph', {
               chart: {
               type: 'area',
               spacingBottom: 30,
               backgroundColor: (Highcharts.theme && Highcharts.theme.chart) || '#f1f4f7'
               },
               title: {
               text: ''
               },
               subtitle: {
               text: '',
               floating: true,
               align: 'right',
               verticalAlign: 'bottom',
               y: 15
               },
               legend: {
               layout: 'vertical',
               align: 'left',
               verticalAlign: 'top',
               x: 80,
               y: 10,
               floating: true,
               borderWidth: 1,
               backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
               },
               xAxis: {
               categories: product1
               },
               yAxis: {
               title: {
               text: 'No. of Spares'
               },
               labels: {
               formatter: function () {
                return this.value;
               }
               }
               },
               tooltip: {
               formatter: function () {
               return '<b>' + this.series.name + '</b><br/>' +
                this.x + ': ' + this.y;
               }
               },
               plotOptions: {
               area: {
               fillOpacity: 0.5
               }
               },
               credits: {
               enabled: false
               },
               series: [{
               name: 'AMC Support',
               data: amc1
               }, {
               name: 'Warranty Support',
               data: warranty1
               }]
               });
               
               // //$("#status").fadeOut();
               // //$("#preloader").fadeOut();
               }
               });
               var filter=$('#csat_insight').val();
               var product_id=$('#product_id').val();
               var label= new Array();
               var actual= new Array();
               var target= new Array();
 
               //     //$("#status").fadeIn();
               ////$("#preloader").fadeIn();
             
               
               //$("#status").fadeOut();
               //$("#preloader").fadeOut();

 });
              // });


   

     // new 

 function current_tickets(){

var company_id="<?php echo $company_id;?>";
var region="<?php echo $region;?>";
var area="<?php echo $area;?>";
var location="<?php echo $location;?>";
var filter=$('#current_tickets').val();
var fresh_result;
var carry_result;
var chart10="";
//$("#status").fadeIn();
//$("#preloader").fadeIn();
$('#Searching_Modal').modal('show');
$.ajax({
url: "<?php echo base_url();?>" + "index.php?/controller_service/tickets_count",
type: 'POST',
data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
dataType: "json",
success: function(data) {
   $('#Searching_Modal').modal('hide');
  console.log(data);
  $('#morris-bar-total').empty();
  $('#morris-bar-carry').empty();
  var total=parseInt(data[2].assigned) + parseInt(data[3].accepted)+parseInt(data[5].escalated)+parseInt(data[6].spare_requested)+parseInt(data[7].work_in_progress)+ parseInt(data[8].unassigned)+ parseInt(data[9].deferred);
if(total<10)
{
total='0'+total;
}
  $('#total_tic').html(total);

  var fresh=parseInt(data[2].assigned) + parseInt(data[3].accepted)+ parseInt(data[8].unassigned)+ parseInt(data[9].deferred);
if(fresh<10)
{
fresh='0'+fresh;
}
  var carry=parseInt(data[5].escalated) + parseInt(data[6].spare_requested)+ parseInt(data[7].work_in_progress);
  if(carry<10)
{
carry='0'+carry;
}
$('#fresh_tic').html(fresh);
  $('#carry_tic').html(carry);
  fresh_result=[
    { name: 'Assigned', y:  data[2].assigned},
    { name: 'Accepted', y:  data[3].accepted},
    { name: 'Unassigned', y:  data[8].unassigned},
    { name: 'Deferred Support', y:  data[9].deferred}
    ];
    carry_result=[
    { name: 'Escalated', y:  data[5].escalated},
    { name: 'Spare Requested', y:  data[6].spare_requested},
    { name: 'In progress', y:  data[7].work_in_progress}
    ];
  if(fresh==0)
{
$('#morris-bar-total').html('<div style="height: 260px; margin: 45% 13% !important; position: relative;"><h3>No Data Available</h3></div>');
}
else{
chart10 = Highcharts.chart('morris-bar-total', {
chart: {
plotBackgroundColor: null,
plotBorderWidth: null,
plotShadow: false,
backgroundColor: null,
type: 'pie'
},
title: {
text: ''
},
plotOptions: {
pie: {
allowPointSelect: true,
cursor: 'pointer',
dataLabels: {
  enabled: false
},
showInLegend: true
}
},"colors":["#337ab7","#d93d5e","#45b6af","#754DEB"],
series: [{
name: 'Tickets',
colorByPoint: true,
data: fresh_result
}]
});

}if(carry==0)
{
$('#morris-bar-carry').html('<div style="height: 260px; margin: 45% 13% !important; position: relative;"><h3>No Data Available</h3></div>');
}
else{
var chart = Highcharts.chart('morris-bar-carry', {
chart: {
plotBackgroundColor: null,
plotBorderWidth: null,
plotShadow: false,
backgroundColor: null,
type: 'pie'
},
title: {
text: ''
},
plotOptions: {
pie: {
allowPointSelect: true,
cursor: 'pointer',
dataLabels: {
  enabled: false
},
showInLegend: true
}
},"colors":["#337ab7","#d93d5e","#45b6af","#754DEB"],
series: [{
name: 'Tickets',
colorByPoint: true,
data: carry_result
}]
});


  
} 
}
});

$("#graph1print").on('click', function (event) {

chart10.print();

});

$("#graph1export").on('click', function (event) {
chart10.exportChart({
type: "image/jpeg"
});
});

} //end function



               
               function exampleData(data) {
               return  data;
               }
               function call_distribution()
                {
                var company_id="<?php echo $company_id;?>";
               var filter=$('#call_distribution').val();
               var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="<?php echo $location;?>";
               var result= new Array();
               var res= new Array();
         //$("#status").fadeIn();
               //$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');
               $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/ticket_distribution",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                     $('#Searching_Modal').modal('hide');
                    //$('#chart1').empty();
                    //$('#chart1').html('');
                    res=new Array(data[0]);var result= new Array();
                    $.each(res[0], function(key, value) {
                      result.push("{'label': '"+key+"','value': '"+value+"'}");
                    });
                    result=JSON.stringify(result);
                    result = result.replace(/\"/g,'');
                    result = result.replace(/\\/g,'');
                    result = result.replace(/\ /g,'');
                    result = result.replace(/\'/g,'"');
                    result=JSON.parse(result);
                    console.log(result);
                  //    $('#chart1').empty();
               historicalBarChart = [
               {
               key: "No. of Tickets",
               values:result,
               "export": {
               "enabled": true
               }
               }
               ];
               
               nv.addGraph(function() {
               var chart = nv.models.discreteBarChart()
               .x(function(d) { return d.label })
               .y(function(d) { return d.value }).valueFormat(d3.format('d'))
               .staggerLabels(true)
               //.staggerLabels(historicalBarChart[0].values.length > 8)
               .showValues(true)
               .duration(250)
               ;
               
               d3.select('#chart1 svg')
               .datum(historicalBarChart)
               .call(chart);
               
               nv.utils.windowResize(chart.update);
               return chart;
               });
               /* var chart = AmCharts.makeChart("chart1", {
               "theme": "light",
               "type": "serial",
               "startDuration": 2,
               "dataProvider":result,
               "depth3D": 20,
               "angle": 30,
               "chartCursor": {
               // "categoryBalloonEnabled": false,
               "cursorAlpha": 0,
               "zoomable": false
               },
               "graphs": [{
               "balloonText": "[[category]]: <b>[[value]]</b>",
               "colorField": "color",
               "fillAlphas": 0.85,
               "lineAlpha": 0.1,
               "type": "column",
               "valueField": "a"
               }],
               "valueAxes": [{
               "gridColor": "#FFFFFF",
               "gridAlpha": 0.2,
               "dashLength": 0,"title":"No. of Tickets"
               }],
               "categoryField": "y",
               "categoryAxis": {
               "gridPosition": "start",
               "axisAlpha":0,
               "gridAlpha":0
               
               },
               "export": {
               "enabled": true
               }
               
               }, 0);*/
               
               /*  var chart = AmCharts.makeChart("chart1", {
               "theme": "light",
               "type": "serial",
               "startDuration": 2,
               "dataProvider":result,
               "depth3D": 40,
               "angle": 30,
               "chartCursor": {
               // "categoryBalloonEnabled": false,
               "cursorAlpha": 0,
               "zoomable": false
               },
               "graphs": [{
               "balloonText": "[[category]]: <b>[[value]]</b>",
               "colorField": "color",
               "fillAlphas": 0.85,
               "lineAlpha": 0.1,
               "type": "column",
               "topRadius":1,
               "valueField": "a"
               }],
               "categoryField": "y",
               "categoryAxis": {
               "gridPosition": "start",
               "axisAlpha":0,
               "gridAlpha":0
               
               },
               "export": {
               "enabled": true
               }
               
               }, 0); */
                  }
               });
               //$("#status").fadeOut();
               //$("#preloader").fadeOut();
                }
               function ticket_insight()
                {
                  var company_id="<?php echo $company_id;?>";
                  var filter=$('#ticket_insight').val();
          var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="<?php echo $location;?>";
               var AMC = new Array();
               var CTPLAMC = new Array();
               var PostWarranty= new Array();
               var Warranty= new Array();
               var Carepack = new Array();
               var product = new Array();
               var total = new Array();
         //$("#status").fadeIn();
               //$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');
               $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/ticket_insight",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                     $('#Searching_Modal').modal('hide');
                    //$('#ticket_insight_graph').empty();
               for(i=0;i<data.length;i++)
               {
                    console.log(data[i]);
               product.push(data[i]['product']);
               AMC.push(parseInt(data[i]['AMC']));
               CTPLAMC.push(parseInt(data[i]['CTPL AMC']));
               PostWarranty.push(parseInt(data[i]['Post Warranty']));
               Warranty.push(parseInt(data[i]['Warranty']));
               Carepack.push(parseInt(data[i]['Carepack']));
               total.push(parseInt(data[i]['Total']));
               }
                chart5=   Highcharts.chart('ticket_insight_graph', {
               chart: {
               backgroundColor: (Highcharts.theme && Highcharts.theme.chart) || '#f1f4f7'
               },
               title: {
               text: ' '
               },
               xAxis: {
               categories: product
               },yAxis: [{
               title: {
                text: 'No. of Tickets'
               }
               }],
               labels: {
               items: [{
               html: '',
               style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
               }
               }]
               },
               series: [{
               type: 'column',
               name: 'AMC',
               data: AMC
               }, {
               type: 'column',
               name: 'CTPL AMC',
               data: CTPLAMC
               }, {
               type: 'column',
               name: 'Post Warranty',
               data: PostWarranty
               }, {
               type: 'column',
               name: 'Warranty',
               data: Warranty
               },  {
               type: 'column',
               name: 'Carepack',
               data: Carepack
               }, {
               type: 'spline',
               name: 'Total',
               data: total,
               marker: {
               lineWidth: 2,
               lineColor: Highcharts.getOptions().colors[3],
               fillColor: 'white'
               }
               }, {
               type: 'pie',
               name: 'Total consumption',
               
               center: [100, 80],
               size: 100,
               showInLegend: false,
               dataLabels: {
               enabled: false
               }
               }]
               });
               
                  }
               });
         //$("#status").fadeOut();
               //$("#preloader").fadeOut();
                }
                ticket_insight();
                 $("#graph3print").on('click', function (event) {
                //  alert("hello");
        chart5.print();

    });
                
                   $("#graph3export").on('click', function (event) {
                //  alert("hello");
        chart5.exportChart({
            type: "image/jpeg"
        });
    });
                function revenue_insight()
                {
                  var company_id="<?php echo $company_id;?>";
                var filter=$('#revenue_insight').val();
        var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="<?php echo $location;?>";
               var product= new Array();
               var contract= new Array();
               var amc= new Array();
               var call= new Array();
               var spare= new Array();
         //$("#status").fadeIn();
               //$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');
               $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/revenue_insight",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                     $('#Searching_Modal').modal('hide');
                    $('#revenue_insight_graph').empty();
                    console.log(data);
                    for(i=0;i<data.length;i++)
               {
                    console.log(data[i]);
               product.push(data[i]['Product']);
               contract.push(parseInt(data[i]['Contract Renewal']));
               amc.push(parseInt(data[i]['New AMC']));
               call.push(parseInt(data[i]['Per Call']));
               spare.push(parseInt(data[i]['Spare Sale']));
               }
               Highcharts.chart('revenue_insight_graph', {
               chart: {
               backgroundColor: (Highcharts.theme && Highcharts.theme.chart) || '#f1f4f7'
               },
               title: {
               text: ' '
               },
               xAxis: {
               categories: product
               },
               labels: {
               items: [{
               html: '',
               style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
               }
               }]
               },
               series: [{
               type: 'column',
               name: 'New AMC',
               data: amc
               }, {
               type: 'column',
               name: 'Per Call',
               data: call
               }, {
               type: 'column',
               name: 'Contract Renewal',
               data: contract
               }, {
               type: 'column',
               name: 'Spare Sale',
               data: spare
               }]
               });
                  }
               });
         //$("#status").fadeOut();
               //$("#preloader").fadeOut();
                }
                function spare_insight()
                {
                  var company_id="<?php echo $company_id;?>";
                var filter=$('#spare_insight').val();
        var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="<?php echo $location;?>";
               var amc1= new Array();
               var warranty1= new Array();
               var product1= new Array();
         //$("#status").fadeIn();
               //$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');
               $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/spare_insight",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                     $('#Searching_Modal').modal('hide');
                    $('#spare_insight_graph').empty();
                    console.log(data);
                    for(i=0;i<data.length;i++)
               {
                    console.log(data[i]);
               product1.push(data[i]['Product']);
               amc1.push(parseInt(data[i]['AMC Support']));
               warranty1.push(parseInt(data[i]['Warranty Support']));
               }
               Highcharts.chart('spare_insight_graph', {
               chart: {
               type: 'area',
               spacingBottom: 30,
               backgroundColor: (Highcharts.theme && Highcharts.theme.chart) || '#f1f4f7'
               },
               title: {
               text: ''
               },
               subtitle: {
               text: '',
               floating: true,
               align: 'right',
               verticalAlign: 'bottom',
               y: 15
               },
               legend: {
               layout: 'vertical',
               align: 'left',
               verticalAlign: 'top',
               x: 80,
               y: 10,
               floating: true,
               borderWidth: 1,
               backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
               },
               xAxis: {
               categories: product1
               },
               yAxis: {
               title: {
               text: 'No. of Spares'
               },
               labels: {
               formatter: function () {
                return this.value;
               }
               }
               },
               tooltip: {
               formatter: function () {
               return '<b>' + this.series.name + '</b><br/>' +
                this.x + ': ' + this.y;
               }
               },
               plotOptions: {
               area: {
               fillOpacity: 0.5
               }
               },
               credits: {
               enabled: false
               },
               series: [{
               name: 'AMC Support',
               data: amc1
               }, {
               name: 'Warranty Support',
               data: warranty1
               }]
               });
                  }
               });
         //$("#status").fadeOut();
               //$("#preloader").fadeOut();
               }
               var chart8="";
               function csat_insight()
                {
                  var company_id="<?php echo $company_id;?>";
                var filter=$('#csat_insight').val();var product_id=$('#product_id').val();
        var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="<?php echo $location;?>";
               var label= new Array();
               var actual= new Array();
               var target= new Array();
         //$("#status").fadeIn();
               //$("#preloader").fadeIn();
               $('#Searching_Modal').modal('show');
         $.ajax({
                  url: "<?php echo base_url();?>" + "index.php?/controller_service/csat_insight",
                  type: 'POST',
                  data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region,'area':area,'location':location},
                  dataType: "json",
                  success: function(data) {
                     $('#Searching_Modal').modal('hide');
                    $('#csat_insight_graph').empty();
                    $('#morris-rat-total').empty();
               console.log(data);
                    if(data.length==0)
               {
               $('#csat_insight_graph').html('<div style="width: 32%; margin: 0px auto; padding-top:15%;"><h3>No Data Available</h3></div>');
               }
               else
               {
               for(i=0;i<data.length;i++)
               {
               if(i!=1)
               {
               
               label.push(data[i].label);
               actual.push(data[i].value);
               target.push(data[i].value1);
               }
               }
              chart8= Highcharts.chart('csat_insight_graph', {
               chart: {
               type: 'bar',
               width: 250,
               backgroundColor: (Highcharts.theme && Highcharts.theme.chart) || null
               },
               title: {
               text: ''
               },
               subtitle: {
               text: ' '
               },
               xAxis: {
               categories:label,
               title: {
               text: null
               }
               },
               yAxis: {
               min: 0,
               title: {
               text: 'Percentage',
               textAlign: 'right'
               },
               labels: {
               overflow: 'justify'
               }
               },
               tooltip: {
               valueSuffix: ' %'
               },
               plotOptions: {
               bar: {
               dataLabels: {
                enabled: true
               }
               }
               },/* 
               legend: {
               layout: 'vertical',
               align: 'right',
               verticalAlign: 'top',
               x: -200,
               y: 250,
               floating: true,
               borderWidth: 1,
               backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#f1f4f7'),
               shadow: true
               }, */
               credits: {
               enabled: false
               },"colors":["#337ab7","#d93d5e"],
               series: [{
               name: 'Actual',showInLegend: false,
               data: actual
               }, {
               name: 'Target',showInLegend: false,
               data: target
               }]
               });
               data_rat=data[1];
               fresh_result=[
                      { name: 'Actual', y:  data_rat['value'],},
                      { name: 'Target', y:  data_rat['value1']}
                      ];   
               console.log(fresh_result);      
               var chart = Highcharts.chart('morris-rat-total', {
               chart: {
               plotBackgroundColor: null,
               plotBorderWidth: null,
               plotShadow: false,
               backgroundColor: null,
               type: 'pie'
               },
               title: {
               text: ''
               },
               plotOptions: {
               pie: {
                allowPointSelect: true,
               center: [80, 100],
               size: 150,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
               }
               },"colors":["#337ab7","#d93d5e"],
               series: [{
               name: 'Customer Rating',
               colorByPoint: true,
               data: fresh_result
               }]
               });
               }
               
                  }
               });
       
               }
               csat_insight();
               $("#graph4export").on('click', function (event) {
  //alert("hello");
        chart8.exportChart({
            type: "image/jpeg"
        });
    });
 $("#graph4print").on('click', function (event) {

        chart8.print();

    });
 
            </script>
        </body>
      </html>