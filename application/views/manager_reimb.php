<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<?php
 		$company_id= $this->session->userdata('companyid');
         include 'assets/lib/cssscript.php'?>
	          <style>
			.bt-buttons{
				display:none !important;
			}
			.dataTables_filter{
				    text-align: right;
			  }
				.vertical-align-center {
    /* To center vertically */
    display: table-cell;
    vertical-align: middle;
}
.modal-dialog-loader {
    /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
    width:inherit;
    height:inherit;
    /* To center horizontally */
    margin: 0 auto;
}
	#mytable_length label {
    float: left;
}
	</style>	 
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
    <!-- BEGIN CONTAINER -->
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include "assets/lib/manager_header.php"?>
        <!-- END HEADER -->
		<div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box dark">
                                <div class="portlet-title">
<div class="caption">Reimbursement </div>
                                        <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_actions_pending" data-toggle="tab">New Claims</a>
                                                </li>
                                                <li>
                                                    <a href="#Accepted" data-toggle="tab">Approved Claims</a>
                                                </li>
                                                <li>
                                                    <a href="#Rejected" data-toggle="tab">Rejected Claims</a>
                                                </li>
                                            </ul>
                                </div>
                                <div class="portlet-body">
                                    <div class="portlet light bordered">
                                    <div class="tab-content">

                                        <div class="tab-pane active" id="tab_actions_pending">
                                            <div class="table=responsive">

                                                <table class="table table-hover table-bordered sample_2">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center">Technician ID</th>
                                                            <th style="text-align:center">Technician Name</th>
                                                            <th style="text-align:center">Start Date</th>
                                                            <th style="text-align:center">End Date</th>
                                                            <th style="text-align:center">Claim Amount</th>
                                                            <th style="text-align:center">View Form</th>
                                                            <th style="text-align:center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($record as $row){
														?>
                                                        <tr>
  <td style="text-align:center" id="<?php echo $row['employee_id']; ?>" onClick="hover_tech(this.id,'<?php echo $row['first_name']; ?>','<?php echo $row['skill_level']; ?>','<?php echo $row['contact_number']; ?>','<?php echo $row['location']; ?>','<?php echo $row['product_name']; ?>','<?php echo $row['cat_name']; ?>')">
                                                                <a>
                                                                    <?php echo $row['employee_id']; ?>
                                                                </a>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row['first_name']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row['start_date']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row['end_date']; ?>
                                                            </td>
															<td style="text-align:center">
                                                                <?php echo $row['total_charge']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                               <span>
																<button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row['technician_id']; ?>" onClick="viewimage(this.id,'<?php echo $row[ 'employee_id'];?>','<?php echo $row[ 'first_name'];?>','<?php echo $row[ 'start_date'];?>','<?php echo $row[ 'end_date'];?>')"><i class="fa fa-eye"></i></button> 
																</span>
                                                            </td>
                                                            <td style="text-align:center">
                                                               <span class="actions">
                                       <button class="btn btn-circle green btn-outline btn-icon-only" id="<?php echo $row['technician_id']; ?>" onClick="accepts(this.id,'<?php echo $row[ 'start_date'];?>','<?php echo $row[ 'end_date'];?>')"><i class="fa fa-check" aria-hidden="true"></i></button>
                                       <button class="btn btn-circle red btn-outline btn-icon-only" id="<?php echo $row['technician_id']; ?>" onClick="Rejects(this.id,'<?php echo $row[ 'start_date'];?>','<?php echo $row[ 'end_date'];?>')"><i class="fa fa-times" aria-hidden="true"></i></button>
                                       </span>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                        <div class="tab-pane fade" id="Accepted">
                                            <div class="table=responsive">

                                                <table class="table table-hover table-bordered sample_2">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center">Technician ID</th>
                                                            <th style="text-align:center">Technician Name</th>
                                                            <th style="text-align:center">Start Date</th>
                                                            <th style="text-align:center">End Date</th>
                                                            <th style="text-align:center">View Form</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($accepted as $row)  {  ?>
                                                        <tr>
  <td style="text-align:center" id="<?php echo $row['employee_id']; ?>" onClick="hover_tech(this.id,'<?php echo $row['first_name']; ?>','<?php echo $row['skill_level']; ?>','<?php echo $row['contact_number']; ?>','<?php echo $row['location']; ?>','<?php echo $row['product_name']; ?>','<?php echo $row['cat_name']; ?>')">
                                                                <a>
                                                                    <?php echo $row[ 'employee_id']; ?>
                                                                </a>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'first_name']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'start_date']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'end_date']; ?>
                                                            </td>
															
                                                            <td style="text-align:center">
                                                               <span>
																<button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row['technician_id']; ?>" onClick="viewimage(this.id,'<?php echo $row[ 'employee_id'];?>','<?php echo $row[ 'first_name'];?>','<?php echo $row[ 'start_date'];?>','<?php echo $row[ 'end_date'];?>')"><i class="fa fa-eye"></i></button> 
																</span>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                        <div class="tab-pane fade" id="Rejected">
                                            <div class="table=responsive">

                                                <table class="table table-hover table-bordered sample_2">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center">Technician ID</th>
                                                            <th style="text-align:center">Technician Name</th>
                                                            <th style="text-align:center">Start Date</th>
                                                            <th style="text-align:center">End Date</th>
                                                            <th style="text-align:center">View Form</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($rejected as $row) { ?>
                                                        <tr>
                                                            <td style="text-align:center" id="<?php echo $row['employee_id']; ?>" onClick="hover_tech(this.id,'<?php echo $row['first_name']; ?>','<?php echo $row['skill_level']; ?>','<?php echo $row['contact_number']; ?>','<?php echo $row['location']; ?>','<?php echo $row['product_name']; ?>','<?php echo $row['cat_name']; ?>')">
                                                                <a>
                                                                    <?php echo $row[ 'employee_id']; ?>
                                                                </a>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'first_name']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'start_date']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'end_date']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                              <span>
																<button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row['technician_id']; ?>" onClick="viewimage(this.id,'<?php echo $row[ 'employee_id'];?>','<?php echo $row[ 'first_name'];?>','<?php echo $row[ 'start_date'];?>','<?php echo $row[ 'end_date'];?>')"><i class="fa fa-eye"></i></button> 
																</span>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
			 <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
    </div>
      <div class="modal" id="Searching_Modal" style="left: 40%;background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Logo"></p>
    </div>
            </div>
  </div>
 <div id="myModal1" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">Details</h5>
               </div>
               <div class="modal-body"id='modal_tech'>
                  <form class="form-horizontal" role="form" >
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
               </div>
            </div>
         </div>
      </div>
	<div id="myModal2" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">Attachement</h5>
               </div>
               <div class="modal-body"id='bill_image'>
                  <form class="form-horizontal" role="form" >
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" onClick="click_modal();">OK</button>
               </div>
            </div>
         </div>
      </div>
     
	   <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Details</h5>
								
                            </div>
                            <div class="modal-body" id='modal_display' style="text-align:center !important">
							<form class="form-horizontal" id="edit_technician"><div class="form-group  col-md-6 col-lg-6 col-sm-12 col-xs-12">
							 <label class=" control-label col-md-5" for="">Employee ID</label>
							<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12"><input type="text" class="form-control" read-only  id="modal_em_id"/>
							 </div>
							 </div>
							<div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
							 <label class="control-label col-md-5" for="">Technician Name</label>
							 <div class=" col-md-6 col-lg-6 col-sm-12 col-xs-12">
							 <input type="text" class="form-control"  read-only id="modal_em_name"/>
							 </div>
							 </div>
							<!--<div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<label class="control-label col-sm-4" for="">Period</label>
							 </div>-->
							<div class="form-group  col-md-6 col-lg-6 col-sm-12 col-xs-12">
							 <label class="control-label col-md-5" for="">The Report From</label>
							 <div class=" col-md-6 col-lg-6 col-sm-12 col-xs-12"><input type="text" class="form-control"  read-only  id="modal_from"/>
							 </div>
							 </div>
							<div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
							 <label class="control-label col-md-5" for="">To</label><div class=" col-md-6 col-lg-6 col-sm-12 col-xs-12"> <input type="text"  class="form-control"  read-only id="modal_to"/>
							 </div>
							 </div>
							 </form>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 table-responsive">
							<hr>
<div class="tools"> </div>
                                    <table class="table table-hover" id="mytable">
                                        <thead>
                                            <td>Ticket Id</td>
											<td>Source</td>
											<td>Destination</td>
                                            <td>Mode Of Travel</td>
                                            <td>No of km</td>
                                            <td>Claim(Rs.)</td>
											<td>Attachements</td>
                                        </thead>
                                        <tbody id="spare_details">
                                        </tbody>
                                    </table>
                        
                               
                                </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="modal-footer">
                            <p><strong>Total amount:</strong> &#x20b9;<span id="total"></span> </p>
                                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                               
                            </div>
                        </div>
                    </div>

                </div>

    <!-- END QUICK SIDEBAR -->
    <?php include 'assets/lib/javascript.php'?>
<script>
        		
       		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
        	$('#man_reim').addClass('open');
	
	$(document).ready(function(){
		$('.sample_2').DataTable({"order": []});
	});
		
</script>
    <script>
			//$company_id = "company1";
        var company_id = "<?php echo $company_id;?>";
function click_modal()
{
$('#myModal2').modal('hide');
$('#myModal').modal('show');
}
        function accepts(id,from_date,to) {
			
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/status",
                type: 'POST',
                data: {
                    'id': id,'from':from_date,'to':to
                },
                //dataType: "json",
                success: function(data){
                   if(data=='Something went Wrong')
				   {
					swal("Oops!", data, "Error")
				   }
                   else{
						swal({
							  title: "",
							  text: data,
							  type: "success",
							  showCancelButton: true,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Ok",
							  cancelButtonText: "Cancel",
							  closeOnConfirm: false,
							  closeOnCancel: false
						},
						function(isConfirm) {
							if (isConfirm) {
								swal.close();
								location.reload();
							}
						else{
								swal.close();
							}
						});
					}
				}
            });
        };

        function Rejects(id,from_date,to) {
			swal({
          		  title: "Are you sure?",
          		  text: "You Want to Reject this Claim !",
          		  type: "warning",
          		  showCancelButton: true,
          		  confirmButtonClass: "btn-danger",
          		  confirmButtonText: "Yes, delete",
          		  cancelButtonText: "No, cancel",
          		  closeOnConfirm: false,
          		  closeOnCancel: false
          		},
				function(isConfirm) {
          		  if (isConfirm) {
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/reject",
                type: 'POST',
                data: {
                    'id': id,'from':from_date,'to':to
                },
                //dataType: "json",
                success: function(data) {
							   			if(data == "Something went Wrong"){
                                              	  swal("Cancelled", "Something Went Wrong:)", "error");
                                             }
							   		  else {
										 swal({
											  title: "Alert",
											  text: data,
											  type: "success",
											  showCancelButton: true,
											  confirmButtonClass: "btn-danger",
											  confirmButtonText: "Ok",
											  cancelButtonText: "Cancel",
											  closeOnConfirm: false,
											  closeOnCancel: false
										},
										function(isConfirm) {
											if (isConfirm) {
												swal.close();
												location.reload();
											}
										}); 
									  }
                            }
            });
        }
		 else{
												
                                                                                          swal.close();
											}
          		});    
        }

        function att_day() {
            $('#tbl_accept').empty();
            var filter = $('#att_day').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/reimb_accept",
                type: 'POST',
                data: {
                    'filter': filter
                },
                dataType: "json",
                success: function(data) {
                    $('#tbl_accept').html('');
                    console.log(data);
                    if (data.length < 1) {
                        console.log(data);
                        $('#tbl_accept').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
                    } else {
                        console.log(data);

                        for (i = 0; i < data.length; i++) {
                            $('#tbl_accept').append('<tr><td style="text-align:center" id="' + data[i].employee_id + '" style="cursor: pointer;" onclick=hover_tech(this.id,"' + data[i].first_name + '","' + data[i].skill_level + '","' + data[i].contact_number + '","' + data[i].location + '","' + data[i].product_name + '","' + data[i].cat_name + '");>' + data[i].employee_id + '</td><td style="text-align:center">' + data[i].first_name + '</td><td style="text-align:center">' + data[i].start_time + '</td><td style="text-align:center">' + data[i].end_time + '</td><td style="text-align:center">' + data[i].travelling_charges + '</td><td style="text-align:center"><button class="btn btn-default red-stripe" id="' + data[i].start_time + '","' + data[i].end_time + '" onclick=viewimage;>View</button></td></tr>');
                        }
                    }
                }
            });
        }
function rej_day() {
            $('#tbl_reject').empty();
            var filter = $('#rej_day').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/reimb_reject",
                type: 'POST',
                data: {
                    'filter': filter
                },
                dataType: "json",
                success: function(data) {
                    $('#tbl_reject').html('');
                    console.log(data);
                    if (data.length < 1) {
                        console.log(data);
                        $('#tbl_reject').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
                    } else {
                        console.log(data);

                        for (i = 0; i < data.length; i++) {
                            $('#tbl_reject').append('<tr><td style="text-align:center" id="' + data[i].technician_id + '" style="cursor: pointer;" onclick=hover_tech(this.id,"' + data[i].first_name + '","' + data[i].skill_level + '","' + data[i].contact_number + '","' + data[i].location + '","' + data[i].product_name + '","' + data[i].cat_name + '");>' + data[i].employee_id + '</td><td style="text-align:center">' + data[i].first_name + '</td><td style="text-align:center">' + data[i].start_time + '</td><td style="text-align:center">' + data[i].end_time + '</td><td style="text-align:center">' + data[i].travelling_charges + '</td><td style="text-align:center"><span><button class="btn btn-circle coral btn-outline btn-sm" id="'+data[i].technician_id+'" onclick="viewimage('+data[i].technician_id+','+data[i].employee_id+','+data[i].first_name+','+data[i].start_date+','+data[i].end_date+')">View</button></span></td></tr>');
                        }
                    }
                }
            });
        }
		 function viewimage1(id) {
		//alert("hi");
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/view_image",
                type: 'POST',
                data: {
                    'id': id
                },
                //dataType: "JSON",
                success: function(data) {

                    var data = JSON.parse(data);
                    
						$("#myModal").modal('hide');
                    if (data['view'] == "" || data['view']== null)
					{
                      swal({
											  title: "",
											  text: "No Data",
											  type: "info",
											  showCancelButton: false,
											  confirmButtonClass: "btn-danger",
											  confirmButtonText: "Ok",
											  closeOnConfirm: true,
											  closeOnCancel: false
										},
										function(isConfirm) {
											if (isConfirm) {
												
												$("#myModal").modal('show');
											}
										}); 
                    } 
					else {
						     $("#myModal").modal('hide');
                       		$('#bill_image').empty();
						    $('#bill_image').append('<img src="'+data['view']+'" height="300px" width="95%">');
                                          $('#myModal2').modal('show');
                    }

                }
            });
        }

		function viewimage(id,employee_id,first_name,start_time,end_time){
	          //alert(start_time);
			  $('#Searching_Modal').modal('show');
			$('#mytable').DataTable().destroy();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/view_form",
                type: 'POST',          
                dataType: "json",
				data:{'tech_id':id,'from':start_time,'to':end_time},
                success: function(data) {
                   console.log(data);
					$('#modal_em_id').val(employee_id);
					$('#modal_em_name').val(first_name);
					$('#modal_from').val(start_time);
					$('#modal_to').val(end_time);
                    if (data.length < 1) {
                        
                        //$('#spare_details').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
                    }
					else 
					{
						$('#mytable').DataTable().destroy();
                   $('#spare_details').html('');
                        var sum = 0;
                        $('#total').empty();
						for (i = 0; i < data.length; i++) {
                            $('#spare_details').append('<tr><td style="text-align:center">' +data[i].ticket_id + '</td><td style="text-align:center">' +data[i].source_Address + '</td>	<td style="text-align:center">' +data[i].destin_Address + '</td><td  style="text-align:center">' + data[i].mode_of_travel + '</td><td style="text-align:center">' +data[i].no_of_km_travelled + '</td><td style="text-align:center">' +data[i].travelling_charges + '</td><td style="text-align:center"><button class="btn btn-circle blue btn-outline btn-icon-only" id="'+ data[i].reim_id + '" onclick=viewimage1(this.id);><i class="fa fa-eye"></i></button></td></tr>');
							sum= sum + parseInt( data[i].travelling_charges);
                            }
                            //$('#total').val(sum);	
                            $('#total').append(sum);
                    }
			 $('#mytable').DataTable({"order": []});
       $('#Searching_Modal').modal('hide');
					$('#myModal').modal('show');
                }
            });
        }
/* 
		$('#mytable').DataTable( {
    dom: 'Bfrtip',
    buttons: [
        'pdf',
        'excel',
        'print'
    ]
} ); */
		function hover_tech(tech_id, first_name, Skill_level, contact_number, location, Product_name, cat_name) {

            $('#modal_tech form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Technician ID</label><div class="col-sm-6 control-label">' + tech_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Name</label><div class="col-sm-6 control-label">' + first_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Skill Level</label><div class="col-sm-6 control-label">' + Skill_level + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Contact Number</label><div class="col-sm-6 control-label">' + contact_number + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Location</label><div class="col-sm-6 control-label">' + location + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Product Category </label><div class="col-sm-6 control-label">' + Product_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Sub Category</label><div class="col-sm-6 control-label">' + cat_name + '</div></div>');
            $('#myModal1').modal('show');
        }
    </script>

</body>

</html>