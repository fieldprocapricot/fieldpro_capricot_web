<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php
               $company_id= $this->session->userdata('companyid');;
                     $region=$user['region'];$area=$user['area'];$location=$user['location'];include 'assets/lib/cssscript.php'?>
		 </head>
         <!-- END HEAD -->
           <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/manager_header.php"?>
               <!-- END HEADER -->
              <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="portlet light portlet-fit bordered">
                                 <div class="portlet-title">
                                    <div class="caption">
                                       <i class=" icon-layers font-green"></i>
                                       <span class="caption-subject font-green bold uppercase">Leaderboard</span>
                                    </div>
                                 </div>
                                 <div class="portlet-body">
                                    <div class="mt-element-step">
                                       <div class="row step-default" id="count_rank">
                                          <div class="mt-step-desc">
                                             <br/>
                                          </div>
                                          <?php $r=$result3->result();
                                             if(!empty($r)){?>
                                          <?php
                                             foreach ($result3->result() as $row1) {
                                             ?>
                                          <div class="col-md-3 bg-grey mt-step-col col-md-offset-1" >
                                             <div id="for_image"><img src="<?php echo base_url().$row1->image ?>" style="width:68px; height:68px; border-radius:50%!important" class="mt-step-title font-silver"></div>
                                             <div class="mt-step-title uppercase font-grey-cascade" id="sethuraman"><?php echo $row1->tech_name; ?></div>
                                             <div class="mt-step-content font-grey-cascade" >
                                                <div id="sethuraman_point"><?php echo $row1->tech_reward_point; ?></div>
                                                <?php } ?>
                                             </div>
                                          </div>
                                          <?php } ?>
                                          <?php $r2=$result2->result();
                                             if(!empty($r2)){?>
                                          <?php
                                             foreach ($result2->result() as $row1) {
                                             	?>
                                          <div class="col-md-4 bg-grey mt-step-col active" >
                                             <div id="for_image1"> <img src="<?php echo base_url().$row1->image ?>" style="width:68px; height:68px; border-radius:50%!important" class="mt-step-title font-silver"></div>
                                             <div class="mt-step-title uppercase font-metalic gold-cascade" id="sethu"><?php echo $row1->tech_name; ?></div>
                                             <div class="mt-step-content font-grey-cascade">
                                                <div id="sethu_point"><?php echo $row1->tech_reward_point; ?></div>
                                                <?php }
                                                   ?>
                                             </div>
                                          </div>
                                          <?php } ?>
                                          <?php $r4=$result4->result();
                                             if(!empty($r4)){?>
                                          <?php
                                             foreach ($result4->result() as $row1) {
                                             	?>
                                          <div class="col-md-3 bg-grey mt-step-col " >
                                             <div id="for_image3">  <img src="<?php echo base_url().$row1->image ?>" style="width:68px; height:68px; border-radius:50%!important" class="mt-step-title font-silver"></div>
                                             <div class="mt-step-title uppercase font-grey-cascade"id="sethu15"><?php echo $row1->tech_name; ?></div>
                                             <div class="mt-step-content font-grey-cascade">
                                                <div id="sethu15_point"><?php echo $row1->tech_reward_point; ?></div>
                                                <?php }				?>
                                             </div>
                                          </div>
                                          <?php }
                                             ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-2 col-lg-offset-10 col-sm-6 margin-bottom-10">
                              <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="inver" onChange="day1();" name="role1">
                                 <option value="" selected >All Level</option>
                                 <option value='L1'>Level-1</option>
                                 <option value='L2'>Level-2</option>
                                 <option value='L3'>Level-3</option>
                                 <option value='L4'>Level-4</option>
                              </select>
                        </div>
                       
                        <div class="container-fluid">
                           <div class="table=responsive">
                              <table class="table table-hover table-bordered datatable_1" id="">
                                 <thead>
                                    <tr>
                                       <th class="text-center">Rank</th>
                                       <th class="text-center">Technician ID</th>
                                       <th class="text-center">Technician Name</th>
                                       <th class="text-center">Score</th>
                                       <th class="text-center">Level</th>
                                    </tr>
                                 </thead>
                                 <tbody id="tbdy_escallated" align="center">
                                    <?php
                                       $i=1;
                                        foreach ($escallated->result() as $row1) {
                                        
                                         ?>
                                    <tr>
                                       <td><?php echo $i; ?></td>
                                       <td><?php echo $row1->employee_id; ?></td>
                                       <td><?php echo $row1->first_name; ?></td>
                                       <td><?php echo $row1->tech_reward_point; ?></td>
                                       <td><?php echo $row1->technician_level; ?></td>
                                    </tr>
                                    <?php 
                                       $i++;} ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <!--/div-->
                     </div>
                     <span class="clearfix"></span>
                     <!-- end col
                        </div>
                        </div> -->
                  </div>
               </div>
            <?php include 'assets/lib/footer.php'?>
            </div>
            <?php include 'assets/lib/javascript.php'?>
            <script>
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
               $('#man_leader').addClass('open');
               
            </script>
            <script>window.history.forward();
               var resizefunc = [];
            </script>
            <script>window.history.forward();
               var company_id = "<?php echo $company_id;?>";
               
               $(document).ready(function() {
			   $('#datatable').DataTable({"order": []});
			   $('.datatable_1').DataTable({"order": []});
               });
                  
            </script>
            <script>
               $(document).ready(function() {
                 $('[data-toggle="tooltip"]').tooltip();
                  var company_id="<?php echo $company_id;?>";
               // var region=$('#fill_region').val();
                 var area=$('#area').val();
                 var location=$('#location').val();
               var region1=$('#fill_region').val();
                 var area1=$('#area1').val();
                 var location1=$('#location1').val();
               var region=$('#fill_region').val();
               //	 var level=$('#inver').val();
               var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="";
              //  $.ajax({
              //    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_area_tech",
              //    type: 'POST',
              //    data: {'company_id':company_id,'region':region,'area':area,'location':location},
              //    dataType: "json",
              //    success: function(data) {
              //    $('#area').html(' <option selected value="">All Area</option>');
              //    $('#area1').html(' <option selected value="">All Area</option>');
              //    console.log(data);
              //    for(i=0;i<data.length;i++)
              //    {
              //    	$('#area').append('<option value="'+data[i].area+'">'+data[i].area+'</option>');
              //    	$('#area1').append('<option value="'+data[i].area+'">'+data[i].area+'</option>');
              //    }
              //    }
              //    });
              //  $.ajax({
              //    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_location_tech",
              //    type: 'POST',
              //    data: {'company_id':company_id,'region':region,'area':area,'location':location},
              //    dataType: "json",
              //    success: function(data) {
              //    $('#location').html(' <option selected value="">All Location</option>');
              //    $('#location1').html(' <option selected value="">All Location</option>');
              //    console.log(data);
              //    for(i=0;i<data.length;i++)
              //    {
              //    	$('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
              //    	$('#location1').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
              //    }
              //    }
              //    });


                //  $.ajax({
                //  url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
                //  type: 'POST',
                //  data: {'company_id':company_id,'region':region,'area':area,'location':location},
                //  dataType: "json",
                //  success: function(data) {
                //  $('#product_id').html(' <option selected value="">All Product-Category</option>');
                //  $('#product_id1').html(' <option selected value="">All Product-Category</option>');
                //  console.log(data);
                //  for(i=0;i<data.length;i++)
                //  {
                //  	$('#product_id').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
                //  	$('#product_id1').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
                //  }
                //  }
                //  });


              //  $.ajax({
              //    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_technician",
              //    type: 'POST',
              //    data: {'company_id':company_id,'region':region,'area':area,'location':location},
              //    dataType: "json",
              //    success: function(data) {
              //    $('#tech').html(' <option selected value="">All Technician</option>');
              //    $('#tech1').html(' <option selected value="">All Technician</option>');
              //    console.log(data);
              //    for(i=0;i<data.length;i++)
              //    {
              //    	$('#tech').append('<option value="'+data[i].technician_id+'">'+data[i].first_name+'</option>');
              //    	$('#tech1').append('<option value="'+data[i].technician_id+'">'+data[i].first_name+'</option>');
              //    }
              //    }
              //    });
               
              //  $.ajax({
              //    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
              //    type: 'POST',
              //    data: {'company_id':company_id,'region':region,'area':area,'location':location},
              //    dataType: "json",
              //    success: function(data) {
              //    $('#region').html(' <option selected value="">All Region</option>');
              //    $('#region1').html(' <option selected value="">All Region</option>');
              //    console.log(data);
              //    for(i=0;i<data.length;i++)
              //    {
              //    	$('#region').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
              //    	$('#region1').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
              //    }
              //    }
              //    });
               
               
                 } );
              //    function inver()
              //       {
              //    $('#tbdy_escallated').empty();
              //  $('#sethuraman').empty();
              //  $('#sethu').empty();
              //  $('#sethu15').empty();
              //  $('#sethuraman_point').empty();
              //  $('#sethu_point').empty();
              //  $('#sethu15_point').empty();
              //    var company_id="<?php echo $company_id;?>";
              //    var filter=$('#inver').val();
              //  var region="<?php echo $region;?>";
              //  var area="<?php echo $area;?>";
              //  var location="";
              //       $.ajax({
              //       url: "<?php echo base_url();?>" + "index.php?/controller_manager/leader_stat",
              //       type: 'POST',
              //       data: {'filter':filter,'region':region,'area':area,'location':location},
              //       dataType: "json",
              //       success: function(data) {
              //       $('#tbdy_escallated').html('');
              //       console.log(data);
              //  $('.datatable_1').DataTable().destroy();
              //       if(data.length<1)
              //       {
              //       console.log(data);
              //       $('#tbdy_escallated').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
              //       }
              //       else
              //       {
              //       console.log(data);
              //       //	data=JSON.parse(data);
              //       if (data.length<=0)
              //  {
              //  $('#count_rank').html("");
              //  }
              //       else{
              //  for(i=0;i<data.length;i++)
              //       {	j=i+1;
               
              //       $('#tbdy_escallated').append('<tr><td>'+j+'</td><td>'+data[i].employee_id+'</td><td>'+data[i].first_name+'</td><td>'+data[i].tech_reward_point+'</td></tr>');
              //  if(i==0){
              //       $('#sethu').html(data[i].first_name);
              //  $('#sethu_point').html(data[0].tech_reward_point);
              //  }
              //  else if(i==1){
              //  $('#sethuraman').html(data[i].first_name);
              //  $('#sethuraman_point').html(data[1].tech_reward_point);
              //  }
              //  else if(i==2){
              //  $('#sethu15').html(data[i].first_name);
              //  $('#sethu15_point').html(data[2].tech_reward_point);
              //  }
               
              //  }
              //  $('.datatable_1').DataTable({"order": []});
              //  }
              //       }
              //       }
              //       });
              //       }


              //  function fill_region()
              //       {
              //  $('#tbdy_escallated').empty();
              //  $('#sethuraman').empty();
              //  $('#sethu').empty();
              //  $('#sethu15').empty();
              //  $('#sethuraman_point').empty();
              //  $('#sethu_point').empty();
              //  $('#sethu15_point').empty();
              //    $('#tbdy_escallated').empty();
              //    var company_id="<?php echo $company_id;?>";
              //  var region="<?php echo $region;?>";
              //  var area="<?php echo $area;?>";
              //  var location="";
              //    var filter=$('#fill_region').val();
              //       $.ajax({
              //       url: "<?php echo base_url();?>" + "index.php?/controller_manager/leader_region",
              //       type: 'POST',
              //       data: {'filter':filter,'region':region,'area':area,'location':location},
              //       dataType: "json",
              //       success: function(data) {
              //       $('#tbdy_escallated').html('');
					
              //   $('.datatable_1').DataTable().destroy();
                   
              //       if(data.length<1)
              //       {
                  
              //       $('#tbdy_escallated').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
              //       }
              //       else
              //       {
                    
              //  if (data.length<=0)
              //  {
              //  $('#count_rank').html("");
              //  }
              //       //	data=JSON.parse(data);
              //       for(i=0;i<data.length;i++)
               
              //       {	j=i+1;
              //       $('#tbdy_escallated').append('<tr><td>'+j+'</td><td>'+data[i].employee_id+'</td><td>'+data[i].first_name+'</td><td>'+data[i].tech_reward_point+'</td></tr>');
              //       if(i==0){
              //       $('#sethu').html(data[i].first_name);
              //  $('#sethu_point').html(data[0].tech_reward_point);
              //  }
              //  else if(i==1){
              //  $('#sethuraman').html(data[i].first_name);
              //  $('#sethuraman_point').html(data[1].tech_reward_point);
              //  }
              //  else if(i==2){
              //  $('#sethu15').html(data[i].first_name);
              //  $('#sethu15_point').html(data[2].tech_reward_point);
              //  }
              //       }$('.datatable_1').DataTable({"order": []});
              //       }
              //       }
              //       });
              //       }
               
                 function day1()
                 {
                  
               $('#tbdy_escallated').empty();
               $('#sethuraman').empty();
               $('#sethu').empty();
               $('#sethu15').empty();
               $('#sethuraman_point').empty();
               $('#sethu_point').empty();
               $('#sethu15_point').empty();
               $('#for_image').empty();
               $('#for_image1').empty();
               $('#for_image3').empty();
               $('#tbdy_escallated').empty();
               $('#tbdy_escallated').empty();
               var company_id="<?php echo $company_id;?>";
               var area=$('#area1').val();
               var location=$('#location1').val();
               var region=$('#fill_region').val();
               var level=$('#inver').val();
               var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="";
                     $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_closed",
                    type: 'POST',
                 data: {'company_id':company_id,'level':level,'region':region,'area':area,'location':location},
                 dataType: "json",
                 success: function(data)  {
                //  data=JSON.parse(data);
                  console.log('leaderboard'+data);
                 $('.datatable_1').DataTable().destroy();
                    $('#tbdy_escallated').html('');
                   
                    if(data.length<1)
                    {
                  
                    $('#tbdy_escallated').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
                    }
                    else
                    {
                    
                    //	data=JSON.parse(data);
                    if (data.length<=0)
               {
               $('#count_rank').html("");
               }
                    else{
               for(i=0;i<data.length;i++)
                    {	j=i+1;
               
                    $('#tbdy_escallated').append('<tr><td>'+j+'</td><td>'+data[i].employee_id+'</td><td>'+data[i].first_name+'</td><td>'+data[i].tech_reward_point+'</td><td>'+data[i].technician_level+'</td><</tr>');
               if(i==0){
                    $('#sethu').html(data[i].first_name);
               $('#sethu_point').html(data[0].tech_reward_point);
               $('#for_image1').html('<img src="'+data[0].image+'" style="width:68px; height:68px; border-radius:50%!important" class="mt-step-title font-silver">');
               }
               else if(i==1){
               $('#sethuraman').html(data[i].first_name);
               $('#sethuraman_point').html(data[1].tech_reward_point);
               $('#for_image').html('<img src="'+data[1].image+'" style="width:68px; height:68px; border-radius:50%!important" class="mt-step-title font-silver">');
               }
               else if(i==2){
               $('#sethu15').html(data[i].first_name);
               $('#sethu15_point').html(data[2].tech_reward_point);
               $('#for_image3').html('<img src="'+data[2].image+'" style="width:68px; height:68px; border-radius:50%!important" class="mt-step-title font-silver">');
               }
               
               }$('.datatable_1').DataTable({"order": []});
               }
                    }
                    
                 },
    error: function (jqXhr, textStatus, errorMessage) { // error callback 
      console.log("end");
    }
                 });
               
                 }
               
               function region()
               {
               var company_id="<?php echo $company_id;?>";
               var region=$('#fill_region').val();
               var area=$('#area').val();
               var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="";
               $.ajax({
                 url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_area_tech",
                 type: 'POST',
                 data: {'company_id':company_id,'region':region,'area':area},
                 dataType: "json",
                 success: function(data) {
                 $('#area').html(' <option selected value="">All Area</option>');
                 console.log(data);
                 for(i=0;i<data.length;i++)
                 {
                 	$('#area').append('<option value="'+data[i].area+'">'+data[i].area+'</option>');
                 }
                 }
                 });
               }
               /* function level()
               {
               var company_id="<?php echo $company_id;?>";
               var level=$('#level').val();
               var area=$('#area').val();
               $.ajax({
                 url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_level",
                 type: 'POST',
                 data: {'company_id':company_id,'level':level,'area':area},
                 dataType: "json",
                 success: function(data) {
                 $('#level').html(' <option selected value="">All Area</option>');
                 console.log(data);
                 for(i=0;i<data.length;i++)
                 {
                 	$('#level').append('<option value="'+data[i].skill_level+'">'+data[i].skill_level+'</option>');
                 }
                 }
                 });
               }
               */		 function area()
               {
               var company_id="<?php echo $company_id;?>";
               var region=$('#fill_region').val();
               var area=$('#area').val();
               var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="";
               $.ajax({
                 url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_location_tech",
                 type: 'POST',
                 data: {'company_id':company_id,'region':region,'area':area},
                 dataType: "json",
                 success: function(data) {
                 $('#location').html(' <option selected value="">All Location</option>');
                 console.log(data);
                 for(i=0;i<data.length;i++)
                 {
                 	$('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
                 }
                 }
                 });
               }
               /* function region1()
               {
               var company_id="<?php echo $company_id;?>";
               var region=$('#fill_region').val();
               var area=$('#area1').val();
               $.ajax({
                 url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
                 type: 'POST',
                 data: {'company_id':company_id,'region':region,'area':area},
                 dataType: "json",
                 success: function(data) {
                 $('#area1').html(' <option selected value="">All Area</option>');
                 console.log(data);
                 for(i=0;i<data.length;i++)
                 {
                 	$('#area1').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
                 }
                 }
                 });
               } */
               function area1()
               {
               //alert("hello");
               var company_id="<?php echo $company_id;?>";
               var region=$('#fill_region').val();
               var area=$('#area1').val();
               var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="";
               $.ajax({
                 url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_location_tech",
                 type: 'POST',
                 data: {'company_id':company_id,'region':region,'area':area},
                 dataType: "json",
                 success: function(data) {
                 $('#location1').html(' <option selected value="">All Location</option>');
                 console.log(data);
                 for(i=0;i<data.length;i++)
                 {
                 	$('#location1').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
                 }
                 }
                 });
               }
               function region1()
               {
               var company_id="<?php echo $company_id;?>";
               var region=$('#fill_region').val();
               var area=$('#area1').val();
               var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="";
               $.ajax({
                 url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_area_tech",
                 type: 'POST',
                 data: {'company_id':company_id,'region':region,'area':area},
                 dataType: "json",
                 success: function(data) {
                 $('#area1').html(' <option selected value="">All Area</option>');
                 console.log(data);
                 for(i=0;i<data.length;i++)
                 {
                 	$('#area1').append('<option value="'+data[i].area+'">'+data[i].area+'</option>');
                 }
                 }
                 });
               }
            </script>
         </body>
      </html>