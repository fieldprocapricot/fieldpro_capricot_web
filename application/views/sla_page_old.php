<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php include 'assets/lib/cssscript.php'?>
            <style>
               .jstree-anchor{
               min-height:110px !important;
               }
               .jstree-icon.jstree-themeicon.jstree-themeicon-custom{
               height:100px;
               width:150px;
               }
               .jstree-default .jstree-anchor {
               //line-height: 6 !important;
               margin-bottom:10%;
               margin-top:2%;
               text-align: center;
               }
               .jstree-icon.jstree-themeicon.jstree-themeicon-custom {
               margin: 8px 0px !important;
               }
               .jstree-node{
               //margin:3px 0px;
               }
               #tree_5{
               padding:2%;
               }
               .jstree-children{
               margin:0% 2%;
               }
               .jstree-icon.jstree-themeicon.jstree-themeicon-custom{
               display:block !important;
               }
               .sweet-alert.showSweetAlert.visible{
               z-index: 999999999 !important;
               }
               .jstree-default .jstree-anchor {
               line-height: 1 !important;
               }
               .jstree-checkbox{
               float: left !important;
               }
               .dt-buttons{
               display:none !important;
               }

               .fileinput-new{
               color:#000 !important;
               }
               .portlet-title3 {
               text-align: center;
               background: #32c5d2;
               color: #fff;
               padding-top: 6px !important;
               }
               .close {
               width: 18px;
               height: 18px;
               }
               .portlet.light.bordered {
               border: 1px solid #16aeae !important;
               }
               .selected{
               box-shadow:0px 12px 22px 1px #333;
               }
               .portlet.box.blue-hoki {
               border: 1px solid #869ab3;
               /*border-top: 0;*/
               }
               /*.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover, .nav > li > a:hover {
               background:none;
               }*/
               #frame{
               height: 150px; /* equals max image height */
               width: 160px;
               border: 1px solid #0a1824;
               white-space: nowrap;
               text-align: center;
               margin: 1em 0;
               }
               .helper{
               display: inline-block;
               height: 100%;
               vertical-align: middle;
               }
               #frame img{
               vertical-align: middle;
               max-height: 150px;
               max-width: 160px;
               width:100%;
               height:auto;
               }
               .product-checkbox2{
               margin: 0 4px -22px 0px !important;
               }
               .sla-target-label {
                  padding: 20px 0;
                  text-align: left !important;
               }
               .tab-height{
                  overflow: hidden;
               }
            </style>
            <link href="assets/global/plugins/jquerysctipttop.css" rel="stylesheet" type="text/css" />
         </head>
         <!-- END HEAD -->
         <body class="page-header-fixed page-sidebar-closed-hide-logo">
            <!-- BEGIN CONTAINER -->
            <div class="wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header_superad.php"?>
               <!-- END HEADER -->
               <div class="container-fluid">
                  <div class="page-content">
                     <!-- BEGIN BREADCRUMBS -->
                     <!-- END BREADCRUMBS -->
                     <!-- BEGIN PAGE BASE CONTENT -->
                     <div class="page-content-container">
                        <div class="page-content-row">
                           <div class="page-content-col">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">SLA Mapping</div>
                                    <div class="tools"> </div>
                                 </div>
                                 <div class="portlet-body">
                                    <div class="portlet light bordered">
                                       <div class="portlet-title tabbable-line">
                                          <div class="caption">
                                          </div>
                                          <ul class="nav nav-tabs">
                                             <li class="active">
                                                <a href="#portlet_tab1" data-toggle="tab"> SLA Mapping </a>
                                             </li>
                                             <li>
                                                <a href="#portlet_tab2" data-toggle="tab"> Last Update </a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="portlet-body">
                                          <div class="tab-content">
                                             <div class="tab-pane active" id="portlet_tab1">
                                                <div class="table-toolbar">
                                                   <div class="row pull-right">
                                                      <div class="col-md-6">
                                                         <div class="btn-group">
                                                            <button id="sample_editable_1_new" class="btn btn-circle green-haze btn-outline btn-md" onClick="update_check();">
                                                            <i class="fa fa-level-up"></i> Update SLA
                                                            </button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="container-fluid" id="div_align">
                                                   <div class="col-md-10 col-md-offset-1">
                                                      <div class="portlet box blue-hoki">
                                                         <div class="portlet-title">
                                                            <div class="caption pull-right">
                                                               <h5 style="margin:0"></h5>
                                                            </div>
                                                         </div>
                                                         <div class="portlet-body">
                                                            <div class="row">
                                                               <div class="col-md-4 col-md-offset-8 col-sm-12">
                                                                  <!-- <select class="form-control" id="target">
                                                                     </select> -->
                                                               </div>
                                                            </div>
                                                            <!--<div id="tree_5" class="tree-demo"></div>-->
                                                            <div class="tabbable tabs-left company_div">
                                                               <div class="tab-content tab-height" id="category_append" style="display:none"></div>
                                                               <div class="col-sm-3 parent-left">
                                                                  <ul class="nav nav-tabs product-tab-left " id="company">
                                                                  </ul>
                                                               </div>
                                                               <div class="col-sm-9 tab-sla-page">
                                                                  <div class="tab-content tab-height" id="product_append">
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="tab-pane" id="portlet_tab2">
                                                <div class="btn-group margin-bottom-10 pull-right">
                                                   <select class="form-control" id="target">
                                                      <option selected disabled>Select Company</option>
                                                      <?php
                                                         foreach($company->result() as $row){
                                                         ?>
                                                      <option value="<?php echo $row->company_id ?>"><?php echo $row->company_name ?></option>
                                                      <?php }
                                                         ?>
                                                   </select>
                                                </div>
                                                <div class="table=responsive">
                                                   <table class="table table-hover" id="sample_2">
                                                      <thead>
                                                         <tr>
                                                            <th>Company Name</th>
                                                            <th style="text-align:center">Priority Level</th>
                                                            <th>Product Category</th>
                                                            <th>Sub Category</th>
                                                            <th>Last Update</th>
                                                            <th>Editable Rights</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="sla_views">
                                                         <?php
                                                            foreach ($get_sla->result() as $row) {
                                                                    ?>
                                                         <tr>
<td><?php echo $row->company_name; ?></td>
                                                            <td style="text-align:center"><?php
                                                               if($row->b=="all"){
                                                                  echo "All Priority Levels";
                                                               }
                                                               else{
                                                               echo $row->b;} ?></td>
                                                            <td><?php
                                                               if($row->product=="all"){
                                                                  echo "All Product Categories";
                                                               }
                                                               else{                                        echo $row->product_name;
                                                               }?></td>
                                                            <td><?php
                                                               if($row->category=="all"){
                                                                  echo "All Sub Categories";
                                                                  }
                                                                  else
                                                                  {
                                                               echo $row->cat_name;
                                                                  }                                            ?></td>
                                                            <td><?php echo $row->last_update; ?></td>
                                                            <!--<td><span><?php if( $row->editable_rights=='yes')
                                                               {?><button class="btn btn-circle dark btn-outline btn-sm" id="<?php echo $row->ref_id?>" onclick=edit_ref(this.id,"<?php echo $row->response_time?>","<?php echo $row->resolution_time?>","<?php echo $row->acceptance_time?>","<?php echo $row->SLA_Compliance_Target?>","<?php echo $row->mttr_target?>","<?php echo $row->company_id?>");>Edit</button></span>
                                                               <span><button class="btn btn-circle red btn-outline btn-sm"  onclick=delete_ref("<?php echo $row->ref_id?>","<?php echo $row->company_id?>");>Delete</button></span><?php }
                                                                  else{
                                                                     echo "No Editable Rights";
                                                                     }?>
                                                            </td>-->
                                                            <td>
                                                               <span>
                                                                  <button class="btn btn-circle dark btn-outline btn-sm" id="<?php echo $row->ref_id?>" onclick=edit_ref(this.id,"<?php echo $row->response_time?>","<?php echo $row->resolution_time?>","<?php echo $row->acceptance_time?>","<?php echo $row->SLA_Compliance_Target?>","<?php echo $row->mttr_target?>","<?php echo $row->company_id?>");>Edit</button>
                                                               </span>
                                                               <span>
                                                                  <button class="btn btn-circle red btn-outline btn-sm"  onclick=delete_ref("<?php echo $row->ref_id?>","<?php echo $row->company_id?>");>Delete</button>
                                                               </span>
                                                            </td>
                                                         </tr>
                                                         <?php } ?>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>
                     <!-- END PAGE BASE CONTENT -->
                  </div>
                  <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
               </div>
            </div>
            <!--Modal Starts-->
            <!-- Modal -->
            <div id="myModal2" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Sub Category</h4>
                        <div class="error" style="display:none">
                           <label id="rowdata_category"></label>
                        </div>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" id="add_subproduct">
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">Sub Category Id:</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="subproduct_id" name="subproduct_id" readonly>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">product Name:</label>
                              <div class="col-sm-8">
                                 <select class="form-control" id="product_names" name="product_names">
                                    <option value="" selected disabled>Select Product</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">Sub Category Name:</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="subproduct_name" name="subproduct_name" placeholder="Sub Category Name">
                              </div>
                           </div>
                           <div class="form-group" style="display:none">
                              <label class="control-label col-sm-4" for="email">Sub Category Modal No:</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="subproduct_modal" name="subproduct_modal" placeholder="Sub Category Modal No">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">Sub Category Description:</label>
                              <div class="col-sm-8">
                                 <textarea class="form-control" rows="5" id="subproduct_desc" name="subproduct_desc" style="resize:none"></textarea>
                              </div>
                           </div>
                           <div class="form-group" style="display:none">
                              <label class="control-label col-sm-4" for="email">company id:</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="companyid" name="companyid" value="<?php echo $this->session->userdata('companyid');?>" />
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">Sub Category Image:</label>
                              <div class="col-sm-8">
                                 <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group input-large">
                                       <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                          <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                          <span class="fileinput-filename"> </span>
                                       </div>
                                       <span class="input-group-addon btn default btn-file">
                                       <span class="fileinput-new"> Select file </span>
                                       <span class="fileinput-exists"> Change </span>
                                       <input name="subproduct_image" id="subproduct_image" type="file"> </span>
                                       <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="submit" class="btn btn-circle blue btn-outline" id="addsubproduct"><i class="fa fa-check"></i> Submit</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                     </div>
                  </div>
               </div>
            </div>
         <!-- Modal -->
            <div id="myModal_sla" class="modal fade" role="dialog">
               <div class="modal-dialog" style="width:1200px">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">SLA Mapping</h4>
                        <div class='error'>
                           <p id='row_data'></p>
                        </div>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" id="add_slassss">
                     <div class="form-wizard">
                                    <div class="form-body">
                                     
                                    <div class="col-lg-2">
                              <div class="col-sm-12"><label class="control-label sla-target-label">Priority Level</label></div>
                              <div class="col-sm-12"><label class="control-label sla-target-label">Resolution Time</label></div>
                              <div class="col-sm-12"><label class="control-label sla-target-label">Response Time</label></div>
                              <div class="col-sm-12"><label class="control-label sla-target-label">Acceptance Time</label></div>
                              <div class="col-sm-12"><label class="control-label sla-target-label">MTTR Target</label></div>
                              <div class="col-sm-12"><label class="control-label sla-target-label">SLA Complaince Target (in %)</label></div>
                                    </div>
                                    <div class="col-lg-10">

                                     <div class="col-sm-3">
                            <div class="form-group">
                              
                               <div class="col-lg-12">
                                        <input type="text" class="form-control" value="P1" id='p1' name="p1"  readonly ></input>
                              </div>
                            </div>                           
                            <div class="form-group">
                              
                               <div class="col-lg-6">
                                  <select class="form-control" id="p1_reso_hour_1" name="p1_reso_hour_1">
                                    <option selected value='not_sel'>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p1_reso_minute_1" name="p1_reso_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                              
                               <div class="col-lg-6">
                                  <select class="form-control" id="p1_res_hour_1" name="p1_res_hour_1">
                                    <option selected value='not_sel'>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>   
                               <div class="col-lg-6">
                                  <select class="form-control" id="p1_res_minute_1" name="p1_res_minute_1">
                                    <option value='not_sel' selected >In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">                             
                               <div class="col-lg-6">
                                  <select class="form-control" id="p1_acc_hour_1" name="p1_acc_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p1_acc_minute_1" name="p1_acc_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>
                            <div class="form-group">
                              
                               <div class="col-lg-12">
                                 <select class="form-control" id="p1_sla_target_1" name="p1_sla_target_1">
                                    <option selected value='not_sel'>Sla Target</option>
                                                   <?php 
                                                      for($i=0;$i<=100;$i++){                                                       
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">                             
                               <div class="col-lg-6">
                                  <select class="form-control" id="p1_mttr_target_hour_1" name="p1_mttr_target_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p1_mttr_target_minute_1" name="p1_mttr_target_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                                        </div>
                                      
                            </div>
                                     <div class="col-sm-3">
                            <div class="form-group">
                                  <div class="col-lg-12">
                                        <input type="text" class="form-control" value="P2" id="p2" name="p2"  readonly ></input>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p2_reso_hour_1" name="p2_reso_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p2_reso_minute_1" name="p2_reso_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p2_res_hour_1" name="p2_res_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>   
                               <div class="col-lg-6">
                                  <select class="form-control" id="p2_res_minute_1" name="p2_res_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p2_acc_hour_1" name="p2_acc_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p2_acc_minute_1" name="p2_acc_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>
                            <div class="form-group">
                               <div class="col-lg-12">
                                 <select class="form-control" id="p2_sla_target_1" name="p2_sla_target_1">
                                    <option selected value='not_sel'>Sla Target</option>
                                                   <?php 
                                                      for($i=0;$i<=100;$i++){                                                       
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p2_mttr_target_hour_1" name="p2_mttr_target_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p2_mttr_target_minute_1" name="p2_mttr_target_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                                        </div>
                                      
                            </div>
                                     
                                     <div class="col-sm-3">
                            <div class="form-group">
                               <div class="col-lg-12">
                                        <input type="text" class="form-control" value="P3" id="p3" name="p3" readonly ></input>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p3_reso_hour_1" name="p3_reso_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p3_reso_minute_1" name="p3_reso_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p3_res_hour_1" name="p3_res_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>   
                               <div class="col-lg-6">
                                  <select class="form-control" id="p3_res_minute_1" name="p3_res_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p3_acc_hour_1" name="p3_acc_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p3_acc_minute_1" name="p3_acc_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>
                            <div class="form-group">
                                  <div class="col-lg-12">
                                 <select class="form-control" id="p3_sla_target_1" name="p3_sla_target_1">
                                    <option selected value='not_sel'>Sla Target</option>
                                                   <?php 
                                                      for($i=0;$i<=100;$i++){                                                       
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p3_mttr_target_hour_1" name="p3_mttr_target_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p3_mttr_target_minute_1" name="p3_mttr_target_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                                        </div>
                                      
                            </div>
                                     
                                     <div class="col-sm-3">
                            <div class="form-group">
                               <div class="col-lg-12">
                                        <input type="text" class="form-control" value="P4" id="p4" name="p4" readonly ></input>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p4_reso_hour_1" name="p4_reso_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p4_reso_minute_1" name="p4_reso_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p4_res_hour_1" name="p4_res_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>   
                               <div class="col-lg-6">
                                  <select class="form-control" id="p4_res_minute_1" name="p4_res_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p4_acc_hour_1" name="p4_acc_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p4_acc_minute_1" name="p4_acc_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>
                            <div class="form-group">
                               <div class="col-lg-12">
                                 <select class="form-control" id="p4_sla_target_1" name="p4_sla_target_1">
                                    <option selected value='not_sel'>Sla Target</option>
                                                   <?php 
                                                      for($i=0;$i<=100;$i++){                                                       
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                            </div>                           
                            <div class="form-group">
                               <div class="col-lg-6">
                                  <select class="form-control" id="p4_mttr_target_hour_1" name="p4_mttr_target_hour_1">
                                    <option value='not_sel' selected>In Hour</option>
                                                   <?php 
                                                      for($i=0;$i<=11;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                               <div class="col-lg-6">
                                  <select class="form-control" id="p4_mttr_target_minute_1" name="p4_mttr_target_minute_1">
                                    <option selected value='not_sel'>In Minute</option>
                                                   <?php 
                                                      for($i=0;$i<=59;$i++){                                                        
                                                   ?>
                                                      <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>  
                                                   <?php } ?>
                                             </select>
                              </div>
                                        </div>
                                      
                            </div>
                                     </div>
                            <hr>
                                     <span class="clearfix"></span>
                            <div class="form-group">
                              <label class="control-label col-lg-6">Editable Rights to Admin</label>
                               <div class="col-lg-6">
                                 <label class="radio-inline">
                                    <input type="radio" name="editable_right" value="yes">Yes
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="editable_right" value="no" checked="checked">No
                                  </label>
                              </div>
                            </div>                       
                          </div>
                        </div>
                </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" onClick="check('p1','p1_reso_hour_1','p1_reso_minute_1','p1_res_hour_1','p1_res_minute_1','p1_acc_hour_1','p1_acc_minute_1','p1_sla_target_1','p1_mttr_target_hour_1','p1_mttr_target_minute_1','p2','p2_reso_hour_1','p2_reso_minute_1','p2_res_hour_1','p2_res_minute_1','p2_acc_hour_1','p2_acc_minute_1','p2_sla_target_1','p2_mttr_target_hour_1','p2_mttr_target_minute_1','p3','p3_reso_hour_1','p3_reso_minute_1','p3_res_hour_1','p3_res_minute_1','p3_acc_hour_1','p3_acc_minute_1','p3_sla_target_1','p3_mttr_target_hour_1','p3_mttr_target_minute_1','p4','p4_reso_hour_1','p4_reso_minute_1','p4_res_hour_1','p4_res_minute_1','p4_acc_hour_1','p4_acc_minute_1','p4_sla_target_1','p4_mttr_target_hour_1','p4_mttr_target_minute_1');" >Submit</button>
                     </div>
                  </div>
               </div>
            </div>
            <div id="view_sla" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" id="view_prioritys">
                           <div class="form-body">
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Response Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_time" name="r_time">
                                          <?php
                                             for($i=00;$i<=11;$i++){  if($i<10){
                                    ?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
                                  }                              ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_time_minute" name="reso_minute_1">
                                          <?php
                                             for($i=00;$i<=59;$i++){  if($i<10){
                                    ?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
                                  }                              ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Resolution Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_levels" name="res_hour_1">
                                          <?php
                                             for($i=00;$i<=11;$i++){  if($i<10){
                                    ?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
                                  }                               ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_levels_minute" name="res_minute_1">
                                          <?php
                                             for($i=00;$i<=59;$i++){  if($i<10){
                                    ?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
                                  }                               ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Acceptance Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="a_time" name="acc_hour_1">
                                          <?php
                                             for($i=00;$i<=11;$i++){  if($i<10){
                                    ?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
                                  }                               ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="a_time_minute" name="acc_minute_1">
                                          <?php
                                             for($i=00;$i<=59;$i++){  if($i<10){
                                    ?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
                                  }                               ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">SLA Complaince Target (in %)</label>
                                    <div class="col-lg-8">
                                       <select class="form-control" id="sla_targetss" name="sla_target_1">
                                          <?php
                                             for($i=00;$i<=100;$i++){  if($i<10){
                                    ?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
                                  }                               ?>
                                       </select>
                                       <!-- <input type="text" class="form-control" id="sla_target" name="sla_target" placeholder="Accepts only Digits "></input> -->
                                    </div>
                                 </div>
                                 <span class="clearfix"></span>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">MTTR Target</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="m_targets" name="mttr_target_hour_1">
                                          <?php
                                             for($i=00;$i<=11;$i++){  if($i<10){
                                    ?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
                                  }                               ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="m_targets_minute" name="mttr_target_minute_1">
                                          <?php
                                             for($i=00;$i<=59;$i++){  if($i<10){
                                    ?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
                                  }                               ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" onClick="edit_sla_modal();" >Submit</button><button type="cancel" class="btn red-haze btn-outline btn-circle btn-md" data-dismiss="modal">Cancel</button>
                        <input type="hidden" class="btn btn-circle blue btn-outline btn-sm" id="hidden_ref_id" >
                        <input type="hidden" class="btn btn-circle blue btn-outline btn-sm" id="hidden_com_id" >
                     </div>
                  </div>
               </div>
            </div>

            <div id="sla_confirm" class="modal fade" role="dialog">
               <div class="modal-dialog">
                   <!-- Modal content-->
                   <div class="modal-content">
                       <div class="modal-header">
                       </div>
                       <div class="modal-body">
                           <form class="form-horizontal" id="add_subproduct">
                               <div class="form-group">
                                   <div class="col-sm-12">
                                       <p id="responses"></p>
                                   </div>
                               </div>
                           </form>
                       </div>
                       <div class="modal-footer">
                           <button type="submit" class="btn btn-circle blue btn-outline"  onclick="confirm_sla()"><i class="fa fa-check"></i> OK !</button>
                       </div>
                   </div>
               </div>
            </div>

            <!-- Modal -->
            <!--Modal End-->
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>
            <script>
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
               $('#sla_page').addClass('open');
            </script>   
            <script>
         function edit_ref(ref_id,response_time,resolution_time,acceptance_time,SLA_Compliance_Target,mttr_target,company_id)
               {
               var res_time=response_time.split(":");
               var resol_time=resolution_time.split(":");
               var acc_time=acceptance_time.split(":");
               var mttr_t=mttr_target.split(":");
               $('#r_time').append('<option value="'+res_time[0] +'" selected >'+res_time[0]+'</option>');
               $('#r_time_minute').append('<option value="'+res_time[1] +'" selected >'+res_time[1]+'</option>');
               $('#r_levels').append('<option value="'+resol_time[0] +'" selected >'+resol_time[0]+'</option>');
               $('#r_levels_minute').append('<option value="'+resol_time[1] +'" selected >'+resol_time[1]+'</option>');
               $('#a_time').append('<option value="'+acc_time[0] +'" selected >'+acc_time[0]+'</option>');
               $('#a_time_minute').append('<option value="'+acc_time[1] +'" selected >'+acc_time[1]+'</option>');
               $('#sla_targetss').append('<option value="'+SLA_Compliance_Target +'" selected >'+SLA_Compliance_Target+'</option>');
               $('#m_targets').append('<option value="'+mttr_t[0] +'" selected >'+mttr_t[0]+'</option>');
               $('#m_targets_minute').append('<option value="'+mttr_t[1] +'" selected >'+mttr_t[1]+'</option>');
               $('#hidden_ref_id').val(ref_id);
               $('#hidden_com_id').val(company_id);
               $('#view_sla').modal('show');
            }
         function edit_sla_modal()
               {
               reso_hour_1=$('#r_levels').val();
                   reso_minute_1=$('#r_levels_minute').val();
                   res_hour_1=$('#r_time').val();
                   res_minute_1=$('#r_time_minute').val();
                   acc_hour_1=$('#a_time').val();
                   acc_minute_1=$('#a_time_minute').val();
                   mttr_target_hour_1=$('#m_targets').val();
                   mttr_target_minute_1=$('#m_targets_minute').val();
              /* if($('#r_levels').val()<10)
                                {
                                   reso_hour_1='0'+$('#r_levels').val();
                                }
                                if($('#r_levels_minute').val()<10)
                                {
                                   reso_minute_1='0'+$('#r_levels_minute').val();
                                }
                                if($('#r_time_minute').val()<10)
                                {
                                   res_minute_1='0'+$('#r_time_minute').val();
                                }
                                if($('#r_time').val()<10)
                                {
                                   res_hour_1='0'+$('#r_time').val();
                                }
                                if($('#a_time').val()<10)
                                {
                                   acc_hour_1='0'+$('#a_time').val();
                                }
                                if($('#a_time_minute').val()<10)
                                {
                                   acc_minute_1='0'+$('#a_time_minute').val();
                                }
                                if($('#m_targets').val()<10)
                                {
                                  mttr_target_hour_1='0'+$('#m_targets').val();
                                }
                                if($('#m_targets_minute').val()<10)
                                {
                                   mttr_target_minute_1='0'+$('#m_targets_minute').val();
                                }*/
                                resolution=reso_hour_1+':'+reso_minute_1+':00';
                                response=res_hour_1+':'+res_minute_1+':00';
                                acceptance=acc_hour_1+':'+acc_minute_1+':00';
                                sla_compliance=$('#sla_targetss').val();
                                mttr=mttr_target_hour_1+':'+mttr_target_minute_1+':00';
                                ref_id=$('#hidden_ref_id').val();
                       company_id=$('#hidden_com_id').val();
                   $.ajax({
                  url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_edit_sla",
                  type     :   "POST",
                  data     :   {'company_id':company_id,'ref_id':ref_id,'response':response,'resolution':resolution,'acceptance':acceptance,'sla_compliance':sla_compliance,'mttr':mttr},// {action:'$funky'}
                  datatype :   "JSON",
                  cache    :   false,
                  success  :   function(data)
                  {
                     data=data.trim();
                     $('#view_sla').modal('hide');
                     if(data=="SLA Updated Successfully")
                     {
                        swal({
                       title: data,
                       type: "success",
                       showCancelButton: false,
                       confirmButtonClass: "btn-danger",
                       confirmButtonText: "Ok",
                       cancelButtonText: "No,Cancel",
                       closeOnConfirm: false,
                       closeOnCancel: false
                     },
                     function(isConfirm) {
                       if (isConfirm) {
                        window.location.reload();
                       }
                     });
                     }
                     else
                     {
                        swal({
                       title: data,
                       type: "success",
                       showCancelButton: false,
                       confirmButtonClass: "btn-danger",
                       confirmButtonText: "Ok",
                       cancelButtonText: "No,Cancel",
                       closeOnConfirm: false,
                       closeOnCancel: false
                     },
                     function(isConfirm) {
                       if (isConfirm) {
                          swal.close();
                        $('#view_sla').modal('show');
                       }
                     });
                     }
                  }
            });
            }

            function delete_ref(ref_id,company_id)
               {
                  swal({
                          title: "Are you sure? You want to Delete",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Yes, Delete!",
                          cancelButtonText: "Cancel",
                          closeOnConfirm: false,
                       //   closeOnCancel: false
                        },
                        function(isConfirm){
                     if (isConfirm) {
                     $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_superad/delet_sla",
                        type        :   "POST",
                        data        :   {'company_id':company_id,'ref_id':ref_id},
                        cache       :   false,
                        success     :   function(data)
                        {
                           swal({
                             title: data,
                             type: "success",
                             showCancelButton: false,
                             confirmButtonClass: "btn-danger",
                             confirmButtonText: "Ok",
                             cancelButtonText: "No,Cancel",
                             closeOnConfirm: false,
                             closeOnCancel: false
                           },
                           function(isConfirm){
                              if (isConfirm) {
                              window.location.reload();
                              }
                           });
                        }
                     });
                  }
               });
               }
         function update_check()
         {
            $('#add_slassss')[0].reset();
            for(var j=1;j<=4;j++){
               $('#p'+j+'_reso_hour_1').attr('disabled', false);
               $('#p'+j+'_reso_minute_1').attr('disabled', false);
               $('#p'+j+'_res_hour_1').attr('disabled', false);
               $('#p'+j+'_res_minute_1').attr('disabled', false);
               $('#p'+j+'_acc_hour_1').attr('disabled', false);
               $('#p'+j+'_acc_minute_1').attr('disabled', false);
               $('#p'+j+'_sla_target_1').attr('disabled', false);
               $('#p'+j+'_mttr_target_hour_1').attr('disabled', false);
               $('#p'+j+'_mttr_target_minute_1').attr('disabled', false);
            }  

            var selected = [];
            $('.company_div  input:checked').each(function() {
               selected.push($(this).attr('name'));
            });

            if(selected.length<=0)
            {
               swal({
                    title: "Please select data before Update",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    cancelButtonText: "No,Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                  });
            }
            else{
               for(var i=0;i<selected.length;i++){
                  var value=selected[i].split('/');                             
                  var values=selected[0].split('/');
                  var values=values[0];
                  var str1='company';
                  var str2='product';
                  var str3='categry';
                  if(values.indexOf(str1) != -1){
                     $.ajax({
                           url      :   "<?php echo base_url(); ?>index.php?/controller_superad/check_slas2",
                           type     :   "POST",
                           data     :   {'company_id':value[1]},
                           datatype :   "JSON",
                           cache    :   false,
                           success  :   function(data)
                                       {
                                          var data=JSON.parse(data);                                          
                                          if(data == 1){
                                             swal({
                                               title: "SLA is already set for Product category do you want to change.",
                                               type: "warning",
                                               showCancelButton: true,
                                               confirmButtonClass: "btn-danger",
                                               confirmButtonText: "Ok",
                                               cancelButtonText: "No,Cancel",
                                               closeOnConfirm: false,
                                               closeOnCancel: false
                                             },
                                             function(isConfirm){
                                                if (isConfirm) {
                                                   swal.close();
                                                   $.ajax({
                                                      url      :   "<?php echo base_url(); ?>index.php?/controller_superad/che_ck",
                                                      type     :   "POST",
                                                      data     :   {'company_id':value[1]},
                                                      datatype :   "JSON",
                                                      cache    :   false,
                                                      success  :   function(data){
                                                                     var data=$.trim(data);
                                                                     if(data){
                                                                        $('#myModal_sla').modal('show');
                                                                     }
                                                                  }
                                                   });                                                   
                                                }else{
                                                   swal.close();
                                                }                                               
                                             });
                                          }else{
                                             for(var i=0; i<data.length;i++){
                                                if(data[i].priority_level=="P1"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p1_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p1_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p1_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p1_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p1_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p1_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p1_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p1_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p1_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   //$('#p1_ref_id').val(data[i].ref_id);

                                                   $('#p1_reso_hour_1').attr('disabled', true);
                                                   $('#p1_reso_minute_1').attr('disabled', true);
                                                   $('#p1_res_hour_1').attr('disabled', true);
                                                   $('#p1_res_minute_1').attr('disabled', true);
                                                   $('#p1_acc_hour_1').attr('disabled', true);
                                                   $('#p1_acc_minute_1').attr('disabled', true);
                                                   $('#p1_sla_target_1').attr('disabled', true);
                                                   $('#p1_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p1_mttr_target_minute_1').attr('disabled', true);

                                                }else if(data[i].priority_level=="P2"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p2_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p2_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p2_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p2_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p2_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p2_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p2_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p2_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p2_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   $('#p2_ref_id').val(data[i].ref_id);

                                                   $('#p2_reso_hour_1').attr('disabled', true);
                                                   $('#p2_reso_minute_1').attr('disabled', true);
                                                   $('#p2_res_hour_1').attr('disabled', true);
                                                   $('#p2_res_minute_1').attr('disabled', true);
                                                   $('#p2_acc_hour_1').attr('disabled', true);
                                                   $('#p2_acc_minute_1').attr('disabled', true);
                                                   $('#p2_sla_target_1').attr('disabled', true);
                                                   $('#p2_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p2_mttr_target_minute_1').attr('disabled', true);

                                                }else if(data[i].priority_level=="P3"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p3_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p3_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p3_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p3_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p3_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p3_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p3_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p3_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p3_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   $('#p3_ref_id').val(data[i].ref_id);

                                                   $('#p3_reso_hour_1').attr('disabled', true);
                                                   $('#p3_reso_minute_1').attr('disabled', true);
                                                   $('#p3_res_hour_1').attr('disabled', true);
                                                   $('#p3_res_minute_1').attr('disabled', true);
                                                   $('#p3_acc_hour_1').attr('disabled', true);
                                                   $('#p3_acc_minute_1').attr('disabled', true);
                                                   $('#p3_sla_target_1').attr('disabled', true);
                                                   $('#p3_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p3_mttr_target_minute_1').attr('disabled', true);

                                                }else if(data[i].priority_level=="P4"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p4_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p4_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p4_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p4_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p4_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p4_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p4_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p4_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p4_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   $('#p4_ref_id').val(data[i].ref_id);

                                                   $('#p4_reso_hour_1').attr('disabled', true);
                                                   $('#p4_reso_minute_1').attr('disabled', true);
                                                   $('#p4_res_hour_1').attr('disabled', true);
                                                   $('#p4_res_minute_1').attr('disabled', true);
                                                   $('#p4_acc_hour_1').attr('disabled', true);
                                                   $('#p4_acc_minute_1').attr('disabled', true);
                                                   $('#p4_sla_target_1').attr('disabled', true);
                                                   $('#p4_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p4_mttr_target_minute_1').attr('disabled', true);

                                                }
                                             }
                                             $('#myModal_sla').modal('show');             
                                          }                                          
                                       }
                     });
                  }
                  else if(values.indexOf(str2) != -1){
                     var company_id="<?php echo $this->session->userdata('companyid');?>";
                     $.ajax({
                           url      :   "<?php echo base_url(); ?>index.php?/controller_superad/check_slas",
                           type     :   "POST",
                           data     :   {'company_id':company_id,'product_id':value[1]},
                           datatype :   "JSON",
                           cache    :   false,
                           success  :   function(data){
                                          var data=JSON.parse(data);
                                          if(data==1){
                                             swal({
                                               title: "SLA is already set for this company",
                                               type: "error",
                                               showCancelButton: false,
                                               confirmButtonClass: "btn-danger",
                                               confirmButtonText: "Ok",
                                               cancelButtonText: "No,Cancel",
                                               closeOnConfirm: false,
                                               closeOnCancel: false
                                             },
                                             function(isConfirm) {
                                                swal.close();
                                             });
                                          }else if(data == 2){
                                             swal({
                                               title: "SLA is already set for sub category do you want to change.",
                                               type: "warning",
                                               showCancelButton: true,
                                               confirmButtonClass: "btn-danger",
                                               confirmButtonText: "Ok",
                                               cancelButtonText: "No,Cancel",
                                               closeOnConfirm: false,
                                               closeOnCancel: false
                                             },
                                             function(isConfirm) {
                                                if (isConfirm) {
                                                   swal.close();
                                                   $.ajax({
                                                      url      :   "<?php echo base_url(); ?>index.php?/controller_superad/chec_k",
                                                      type     :   "POST",
                                                      data     :   {'company_id':value[1]},
                                                      datatype :   "JSON",
                                                      cache    :   false,
                                                      success  :   function(data){
                                                                     if(data){
                                                                        $('#myModal_sla').modal('show');
                                                                     }
                                                                  }
                                                   });                                                   
                                                }else{
                                                   swal.close();
                                                }                                                
                                             });
                                          }else{
                                             for(var i=0; i<data.length;i++){
                                                if(data[i].priority_level=="P1"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p1_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p1_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p1_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p1_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p1_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p1_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p1_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p1_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p1_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   //$('#p1_ref_id').val(data[i].ref_id);

                                                   $('#p1_reso_hour_1').attr('disabled', true);
                                                   $('#p1_reso_minute_1').attr('disabled', true);
                                                   $('#p1_res_hour_1').attr('disabled', true);
                                                   $('#p1_res_minute_1').attr('disabled', true);
                                                   $('#p1_acc_hour_1').attr('disabled', true);
                                                   $('#p1_acc_minute_1').attr('disabled', true);
                                                   $('#p1_sla_target_1').attr('disabled', true);
                                                   $('#p1_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p1_mttr_target_minute_1').attr('disabled', true);

                                                }else if(data[i].priority_level=="P2"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p2_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p2_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p2_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p2_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p2_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p2_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p2_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p2_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p2_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   $('#p2_ref_id').val(data[i].ref_id);

                                                   $('#p2_reso_hour_1').attr('disabled', true);
                                                   $('#p2_reso_minute_1').attr('disabled', true);
                                                   $('#p2_res_hour_1').attr('disabled', true);
                                                   $('#p2_res_minute_1').attr('disabled', true);
                                                   $('#p2_acc_hour_1').attr('disabled', true);
                                                   $('#p2_acc_minute_1').attr('disabled', true);
                                                   $('#p2_sla_target_1').attr('disabled', true);
                                                   $('#p2_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p2_mttr_target_minute_1').attr('disabled', true);

                                                }else if(data[i].priority_level=="P3"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p3_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p3_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p3_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p3_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p3_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p3_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p3_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p3_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p3_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   $('#p3_ref_id').val(data[i].ref_id);

                                                   $('#p3_reso_hour_1').attr('disabled', true);
                                                   $('#p3_reso_minute_1').attr('disabled', true);
                                                   $('#p3_res_hour_1').attr('disabled', true);
                                                   $('#p3_res_minute_1').attr('disabled', true);
                                                   $('#p3_acc_hour_1').attr('disabled', true);
                                                   $('#p3_acc_minute_1').attr('disabled', true);
                                                   $('#p3_sla_target_1').attr('disabled', true);
                                                   $('#p3_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p3_mttr_target_minute_1').attr('disabled', true);

                                                }else if(data[i].priority_level=="P4"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p4_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p4_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p4_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p4_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p4_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p4_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p4_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p4_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p4_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   $('#p4_ref_id').val(data[i].ref_id);

                                                   $('#p4_reso_hour_1').attr('disabled', true);
                                                   $('#p4_reso_minute_1').attr('disabled', true);
                                                   $('#p4_res_hour_1').attr('disabled', true);
                                                   $('#p4_res_minute_1').attr('disabled', true);
                                                   $('#p4_acc_hour_1').attr('disabled', true);
                                                   $('#p4_acc_minute_1').attr('disabled', true);
                                                   $('#p4_sla_target_1').attr('disabled', true);
                                                   $('#p4_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p4_mttr_target_minute_1').attr('disabled', true);

                                                }                 
                                                $('#myModal_sla').modal('show');
                                             }                                             
                                          }
                                             
                                       }
                     });
                  }
                  else if(values.indexOf(str3) != -1){
                     var company_id="<?php echo $this->session->userdata('companyid');?>";
                     $.ajax({
                           url      :   "<?php echo base_url(); ?>index.php?/controller_superad/check_slas1",
                           type     :   "POST",
                           data     :   {'company_id':company_id,'prod_id':value[1]},
                           datatype :   "JSON",
                           cache    :   false,
                           success  :   function(data){ 
                                          var data=JSON.parse(data);
                                          if(data==1){
                                             swal({
                                               title: "SLA is already set to the Product Category",
                                               type: "error",
                                               showCancelButton: false,
                                               confirmButtonClass: "btn-danger",
                                               confirmButtonText: "Ok",
                                               cancelButtonText: "No,Cancel",
                                               closeOnConfirm: false,
                                               closeOnCancel: false
                                             },
                                             function(isConfirm) {
                                                swal.close();
                                             });
                                          }else{
                                             for(var i=0; i<data.length;i++){
                                                if(data[i].priority_level=="P1"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p1_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p1_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p1_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p1_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p1_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p1_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p1_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p1_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p1_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   //$('#p1_ref_id').val(data[i].ref_id);

                                                   $('#p1_reso_hour_1').attr('disabled', true);
                                                   $('#p1_reso_minute_1').attr('disabled', true);
                                                   $('#p1_res_hour_1').attr('disabled', true);
                                                   $('#p1_res_minute_1').attr('disabled', true);
                                                   $('#p1_acc_hour_1').attr('disabled', true);
                                                   $('#p1_acc_minute_1').attr('disabled', true);
                                                   $('#p1_sla_target_1').attr('disabled', true);
                                                   $('#p1_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p1_mttr_target_minute_1').attr('disabled', true);

                                                }else if(data[i].priority_level=="P2"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p2_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p2_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p2_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p2_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p2_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p2_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p2_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p2_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p2_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   $('#p2_ref_id').val(data[i].ref_id);

                                                   $('#p2_reso_hour_1').attr('disabled', true);
                                                   $('#p2_reso_minute_1').attr('disabled', true);
                                                   $('#p2_res_hour_1').attr('disabled', true);
                                                   $('#p2_res_minute_1').attr('disabled', true);
                                                   $('#p2_acc_hour_1').attr('disabled', true);
                                                   $('#p2_acc_minute_1').attr('disabled', true);
                                                   $('#p2_sla_target_1').attr('disabled', true);
                                                   $('#p2_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p2_mttr_target_minute_1').attr('disabled', true);

                                                }else if(data[i].priority_level=="P3"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p3_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p3_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p3_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p3_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p3_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p3_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p3_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p3_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p3_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   $('#p3_ref_id').val(data[i].ref_id);

                                                   $('#p3_reso_hour_1').attr('disabled', true);
                                                   $('#p3_reso_minute_1').attr('disabled', true);
                                                   $('#p3_res_hour_1').attr('disabled', true);
                                                   $('#p3_res_minute_1').attr('disabled', true);
                                                   $('#p3_acc_hour_1').attr('disabled', true);
                                                   $('#p3_acc_minute_1').attr('disabled', true);
                                                   $('#p3_sla_target_1').attr('disabled', true);
                                                   $('#p3_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p3_mttr_target_minute_1').attr('disabled', true);

                                                }else if(data[i].priority_level=="P4"){
                                                   var reso_hour=data[i].resolution_time.split(':');
                                                   var res_hour=data[i].response_time.split(':');
                                                   var acc_hour=data[i].acceptance_time.split(':');
                                                   var mttr_target=data[i].mttr_target.split(':');
                                                   $('#p4_reso_hour_1').val(parseInt(reso_hour[0], 10));
                                                   $('#p4_reso_minute_1').val(parseInt(reso_hour[1], 10));
                                                   $('#p4_res_hour_1').val(parseInt(res_hour[0], 10));
                                                   $('#p4_res_minute_1').val(parseInt(res_hour[1], 10));
                                                   $('#p4_acc_hour_1').val(parseInt(acc_hour[0], 10));
                                                   $('#p4_acc_minute_1').val(parseInt(acc_hour[1], 10));        
                                                   $('#p4_sla_target_1').val(parseInt(data[i].SLA_Compliance_Target, 10));
                                                   $('#p4_mttr_target_hour_1').val(parseInt(mttr_target[0], 10));
                                                   $('#p4_mttr_target_minute_1').val(parseInt(mttr_target[1], 10));
                                                   $('#p4_ref_id').val(data[i].ref_id);

                                                   $('#p4_reso_hour_1').attr('disabled', true);
                                                   $('#p4_reso_minute_1').attr('disabled', true);
                                                   $('#p4_res_hour_1').attr('disabled', true);
                                                   $('#p4_res_minute_1').attr('disabled', true);
                                                   $('#p4_acc_hour_1').attr('disabled', true);
                                                   $('#p4_acc_minute_1').attr('disabled', true);
                                                   $('#p4_sla_target_1').attr('disabled', true);
                                                   $('#p4_mttr_target_hour_1').attr('disabled', true);
                                                   $('#p4_mttr_target_minute_1').attr('disabled', true);

                                                }
                                             }              
                                             $('#myModal_sla').modal('show');
                                          }                                          
                                       }
                     });                     
                  }
               }
               
            }
         }

         function onclick_company(company_id)
         {
            if($('input[name="company/'+company_id+'"]:checked').length > 0)
            {
               $(".nothings :checkbox").prop("checked", true);
               $("#subcategory :checkbox").prop("checked", true);//$(":checkbox.nothings").attr("checked", true);
            }
            else{
               $(".nothings :checkbox").prop("checked", false);
               $("#subcategory :checkbox").prop("checked", false);
            }
         }
         function onclick_product(prod_id,company_id){
            if($('input[name="#product/'+prod_id+' :checked').length <=0)
               {
                  $('input[name="company/'+company_id+'"]:checkbox').prop("checked", false);
                  $("#subcategory :checkbox").prop("checked", false);
               }
         }

         function onclick_cat(cat_id,prod_id,company_id){
            if($('input[name="#categry/'+cat_id+' :checked').length <=0)
               {
                  $('input[name="product/'+prod_id+'"]:checkbox').prop("checked", false);
                  $('input[name="company/'+company_id+'"]:checkbox').prop("checked", false);
               }
         }
            $(document).ready(function(){
                   $.ajax({
                       url         :   "<?php echo base_url(); ?>index.php?/controller_superad/get_details_company",
                       type        :   "POST",
                       data        :   "",// {action:'$funky'}
                       datatype     :   "JSON",
                       cache       :   false,
                       success     :   function(data)
                  {
                        var data=JSON.parse(data);
                  for(i=0;i<data.length;i++)
                  {
                     var company=data[i].company_id;
                            //$('#company').append("<li id="+data[i].company_id+" onclick=onclick_company('"+data[i].company_id+"');><div class='product-checkbox2'><input type='checkbox' class='product_checkbox' value='"+data[i].company_id+"' name='company/"+data[i].company_id+"'></div><a href='."+data[i].company_id+"' data-toggle='tab'><div id='frame'><span class='helper'></span><img src="+data[i].company_logo+" height=250 /></div></a><div class='caption catg-title2'><p>"+data[i].company_name+"</p></div></li>");
                            $('#company').append("<li id="+data[i].company_id+" onclick=onclick_company('');><div class='product-checkbox2'><input type='checkbox' class='product_checkbox' value='"+data[i].company_id+"' name='company/"+data[i].company_id+"'></div><a href='."+data[i].company_id+"' data-toggle='tab'><div id='frame'><span class='helper'></span><img src="+data[i].company_logo+" height=250 /></div></a><div class='caption catg-title2'><p>"+data[i].company_name+"</p></div></li>");
                            $('ul#company li:first-child').addClass('active');
                            $('#product_append').append("<div class='tab-pane product_append "+data[i].company_id+"' id><p class='main-category'>Product Category</p></div>");
                            $.ajax({
                           url         :   "<?php echo base_url(); ?>index.php?/controller_superad/get_product_company",
                           type        :   "POST",
                           data        :   {'company_id':data[i].company_id},// {action:'$funky'}
                           datatype    :   "JSON",
                           success     :   function(data)
                           {
                              var data=JSON.parse(data);                             
                              for(i=0;i<data.length;i++)
                              {
                                 $("."+data[i].company_id+"").append("<div class='col-sm-4 nothings "+data[i].product_id+"' onclick=onclick_product('"+data[i].product_id+"','"+company+"');><div class='product-checkbox'><input type='checkbox' value='"+data[i].product_id+"' name='product/"+data[i].product_id+"' ></div><a id='"+data[i].product_id+"product_display' data-toggle='collapse' data-parent='#accordion' href='#"+data[i].product_id+"'><img src="+data[i].product_image+" class='img-responsive'  /></a><div class='caption catg-title2'><p>"+data[i].product_name+"</p></div></div>");
                                 $("#"+data[i].product_id+"product_display").click(function()
                                 {
                                    var sub_load=$(this).attr('href');
                                    var sub_load = sub_load.replace('#', '');
                                    $('.nothings').toggle();
                                    $("."+sub_load).toggle();
                                    if($('#subcategory').length>0)
                                    {
                                       $('span.clearfix').remove();
                                       $('#subcategory').remove();
                                    }
                                    else
                                    {
                                       $("<span class='clearfix'></span><div class='col-sm-12' id='subcategory'><div id="+sub_load+" class='sub-products'><p class='main-category'>Sub Category</p></div></div>" ).insertAfter( "."+sub_load );
                                       $.ajax({
                                          url         :   "<?php echo base_url(); ?>index.php?/controller_superad/get_subproduct_company",
                                          type        :   "POST",
                                          data        :   {'prod_id':sub_load},// {action:'$funky'}
                                          datatype    :   "JSON",
                                          success     :   function(data)
                                          {
                                             var data=JSON.parse(data);
                                             for(i=0;i<data.length;i++)
                                             {
                                                $("#"+data[i].prod_id).append("<div class='col-sm-4' id="+data[i].cat_id+" onclick=onclick_cat('"+data[i].cat_id+"','"+sub_load+"','"+company+"');><div class='product-checkbox'><input type='checkbox'  value="+data[i].cat_id+" name='categry/"+data[i].cat_id+"'></div><a data-toggle='' data-parent='' href=''><img src="+data[i].cat_image+" class='img-responsive'  /></a><div class='caption catg-title2'><p>"+data[i].cat_name+"</p></div>");
                                             }
                                           }
                                       });
                                    }
                                 });
                              }

                           $("#product_append div:first").addClass("active");
                           },
                        });
                        //$('#product_append').append("");
                     }
                  },
                   });
               });
            function check(p1,p1_reso_hour_1,p1_reso_minute_1,p1_res_hour_1,p1_res_minute_1,p1_acc_hour_1,p1_acc_minute_1,p1_sla_target_1,p1_mttr_target_hour_1,p1_mttr_target_minute_1,p2,p2_reso_hour_1,p2_reso_minute_1,p2_res_hour_1,p2_res_minute_1,p2_acc_hour_1,p2_acc_minute_1,p2_sla_target_1,p2_mttr_target_hour_1,p2_mttr_target_minute_1,p3,p3_reso_hour_1,p3_reso_minute_1,p3_res_hour_1,p3_res_minute_1,p3_acc_hour_1,p3_acc_minute_1,p3_sla_target_1,p3_mttr_target_hour_1,p3_mttr_target_minute_1,p4,p4_reso_hour_1,p4_reso_minute_1,p4_res_hour_1,p4_res_minute_1,p4_acc_hour_1,p4_acc_minute_1,p4_sla_target_1,p4_mttr_target_hour_1,p4_mttr_target_minute_1){ 

               $('#row_data').empty();
               $('#responses').empty();
               $('#p1').css('background-color','#eef1f5');   
               $('#p1').css('color','#000'); 
               $('#p2').css('background-color','#eef1f5');   
               $('#p2').css('color','#000'); 
               $('#p3').css('background-color','#eef1f5');   
               $('#p3').css('color','#000'); 
               $('#p4').css('background-color','#eef1f5');   
               $('#p4').css('color','#000'); 

               var rights = $('input[name=editable_right]:checked').val();

              p1=$('#p1').val();
              p1_reso_hour_1=$('#p1_reso_hour_1').val();
              p1_reso_minute_1=$('#p1_reso_minute_1').val();
              p1_res_hour_1=$('#p1_res_hour_1').val();
              p1_res_minute_1=$('#p1_res_minute_1').val();
              p1_acc_hour_1=$('#p1_acc_hour_1').val();
              p1_acc_minute_1=$('#p1_acc_minute_1').val();
              p1_mttr_target_hour_1=$('#p1_mttr_target_hour_1').val();
              p1_mttr_target_minute_1=$('#p1_mttr_target_minute_1').val();
              if($('#p1_reso_hour_1').val()<10)
              {
                 p1_reso_hour_1='0'+$('#p1_reso_hour_1').val();
              }
              if($('#p1_reso_minute_1').val()<10)
              {
                 p1_reso_minute_1='0'+$('#p1_reso_minute_1').val();
              }
              if($('#p1_res_minute_1').val()<10)
              {
                 p1_res_minute_1='0'+$('#p1_res_minute_1').val();
              }
              if($('#p1_res_hour_1').val()<10)
              {
                 p1_res_hour_1='0'+$('#p1_res_hour_1').val();
              }
              if($('#p1_acc_hour_1').val()<10)
              {
                 p1_acc_hour_1='0'+$('#p1_acc_hour_1').val();
              }
              if($('#p1_acc_minute_1').val()<10)
              {
                 p1_acc_minute_1='0'+$('#p1_acc_minute_1').val();
              }
              if($('#p1_mttr_target_hour_1').val()<10)
              {
                p1_mttr_target_hour_1='0'+$('#p1_mttr_target_hour_1').val();
              }
              if($('#p1_mttr_target_minute_1').val()<10)
              {
                 p1_mttr_target_minute_1='0'+$('#p1_mttr_target_minute_1').val();
              }
              p1_resolution=p1_reso_hour_1+':'+p1_reso_minute_1+':00';
              p1_response=p1_res_hour_1+':'+p1_res_minute_1+':00';
              p1_acceptance=p1_acc_hour_1+':'+p1_acc_minute_1+':00';
              p1_sla_compliance=$('#p1_sla_target_1').val();;
              p1_mttr=p1_mttr_target_hour_1+':'+p1_mttr_target_minute_1+':00';
              
              
              p2=$('#p2').val();
              p2_reso_hour_1=$('#p2_reso_hour_1').val();
              p2_reso_minute_1=$('#p2_reso_minute_1').val();
              p2_res_hour_1=$('#p2_res_hour_1').val();
              p2_res_minute_1=$('#p2_res_minute_1').val();
              p2_acc_hour_1=$('#p2_acc_hour_1').val();
              p2_acc_minute_1=$('#p2_acc_minute_1').val();
              p2_mttr_target_hour_1=$('#p2_mttr_target_hour_1').val();
              p2_mttr_target_minute_1=$('#p2_mttr_target_minute_1').val();
              if($('#p2_reso_hour_1').val()<10)
              {
                 p2_reso_hour_1='0'+$('#p2_reso_hour_1').val();
              }
              if($('#p2_reso_minute_1').val()<10)
              {
                 p2_reso_minute_1='0'+$('#p2_reso_minute_1').val();
              }
              if($('#p2_res_minute_1').val()<10)
              {
                 p2_res_minute_1='0'+$('#p2_res_minute_1').val();
              }
              if($('#p2_res_hour_1').val()<10)
              {
                 p2_res_hour_1='0'+$('#p2_res_hour_1').val();
              }
              if($('#p2_acc_hour_1').val()<10)
              {
                 p2_acc_hour_1='0'+$('#p2_acc_hour_1').val();
              }
              if($('#p2_acc_minute_1').val()<10)
              {
                 p2_acc_minute_1='0'+$('#p2_acc_minute_1').val();
              }
              if($('#p2_mttr_target_hour_1').val()<10)
              {
                p2_mttr_target_hour_1='0'+$('#p2_mttr_target_hour_1').val();
              }
              if($('#p2_mttr_target_minute_1').val()<10)
              {
                 p2_mttr_target_minute_1='0'+$('#p2_mttr_target_minute_1').val();
              }
              p2_resolution=p2_reso_hour_1+':'+p2_reso_minute_1+':00';
              p2_response=p2_res_hour_1+':'+p2_res_minute_1+':00';
              p2_acceptance=p2_acc_hour_1+':'+p2_acc_minute_1+':00';
              p2_sla_compliance=$('#p2_sla_target_1').val();
              p2_mttr=p2_mttr_target_hour_1+':'+p2_mttr_target_minute_1+':00';
              
              
              p3=$('#p3').val();
              p3_reso_hour_1=$('#p3_reso_hour_1').val();
              p3_reso_minute_1=$('#p3_reso_minute_1').val();
              p3_res_hour_1=$('#p3_res_hour_1').val();
              p3_res_minute_1=$('#p3_res_minute_1').val();
              p3_acc_hour_1=$('#p3_acc_hour_1').val();
              p3_acc_minute_1=$('#p3_acc_minute_1').val();
              p3_mttr_target_hour_1=$('#p3_mttr_target_hour_1').val();
              p3_mttr_target_minute_1=$('#p3_mttr_target_minute_1').val();
              if($('#p3_reso_hour_1').val()<10)
              {
                 p3_reso_hour_1='0'+$('#p3_reso_hour_1').val();
              }
              if($('#p3_reso_minute_1').val()<10)
              {
                 p3_reso_minute_1='0'+$('#p3_reso_minute_1').val();
              }
              if($('#p3_res_minute_1').val()<10)
              {
                 p3_res_minute_1='0'+$('#p3_res_minute_1').val();
              }
              if($('#p3_res_hour_1').val()<10)
              {
                 p3_res_hour_1='0'+$('#p3_res_hour_1').val();
              }
              if($('#p3_acc_hour_1').val()<10)
              {
                 p3_acc_hour_1='0'+$('#p3_acc_hour_1').val();
              }
              if($('#p3_acc_minute_1').val()<10)
              {
                 p3_acc_minute_1='0'+$('#p3_acc_minute_1').val();
              }
              if($('#p3_mttr_target_hour_1').val()<10)
              {
                p3_mttr_target_hour_1='0'+$('#p3_mttr_target_hour_1').val();
              }
              if($('#p3_mttr_target_minute_1').val()<10)
              {
                 p3_mttr_target_minute_1='0'+$('#p3_mttr_target_minute_1').val();
              }
              p3_resolution=p3_reso_hour_1+':'+p3_reso_minute_1+':00';
              p3_response=p3_res_hour_1+':'+p3_res_minute_1+':00';
              p3_acceptance=p3_acc_hour_1+':'+p3_acc_minute_1+':00';
              p3_sla_compliance=$('#p3_sla_target_1').val();
              p3_mttr=p3_mttr_target_hour_1+':'+p3_mttr_target_minute_1+':00';
              
              
              p4=$('#p4').val();
              p4_reso_hour_1=$('#p4_reso_hour_1').val();
              p4_reso_minute_1=$('#p4_reso_minute_1').val();
              p4_res_hour_1=$('#p4_res_hour_1').val();
              p4_res_minute_1=$('#p4_res_minute_1').val();
              p4_acc_hour_1=$('#p4_acc_hour_1').val();
              p4_acc_minute_1=$('#p4_acc_minute_1').val();
              p4_mttr_target_hour_1=$('#p4_mttr_target_hour_1').val();
              p4_mttr_target_minute_1=$('#p4_mttr_target_minute_1').val();
              if($('#p4_reso_hour_1').val()<10)
              {
                 p4_reso_hour_1='0'+$('#p4_reso_hour_1').val();
              }
              if($('#p4_reso_minute_1').val()<10)
              {
                 p4_reso_minute_1='0'+$('#p4_reso_minute_1').val();
              }
              if($('#p4_res_minute_1').val()<10)
              {
                 p4_res_minute_1='0'+$('#p4_res_minute_1').val();
              }
              if($('#p4_res_hour_1').val()<10)
              {
                 p4_res_hour_1='0'+$('#p4_res_hour_1').val();
              }
              if($('#p4_acc_hour_1').val()<10)
              {
                 p4_acc_hour_1='0'+$('#p4_acc_hour_1').val();
              }
              if($('#p4_acc_minute_1').val()<10)
              {
                 p4_acc_minute_1='0'+$('#p4_acc_minute_1').val();
              }
              if($('#p4_mttr_target_hour_1').val()<10)
              {
                p4_mttr_target_hour_1='0'+$('#p4_mttr_target_hour_1').val();
              }
              if($('#p4_mttr_target_minute_1').val()<10)
              {
                 p4_mttr_target_minute_1='0'+$('#p4_mttr_target_minute_1').val();
              }
              p4_resolution=p4_reso_hour_1+':'+p4_reso_minute_1+':00';
              p4_response=p4_res_hour_1+':'+p4_res_minute_1+':00';
              p4_acceptance=p4_acc_hour_1+':'+p4_acc_minute_1+':00';
              p4_sla_compliance=$('#p4_sla_target_1').val();
              p4_mttr=p4_mttr_target_hour_1+':'+p4_mttr_target_minute_1+':00';
              
              var rights = $("input[name='editable_right']:checked").val();
              
              var selected = [];
              $('.company_div  input:checked').each(function() {
               selected.push($(this).attr('name'));
              });
              
              selected=selected.sort();
                              
              var msg=new Array();

              var nanthakumar = [];

              if((p1=='P1' && p1_reso_hour_1=='not_sel' && p1_reso_minute_1=='not_sel' && p1_res_hour_1=='not_sel' && p1_res_minute_1=='not_sel' && p1_acc_hour_1=='not_sel'  && p1_acc_minute_1=='not_sel' && p1_sla_compliance=='not_sel'&& p1_mttr_target_hour_1=='not_sel' && p1_mttr_target_minute_1=='not_sel') && (p2=='P2' && p2_reso_hour_1=='not_sel' && p2_reso_minute_1=='not_sel' && p2_res_hour_1=='not_sel' && p2_res_minute_1=='not_sel' && p2_acc_hour_1=='not_sel'  && p2_acc_minute_1=='not_sel' && p2_sla_compliance=='not_sel'&& p2_mttr_target_hour_1=='not_sel' && p2_mttr_target_minute_1=='not_sel') && (p3=='P3' && p3_reso_hour_1=='not_sel' && p3_reso_minute_1=='not_sel' && p3_res_hour_1=='not_sel' && p3_res_minute_1=='not_sel' && p3_acc_hour_1=='not_sel'  && p3_acc_minute_1=='not_sel' && p3_sla_compliance=='not_sel'&& p3_mttr_target_hour_1=='not_sel' && p3_mttr_target_minute_1=='not_sel') && (p4=='P4' && p4_reso_hour_1=='not_sel' && p4_reso_minute_1=='not_sel' && p4_res_hour_1=='not_sel' && p4_res_minute_1=='not_sel' && p4_acc_hour_1=='not_sel'  && p4_acc_minute_1=='not_sel' && p4_sla_compliance=='not_sel'&& p4_mttr_target_hour_1=='not_sel' && p4_mttr_target_minute_1=='not_sel')){
                     msg.push("Any one of the priority is mandatory to assign SLA Target");
                    /*swal({
                       title: "Any one of the priority is mandatory to assign SLA Target",
                       type: "error",
                       showCancelButton: false,
                       confirmButtonClass: "btn-danger",
                       confirmButtonText: "Ok",
                       cancelButtonText: "No,Cancel",
                       closeOnConfirm: false,
                       closeOnCancel: false
                     },
                     function(isConfirm) {
                        swal.close();
                     }); */                    
               }else if(rights == ""){
                     /*swal({
                       title: "Editable rights are mandatory",
                       type: "error",
                       showCancelButton: false,
                       confirmButtonClass: "btn-danger",
                       confirmButtonText: "Ok",
                       cancelButtonText: "No,Cancel",
                       closeOnConfirm: false,
                       closeOnCancel: false
                     },
                     function(isConfirm) {
                        swal.close();
                     }); */
               }else{
                  msg=[];
                  //nanthakumar=[];
                  if(p1_reso_hour_1 != 'not_sel' || p1_reso_minute_1 != 'not_sel' || p1_res_hour_1 != 'not_sel' || p1_res_minute_1 != 'not_sel' || p1_acc_hour_1 != 'not_sel' || p1_acc_minute_1 != 'not_sel' || p1_sla_compliance != 'not_sel' || p1_mttr_target_hour_1 != 'not_sel' || p1_mttr_target_minute_1 != 'not_sel'){
                        if(p1_reso_hour_1 == 'not_sel' || p1_reso_minute_1 == 'not_sel' || p1_res_hour_1 == 'not_sel' || p1_res_minute_1 == 'not_sel' || p1_acc_hour_1 == 'not_sel' || p1_acc_minute_1 == 'not_sel' || p1_sla_compliance == 'not_sel' || p1_mttr_target_hour_1 == 'not_sel' || p1_mttr_target_minute_1 == 'not_sel'){

                           msg=[]; 
                           $('#p1').css('background-color','red');   
                           $('#p1').css('color','white');                     
                           msg.push("All fields are Mandatory in Red backgrounded priority ");                           

                        }else{                      

                           if(selected.length<=0){
                              swal({
                                title: "Please select data before Update",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                cancelButtonText: "No,Cancel",
                                closeOnConfirm: false,
                                closeOnCancel: false
                              },
                              function(isConfirm) {
                              });
                           }else{
                              
                              for(var i=0;i<selected.length;i++){
                                 var value=selected[i].split('/');                              
                                 var values=selected[0];                              
                                 var str1='company';
                                 var str2='product';
                                 var str3='categry';
                                 if(values.indexOf(str1) != -1){
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':value[1],'product_id':'all','cat_id':'all','priority':p1,'response':p1_response,'resolution':p1_resolution,'acceptance':p1_acceptance,'sla_compliance':p1_sla_compliance,'mttr':p1_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide');                                             
                                             nanthakumar = [];
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p1+'<br>'); 
                                          }
                                    });
                                 }
                                 else if(values.indexOf(str2) != -1){
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':'','product_id':value[1],'cat_id':'all','priority':p1,'response':p1_response,'resolution':p1_resolution,'acceptance':p1_acceptance,'sla_compliance':p1_sla_compliance,'mttr':p1_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide'); 
                                             nanthakumar = [];                                            
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p1+'<br>'); 
                                          }
                                    });
                                 }

                                 else{
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':'','product_id':'','cat_id':value[1],'priority':p1,'response':p1_response,'resolution':p1_resolution,'acceptance':p1_acceptance,'sla_compliance':p1_sla_compliance,'mttr':p1_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide'); 
                                             nanthakumar = [];                                            
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p1+'<br>'); 
                                          }
                                    });
                                 }
                              }
                           }

                        }
                     }

                     if(p2_reso_hour_1 != 'not_sel' || p2_reso_minute_1 != 'not_sel' || p2_res_hour_1 != 'not_sel' || p2_res_minute_1 != 'not_sel' || p2_acc_hour_1 != 'not_sel' || p2_acc_minute_1 != 'not_sel' || p2_sla_compliance != 'not_sel' || p2_mttr_target_hour_1 != 'not_sel' || p2_mttr_target_minute_1 != 'not_sel'){
                        if(p2_reso_hour_1 == 'not_sel' || p2_reso_minute_1 == 'not_sel' || p2_res_hour_1 == 'not_sel' || p2_res_minute_1 == 'not_sel' || p2_acc_hour_1 == 'not_sel' || p2_acc_minute_1 == 'not_sel' || p2_sla_compliance == 'not_sel' || p2_mttr_target_hour_1 == 'not_sel' || p2_mttr_target_minute_1 == 'not_sel'){

                           msg=[]; 
                           $('#p2').css('background-color','red');   
                           $('#p2').css('color','white');                     
                           msg.push("All fields are Mandatory in Red backgrounded priority ");

                        }else{                      

                           if(selected.length<=0){
                              swal({
                                title: "Please select data before Update",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                cancelButtonText: "No,Cancel",
                                closeOnConfirm: false,
                                closeOnCancel: false
                              },
                              function(isConfirm) {
                              });
                           }else{
                              
                              for(var i=0;i<selected.length;i++){
                                 var value=selected[i].split('/');                              
                                 var values=selected[0];                              
                                 var str1='company';
                                 var str2='product';
                                 var str3='categry';
                                 if(values.indexOf(str1) != -1){
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':value[1],'product_id':'all','cat_id':'all','priority':p2,'response':p2_response,'resolution':p2_resolution,'acceptance':p2_acceptance,'sla_compliance':p2_sla_compliance,'mttr':p2_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide');   
                                             nanthakumar = [];                                          
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p2+'<br>'); 
                                          }
                                    });
                                 }

                                 else if(values.indexOf(str2) != -1){
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':'','product_id':value[1],'cat_id':'all','priority':p2,'response':p2_response,'resolution':p2_resolution,'acceptance':p2_acceptance,'sla_compliance':p2_sla_compliance,'mttr':p2_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide'); 
                                             nanthakumar = [];                                            
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p2+'<br>'); 
                                          }
                                    });
                                 }

                                 else{
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':'','product_id':'','cat_id':value[1],'priority':p2,'response':p2_response,'resolution':p2_resolution,'acceptance':p2_acceptance,'sla_compliance':p2_sla_compliance,'mttr':p2_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide'); 
                                             nanthakumar = [];                                            
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p2+'<br>'); 
                                          }
                                    });
                                 }
                              }
                           }

                        }
                     }

                     if(p3_reso_hour_1 != 'not_sel' || p3_reso_minute_1 != 'not_sel' || p3_res_hour_1 != 'not_sel' || p3_res_minute_1 != 'not_sel' || p3_acc_hour_1 != 'not_sel' || p3_acc_minute_1 != 'not_sel' || p3_sla_compliance != 'not_sel' || p3_mttr_target_hour_1 != 'not_sel' || p3_mttr_target_minute_1 != 'not_sel'){
                        if(p3_reso_hour_1 == 'not_sel' || p3_reso_minute_1 == 'not_sel' || p3_res_hour_1 == 'not_sel' || p3_res_minute_1 == 'not_sel' || p3_acc_hour_1 == 'not_sel' || p3_acc_minute_1 == 'not_sel' || p3_sla_compliance == 'not_sel' || p3_mttr_target_hour_1 == 'not_sel' || p3_mttr_target_minute_1 == 'not_sel'){

                           msg=[]; 
                           $('#p3').css('background-color','red');   
                           $('#p3').css('color','white');                     
                           msg.push("All fields are Mandatory in Red backgrounded priority ");

                        }else{                      

                           if(selected.length<=0){
                              swal({
                                title: "Please select data before Update",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                cancelButtonText: "No,Cancel",
                                closeOnConfirm: false,
                                closeOnCancel: false
                              },
                              function(isConfirm) {
                              });
                           }else{
                              
                              for(var i=0;i<selected.length;i++){
                                 var value=selected[i].split('/');                              
                                 var values=selected[0];                              
                                 var str1='company';
                                 var str2='product';
                                 var str3='categry';
                                 if(values.indexOf(str1) != -1){
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':value[1],'product_id':'all','cat_id':'all','priority':p3,'response':p3_response,'resolution':p3_resolution,'acceptance':p3_acceptance,'sla_compliance':p3_sla_compliance,'mttr':p3_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide');    
                                             nanthakumar = [];                                     
                                             nanthakumar.push(data);                                             
                                             $('#responses').append(nanthakumar+' for the priority '+p3+'<br>'); 
                                          }
                                    });
                                 }

                                 else if(values.indexOf(str2) != -1){
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':'','product_id':value[1],'cat_id':'all','priority':p3,'response':p3_response,'resolution':p3_resolution,'acceptance':p3_acceptance,'sla_compliance':p3_sla_compliance,'mttr':p3_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide'); 
                                             nanthakumar = [];                                            
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p3+'<br>'); 
                                          }
                                    });
                                 }

                                 else{
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':'','product_id':'','cat_id':value[1],'priority':p3,'response':p3_response,'resolution':p3_resolution,'acceptance':p3_acceptance,'sla_compliance':p3_sla_compliance,'mttr':p3_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide'); 
                                             nanthakumar = [];                                            
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p3+'<br>'); 
                                          }
                                    });
                                 }
                              }
                           }

                        }
                     }

                     if(p4_reso_hour_1 != 'not_sel' || p4_reso_minute_1 != 'not_sel' || p4_res_hour_1 != 'not_sel' || p4_res_minute_1 != 'not_sel' || p4_acc_hour_1 != 'not_sel' || p4_acc_minute_1 != 'not_sel' || p4_sla_compliance != 'not_sel' || p4_mttr_target_hour_1 != 'not_sel' || p4_mttr_target_minute_1 != 'not_sel'){
                        if(p4_reso_hour_1 == 'not_sel' || p4_reso_minute_1 == 'not_sel' || p4_res_hour_1 == 'not_sel' || p4_res_minute_1 == 'not_sel' || p4_acc_hour_1 == 'not_sel' || p4_acc_minute_1 == 'not_sel' || p4_sla_compliance == 'not_sel' || p4_mttr_target_hour_1 == 'not_sel' || p4_mttr_target_minute_1 == 'not_sel'){

                           msg=[]; 
                           $('#p4').css('background-color','red');   
                           $('#p4').css('color','white');                     
                           msg.push("All fields are Mandatory in Red backgrounded priority ");

                        }else{                      

                           if(selected.length<=0){
                              swal({
                                title: "Please select data before Update",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                cancelButtonText: "No,Cancel",
                                closeOnConfirm: false,
                                closeOnCancel: false
                              },
                              function(isConfirm) {
                              });
                           }else{
                              
                              for(var i=0;i<selected.length;i++){
                                 var value=selected[i].split('/');                              
                                 var values=selected[0];                              
                                 var str1='company';
                                 var str2='product';
                                 var str3='categry';
                                 if(values.indexOf(str1) != -1){
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':value[1],'product_id':'all','cat_id':'all','priority':p4,'response':p4_response,'resolution':p4_resolution,'acceptance':p4_acceptance,'sla_compliance':p4_sla_compliance,'mttr':p4_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide'); 
                                             nanthakumar = [];                                            
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p4+'<br>'); 
                                          }
                                    });
                                 }

                                 else if(values.indexOf(str2) != -1){
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':'','product_id':value[1],'cat_id':'all','priority':p4,'response':p4_response,'resolution':p4_resolution,'acceptance':p4_acceptance,'sla_compliance':p4_sla_compliance,'mttr':p4_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide'); 
                                             nanthakumar = [];                                            
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p4+'<br>'); 
                                          }
                                    });
                                 }

                                 else{
                                    $.ajax({
                                          url      :   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                                          type     :   "POST",
                                          data     :   {'company_id':'','product_id':'','cat_id':value[1],'priority':p4,'response':p4_response,'resolution':p4_resolution,'acceptance':p4_acceptance,'sla_compliance':p4_sla_compliance,'mttr':p4_mttr,'rights':rights},
                                          datatype :   "JSON",
                                          cache    :   false,
                                          success  :   function(data)
                                          {                                          
                                             data=data.trim();
                                             //$('#myModal_sla').modal('hide'); 
                                             nanthakumar = [];                                            
                                             nanthakumar.push(data);
                                             $('#responses').append(nanthakumar+' for the priority '+p4+'<br>'); 
                                          }
                                    });
                                 }

                              }
                           }

                        }
                     }
                 }
                 
                 $('#row_data').text(msg);
                 $('.error').show();
                 if($('#responses').html()==""){
                     $('#responses').text(msg);
                     $('#sla_confirm').modal('show');
                 }else{
                     $('#sla_confirm').modal('show');
                 }                 
         }
            </script>
            <script>
               $('.collapse').on('show.bs.collapse', function (e) {
                  $('.collapse').not(e.target).removeClass('in');
               });
            </script>
            <script>
               $('.tab-sla-page img').click(function(){
                 $('.selected').removeClass('selected'); // removes the previous selected class
                 $(this).addClass('selected'); // adds the class to the clicked image
               });
            </script>
            <script>
               /*$('.product-checkbox2').click(function(){
                   alert($(this).val());
               });*/
               function confirm_sla(){                  
                  var row_data=$('#row_data').html();
                  if(row_data == ""){
                     location.reload();
                  }else{
                     $('#sla_confirm').modal('hide');
                  }
               }
            </script>            
            <!--<script type="text/javascript" src="assets/global/plugins/checkbox.js" ></script>-->
         </body>
      </html>