<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php
               $company_id=$this->session->userdata('companyid');
                     include 'assets/lib/cssscript.php'?>
            <style>
               .dt-buttons{
               display:none !important;
               }
               .sweet-alert.showSweetAlert.visible{
               z-index: 999999999 !important;
               border: 1px solid red;
               }
            </style>
         </head>
         <!-- END HEAD -->
         <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "assets/lib/manager_header.php"?>
            <!-- END HEADER -->
               <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                           <!-- BEGIN EXAMPLE TABLE PORTLET-->
                           <div class="portlet box dark col-md-6 col-md-offset-3">
                              <div class="portlet-title">
                                 <h4>Service Desk Metrix</h4>
                              </div>
                              <div class="portlet light bordered">
                                 <div class="portlet-title tabbable-line">
                                    <ul class="nav nav-tabs">
                                       <li class="active">
                                          <a href="#Manager_attendance" data-toggle="tab">Allowance</a>
                                       </li>
                                       <li>
                                          <a href="#Manager_billing" data-toggle="tab">Tax</a>
                                       </li>
                                       <li>
                                          <a href="#Manager_contract" data-toggle="tab">Call Category</a>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="tab-content" style="text-align:center">
                                    <?php
									if(!empty($wheels))
									{
										foreach($wheels as $w)
										{?> 
											<div id="Manager_attendance" class="tab-pane fade in active">
											   <form id='configuration'>
												  <div class="form-group row">
													 <label class="col-xs-6 col-form-label">Two Wheeler Per Km</label>
													 <div class="col-xs-6">
														<input class="form-control spinner input-circle allow_float" type="text" value="<?php echo $w['two_whel']?>" id="two" name="pl3">
													 </div>
												  </div>
												  <div class="form-group row">
													 <label class="col-xs-6 col-form-label">Four Wheeler Per Km</label>
													 <div class="col-xs-6">
														<input class="form-control spinner input-circle allow_float" type="text" value="<?php echo $w['four_whel']?>" id="four" name="pl3">
													 </div>
												  </div>
												  <hr>
												  </hr>
												  <div style="text-align:center" class="form-group row">
													 <div class="col-md-6 col-md-offset-3">
														<button id="send" type="button" class="btn btn-circle green btn-outline btn-sm" onClick="add_score()">Submit</button>
														<button type="button" onClick="set()" class="btn btn-circle red btn-outline btn-sm">Reset</button>
													 </div>
												  </div>
											   </form>
											</div>
											<div id="Manager_billing" class="tab-pane fade">
											   <form id='config'>
												  <div class="form-group row" style="display:none;">
													 <label class="col-xs-6 form-label" style="display:none">
													 Company ID
													 </label>
													 <div class="col-xs-6">
														<input type="text" class="form-control form-control1" id="comp_id" name="comp_id" value="<?php echo $this->session->userdata('companyid');?>" readonly>
													 </div>
												  </div>
												  <div class="form-group row">
													 <label class="col-xs-6 col-form-label">Product GST</label>
													 <div class="col-xs-6">
														<input class="form-control spinner input-circle allow_float" type="text" value="<?php echo $w['pro_gst']?>" id="prod" name="pl3">
													 </div>
												  </div>
												  <div class="form-group row">
													 <label class="col-xs-6 col-form-label">Service GST</label>
													 <div class="col-xs-6">
														<input class="form-control spinner input-circle allow_float" type="text" value="<?php echo $w['ser_gst']?>" id="serv" name="pl3">
													 </div>
												  </div>
												  <hr>
												  </hr>
												  <div style="text-align:center" class="form-group row">
													 <div class="col-md-6 col-md-offset-3">
														<button id="send" type="button" class="btn btn-circle green btn-outline btn-sm" onClick="add_tax()">Submit</button>
														<button type="button" onClick="set()" class="btn btn-circle red btn-outline btn-sm">Reset</button>
													 </div>
												  </div>
											   </form>
											</div>
                                    <?php  }
									} 
									else
									{?> 
									<div id="Manager_attendance" class="tab-pane fade in active">
										<form id='configuration'>
										   <div class="form-group row">
											  <label class="col-xs-6 col-form-label">Two Wheeler Per Km</label>
											  <div class="col-xs-6">
												 <input class="form-control spinner input-circle allow_float" type="text" value="" id="two" name="pl3">
											  </div>
										   </div>
										   <div class="form-group row">
											  <label class="col-xs-6 col-form-label">Four Wheeler Per Km</label>
											  <div class="col-xs-6">
												 <input class="form-control spinner input-circle allow_float" type="text" value="" id="four" name="pl3">
											  </div>
										   </div>
										   <hr>
										   </hr>
										   <div style="text-align:center" class="form-group row">
											  <div class="col-md-6 col-md-offset-3">
												 <button id="send" type="button" class="btn btn-circle green btn-outline btn-sm" onClick="add_score()">Submit</button>
												 <button type="button" onClick="set()" class="btn btn-circle red btn-outline btn-sm">Reset</button>
											  </div>
										   </div>
										</form>
									</div>
									<div id="Manager_billing" class="tab-pane fade">
										<form id='config'>
										   <div class="form-group row" style="display:none;">
											  <label class="col-xs-6 form-label" style="display:none">
											  Company ID
											  </label>
											  <div class="col-xs-6">
												 <input type="text" class="form-control form-control1" id="comp_id" name="comp_id" value="<?php echo $this->session->userdata('companyid');?>" readonly>
											  </div>
										   </div>
										   <div class="form-group row">
											  <label class="col-xs-6 col-form-label">Product GST</label>
											  <div class="col-xs-6">
												 <input class="form-control spinner input-circle" type="text" value="" id="prod" name="pl3">
											  </div>
										   </div>
										   <div class="form-group row">
											  <label class="col-xs-6 col-form-label">Service GST</label>
											  <div class="col-xs-6">
												 <input class="form-control spinner input-circle" type="text" value="" id="serv" name="pl3">
											  </div>
										   </div>
										   <hr>
										   </hr>
										   <div style="text-align:center" class="form-group row">
											  <div class="col-md-6 col-md-offset-3">
												 <button id="send" type="button" class="btn btn-circle green btn-outline btn-sm" onClick="add_tax()">Submit</button>
												 <button type="button" onClick="set()" class="btn btn-circle red btn-outline btn-sm">Reset</button>
											  </div>
										   </div>
										</form>
									</div>
                                    <?php 
									}?>
                                    <div id="Manager_contract" class="tab-pane fade">
                                       <div class="x_content">
                                          <div class="form-group col-md-6 col-sm-12" style="display:none">
                                             <label class="control-label col-md-5">Company ID
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="c_id1" name="c_id1" value="<?php echo $this->session->userdata('companyid');?>" readonly>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-xs-6 col-form-label">Call Category</label>
                                             <div class="col-xs-6">
                                                <input class="form-control spinner input-circle" type="text" value="" id="call" name="pl3">
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-xs-6 col-form-label">Mean Time To Repair</label>
                                             <div class="col-xs-6">
                                                <input class="form-control spinner input-circle" type="time" min="10:00" max="23:00" value="" id="mttr" name="pl3">
                                             </div>
                                          </div>
                                          <hr>
                                          <hr>
                                          <div style="text-align:center" class="form-group row">
                                             <div class="col-md-6 col-md-offset-3">
                                                <button id="send" type="button" class="btn btn-circle green btn-outline btn-sm" onClick="add_call(this.id)">Submit</button>
                                                <!--<button type="reset" class="btn btn-circle red btn-outline btn-sm">Cancel</button>-->
                                             </div>
                                          </div>
                                          <div class="table=responsive">
                                             <table class="table table-hover sample_2">
                                                <thead>
                                                   <tr>
                                                      <th style="text-align:center">Call Category</th>
                                                      <th style="text-align:center">Mean Time To Repair</th>
													  <th style="text-align:center">Action</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                      foreach ($record->result() as $row) {
                                                      ?>
                                                   <tr>
                                                      <td style="text-align:center">
                                                         <?php echo $row->call; ?>
                                                      </td>
                                                      <td style="text-align:center">
                                                         <?php echo $row->mttr; ?>
                                                      </td>
													  <td style="text-align:center">
                                                        <span>
                                                            <button class="btn btn-circle blue btn-outline btn-icon-only"  id="<?php echo $row->id ?>" onclick="edit_call(this.id);"><i class="fa fa-edit"></i></button>           
                                                            <button class="btn btn-circle red btn-outline btn-icon-only" id="<?php echo $row->id ?>" onclick="delete_call(this.id)"><i class="fa fa-trash"></i></button>
                                                        </span>
                                                     </td>
                                                   </tr>
                                                   <?php } ?>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                              </div>
                           </div>
                           </div>
						    <span class="clearfix"></span><br>
                            <div id="edit_call" class="modal fade" role="dialog" data-backdrop="static">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Update Call Category</h4>
                            <div class="error" style="display:none">
                                <label id="rowdata_1"></label>
                            </div>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" id="edit_call">
                               
                            <div class="form-group" style="display:none;">
                                    <label class="control-label col-sm-4" for="email">Id</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="call_id" name="call_id" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Call category</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="call_cat" name="call_category" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Mean time to repair:</label>
                                    <div class="col-sm-8">
                                       
                                        <input class="form-control spinner input-circle" type="time" value="" id="meantime" name="meantime">
                                    </div>
                                </div>
                              
                              
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-circle blue btn-outline" id="update_cal"><i class="fa fa-check"></i> Update </button>
                            <button type="button" class="btn btn-circle red btn-outline" id="cancel_edit" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
                        </div>
                     </div>
                     <!-- END EXAMPLE TABLE PORTLET-->
                  </div>
            <?php include 'assets/lib/footer.php';?>
            </div>
			
            <?php include 'assets/lib/javascript.php';?>
            <script>
               $('.nav.navbar-nav').find('.open').removeClass('open');
               $('#man_metrics').addClass('open active');
 	           $('#man_serv').addClass('open');
            </script>
            <script>
           


             var el = $('.allow_float');
   el.prop("autocomplete",false); // remove autocomplete (optional)
   el.on('keydown',function(e){
	var allowedKeyCodesArr = [9,96,97,98,99,100,101,102,103,104,105,48,49,50,51,52,53,54,55,56,57,8,37,39,109,189,46,110,190];  // allowed keys
	if($.inArray(e.keyCode,allowedKeyCodesArr) === -1 && (e.keyCode != 17 && e.keyCode != 86)){  // if event key is not in array and its not Ctrl+V (paste) return false;
		e.preventDefault();
	} else if($.trim($(this).val()).indexOf('.') > -1 && $.inArray(e.keyCode,[110,190]) != -1){  // if float decimal exists and key is not backspace return fasle;
		e.preventDefault();
	} else {
		return true;
	};  
}).on('paste',function(e){  // on paste
	var pastedTxt = e.originalEvent.clipboardData.getData('Text').replace(/[^0-9.]/g, '');  // get event text and filter out letter characters
	if($.isNumeric(pastedTxt)){  // if filtered value is numeric
		e.originalEvent.target.value = pastedTxt;
		e.preventDefault();
	} else {  // else 
		e.originalEvent.target.value = ""; // replace input with blank (optional)
		e.preventDefault();  // retur false
	};
});

                              window.history.forward();
                              $(document).ready(function() {
                                  //$('.toggle').toggles({text:{on:'HI',off:'BYE'}});
                                  $(".datatable").DataTable({
                                      dom: "Bfrtip",
                                      buttons: [{
                                          extend: "copy",
                                          className: "btn-sm"
                                      }, {
                                          extend: "csv",
                                          className: "btn-sm"
                                      }, {
                                          extend: "excel",
                                          className: "btn-sm"
                                      }, {
                                          extend: "pdfHtml5",
                                          className: "btn-sm"
                                      }, {
                                          extend: "print",
                                          className: "btn-sm"
                                      }]
                                  });
                              });
                          
            </script>
            <script type="text/javascript">
               function update_revenue_1(id) {
                   $('#id_1').val(id);
                   $('#update_revenue').modal('show');
                   /* $.ajax({
                   	url: "<?php echo base_url();?>" + "index.php?/controller_manager/updateserice",
                   	type: 'POST',
                   	data: {'id':id},
                   	//dataType: "json",
                   	success: function(data) {
               
                   		//alert(data);	
                   		 location.reload();
                   	}
                   }); */
               }
               function edits(){
               //$('#id2').val(id);						
                $('#edits').modal('show');
               };
               function edit_call(id){
              
                $.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/Controller_manager/display_call_category",
                    type        :   "POST",
                    data        :   {'id':id},
                    datatype    :   "JSON",
                    cache       :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
                                        $('#call_id').val(data[0]['id']);
                                        $('#call_cat').val(data[0]['call']);
                                        $('#meantime').val(data[0]['mttr']);
                                        $('#edit_call').modal('show');
                                    },
                });
               };

                $('#update_cal').click(function(){
                $('#rowdata_1').empty();
               
                var call = $("#call_cat").val();
                var meantime = $("#meantime").val();
                var id  =$("#call_id").val();
              
				if(call!="" && meantime!="")
				{

                    var form_data = new FormData();
                    form_data.append('id',id);
                    form_data.append('call',call);
                    form_data.append('mttr',meantime);


                    $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_manager/update_cal_cat",
                        type        :   "POST",
                        data        :   form_data,// {action:'$funky'}
                        //datatype  :   "JSON",
                        contentType:false,
                        processData: false,
                        success     :   function(data){
                            data=$.trim(data);
                                            
                                            if(data == "Call category updated"){
                                         swal({
									  title: "Updated!",
									  text: data,
									  type: "success",
									  confirmButtonClass: "btn-primary",
									  confirmButtonText: "Ok.",
									  closeOnConfirm: false,
									},
									function(isConfirm) {
									  if (isConfirm) {
										  $("#edit_call").modal('hide');
										  location.reload();
									 }
									});
                                          }
							    else{
                                                $('#rowdata').append(data);
                                                $('.error').show();
                                            }
                                        },
                    });
              

          }
			

				else {
					$('#rowdata_1').append("Fill all Mandatory Fields");
			        $('#myModal1').animate({ scrollTop: 0 });
                    $('.error').show();
				}
         });
         //end update call category


           function delete_call(id){
            	swal({
            		  title: "Are you sure?",
            		  text: "You will not be able to recover this call tag",
            		  type: "warning",
            		  showCancelButton: true,
            		  confirmButtonClass: "btn-danger",
            		  confirmButtonText: "delete!",
            		  cancelButtonText: "No, Cancel!",
            		  closeOnConfirm: false,
            		  closeOnCancel: false
            		},
            		function(isConfirm) {
            		  if (isConfirm) {
            			  $.ajax({
                              url         :   "<?php echo base_url(); ?>index.php?/controller_manager/delete_call_cat",
                              type        :   "POST",
                              data        :   {'id':id},// {action:'$funky'}
                              //datatype  :   "JSON",
                              cache       :   false,
                              success     :   function(data){
                                                  if(data == 1){
                                          		    //swal("Deleted!", "Your Spare has been deleted Successfully.", "success");
													  swal({
														  title: "Call tag deleted Successfully",
														  type: "success",
														  showCancelButton: false,
														  confirmButtonClass: "btn-danger",
														  confirmButtonText: "Ok!",
														  cancelButtonText: "No, cancel plx!",
														  closeOnConfirm: false,
														  closeOnCancel: false
														},
														function(isConfirm) {
														  if (isConfirm) {
															window.location.reload();
														  }
														});
                                                  }else{
													   swal({
														  title: "Call tag has not been deleted Successfully",
														  type: "success",
														  showCancelButton: false,
														  confirmButtonClass: "btn-danger",
														  confirmButtonText: "Ok!",
														  cancelButtonText: "No, cancel plx!",
														  closeOnConfirm: false,
														  closeOnCancel: false
														},
														function(isConfirm) {
														  if (isConfirm) {
															window.location.reload();
														  }
														});
                                                	  //swal("Cancelled", "Your Spare has not been deleted Successfully:", "error");
                                                  }
                              				}
                              })
            		  }else{
                    	  swal.close();
                      }
            		});
            }
            //del cal category
               
               function hover_spare(ticket_id, requested_spare) {
                   $('#tbdy_inventory1').empty();
                                  
                   for (i = 0; i < data.length; i++) {
                       $('#tbdy_inventory1').append('<tr><td>' + data[i].Spare_code + '</td><td>' + data[i].quantity + '</td><td>' + data[i].spare_source + '</td></tr>');
               
                   }
                   $('#myModal').modal('show');
               }
               
                             function add_score() {
                   var two = $("#two").val();
                   var four = $("#four").val();
               if(two == "" || four == "")
               {
               swal("Fill All Fields");
               }
               else
               {
                   $.ajax({
                       url: "<?php echo base_url();?>" + "index.php?/Controller_manager/Service_disc",
                       type: 'POST',
                       data: {
                           'two': two,
                           'four': four
                       },
                       success: function(data) {
               $("#two").empty();
               $("#four").empty();      
                           //alert(data);
                           swal({
               title: "Good job!",
               text: "Successfully Added",
               type: "success",
               confirmButtonClass: "btn-primary",
               confirmButtonText: "Ok.",
               closeOnConfirm: false,
               },
               function(isConfirm) {
               if (isConfirm) {
               window.location.reload();
               }
               });
                      		 }
                   	});
               }
               }
               
               function add_tax() {
                   var prod = $('#prod').val();
                   var serv = $('#serv').val();
                   var cid = $('#comp_id').val();
               if(prod == "" || serv == "" || cid=="")
               {
               swal("Fill All Fields");
               }
               else
               {
                   $.ajax({
                       url: "<?php echo base_url();?>" + "index.php?/Controller_manager/Service_tax",
                       type: 'POST',
                       data: {
                           'prod': prod,
                           'serv': serv,
                           'cid': cid
                       },
                       success: function(data) {
                           $("#prod").empty();
                           $("#serv").empty(); 
               if(data="Added successfully")
               {
                             swal({
               title: "Good job!",
               text: "Successfully Added",
               type: "success",
               confirmButtonClass: "btn-primary",
               confirmButtonText: "Ok.",
               closeOnConfirm: false,
               },
               function(isConfirm) {
               if (isConfirm) {
               window.location.reload();
                               	 window.location.href="<?php echo base_url();?>"+"index.php?/controller_manager/score#Manager_billing"; 
               }
               });
               }
               else{
               swal(data);
               }
                       }
                   });
               }
               }
               function add_call(id) {
               var c_id=$('#c_id1').val();
                   var call = $('#call').val();
                   var mttr = $('#mttr').val();
               if(call=="" || mttr=="" || c_id == ""){
               swal("Fill All Fields");
               }
               else
               {
                   $.ajax({
                       url: "<?php echo base_url();?>" + "index.php?/Controller_manager/Service_call",
                       type: 'POST',
                       data: {
               'c_id':c_id,
                           'call': call,
                           'mttr': mttr
                       },
                       success: function(data) {
                            $("#call").val("");
                           $("#mttr").val(""); 
                               swal({
               title: "Good job!",
               text: "Successfully Added",
               type: "success",
               confirmButtonClass: "btn-primary",
               confirmButtonText: "Ok.",
               closeOnConfirm: false,
               },
               function(isConfirm) {
               if (isConfirm) {
               window.location.reload();
                 window.location.href="<?php echo base_url();?>"+"index.php?/controller_manager/score#Manager_contract"; 
               }
               });
                              
                       }
                   });
               }
               }
               
            </script>
            <script>
               /*$('[data-toggle=confirmation]').confirmation({
               rootSelector: '[data-toggle=confirmation]',
               // other options
               });*/
            </script>
            <script>
               var resizefunc = [];
            </script>
            <script type="text/javascript">
               $(document).ready(function() {
                   //$('#datatable').DataTable();
                   /* $('#datatable-keytable').DataTable( { keys: true } );
                   $('#datatable-responsive').DataTable();
                   $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                   var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } ); */
               });
               //TableManageButtons.init();
            </script>
            <script>
               function accepts(id) {
                   $.ajax({
                       url: "<?php echo base_url();?>" + "index.php?/controller_manager/statuschange",
                       type: 'POST',
                       data: {
                           'id': id
                       },
                       //dataType: "json",
                       success: function(data) {
                           alert(data);
                           location.reload();
                           $('#tbl_view').html('');
                          
                           if (data.length < 1) {
                               $('#tbl_view').html('<tr><td colspan=9><b><i>No records found</i></b></td></tr>');
                           } else {
                               for (i = 0; i < data.length; i++) {
                                   var cat_name = data[i].cat_name;
                                   var cat_name = cat_name.replace(/ /g, ":");
                                   $('#tbl_view').append('<tr><td id="' + data[i].ticket_id + '" style="cursor: pointer;" onclick=hover_ticket(this.id,"' + data[i].customer_name + '","' + data[i].location + '","' + data[i].product_name + '","' + data[i].cat_name + '","' + problem + '","' + data[i].priority + '");>' + data[i].ticket_id + '</td><td id="' + data[i].tech_id + '" style="cursor: pointer;" onclick=hover(this.id,"' + data[i].first_name + '","' + data[i].email_id + '","' + data[i].contact_number + '","' + data[i].skill_level + '","' + data[i].today_task_count + '");>' + data[i].tech_id + '</td><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td>' + data[i].spare_code + '</td><td>' + data[i].quantity + '</td><td>' + data[i].spare_source + '</td></tr>');
                               }
                           }
               
                       }
                   });
               }
               $(document).delegate("#send", "vclick", function() {
                   alert($("#first_time").val());
               });
               
               function Rejects(id) {
                   $.ajax({
                       url: "<?php echo base_url();?>" + "index.php?/controller_manager/rejectstatus",
                       type: 'POST',
                       data: {
                           'id': id
                       },
                       //dataType: "json",
                       success: function(data) {
               
                           alert(data);
                           location.reload();
                       }
                   });
               }
               
               function hover_tech(tech_id, first_name, Skill_level, Product_name, cat_name) {
                   cat_name = cat_name.replace(/\:/g, " ");
                   $('#modal_tech form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Technician ID</label><div class="col-sm-6 control-label">' + tech_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Name</label><div class="col-sm-6 control-label">' + first_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Skill Level</label><div class="col-sm-6 control-label">' + Skill_level + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Product Category </label><div class="col-sm-6 control-label">' + Product_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Sub Category</label><div class="col-sm-6 control-label">' + cat_name + '</div></div>');
                   $('#myModal1').modal('show');
               }
               
               function hover_tic(ticket_id, cus_id, cus_name, amc_id, model) {
               
                   $('#modal_tic form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Ticket ID</label><div class="col-sm-6 control-label">' + ticket_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Customer Id</label><div class="col-sm-6 control-label">' + cus_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Customer Name</label><div class="col-sm-6 control-label">' + cus_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Amc Id </label><div class="col-sm-6 control-label">' + amc_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Model</label><div class="col-sm-6 control-label">' + model + '</div></div>');
                   $('#myModa2').modal('show');
               }
               
               function hover_spare(ticket_id, requested_spare) {
                   $('#spare_details').empty();
                   var data = JSON.parse(requested_spare);
                   console.log(data);
               
                   for (i = 0; i < data.length; i++) {
                       $('#spare_details').append('<tr><td>' + data[i].Spare_code + '</td><td>' + data[i].quantity + '</td><td>' + data[i].spare_source + '</td></tr>');
               
                   }
                   /*$('#modal_display').html('<table><thead><th>Spare Code</th><th>Spare Required</th><th>Spare Source</th></thead>');
               for(i=0; i<data.length; i++)
               {
               $('#modal_display').append('<tbody><tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr></tbody>');
               
               }
               $('#modal_display').append('</table>');*/
                   $('#myModal').modal('show');
               
               }
               
               function hover_ticket(ticket_id, customer_name, location, product_name, cat_name, problem, priority) {
                   var replaceSpace = problem;
                   problem = problem.replace(/\:/g, " ");
                   var location = location;
                   location = location.replace(/\:/g, " ");
                   var product_name = product_name;
                   product_name = product_name.replace(/\:/g, " ");
                   var cat_name = cat_name;
                   cat_name = cat_name.replace(/\:/g, " ");
                   $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 control-label"for="inputEmail3">Ticket ID</label><div class="col-sm-4 control-label">' + ticket_id + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Customer Name</label><div class="col-sm-4 control-label">' + customer_name + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Location</label><div class="col-sm-4 control-label">' + location + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >-Category</label><div class="col-sm-4 control-label">' + product_name + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Sub-Category</label><div class="col-sm-4 control-label">' + cat_name + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Problem</label><div class="col-sm-4 control-label">' + problem + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Priority</label><div class="col-sm-4 control-label">' + priority + '</div></div></div>');
                   $('#myModal').modal('show');
               }
               
               var company_id = "<?php echo $company_id;?>";
               
               function viewimage(id) {
                   $.ajax({
                       url: "<?php echo base_url();?>" + "index.php?/controller_manager/view",
                       type: 'POST',
                       data: {
                           'id': id
                       },
                       //dataType: "JSON",
                       success: function(data) {
                           var data = JSON.parse(data);
                           //console.log(data['view']);	
                           $("#token").attr('src', data['view']);
                           $("#myModal").modal('show');
               
                       }
                   });
               }
            </script>
            <script type="text/javascript">
               //$('.datatable').dataTable();
               $('#datatable-keytable').DataTable({
                   keys: true
               });
               $('#datatable-responsive').DataTable();
               $('#datatable-scroller').DataTable({
                   ajax: "assets/plugins/datatables/json/scroller-demo.json",
                   deferRender: true,
                   scrollY: 380,
                   scrollCollapse: true,
                   scroller: true
               });
               var table = $('#datatable-fixed-header').DataTable({
                   fixedHeader: true
               });
            </script>
            <script>
               function set(){
               	$('#configuration')[0].reset();
               	$('#config')[0].reset();
               }
            </script>
         </body>
      </html>