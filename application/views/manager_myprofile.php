<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head><meta http-equiv="Content-Type" content="text/html; charset=WINDOWS-1252">
            
            <?php 
include 'assets/lib/cssscript.php'?>
          <style>
            span.help-block {
                color: red !important;
            }
            </style>
         </head>
         <!-- END HEAD -->
         <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/manager_header.php"?>
               <!-- END HEADER -->
               <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">

                     <!-- BEGIN PAGE BASE CONTENT -->
                     <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN PROFILE SIDEBAR -->
                                <div class="col-md-3">
                                    <!-- PORTLET MAIN -->
                                    <div class="portlet light profile-sidebar-portlet text-center">
                                        <!-- SIDEBAR USERPIC -->
                                        <div class="profile-userpic profile_img">
                                            <img src="<?php echo base_url() ?>/assets/pages/media/profile/profile_image.jpeg" class="img-responsive" alt=""> </div>
                                        <!-- END SIDEBAR USERPIC -->
                                        <!-- SIDEBAR USER TITLE -->
                                        <div class="profile-data">
                                            <div class="profile-usertitle-name"> <?php echo $this->session->userdata('username');?> </div>
                                            <div class="profile-usertitle-job">  <?php echo $this->session->userdata('role'); ?>  </div>
                                        </div>
                                        <!-- END SIDEBAR USER TITLE -->
                                      
                                    </div>
                                    <!-- END PORTLET MAIN -->
                                </div>
                                <!-- END BEGIN PROFILE SIDEBAR -->
                                <!-- BEGIN PROFILE CONTENT -->
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light ">
                                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                    </div>
                                                    
                                                </div>
                                             <div class="portlet-body">
                                                    <form role="form" id="man_profileform" name="man_profileform" action="#" class="form-horizontal">
                                                                <div class="form-group" style="display: none;">
                                                                    <label class="control-label col-md-3" style="text-align: left;">Company</label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="c_id" name="c_id" value="<?php echo $this->session->userdata('companyid');?>" readonly /></div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">Role <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" readonly class="form-control" id="user_role" name="user_role" value="<?php echo $this->session->userdata('role'); ?>"/> </div></div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">Employee ID
                                                                      <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" readonly placeholder="Marcus" class="form-control" id="profile_id" name="profile_id" value="<?php echo $employee_id; ?>" required/> </div></div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">First Name   <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="fname" name="fname" value="<?php echo $first_name; ?>" required/> </div></div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">Last Name  <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="lname" name="lname" value="<?php echo $last_name; ?>" required/> </div></div>

                                                                 <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">Contact Number  <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="c_number" name="c_number" value="<?php echo $contact_number; ?>" /> </div></div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">Alternate Number</label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="alt_num" name="alt_num" value="<?php echo $alternate_number; ?>" /></div></div>                                                            
                                                               
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">Email  <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" id="profile_email" name="profile_email" class="form-control" value="<?php echo $this->session->userdata('session_username');?>" required/> 
                                                                    </div></div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">Region  <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" readonly id="region" name="region" class="form-control" value="<?php echo $region; ?>" required/> 
                                                                    </div></div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">PM Calls Target <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <input type="text" id="pm_calls_target" name="pm_calls_target" class="form-control" value="<?php echo $pm_calls_target; ?>" required/> 
                                                                    </div></div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3" style="text-align: left;">CTPL Technician<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <select name="ctpl_technician" id="ctpl_technician" class="form-control" required> 
                                                                    <!-- <option value="<?php echo $ctpl_technician; ?>" selected><?php echo $ctpl_technician; ?></option> -->
                                                                    </select>
                                                                    </div></div>

                                                                <div class="form-group" style="display: none;">
                                                                    <label class="control-label col-md-3" style="text-align: left;">Old Email  <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <input type="mail" id="old_usremail" name="old_usremail" class="form-control" value="<?php echo $this->session->userdata('session_username');?>" readonly required/> 
                                                                    </div></div>

                                                               <!--  <div class="form-group">
                                                                    <label class="control-label col-md-3">About</label>
                                                                    <div class="col-md-9">
                                                                    <textarea class="form-control" rows="3" placeholder=" "></textarea></div>
                                                                </div> -->
                                                               
                                                                <div class="pull-right">
                                                                    <a href="javascript:;" class="btn green btn-circle btn-outline" id="manprofile_submit" value="Submit"> Save Changes </a>
                                                                </div>
                                                            </form>
                                                            <span class="clearfix"></span><br>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PROFILE CONTENT -->
                            </div>
                        </div>
                     
                     <!-- END PAGE BASE CONTENT -->
                  </div>
                  
               </div>
			   </div>
			   <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
            </div>
            <!-- END CONTAINER -->         
            <?php include 'assets/lib/javascript.php'?>  

                <script>
             $(document).ready(function () {

                var company_id="<?php echo $this->session->userdata('companyid'); ?>";
                var email_id = "<?php echo $this->session->userdata('session_username'); ?>";
                var emp_id = $("#profile_id").val();


                $.ajax({ // to append the selected technician value in dropdown
                    url: "<?php echo base_url();?>" + "index.php?/Login/get_ctpl_technician",
                    type: 'POST',
                    data:{'email_id':email_id},
                    success: function(result) {
                            var result=JSON.parse(result);
                            console.log(result);
                            var tech_name = (result[0]['first_name'])+' '+(result[0]['last_name']);
                            $('#ctpl_technician').append('<option value='+result[0]['ctpl_technician']+' selected>'+tech_name+'</option>');
                            
                    }
                });

                $.ajax({ // to list the technician values 
                    url: "<?php echo base_url(); ?>" + "index.php?/controller_call/modal_technician",
                    type: 'POST',
                    data:{'company_id':company_id},
                    success: function(result) {
                    var result=JSON.parse(result);
                      console.log(result);
                         for(i=0;i<result.length;i++)
                         {
                            var tech_name = (result[i]['first_name'])+' '+(result[i]['last_name']);
                            $('#ctpl_technician').append('<option value='+result[i]['technician_id']+' >'+tech_name+'</option>');
                         }
                }
            });
           

         });
              var formname = "#man_profileform";    
             jQuery.validator.addMethod("notEqual", function(value, element, param) {
                    return this.optional(element) || value != param;
                }, "Please specify a different (non-default) value");

             jQuery.validator.addMethod("validEmail", function(value, element) 
                {
                    if(value == '') 
                        return true;
                    var temp1;
                    temp1 = true;
                    var ind = value.indexOf('@');
                    var str2=value.substr(ind+1);
                    var str3=str2.substr(0,str2.indexOf('.'));
                    if(str3.lastIndexOf('-')==(str3.length-1)||(str3.indexOf('-')!=str3.lastIndexOf('-')))
                        return false;
                    var str1=value.substr(0,ind);
                    if((str1.lastIndexOf('_')==(str1.length-1))||(str1.lastIndexOf('.')==(str1.length-1))||(str1.lastIndexOf('-')==(str1.length-1)))
                        return false;
                    str = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/;
                    temp1 = str.test(value);
                    return temp1;
                }, "Please enter valid email.");

                $(formname).validate({
                doNotHideMessage:!0,errorElement:"span",errorClass:"help-block help-block-error",focusInvalid:!1,
                rules: {
                    profile_id:{required:!0},
                    profile_email:{required:!0,validEmail:!0},
                    c_number:{digits:!0,minlength:10,maxlength:10,required:!0},   
                    alt_num:{digits:!0,minlength:10,maxlength:10,notEqualTo:"#c_number"},
                    fname:{required:!0},
                    lname:{required:!0},
                   // received_date   :{required:!0},
                },
            
            });  

            $("#manprofile_submit").click(function(){  
                   
                if ($(formname).valid()){
                    $(formname+" button").removeAttr("disabled");
                    $.ajax({
                type:"POST",
                url: "<?php echo base_url();?>"+"index.php?/Login/submit_profile",
                data: $("#man_profileform").serialize() ,
                success:function(data){
                    console.log(data);
                      data=$.trim(data);  
                                 
             if(data=="Provide Proper Mobile Number or/and Email Id")  {
                          // alert ("Provide Proper Mobile Number or/and Email Id")
                            swal("Provide Proper Contact Number");
                        }
              else if(data=="All Fields are Mandatory")  {
                              //alert("All Fields are Mandatory");
                                swal("All Field Are Mandatory")
                        }
              else if(data=="Duplication occured")  {
                      swal("Duplicate entry, Check Company Name & Admin Details");
               }
               else if(data=="Details Updated!!")
              {
                    //swal("success","Details Updated!!");    
                    swal(
                      'Updated!',
                      'Details Saved!',
                      'success'
                    )    
              }
              else {
                  swal(data);
              }
            
                   }
               });

                }
                else{
                    //swal("Form not valid");
                    $(formname+" button").attr('disabled', 'disabled');
                }
            });
            </script>
   </body>
</html>