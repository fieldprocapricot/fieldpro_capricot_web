<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php include 'assets/lib/cssscript.php'?>
		
			 	  <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
	
            <style>
               <!-- .sweet-alert.showSweetAlert.visible{
                  z-index:99999999 !important;
                  }-->
               .fileinput-new, .fileinput-exists {
                 color: #000;
               }
               .sweet-alert.showSweetAlert.visible{
                 border:1px solid red;
               }
              /* .dt-buttons{
                 display:none !important;
               }*/
               /* .dataTables_filter
               {
                 text-align-last: right;
               } */
                 .uneditable-input {
min-width:auto !important;
}
.form-control.uneditable-input.input-fixed {
    max-width: 233px !important;
}
@media screen and (max-width: 540px) {
.form-control.uneditable-input.input-fixed {
    max-width: 163px !important;
}
}
input .control-label .required, .form-group .required {
    color: black;
}
span .required
{
    color: red;
}
span.help-block-error {
    color: red !important;
}
				.custome-dt-button .dt-buttons {
    margin-top: -120px !important;
    border: 1px solid #fff;
    border-radius: 30px !important;
}
.custome-dt-button .dt-buttons a {
color:#fff !important;
	line-height: 14px !important;
}
            </style>
         </head>
         <!-- END HEAD -->
          <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/manager_header.php"?>
               <!-- END HEADER -->
               <div class="page-container">
               <div class="page-sidebar-wrapper">
               <?php include "assets/lib/manager_sidebar.php"?>
            </div>
               <div class="page-content-wrapper">
                  <div class="page-content">
            <?php
                    $fromdate =date("d/m/Y");						
                $todate =date("d/m/Y");	
                        ?>
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">
                                    CTPL KRA KPI Report
                                   <!-- <?php echo $webPageheading; ?> -->
                                    </div>
                                    <div class="actions">
                                </div>
                                 </div>
                                 <div class="portlet-body">
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                         <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 fltr_btn  pull-right" style="padding-top: 15px;">
                                            <button id="filter_submit" class="btn btn-circle blue btn-outline filter_submit">Submit
                                            </button>
                                        </div>

                                    <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                        <b>To :</b>  <input name="enddate" id="enddate" type="text" value="<?php echo $todate; ?>" placeholder="From Date" data-date-format="dd/mm/yyyy" class=" required form-control textdate textbox-bg-shadow from_filter" style="border-radius: 0px !important;" />
                                    </div>
						            <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
							            <b>From :</b>  <input name="startdate" id="startdate" type="text" value="<?php echo $fromdate; ?>" placeholder="From Date" data-date-format="dd/mm/yyyy" class=" required form-control textdate textbox-bg-shadow to_filter" style="border-radius: 0px !important;" />
                                    </div>
                                    </div>
                                    <br><br><br>
										

                                    <div class="table=responsive custome-dt-button">
										
                           	        <table class='table table-striped table-bordered table-hover load_data' id='clienttable'>
                                        <thead> 
                                            <tr>
                                                <th class="text-center" colspan="2">Technician List</th>
                                                <th class="text-center" colspan="3">MTR</th>
                                                <th class="text-center" colspan="3">MTC</th>
                                                <th class="text-center" colspan="3">Customer Feedback</th>
                                            </tr>
                                                        
                                            <tr>
                                                <th class="text-center">S.No </th>
                                                <th class="text-center">Technician Name</th>

                                                <th class="text-center">Target</th>  <!--MTR-->
                                                <th class="text-center">Actual Value</th>
                                                <th class="text-center">Average</th>

                                                <th class="text-center">Target</th> <!--MTC-->
                                                <th class="text-center">Actual Value</th>
                                                <th class="text-center">Average</th>

                                                <th class="text-center">Target</th> <!--Customer Feedback-->
                                                <th class="text-center">Actual Value</th>
                                                <th class="text-center">Average</th>

                                                
                                            </tr>

                                        </thead>
                                                 
                                        <tbody align="center">
                                                     
                                        </tbody>
                                    </table>
                                    </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>


                     <!-- END PAGE BASE CONTENT -->
                        <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                  </div>
            <!--Modal Starts-->
            <!-- Modal -->
           
           
            <!-- Modal -->
           
            <!--Modal End-->
          
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>
		
			  <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script>
    var startdate =  $('#startdate').val();
    var enddate =  $('#enddate').val();
    var technician =  $('#technician').val();
    var table = $('#clienttable');
			
        var otable= table.dataTable({
        autoWidth: false,
        bSort : false,
	    "ajax": {
		 	"type": "POST",
            url : "<?php echo base_url();?>index.php/ctpl_kra_kpi_reports/ctpl_kra_kpi_list_all/",
          	   "data": function(d){
                    d.startdate=startdate;
                    d.enddate=enddate;
                    d.technician=technician;
                        }
                 },

            "aoColumnDefs": [{
                "aTargets": [2,3],
                "defaultContent": "",
                }],
		  
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },


		    dom: 'lBfrtip',
            buttons: [
       
                { extend: 'excel', text: 'Excel Export', filename:'CTPL KRA KPI Report', className: 'btn btn-outline' }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: false,
            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
		    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

       
      });


        $(".filter_submit").click(function() {  // submit btn after choosing filter

            filter_list()  // filter data list

        });


function filter_list()    //  filter select datatable
{
    table = $('#clienttable');
			table.DataTable().destroy();
		
				var startdate =  $('#startdate').val();
				var enddate =  $('#enddate').val();
				var technician =  $('#technician').val();
			
		
			otable= table.dataTable({
		  autoWidth: false,
          bSort : false,
	 "ajax": {
			 "type": "POST",
            url : "<?php echo base_url();?>index.php/ctpl_kra_kpi_reports/ctpl_kra_kpi_list_all/",
           
       	   "data": function(d){
			   
			    d.startdate=startdate;
	d.enddate=enddate;
	d.technician=technician;

        }
        },
		  
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

		        dom: 'lBfrtip',
                buttons: [
                { extend: 'excel', text: 'Excel Export', filename:'CTPL KRA KPI Report', className: 'btn btn-outline ' }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: false,
            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
		    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable



       
        });

}

$(document).ready(function(){
	
      $('#startdate, #enddate').datepicker({
   language: 'nl',
   orientation: 'auto bottom'
   }).on('show', function () {
   $('.datepicker-orient-bottom').removeClass('datepicker-orient-bottom');
   $('.datepicker-dropdown').addClass('datepicker-orient-top');
   });		
      
   $('#startdate').datepicker('setDate', new Date());
      $('#enddate').datepicker('setStartDate', new Date());
      $("#enddate").datepicker({ defaultDate: new Date() });
      $('#enddate').datepicker('setDate', new Date());
      $('#startdate').datepicker('setEndDate', new Date());

      
      $("#startdate").datepicker({
         todayBtn:  1,
         autoclose: true,
      }).on('changeDate', function (selected) {
         var minDate = new Date(selected.date.valueOf());
         // var maxDate = new Date();
         
      
         $('#enddate').datepicker('setStartDate', minDate);
         // $('#enddate').datepicker('setEndDate', maxDate);
         $('.datepicker').hide();
      });
      
      $("#enddate").datepicker()
         .on('changeDate', function (selected) {
               var minDate = new Date(selected.date.valueOf());
         $('.datepicker').hide();
         });	
		
 });


</script>
           
</body>
</html>