<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
			 
			 <?php 
               $company_id=$this->session->userdata('companyid');
               $region=$user['region'];
			 $area=$user['area'];
			 $location=$user['location'];
		?>
			     <?php include 'assets/lib/cssscript.php'; ?>
            <link href="assets/global/plugins/chart-graph/nv.d3.css" rel="stylesheet" type="text/css">
			 
        
		
		 <style>
              
         /* html, body, #chart1, svg {
               margin: 0px !important;
               padding: 0px !important;
               height: 100% !important;
               width: 100% !important;
               }*/
               .highcharts-button-symbol {
               display: none;
               }
               .highcharts-credits {
               display: none;
               }
               #preloader {
               position: fixed;
               top: 0;
               left: 0;
               right: 0;
               bottom: 0;
               background-color: #fff;
               /* change if the mask should have another color then white */
               z-index: 99;
               /* makes sure it stays on top */
               }
               #status {
               width: 200px;
               height: 200px;
               position: absolute;
               left: 50%;
               /* centers the loading animation horizontally one the screen */
               top: 50%;
               /* centers the loading animation vertically one the screen */
               background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
               /* path to your loading animation */
               background-repeat: no-repeat;
               background-position: center;
               margin: -100px 0 0 -100px;
               /* is width and height divided by two */
               }
               .highcharts-button {
               display: none;
               }
            </style>
            <style>
               .amcharts-chart-div a {
               display: none !important;
               }
               .piechart5 {
               margin: -12% 8%;
               }
               @media screen and (min-width:1600px) and (max-width:2100px)
               {
               .piechart5 {
               margin: -10% 20%;
               }	
               }
            </style>
         </head>
         <!-- END HEAD -->
          <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php //include "assets/lib/header.php"?>
			 <?php include "assets/lib/manager_header.php"?>	
               <!-- END HEADER -->
               <div class="page-container">
               <div class="page-sidebar-wrapper">
               <?php include "assets/lib/manager_sidebar.php"?>
   </div>
               <div class="page-content-wrapper">
                  <div class="page-content">

                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">
                             <?php echo $webPageheading; ?>
                                    </div>
                                    <div class="actions">
										  <button class="btn btn-circle green btn-outline btn-sm" id="calltrend3export">
                                            <i class="fa fa-pencil"></i> Export </button>



                                        <button class="btn btn-circle green btn-outline btn-sm" id="calltrend3print">
                                            <i class="fa fa-print"></i> Print </button>
  </div>
                                 </div>
								  </div>
                                 <div class="portlet-body">
									 	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
											  <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
												<!--<select class="form-control" id="call_trend" onChange="call_trend();" name="role1">
                                          <option value='year' selected>This Year</option>
                                       </select> -->
                                       </div>
									  
							    </div>
									 <br><br>
		           
                                    <div class="table=responsive custome-dt-button">
					  
                            
                                 
                                    <div class="gallery" id="call_trend_graph" style="height: 345px !important;">
                                       <svg></svg>
                                    </div>
                                 
                              
                           
                              </div>
                                 
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>


                     <!-- END PAGE BASE CONTENT -->
                        <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                  </div>
     
          
            <!-- END QUICK SIDEBAR -->
		  <div id="preloader">
               <div id="status">&nbsp;</div>
            </div>
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?> 
			<script src="assets/global/plugins/chart-graph/d3.min.js" charset="utf-8"></script>
            <script src="assets/global/plugins/chart-graph/nv.d3.js"></script>
            <script src="assets/global/plugins/chart-graph/stream_layers.js"></script>
            <!-- C-sat Insight Plugin-->
            <script src="assets/global/plugins/chart-graph/highcharts/highcharts.js"></script>
            <script src="assets/global/plugins/chart-graph/highcharts/exporting.js"></script>
            <script>
               $('#man_call_trend').addClass('open');
               $('#status').fadeOut(); // will first fade out the loading animation 
               $('#preloader').fadeOut('slow'); 
            </script>

            <script>
			
$(document).ready(function(){
call_trend()
		
 });
				
               function call_trend()
               	{
               		var company_id="<?php echo $company_id;?>";
               		var filter=$('#call_trend').val();
					var region="<?php echo $region;?>";
               var area="<?php echo $area;?>";
               var location="<?php echo $location;?>";
            
			   //$("#status").fadeIn();
               //$("#preloader").fadeIn();
               $.ajax({
               		url: "<?php echo base_url();?>" + "index.php?/call_trend_reports/call_trend",
               		type: 'POST',
               		data: {'company_id':company_id,'filter':filter,'region':region,'area':area,'location':location},
               		dataType: "json",
               		success: function(data) {
               			//$('#call_trend_graph').empty();
             
               chart30=Highcharts.chart('call_trend_graph', {
               chart: {
				    type: 'spline',
               backgroundColor: (Highcharts.theme && Highcharts.theme.chart) || '#f1f4f7'
               },
               title: {
               text: ' '
               },
               xAxis: {
        categories:data.category,
				   allowDecimals: false
    },
    yAxis: {
        title: {
            text: 'Tickets'
        },
		allowDecimals: false
	},
				    plotOptions: {
        series: {
            dataLabels: {
                enabled: true
            }
        }
    },
               labels: {
               items: [{
               html: '',
               style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
               }
               }]
               },
               series: data.data
               });
               
               		}
               });
			   //$("#status").fadeOut();
               //$("#preloader").fadeOut();
               	}

             				
			    $("#calltrend3print").on('click', function (event) {
                //  alert("hello");
        chart30.print();

    });
                
                   $("#calltrend3export").on('click', function (event) {
                //  alert("hello");
        chart30.exportChart({
            type: "image/jpeg"
        });
    });

         </script>
           
         </body>
      </html>