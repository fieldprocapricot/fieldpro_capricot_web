<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php include 'assets/lib/cssscript.php'?>
		
			 	  <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
	
            <style>
               <!-- .sweet-alert.showSweetAlert.visible{
                  z-index:99999999 !important;
                  }-->
               .fileinput-new, .fileinput-exists {
                 color: #000;
               }
               .sweet-alert.showSweetAlert.visible{
                 border:1px solid red;
               }
              /* .dt-buttons{
                 display:none !important;
               }*/
               /* .dataTables_filter
               {
                 text-align-last: right;
               } */
                 .uneditable-input {
min-width:auto !important;
}
.form-control.uneditable-input.input-fixed {
    max-width: 233px !important;
}
@media screen and (max-width: 540px) {
.form-control.uneditable-input.input-fixed {
    max-width: 163px !important;
}
}
input .control-label .required, .form-group .required {
    color: black;
}
span .required
{
    color: red;
}
span.help-block-error {
    color: red !important;
}
				.custome-dt-button .dt-buttons {
    margin-top: -120px !important;
    border: 1px solid #fff;
    border-radius: 30px !important;
}
.custome-dt-button .dt-buttons a {
color:#fff !important;
	line-height: 14px !important;
}
            </style>
         </head>
         <!-- END HEAD -->
          <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/manager_header.php"?>
               <!-- END HEADER -->
               <div class="page-container">
               <div class="page-sidebar-wrapper">
               <?php include "assets/lib/manager_sidebar.php"?>
   </div>
               <div class="page-content-wrapper">
                  <div class="page-content">
<?php
		$fromdate =date("d/m/Y");						
	$todate =date("d/m/Y");	
			 ?>
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">
                                   <?php echo $webPageheading; ?>
                                    </div>
                                    <div class="actions">
  </div>
                                 </div>
                                 <div class="portlet-body">
									 	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                         <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 fltr_btn  pull-right" style="padding-top: 15px;">
                                                <button id="filter_submit" class="btn btn-circle blue btn-outline filter_submit">Submit
                                                </button>
                                             </div>



                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
			  <b> Technician: </b>  <select class="form-control col-lg-2 col-lg-offset-1 pull-right tech_rpt" id="technician" name="technician">
				    <option value="0">All</option>
					<?php 
                        if($technician_all->num_rows() > 0):
                        ?> 
                        <?php
                        foreach ($technician_all->result() as $row):
                        ?>
                        <option value="<?php echo $row->technician_id; ?>"><?php echo $row->first_name; ?></option>
                        <?php endforeach; endif; ?>
													
                                                      </select>
                                                   </div> 
											  <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
												  <b>To :</b>  <input name="enddate" id="enddate" type="text" value="<?php echo $todate; ?>" placeholder="From Date" data-date-format="dd/mm/yyyy" class=" required form-control textdate textbox-bg-shadow tech_rpt" style="border-radius: 0px !important;" />
                                                   </div>
						   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
							   <b>From :</b>  <input name="startdate" id="startdate" type="text" value="<?php echo $fromdate; ?>" placeholder="From Date" data-date-format="dd/mm/yyyy" class=" required form-control textdate textbox-bg-shadow tech_rpt" style="border-radius: 0px !important;" />
                                                   </div>
                                                </div>
									 <br><br><br>
										


                                    <div class="table=responsive custome-dt-button">
										
                           	<table class='table table-striped table-bordered table-hover load_data' id='clienttable'>
                                                    <thead> 
                                                        
                                                        <tr>
                                                        <th class="text-center">S.No</th>
                                                        <th class="text-center">Ticket ID</th>
                                                        <th class="text-center">Customer Name</th>
                                                        <th class="text-center">Technician Name</th>
                                                        <th class="text-center">Call Type</th>
                                                        <th class="text-center">Product</th>
                                                        <th class="text-center">Sub Catogory</th>
                                                        <th class="text-center">Serial No</th>
                                                        <th class="text-center">Ticket Raised Date and Time</th>
                                                        <th class="text-center">Ticket Assigned Date and Time</th>
                                                        <th class="text-center">Ticket Accepted Date and Time</th>
                                                        <th class="text-center">Ticket Start date and time</th>
                                                        <th class="text-center">Travel Start date and time</th>
                                                        <th class="text-center">Travel End date and time</th>
                                                        <th class="text-center">Date and Time of Completion</th>
                                                        <th class="text-center">Customer rating</th>
                                                        <th class="text-center">NPS feedback</th>
                                                        <th class="text-center">Problem Description</th>
                                                        <th class="text-center">Resolution summary</th>
                                                        <th class="text-center">Total Time</th>
                                                        <th class="text-center">Turn Around time</th>
                                                        <th class="text-center">Status</th>
                                                        </tr>
                                                    </thead>
                                                 
                                                    <tbody align="center">
                                                     
                      
                    
                                                    </tbody>
                                                </table>
                                    </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>


                     <!-- END PAGE BASE CONTENT -->
                        <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                  </div>

               <!--loading model-->
               <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
                <p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
                    </div>
                            </div>
                </div>
                <!-- end loading model-->
          
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>
		
			  <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

            <script>
				var startdate =  $('#startdate').val();
				var enddate =  $('#enddate').val();
				var technician =  $('#technician').val();
				var table = $('#clienttable');
			
			var otable= table.dataTable({
		  autoWidth: false,
          bSort : false,
	 "ajax": {
		 	"type": "POST",
            url : "<?php echo base_url();?>index.php/technician_reports/technician_report_all/",
          	   "data": function(d){
	 d.startdate=startdate;
	d.enddate=enddate;
	d.technician=technician;
        }
        },
		  
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
                
            },

		// dom: 'lBfrtip',
              buttons: [
             //   { extend: 'print', className: 'btn btn-outline' },
               
             //   { extend: 'pdf', className: 'btn btn-outline' },
                { extend: 'excel', text: 'Excel Export', filename:'Technician Report', className: 'btn btn-outline ' }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: false,
            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
		    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable



       
        });
	
		$(".filter_submit").click(function() {
			table = $('#clienttable');
			table.DataTable().destroy();
            // $('#Searching_Modal').modal('show');
		
				var startdate =  $('#startdate').val();
				var enddate =  $('#enddate').val();
				var technician =  $('#technician').val();	
		
			otable= table.dataTable({
		  autoWidth: false,
          bSort : false,

	 "ajax": {
			 "type": "POST",
            url : "<?php echo base_url();?>index.php/technician_reports/technician_report_all/",
           
       	   "data": function(d){
			   
			    d.startdate=startdate;
                d.enddate=enddate;
                d.technician=technician;
        },
        // "success":function(d){
        //     $('#Searching_Modal').modal('hide');
        //     table.show();
        // }
        },
		  
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"

            },

		// dom: 'lBfrtip',
              buttons: [
             //   { extend: 'print', className: 'btn btn-outline' },
               
             //   { extend: 'pdf', className: 'btn btn-outline' },
                { extend: 'excel', text: 'Excel Export', filename:'Technician Report', className: 'btn btn-outline ' }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: false,
            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
		    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable



       
        });


}); 
		
$(document).ready(function(){
	
	$('#startdate, #enddate').datepicker({
  language: 'nl',
  orientation: 'auto bottom'
}).on('show', function () {
  $('.datepicker-orient-bottom').removeClass('datepicker-orient-bottom');
  $('.datepicker-dropdown').addClass('datepicker-orient-top');
});		
	
  $('#startdate').datepicker('setDate', new Date());
	 $('#enddate').datepicker('setStartDate', new Date());
	$("#enddate").datepicker({ defaultDate: new Date() });
	$('#enddate').datepicker('setDate', new Date());
	$('#startdate').datepicker('setEndDate', new Date());
    $("#startdate").datepicker({
        todayBtn:  1,
        autoclose: true,
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
		// var maxDate = new Date();
		
	
        $('#enddate').datepicker('setStartDate', minDate);
		// $('#enddate').datepicker('setEndDate', maxDate);
		$('.datepicker').hide();
    });
    
    $("#enddate").datepicker()
        .on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
         //$('#startdate').datepicker('setEndDate', minDate);
		 // $('#startdate').datepicker('setEndDate', minDate);
		$('.datepicker').hide();
        });	
		
 });
			

         </script>
           
         </body>
      </html>