<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php include 'assets/lib/cssscript.php'?>
            <style>
               <!-- .sweet-alert.showSweetAlert.visible{
                  z-index:99999999 !important;
                  }-->
               .fileinput-new, .fileinput-exists {
                 color: #000;
               }
               .sweet-alert.showSweetAlert.visible{
                 border:1px solid red;
               }
              /* .dt-buttons{
                 display:none !important;
               }*/
               /* .dataTables_filter
               {
                 text-align-last: right;
               } */
                 .uneditable-input {
min-width:auto !important;
}
.form-control.uneditable-input.input-fixed {
    max-width: 233px !important;
}
@media screen and (max-width: 540px) {
.form-control.uneditable-input.input-fixed {
    max-width: 163px !important;
}
}
input .control-label .required, .form-group .required {
    color: black;
}
span .required
{
    color: red;
}
span.help-block-error {
    color: red !important;
}
				.custome-dt-button .dt-buttons {
    margin-top: -120px !important;
    border: 1px solid #fff;
    border-radius: 30px !important;
}
.custome-dt-button .dt-buttons a {
color:#fff !important;
	line-height: 14px !important;
}
            </style>
         </head>
         <!-- END HEAD -->
          <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header.php"?>
               <!-- END HEADER -->
               <div class="page-container">
               <div class="page-sidebar-wrapper">
               <?php include "assets/lib/manager_sidebar.php"?>
				   
				   
               </div>
               <div class="page-content-wrapper">
                  <div class="page-content">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">
                                   <?php echo $webPageheading; ?>
                                    </div>
                                    <div class="actions">
  </div>
                                 </div>
                                 <div class="portlet-body">
									 	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                               
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
               <select class="form-control col-lg-2 col-lg-offset-1 pull-right service_grp" id="service_group" name="service_group">
					<?php 
                        if($service_group->num_rows() > 0):
                        ?> 
                        <?php
                        foreach ($service_group->result() as $row):
                        ?>
                        <option value="<?php echo $row->service_group_id; ?>"><?php echo $row->service_group; ?></option>
                        <?php endforeach; endif; ?>
													
                                                      </select>
                                                   </div> 
												 
                                                </div>
									 <br><br><br>
										

 




                                    
                                    <div class="table=responsive custome-dt-button">
										
                           	<table class='table table-striped table-bordered table-hover load_data' id='clienttable'>
                                                    <thead> 
                                                        
                                                        <tr>
															<th class="text-center">Services</th>
                                                            <th class="text-center">Calls B/O from previous week</th>
                                                            <th class="text-center">B/O calls closed this week</th>
															 <th class="text-center">Current week's Calls Logged</th>
                                                            <th class="text-center">Current week's Closed Calls</th>
                                                            <th class="text-center">B/O calls Open for this week</th>
															 <th class="text-center">Current week's Open Calls</th>
															<th class="text-center">Calls C/F</th>
                                                        </tr>
                                                    </thead>
                                                 
                                                    <tbody align="center">
                                                     
                      
                    
                                                    </tbody>
                                                </table>
                                    </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>


                     <!-- END PAGE BASE CONTENT -->
                        <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                  </div>
            <!--Modal Starts-->
            <!-- Modal -->
           
           
            <!-- Modal -->
           
            <!--Modal End-->
          
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>
            <script>
				var service_group =  $('#service_group').val();
				var table = $('#clienttable');
			
			var otable= table.dataTable({
		  autoWidth: false,
          bSort : false,
		 paging: false,
   ordering: false,
   searching: false,
	   info: false,		
	 "ajax": {
	
            url : "<?php echo base_url();?>index.php/call_reports/call_report_all/"+service_group,
           "type": "POST",
       	   "data": function(d){
	
       //     d.custno = $('#custno').val();
    	//	d.email = $('#custemail').val();
        }
        },
		  
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

		 dom: 'lBfrtip',
              buttons: [
             //   { extend: 'print', className: 'btn btn-outline' },
               
             //   { extend: 'pdf', className: 'btn btn-outline' },
                { extend: 'excel', text: 'Excel Export', filename:'Call Report', className: 'btn btn-outline ' }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,
            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
		    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable



       
        });
	
		$(".service_grp").change(function() {
			table = $('#clienttable');
			table.DataTable().destroy();
		
	var	service_group =  $('#service_group').val();	
	//	alert(service_group);	
		
			otable= table.dataTable({
		  autoWidth: false,
          bSort : false,
		 paging: false,
   ordering: false,
   searching: false,
	   info: false,		
	 "ajax": {
	
            url : "<?php echo base_url();?>index.php/call_reports/call_report_all/"+service_group,
           "type": "POST",
       	   "data": function(d){
	
       //     d.custno = $('#custno').val();
    	//	d.email = $('#custemail').val();
        }
        },
		  
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

		// dom: 'lBfrtip',
              buttons: [
             //   { extend: 'print', className: 'btn btn-outline' },
               
             //   { extend: 'pdf', className: 'btn btn-outline' },
                { extend: 'excel', text: 'Excel Export', filename:'Call Report', className: 'btn btn-outline ' }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,
            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
		    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable



       
        });


}); 
		
		$(document).ready(function(){
	
			
	//alert("hi");
	
//alert(service_group);	
        

        // handle datatable custom tools
       /* $('#sample_3_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });*/
	
		/* $('#formbtn').click(function() {
		 //alert("Hi");
	
        var oTable= table.DataTable().ajax.reload();
    });
	*/
		$('.panel-close').click(function(){
    $('.custom-content').magnificPopup('close');
 });
			
		
 });
			

         </script>
           
         </body>
      </html>