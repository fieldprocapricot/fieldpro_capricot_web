<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include 'assets/lib/cssscript.php'?>
    <style>	
	.innerline{
		margin:5px !important;
		border: 1px solid #36c6d3 !important;
	}	
	.smiley_link{
		float : left;
		margin:10px;
	}
	.smiley_link p {
		text-decoration:none !important;
		margin:0px !important;
		color:black;
		font-size:13px;
	}
	.smiley_scale{
		text-align:center;
	}
	.t5est{
		margin-left: 44px;
		
	}
	.logo_bottom{
		padding-left: 13px;
    	padding-top: 5px;
	}

		
	</style>
</head>

<body>	
	
    <div class="container">
        <div class="card card-container col-lg-8 col-lg-offset-2" style="background-color:#fff;margin-top:3%">            
            <p id="profile-name" class="profile-name-card"></p>	
  <?php if($ticketcurrentstatus!=12||$ticketcurrentstatus!='12'){?>	
            <form id="register" style="padding:2%">
				<span class="error_msg" style="color:red"><p id="error_msg"></p></span>
                <span id="reauth-email" class="reauth-email"></span>
            <div class="form-group">
                    <label for="username">Email:</label>
                    <input type="text" id="email" name="email" value="<?php echo $emailid;?>" class="form-control" readonly>
            	</div>				
                <!--<div class="form-group">
                    <label for="username">Company Name:</label>
                    <input type="text" id="c_name" name="c_name" class="form-control"readonly>
            	</div>	-->	
            	 <div class="form-group">
                    <label for="username">Ticket ID:</label>
                    <input type="text" id="ticket_id" name="ticket_id" value="<?php echo $ticket_id;?>" class="form-control" readonly>
            	</div>		
				<div class="form-group" style="display:none;">
                    <label for="username">Work type:</label>
                    <input type="text" id="work_type" name="work_type" value="<?php echo $work_type;?>" class="form-control" readonly>
            	</div>				
                <div class="form-group" style="display:none;">
                    <label for="username">Ticket ID:</label>
                    <input type="text" id="primary_key" name="primary_key" value="<?php echo $primaryid;?>" class="form-control" readonly>
            	</div>	
					
				
              	<div class="form-group">
                    <label for="password">Customer Feedback:</label>
                    <!-- <input type="text" id="password" name="password" class="form-control" placeholder="Feedback" required autofocus> -->
					<select class="form-control" id="password"  name="password" required>
         				 <option value="" >Select Feedback </option>
                        <option value="Completely Satisfied">Completely Satisfied</option>
                        <option value="Satisfied">Satisfied</option>
                        <option value="Dis-satisfied">Dis-satisfied</option>
                        <option value="No feedback">No feedback</option>
                        <option value="Not applicable">Not applicable</option>
	
                    </select>
					<span id="result"></span>
					<span id="results" style="color:red"><p id="error_password"></p></span>					
              	</div>		

				  <div class="form-group">
                    <label for="username">Comments:</label>
                    <textarea rows="4" cols="50" class="form-control" id="feedback_comments" name="feedback_comments" placeholder="Add a Comment" style="resize:none;"></textarea>
					<span id="results" style="color:red"><p id="error_comments"></p></span>	
					<input type="hidden" id="cap_feedback_val" name="cap_feedback_val" >
            		</div>

					<div class="form-group" id="capricot_feedback">
                    <label for="username">How likely is it you would recommend our company to a friend or colleague?</label>
					<table>
					<tr>
					<td>
					<div class="logo-top" align="right"><img src ="<?=$company_logo?>" alt="Company_logo"></div>
					<div class="logo_bottom"><b>Your vision, Our mission</b><hr class="innerline"></div>
					</td>
					<td> 
					<div class="t5est">
					<a onclick='satisfied()' class="smiley_link"><img id="green_id" src ="assets/images/feedback_logos/green_smiley.jpg" alt="Company_logo" height="50" width="50">
					<div>
					<p>Satisfied</p>
					<p class="smiley_scale">9-10</p>
					</div>
					</a>
					<a onclick='neutral()' class="smiley_link"><img id="yellow_id" src ="assets/images/feedback_logos/yellow_smiley.jpg" alt="Company_logo" height="50" width="50">
					<div>
					<p>Neutral</p>
					<p class="smiley_scale">7-8</p>
					</div>
					</a>
					<a onclick='dissatisfied()' class="smiley_link"><img id="red_id" src ="assets/images/feedback_logos/red_smiley.jpg" alt="Company_logo" height="50" width="50">
					<div>
					<p>Dissatisfied</p>
					<p class="smiley_scale">0-6</p>
					</div>
					</a>
					</div>
					</td>
					</tr>
					</table>
					<span id="results" style="color:red"><p id="error_company_fb"></p></span>	
                    
            		</div>
               	<button class="btn btn-lg btn-primary col-lg-offset-5" type="button" id="password_enter">Submit</button>
            </form>

              <?php }else {?>
              <div style="text-align:center"><b>Thank You for your feedback!!!!</b></div>
               <?php }?>
        </div>
    </div>
    <?php include 'assets/lib/javascript.php'?>	
	<script>
//window.history.forward();	
var feed_falg = 1;
var work_type = $("#work_type").val();
$("#capricot_feedback").hide();
if(work_type == 9) //if ticket is ctpl call, no need of capricot feedback.
{
	$("#capricot_feedback").hide();
	feed_flag = 0;
}
else
{
	$("#capricot_feedback").show();
	feed_flag = 1;
}

function satisfied()
{
	var img = document.getElementById('green_id').src;
	if (img.indexOf('green_smiley')!=-1) {
			$("#cap_feedback_val").val("Satisfied");
            document.getElementById('green_id').src  = 'assets/images/feedback_logos/green_solid.jpg';
			document.getElementById('red_id').src = 'assets/images/feedback_logos/red_smiley.jpg';
			document.getElementById('yellow_id').src = 'assets/images/feedback_logos/yellow_smiley.jpg';
        }
         else {
			$("#cap_feedback_val").val("");
           document.getElementById('green_id').src = 'assets/images/feedback_logos/green_smiley.jpg';
       }

}

function dissatisfied()
{
	var img = document.getElementById('red_id').src;
	if (img.indexOf('red_smiley')!=-1) {
		$("#cap_feedback_val").val("Dissatisfied");
            document.getElementById('red_id').src  = 'assets/images/feedback_logos/red_solid.jpg';
			document.getElementById('green_id').src = 'assets/images/feedback_logos/green_smiley.jpg';
			document.getElementById('yellow_id').src = 'assets/images/feedback_logos/yellow_smiley.jpg';
        }
         else {
			$("#cap_feedback_val").val("");
           document.getElementById('red_id').src = 'assets/images/feedback_logos/red_smiley.jpg';
       }

}

function neutral()
{
	var img = document.getElementById('yellow_id').src;
	if (img.indexOf('yellow_smiley')!=-1) {
			$("#cap_feedback_val").val("Neutral");
            document.getElementById('yellow_id').src  = 'assets/images/feedback_logos/yellow_solid.jpg';
			document.getElementById('red_id').src = 'assets/images/feedback_logos/red_smiley.jpg';
			document.getElementById('green_id').src = 'assets/images/feedback_logos/green_smiley.jpg';
        }
         else {
			$("#cap_feedback_val").val("");
           document.getElementById('yellow_id').src = 'assets/images/feedback_logos/yellow_smiley.jpg';
       }

}


function restrictMinus(e) {
        var drpCatEarnning = document.getElementsByTagName('input[type=number]');
        var inputKeyCode = e.keyCode ? e.keyCode : e.which;
        if (inputKeyCode != null) {
            if (inputKeyCode == 45) e.preventDefault();
        }
      }	
		$('#password_enter').click(function(){
			var feedback=$("#password").val();
			var rating=$("#c_password").val();
			var capricot_feedback = $("#cap_feedback_val").val();
			var comments = $("#feedback_comments").val();

			if(feed_flag == 1)
			{
				if(feedback !="" && comments != "" && capricot_feedback != ""){
					$.ajax({
						url         :   "<?php echo base_url(); ?>index.php?/webservice_v2/close_tkt",
						type        :   "POST",
						data        :   $('#register').serialize(),// {action:'$funky'}
						//datatype	:	"JSON",	
						cache       :   false,
						success    	: 	function(data){	
							
						swal({
							title: "",
						     text: "Thanks for your update",
						     type: "success",
						     confirmButtonClass: "btn-primary",
						     confirmButtonText: "Ok.",
						     closeOnConfirm: false,
						},
						function(isConfirm) {
								if (isConfirm) {
                        //    window.close();
						location.reload();
									       }
					        }); 
     
},
										
					});
				
			}else if(feedback == ""){
				$('#error_password').html('Feedback is mandatory');
				$('#error_company_fb').html('');
				$('#error_comments').html('');
			}
			else if(comments == ""){
				$('#error_comments').html('Comments are mandatory');
				$('#error_company_fb').html('');
				$('#error_password').html('');
			}
			else if(capricot_feedback == ""){
				$('#error_company_fb').html('Feedback is mandatory');
				$('#error_password').html('');
				$('#error_comments').html('');
			}

			}
			else{

				if(feedback !="" && comments != ""){
					$.ajax({
						url         :   "<?php echo base_url(); ?>index.php?/webservice_v2/close_tkt",
						type        :   "POST",
						data        :   $('#register').serialize(),// {action:'$funky'}
						//datatype	:	"JSON",	
						cache       :   false,
						success    	: 	function(data){	
							
					swal({
	 				title: "",
						     text: "Thanks for your update",
						     type: "success",
						     confirmButtonClass: "btn-primary",
						     confirmButtonText: "Ok.",
						     closeOnConfirm: false,
						},
						function(isConfirm) {
								if (isConfirm) {
                        //    window.close();
						location.reload();
									       }
					        }); 
         			
},
										
					});
				
			}else if(feedback == ""){
				$('#error_password').html('Feedback is mandatory');
				$('#error_company_fb').html('');
				$('#error_comments').html('');
			}
			else if(comments == ""){
				$('#error_comments').html('Comments are mandatory');
				$('#error_company_fb').html('');
				$('#error_password').html('');
			}

			}
					
			$('#password').keyup(function(){
				$('#error_password').html("");
			});						
			$('#c_password').keyup(function(){
				$('#error_cpassword').html("");
			});
		});		
		
	</script>
   
</body>
</html>