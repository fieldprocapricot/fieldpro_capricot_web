<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php include 'assets/lib/cssscript.php'?>
            <style>
               <!-- .sweet-alert.showSweetAlert.visible{
                  z-index:99999999 !important;
                  }-->
               .fileinput-new, .fileinput-exists {
                 color: #000;
               }
               .sweet-alert.showSweetAlert.visible{
                 border:1px solid red;
               }
               .dt-buttons{
                 display:none !important;
               }
               /* .dataTables_filter
               {
                 text-align-last: right;
               } */
                 .uneditable-input {
min-width:auto !important;
}
.form-control.uneditable-input.input-fixed {
    max-width: 233px !important;
}
@media screen and (max-width: 540px) {
.form-control.uneditable-input.input-fixed {
    max-width: 163px !important;
}
}
input .control-label .required, .form-group .required {
    color: black;
}
span .required
{
    color: red;
}
span.help-block-error {
    color: red !important;
}
            </style>
         </head>
         <!-- END HEAD -->
          <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header.php"?>
               <!-- END HEADER -->
               <div class="page-container">
               <div class="page-sidebar-wrapper">
               <?php include "assets/lib/admin_sidebar.php"?>
               </div>
               <div class="page-content-wrapper">
                  <div class="page-content">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">
                                       Service Group Management
                                    </div>
                                    <div class="actions">
                                    <div class="btn-group">

                                                <button id="sample_editable_1_new" class="btn btn-circle green btn-outline" onClick="create_details()"> Add Service Group
                                                <i class="fa fa-plus"></i>
                                                </button>
                                             </div>
                                     </div>
                                 </div>
                                 <div class="portlet-body">
                                    
                                    <div class="table=responsive">
                                      <table class="table table-striped table-bordered table-hover load_data" id="servicegrouptable">
                                                    <thead> 
                                                        
                                                        <tr>
                                                            <th class="text-center">S.No</th>
                                                            <th class="text-center">Service Group</th>
															  <th class="text-center">Checklist</th>
                                                          
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                 
                                                    <tbody align="center">
                                                     
                      
                    
                                                    </tbody>
                                                </table>
                                    </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>


                     <div id="servicegroupform" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id="servicegroupmodaltitle"></span></h4>
      </div>
     
         <form class="form-horizontal" action="javascript:void(0);" id="submit_form" method="POST">
             <div class="modal-body">
                 <input type="hidden" class="form-control required" name="servicegroupid" id="servicegroupid" />
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="form-group">
                       <label class="col-md-6">Service Group
                             <span class="required"> * </span>
                                </label>
                        <div class="col-md-6 col-sm-6">
                               <input type="text" class="form-control" name="servicegroupname" id="servicegroupname" />
                        </div>
                        </div>
                        </div>
            </div>
            </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-circle green btn-outline">Submit</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal">Cancel</button>
      </div>
        </form>
      
    </div>

  </div>
</div>
                     <!-- END PAGE BASE CONTENT -->
                        <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                  </div>
            <!--Modal Starts-->
            <!-- Modal -->
           
           
            <!-- Modal -->
           
            <!--Modal End-->
          
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>
            <script>
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
               $('#service_group').addClass('open');
               var formname = "#submit_form";
               $(formname).validate({
    doNotHideMessage:!0,errorElement:"span",errorClass:"help-block help-block-error",focusInvalid:!1,
    rules: {
        servicegroupname:{required:!0},
  
    },
    messages: {                                     
           servicegroupname: {
            
        lesserThan:"Service group name is required"
          },
          
    
}
});
               // $(document).ready(function(){
//alert(1);
                 var table = $('#servicegrouptable');
                   table.dataTable({
          autoWidth: false,
          bSort : false,
          
     "ajax": {
    
            url : "<?php echo base_url();?>index.php?/controller_admin/getservice_groupall",
           "type": "POST",
       "data": function(d){
       //     d.custno = $('#custno').val();
        //  d.email = $('#custemail').val();
        }
        },
          
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                //    "sortAscending": ": activate to sort column ascending",
                //    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

         dom: 'lBfrtip',
         
         buttons: [
            //    { extend: 'print', className: 'btn green btn-outline' },
           //     { extend: 'copy', className: 'btn red btn-outline' },
           //     { extend: 'pdf', className: 'btn green btn-outline' },
              //  { extend: 'excel', className: 'btn green btn-outline',exportOptions: {
//columns: 'th:not(:last-child)'} },
           //     { extend: 'csv', className: 'btn purple btn-outline ' },
           //     { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'}
            ],
            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,

            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,

       
        });
                   
               //});

               $( document).on( "submit", "#submit_form", function(e) {
    //alert('submit');
     
       if ($(formname).valid())
        { 
        $('#service_group button[type="submit"]').attr("disabled","disabled");
        //var  actionUrl = $(formname).attr('action');
        //  alert("hi");
        $(formname+" button").attr("disabled","disabled");
        var  actionUrl = "<?php echo base_url(); ?>index.php/travelhistory/travelhistoryInsert";
        e.preventDefault();
            var servicegroupid=$('#servicegroupid').val();
            var servicegroupname=$('#servicegroupname').val();
            $.ajax({
                       url         :   "<?php echo base_url();?>index.php?/controller_admin/insertservicegroup",
                       type        :   "POST",
                       data        :   {'servicegroupid' : servicegroupid,'servicegroupname':servicegroupname},
                     //  datatype    :   "JSON",
                       cache       :   false,
                      // process     :   false,
                       success     :   function(data){
                                          // var data=JSON.parse(data);
                                          $(formname+" button").removeAttr("disabled");
                                         var obj = JSON.parse(data);
                                         // console.log(obj);
                                         // console.log(obj['message']);
                                         // console.log(obj.message);
                                        // console.log(obj[0]['message']);
                                        // console.log(obj[0].message);
                                          $('#servicegroupform').modal('hide');
                                          $('#servicegroupid').val('');
                      $('#servicegroupname').val('');
                      alert(obj['message']);
                                        /*  $.dialogbox({
               type:'msg',
               content:'cvhdfhd',
               closeBtn:true,
               btn:['Ok.'],
               call:[
                function(){
                    $.dialogbox.close();
                   
                }
               ]
               });*/
                                         var oTable= table.DataTable().ajax.reload();
                                       },
                   })            
        }
        else
        {               
            return false;
        }
    
      });
              
               function create_details()
                   {
                   
                    $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
});     
                   
                     $('#servicegroupform').modal('show');
                     $('#servicegroupid').val(0);
                      $('#servicegroupmodaltitle').html('Add Service Group');
                   }
                   function edit_details(id)
                   {
var name='';
                    $.ajax({
                       url         :   "<?php echo base_url();?>index.php?/controller_admin/editservicegroup",
                       type        :   "POST",
                       data        :   {'servicegroupid' : id},
                       datatype    :   "JSON",
                       cache       :   false,
                      // process     :   false,
                       success     :   function(data){
                                          // var data=JSON.parse(data);
                                         var obj = JSON.parse(data);
                                        // console.log(obj);
                                        // console.log(obj[0]['service_group']);
                                        console.log(obj[0].service_group);
                                    name=obj[0]['service_group'];
                                    console.log(name);
                                     $('#servicegroupmodaltitle').html('Edit Service Group');
                                     $('#servicegroupname').val(name);
                                        // var oTable= table.DataTable().ajax.reload();
                                       },
                   }) 

                    $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
});
console.log(name);
                     $('#servicegroupform').modal('show');
                     $('#servicegroupid').val(id);
                     
                   }
                    function delete_details(id){
               swal({
  title: "Are you sure to delete this service group?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){
$.ajax({
                       url         :   "<?php echo base_url();?>index.php?/controller_admin/deleteservicegroup",
                       type        :   "POST",
                       data        :   {'servicegroupid' : id},
                     //  datatype    :   "JSON",
                       cache       :   false,
                      // process     :   false,
                       success     :   function(data){
                                          // var data=JSON.parse(data);
                                          $(formname+" button").removeAttr("disabled");
                                         console.log(data);
                                         var obj = JSON.parse(data);
                                         console.log(obj);
                                     if(obj['status']=='success')
                                     {
                                        swal("Deleted!", data['message'], "success");
                                     }
                                     else
                                     {
                                        swal("Not Deleted!", data['message'], "error");
                                     }

                                         var oTable= table.DataTable().ajax.reload();
                                       },
                   }) 

  
});

               }
            </script>
           
         </body>
      </html>