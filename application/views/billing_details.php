<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php 
            $company_id=$this->session->userdata('companyid');
            include 'assets/lib/cssscript.php'?>
             
             <style>
                 .sweet-alert.showSweetAlert.visible{
                         z-index: 999999;
                            border: 1px solid cadetblue;
                                 margin-top: -118px !important;
                 }
                               span.fileinput-new {
                                         color: #000 !important;
                              }
             </style>
         </head>
         <!-- END HEAD -->
         <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header_superad.php"?>
               <!-- END HEADER -->
               <div class="page-container">
               <div class="page-sidebar-wrapper">
               <?php include "assets/lib/superad_sidebar.php"?>
               </div>
               <div class="page-content-wrapper">
                  <div class="page-content">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->                             
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption"> BILLING DETAILS </div>
                                    <ul class="nav nav-tabs">
                                             <!--<li class="active">-->
                                             <li class="active">
                                                <a href="#unassigned" data-toggle="tab">Set Charges</a>
                                             </li>
                                             <li>
                                                <a href="#assigned" data-toggle="tab">View Available</a>
                                             </li>
                                            
                                          </ul>
                                 </div>
                                 <div class="portlet-body">
                                          <div class="tab-content">
                                             <div class="tab-pane active" id="unassigned">
                          
                      
                               <form class="form-horizontal" id="submit_form" method="POST" novalidate="novalidate">
                                <div class="form-wizard">
                                    <div class="form-body" id="form_body">
                                     <div class="row">
                                            <div class="col-md-2 pull-right">
                                                <button type="button" class="btn btn-outline blue btn-circle btn-md" data-target="#bulkupload_s" data-toggle="modal">
                                                 Bulk Upload</button>
                                            </div>    
                                    </div>
                                    <br>
                                     <div class="row">
                                           <div class="col-lg-10 col-lg-offset-1">
                                                <label class="control-label col-lg-4">Company Name</label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="company_name" name="company_name"></select>
                                                    </div>
                                            </div>
                                    </div>
                                     <br>
                                     <div class="row">
                                          <div class="col-lg-10 col-lg-offset-1">
                                                <label class="control-label col-lg-4">Priority Level</label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="priority" name="priority"></select>
                                                    </div>
                                            </div>
                                        </div>
                                        <br>
                                      <div class="row">
                                           <div class="col-lg-10 col-lg-offset-1">
                                                <label class="control-label col-lg-4">Product Category</label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="product" name="product">
                                                            <option value="empty" selected disabled>No Product-Category Available</option>
                                                         </select>
                                               <!--<input type="text" class="form-control" id="product" name="product"></input>-->
                                            </div>
                                          </div>
                                     </div>
                                     <br>
                                     <div class="row">
                                          <div class="col-lg-10 col-lg-offset-1">
                                             <label class="control-label col-lg-4">Sub-Category</label>
                                                <div class="col-lg-8">
                                                        <select class="form-control" id="category" name="category">
                                                            <option value="empty" selected disabled>No Sub-Category Available</option>
                                                       </select>
                                               <!--<input type="text" class="form-control" id="category" name="category"></input>-->
                                                </div>
                                         </div>
                                     </div>
                                     <br>
                                     <div class="row">
                                          <div class="col-lg-10 col-lg-offset-1">
                                             <label class="control-label col-lg-4">Per-Call Charges within SLA</label>
                                                 <div class="col-lg-8">
                                                     <input type="text" class="form-control sla_amt" id="sla_amt" name="sla_amt" maxlength="4" placeholder="Digits Only"></input>
                                                </div>
                                        </div>
                                     </div>
                                     <br>
                                     <div class="row">
                                         <div class="col-lg-10 col-lg-offset-1">
                                             <label class="control-label col-lg-4">Per-Call Charges for Elapsed SLA</label>
                                                 <div class="col-lg-8">
                                                     <input type="text" class="form-control elapsed_amt" id="elapsed_amt" name="elapsed_amt" maxlength="4" placeholder="Digits Only"></input>
                                                </div>
                                         </div>
                                     </div>
                                     <br>
                                    
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                    <div class="text-center">
                                                    <button type="button" class="btn btn-circle green-haze btn-outline btn-md" id="sub_button" disabled="disabled"> Submit
                                                        </button>
                                                        <button type="reset" class="btn red-haze btn-outline btn-circle btn-md" id="clear">Reset</button>
                                                    </div>
                                               
                                            </div>
                                        </div>
                                      </div>
                                </div>
                          </form>
                            </div>
                  
                                             <div class="tab-pane" id="assigned">
                                                 <div class="col-md-4 pull-right">
                                                        <select class="form-control" id="list_company"> 
                                                         </select>
                                                 </div>
												 <span class="clearfix"></span>
                                                 <div class="table-responsive">

                                    <table class="table table-hover">
                                        <thead>
                                            <tr style="text-align:center">
                                                <th style="text-align:center" colspan="2">Billing Details</th>
                                                <th style="text-align:center" colspan="4">Charges Within SLA</th>
                                                <th style="text-align:center" colspan="4">Charges for Elapsed SLA</th>
                                            
                                            </tr>
                                            
                                            <tr>
                                                <th style="text-align:center">Product-Category</th>
                                                <th style="text-align:center">Sub-Category</th>
                                            <th style="text-align:center">P1</th>
                                            <th style="text-align:center">P2</th>
                                            <th style="text-align:center">P3</th>
                                            <th style="text-align:center">P4</th>
                                            <th style="text-align:center">P1</th>
                                            <th style="text-align:center">P2</th>
                                            <th style="text-align:center">P3</th>
                                            <th style="text-align:center">P4</th>
                                            <th style="text-align:center" colspan="4">Action</th>
                                            </tr>
                                            
                                        </thead>
                                        <tbody id="alrdy_existing" name="alrdy_existing">                                        
                                            
                                          
                                            </tbody>
                                            
                                    </table>
                                </div>
                                             </div>
                                            
                                            
                                        
                                        
                                          </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>
                  <!-- END PAGE BASE CONTENT -->
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
            </div>
           <!--modal-->
                <div id="bulkupload_s" class="modal fade" role="dialog">
            <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content">
                 <div class="modal-body">
                        <div class="portlet light bordered" id="form_wizard_1" style="margin-bottom:0">
                        <div class="portlet-title">
                            <label class="control-label" style="font-size: 20px !important;">Upload File</label>
                             <button type="button" class="close pull-right" data-dismiss="modal">&times;</button> 
                            
                      </div>
                     <div class="portlet-body form">
                     <form class="form-horizontal" id="bill_form" method="POST" novalidate="novalidate">
                                <div class="form-wizard">
                                    <div class="form-body">
                                    <div class="row">
                                        <div class="col-sm-3 text-center">
                                            <?php $fname='sla_bill.xlsx'; ?>
                                <a class="btn purple-sharp btn-circle btn-outline sample-temp" style="margin-left: -10px !important;" href="<?php echo base_url(); ?>index.php?/controller_superad/download_sampletemplate/<?php echo $fname;?>">Sample Template</a>                  
                                            <!--<button type="button" class="btn btn-circle btn-primary" >Sample Template</button>-->
                                        </div>
                                           <div class="col-sm-9">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="input-group input-large">
                                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                        <span class="fileinput-filename"> </span>
                                                    </div>
                                             <span class="input-group-addon btn default btn-file">
                                                        <span class="fileinput-new" id="span-button"> Select file </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="file-upload" id="file-upload"> </span>
                                                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                     </div>
                                  </div>
                                    <div class="row">
                                            <div class="col-md-12">
                                                    <div class="pull-right">
                                                        <button type="button" class="btn btn-outline green btn-circle btn-md" id="upload_bulk">
                                                        <i class="fa fa-upload" aria-hidden="true"></i>Upload</button> &nbsp;&nbsp;&nbsp;
                                                        <button type="cancel" class="btn red-haze btn-outline btn-circle btn-md">Cancel</button>
                                                    </div>
                                               
                                            </div>
                                        </div>
                                </div>
                     </form>
                     </div>
                 </div>         
                 </div>
                </div>
                </div>
           </div>
           <!--end-->
            <div id="update_modal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
               <!-- Modal content-->
               <div class="modal-content">
                 <div class="modal-body">
                        <div class="portlet light bordered" id="form_wizard_1" style="margin-bottom:0">
                        <div class="portlet-title">
                            <label class="control-label" style="font-size: 20px !important;">Update SLA Charges</label>
                             <button type="button" class="close pull-right" data-dismiss="modal">&times;</button> 
                            
                      </div>
                     <div class="portlet-body form">
                     <form class="form-horizontal" id="update_form" method="POST" novalidate="novalidate">
                                <div class="form-wizard">
                                    <div class="form-body">
                                        <div class="row" style="display:none">
                                                <div class="col-md-3">
                                                    <label class="control-label">Company ID</label>
                                                </div>
                                               <div class=" form-group col-md-5">
                                                    <input type="text" class="form-control" id="comp_id" name="comp_id" readonly/>
                                               </div>
                                       </div>
                                        <div class="row">
                                                <div class="col-md-3">
                                                    <label class="control-label" style="text-align:left">Product Category</label>
                                                </div>
                                               <div class=" form-group col-md-5">
                                                    <input type="text" class="form-control" id="p_name" name="p_name" readonly/>
                                               </div>
                                       </div>
                                        <div class="row">
                                                <div class="col-md-3">
                                                    <label class="control-label" style="text-align:left">Sub Category</label>
                                                </div>
                                               <div class=" form-group col-md-5">
                                                    <input type="text" class="form-control" id="c_name" name="c_name" readonly/>
                                               </div>
                                       </div>
                                        <div class="row">
                                            <table style="border:0px !important">
                                                 <thead>
                                            <tr style="text-align:center">
                                                <th style="text-align:center" colspan="4">Charges Within SLA</th>
                                                <th style="text-align:center" colspan="4">Charges for Elapsed SLA</th>
                                            
                                            </tr>
                                              <tr>
                                                <th colspan="4">&nbsp;</th>
                                                <th colspan="4">&nbsp;</th>
                                            
                                            </tr>
                                            <tr>
                                                <th style="text-align:center">P1</th>
                                                <th style="text-align:center">P2</th>
                                                <th style="text-align:center">P3</th>
                                                <th style="text-align:center">P4</th>
                                                <th style="text-align:center">P1</th>
                                                <th style="text-align:center">P2</th>
                                                <th style="text-align:center">P3</th>
                                                <th style="text-align:center">P4</th>
                                            </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                          <td><input type="text" class="form-control" id="sla_P1" name="sla_P1"/></td>
                                         <td><input type="text" class="form-control" id="sla_P2" name="sla_P2"/></td>
                           <td><input type="text" class="form-control" id="sla_P3" name="sla_P3"/></td>
                    <td><input type="text" class="form-control" id="sla_P4" name="sla_P4"/></td>
                                    

                    <td><input type="text" class="form-control" id="elap_P1" name="elap_P1"/></td>  
                                       
                              <td><input type="text" class="form-control" id="elap_P2" name="elap_P2"/></td>    
                                         
                    <td><input type="text" class="form-control" id="elap_P3" name="elap_P3"/></td>      
                                           <td><input type="text" class="form-control" id="elap_P4" name="elap_P4"/></td>
                                                    </tr>
                                                </tbody>
                                             </table>
                                        </div>
                                    <!--    <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label" style="text-align:left">Sla Amount for P1</label>
                                                </div>
                                               <div class=" form-group col-md-8">
                                                    <input type="text" class="form-control" id="sla_P1" name="sla_P1"/>
                                               </div>
                                       </div>
                                        <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label" style="text-align:left">Elapsed Amount for P1</label>
                                                </div>
                                               <div class=" form-group col-md-8">
                                                    <input type="text" class="form-control" id="elap_P1" name="elap_P1"/>
                                               </div>
                                       </div>
                                        <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label" style="text-align:left">Sla Amount for P2</label>
                                                </div>
                                               <div class=" form-group col-md-8">
                                                    <input type="text" class="form-control" id="sla_P2" name="sla_P2"/>
                                               </div>
                                       </div>
                                       <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label" style="text-align:left">Elapsed Amount for P2</label>
                                                </div>
                                               <div class=" form-group col-md-8">
                                                    <input type="text" class="form-control" id="elap_P2" name="elap_P2"/>
                                               </div>
                                       </div>
                                      <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label" style="text-align:left">Sla Amount for P3</label>
                                                </div>
                                               <div class=" form-group col-md-8">
                                                    <input type="text" class="form-control" id="sla_P3" name="sla_P3"/>
                                               </div>
                                       </div>
                                       <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label" style="text-align:left">Elapsed Amount for P3</label>
                                                </div>
                                               <div class=" form-group col-md-8">
                                                    <input type="text" class="form-control" id="elap_P3" name="elap_P3"/>
                                               </div>
                                       </div>
                                      <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label" style="text-align:left">Sla Amount for P4</label>
                                                </div>
                                               <div class=" form-group col-md-8">
                                                    <input type="text" class="form-control" id="sla_P4" name="sla_P4"/>
                                               </div>
                                       </div>
                                       <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label" style="text-align:left">Elapsed Amount for P4</label>
                                                </div>
                                               <div class=" form-group col-md-8">
                                                    <input type="text" class="form-control" id="elap_P4" name="elap_P4"/>
                                               </div>
                                       </div>-->
                                  </div>
                                  <div class="form-actions">
                                    <div class="row">
                                            <div class="col-md-12">
                                                    <div class="pull-right">
                                                        <button type="button" class="btn btn-outline green btn-circle btn-md" id="upload_sladetails">
                                                        Update</button> &nbsp;&nbsp;&nbsp;
                                                        <button type="cancel" class="btn red-haze btn-outline btn-circle btn-md" data-dismiss="#update_modal">Cancel</button>
                                                    </div>
                                               
                                            </div>
                                        </div>
                                  </div>
                                </div>
                     </form>
                     </div>
                 </div>         
                 </div>
                </div>
                </div>
           </div>
    <div id="add_confirm" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="add_subproduct">
                            <div class="form-group">                                
                                <div class="col-sm-12">
                                    <p id="sla_detail_confirm"></p>
                                </div>
                            </div>                          
                        </form>
                    </div>
                    <div class="modal-footer">
                        
                        <button type="button" class="btn btn-circle red btn-outline" id="closeandreload" data-dismiss="modal"><i class="fa fa-times">
                                       </i> Close</button>
                    </div>
                </div>
            </div>
        </div>
        
            <!-- END QUICK SIDEBAR -->
        <?php include 'assets/lib/javascript.php'?>   
             <script>               
            $('.nav.navbar-nav').find('.open').removeClass( 'open' );
            $('#focus_billing').addClass('open');
        </script>
           <script type="text/javascript">
        $(document).ready(function() {

          $('#sla_P1').on('input', function (event) { 
                 this.value = this.value.replace(/[^0-9]/g, '');
          });
          $('#elap_P1').on('input', function (event) { 
                this.value = this.value.replace(/[^0-9]/g, '');
          });

          $('#sla_P2').on('input', function (event) { 
                 this.value = this.value.replace(/[^0-9]/g, '');
          });
          $('#elap_P2').on('input', function (event) { 
                this.value = this.value.replace(/[^0-9]/g, '');
          });
        
          $('#sla_P3').on('input', function (event) { 
                 this.value = this.value.replace(/[^0-9]/g, '');
          });
          $('#elap_P3').on('input', function (event) { 
                this.value = this.value.replace(/[^0-9]/g, '');
          });
        
          $('#sla_P4').on('input', function (event) { 
                 this.value = this.value.replace(/[^0-9]/g, '');
          });
          $('#elap_P4').on('input', function (event) { 
                this.value = this.value.replace(/[^0-9]/g, '');
          });
        
            
            $.ajax({
                url: "<?php echo site_url('controller_superad/choose_company')?>",
                method: "POST",
                    success : function(data){
                        console.log(data);
                        var result = JSON.parse(data);
                        $('#list_company').val();
                        //$('#sla_cname').append('<option selectedn</option>');
                        for(i=0;i<result.length;i++)
                            {
                                $('#list_company').append('<option value='+result[i]['company_id']+'>'+result[i]['company_name']+'</option>');
                            }
                        $("#alrdy_existing").html();
            var company_id=$("#list_company").val();
             $.ajax({
                url: "<?php echo site_url('controller_superad/view_existingbilling')?>",
                method: "POST",
                data:{'company_id':company_id},
                    success : function(data){
                        var result = JSON.parse(data);
                                                console.log(result);

                    if(result.length>0)
                            
                            {
                                    for(i=0;i<result.length;i++)
                                {
                                var product=result[i].product_name;
                                    var prod= product.replace(/ /g, ":");   
                                var category=result[i].cat_name;
                                     var cat=category.replace(/ /g, ":");
                                var pr=result[i].priority;
                                 console.log(pr); 
                                //var dr=pr[2].P3[0].elapsed_amount;
                                //alert(dr);
                             $("#alrdy_existing").append("<tr><td style='text-align:center' id='prod'"+i+"' name='prod'"+i+"' >"+result[i].product_name+"</td><td style='text-align:center' id='cat'"+i+"' name='cat'"+i+"'>"+result[i].cat_name+"</td><td style='text-align:center' id='p1'"+i+"' name='p1'"+i+"'>"+pr[0].P1[0].sla_amount+"</td><td style='text-align:center' id='p2'"+i+"' name='p2'"+i+"'>"+pr[1].P2[0].sla_amount+"</td><td style='text-align:center' id='p3'"+i+"' name='p3'"+i+"'>"+pr[2].P3[0].sla_amount+"</td><td style='text-align:center' id='p4'"+i+"' name='p4'"+i+"'>"+pr[3].P4[0].sla_amount+"</td><td style='text-align:center' id='ep1'"+i+"' name='ep1'"+i+"'>"+pr[0].P1[0].elapsed_amount+"</td><td style='text-align:center' id='ep2'"+i+"' name='ep2'"+i+"'>"+pr[1].P2[0].elapsed_amount+"</td><td style='text-align:center' id='ep3'"+i+"' name='ep3'"+i+"'>"+pr[2].P3[0].elapsed_amount+"</td><td style='text-align:center' id='ep4'"+i+"' name='ep4'"+i+"'>"+pr[3].P4[0].elapsed_amount+"</td><td style='text-align:center'><button type='button' id='"+i+"' class='btn btn-outline blue btn-circle btn-icon-only' onclick=function_edit(this.id,'"+company_id+"','"+prod+"','"+cat+"','"+pr[0].P1[0].sla_amount+"','"+pr[1].P2[0].sla_amount+"','"+pr[2].P3[0].sla_amount+"','"+pr[3].P4[0].sla_amount+"','"+pr[0].P1[0].elapsed_amount+"','"+pr[1].P2[0].elapsed_amount+"','"+pr[2].P3[0].elapsed_amount+"','"+pr[3].P4[0].elapsed_amount+"')><i class='fa fa-edit'></i></button></td></tr>");
                            }
                    
                            }
                        else    
                    {
                         $("#alrdy_existing").append("No SLA Details Available");
                    }
                    
                        },
                    error: function( error )
                                {
                                    alert( "Error" );
                                }
                });
        
                    }
            });
            
            
        
                $.ajax({
                url: "<?php echo site_url('controller_superad/choose_company')?>",
                method: "POST",
                    success : function(data){
                        console.log(data);
                        var result = JSON.parse(data);
                        $('#company_name').val();
                        $('#company_name').append('<option selected disabled>Select Company</option>');
                        for(i=0;i<result.length;i++)
                            {
                                $('#company_name').append('<option value='+result[i]['company_id']+'>'+result[i]['company_name']+'</option>');
                            }
                        },
                    error: function( error )
                                {
                                    alert( "Error" );
                                }
                });
        
                $.ajax({
                url: "<?php echo site_url('controller_superad/load_priority')?>",
                method: "POST",
                    success : function(data){
                        console.log(data);
                        var result = JSON.parse(data);
                        $('#priority').val();
                        $('#priority').append('<option selected disabled>Select Priority</option>');
                        for(i=0;i<result.length;i++)
                            {
                                $('#priority').append('<option value='+result[i]['priority_level']+'>'+result[i]['priority_level']+'</option>');
                                //$('#priority').append('<option value='+result[i]['company_id']+'>'+result[i]['company_name']+'</option>');
                            }
                        },
                    error: function( error )
                                {
                                    alert( "Error" );
                                }
                });
        });
        
       function function_edit(id,company_id,product,cat_name,P1sla,P2sla,P3sla,P4sla,P1elap,P2elap,P3elap,P4elap){
           var replaceSpace=product;
            var prod = replaceSpace.replace(/\:/g," "); 
            var cat=cat_name;
            var category= cat.replace(/\:/g," ");
             //alert(id);
             //alert(P3sla);
          // alert(P4sla);
           //alert(category);alert(P1sla);alert(P1elap);alert(company_id);
           $('#update_modal').modal('show');
            $('#comp_id').val(company_id);
           $('#p_name').val(prod);
           $('#c_name').val(category);
           $('#sla_P1').val(P1sla);
           $('#elap_P1').val(P1elap);
           $('#sla_P2').val(P2sla);
           $('#elap_P2').val(P2elap);
           $('#sla_P3').val(P3sla);
           $('#elap_P3').val(P3elap);
           $('#sla_P4').val(P4sla);
           $('#elap_P4').val(P4elap);
           
           
           
        }
    
        $("#list_company").change(function(){
                    var comp=$("#list_company").val();
             $.ajax({
                url: "<?php echo site_url('controller_superad/view_existingbilling')?>",
                method: "POST",
                data:{'company_id':comp},
                    success : function(data){
                         $("#alrdy_existing").html('');
                        var result = JSON.parse(data);
                        console.log(result);

                        if(result.length>0)
                        {
                            for(i=0;i<result.length;i++)
                            
                            {
                                var product=result[i].product_name;
                                    var prod= product.replace(/ /g, ":");   
                                var category=result[i].cat_name;
                                     var cat=category.replace(/ /g, ":");
                                var pr=result[i].priority;
                                 console.log(pr); 
                                //var dr=pr[2].P3[0].elapsed_amount;
                                //alert(dr);
                             $("#alrdy_existing").append("<tr><td style='text-align:center' id='prod'"+i+"' name='prod'"+i+"' >"+result[i].product_name+"</td><td style='text-align:center' id='cat'"+i+"' name='cat'"+i+"'>"+result[i].cat_name+"</td><td style='text-align:center' id='p1'"+i+"' name='p1'"+i+"'>"+pr[0].P1[0].sla_amount+"</td><td style='text-align:center' id='p2'"+i+"' name='p2'"+i+"'>"+pr[1].P2[0].sla_amount+"</td><td style='text-align:center' id='p3'"+i+"' name='p3'"+i+"'>"+pr[2].P3[0].sla_amount+"</td><td style='text-align:center' id='p4'"+i+"' name='p4'"+i+"'>"+pr[3].P4[0].sla_amount+"</td><td style='text-align:center' id='ep1'"+i+"' name='ep1'"+i+"'>"+pr[0].P1[0].elapsed_amount+"</td><td style='text-align:center' id='ep2'"+i+"' name='ep2'"+i+"'>"+pr[1].P2[0].elapsed_amount+"</td><td style='text-align:center' id='ep3'"+i+"' name='ep3'"+i+"'>"+pr[2].P3[0].elapsed_amount+"</td><td style='text-align:center' id='ep4'"+i+"' name='ep4'"+i+"'>"+pr[3].P4[0].elapsed_amount+"</td><td style='text-align:center'><button type='button' id='"+i+"' class='btn btn-outline blue btn-circle btn-md' onclick=function_edit(this.id,'"+comp+"','"+prod+"','"+cat+"','"+pr[0].P1[0].sla_amount+"','"+pr[1].P2[0].sla_amount+"','"+pr[2].P3[0].sla_amount+"','"+pr[3].P4[0].sla_amount+"','"+pr[0].P1[0].elapsed_amount+"','"+pr[1].P2[0].elapsed_amount+"','"+pr[2].P3[0].elapsed_amount+"','"+pr[3].P4[0].elapsed_amount+"')>Update</button></td></tr>");
                            }
                        }
                        else{
                             $("#alrdy_existing").append("<tr style='text-align:center'><td colspan='10'>No Billing Information Available<td></tr>");
                        }
                        },
                    error: function( error )
                                {
                                    alert( "Network Error" );
                                }
                }); 
        });
             
        // function function_edit(id,product,cat_name,P1,P2,P3,P4,'"+rpr[1].P2[0].sla_amount+"','"+rpr[2].P3[0].sla_amount+"','"+rpr[3].P4[0].sla_amount+"')
        
        $('.sla_amt').on('input', function (event) { 
            this.value = this.value.replace(/[^0-9]/g, '');
        });
   
        $('.elapsed_amt').on('input', function (event) { 
            this.value = this.value.replace(/[^0-9]/g, '');
        });
        
        $('#bulkuploads').click(function () {
           $('#bulkupload_s').modal('show');
        });
        
        $('#company_name').change(function(){
            var value=$('#company_name').val();
            if(value=="Select Company"){
                 $('#sub_button').attr('disabled', 'disabled');
                 //$('#priority').attr('disabled', 'disabled');  
            }
            else {
                $('#sub_button').removeAttr('disabled');
                //$('#priority').removeAttr('disabled');
                $.ajax({
                url: "<?php echo site_url('controller_superad/load_product')?>",
                method: "POST",
                data:{'company_id':value},
                    success : function(data){
                        //alert(data.length);
                        /*if(data.length<1){
                              $('#product').html('');
                                $('#product').val();
                                $('#product').append('<option selected disabled>No Product Category available</option>');
                            //$('#product').append('<option value="empty" selected disabled>No Product-Category Available</option>');
                        
                        } */
                        if(data.length>1){
                            var result = JSON.parse(data);  
                            //alert("product");
                                console.log(result);
                            if(result.length>0){
                                $('#product').html('');
                                $('#product').val();
                                $('#product').append('<option selected disabled>Select Product Category</option>');
                                for(i=0;i<result.length;i++)
                                    {
                                        $('#product').append('<option value='+result[i]['product_id']+'>'+result[i]['product_name']+'</option>');
                                    }
                            }
                            else{
                                $('#product').empty();
                                $('#product').append('<option value="empty" selected disabled>No Product-Category Available</option>');
                            }
                        }
                        else if(data.length<1){
                            
                            $('#product').empty();
                                //$('#product').append('<option selected disabled>No Product Category available</option>');
                            $('#product').append('<option value="empty" selected disabled>No Product-Category Available</option>');
                        }
                        },
                    error: function( error )
                                {
                                    alert( "Network Error" );
                                }
                });
            }
        
        });
       
       $('#upload_sladetails').click(function () {
            $.ajax({
                type:"POST",
                url: "<?php echo base_url();?>"+"index.php?/controller_superad/update_sla",
                data: $('#update_form').serialize(),
                success:function(data){
                    console.log(data);
                    data=$.trim(data);
                  if(data=="updated"){
                            swal({
                              title: 'Updated!',
                              text: "Details are Saved",
                              //showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              confirmButtonText: 'Ok.'
                            },
                            function(isConfirm){
                                if (isConfirm) {
                                    $('#update_modal').modal('hide');
                                    window.location.reload();
                                    window.location.href="<?php echo base_url();?>"+"index.php?/controller_superad/billing_details#assigned";
                                }
                            });
                  }
                  else {
                             swal("Error during Update,Try Again !")
                       }
                }
            });
        });
        
        $('#sub_button').click(function () {
            $.ajax({
                type:"POST",
                url: "<?php echo base_url();?>"+"index.php?/controller_superad/submit_amount",
                data: $('#submit_form').serialize(),
                success:function(data){
                    console.log(data);
                    data=$.trim(data);
                  if(data=="Provide Proper Inputs for the Fields"){
                             swal("Provide Proper Inputs for the Fields!")
                        }
                  else if(data=="All Fields are Mandatory"){
                             swal("Fields All Fields!")
                        }
                 
                 else {
                        swal({
                             title: '',
                              text: "Billing details updated successfully",
                              //showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              confirmButtonText: 'Ok.'
                            },
                            function(isConfirm){
                                if (isConfirm) {
                                    window.location.reload();
                                }
                            });
                      }
                }
            });
        });
        
        $('#product').change(function(){
            var prod=$('#product').val();
            $.ajax({
                url: "<?php echo site_url('controller_superad/load_subcategory')?>",
                method: "POST",
                data:{'prod_id':prod},
                    success : function(data){
                        var result = JSON.parse(data);  
                        console.log(result);
                        $('#category').html('');
                        $('#category').val();
                        if(result.length>0) {
                        $('#category').append('<option selected disabled>Select Sub-Category</option>');
                        for(i=0;i<result.length;i++)
                            {
                                $('#category').append('<option value='+result[i]['cat_id']+'>'+result[i]['cat_name']+'</option>');
                            }
                        }
                        else{
                            $('#category').append('<option selected disabled>No Sub-Category available</option>');
                        }
                        },
                    error: function( error )
                                                                {
                                    alert("Network Error");
                                }
                });
        });
        $('#category').change(function(){
           $.ajax({
                type:"POST",
                url: "<?php echo base_url();?>"+"index.php?/controller_superad/check_combination",
                data: $('#submit_form').serialize(),
                success:function(data){
                                $('#sla_amt').html('');
                                $('#elapsed_amt').html('');
                    console.log(data);
                            var result = JSON.parse(data);
                           if(result!='')
                           {
                                 swal("This Combination already exists, Update if necessary!")
                               $("#sla_amt").attr("disabled", "disabled"); 
                               $("#elapsed_amt").attr("disabled", "disabled"); 
                             //$('#sla_amt').val(result[0]['sla_amount']);
                             //$('#elapsed_amt').val(result[0]['elapsed_amount']);
                           }
                         else
                           {
                               $("#sla_amt").removeAttr("disabled"); 
                               $("#elapsed_amt").removeAttr("disabled");
                               $('#sla_amt').html('');
                              $('#elapsed_amt').html('');
                           }
                     }
                 });
        });
               
       $('#priority').change(function(){
           $.ajax({
                type:"POST",
                url: "<?php echo base_url();?>"+"index.php?/controller_superad/check_combination",
                data: $('#submit_form').serialize(),
                success:function(data){
                                $('#sla_amt').html('');
                                $('#elapsed_amt').html('');
                    console.log(data);
                            var result = JSON.parse(data);
                           if(result!='')
                           {
                                 swal("This Combination already exists, Update if necessary!")
                               $("#sla_amt").attr("disabled", "disabled"); 
                               $("#elapsed_amt").attr("disabled", "disabled"); 
                             //$('#sla_amt').val(result[0]['sla_amount']);
                             //$('#elapsed_amt').val(result[0]['elapsed_amount']);
                           }
                         else
                           {
                               $("#sla_amt").removeAttr("disabled"); 
                               $("#elapsed_amt").removeAttr("disabled");
                               $('#sla_amt').html('');
                              $('#elapsed_amt').html('');
                           }
                     }
                 });
        });
        
  </script>   
  <script>
      $('#upload_bulk').click(function () {
          var inp= $('#file-upload');
          if(inp.val().length > 0)
          {
                var ext = $('#file-upload').val().toString().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['xls', 'xlsx', 'csv']) == -1) {
                    //$("#bulkupload_s").modal('hide');
                    swal({
                        title:"Upload an Excel File",
                        //text: "No file Chosen!",
                        type:"warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok.",
                        cancelButtonText: "Cancel !",
                        closeOnConfirm: false,
                        closeOnCancel: false
               },
                function(isConfirm){
                    if (isConfirm) {
                        swal.close();
                        //$("#bulkupload_s").modal('show');
                        
                    } else {
                    swal.close();
                    }
            }); 
                    //$("#bulkupload_s").modal('show');
                    //return false;
                }
                else {
                    var file_data = $('#file-upload').prop('files')[0];   
                    var form_data = new FormData();
                    form_data.append('file-upload', file_data);
                    $.ajax({                
                        type:'POST',
                        url:'<?php echo base_url(); ?>'+'index.php?/controller_superad/bulk_slabill',
                        contentType:false,
                        processData: false,
                        cache:false,
                        data:form_data,         
                        success: function(data) {
                                var data=JSON.parse(data);
                                       $('#bulkupload_s').modal('hide');
                                       $('#add_confirm').modal('show');
                                    console.log(data.length);
                                    for(i=0;i<data.length;i++){
                                         $('#sla_detail_confirm').append(data[i]+'<br><hr>');
                                    }
                                }
                    });         
                } 
         }
         else{
         $("#bulkupload_s").modal('hide');
         swal({
                        title:"Empty Field, No File Chosen!",
                        //text: "No file Chosen!",
                        type:"warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok.",
                        closeOnConfirm: false,
                        closeOnCancel: false
               },
                function(isConfirm){
                    if (isConfirm) {
                        swal.close();
                        $("#bulkupload_s").modal('show');
                        
                    }
            }); 
            //  swal("Empty Field, No File Chosen!")     
             }
      });

         $("#closeandreload").click(function (){
               $('#add_confirm').modal('show');
                          window.location.reload();
             });
      
                    /*      console.log(data);
                         swal("Details have been Uploaded!")    
                            $("#bulkupload_s").modal('hide');*/                                 
  </script>
         </body>
      </html>