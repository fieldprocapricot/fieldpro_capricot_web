<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<style>
    .dt_buttons{
        display:none;
    }
  .dataTables_filter{
        text-align: right;
    }
.pagination li.prev a, .pagination li.next a {
    padding: 8.5px 8px !important;
}
    </style>
<?php
         include 'assets/lib/cssscript.php'?>    
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
    <!-- BEGIN CONTAINER -->
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include "assets/lib/header_service.php"?>
        <!-- END HEADER -->
        <div class="page-container">
               <div class="page-sidebar-wrapper">
               <?php include "assets/lib/service_sidebar.php"?>
               </div>
               <div class="page-content-wrapper">
                  <div class="page-content">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box dark">
                                <div class="portlet-title">
                                            <div class="caption">
                                       <i class=""></i>New Registration
                                    </div>
                                </div>
                                  
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row" style="display:none">
                                       <div class="form-group col-md-6 col-sm-12">
                                           <div class="col-md-7">
                                                    <input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php echo $this->session->userdata('companyid');?>" readonly>
                                             </div>
                                        </div>
                                     </div>
                                        <div class="tab-pane active" id="tab_actions_pending">
                                           
                                                <table class="table table-hover table-bordered datatable1" id="">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center">Customer ID</th>
                                                            <th style="text-align:center">Product Category</th>
                                                            <th style="text-align:center">Sub Category</th>
                                                            <th style="text-align:center">Model No</th>
                                                            <th style="text-align:center">Serial No</th>
                                                            <th style="text-align:center">Contract Type</th>
                                                            <th style="text-align:center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="display_contract">
                                                        <?php foreach ($record as $row) { ?>
                                                        <tr>
													
                                                            <td style="text-align:center" id="<?php echo $row['cust_id']; ?>" onClick="hover_tech(this.id,'<?php echo $row['customer_name']; ?>','<?php echo $row['email_id']; ?>','<?php echo $row['contact_number']; ?>','<?php echo $row['alternate_number']; ?>');">
                                                                <a>
                                                                    <?php echo $row[ 'cust_id']; ?>
                                                                </a>
                                                            </td>
                                                            
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'product_name']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'cat_name']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                  <?php echo $row['model_no']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'serial_no']; ?>
                                                            </td>   
                                                            <td style="text-align:center">
                                                                <?php echo $row[ 'contract_type']; ?>
                                                            </td>
                                                                                                                    
                                                            <td style="text-align:center !important">
        <button class="btn btn-circle green btn-outline btn-icon-only" id="<?php echo $row['id']; ?>" onClick="accepts(this.id)"><i class="fa fa-check" aria-hidden="true"></i></button>
        <button class="btn btn-circle red btn-outline btn-icon-only" id="<?php echo $row['id']; ?>" onClick="Rejects(this.id)"><i class="fa fa-trash" aria-hidden="true"></i></button>       
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            
                                      
                                    </div><!--end tab content-->
                                  
                                </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                        <!-- BEGIN FOOTER -->
        <?php include "assets/lib/footer.php"?>
        <!-- END FOOTER -->
            </div>                                              

          <div id="viewimage" class="modal fade" role="dialog">
              <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content">
                        <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">View Image</h4>
                    </div>
                    <div class="modal-body">  
                        <div class="modal-dialog" id="view_imagetech">  
                       </div>
                    </div>
            </div>
          </div>
        </div>
        <div id="myModal1" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">Details</h5>
               </div>
               <div class="modal-body"id='modal_tech'>
                  <form class="form-horizontal" role="form" >
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
               </div>
            </div>
         </div>
      </div>


    <!--Modal Starts-->
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header" >
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id='modal_head'>Details</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id='modal_display'>
						</form>
                     </div>
                     <div class="modal-footer" id="footer_formodal">
                       <!-- <button type="button" class="btn btn-circle blue btn-outline btn-sm" data-dismiss="modal">OK</button>-->
                     </div>
                  </div>
               </div>
            </div>
           
    
    <!-- END QUICK SIDEBAR -->
    <?php include 'assets/lib/javascript.php'?>
    <script>   
        $('.nav.navbar-nav').find('.open').removeClass( 'open' );
        $('#customer_req').addClass('open');
            $(document).ready(function() {
                $('.datatable1').DataTable();
        });
    </script>
    <script>
            
        var company_id="<?php echo $this->session->userdata('companyid');?>";
        function viewbillimage(id)
        {
              $.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_manager/view_billimages",
                    type        :   "POST",
                    data        :   {'id':id,'company':company_id},
                    datatype    :   "JSON", 
                    cache       :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
                                        console.log(data['bill_image']);
                                       if(data['bill_image']=="" ||typeof data['bill_image']=="undefined" )
                                       {
                                          
                                          $('#view_imagetech').empty();
                                           $('#view_imagetech').append('<div id="textforimage"  style="height:200px;background: white;width:95%";><p style="text-align: center;padding-top: 82px;font-size: x-large;">No image available for this Ticket</p></div>');
                                           $('#viewimage').modal('show');
                                       }
                                      else 
                                      {
                                          //console.log(data['image']);
                                           $('#view_imagetech').empty();
                                           $('#view_imagetech').append('<img src="'+data['bill_image']+'" height="300px" width="95%">');
                                           $('#viewimage').modal('show');
                                      }
                                    },
                });
        }
  function accepts(id) {
            
            swal({
                  title: "Are you sure?",
                  text: "You Want To Accept This  Contract Details!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, Accept",
                  cancelButtonText: "No, cancel",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
function(isConfirm) {
if (isConfirm){
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_service/contract_status",
                type: 'POST',
                data: {'id': id,'company_id':company_id},
                success: function(data) {
                         if(data=="You have Approved the Contract"){
                             swal({
                                  title: "Approved",
                                  text: data,
                                  type: "success",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok",
                                  cancelButtonText: "Cancel",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    swal.close();
                                    location.reload();
                                }
                            }); 
                          }
                        else if(data=="This Contract alrdy exists."){
                            swal({
                                      title: "Rejected",
                                      text: "Contract rejected because it already exists",
                                      type: "warning",
                                      showCancelButton: true,
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok",
                                      cancelButtonText: "Cancel",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        swal.close();
                                        location.reload();
                                    }
                                }); 
                     }
           else if(data=="No data found."){
              swal({
                    title: "Rejected",
                    text: "Contract rejected because no data was found",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                  if (isConfirm) {
                    swal.close();
                    location.reload();
                  }
                }); 
           }
                    else {
                              swal("Cancelled", "Something Went Wrong!", "error");
                     }
                }
            });
        }
     else{                          
                   swal.close();
        }
    });  

}
        function Rejects(id) {
            var company_id="<?php echo $this->session->userdata('companyid');?>";
          
            swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover the Contract Details!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, delete",
                  cancelButtonText: "No, cancel",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm) {
                  if (isConfirm) {
                       $.ajax({
                            url   : "<?php echo base_url();?>" + "index.php?/controller_service/contract_reject",
                            type  : 'POST',
                            data  : {'id': id,'company_id':company_id},
                            //dataType: "json",
                           success: function(data) {
                                        if(data == "Something went Wrong"){
                                                  swal("Cancelled", "Something Went Wrong:)", "error");
                                             }
                                      else {
                                         swal({
                                              title: "Deleted",
                                              text: data,
                                              type: "success",
                                              showCancelButton: false,
                                              confirmButtonClass: "btn-danger",
                                              confirmButtonText: "Ok",
                                              cancelButtonText: "Cancel",
                                              closeOnConfirm: false,
                                              closeOnCancel: false
                                        },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                swal.close();
                                                location.reload();
                                            }
                                        }); 
                                      }
                            }
                       });
                  }
                else{
                     swal.close();
                 }
                });    
        }


        function viewimage(id) {

            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/view",
                type: 'POST',
                data: {
                    'id': id
                },
                //dataType: "JSON",
                success: function(data) {

                    var data = JSON.parse(data);
                    //console.log(data['view']);  
                    $("#token").attr('src', data['view']);
                    if (data['view'] == "") {
                        $.dialogbox({
                            type: 'msg',
                            content: 'No Data',
                            closeBtn: true,
                            btn: ['Ok.'],
                            call: [
                                function() {
                                    $.dialogbox.close();
                                }
                            ]

                        });
                    } else {
                        $("#myModal").modal('show');
                    }

                }
            });
        }

		
		 function hover_tech(cust_id,cust_name,cust_email,contact_number,alternate_number)
         {

$('#modal_head').html('Customer Details');
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Customer ID</label><div class="col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+cust_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Name</label><div class=" col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+cust_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Email ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cust_email+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Mobile </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+contact_number+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Alternative No</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+alternate_number+'</div></div></div>');
         $('#myModal').modal('show');
         }
 
    </script>

</body>

</html>