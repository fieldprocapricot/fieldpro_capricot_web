<!DOCTYPE html>
<html lang="en">
   <!--<![endif]-->
   <!-- BEGIN HEAD -->
   <head>
      <?php include 'assets/lib/cssscript.php'?>

     <style>
        .sweet-alert.showSweetAlert.visible{
	     z-index: 999999;
    	     border: 1px solid cadetblue;
	     margin-top: -118px !important;
         }
		 .dropdown-menu{
			  z-index: 999999;
		 }
		 .open>.dropdown-menu {
               display: inherit !important;
         }
		 .dropdown-toggle{
			 cursor: pointer !important;
		 }
     </style>
   </head>
   <!-- END HEAD -->
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
      <!-- BEGIN CONTAINER -->
     <div class="page-wrapper">
      <!-- BEGIN HEADER -->
      <?php include "assets/lib/header_superad.php"?>
      <!-- END HEADER -->
       <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/superad_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
               <div class="col-md-12 col-sm-6">
                  <div class="portlet light bordered">
                     <div class="portlet-title">
                        <div class="col-md-3">
                           <div class="caption">
                              <i class="icon-bubble font-dark hide"></i>
                              <span class="caption-subject font-hide bold uppercase" style="font-size: 21px !important;">Existing Users</span>
                           </div>
                        </div>
                     </div>
   
                           <div class="col-md-12" name="content" id="content">
                           </div>
                           <!-- end col -->
                           <!-- end row -->
                           <div class="row" id="append_values" style="padding:2%;background-color:#fff;">						
                           </div>

                  </div>
               </div>
			    <div class="col-md-12 col-sm-6">
                  <div class="portlet light bordered">
                     <div class="portlet-title">
                        <div class="col-md-3">
                           <div class="caption">
                              <i class="icon-bubble font-dark hide"></i>
                              <span class="caption-subject font-hide bold uppercase" style="font-size: 21px !important;">Blocked Users</span>
                           </div>
                        </div>
                     </div>

                           <div class="col-md-12" name="content" id="content">
                           </div>
                           <!-- end col -->
                           <!-- end row -->
                           <div class="row" id="append_values1" style="padding:2%;background-color:#fff;">						
                           </div>

                  </div>
               </div>
               <!-- END PAGE BASE CONTENT -->
            </div>
			</div>
		</div>
		</div>
            <!-- BEGIN FOOTER -->
            <?php include "assets/lib/footer.php"?>
            <!-- END FOOTER -->
         </div>
 
      <!--Modal -->
      <div id="myModal2" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header" style="border:0">
                  <label class="control-label col-lg-4" style="font-size: 30px !important;">Renew Info</label>
                  <button type="button" class="close" data-dismiss="modal">&times;</button> 
               </div>
               <div class="modal-body">
                  <div class="portlet light bordered" id="form_wizard_1" style="margin-bottom:0">
                     <div class="portlet-body form">
                        <form class="form-horizontal" action="#" id="cust_data" method="POST" novalidate="novalidate">
                           <div class="form-wizard">
                              <div class="form-body">
                               <div class="row" style="display:none">
                                    <label class="control-label col-lg-4"> CompanyID</label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" name="hidden_cid" id="hidden_cid" readonly/>
                                    </div>
                                 </div>
                     
                                 <div class="row">
                                    <label class="control-label col-lg-4"> Company Name </label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" name="cmodal_name" id="cust_id_modal2"  readonly/>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row">
                                    <label class="control-label col-lg-4">No.of Technicians</label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" name="cmodal_tech" id="cmodal_tech" readonly> 
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row">
                                    <label class="control-label col-lg-4">No.of Service </label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" name="cmodal_serv" id="cmodal_serv" readonly> 
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row">
                                    <label class="control-label col-lg-4" >Subscription Plan</label>
                                    <div class="col-lg-8">
                                       <select class="form-control" name="time_yr"  id="time_yr"  data-placeholder="Choose a Category" tabindex="1">
                                          <option value="Select Subscription Plan">Select Subscription Plan</option>
                                          <option value="category1" disabled>1 month Trail</option>
                                          <option value="category2">Quarterly Subscription</option>
                                          <option value="category3">Half-Yearly Subscription</option>
                                          <option value="category4">Annual Subscription</option>
                                       </select>
                                     
                                    </div>
                                 </div>
                               <br>
                            <div class="row">
                               <label class="control-label col-lg-4" >Renewal Date</label>
                                <div class="col-lg-8">
                                  <input class="form-control" type="text" id="cust_id_renew" name="cust_id_renew" readonly>
                                </div>
                            </div>
                                 <br>
								  <div class="row" style="display:none;">
                               <label class="control-label col-lg-4" >Renewal Date</label>
                                <div class="col-lg-8">
                                  <input class="form-control" type="text" id="cust_id_renew1" name="cust_id_renew1" readonly>
                                </div>
                            </div>
                              </div>
                              <div class="form-actions">
                                 <div class="row">
                                    <div class="col-md-12" style="margin-left: 6% !important;">
                                       <div class="col-md-5 col-md-offset-3">
                                          <button type="button" class="btn btn-circle green btn-outline  btn-md " onClick="custdata();">Submit</button>
                                          <button type="cancel" class="btn red-haze btn-outline btn-circle btn-md" id="cancel_butn">Cancel</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div>
		   <div class="modal fade in" id="upgrade_modal">
               <div class="modal-dialog">
                  <form id="cust_data" class="form-horizontal" role="form">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">?</span><span class="sr-only">Close</span></button>
                           <h4 class="modal-title">Service Upgradation</h4>
                           <div class="error" style="display:none">
                              <label id="rowdata"></label>
                           </div>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="form-group col-sm-12" style="display:none">
                                 <label class="control-label col-sm-4" for="email">ID<span class="errors">
                                 </span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control" id="id1" name="id1" placeholder="" type="text" value="" readonly>
                                 </div>
                              </div>
							   <div class="form-group col-sm-12" style="display:none">
                                 <label class="control-label col-sm-4" for="email">ID<span class="errors">
                                 </span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control" id="cid" name="cid" placeholder="" type="text" value="" readonly>
                                 </div>
                              </div>
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-4" for="email">Company Name<span class="errors">
                                 </span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control" id="cname" name="cname" placeholder="" type="text" value="" readonly>
                                 </div>
                              </div>
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-4" for="email">Service Desk Subscribed <span class="errors">
                                 </span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control" id="service_reg" name="service_reg" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-4" for="email">Service Desk Required<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control digit_only" id="service_need" name="service_need" placeholder="" type="text">
                                 </div>
                              </div>
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-4" for="email">Technician Subscribed <span class="errors">
                                 </span>
                                 </label> 
                                 <div class="col-sm-8">
                                    <input class="form-control" id="tech_reg" name="tech_reg" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-4" for="email">Technician Required <span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control digit_only" id="tech_need" name="tech_need" placeholder="" type="text">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-circle green btn-outline" id="add_lic" name="add_lic"><i class="fa fa-check"></i> Upgrade</button>
                           <button type="button" class="btn btn-circle red btn-outline" id="close_upgrade"><i class="fa fa-times"></i> Close</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
		   <div class="modal fade in" id="show_details">
               <div class="modal-dialog">
                  <form id="confirm_licences" class="form-horizontal" role="form">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close change_lic"><span aria-hidden="true">?</span><span class="sr-only">Close</span></button>
                           <h4 class="modal-title">Confirm Upgradation</h4>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="form-group col-sm-12" style="display:none">
                                 <label class="control-label col-sm-6" for="email">Id<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="admin_id" name="admin_id" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                           </div>
							<div class="form-group col-sm-12" style="display:none">
                                 <label class="control-label col-sm-6" for="email">Id<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="comp_id" name="comp_id" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                          
                           <div class="row">
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-6" for="email">Total Service Desk<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="total_service" name="total_service" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-6" for="email">Total Technicians<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="total_techni" name="total_techni" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-circle green btn-outline" id="add_licence" name="add_licence"><i class="fa fa-check"></i> Confirm</button>
                           <button type="button" class="btn btn-circle red btn-outline change_lic" id="change_lic" name="change_lic" ><i class="fa fa-times"></i> Change</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
		  	   <div class="modal fade" id="extend_date">
               <div class="modal-dialog">
                  <form id="ondemand_renewal" class="form-horizontal" role="form">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">?</span><span class="sr-only">Close</span></button>
                           <h4 class="modal-title">Renewal On-Demand</h4>
							 <div class="error" style="display:none">
                              <label id="rowdata1"></label>
                           </div>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="form-group col-sm-12" style="display:none">
                                 <label class="control-label col-sm-6" for="email">Id<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="admin_id" name="admin_id" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                           </div>
							<div class="row">
							<div class="form-group col-sm-12" style="display:none">
                                 <label class="control-label col-sm-6" for="email">Id<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="companyid" name="companyid" placeholder="" type="text" readonly>
                                 </div>
                              </div>
						 </div>
							<div class="row">
							<div class="form-group col-sm-12">
                                 <label class="control-label col-sm-6" for="email">Company Name<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="companyname" name="companyname" placeholder="" type="text" readonly>
                                 </div>
                              </div>
						 </div>
                           <div class="row">
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-6" for="email">Subscription Plan<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="subscription_plan" name="subscription_plan" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-6" for="email">Renewal Date <span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="renewaldate" name="renewaldate" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                           </div>
							  <div class="row">
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-6" for="email">Due Days <span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="extra_date" name="extra_date" placeholder="" type="text">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-circle green btn-outline" id="submit_date" name="submit_date"><i class="fa fa-check"></i> Submit</button>
                           <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal" id="close_date" name="close_date" ><i class="fa fa-times"></i> Close</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
      <div id="myModal1" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="">
               <div class="modal-body">
                  <div class="portlet light bordered" id="form_wizard_1" style="margin-bottom:0">
                     <div class="portlet-title">
                        <label class="control-label" style="font-size: 20px !important;">View Info</label>
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button> 
                     </div>
                     <div class="portlet-body form">
                        <form class="form-horizontal" action="#" id="submit_form1" method="POST" novalidate="novalidate">
                           <div class="form-wizard">
                              <div class="form-body">
                                 <div class="row">
                                    <label class="control-label col-lg-4">Company Name</label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" name="c_name" id="cust_id_modal1" readonly></input>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row">
                                    <label class="control-label col-lg-4">Contact Number</label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" name="c_number" id="cust_modal1" readonly></input>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row">
                                    <label class="control-label col-lg-4">Renewal Date</label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" name="c_renew" id="cust_modal2" readonly></input>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row">
                                    <label class="control-label col-lg-4">Technicians</label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" id="cust_modal3" readonly></input>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row">
                                    <label class="control-label col-lg-4">Service Desk Persons</label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" id="cust_modal4" readonly></input>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row">
                                    <label class="control-label col-lg-4">Subscription Plan</label>
                                    <div class="col-lg-8">
                                       <input type="text" class="form-control" id="cust_modal5" readonly></input>
                                    </div>
                                 </div>
                                 <br>
                            
                              </div>
                              <!--<div class="form-actions">-->
                              <div class="row">
                                 <div class="col-md-12" style="margin-left: 16% !important;">
                                    <div class="col-md-5 col-md-offset-3">
                                       <button type="button" class="btn red-haze btn-outline btn-circle btn-md" id="close_button">Close</button>
                                    </div>
                                 </div>
                              </div>
                              <!--</div>-->
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--/End modal-->
      <!-- END CONTAINER -->
      <!-- BEGIN QUICK SIDEBAR -->
      <!-- END QUICK SIDEBAR -->   
      <?php include 'assets/lib/javascript.php'?>  
		<script>        		
       		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
        	$('#focus_managesubs').addClass('open');
		</script>   
      <script type="text/javascript">
	  window.history.forward();
         $(document).ready(function() {
         $.ajax({
         	url: "<?php echo base_url();?>" + "index.php?/controller_superad/onload",
             type: 'POST',
         	success: function(data) {
         	console.log(data);
         	$('#append_values').html('');							
         			data=JSON.parse(data);
         			console.log(data);
         			for(i=0;i<data.length;i++){	

         if(data[i].block==0){
                if(data[i].company_logo){
                               $('#append_values').append("<div class='col-md-4' style='margin-top:2%'><div class='mt-widget-1'><div class='mt-icon'><div class='dropdown'><div class='dropdown-toggle' id="+data[i].company_id+" type='button' data-toggle='dropdown' style='background:url('http://www.ochoriosholidayvacationtours.com/wp-content/uploads/2015/01/facebook-logo-png-transparent-background-50x50.png'); width:50px;height:50px;'><i class='fa fa-info-circle' style='color:#bfbfbf !important'></i></div><ul class='dropdown-menu manage_dropdown' role='menu' aria-labelledby='menu1'><li role='presentation'><a id="+data[i].company_id+" href='#' role='menuitem' tabindex='-1' data-toggle='modal' onclick='display_modal(this.id)'>View Info</a></li><li role='presentation'><a id="+data[i].company_id+" href='#' role='menuitem' tabindex='-1' data-toggle='modal' onclick='display_modal1(this.id)'>Upgrade Users</a></li><li role='presentation'><a id="+data[i].company_id+" href='#' role='menuitem' tabindex='-1' data-toggle='modal' onclick='display_modal3(this.id)'>Renew OnDemand</a></li></ul></div></div><div class='subs-img'><img src="+data[i].company_logo+" height='80' width='180' style='margin:40px 0px 30px;'></div><div class='mt-body'><h3 class='mt-username'>"+data[i].company_name+"</h3><div class='mt-stats'><div class='btn-group btn-group btn-group-justified'><a id="+data[i].company_id+" class='btn font-green' onclick='display_modal2(this.id);'><i class='fa fa-refresh'></i> Renew </a><a href='javascript:;' class='btn font-black tooltip-test' title='Contract End Date'>"+data[i].renewal_date+"</a><a id="+data[i].admin_id+" onclick='change_block(this.id);' class='btn font-red'><i class='fa fa-ban'></i> block </a></div></div></div></div></div>");
}
else {
                            $('#append_values').append("<div class='col-md-4' style='margin-top:2%'><div class='mt-widget-1'><div class='mt-icon'><div class='dropdown'><div class='dropdown-toggle' id="+data[i].company_id+" type='button' data-toggle='dropdown' style='background:url('http://www.ochoriosholidayvacationtours.com/wp-content/uploads/2015/01/facebook-logo-png-transparent-background-50x50.png'); width:50px;height:50px;'><i class='fa fa-info-circle' style='color:#bfbfbf !important'></i></div><ul class='dropdown-menu manage_dropdown' role='menu' aria-labelledby='menu1'><li role='presentation'><a id="+data[i].company_id+" href='#' role='menuitem' tabindex='-1' data-toggle='modal' onclick='display_modal(this.id)'>View Info</a></li><li role='presentation'><a id="+data[i].company_id+" href='#' role='menuitem' tabindex='-1' data-toggle='modal' onclick='display_modal1(this.id)'>Upgrade Users</a></li><li role='presentation'><a id="+data[i].company_id+" href='#' role='menuitem' tabindex='-1' data-toggle='modal' onclick='display_modal3(this.id)'>Renew OnDemand</a></li></ul></div></div><div class='subs-img'><img src='assets/upload/no-logo.png' height='80' width='180' style='margin:40px 0px 30px;'></div><div class='mt-body'><h3 class='mt-username'>"+data[i].company_name+"</h3><div class='mt-stats'><div class='btn-group btn-group btn-group-justified'><a id="+data[i].company_id+" class='btn font-green' onclick='display_modal2(this.id);'><i class='fa fa-refresh'></i> Renew </a><a href='javascript:;' class='btn font-black tooltip-test' title='Contract End Date'>"+data[i].renewal_date+"</a><a id="+data[i].admin_id+" onclick='change_block(this.id);' class='btn font-red'><i class='fa fa-ban'></i> block </a></div></div></div></div></div>");
   }  		
     }
      
	else
	{
     if(data[i].company_logo){
         $('#append_values1').append("<div class='col-md-4' style='margin-top:2%'><div class='mt-widget-1'><div class='mt-icon'></div><div class='subs-img'><img src="+data[i].company_logo+" height='80' width='180' style='margin:40px 0px 30px;'></div><div class='mt-body'><h3 class='mt-username'>"+data[i].company_name+"</h3><div class='mt-stats'><div class='btn-group btn-group btn-group-justified'><a id="+data[i].admin_id+" class=' btn-sm col-lg-4 col-lg-offset-4' onclick='change_view(this.id);'><i class='fa fa-refresh' ></i> Unblock</a></div></div></div></div></div>");
       }
   else 
   {
    $('#append_values1').append("<div class='col-md-4' style='margin-top:2%'><div class='mt-widget-1'><div class='mt-icon'></div><div class='subs-img'><img src='assets/upload/no-logo.png' height='80' width='180' style='margin:40px 0px 30px;'></div><div class='mt-body'><h3 class='mt-username'>"+data[i].company_name+"</h3><div class='mt-stats'><div class='btn-group btn-group btn-group-justified'><a id="+data[i].admin_id+" class=' btn-sm col-lg-4 col-lg-offset-4' onclick='change_view(this.id);'><i class='fa fa-refresh' ></i> Unblock</a></div></div></div></div></div>");
 
         }
         }
         }
         }
    });
         
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_superad/notification",
             type: 'POST',
         	success: function(data) {
         		$('#content1').html('');
         
         	$('#content1').append('<ul class="list-group list-no-border user-list">');							
         			data=JSON.parse(data);
         		
         			  if(data.length<=0){
                                 $('#content1').append(" <li class='list-group-item'><div class='avatar'>No Companies for Renewal</li>");
         			  }
         			  else {
         			for(i=0;i<data.length;i++){							
         			//var company_logo = data[i].company_logo.replace(new RegExp("\\\\", "g"), "");
                                     var company_logo = data[i].company_logo;
         $('#content1').append(" <li class='list-group-item'><div class='avatar'><img src='"+company_logo+"' width='30px' height='30px' border-radius='25px' alt=''><span class='user-desc'><div class='name'>"+data[i].company_name+"</span></div><div class='time'>"+data[i].renewal_date+"</div></div></li>");
         	}
         }
         $('#content1').append('</ul>');		
         }
         });
                $(document).ready(function() {
                	
           	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_superad/noti_count",
             type: 'POST',
         	success: function(data) {
         		$('.noti-dot').html('');
         	console.log(data);
         	$('.noti-dot').html(data);
                                 }							
         
         });
                    });
         });
             
		//$(".dropdown>[data-toggle='dropdown']").on('click', function(){
                                       //  $(this).parent(".dropdown").toggleClass("open");
                             //});
		  
	   $('#cmodal_tech').on('input', function (event) { 
            this.value = this.value.replace(/[^0-9]/g, '');
       });
           $('#service_need').on('input', function (event) { 
                 this.value = this.value.replace(/[^0-9]/g, '');
          });

           $('#tech_need').on('input', function (event) { 
                 this.value = this.value.replace(/[^0-9]/g, '');
          });
		  
      $('#cmodal_serv').on('input', function (event) { 
              this.value = this.value.replace(/[^0-9]/g, '');
       });
		  
	 $('#add_lic').click(function(){
               	$('#rowdata').empty();
               	var ser_need=$('#service_need').val();
               	var tech_need=$('#tech_need').val();
               	var service_reg=$('#service_reg').val();
               	var tech_reg=$('#tech_reg').val();
               	if(ser_need.match(/^\d+$/) && ser_need!="" )
				//if(ser_need!="" || tech_need!="")
				{
               		var total_service=parseInt(service_reg)+parseInt(ser_need);
               		//var total_tech=parseInt(tech_reg)+parseInt(tech_need);
               		$('#total_techni').val(total_tech);
               		$('#total_service').val(total_service);
               		$('#admin_id').val($('#id1').val());
               		$('#comp_id').val($('#cid').val());
               		$('#upgrade_modal').modal('hide');
               		$('#show_details').modal('show');
					$('#total_techni').val($('#tech_reg').val());					
             
				}
				if(tech_need!="" && tech_need.match(/^\d+$/))
				//if(ser_need!="" || tech_need!="")
				{
               		//var total_service=parseInt(service_reg)+parseInt(ser_need);
               		var total_tech=parseInt(tech_reg)+parseInt(tech_need);
               		$('#total_techni').val(total_tech);
               		$('#total_service').val(total_service);
               		$('#admin_id').val($('#id1').val());
               		$('#comp_id').val($('#cid').val());
               		$('#upgrade_modal').modal('hide');
               		$('#show_details').modal('show');
					$('#total_service').val($('#service_reg').val());					
             
				}
				else{
               		$('#rowdata').append("Fill atleast one of the Mandatory fields.");
                                $('#myModal').animate({ scrollTop: 0 });
                                $('.error').show();
               	}
               });
		 $('#submit_date').click(function(){
			 	$('#rowdata1').empty();
			      var extdays=$("#extra_date").val();
			 	if(extdays.match(/^\d+$/) &&  extdays!=""){
						$.ajax({
                            url         :   "<?php echo base_url(); ?>controller_superad/renew_ondemand",
                            type        :   "POST",
                            data        :   $('#ondemand_renewal').serialize(),// {action:'$funky'}
                            datatype    :  "JSON",  
                            cache       :   false,
                            success     :  function(data){
               						if(data=='true'){
               							$('#extend_date').modal('hide');
               							swal({
                                             		  title: "Due Date Updated!",                        		  
                                             		  type: "success",
                                             		  showCancelButton: false,
                                             		  confirmButtonClass: "btn-danger",
                                             		  confirmButtonText: "Ok!",
                                             		  cancelButtonText: "No, cancel plx!",
                                             		  closeOnConfirm: false,
                                             		  closeOnCancel: false
                                             		},
                                             		function(isConfirm) {
                                             		  if (isConfirm) {
               									window.location.reload();
                                             		  }
                                             		});
               							
               						}else{
               							$('#rowdata1').append(data);
               							$('#edits').animate({ scrollTop: 0 });
               							$('.error').show();
               						}
                                        },
                         });		
				}
			 else {
				 $('#rowdata1').append("Fill no. of Days to extend the Service");
                                $('#extend_date').animate({ scrollTop: 0 });
                                $('.error').show();
			 }
		 });
		
	 $('#close_upgrade').click(function(){
               	$('#upgrade_modal').modal('hide');
		        window.location.reload();
               });
		  
     $('.change_lic').click(function(){
               	$('#show_details').modal('hide');
               	$('#upgrade_modal').modal('show');
               });
		  
       $('#add_licence').click(function(){
               	$.ajax({
                            url         :   "<?php echo base_url(); ?>"+"index.php?/controller_superad/insert_adminlicence",
                            type        :   "POST",
                            data        :   $('#confirm_licences').serialize(),// {action:'$funky'}
                            datatype    :  "JSON",  
                            cache       :   false,
                            success     :  function(data){
               						if(data=='true'){
               							$('#upgrade_modal').modal('hide');
               							$('#show_details').modal('hide');
               							swal({
                                             		  title: "Service Upgraded succesfully",                        		  
                                             		  type: "success",
                                             		  showCancelButton: false,
                                             		  confirmButtonClass: "btn-danger",
                                             		  confirmButtonText: "Ok!",
                                             		  cancelButtonText: "No, cancel plx!",
                                             		  closeOnConfirm: false,
                                             		  closeOnCancel: false
                                             		},
                                             		function(isConfirm) {
                                             		  if (isConfirm) {
               									window.location.reload();
                                             		  }
                                             		});
               							
               						}else{
               							$('#rowdata').append(data);
               							$('#edits').animate({ scrollTop: 0 });
               							$('.error').show();
               						}
                                        },
                         });				
               });
  function change_view(id){
         swal({
          		  title: "Are you sure?",
          		  text: "You want to UnBlock the User!",
          		  type: "warning",
          		  showCancelButton: true,
          		  confirmButtonClass: "btn-danger",
          		  confirmButtonText: "Yes, UnBlock",
          		  cancelButtonText: "No, cancel",
          		  closeOnConfirm: false,
          		  closeOnCancel: false
          		},
          		function(isConfirm) {
          		  if (isConfirm) {
          			   $.ajax({
                			url		: "<?php echo base_url();?>" + "index.php?/controller_superad/set_block",
         					type	: 'POST',
         					data	: {'id':id},
                			//dataType: "json",
               			   success: function(data) {
							   			if(data == "updated"){
										 swal({
											  title: "Alert",
											  text: "UnBlocked Successfully",
											  type: "success",
											  showCancelButton: false,
											  confirmButtonClass: "btn-primary",
											  confirmButtonText: "Ok",
											  cancelButtonText: "Cancel",
											  closeOnConfirm: false,
											  closeOnCancel: false
										},
										function(isConfirm) {
											if (isConfirm) {
												swal.close();
												location.reload();
											}
										
										}); 
		      							} 
							   else{
								   
								//swal(data);
swal({
title: "Alert",
text: "Your Subscription has Expired, Renew to continue Service!",
type: "warning",
showCancelButton: true,
confirmButtonClass: "btn-primary",
confirmButtonText: "Ok",
cancelButtonText: "Cancel",
closeOnConfirm: false,
closeOnCancel: false
},
function(isConfirm) {
if (isConfirm) {
swal.close();
$.ajax({
url : "<?php echo base_url();?>" + "index.php?/controller_superad/fetch_renewcompany",
type : 'POST',
data : {'id':id},
dataType:"JSON", 
success: function(data){
console.log(data);
$('#hidden_cid').val(data['company_id']);
$('#cust_id_modal2').val(data['company_name']);
$('#cmodal_tech').val(data['technicians']);
$('#cmodal_serv').val(data['service_desk']);
$('#cust_id_renew').val(data['renewal_date']);
$('#cust_id_renew1').val(data['renewal_date']);
$('#myModal2').modal('show');
}
});
} 
else{
swal.close();
location.reload();
} 
}); 
							   }

						   }
                       });
          		  }
			 else {
				 swal.close();
			 }
          });    
	}
		  function change_block(id){
         swal({
          		  title: "Are you sure?",
          		  text: "You want to Block the User!",
          		  type: "warning",
          		  showCancelButton: true,
          		  confirmButtonClass: "btn-danger",
          		  confirmButtonText: "Yes, Block",
          		  cancelButtonText: "No, cancel",
          		  closeOnConfirm: false,
          		  closeOnCancel: false
          		},
          		function(isConfirm) {
          		  if (isConfirm) {
          			   $.ajax({
                			url		: "<?php echo base_url();?>" + "index.php?/controller_superad/set_unblock",
         					type	: 'POST',
         					data	: {'id':id},
                			//dataType: "json",
               			   success: function(data) {
							   			if(data == "updated"){
										 swal({
											  title: "Alert",
											  text: "Blocked Successfully",
											  type: "success",
											  showCancelButton: false,
											  confirmButtonClass: "btn-primary",
											  confirmButtonText: "Ok",
											  cancelButtonText: "Cancel",
											  closeOnConfirm: false,
											  closeOnCancel: false
										},
										function(isConfirm) {
											if (isConfirm) {
												swal.close();
												location.reload();
											}
										
										}); 
		      							} 
							   else{
								   
								swal.close();
							   }

						   }
                       });
          		  }
			 else {
				 swal.close();
			 }
          		});    
			}
         
         function show_modal(id){			
         $.ajax({
         url		: "<?php echo base_url();?>" + "index.php?/controller_superad/fetch_company",
         type	: 'POST',
         data	: {'id':id},
         dataType:"JSON",	
         success	: function(data) {
         			$('#modal_display').html('');
         			console.log(data);
         			$('#modal_display').append('<div class="form-group"><label  class="col-sm-3 control-label"for="inputEmail3">Company  ID</label><div class="col-sm-4 control-label" id="c_name">'+data['company_name']+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Contact Number</label><div class="col-sm-4 control-label">'+data['ad_contact']+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >E-Mail ID</label><div class="col-sm-4 control-label" id="c_mail">'+data['ad_mailid']+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Renewal Date</label><div class="col-sm-4 control-label" id="c_renew">'+data['renewal_date']+'</div></div><div class="form-group row"><div class="form-group text-center m-b-0"><input type="button" data-dismiss="modal" class="btn btn-primary" value="Sendmail" onclick="maildata();"/></div></div>');
         			$('#modalshow').modal('show');
         		}
         });
         }
         function maildata(){
         var reciever = $('#c_mail').text();
         var company = $('#c_name').text();
         var renew = $('#c_renew').text();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_superad/send_renewmail",
         type	: 'POST',
         data	: 'reciever=' + reciever + '&company=' + company + '&renew=' + renew,
         dataType:"JSON",	
         success	: function(data) {
         		$.dialogbox({
         						type:'msg',
         						content:'Mail has been sent!',
         						closeBtn:true,
         						btn:['Ok.'],
         						call:[
         							function(){
         								$.dialogbox.close();
         								$('#modalshow').modal('hide');
         							}
         						]							
         					});
         	}
         });
         }
         function display_modal(id){
         $.ajax({
         url		: "<?php echo base_url();?>" + "index.php?/controller_superad/fetch_company",
         type	: 'POST',
         data	: {'id':id},
         dataType:"JSON",	
         success: function(data){
         console.log(data);
         	$('#cust_id_modal1').val(data['company_name']);
         	$('#cust_modal1').val(data['ad_contact']);
         	$('#cust_modal2').val(data['renewal_date']);
         	$('#cust_modal3').val(data['technicians']);
         	$('#cust_modal4').val(data['service_desk']);
         	$('#cust_modal5').val(data['subs_plan']);
         	$('#myModal1').modal('show');
         }
         });
         }
		  
		 function display_modal1(id){
                    var company_id = id;
			// alert(company_id);
                    $('#rowdata_1').empty();
                    $.ajax({
                        url         :   "<?php echo base_url(); ?>"+"index.php?/controller_superad/get_licence",
                        type        :   "POST",
                        data        :   {'company_id' : company_id},// {action:'$funky'}
                        datatype    :  "JSON",  
                        cache       :   false,
                        success     :  function(data){
                                   //data=$.trim(data);
                                    var data=JSON.parse(data);
                                    console.log(data);
                                    $('#id1').val(data['admin_id']);
                                    $('#cname').val(data['company_name']);
                                    $('#service_reg').val(data['service_desk']);
                                    $('#tech_reg').val(data['technicians']);									
                                    $('#aid').val(data['admin_id']);
                                    $('#cid').val(data['company_id']);
                                    
                                },
                    });
                    $('#upgrade_modal').modal('show');
                
         }
		 
		  function display_modal3(id){
         $.ajax({
         url		: "<?php echo base_url();?>" + "index.php?/controller_superad/fetch_company",
         type	: 'POST',
         data	: {'id':id},
         dataType:"JSON",	
         success: function(data){
         console.log(data);
         	$('#companyname').val(data['company_name']);
         	$('#companyid').val(id);
         	$('#renewaldate').val(data['renewal_date']);
         	$('#subscription_plan').val(data['subs_plan']);
         	$('#extend_date').modal('show');
         }
         });
         }
		  
         function display_modal2(id){
        //alert(id);
         $.ajax({
         url    : "<?php echo base_url();?>" + "index.php?/controller_superad/fetch_company",
         type	: 'POST',
         data	: {'id':id},
         dataType:"JSON",	
         success: function(data){
         console.log(data);
                $('#hidden_cid').val(id);
         	$('#cust_id_modal2').val(data['company_name']);
                $('#cmodal_tech').val(data['technicians']);
                $('#cmodal_serv').val(data['service_desk']);
                $('#cust_id_renew').val(data['renewal_date']);
                $('#cust_id_renew1').val(data['renewal_date']);
         	$('#myModal2').modal('show');
         }
         });
         }
         
         function custdata(){
           var c_id=$('#hidden_cid').val();
            var cust_id_renew= $('#cust_id_renew').val();
            $.ajax({
            type:"POST",
            url:"<?php echo base_url();?>" + "controller_superad/fetch_renewdate",
            data:'company=' + c_id,
         	success:function(data){
         	 console.log(data);
               var ans=JSON.parse(data);
               //alert(ans['renewal_date']);
               //return false;
         if (cust_id_renew ==ans['renewal_date']){
              swal("No Changes made!")
                  // alert ("No Changes");
                  //return false;
               }
         else
           {
              var comp=$('#cust_id_modal2').val();
         $.ajax({
            type:"POST",
         url:"<?php echo base_url();?>" + "controller_superad/renew_info",
         data: $('#cust_data').serialize(),
         	success:function(data){
         	 console.log(data);
         	 if(data=="inserted"){
                      $('#myModal2').modal('hide');
         		
                      swal({
							  title: "Good Job!",
							  text: "Renewed Successfully",
							  type: "success",
							  showCancelButton: true,
							  confirmButtonClass: "btn-primary",
							  confirmButtonText: "Ok",
							  cancelButtonText: "Cancel",
							  closeOnConfirm: false,
							  closeOnCancel: false
						},
						function(isConfirm) {
							if (isConfirm) {
								swal.close();
								location.reload();
							}
						else{
								swal.close();
						   }
			            });
         	            }
                        }
                  });
           } 
        }
         	
       });
     }

         $('#cancel_butn').click(function(){
            $('#myModal2').modal('hide');
         });
         
        $('#close_button').click(function(){
            $('#myModal1').modal('hide');
         });
         
      </script>		
      <script>
         $('#time_yr').change(function(){
             var yr=$( "#time_yr" ).val();
             var date=$("#cust_id_renew1").val();
               //alert(yr);
            $.ajax({
            	url    : "<?php echo base_url();?>"+"controller_superad/renewal_date",
            	method : "POST",
                data   : 'yr=' + yr + '&date=' + date,
            	//data: {'yr': yr},
            	success:function(data){
            	console.log(data);
                // alert(data);
            	  $('#cust_id_renew').val(data);
            	}
            });
            
              });
      </script>
   </body>
</html>