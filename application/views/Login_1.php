<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include 'assets/lib/cssscript.php'?>
    <style>
		/*
		 * Specific styles of signin component
		 */
		/*
		 * General styles
		 */
		/* body, html {
			height: 100%;
			background-repeat: no-repeat;
			//background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));
			background-image: url('assets/global/img/background/loginbg.jpg');
		} */
		.background-image {
			  position: fixed;
			  left: 0;
			  right: 0;
			  z-index: -1;
			  display: block;
			  //background-image: url('assets/global/img/background/loginbg.jpg');
			  background-image: url('http://ae.ucr.edu/slim/gen3.png');
			  background-size:cover;
			  //background-position:center center;
			  width: 100%;
			  height: 150%;

			  /* -webkit-filter: blur(10px);
			  -moz-filter: blur(10px);
			  -o-filter: blur(10px);
			  -ms-filter: blur(10px);
			  filter: blur(10px); */
			-webkit-animation: 85s bg linear infinite;
			  animation: 85s bg linear infinite;

			}
		.background-image::after{
			content: '';
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			background-color: #000;
			opacity: .6; 
		}	
		/* body::after{
			content: '';
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			background-color: #000;
			opacity: .6; 
		} */		
		@-webkit-keyframes bg {
		  0% {
			-webkit-transform: translateY(0);
			transform: translateY(0);
		  }
		  50% {
			-webkit-transform: translateY(-30%);
			transform: translateY(-30%);
		  }
		}
		@-webkit-keyframes bg2 {
		  0% {
			-webkit-transform: translateY(100%);
			transform: translateY(100%);
		  }
		  100% {
			-webkit-transform: translateY(0%);
			transform: translateY(0%);
		  }
		}
		@keyframes bg {
		  0% {
			-webkit-transform: translateY(0);
			transform: translateY(0);
		  }
		  50% {
			-webkit-transform: translateY(-30%);
			transform: translateY(-30%);
		  }
		}
		@keyframes bg2 {
		  0% {
			-webkit-transform: translateY(100%);
			transform: translateY(100%);
		  }
		  100% {
			-webkit-transform: translateY(0%);
			transform: translateY(0%);
		  }
		}
		.card-container.card {
			max-width: 350px;
			padding: 40px 40px;
		}		
		.btn {
			font-weight: 700;
			height: 36px;
			-moz-user-select: none;
			-webkit-user-select: none;
			user-select: none;
			cursor: default;
		}		
		/*
		 * Card component
		 */
		.card {
			//background-color: #F7F7F7;
			background-color: rgba(0, 0, 0, 0.3);
			/* just in case there no content*/
			padding: 20px 25px 30px;
			margin: 0 auto 25px;
			margin-top: 15%;
			border: 2px solid #fff;
			/* shadows and rounded borders */
			-moz-border-radius: 2px;
			-webkit-border-radius: 2px;
			border-radius: 2px;
			-moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
			-webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
			box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
			color: #fff;
		}
		
		.profile-img-card {
			width: 96px;
			height: 96px;
			margin: 0 auto 10px;
			display: block;
			-moz-border-radius: 50%;
			-webkit-border-radius: 50%;
			border-radius: 50%;
		}
		
		/*
		 * Form styles
		 */
		.profile-name-card {
			font-size: 16px;
			font-weight: bold;
			text-align: center;
			margin: 10px 0 0;
			min-height: 1em;
		}
		
		.reauth-email {
			display: block;
			color: #404040;
			line-height: 2;
			margin-bottom: 10px;
			font-size: 14px;
			text-align: center;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
			-moz-box-sizing: border-box;
			-webkit-box-sizing: border-box;
			box-sizing: border-box;
		}
		
		.form-signin #inputEmail,
		.form-signin #inputPassword {
			direction: ltr;
			height: 44px;
			font-size: 16px;
		}
		
		.form-signin input[type=email],
		.form-signin input[type=password],
		.form-signin input[type=text],
		.form-signin button {
			width: 100%;
			display: block;
			margin-bottom: 10px;
			z-index: 1;
			position: relative;
			-moz-box-sizing: border-box;
			-webkit-box-sizing: border-box;
			box-sizing: border-box;
		}
		
		.form-signin .form-control:focus {
			border-color: rgb(104, 145, 162);
			outline: 0;
			-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
			box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
		}
		
		.btn.btn-signin {
			/*background-color: #4d90fe; */
			background-color: rgb(104, 145, 162);
			/* background-color: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
			padding: 0px;
			font-weight: 700;
			font-size: 14px;
			height: 36px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			border-radius: 3px;
			border: none;
			-o-transition: all 0.218s;
			-moz-transition: all 0.218s;
			-webkit-transition: all 0.218s;
			transition: all 0.218s;
		}
		
		.btn.btn-signin:hover,
		.btn.btn-signin:active,
		.btn.btn-signin:focus {
			background-color: rgb(12, 97, 33);
		}
		
		.forgot-password {
			color: rgb(104, 145, 162);
		}
		
		.forgot-password:hover,
		.forgot-password:active,
		.forgot-password:focus{
			color: rgb(12, 97, 33);
		}
		.login-button {
			position: absolute;
			//right: -25px;
			margin-left: 7%;
			top: 26%;
			background: #353029;
			color: #999999;
			padding: 11px 0;
			width: 75px;
			height: 75px;
			margin-top: -25px;
			border: 5px solid #fff;
			border-radius: 100%;
			transition: all ease-in-out 500ms;
		}
		@keyframes animatedBackground {
			from { background-position: 0 0; }
			to { background-position: 100% 0; }
		}
	</style>
</head>

<body>	
	<div class="background-image"></div>
	<?php
        if (isset($this->session->userdata['logged_in'])) {        
        header("location: http://localhost/metronic/Login/user_login_process");
        }
    ?>
    <?php
		if (isset($message_display)) {
		echo "<div class='message'>";
			echo $message_display;
		echo "</div>";
		}
	?>
	<!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
    <div class="container">
        <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <!--<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />-->
            <p id="profile-name" class="profile-name-card"></p>
            <?php echo form_open('Login/login'); ?>
			<?php
				echo "<div class='error_msg'>";
				if (isset($error_message)) {
				echo $error_message;
				}
				echo validation_errors();
				echo "</div>";
            ?>
            <!--<label>UserName :</label>
            <input type="text" name="username" id="name" placeholder="username"/><br /><br />
            <label>Password :</label>
            <input type="password" name="password" id="password" placeholder="**********"/><br/><br />
            <input type="submit" value=" Login " name="submit"/><br />-->
            <form class="form-signin">
                <span id="reauth-email" class="reauth-email"></span>
				<img class="img-circle login-button" id="img_logo" src="assets/global/img/background/theme.png">
                <div class="form-group">
                    <label for="username">Email:</label>
                    <input type="username" id="username" name="username" class="form-control" placeholder="Email address" required autofocus>
            	</div>
              	<div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
              	</div>
              	<div class="checkbox">
	                <label><input type="checkbox"> Remember me</label>
              	</div>
               	 	<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
                
                <!--<div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>-->
                <!--<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>-->
            </form><!-- /form -->
            <a href="#" class="forgot-password">
                Forgot the password?
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->
    <?php include 'assets/lib/javascript.php'?>
    <script>
		$( document ).ready(function() {
			// DOM ready
		
			// Test data
			/*
			 * To test the script you should discomment the function
			 * testLocalStorageData and refresh the page. The function
			 * will load some test data and the loadProfile
			 * will do the changes in the UI
			 */
			// testLocalStorageData();
			// Load profile if it exits
			loadProfile();
		});
		
		/**
		 * Function that gets the data of the profile in case
		 * thar it has already saved in localstorage. Only the
		 * UI will be update in case that all data is available
		 *
		 * A not existing key in localstorage return null
		 *
		 */
		function getLocalProfile(callback){
			var profileImgSrc      = localStorage.getItem("PROFILE_IMG_SRC");
			var profileName        = localStorage.getItem("PROFILE_NAME");
			var profileReAuthEmail = localStorage.getItem("PROFILE_REAUTH_EMAIL");
		
			if(profileName !== null
					&& profileReAuthEmail !== null
					&& profileImgSrc !== null) {
				callback(profileImgSrc, profileName, profileReAuthEmail);
			}
		}
		
		/**
		 * Main function that load the profile if exists
		 * in localstorage
		 */
		function loadProfile() {
			if(!supportsHTML5Storage()) { return false; }
			// we have to provide to the callback the basic
			// information to set the profile
			getLocalProfile(function(profileImgSrc, profileName, profileReAuthEmail) {
				//changes in the UI
				$("#profile-img").attr("src",profileImgSrc);
				$("#profile-name").html(profileName);
				$("#reauth-email").html(profileReAuthEmail);
				$("#inputEmail").hide();
				$("#remember").hide();
			});
		}
		
		/**
		 * function that checks if the browser supports HTML5
		 * local storage
		 *
		 * @returns {boolean}
		 */
		function supportsHTML5Storage() {
			try {
				return 'localStorage' in window && window['localStorage'] !== null;
			} catch (e) {
				return false;
			}
		}
		
		/**
		 * Test data. This data will be safe by the web app
		 * in the first successful login of a auth user.
		 * To Test the scripts, delete the localstorage data
		 * and comment this call.
		 *
		 * @returns {boolean}
		 */
		function testLocalStorageData() {
			if(!supportsHTML5Storage()) { return false; }
			localStorage.setItem("PROFILE_IMG_SRC", 		"//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" );
			localStorage.setItem("PROFILE_NAME", "César Izquierdo Tello");
			localStorage.setItem("PROFILE_REAUTH_EMAIL", "oneaccount@gmail.com");
		}
	</script>
</body>
</html>