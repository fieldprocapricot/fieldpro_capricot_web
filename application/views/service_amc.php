<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
<?php $company_id=$this->session->userdata('companyid');
         $region=$user['region'];$area=$user['area'];$location=$user['location'];
         include 'assets/lib/cssscript.php';?>
        
		<style>
		
        #errmsg1
        {
           color: red;
        }		
	.fa-big{
          font-size: 16px !important;
     }
    #span-button:hover
	{ cursor: pointer !important;}
    
   .sweet-alert.showSweetAlert.visible{
	  z-index: 999999;
    	  border: 1px solid cadetblue;
	  margin-top: -118px !important;
    }
     </style>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
	 <div class="page-wrapper">
     <?php
		//$start_date =date("d/m/Y");						
			 ?>
            <!-- BEGIN HEADER -->
            <?php include "assets/lib/header_service.php"?>
            <!-- END HEADER -->
            <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/service_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
				  
				   <div class="tab-pane" id="tab_2">
                                        <div class="portlet box dark">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                   Raise Contract</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <!--<a href="javascript:;" class="remove"> </a>-->
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form class="form-horizontal" id="form_sample_3">
                                                    <div class="form-body">
                                                        <h3 class="form-section">Customer Info</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4" style="padding-left: 0px !important;">Contact Number
																	 <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
																	<div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-phone"></i>
                                                                    </span>
                                                                         <input type="text" name="contact_num" id="contact_num" data-required="1" class="form-control"  placeholder="Enter Contact Number to Search" required/> 
                                                                       <!-- <span class="help-block"> This is inline help </span>-->
																	 <span class="input-group-btn">
                                                                       <button class="btn blue" type="button" id="search">Go!</button>
                                                                    </span>
                                                                    </div> </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                         <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Customer ID
																	 <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                       <input type="text" required readonly class="form-control" id="cust_id" name="cust_id" />
                                                                        <!--<span class="help-block"> Select your gender. </span>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                             <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Name
																	 <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
																	<input type="text" name="cname" id="cname" data-required="1" class="form-control" readonly/>
                                                                        <!--<input type="text" class="form-control" placeholder="dd/mm/yyyy">--> 
																		</div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                              <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Email Address
																	<span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                         <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-envelope"></i>
                                                                    </span>
																	  <input type="text" readonly class="form-control" name="email" id="email" placeholder="Email Address" /> 
																	</div>
                                                                        <!--<span class="help-block"> Select your gender. </span>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
														<div class="row">
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                            <label class="control-label col-md-4" style="padding-left: 0px !important;">
Alternate Number
																	</label>
																	   <div class="col-md-8">
                                                                         <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-phone"></i>
                                                                    </span>
																	 <input type="text" readonly class="form-control" name="altnum" id="altnum" /> 
																	</div>
                                                                        <!--<input type="text" class="form-control" placeholder="dd/mm/yyyy">--> 
																		</div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                
														<!-- New Section-->
                                                        <h3 class="form-section">Address</h3>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Door/ Plot no.</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="door" id="door" data-required="1" class="form-control" readonly/></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Street/ Locality</label>
                                                                    <div class="col-md-8">
                                                                         <input type="text" name="street" id="street" data-required="1" class="form-control" readonly/></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                       <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">Town</label>
                                                                <div class="col-md-8">
                                                      <input type="text" class="form-control" name="town" id="town" data-required="1" readonly/>
																	
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Land mark</label>
                                                                    <div class="col-md-8">
                                                                   <input type="text" class="form-control" name="land_mark" id="land_mark" data-required="1" readonly/>
																	
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">City</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="city" id="city" data-required="1" class="form-control" readonly/> </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">State</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="state" id="state" data-required="1" class="form-control" readonly/></div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
															 <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Country</label>
                                                                   <div class="col-md-8">
																	 <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-globe"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control" id="country" name="country" readonly> </div>
																</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Post Code</label>
                                                                   <div class="col-md-8">
																	 <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-map-marker"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control" id="pin" name="pin" readonly> </div>
																</div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                           
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
													 <h3 class="form-section" id="contract_div" style="display:none;">Contract Info</h3>
														 <div class="row">
															<div class="table-responsive" id="prod_table" style="display:none;">
														  <table id="datatable11" class="table table-hover ">
															<thead>
															  <tr>
																<th>Product Category</th>
																<th>Sub-Category</th>
																 <th>Serial No</th>
																<th>Contract Type</th>
																<th>Expiry Date</th>
																<th>Action</th>
															  </tr>
															</thead>
															<tbody id="dynamic_table">
															</tbody>
														  </table>
														</div>
													  </div>
													  <!--/row-->
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-3 pull-right">
				          <button type="button" id="new_contract" class="btn btn-circle green btn-outline  btn-md tooltip-test" title='Raise New Contract'>New Contract</button> 
                                                                  <button type="reset" id="clear_contract" class="btn red-haze btn-outline btn-circle btn-md">Reset</button>
                                                             </div>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                           
                                        </div>
                                    </div>
				            </div>
						</div><!--container-->
						</div>
                <!-- BEGIN FOOTER -->
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
            </div>
		
		<!-- Modal Starts-->
		    <div id="myModal1" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
               <!-- Modal content-->
               <div class="modal-content">
                 <div class="modal-header" style="border:0">           
                     <div class="error" style="display:none">
                        <label id="rowdata"></label>
                     </div>
                  </div>
                  <div class="modal-body">
                  	<div class="portlet light bordered" id="form_wizard_1" style="margin-bottom:0">
                        <div class="portlet-title" style="margin-top: -27px !important;">
                            
							<div class="actions">
							<button type="button" class="close" data-dismiss="modal" style="margin-top: 20px !important;">&times;</button>   
							</div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                                <div class="form-wizard">
                                    <div class="form-body">
                                        <ul class="nav nav-pills nav-justified steps">
                                            <li class="active">
                                                <a href="#tab1" data-toggle="tab" class="step" aria-expanded="true">
                                                    <span class="number"> 1 </span>
                                                    <span class="desc">
                                                        <i class="fa fa-check"></i> Customer Info</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab2" data-toggle="tab" class="step">
                                                    <span class="number"> 2 </span>
                                                    <span class="desc">
                                                        <i class="fa fa-check"></i>Product Info</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab3" data-toggle="tab" class="step">
                                                    <span class="number"> 3 </span>
                                                    <span class="desc">
                                                        <i class="fa fa-check"></i>Confirm Details</span>
                                                </a>
                                            </li>
                                        </ul>
                                      
                                        <div class="tab-content">
                                            <div class="alert alert-danger display-none">
                                                <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                           
                                            <div class="tab-pane active" id="tab1">
                                                <h3 class="block" style="border-bottom:1px solid #ddd !important">Customer details</h3>
                                               
                                               <div class="row" style="visibility:hidden">
                                                <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4" style="padding-right: 0%;">Unique row id
                                                                     <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                 <input type="text" name="unique_rowid" id="unique_rowid" class="form-control" readonly/> 
                                                                 
                                                                    </div> 
                                                                </div>
                                                           </div>
                                               </div>
                                               <div class="row">
													<div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4" style="padding-right: 0%;">Contact Number
																	 <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                 <input type="text" name="number" id="number" data-required="1" class="form-control" readonly/> 
                                                                 
                                                                    </div> 
																</div>
                                                           </div>
                                                    
                                                          <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Ticket ID
																	<span class="required" aria-required="true"> * </span>
																	</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="tick_id" id="tick_id" readonly class="form-control" /> </div>
                                                                </div>
                                                            </div>
															
                                                  </div>
		                          <div class="row" style="display:none">
							<div class="col-md-6">
                                                        <div class="form-group">
							<label class="control-label col-md-4" style="padding-right: 0%;">AMC ID 
							<span class="required"> * </span></label>
							<div class="col-md-8">
																	
                                              <input type="text" name="amc_id" id="amc_id" data-required="1" class="form-control"/> 
					                  </div> 
							       </div>
								</div>
						   </div>
                                               <div class="row">
														<div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Customer ID
																	<span class="required" aria-required="true"> * </span>
																	</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="c_id" id="c_id" required class="form-control" readonly/> </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Name
																	<span class="required" aria-required="true"> * </span> </label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="name" id="name" readonly required class="form-control" readonly/></div>
                                                                </div>
                                                            </div>
															
                                                            <!--/span-->
                                                           
                                                            <!--/span-->
                                                </div>
                                                <div class="row">
													<div class="col-md-6">
                                                                <div class="form-group">
                          
																	<label class="control-label col-md-4">Email Id
																	<span class="required" aria-required="true"> * </span>
																	</label>
                                                                    <div class="col-md-8">
                                             <input type="text" name="cust_mail" id="cust_mail" required class="form-control" /> </div>
                                                                </div>
                                                    </div>
                                                   <div class="col-md-6">
                                                                <div class="form-group">
                                                                 <label class="control-label col-md-4" style="padding-left: 0% !important;">
																 Alternate-Number </label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="anum" id="anum" class="form-control" /></div>
                                                                </div>
                                                    </div>
                                                </div>
												 <h3 class="block" style="border-bottom:1px solid #ddd !important">Address Details</h3>
												 <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Door/ Plot no.
                                                       <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="doornum" id="doornum" required class="form-control" /></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Street/ Locality <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                         <input type="text" name="address" id="address" required class="form-control" /></div>
                                                                </div>
                                                            </div>
                                                        </div>
												         <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Town
                                                                         <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="cont_town" id="cont_town" class="form-control" required />                                                                       </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Land mark</label>
                                                                    <div class="col-md-8">
                                                                         <input type="text" name="cont_ldmrk" id="cont_ldmrk" class="form-control"/>                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">City
                                                                              <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                         <input type="text" name="addr" id="addr" required class="form-control" required/></div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
															 <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Country
                                                                          <span class="required"> * </span></label>
                                                                   <div class="col-md-8">
                                                                    <select name="ccountry" id="ccountry" required class="form-control"></select> 
																</div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
															<div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">State
                                                                              <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                        <select name="cstate" id="cstate" required class="form-control"></select>
																	</div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Post Code
                                                                            <span class="required"> * </span></label>
                                                                   <div class="col-md-8">
																	 
                                                                    <input type="text" id="postcode" name="postcode" required class="form-control"> 
																</div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                           
                                                        </div>
                                             </div>   
                                            <div class="tab-pane" id="tab2">
                                               
												<h3 class="block" style="border-bottom:1px solid #ddd !important">Provide Product Info</h3>
												<!--<a onclick="add_text();" class="btn font-black tooltip-test" title="Add Product"><i class="fa fa-plus-circle fa-2x"></i></a>-->
						  <div class="row" style="display:none">
							<div class="col-md-6">
                                                        <div class="form-group">
							<label class="control-label col-md-4" style="padding-right: 0%;">productid
							<span class="required"> * </span></label>
							<div class="col-md-8">
																	
                                              <input type="text" name="cust_prod" id="cust_prod" data-required="1" class="form-control"/> 
					                  </div> 
							   </div>
                                                       </div>
                                                <div class="col-md-6">
                                                        <div class="form-group">
							<label class="control-label col-md-4" style="padding-right: 0%;">catID 
							<span class="required"> * </span></label>
							<div class="col-md-8">
																	
                                              <input type="text" name="cust_cat" id="cust_cat" data-required="1" class="form-control"/> 
					                  </div> 
							       </div>
								</div>
						   </div>						
                                                <div class="row">                                                   
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Product-Category
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" name="cust_prodname" id="cust_prodname" required class="form-control" readonly> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Sub-Category
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                       <input type="text" name="cust_catname" id="cust_catname" required class="form-control" readonly> 
                                                        </div>
                                                    </div>
                                                </div>
												<div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Model No. <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control" id="model_no" name="model_no" required readonly>
                                                        </div>
                                                    </div>
                                                              <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Contract Type
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <!-- <input type="text" name="contract_type" id="contract_type" required class="form-control" readonly>  -->
                                                            <select class="form-control" name="contract_type" id="contract_type" required="" aria-required="true">
                                                <option selected>Contract Type</option>
                                                <option value="Warranty">Warranty</option>
                                                <option value="AMC">AMC</option>
                                                <option value="CTPL AMC">CTPL AMC</option>
                                                <option value="Post Warranty">Post Warranty</option>
                                                <option value="Carepack">Carepack</option>
                                                <option value="Non-contract">Non-contract</option></select>
                                                        </div>
                                                    </div>
								
                                                </div>
												
                                                <div class="row">                                                   
                                                  <div class="form-group col-md-6 col-sm-12">
						<label class="control-label col-md-5">Serial No.  <span class="required" aria-required="true"> * </span>  </label>
									 				  
							<div class="col-md-7">
							<input type="text" required class="form-control" id="s_no" name="s_no" readonly> <!--- raise contract -->
								</div>
								</div>
				   <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Quantity
                                                           <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" min="0" max="99" name="call_tag" id="call_tag" required class="form-control" value='1' readonly> <!--- raise contract -->
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="row" style="display:none">
												   <div class="form-group col-md-6 col-sm-12">
												   <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="com_id" name="com_id" value="<?php echo $this->session->userdata('companyid');?>" readonly>
                                                        </div>
												   </div>
												</div>
												<div class="row">                                                   
                                                   
													   <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5" style="
    padding-left: 0px !important;">Preferred Datetime
                                                                <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" size="16" id="preferred_date" name="preferred_date" class="form-control">
                                                        
                                                        </div>
                                                    </div>
                                                   <div class="form-group col-md-6 col-sm-12">
                                                   <label class="control-label col-md-5" style="
    padding-left: 0px !important;">Start date<span class="required" aria-required="true"> * </span></label>
    <div class="col-md-7">
    <input name="startdate" id="startdate" type="text"  placeholder="Start Date" data-date-format="dd/mm/yyyy" class="form-control" style="border-radius: 0px !important;" /></div>
                                                   </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5" style="
    padding-left: 0px !important;">Contract Period(In months)<span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="number" min="0" max="999" id="raised_contract_period" name="raised_contract_period" class="form-control">
                                                        
                                                        </div>
                                                    </div>
                                                    
                                                </div> 

											   
                                              </div>
											   <div class="tab-pane" id="tab3">
                                                <h3 class="block" style="border-bottom:1px solid #ddd !important">Customer details</h3>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Ticket ID:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="tick_id"> </p>
                                                        </div>
                                                    </div>	
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Name:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="name"> </p>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="row">											
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Email ID:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="cust_mail"> </p>
                                                        </div>
                                                    </div>												
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Contact Number:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="number"> </p>
                                                        </div>
                                                    </div>
                                                </div>
												<h3 class="block" style="border-bottom:1px solid #ddd !important">Product Info</h3>
												<div class="row">											
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Product-Category:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="cust_prodname"> </p>
                                                        </div>
                                                    </div>												
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Sub-Category:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="cust_catname"> </p>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="row">											
                                                   											
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Contract Type :</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="contract_type"> </p>
                                                        </div>
                                                    </div>
                                                   <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Date-Time of Visit:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="preferred_date"> </p>
                                                        </div>
                                                    </div>	
                                                </div>  
                                            </div> 
                                           </div>
					 </div>
                                    </div>
                                    <div class="form-actions" style="border-top:1px solid #ddd !important">
                                        <div class="row">
                                            <div class="col-md-offset-5 col-md-7">
                                                <a href="javascript:;" class="btn btn-circle red btn-outline button-previous disabled" style="display: none;">
                                                    <i class="fa fa-angle-left"></i> Back </a>
                                                <a href="javascript:;" class="btn btn-circle blue btn-outline button-next"> Continue
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                                <a href="javascript:;" class="btn btn-circle green btn-outline button-submit" style="display: none;" id="raise_amcbutton"> Submit
                                                    <i class="fa fa-check"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                  </div>
                 
               </div>
            </div>
		<!--/end Modal-->
  
		<!--new contract-->
		<div id="myModal3" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
               <!-- Modal content-->
               <div class="modal-content">
                 <div class="modal-header" style="border:0">           
                     <div class="error" style="display:none">
                        <label id="rowdata_cust"></label>
                     </div>
                  </div>
                  <div class="modal-body">
                  	<div class="portlet light bordered" id="form_wizard_4" style="margin-bottom:0">
                        <div class="portlet-title" style="margin-top: -27px !important;">
                            
							<div class="actions">
							<button type="button" class="close" data-dismiss="modal" style="margin-top: 20px !important;">&times;</button>   
							</div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal" action="#" id="submit_form_3" method="POST" novalidate="novalidate">
                                <div class="form-wizard">
                                    <div class="form-body">
                                        <ul class="nav nav-pills nav-justified steps">
                                            <li class="active">
                                                <a href="#tab4" data-toggle="tab" class="step" aria-expanded="true">
                                                    <span class="number"> 1 </span>
                                                    <span class="desc">
                                                        <i class="fa fa-check"></i> Customer and Contract Info</span>
                                                </a>
                                            </li>
                                           
						<li>
                                                <a href="#tab12" data-toggle="tab" class="step">
                                                    <span class="number"> 2 </span>
                                                    <span class="desc">
                                                        <i class="fa fa-check"></i>Confirm Details</span>
                                                </a>
                                            </li>
                                              
                                        </ul>
                                      
                                        <div class="tab-content">
                                            <div class="alert alert-danger display-none">
                                                <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                           
                                            <div class="tab-pane active" id="tab4">
                                                <h3 class="block" style="border-bottom:1px solid #ddd !important">Customer details</h3>
                                               <div class="row">
													<div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4" style="padding-right: 0%;">Contact Number
																	 <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
																	<div class="input-group">
                                                                 <input type="text" name="modal_number" id="modal_number" data-required="1" class="form-control" placeholder="Enter Contact Number"/> 
                                                                      
																<span class="input-group-btn">
                                                                 <button class="btn blue" type="button" name="cont_search" id="cont_search">Go!</button>
                                                                    </span>
                                                                    </div> 
																	</div>
                                                                </div>
                                                     </div>
															
                                                          <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Ticket ID
																	<span class="required" aria-required="true"> * </span>
																	</label>
                                                                    <div class="col-md-8">
           <input type="text" readonly name="modal_ticket" id="modal_ticket" class="form-control"   /> </div>
                                                                </div>
                                                            </div>
															
                                                            <!--/span-->
                                                           
                                                            <!--/span-->
                                                </div>
												<div class="row" style="display:none">
														<div class="col-md-6">
                                                            <div class="form-group">
																<label class="control-label col-md-4" style="padding-right: 0%;">AMC ID 
																 <span class="required"> * </span></label>
																<div class="col-md-8">
																	<input type="text" name="modal_amc" id="modal_amc" data-required="1" class="form-control"/> 
																</div> 
															</div>
													   </div>
												   </div>
                                               <div class="row">
														<div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Customer ID
																	<span class="required" aria-required="true"> * </span>
																	</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="modal_cid" id="modal_cid" required class="form-control" readonly/> </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Name
																	<span class="required" aria-required="true"> * </span> </label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="modal_cname" id="modal_cname" required class="form-control" readonly/></div>
                                                                </div>
                                                            </div>
															
                                                            <!--/span-->
                                                           
                                                            <!--/span-->
                                                </div>
                                                <div class="row">
													<div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Email Id
																	<span class="required" aria-required="true"> * </span>
																	</label>
                                                                    <div class="col-md-8">
                  <input type="email" name="modal_cmail" id="modal_cmail" readonly required class="form-control" /> </div>
                                                                </div>
                                                    </div>
                                                   <div class="col-md-6">
                                                                <div class="form-group">
                                                                 <label class="control-label col-md-4" style="padding-left: 0% !important;">
																 Alternate-Number </label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="modal_anum" id="modal_anum" class="form-control" /></div>
                                                                </div>
                                                    </div>
                                                </div>
												 <h3 class="block" style="border-bottom:1px solid #ddd !important">Address Details</h3>
												 <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                  <label class="control-label col-md-4">Door/ Plot no.
                                                                    <span class="required" aria-required="true"> * </span>
                                                                  </label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="modal_door" id="modal_door" required class="form-control" /></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Street/ 
                                                        Locality <span class="required" aria-required="true"> * </span>
                                                       </label>
                                                                    <div class="col-md-8">
                                                                         <input type="text" name="modal_address" id="modal_address" required class="form-control" /></div>
                                                                </div>
                                                            </div>
                                                        </div>
												  <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Town
                                                                      <span class="required" aria-required="true"> * </span>
                                                                    </label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="modal_town" id="modal_town" required class="form-control" /></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">land mark</label>
                                                                    <div class="col-md-8">
                                                                         <input type="text" name="modal_lmrk" id="modal_lmrk" class="form-control" /></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">City <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-8">
                                                                         <input type="text" name="modal_addr" id="modal_addr" required class="form-control" /></div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Country <span class="required" aria-required="true"> * </span></label>
                                                                   <div class="col-md-8">
                                                                    <select name="modal_ccountry" id="modal_ccountry" required class="form-control"> 
																	   </select>
																</div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
															
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
															<div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">State <span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-8">
                                                                        <select name="modal_cstate" id="modal_cstate" required class="form-control">
																		</select>
																   </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-4">Post Code <span class="required" aria-required="true"> * </span></label>
                                                                   <div class="col-md-8">
																	 
                                          <input type="text" id="modal_post" name="modal_post" required class="form-control" /> 
																</div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                           
                                                        </div>
													 <h3 class="block" style="border-bottom:1px solid #ddd !important">Product Info</h3>
												              <div class="row">                                                   
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                           <label class="control-label col-md-4" style="padding-left: 1px !important;">
                                                               Product-Category
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-8">
                                                              <select class="form-control" name="modal_prod" id="modal_prod" required > 
							   								</select>
                                                         </div>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                        <label class="control-label col-md-4">Sub-Category
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                       <select name="modal_cat" id="modal_cat" required class="form-control"> 
													   </select>
                                                           </div>
                                                        </div>
                                                    </div>
                                                </div>
						<div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                         <div class="form-group">
                                                        <label class="control-label col-md-4">Model No.
                                                               <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="modal_modelno" name="modal_modelno" required>                          </div>
                                                        </div>
                                                    </div>
                                  <div class="col-md-6 col-sm-12">
                                                         <div class="form-group">
                                                        <label class="control-label col-md-4">Contract Type
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <select name="modal_contract" id="modal_contract" required class="form-control"> 
															</select>
                                                                </div>
                                                             </div>
							</div>		
						
                                                </div>
												
                                                <div class="row">                                                   
                                                  <div class="col-md-6 col-sm-12">
                                                    <div class="form-group">
						<label class="control-label col-md-4">Serial No.
							 <span class="required" aria-required="true"> * </span>
							</label>
							<div class="col-md-8">
						<input type="text" required class="form-control" id="modal_sno" name="modal_sno" /> 
									<span style="color:red;" id="cmmterror"></span>	
								</div>
							  </div>
                                                     </div> 	
                                               <div class="col-md-6 col-sm-12">
                                                       <div class="form-group">
                                                        <label class="control-label col-md-4">Quantity
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
           <input type="text" min="0" max="99" name="modal_quantity" id="modal_quantity" required class="form-control" readonly value='1' />     <!--- raju new contract -->      
		   </div>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="row" style="display:none">
												   <div class="form-group col-md-6 col-sm-12">
												   <div class="col-md-7">
                                                            <input type="text" readonly class="form-control form-control1" id="c_id" name="c_id" value="<?php echo $this->session->userdata('companyid');?>" />
                                                        </div>
												   </div>
												   <div class="form-group col-md-6 col-sm-12">
												   <div class="col-md-7">
                                                        <input type="text" readonly class="form-control form-control1" id="contract_period" name="contract_period" />
                                                        </div>
												   </div>
												</div>
												<div class="row">                                                   
                                                   
			 <div class="col-md-6 col-sm-12">
                                     <div class="form-group">
                                                        <label class="control-label col-md-5">Preferred Date-time
                                                            <span class="required" aria-required="true"> * </span></label>
                                                        <div class="col-md-7">
                                                          <input class="form-control" type="text" required name="date" id="date" value=""  />
                                                        </div>
                                                    </div>
                                                    </div>

                                                    <!--Added by sri hari on feb 23,2019-->
                                                   <div class="col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Contract period(In months)
                                                            <span class="required" aria-required="true"> * </span></label>
                                                        <div class="col-md-7">
      <input class="form-control" required type="number" min="0" max="999" name="new_contract_period" id="new_contract_period" value="" />
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <!--Addition end by sri hari-->
													
												        <!--Added by sri hari on feb 23,2019-->
                                                 <div class="col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Work Type
                                                            <span class="required" aria-required="true"> * </span></label>
                                                        <div class="col-md-7">
                         <select name="modal_worktype" id="modal_worktype" required class="form-control" style="display:none" > 
							 </select>
					<select name="modal_nonworktype" id="modal_nonworktype" required class="form-control"  > 
															</select>
															
                                                        </div>
                                                    </div>
                                                    </div> 
                                                    <!--Addition end by sri hari-->
                                            	
                                             </div>
                                          </div>   
                                         
											
								  <div class="tab-pane" id="tab12">
                                                <h3 class="block" style="border-bottom:1px solid #ddd !important">Contract Info</h3>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Ticket ID:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="modal_ticket"> </p>
                                                        </div>
                                                    </div>	
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Customer ID:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="modal_cid"> </p>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="row">											
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Name:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="modal_cname"> </p>
                                                        </div>
                                                    </div>												
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Contact Number:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="modal_number"> </p>
                                                        </div>
                                                    </div>
                                                </div>
												<!--<h3 class="block" style="border-bottom:1px solid #ddd !important">Product Info</h3>-->
												<div class="row">											
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Product-Category:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="modal_prod"> </p>
                                                        </div>
                                                    </div>												
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Sub-Category:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="modal_cat"> </p>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="row">											
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Model No:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="modal_modelno"> </p>
                                                        </div>
                                                    </div>												
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Contract Type :</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static" data-display="modal_contract"> </p>
                                                        </div>
                                                    </div>
                                                </div> 
                                              </div>
                                           
                                             </div>
					 </div>
                                    </div>
                                    <div class="form-actions" style="border-top:1px solid #ddd !important">
                                        <div class="row">
                                            <div class="col-md-offset-5 col-md-7">
                                
                                                <a href="javascript:;" class="btn btn-circle red btn-outline button-previous disabled" style="display: none;">
                                                    <i class="fa fa-angle-left"></i> Back </a>
                                                <a href="javascript:;" class="btn btn-circle blue btn-outline button-next"> Continue
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                                <a href="javascript:;" class="btn btn-circle green btn-outline button-submit" style="display: none;" id="contract_button">                                                                  Submit
                                                    <i class="fa fa-check"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                  </div>
                 
               </div>
            </div>

 <!--loading model-->
          <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->			
		
		<?php include 'assets/lib/javascript.php'?>

		
			  <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script> 
        var startdate =  $('#startdate').val();

		   $(window).load(function(){				
               	$('.nav.navbar-nav').find('.open').removeClass( 'open' );
               	$('#service_amc').addClass('open');
			   
				   $('#ccountry').focus(function(){
						$('#ccountry').empty();
						populateCountries("ccountry", "cstate");
				  });
				 $('#modal_ccountry').focus(function(){
					  $('#modal_ccountry').empty();
					  populateCountries("modal_ccountry", "modal_cstate");
				});
				   
		   });
            </script> 
	<script type="text/javascript">
	
	$(document).ready(function() {  
		/* $("#postcode").on('keypress',(function (e) {
                     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                $("#postcode").html('');
                            $("#postcode").text("Numbers Only");
               }
          else {
                       if($(this).val().length>5){
                             return false;
                       }
                       
               }
           }));

          $("#modal_post").on('keypress',(function (e) {
                     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                $("#modal_post").html('');
                            $("#modal_post").text("Numbers Only");
               }
          else {
                       if($(this).val().length>5){
                             return false;
                       }
                       
               }
           })); */
	
       

           $("#call_tag").on('keypress',(function (e) {
                     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                               return false;
                  }
            }));

         $("#modal_quantity").on('keypress',(function (e) {
                     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                               return false;
                  }
            }));
	

      /*    $("#anum").keydown(function (e) {
                     // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                       // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, dont do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }); */

      /*  $("#modal_anum").keydown(function (e) {
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                   
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                      
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                      
                 return;
        }
     
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }); */
 
                   /* $("#anum").on('keypress',function(){   
                         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                            $("#anum").html("Numbers Only").show();
                         // $("#postcode").html("Numbers Only").show().fadeOut("slow");
                          return false;
            
                           }
                       }); */

 
$('#preferred_date').appendDtpicker({
				"inline": false,
				"dateFormat": "YYYY-MM-DD hh:mm",
                                "todayButton": true,
                                "futureOnly": true,
                                 "closeOnSelected": true
			});
		
       $('#date').appendDtpicker({
				"inline": false,
				"dateFormat": "YYYY-MM-DD hh:mm",
                                "todayButton": true,
                                "futureOnly": true,
                                 "closeOnSelected": true
			});
		
		
		
			var company_id="<?php echo $this->session->userdata('companyid');?>";
		
	/*	$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_call/load_amctype",
				    type: 'POST',
					data:{'company_id':company_id},
					success: function(data) {
					var result=JSON.parse(data);
							console.log(result);
							$('#modal_contract').append('<option selected disabled>Select Contract-Type</option>');
								 for(i=0;i<result.length;i++)
								 {
								  $('#modal_contract').append('<option value='+result[i]['amc_type']+'>'+result[i]['amc_type']+'</option>');
								 }
					}
			});
			*/
		
			$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_call/load_amctype_raise_tkt_non_contract",
				    type: 'POST',
					data:{'company_id':company_id},
					success: function(data) {
					var result=JSON.parse(data);
							console.log(result);
							$('#modal_contract').append('<option selected disabled>Select Contract-Type</option>');
								 for(i=0;i<result.length;i++)
								 {
								  $('#modal_contract').append('<option value='+result[i]['amc_type']+'>'+result[i]['amc_type']+'</option>');
								 }
					}
			});
		
		
			$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_call/load_worktype",
				    type: 'POST',
					data:{'company_id':company_id},
					success: function(data) {
					var result=JSON.parse(data);
							console.log(result);
							$('#modal_worktype').append('<option selected disabled>Select Work-Type</option>');
								 for(i=0;i<result.length;i++)
								 {
								  $('#modal_worktype').append('<option value='+result[i]['service_group_id']+'>'+result[i]['service_group']+'</option>');
								 }
					}
			});
			
			$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_call/load_product_amc",
				    type: 'POST',
					data:{'company_id':company_id},
					success: function(data) {
					var result=JSON.parse(data);
							console.log(result);
							$('#modal_prod').append('<option selected disabled>Select Product</option>');
								 for(i=0;i<result.length;i++)
								 {
								  $('#modal_prod').append('<option value='+result[i]['product_id']+'>'+result[i]['product_name']+'</option>');
								 }
					}
			});
		
	$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_call/load_nonworktype",
				    type: 'POST',
					data:{'company_id':company_id},
					success: function(data) {
					var result=JSON.parse(data);
							console.log(result);
							$('#modal_nonworktype').append('<option selected disabled>Select Work-Type</option>');
								 for(i=0;i<result.length;i++)
								 {
								  $('#modal_nonworktype').append('<option value='+result[i]['service_group_id']+'>'+result[i]['service_group']+'</option>');
								 }
					}
			});		
		
			
	});
		
	

	/*$('#postcode').on('change',(function(e) {
           		  //if($(this).val().length==0){
                            // alert ("Pincode cannot be Empty!");
                       //} 
 
            if($(this).val().length>0 && $(this).val().length<6){
                             alert ("Pincode should have 6 digits");
                       }
              else 
                return false;
       })); */

     /* $('#modal_post').on('change',(function(e) {
             	 // if($(this).val().length==0){
                        //     alert ("Pincode cannot be Empty!");
                      // } 
 
            if($(this).val().length>0 && $(this).val().length<6){
                             alert ("Pincode should have 6 digits");
                       }
              else 
                return false;
       }));  */

      /* $('#modal_anum').on('change',(function(e) {

             if($(this).val().length>0 && $(this).val().length<10){
                             alert ("Provide Proper Alternate Number");
              }
             else if($(this).val().length==10){
                             return false;
              } 
 
              else {
             }
       })); */ 

       $('#modal_anum').on('change',(function(e) {

               $("#rowdata_cust").empty();
              var inputVal = $(this).val();
				var testReg = /^[0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/;
				if(!testReg.test(inputVal)) {
					//alert("Numeric characters only");
					 $("#rowdata_cust").html('Provide proper Alternate Contact number !');
						$(".error").show();
						 $("#myModal3").animate({scrollTop:0});
				}
		  else{
			  $("#rowdata_cust").empty();
		  }
		 
       }));

	$('#clear_contract').click(function() {
             		$('#contract_div').css({'display':'none'});
             		$('#prod_table').css({'display':'none'});
             		$('#dynamic_table').html('');
          		//contract_div
          		//prod_table
          		//dynamic_table
    	});
 

	$('#search').click(function() {
		//var value=$.trim($("#contact_num").val());
	  if($("#contact_num").is(":blank"))
	  {
			swal("Contact Field Empty!")
	  }
	  else 
	  {
	    var contact_number=$('#contact_num').val();				
			$.ajax({
				url: "<?php echo base_url();?>" + "index.php?/controller_call/fetch",
				type: 'POST',
				data:{'contact_number':contact_number},
			    dataType:"json",				
				success: function(info) {					
					//console.log(info);
					if(info=='Provide Proper Mobile Number'){
						swal("Oops!", "Provide Proper Mobile Number!", "warning")
					}
					else if(info=='No details!') {
						swal({
							title:"Sorry",
							  text: " No details Available!",
							  showCancelButton: false,
							  confirmButtonColor: "#DD6B55",
							  confirmButtonText: "Ok.",
							  cancelButtonText: "No, cancel !",
							  closeOnConfirm: false,
							  closeOnCancel: false
						},
						function(isConfirm){
							if (isConfirm) {
								//$('#myModal3').modal('show');
								swal.close();
							    $('#contact_num').val('');
							} else {
								swal.close();
							}
						});
					}
					else {
						var cus=info[0].customer;
                        var productarray=info[1].product;
						var con_length=info.length-1;						
						var cust=info[con_length].contract;			
					    //var cust=info[4].contract;
						var data1= cus;
							for(k=0;k<data1.length;k++) {
								$('#cust_id').val(data1[k]['customer_id']);
								$('#cname').val(data1[k]['customer_name']);
								$('#email').val(data1[k]['email_id']);
								$('#altnum').val(data1[k]['alternate_number']);
							    $('#door').val(data1[k]['door_num']);
							    $('#street').val(data1[k]['address']);
							    $('#town').val(data1[k]['cust_town']);
                                $('#land_mark').val(data1[k]['landmark']);
                                $('#city').val(data1[k]['city']);  
							    $('#state').val(data1[k]['state']);
							    $('#country').val(data1[k]['cust_country']);
							    $('#pin').val(data1[k]['pincode']);
                                                       }
							//var parsing=JSON.parse(cust);
							var cont= cust;
								 //var cont= $.makeArray(cont1);		
								 console.log(cont);
								 $('#contract_div').css({'display':'block'});
								 $('#prod_table').css({'display':'block'});
								 $('#dynamic_table').html('');
								 for(i=0;i<cont.length;i++)
								 {	
								 var replaceSpace=cont[i].type_of_contract;
								var contract = replaceSpace.replace(/ /g, ":");	
									 
								var replaceSpace=cont[i].address;
								var address = replaceSpace.replace(/ /g, ":");	
									   if(cont[i].cust_town==null)
							{
							var addr = '';
							}
						   else
						   {
                       var replaceSpace=cont[i].cust_town;
                       var addr = replaceSpace.replace(/ /g, ":"); 
						   }
								
									 
                                var land=cont[i].land_mark;
									var location= land.replace(/ /g, ":");	
									 
                                var city=cont[i].city;
									var city= city.replace(/ /g, ":");
									 
                                var state=cont[i].state;
									var state= state.replace(/ /g, ":");	
									 
                               var product_name=cont[i].product_name;
									var product_name= product_name.replace(/ /g, ":");	
									 
                               var cat_name=cont[i].cat_name;
									var cat_name= cat_name.replace(/ /g, ":");	
                            
                               var customer_name=cont[i].customer_name;
									var customer_name= customer_name.replace(/ /g, ":");		
                               var door_num=cont[i].door_num;
									var door_num= door_num.replace(/ /g, ":");	
									 
								if(cont[i].model_no!="")
                                                        {
															 var model=cont[i].model_no;
															 var model= model.replace(/ /g, ":");
                                                        }
 								if(cont[i].serial_no!="")
                                                        {     
															var serial_no=cont[i].serial_no;
									                        var serial_no= serial_no.replace(/ /g, ":");
                                                        }
									 else
									 {
										  var serial_no='';
									 }
								var d = new Date();
								 var month = d.getMonth()+1;
                                                                var day = d.getDate();

                                                            var output = d.getFullYear() + '-' +(month<10 ? '0' : '') + month + '-' +
                                                                       (day<10 ? '0' : '') + day;
									//alert(cont[i]['rowid']);
                                   // console.log(cont[i]);
								if(cont[i]['warrenty_expairy_date']>=output)
							        {
									$('#dynamic_table').append("<tr><td>"+cont[i]['product_name']+"<input class='form-control' type='hidden' name='prod_"+i+"' required id='prod_"+i+"' value='"+cont[i]['product_name']+"' readonly></td><td>"+cont[i]['cat_name']+"<input class='form-control' type='hidden' name='cat_"+i+"' required id='cat_"+i+"' value='"+cont[i]['cat_name']+"' readonly></td><td>"+serial_no+"<input class='form-control' type='hidden' name='serialno_"+i+"' required id='serialno_"+i+"' value='"+serial_no+"'readonly></td><td>"+contract+"<input class='form-control' type='hidden' name='amctype_"+i+"' required id='amctype_"+i+"' value='"+contract+"'readonly></td><td><input class='form-control' type='text' name='date_"+i+"' id='date_"+i+"' value='"+cont[i]['warrenty_expairy_date']+"' readonly></td><td><button type='button' class='btn btn-circle blue btn-outline' onclick=renew_contracts('"+cont[i]['contract_id']+"','"+cont[i]['custid']+"','"+customer_name+"','"+cont[i]['contact']+"','"+cont[i]['email_id']+"','"+cont[i]['anumber']+"','"+door_num+"','"+address+"','"+addr+"','"+location+"','"+city+"','"+state+"','"+cont[i]['cust_country']+"','"+cont[i]['pincode']+"','"+cont[i]['product_id']+"','"+cont[i]['cat_id']+"','"+contract+"','"+product_name+"','"+cat_name+"','"+model+"','"+serial_no+"');>Renew Contract</button></td></tr>");
								}
								else
								{
									//var contract = 'On-Demand';	
									$('#dynamic_table').append("<tr><td>"+cont[i]['product_name']+"<input class='form-control' type='hidden' name='prod_"+i+"' required id='prod_"+i+"' value='"+cont[i]['product_name']+"' readonly></td><td>"+cont[i]['cat_name']+"<input class='form-control' type='hidden' name='cat_"+i+"' required id='cat_"+i+"' value='"+cont[i]['cat_name']+"' readonly></td><td>"+serial_no+"<input class='form-control' type='hidden' name='serialno_"+i+"' required id='serialno_"+i+"' value='"+serial_no+"'readonly></td><td>"+contract+"<input class='form-control' type='hidden' name='amctype_"+i+"' required id='amctype_"+i+"' value='"+contract+"'readonly></td><td><input class='form-control' type='text' name='date_"+i+"' id='date_"+i+"' value='Contract Expired' readonly style='color:red !important;'></td><td><button type='button' class='btn btn-circle blue btn-outline' onclick=renew_contracts('"+cont[i]['contract_id']+"','"+cont[i]['custid']+"','"+customer_name+"','"+cont[i]['contact']+"','"+cont[i]['email_id']+"','"+cont[i]['anumber']+"','"+door_num+"','"+address+"','"+addr+"','"+location+"','"+city+"','"+state+"','"+cont[i]['cust_country']+"','"+cont[i]['pincode']+"','"+cont[i]['product_id']+"','"+cont[i]['cat_id']+"','"+contract+"','"+product_name+"','"+cat_name+"','"+model+"','"+serial_no+"');>Renew Contract</button></td></tr>");
								}
						}							
					}
				}
			});
		}
	});
		
	function renew_contracts(contract_id,custid,name,contact,email_id,anumber,door_num,address,addr,location1,city,state,country,pincode,product_id,cat_id,contract,product_name,cat_name,model,serial)
	{	
      //  alert(rowid);
        var contract_id=contract_id;
		var replaceSpace=address;
		var address = replaceSpace.replace(/\:/g," ");	
		var loca=addr;
		var loc2= loca.replace(/\:/g," ");	
		var name= name.replace(/\:/g," ");	
		var door_num= door_num.replace(/\:/g," ");	
		   var loc=location1;
		var loc= loc.replace(/\:/g," ");
                var ccity=city;
		var cont_city= ccity.replace(/\:/g," ");	
                var state1=state;
		var state1= state1.replace(/\:/g," ");	
		var product_name=product_name;
		var product_name= product_name.replace(/\:/g," ");	
		var cat_name=cat_name;
		var cat_name= cat_name.replace(/\:/g," ");	
		var contract_type =contract;
		var contract_type= contract_type.replace(/\:/g," ");	
		var model_no =model;
		var model_no= model_no.replace(/\:/g," ");	
		
		var s_no =serial;
		var s_no= s_no.replace(/\:/g," ");	
		
		/* $('#preferred_date').datetimepicker({
		   // dateFormat: 'dd-mm-yy',
		   format:'YYYY-MM-DD HH:mm:ss',
		});*/
		/*var replaceSpace=address;
		var address = replaceSpace.replace(/\:/g," ");	
		var location=addr;
		var location= location.replace(/\:/g," ");
                var loc=location1;
		var loc= loc.replace(/\:/g," ");
                var ccity=city;
		var ccity= ccity.replace(/\:/g," ");	
                var state1=state;
		var state1= state1.replace(/\:/g," ");	
		var product_name=product_name;
		var product_name= product_name.replace(/\:/g," ");	
		var cat_name=cat_name;
		var cat_name= cat_name.replace(/\:/g," ");	
		var contract_type =contract;
		var contract_type= contract_type.replace(/\:/g," ");	
		var model_no =model;
		var model_no= model_no.replace(/\:/g," ");	
		
		var s_no =serial;
		var s_no= s_no.replace(/\:/g," ");	*/
		$('#unique_rowid').val(contract_id);
		$('#c_id').val(custid);
		$('#name').val(name);
		$('#number').val(contact);
		$('#cust_mail').val(email_id);
		$('#anum').val(anumber);
		$('#doornum').val(door_num);
		$('#address').val(address);
		$('#cont_town').val(loc2);
        $('#cont_ldmrk').val(loc);
		$('#addr').val(cont_city);
		 $('#ccountry').append('<option value="'+country+'" selected >'+country+'</option>');
		 $('#cstate').append('<option value="'+state1+'" selected >'+state1+'</option>');
		//$('#cstate').val(state1);
		//$('#ccountry').val(country);
		$('#postcode').val(pincode);
                $('#product_name').text(product_name);
		$('#cust_prod').val(product_id);
		$('#cust_cat').val(cat_id);
		$('#cust_prodname').val(product_name);
		$('#cust_catname').val(cat_name);
		$('#contract_type').val(contract_type);
		$('#model_no').val(model_no);
		$('#s_no').val(serial);
		
		$('#myModal1').modal('show');
		
		var company_id="<?php echo $this->session->userdata('companyid');?>";
		$.ajax({
			url: "<?php echo base_url();?>" + "index.php?/controller_call/ticket",
			type: 'POST',
			data:{'company_id':company_id},
			success: function(data) {
					console.log(data);
					$("#tick_id").val(data);
			}
		});
		
		$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_call/select_amc1",
				    type: 'POST',
					data:{'company_id':company_id},
					//dataType:'JSON',
					success: function(data) {
					//var result=JSON.parse(data);
							console.log(data);
							$('#amc_id').html('');
								  $('#amc_id').val(data);
								
					}
			});
		//$('#smallModal1').modal('show');
		
		 var ajaxResult=[];
		$.ajax({
			url: "<?php echo base_url();?>" + "index.php?/controller_call/fet_details",
			type: 'POST',
			async:false,
			success: function(data) { 
			data=JSON.parse(data);
			ajaxResult=[];
			ajaxResult=data;
			},
		});
			var res=ajaxResult; 
			console.log(res);
			/*$('#call_tag').autocomplete({
				source: res
            });	*/
			
		
	}
	
	$("#new_contract").click(function(){
		var contact_num=$('#contact_num').val();
		var cust_id=$('#cust_id').val();
		var cname=$('#cname').val();
		var email=$('#email').val();
		var altnum=$('#altnum').val();
		var door=$('#door').val();
		var street=$('#street').val();
		var town=$('#town').val();
		var land_mark=$('#land_mark').val();
		var city=$('#city').val();
		var state=$('#state').val();
		var country=$('#country').val();
		var pin=$('#pin').val();
		
		$('#modal_number').val(contact_num);
		$('#modal_cid').val(cust_id);
		$('#modal_cname').val(cname);
		$('#modal_cmail').val(email);
		$('#modal_anum').val(altnum);
		$('#modal_door').val(door);
		$('#modal_address').val(street);
		$('#modal_town').val(town);
		
		$('#modal_lmrk').val(land_mark);
		$('#modal_addr').val(city);
		$('#modal_ccountry').append('<option value='+country+'>'+country+'</option>');
		$('#modal_cstate').append('<option value='+state+'>'+state+'</option>');
		$('#modal_post').val(pin);
	
		$('#myModal3').modal('show');
		var company_id="<?php echo $this->session->userdata('companyid');?>";
		
		$.ajax({
			url: "<?php echo base_url();?>" + "index.php?/controller_call/ticket",
			type: 'POST',
			data:{'company_id':company_id},
			success: function(data) {
					console.log(data);
					$("#modal_ticket").val(data);
			}
		}); 
		
		var company_id="<?php echo $this->session->userdata('companyid');?>";
		$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_call/select_amc1",
				    type: 'POST',
					data:{'company_id':company_id},
					success: function(data) {
							console.log(data);
							$('#modal_amc').html('');
								  $('#modal_amc').val(data);
								
					}
			});
	});
		
	/* $('#modal_cmail').on('change',(function(e) {
		 $("#rowdata_cust").empty();
		   var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
   			 var emailinput = $('#modal_cmail').val();
			if (email_reg.test(emailinput) == false) {
				 $("#rowdata_cust").html("Please enter a valid email");
						$(".error").show();
						 $("#myModal3").animate({scrollTop:0});
			}
		else{
			 $("#rowdata_cust").empty();
		}
    })); */
		
	$('#modal_anum').on('change',(function(e) {
		   $("#rowdata").empty();
              var inputVal = $(this).val();
				var numericReg = /^[0-9?=.*!@#$%^&*]+$/;
				if(!numericReg.test(inputVal)) {
					//alert("Numeric characters only");
					 $("#rowdata").html('Provide proper alternate number !');
						$(".error").show();
						 $("#myModal1").animate({scrollTop:0});
				}
		   else{
			     $("#rowdata").empty();
			   window.scrollTo(40, 60);
		   }
       })); 
	
	$("#cont_search").click(function(){
		if($("#modal_number").is(":blank"))
	  {
			swal("Contact Field Empty!")
	  }
	  else 
	  {
	    var contact_number=$('#modal_number').val();	
		var company_id="<?php echo $this->session->userdata('companyid');?>";
		$.ajax({
				url: "<?php echo base_url();?>" + "index.php?/controller_call/customer_fetch",
				type: 'POST',
				data : { 'contact_number' : contact_number, 'company_id' : company_id },
			    dataType:"json",				
				success: function(info) {
					console.log(info);
					if(info=='Provide Proper Mobile Number'){
						//$('#myModal3').modal('hide');
						swal("Oops!", "Provide Proper Mobile Number!", "warning")
						//$('#myModal3').modal('show');
					}
					else if(info=='No details!') {
						 //$('#myModal3').modal('hide');
						swal({
							title:"Oops!",
							  text: " No Details Found for this Number",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonColor: "#DD6B55",
							  confirmButtonText: "Ok.",
							  cancelButtonText: "No, cancel !",
							  closeOnConfirm: false,
							  closeOnCancel: false
						},
						function(isConfirm){
								if(isConfirm)
								{
									 swal.close();
									$('#modal_number').val('');
									//$('#myModal3').modal('show');
								   
							        } 
                                                       else {
								swal.close();
							}
						});
					}
					else {
					    var cont=info[0].contract;
					    var data1= cont;
						//$('#modal_prod').html('');
						//$('#modal_prod').append('<option selected disabled>Select Product-Category</option>');
							for(k=0;k<data1.length;k++) {
							    $('#modal_cid').val(data1[k]['custid']);
							    $('#modal_cname').val(data1[k]['customer_name']);
							    $('#modal_cmail').val(data1[k]['email_id']);
							    $('#modal_anum').val(data1[k]['anumber']);
							    $('#modal_door').val(data1[k]['door_num']);
							    $('#modal_address').val(data1[k]['address']);
							    $('#modal_town').val(data1[k]['cust_town']);
							    $('#modal_lmrk').val(data1[k]['l_mrk']);
							    $('#modal_addr').val(data1[k]['city']);
							    $('#modal_ccountry').append('<option value="'+data1[k]['cust_country']+'" selected >'+data1[k]['cust_country']+'</option>');
		  						$('#modal_cstate').append('<option value="'+data1[k]['state']+'" selected >'+data1[k]['state']+'</option>');
							    //$('#modal_cstate').val(data1[k]['state']);
							    //$('#modal_ccountry').val(data1[k]['cust_country']);
							    $('#modal_post').val(data1[k]['pincode']);
								
				//$('#modal_prod').append('<option value='+data1[k]['product_id']+'>'+data1[k]['product_name']+'</option>');
					   }
					}
				}	
			});
		}
	});
	
	$('#modal_prod').change(function(){
		var prod= $('#modal_prod').val();
		var company_id="<?php echo $this->session->userdata('companyid');?>";
		$.ajax({
				url: "<?php echo site_url('controller_call/choose_cat')?>",
				method: "POST",
				 data : { 'prod' : prod, 'company_id' : company_id },
					success	: function(data){
						$('#modal_cat').html('');
						console.log(data);
						var result = JSON.parse(data);
						$('#modal_cat').append('<option selected disabled>Select Sub-Category</option>');
						for(i=0;i<result.length;i++)
								   {
										$('#modal_cat').append('<option value='+result[i]['cat_id']+'>'+result[i]['cat_name']+'</option>');
								   }
							},
					error: function( error )
                                {
									alert( error );
								}
				});
	});
	$('#modal_contract').change(function(){
		var modal_contract= $('#modal_contract').val();				
		var company_id="<?php echo $this->session->userdata('companyid');?>";
		//alert(modal_contract);
		$.ajax({
			url: "<?php echo base_url();?>" + "index.php?/controller_call/calculate_period",
			type: 'POST',
			data :'cont_type=' + modal_contract + '&company_id=' + company_id ,
			success: function(data) {
				$('#contract_period').html('');
					console.log(data);
				 if(modal_contract=='Non-Contract')
			 {
				  $("#modal_quantity").attr("disabled", "disabled"); 
				  $("#modal_quantity").val('0');
			   $("#new_contract_period").attr("disabled", "disabled"); 
				 $("#new_contract_period").val('0');
				   $("#contract_period").val('');
				//  $("#modal_worktype").css("display", "none");
				//   $("#modal_nonworktype").css("display", "block");
			 }
			 else
			 {
			  $("#modal_quantity").removeAttr("disabled"); 
				 $("#modal_quantity").val('1');
				   $("#new_contract_period").removeAttr("disabled"); 
				 $("#new_contract_period").val(0);
               $("#contract_period").val(data);
				// $("#modal_worktype").css("display", "block");
				//   $("#modal_nonworktype").css("display", "none"); 
			 }
				
			}
		});
	});
	
		
	</script>
	
	<script>		
	
	var companyid="<?php echo $this->session->userdata('companyid');?>";
	$('#raise_amcbutton').click(function(){
		var region="<?php echo $region;?>";
						 var area="<?php echo $area;?>";
		$('#Searching_Modal').modal('show');
		$.ajax({
					url 	: "<?php echo site_url('controller_call/renew_existingcontract')?>",
					method  :'POST',
					data 	:  $("#submit_form").serialize() + '&company_id=' + companyid+ '&region=' + region+ '&area=' + area,
					success :   function(data){     
						$('#Searching_Modal').modal('hide');
                            if(data=="Ticket has been Raised.")
                            {
                                $.ajax({
                                    url         :   "<?php echo base_url(); ?>index.php?/AutoNewAssign/autoassign",
                                    type        :   "POST",
                                    data        : {"company_id":companyid},
                                    //datatype  :   "JSON", 
                                    contentType :false,
                                    processData	: false,
                                    cache		:false,  
                                    success     :   function(data){
                                    }
                                });	
								$('#myModal1').modal('hide');
									swal({
										  title: "Good job!",
										  text: data,
										  type: "success",
										  confirmButtonClass: "btn-primary",
										  confirmButtonText: "Ok.",
										  closeOnConfirm: false,
									},
									function(isConfirm) {
										  if (isConfirm) {
										  location.reload();
									    }
							          }); 
                            }
                            else {
                                    //$('#myModal1').modal('hide');
                                    swal(data);
                                   // $('#myModal1').modal('show');
                            }
                    }
				});
	});
	
	$('#contract_button').click(function(){
		var companyid="<?php echo $this->session->userdata('companyid');?>";
		var region="<?php echo $region;?>";
						 var area="<?php echo $area;?>";
		$('#Searching_Modal').modal('show');
			$.ajax({
					url 	: "<?php echo site_url('controller_call/raise_amcticket')?>",
					method  :'POST',
					data 	:  $("#submit_form_3").serialize() + '&company_id=' + companyid+ '&region=' + region+ '&area=' + area,
					success :   function(data){                      
                     		$('#Searching_Modal').modal('hide');
                            if(data=="Ticket has been Raised.")
                            {
                                    $.ajax({
                                        url         :   "<?php echo base_url(); ?>index.php?/AutoNewAssign/autoassign",
                                        type        :   "POST",
                                        data        : {"company_id":companyid},
                                        //datatype  :   "JSON", 
                                        contentType :false,
                                        processData	: false,
                                        cache		:false,  
                                        success     :   function(data){
                                        }
                                    });	
								$('#myModal3').modal('hide');
									swal({
										  title: "Good job!",
										  text: data,
										  type: "success",
										  confirmButtonClass: "btn-primary",
										  confirmButtonText: "Ok.",
										  closeOnConfirm: false,
									},
									function(isConfirm) {
										  if (isConfirm) {
										  location.reload();
									    }
							          }); 
                            }
                            else {
                              //  console.log(data);
                              //  return false;
                        			
                                   // $('#myModal3').modal('hide');
                                    swal(data);
                                   // $('#myModal3').modal('show');
                            }
                    }
            });
	
	});
		
		
	$( document ).on( "change", "#modal_sno", function(e) {
	
	//alert("hello");
		 var modal_sno =  $(this).val();
		var customer_id =  $('#modal_cid').val();
	
	//alert(modal_sno);
	//alert(customer_id);	
 
        $.ajax({
                type: "POST",
                data: {'serial_no':modal_sno,'customer_id':customer_id},
                url: "<?php echo base_url(); ?>index.php?/controller_call/check_serial_no",
                dataType: 'json',
                success: function(response) {
                 if(response == 1){
                       // alert(response);
					  $('#modal_sno').val('');
					  $('#cmmterror').html(modal_sno+' Already Exists!'); 
					  $('#modal_sno').focus();
				 }
					else
					{
						 $('#cmmterror').html('');
					}
                 
                }
        }); 
    });

    $(document).ready(function(){
	
      $('#startdate').datepicker({
   language: 'nl',
   orientation: 'auto bottom'
   }).on('show', function () {
   $('.datepicker-orient-bottom').removeClass('datepicker-orient-bottom');
   $('.datepicker-dropdown').addClass('datepicker-orient-top');
   });		
      
   $('#startdate').datepicker('setDate', new Date());
      $("#startdate").datepicker({
         todayBtn:  1,
         autoclose: true,
      }).on('changeDate', function (selected) {
         var startdate = new Date(selected.date.valueOf());
         
         $('.datepicker').hide();
      });
		
 });	
 

	
	</script>
	</body>
</html>
