<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include 'assets/lib/cssscript.php'?>
    <style>		
		
	</style>
</head>

<body>	
	
    <div class="container">
        <div class="card card-container col-lg-6 col-lg-offset-3" style="background-color:#fff;margin-top:3%">            
            <p id="profile-name" class="profile-name-card"></p>		
            <form id="register" style="padding:2%">
				<span class="error_msg" style="color:red"><p id="error_msg"></p></span>
                <span id="reauth-email" class="reauth-email"></span>
                <div class="form-group">
                    <label for="username">Email:</label>
                    <input type="text" id="email" name="email" class="form-control" readonly>
            	</div>				
                <!--<div class="form-group">
                    <label for="username">Company Name:</label>
                    <input type="text" id="c_name" name="c_name" class="form-control"readonly>
            	</div>	-->					
                <div class="form-group" style="display:none;">
                    <label for="username">Company ID:</label>
                    <input type="text" id="c_id" name="c_id" class="form-control" readonly>
            	</div>	
                <div class="form-group">
                    <label for="username">Role:</label>
                    <input type="text" id="role" name="role" class="form-control" readonly>
            	</div>				
              	<div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required autofocus>
					<span id="result"></span>
					<span id="results" style="color:red"><p id="error_password"></p></span>					
              	</div>				
              	<div class="form-group">
                    <label for="password">Confirm Password:</label>
                    <input type="password" id="c_password" name="c_password" class="form-control" placeholder="Confirm Password" required>
					<span id="results" style="color:red"><p id="error_cpassword"></p></span>						
              	</div>
               	<button class="btn btn-lg btn-primary col-lg-offset-5" type="button" id="password_enter">Submit</button>
            </form>
        </div>
    </div>
    <?php include 'assets/lib/javascript.php'?>	

	<script>
		function getParameterByName(name, url) {
			if (!url) {
			  url = window.location.href;
			}
			name = name.replace(/[\[\]]/g, "\\$&");
			var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return '';
			return decodeURIComponent(results[2].replace(/\+/g, " "));
		}
		var user = getParameterByName('user');		
		var company_name = getParameterByName('companyname');
		var company_id = getParameterByName('company_id');			
		var role = getParameterByName('role');			

		if(user=="" || company_name=="" || company_id==""|| role==""){
			alert("Url is not valid");
		}else{
			$('#email').val(user);
			$('#c_name').val(company_name);
			$('#c_id').val(company_id);
			$('#role').val(role);
		}
	</script>
	<script>
window.history.forward();		
		$('#password_enter').click(function(){
			var password=$("#password").val();
			var c_password=$("#c_password").val();
			if(password !="" && c_password != ""){
				if(password == c_password){					
					$.ajax({
						url         :   "<?php echo base_url(); ?>index.php?/Login/new_user_registration",
						type        :   "POST",
						data        :   $('#register').serialize(),// {action:'$funky'}
						//datatype	:	"JSON",	
						cache       :   false,
						success    	: 	function(data){	
if(data==1){
swal({
	 title: "",
						     text: "Password Set Successfully",
						     type: "success",
						     confirmButtonClass: "btn-primary",
						     confirmButtonText: "Ok.",
						     closeOnConfirm: false,
						},
						function(isConfirm) {
								if (isConfirm) {
										 window.location.href="<?php echo base_url(); ?>index.php?/login/login";
									       }
					        }); 
         				}else{
$('#error_msg').html(data);
}
},
										
					});
				}else{
					$('#error_msg').html('Password and Confirm Password should be same....');
				}
			}else if(password == ""){
				$('#error_password').html('Fields are mandatory');
			}else if(c_password == ""){
				$('#error_cpassword').html('Fields are mandatory');
			}			
			$('#password').keyup(function(){
				$('#error_password').html("");
			});						
			$('#c_password').keyup(function(){
				$('#error_cpassword').html("");
			});
		});		
		$('#password').keyup(function() {
			$('#result').html(checkStrength($('#password').val()))
		})
		function checkStrength(password) {
			var strength = 0
			if (password.length < 6) {
				$('#result').removeClass()
				$('#result').addClass('short')
				return 'Too short'
			}
			if (password.length > 7) strength += 1
				// If password contains both lower and uppercase characters, increase strength value.
			if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
				// If it has numbers and characters, increase strength value.
			if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
				// If it has one special character, increase strength value.
			if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
				// If it has two special characters, increase strength value.
			if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
				// Calculated strength value, we can return messages
				// If value is less than 2
			if (strength < 2) {
				$('#result').removeClass()
				$('#result').addClass('weak')
				return 'Weak'
			} else if (strength == 2) {
				$('#result').removeClass()
				$('#result').addClass('good')
				return 'Good'
			} else {
				$('#result').removeClass()
				$('#result').addClass('strong')
				return 'Strong'
			}
		}
	</script>
    <script>		
		/*$('#password').hideShowPassword({
		  innerToggle: true,
		  touchSupport: Modernizr.touch
		});
		$('#c_password').hideShowPassword({
		  innerToggle: true,
		  touchSupport: Modernizr.touch
		});*/
	</script>
</body>
</html>