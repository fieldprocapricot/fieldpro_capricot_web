<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head> 
        <?php include 'assets/lib/cssscript.php'?>
		<style>
        span.help-block {
            color: red !important;
        }
			.open>.dropdown-menu {
                display: block !important;
            }
            .tab-height{
                border:0 !important;
            }
			.jstree-anchor{
				min-height:110px !important;
			}
			.jstree-icon.jstree-themeicon.jstree-themeicon-custom{
				height:100px;
				width:150px;
			}
			.jstree-default .jstree-anchor {
				//line-height: 6 !important;
				margin-bottom:6%;
				text-align: center;
			}
			.jstree-icon.jstree-themeicon.jstree-themeicon-custom {
				margin: 8px 0px !important;
			}
			.jstree-node{
				margin:3px 0px;
			}
			#tree_5{
				padding:2%;
			}
			.jstree-children{
				margin:0% 2%;
			}
			.jstree-icon.jstree-themeicon.jstree-themeicon-custom{
				display:block !important;
			}
			.sweet-alert.showSweetAlert.visible{
				z-index: 999999999 !important;
			}	
			.fileinput-new{
				color:#000 !important;
			}	
            .selected{
               box-shadow:0px 12px 22px 1px #333;
            }
            .portlet.box.blue-hoki {
                border: 1px solid #869ab3;
                /*border-top: 0;*/
            }#frame{
                height: 150px; /* equals max image height */
                width: 160px;
                border: 1px solid #0a1824;
                white-space: nowrap;
                text-align: center; 
                margin: 1em 0;
            }
            .helper{
                display: inline-block;
                height: 100%;
                vertical-align: middle;
            }
            #frame img{
                vertical-align: middle;
                max-height: 150px;
                max-width: 160px;
                width:100%;
                height:auto;
            }
            .product-checkbox2{
                margin: 0 7px -15px 0 !important;
            }
            button.accordion2 {
              background-color: #eee;
              color: #444;
              cursor: pointer;
              padding: 18px;
              width: 100%;
              border: none;
              text-align: left;
              outline: none;
              font-size: 15px;
              transition: 0.4s;
              min-height: 140px !important;
              margin-bottom:1%;
          }

          button.accordion2.active, button.accordion2:hover {
              background-color: #ddd;
          }

          button.accordion2:after {
              content: '\002B';
              color: #777;
              font-size: 30px;
              float: right;
              margin-left: 5px;
              padding: 20px 0;
          }

          button.accordion2.active:after {
              content: "\2212";
          }

          div.panel2 {
              padding: 0 18px;
              background-color: white;
              max-height: 0;
              overflow: hidden;
              transition: max-height 0.2s ease-out;
              margin: 10px 0;
          }
          .sub-products img
          {
            border:1px solid #3A6C86;
            width: 100px;
            height: 100px;
          //border-radius: 80px !important;
          }
          .accordion2 img {
              //border-radius: 80px !important;
              width: 100px !important;
              height: 100px !important;
          }
          .pdt-content {
              width: 65%;
              margin-top: -108px;
          }
          .pdt-title {
              font-size: 18px;
          }
          button.accordion2::after {
            margin-top: -105px;
          }
          .sub-content {
              padding: 3px 0;
          }
          .prroducts{
            //margin-bottom:1% !important;
          }
          .pen-icon2 {
              float: right;
              margin: -44px 0 0 0 !important;
              border: 1px solid #dddddd;
              padding: 8px;
              border-radius: 100px !important;
              background-color: #fff;
          }
          .pen-icon2 a .icon-pencil{
            color:#000 !important;
          }
          .side_animate{
            position: relative;
            float: right;
            margin-right: -13%;
            margin-top: 1%;
            padding: 1%;
            border-radius:100px !important;
            background: #fff;
            border: 1px solid #eee;
            opacity:0;
            transition: opacity 1s ease-in-out;
            -moz-transition: opacity 1s ease-in-out;
            -webkit-transition: opacity 1s ease-in-out;
          }
          .side_animate .first_fa{
            padding-right: 10px;

          }
          .pen-icon {
            float: right;
            margin: 0px -14px -39px 0 !important;
            position: relative;
            background: #fff;
            padding: 24px 10px 10px 18px;
            border-radius: 100% !important;
            border: 1px solid #eee;
            height: 70px;
            width: 70px;

              /*-webkit-transition-duration: 2s;
              -moz-transition-duration: 2s;
              -o-transition-duration: 2s;
              transition-duration: 2s;
              -webkit-transition-property: -webkit-transform;
              -moz-transition-property: -moz-transform;
              -o-transition-property: -o-transform;
              transition-property: transform;
              -webkit-transform:rotate(-180deg);
              -moz-transform:rotate(-180deg);
              -o-transform:rotate(-180deg);*/
          }
          .first_fa {
            margin-right:25%;
          }
          /*.pen-icon:hover{
            -webkit-transition-duration: 2s;
            -moz-transition-duration: 2s;
            -o-transition-duration: 2s;
            transition-duration: 2s;
            -webkit-transition-property: -webkit-transform;
            -moz-transition-property: -moz-transform;
            -o-transition-property: -o-transform;
            transition-property: transform;
            -webkit-transform:rotate(180deg);
            -moz-transform:rotate(180deg);
            -o-transform:rotate(180deg);
          }*/
          .pen-icon:hover + .side_animate{
            opacity: 1.0;
            transition: opacity 1s ease-in-out;
            -moz-transition: opacity 1s ease-in-out;
            -webkit-transition: opacity 1s ease-in-out;
          }
		</style>
        <link href="<?php echo base_url();?>assets/global/plugins/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
        <!-- BEGIN CONTAINER -->
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "assets/lib/header_superad.php"?>
            <!-- END HEADER -->
           <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/superad_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                            	<!-- BEGIN EXAMPLE TABLE PORTLET-->
								<div class="portlet box dark">
									<div class="portlet-title">
										<div class="caption">Product Master</div>
										<div class="tools"> </div>
            
									</div>
									<div class="portlet-body">                                                                            
                                    <div class="table-toolbar">
                                        <div class="row pull-right">
                                            <div class="col-sm-12">
                                                
                                            <div class="btn-group">
                                              <select  name="company_list" id="company_list" onChange="company_change()" class="form-control input-circle green-haze" tabindex="-1" aria-hidden="true" data-width="75%" required>
                                                    <!-- <option value="" selected disabled>Select Company</option> -->
                                                        <?php
                                                            foreach ($company->result() as $row) {                 
                                                        ?>
                                                     <option value="<?php echo $row->company_id; ?>"><?php echo $row->company_name; ?></option>
                                                   <?php } ?>
                                            </select>
                                                </div>
                                                <div class="btn-group">
                                                    <button id="sample_editable_1_new" class="btn btn-circle green-haze btn-outline btn-md" onClick="add_prod()">
                                                    <i class="fa fa-plus"></i> Add Product Category
                                                    </button>
                                                </div>
                                                <!-- <div class="btn-group">
                                                    <button id="sample_editable_1_new" class="btn btn-circle green-haze btn-outline btn-md" onclick='add_sub()'>
                                                    <i class="fa fa-plus"></i> Add Sub Category
                                                    </button>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
										
										<div class="container-fluid" id="div_align">
												<div class="portlet box blue-hoki">
													
													<div class="portlet-body">
														
                                                        <div class="tabbable tabs-left">
<!-- <div class="col-sm-3 parent-left">
        <ul class="nav nav-tabs product-tab-left" id='company_append'>
         
        </ul>
</div>
<div class="col-sm-9" style="border: 1px solid #099 !important">
        <div class="tab-content tab-height" id='product_append'>
        </div>
</div>
      </div> -->
      <div class="col-sm-3 parent-left" style="display:none">
        <ul class="nav nav-tabs product-tab-left" id='company_append'>
         
        </ul>
     </div>
      <div class="col-sm-12" style="border: 0px solid #099 !important">
        <div class="tab-content tab-height" id='product_append'>
        </div>
      </div>
      </div>
                                                        
													</div>
												</div>
										</div>
									</div>
								</div>
								<!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>                    
                    <!-- END PAGE BASE CONTENT -->
					 <!-- BEGIN FOOTER -->
            <?php include "assets/lib/footer.php"?>
            <!-- END FOOTER -->
                 </div> 
        
        <!--Modal Starts-->
        <!-- Modal -->
        <div id="myModal1" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Add Product Category</h4>
                                    <div class="error" style="display:none">
                                        <label id="rowdata"></label>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" id="add_product">                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Company</label>
                                            <div class="col-sm-9">
                                                <select disabled name="company" id="companyadd" class="form-control" tabindex="-1" aria-hidden="true" required>
                                                    <option value="" disabled>Select Company</option>
                                                        <?php
                                                            foreach ($company->result() as $row) {                 
                                                        ?>
                                                   <option value="<?php echo $row->company_id; ?>"><?php echo $row->company_name; ?></option>
                                                   <?php } ?>
                                                </select>
                                                <!--<input type="text" class="form-control" id="company" name="company" placeholder="Company" value='<?php echo $this->session->userdata('companyid');?>'>-->
                                            </div>
                                        </div>
                                        <div class="form-group" style='display:none'>
                                            <label class="control-label col-sm-3" for="email">Product Id</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="product_id" name="product_id" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Product Category</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name">
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label class="control-label col-sm-3" for="email">Model No</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="product_modal" name="product_modal" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Description</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="5" id="product_desc" name="product_desc" style="resize:none"></textarea>
                                                <!--<input type="text" class="form-control" id="product_desc" name="product_desc" />-->
                                            </div>
                                        </div>
                                        
                                          <div class="form-group">
                                              <label class="control-label col-md-3">Upload Image</label>
                                              <div class="col-md-9">
                                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                                      <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> 
                                                        <img src="assets/images/Placeholder.jpg" id="pro_new_image" alt="Update Image" style="width: 200px; height: 150px; line-height: 150px;"/>
                                                      </div>
                                                      <div>
                                                          <span class="btn red btn-outline btn-file">
                                                              <span class="fileinput-new"> Select image </span>
                                                              <span class="fileinput-exists"> Change </span>
                                                              <input type="file" name="product_image" id='product_image'> </span>
                                                          <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-circle blue btn-outline" id="addproduct"><i class="fa fa-check"></i> Submit</button>
                                    <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
        <div id="edits" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Product</h4>
                        <div class="error" style="display:none">
                            <label id="rowdata_1"></label>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="edit_product">
                            <div class="form-group" style="display:none">
                                <label class="control-label col-sm-3" for="email">Id</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="id1" name="id1" readonly>
                                </div>
                            </div>
                            <div class="form-group" style="display:none">
                                <label class="control-label col-sm-3" for="email">Product Category Id</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="product_id1" name="product_id1" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Product Category</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="product_name1" name="product_name1" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Model No</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="product_modal1" name="product_modal1" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="5" id="product_desc1" name="product_desc1" style="resize:none"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Upload Image</label>
                                <div class="col-md-9">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> 
                                        <img src="" id="pro_edit_image" alt="Update Image" />
                                      </div>
                                        <div>
                                            <span class="btn red btn-outline btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="product_image1" id='product_image1'> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" id="product_image2" name="product_image2" style="display:none" />
                                </div>
                            </div>
                            <div class="form-group" style="display:none">
                                <label class="control-label col-sm-4" for="email">Company ID:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="companyid1" name="companyid1" placeholder="">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-circle blue btn-outline" onClick="submit_product()"><i class="fa fa-check"></i> Submit</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                </div>
            </div>
        </div>
         <!-- Modal -->
         <!-- Modal -->
       <div id="myModal2" class="modal fade" role="dialog">
			   <div class="modal-dialog">
				  <!-- Modal content-->
  				<div class="modal-content" style="width: 107%; !important">
    					<div class="modal-header">
    						<button type="button" class="close" data-dismiss="modal">&times;</button>
    						<h4 class="modal-title">Add Sub Category</h4>
    						<div class="error" style="display:none">
    							<label id="rowdata_category"></label>
    						</div>
    					</div>
      					<div class="modal-body">
      						<form class="form-horizontal" id="add_subproduct">
                       <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Company</label>
                        <div class="col-sm-8">
                            <select name="prodcompany_id" id="prodcompany_id" class="form-control" tabindex="-1" required aria-hidden="true" >
                                <option value="" selected disabled>Select Company</option>
                                    <?php
                                        foreach ($company->result() as $row) {                 
                                    ?>
                               <option value="<?php echo $row->company_id; ?>"><?php echo $row->company_name; ?></option>
                               <?php } ?>
                            </select>
                            
                        </div>
                    </div>
      							<div class="form-group" style='display:none'>
      								<label class="control-label col-sm-4" for="email">Sub Category Id</label>
      								<div class="col-sm-8">
      									<input type="text" class="form-control" id="subproduct_id" name="subproduct_id" readonly>
      								</div>
      							</div>
      							<div class="form-group" >
      								<label class="control-label col-sm-4" for="email">Product Category</label>
      								<div class="col-sm-8">
                                          <select class="form-control" id="product_names" name="product_names" required>
                                              <option value="" selected disabled>Select Product Category</option>
                                              
                                          </select>
      									<!--<input type="text" class="form-control" id="product_names" name="product_names" readonly>-->
      								</div>
      							</div>
    							<!--<div class="form-group" >
    								<label class="control-label col-sm-4" for="email">Product Category</label>
    								<div class="col-sm-8">
    									<input type="text" class="form-control" id="product_value" name="product_value" readonly>
    								</div>
    							</div>-->							
    							<div class="form-group">
    								<label class="control-label col-sm-4" for="email">Sub Category Name</label>
    								<div class="col-sm-8">
    									<input type="text" class="form-control" id="subproduct_name" name="subproduct_name" placeholder="Sub Category Name" required>
    								</div>
    							</div>
    							<div class="form-group" style="display:none">
    								<label class="control-label col-sm-4" for="email">Sub Category Model No</label>
    								<div class="col-sm-8">
    									<input type="text" class="form-control" id="subproduct_modal" name="subproduct_modal" placeholder="Sub Category Model No" required>
    								</div>
    							</div>
    							<div class="form-group">
    								<label class="control-label col-sm-4" for="email">Sub Category Description</label>
    								<div class="col-sm-8">
    									<textarea class="form-control" rows="5" id="subproduct_desc" name="subproduct_desc" required style="resize:none"></textarea>
    								</div>
    							</div>
    							<div class="form-group" style="display:none">
    								<label class="control-label col-sm-4" for="email">company id</label>
    								<div class="col-sm-8">
    									<input type="text" class="form-control" id="c_id" name="c_id" placeholder="">
    								</div>
    							</div>						
    							<div class="form-group ">
                                    <label class="control-label col-md-4">Upload Image</label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> 
                                               <img src="assets/images/Placeholder.jpg" id="sub_new_image" alt="Update Image" style="width: 200px; height: 150px; line-height: 150px;"/>
                                             </div>
                                            <div>
                                                <span class="btn red btn-outline btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="subproduct_image" id='subproduct_image'> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    						</form>
    					</div>
  					<div class="modal-footer">
  						<button type="submit" class="btn btn-circle green btn-outline" id="addsubproduct"><i class="fa fa-check"></i> Submit</button>
  						<button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
  					</div>
			    </div>
	      </div>
		  </div>

      <div id="new_subcategory" class="modal fade" role="dialog">
         <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content" style="width: 107%; !important">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Sub Category</h4>
                <div class="error" style="display:none">
                  <label id="rowdata_newcat"></label>
                </div>
              </div>
                <div class="modal-body">
                  <form class="form-horizontal" id="add_subproduct_new">
                       
                    <div class="form-group" style='display:none'>
                      <label class="control-label col-sm-4" for="email">Sub Category Id</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="subproduct_id_new" name="subproduct_id_new" readonly>
                      </div>
                    </div>
                   <div class="form-group" style='display:none'>
                      <label class="control-label col-sm-4" for="email">Company ID</label>
                      <div class="col-sm-8">
                          <input type="text" class="form-control" id="c_id_new" name="c_id_new" readonly>
                      </div>
                    </div> 
                    <div class="form-group" style='display:none'>
                      <label class="control-label col-sm-4" for="email">Product Category</label>
                      <div class="col-sm-8">
                          <input type="text" class="form-control" id="product_id_new" name="product_id_new" readonly>
                      </div>
                    </div> 
                  <div class="form-group" >
                    <label class="control-label col-sm-4" for="email">Product Category</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="product_name_new" name="product_name_new" readonly>
                    </div>
                  </div>             
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Sub Category Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="subproduct_name_new" name="subproduct_name_new" placeholder="Sub Category Name" required>
                    </div>
                  </div>
                  <div class="form-group" style="display:none">
                    <label class="control-label col-sm-4" for="email">Sub Category Model No</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="subproduct_modal_new" name="subproduct_modal_new" placeholder="Sub Category Model No" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Sub Category Description</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" rows="5" id="subproduct_desc_new" name="subproduct_desc_new" required style="resize:none"></textarea>
                    </div>
                  </div>
                            
                  <div class="form-group ">
                      <label class="control-label col-md-4">Upload Image</label>
                      <div class="col-md-8">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> 
                                 <img src="assets/images/Placeholder.jpg" id="subpro_new_image" alt="Update Image" style="width: 200px; height: 150px; line-height: 150px;"/>                              
                            </div>
                              <div>
                                  <span class="btn red btn-outline btn-file">
                                      <span class="fileinput-new"> Select image </span>
                                      <span class="fileinput-exists"> Change </span>
                                      <input type="file" name="subproduct_image_new" id='subproduct_image_new'> </span>
                                  <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                              </div>
                          </div>
                      </div>
                  </div>
                </form>
              </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-circle green btn-outline" id="addsubproduct_new"><i class="fa fa-check"></i> Submit</button>
              <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
          </div>
        </div>
      </div>

        <div id="edits_category" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Update Category</h4>
                                    <div class="error" style="display:none">
                                        <label id="rowdata_category_1"></label>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" id="edit_product">
                                        <div class="form-group" style="display:none">
                                            <label class="control-label col-sm-3" for="email">Id:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="id1" name="id1" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display:none">
                                            <label class="control-label col-sm-3" for="email">Sub Category Id:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="subproduct_id1" name="subproduct_id1" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Product Category:</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="product_name_1" name="product_name_1">
                                                    <?php
                                                            foreach ($record->result() as $row) {                       
                                                                ?>
                                                        <option value="<?php echo $row->product_id; ?>">
                                                            <?php echo $row->product_name; ?>
                                                        </option>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Sub Category:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="subproduct_name1" name="subproduct_name1" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display:none">
                                            <label class="control-label col-sm-4" for="email">Sub Category Model No:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="subproduct_modal1" name="subproduct_modal1" placeholder="Sub Category Model No">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Sub Category Description:</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="5" id="subproduct_desc1" name="subproduct_desc1" style="resize:none"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label class="control-label col-md-3">Upload Image</label>
                                            <div class="col-md-9">
                                              <input type="text" class="form-control" id="subproduct_image2" name="subproduct_image2" style="display:none" />
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> 
                                                        <img src="" id="sub_pro_edit_image" alt="Update Image" />
                                                      </div>
                                                    <div>
                                                        <span class="btn red btn-outline btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="subproduct_image1" id='subproduct_image1'> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-circle blue btn-outline" onClick="submit_subproduct()"><i class="fa fa-check"></i> Submit</button>
                                    <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
        <!--Modal End-->
                
        <!-- END QUICK SIDEBAR -->
		<?php include 'assets/lib/javascript.php'?>    
		<script>        		
       		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
        	$('#product_m').addClass('open');
		</script>		
		<script>

			$(document).ready(function(){
            var prod_name;
            var c_id;
                            $.ajax({
                                url         :   "<?php echo base_url(); ?>index.php?/controller_superad/get_details_company1",
                                type        :   "POST",
                                data        :   "",
                                datatype  	:   "JSON", 
                                cache       :   false,
                                success     :   function(data){
                                                var data=JSON.parse(data);
                                                    
                                                    for(i=0;i<data.length;i++){
                                                        //$('#company').append("<li id='frame'><span class='helper'></span><div class='product-checkbox2'><input type='checkbox' value=''></div><a href='#a' data-toggle='tab'><img src="+data[i].company_logo+" class='img-responsive' width='150' height='150' /></a></li><br>")
                                                        $('#company_append').append("<li id="+data[i].company_id+" ><a href='."+data[i].company_id+"' data-toggle='tab'><div id='frame'><span class='helper'></span><img src="+data[i].company_logo+" height=250 /></div></a><div class='caption catg-title2'><p>"+data[i].company_name+"</p></div></li>");
                                                        $('ul#company_append li:first-child').addClass('active');
                                                        $('#product_append').append("<div class='tab-pane "+data[i].company_id+"' id><p class='main-category'>Product Category</p></div>");
                                                        $.ajax({
                                                            url         :   "<?php echo base_url(); ?>index.php?/controller_superad/get_product_company1",
                                                            type        :   "POST",
                                                            data        :   {'company_id':data[i].company_id},
                                                            datatype    :   "JSON", 
                                                            cache       :   false,
                                                            success     :   function(data){
                                                                                var data=JSON.parse(data);
            
                                                                                var prod_name = "";
                                                                                var c_id = "";
                                                                                if(data.length>0){
                                                                                  
                                                                                }
                                                                                else{
                                                                                   
                                                                                }
                                                            for(k=0;k<data.length;k++){
                                                                     
                                                            $("."+data[k].company_id+"").append("<p class='pen-icon'><a id='"+data[k].product_id+"' onclick='edit_prod(this.id)'><span class='first_fa'><i class='icon-pencil'></i></span></a><a id='"+data[k].product_id+"' onclick='deletes(this.id)'><span class=''><i class='fa fa-trash'></i></span></a></p><button class='accordion2' id='"+data[k].product_id+"'><img src='"+data[k].product_image+"' width='140'><div class='pdt-content center-block'><p class='pdt-title'>"+data[k].product_name+"</p><p>"+data[k].product_desc+"</p></div></button><div class='panel2 sub-products "+data[k].product_id+"'><a  id='"+data[k].product_id+"/"+data[k].product_name+"/"+data[k].company_id+"' onclick='adding_sub(this.id)' class='btn btn-circle green btn-sm pull-right'><i class='fa fa-plus'></i> Add Sub Category</a><span class='clearfix'></span><br></div>");

                          $.ajax({
                              url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_subproduct_company1",
                              type        :   "POST",
                              data        :   {'prod_id':data[k].product_id},
                              datatype    :   "JSON",
                              success     :   function(data){
                                                  var data=JSON.parse(data);
                                                    for(j=0;j<data.length;j++){
                                                      $('.'+data[j].prod_id).append("<p class='pen-icon'><a id='"+data[j].cat_id+"' onclick='edit_sub(this.id)'><span class='first_fa'><i class='icon-pencil'></i></span></a><a id='"+data[j].cat_id+"' onclick='deletes_category(this.id)'><span class=''><i class='fa fa-trash'></i></span></a></p><div class='col-sm-3'><img src='"+data[j].cat_image+"' ></div><div class='col-sm-9 sub-content'><p class='pdt-title'>"+data[j].cat_name+"</p>"+data[j].cat_desc+"</div><span class='clearfix'></span><br>");
                                                    }
                                              }
                          });

                          var acc = document.getElementsByClassName("accordion2");
                          var i;

                          for (i = 0; i < acc.length; i++) {
                            acc[i].onclick = function() {
                              this.classList.toggle("active");
                              var panel = this.nextElementSibling;
                              if (panel.style.maxHeight){
                                panel.style.maxHeight = null;
                              } else {
                                panel.style.maxHeight = panel.scrollHeight + "px";
                              }
                            }
                          }

                          $(".pen-icon").hover(function () {
                            $(".design-nav").animate({
                                opacity: "1"
                            }, {
                                queue: false
                            });
                              }, function () {
                            $(".design-nav").animate({
                                opacity: "0"
                            }, {
                                queue: false
                            });
                        });
                                      
                              }

                               $("#product_append div:first").addClass("active");                                                                        
                                },
                                  });
                                                        
                                 }
                             },
                            });
			})			
			
			$("#tree_5").jstree({
				"core" : {
					"themes" : {
						"responsive": false 
					}, 
					// so that create works
					"check_callback" : true,
					'data':{
						'url':'<?php echo base_url(); ?>index.php?/controller_superad/getproduct1',
						//'plugins' : [ "wholerow", "checkbox" ],
						'dataType':'json',
					},
				},
				"types" : {
					"default" : {
						"icon" : "fa fa-folder icon-state-warning icon-lg"
					},
					"file" : {
						"icon" : "fa fa-file icon-state-warning icon-lg"
					}
				},
				"state" : { "key" : "demo2" },
				"plugins" : ["themes", "json", "grid", "dnd", "contextmenu", "search"],
				"contextmenu":{        
				    "items": function($node) {
				        var tree = $("#tree_5").jstree(true);
				        return {
				            "Create": {
				                "separator_before": false,
				                "separator_after": false,
				                "label": "Create",
				                "action": function (obj) {
				                	var checked,selected_node,chosen_nodes,datastring,get_storedid,parent,child,g_parents,g_child_d,g_child;
									var company_id
				                	checked =  $("#tree_5").jstree(true).get_selected(true);
				                	selected_node= JSON.stringify(checked);
									chosen_nodes=JSON.parse(selected_node);
									//console.log(chosen_nodes);
									parent=chosen_nodes[0]['parent'];
									g_parents=chosen_nodes[0]['parents'].length;									
									if(g_parents==1){
										/* parent=chosen_nodes[0]['parents'][0];
										add_prod(parent); */
										parent=chosen_nodes[0]['id'];
										add_prod(parent);
									}else if(g_parents==2){
										/* parent=chosen_nodes[0]['parents'][1];
										child=chosen_nodes[0]['parents'][0];
										add_sub(parent,child); */
										parent=chosen_nodes[0]['parents'][0];
										child=chosen_nodes[0]['id'];
										add_sub(parent,child); 
									}									
				                }
				            },
				            "Rename": {
				                "separator_before": false,
				                "separator_after": false,
				                "label": "Edit",
				                "action": function (obj) { 
				                	var checked,selected_node,chosen_nodes,datastring,get_storedid,parent,child,g_parents,g_child_d,g_child;
									var company_id
				                	checked =  $("#tree_5").jstree(true).get_selected(true);
				                	selected_node= JSON.stringify(checked);
									chosen_nodes=JSON.parse(selected_node);
									//console.log(chosen_nodes);
									parent=chosen_nodes[0]['parent'];
									g_parents=chosen_nodes[0]['parents'].length;	
									if(g_parents==1){
										swal("Can not edit or delete a company");
									}						
									else if(g_parents==2){
										id=chosen_nodes[0]['id'];
										edit_prod(id);
									}else{
										id=chosen_nodes[0]['id'];
										edit_sub(id);
									}
				                }
				            },                         
				            "Remove": {
				                "separator_before": false,
				                "separator_after": false,
				                "label": "Delete",
				                "action": function (obj) { 
				                	var checked,selected_node,chosen_nodes,datastring,get_storedid,parent,child,g_parents,g_child_d,g_child;
									var company_id
				                	checked =  $("#tree_5").jstree(true).get_selected(true);
				                	selected_node= JSON.stringify(checked);
									chosen_nodes=JSON.parse(selected_node);
									//console.log(chosen_nodes);
									parent=chosen_nodes[0]['parent'];
									g_parents=chosen_nodes[0]['parents'].length;							
									if(g_parents==1){
										swal("Can not edit or delete a company");
									}						
									else if(g_parents==2){
										id=chosen_nodes[0]['id'];
										deletes(id);
									}else{
										id=chosen_nodes[0]['id'];
										deletes_category(id);
									}
				                }
				            }
				        };
				    }
				}
			});
		</script> 
		<script>

  
     
     function company_change(){       
     var company_id = $('#company_list').val(); 
     $( "#companyadd" ).find( 'option[value="' + company_id + '"]' ).prop( "selected", true );   
     $('.nav-tabs a[href=".'+company_id+'"]').tab('show');     
     }

    $('#prodcompany_id').change(function() {
      var company_id= $('#prodcompany_id').val();
        $('#product_names').html('');
        $.ajax({
              url: "<?php echo base_url();?>" + "index.php?/controller_call/modal_product",
              type: 'POST',
              data:{'company_id':company_id},
              success: function(result) {
                    //console.log(result);
                    var result= JSON.parse(result);
                    if(result.length>0){
                      $('#product_names').append('<option selected value="dummydata" disabled>Product Category</option>');
                       for(i=0;i<result.length;i++)
                       {
                        $('#product_names').append('<option value='+result[i]['product_id']+'>'+result[i]['product_name']+'</option>');
                       }
                    }
                    else{
                      $('#product_names').append('<option selected value="dummydata" disabled>No Product Category found</option>');
                      swal('Add Product-Category first to proceed!!');
                    }
                    
              }
            });
    });

		function add_prod(parent){
			//$('#company').val(parent);
			$.ajax({
                url         :   "<?php echo base_url();?>index.php?/controller_admin/productid_check",
                type        :   "POST",
                data        :   "",
                cache       :   false,
                success     :   function(data){                                     
                                    $('#product_id').val($.trim(data));
                                    $('#myModal1').modal('show');
                                },
            })
			
		}
    var form_name = "#add_product";
    $(form_name).validate({
                doNotHideMessage:!0,errorElement:"span",errorClass:"help-block help-block-error",focusInvalid:!1,
                rules: {
                    company:{required:!0},
                    product_modal:{required:!0},
                    product_name:{minlength:4,maxlength:50,required:!0},  
                    //product_desc:{required:!0,minlength:10,maxlength:200},
                   
                },            
            });

            var form_name1 = "#edit_product";
    $(form_name1).validate({
                doNotHideMessage:!0,errorElement:"span",errorClass:"help-block help-block-error",focusInvalid:!1,
                rules: {
                   
                    product_modal1:{required:!0},
                    product_name1:{minlength:4,maxlength:50,required:!0},  
                    
                   
                },            
            });


		$('#addproduct').click(function() {
      if ($(form_name).valid()){
            $('#rowdata').empty();
            var product_id = $("#product_id").val();
            var product_name = $("#product_name").val();
            var product_modal = $("#product_modal").val();
            var product_desc = $("#product_desc").val();
            var company = $("#companyadd").val();
            var product_image = $('#product_image').val().toString().split('.').pop().toLowerCase();
            if ($.inArray(product_image, ['gif', 'jpg', 'jpeg', 'png']) !== -1) {
                var product_image = $('#product_image').prop('files')[0];
                var form_data = new FormData();
                form_data.append('product_id', product_id);
                form_data.append('product_name', product_name);
                form_data.append('company', company);
                form_data.append('product_modal', product_modal);
                form_data.append('product_desc', product_desc);
                form_data.append('product_image', product_image);
                $.ajax({
                    url: "<?php echo base_url(); ?>index.php?/controller_admin/insertproduct",
                    type: "POST",
                    data: form_data, 
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        data = $.trim(data);
                        if (data == "product added Successfully") {
                        	$('#myModal1').modal('hide'); 
                        	swal({
                        		  title: "Product added Successfully",                        		  
                        		  type: "success",
                        		  showCancelButton: false,
                        		  confirmButtonClass: "btn-danger",
                        		  confirmButtonText: "Ok!",
                        		  cancelButtonText: "No, cancel plx!",
                        		  closeOnConfirm: false,
                        		  closeOnCancel: false
                        		},
                        		function(isConfirm) {
                        		  if (isConfirm) {
                        		    window.location.reload();
                        		  }
                        		});
                        }
                         else {
                            $('#rowdata').append(data);
                            $('#myModal1').animate({ scrollTop: 0 });
                            $('.error').show();
                        }
                    },
                });
            }
             else {
                swal('Product Image should be a Image file');
                
            }
        }
    });

       

        function edit_prod(id){
    		$.ajax({
                url: "<?php echo base_url(); ?>index.php?/controller_superad/getdetails_product",
                type: "POST",
                data: {
                    'id': id
                }, // {action:'$funky'}
                datatype: "JSON",
                cache: false,
                success: function(data) {
                    var data = JSON.parse(data);
                   
                    $('#id1').val(data['id']);
                    $('#product_id1').val(data['product_id']);
                    $('#product_name1').val(data['product_name']);
                    $('#product_modal1').val(data['product_modal']);
                    $('#product_desc1').val(data['product_desc']);
                    $('#product_image2').val(data['product_image']);
                    $('#companyid1').val(data['company_id']);
                    $('#pro_edit_image').attr("src",data['product_image']);
                    $('#rowdata_1').empty();
                    $('#edits').modal('show');                 
                },
            });
        }

        function submit_product() {
            if ($(form_name1).valid()){
            $('#rowdata_1').empty();
            var id1 = $("#id1").val();
            var product_id1 = $("#product_id1").val();
            var product_name1 = $("#product_name1").val();
            var companyid1 = $("#companyid1").val();
            var product_modal1 = $("#product_modal1").val();
            var product_desc1 = $("#product_desc1").val();
            var product_image2 = $("#product_image2").val();
            var product_image1 = $('#product_image1').val().toString().split('.').pop().toLowerCase();
            if (product_image1 == "") {
                product_image1 = product_image2;
                $.ajax({
                    url: "<?php echo base_url(); ?>index.php?/controller_admin/edit_product1",
                    type: "POST",
                    data: {
                        'id1': id1,
                        'product_id1': product_id1,
                        'product_name1': product_name1,
                        'product_modal1': product_modal1,
                        'product_desc1': product_desc1,
                        'companyid1': companyid1,
                        'product_image1': product_image1
                    }, 
                    success: function(data) {
                        data = $.trim(data);
                       
                        if (data == "Product updated Successfully") {
                        	$('#edits').modal('hide'); 
                        	swal({
                        		  title: "Product updated Successfully",                        		  
                        		  type: "success",
                        		  showCancelButton: false,
                        		  confirmButtonClass: "btn-danger",
                        		  confirmButtonText: "Ok!",
                        		  cancelButtonText: "No, cancel plx!",
                        		  closeOnConfirm: false,
                        		  closeOnCancel: false
                        		},
                        		function(isConfirm) {
                        		  if (isConfirm) {
                        		    window.location.reload();
                        		  }
                        		});
                        } else {
                            $('#rowdata_1').append(data);
                            $('#edits').animate({ scrollTop: 0 });
                            $('.error').show();
                        }
                    },
                
                });
            } else {
                if ($.inArray(product_image1, ['gif', 'jpg', 'jpeg', 'png']) !== -1) {
                    var product_image1 = $('#product_image1').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('id1', id1);
                    form_data.append('product_id1', product_id1);
                    form_data.append('product_name1', product_name1);
                    form_data.append('product_modal1', product_modal1);
                    form_data.append('product_desc1', product_desc1);
                    form_data.append('companyid1', companyid1);
                    form_data.append('product_image1', product_image1);
                    $.ajax({
                        url: "<?php echo base_url(); ?>index.php?/controller_admin/edit_product",
                        type: "POST",
                        data: form_data, // {action:'$funky'}
                        //datatype  :   "JSON",
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            data = $.trim(data);
                            if (data == "Product updated Successfully") {
                            	$('#edits').modal('hide'); 
                            	swal({
                            		  title: "Product updated Successfully",                        		  
                            		  type: "success",
                            		  showCancelButton: false,
                            		  confirmButtonClass: "btn-danger",
                            		  confirmButtonText: "Ok!",
                            		  cancelButtonText: "No, cancel plx!",
                            		  closeOnConfirm: false,
                            		  closeOnCancel: false
                            		},
                            		function(isConfirm) {
                            		  if (isConfirm) {
                            		    window.location.reload();
                            		  }
                            		});
                            } else {
                                $('#rowdata_1').append(data);
                                $('#edits').animate({ scrollTop: 0 });
                                $('.error').show();
                            }
                        },
                    });
                } else {
                	swal('product Category Image should be a image file');
                    //bootbox.alert('Product image should be a image file');
                }
            }
            }
        }
        function deletes(id) {
        	swal({
        		  title: "Are you sure?",
        		  text: "You will not be able to recover this Product Category!",
        		  type: "warning",
        		  showCancelButton: true,
        		  confirmButtonClass: "btn-danger",
        		  confirmButtonText: "Yes, delete it!",
        		  cancelButtonText: "No, keep it!",
        		  closeOnConfirm: false,
        		  closeOnCancel: false
        		},
        		function(isConfirm) {
        		  if (isConfirm) {
        			  $.ajax({
                          url: "<?php echo base_url(); ?>index.php?/controller_superad/deleteproduct",
                          type: "POST",
                          data: {
                              'id': id
                          }, // {action:'$funky'}
                          //datatype  :   "JSON", 
                          cache: false,
                          success: function(data) {
                              if (data == 1) {
                            	  swal({
                            		  title: "Product Category Deleted Successfully",                        		  
                            		  type: "success",
                            		  showCancelButton: false,
                            		  confirmButtonClass: "btn-danger",
                            		  confirmButtonText: "Ok!",
                            		  cancelButtonText: "No, cancel plx!",
                            		  closeOnConfirm: false,
                            		  closeOnCancel: false
                            		},
                            		function(isConfirm) {
                            		  if (isConfirm) {
                            		    window.location.reload();
                            		  }
                            		});
                              } else {
                            	  swal({
                            		  title: "Product Category Not Deleted Successfully",                        		  
                            		  type: "success",
                            		  showCancelButton: false,
                            		  confirmButtonClass: "btn-danger",
                            		  confirmButtonText: "Ok!",
                            		  cancelButtonText: "No, cancel plx!",
                            		  closeOnConfirm: false,
                            		  closeOnCancel: false
                            		},
                            		function(isConfirm) {
                            		  if (isConfirm) {
                            		    window.location.reload();
                            		  }
                            		});
                              }
                          },
                      });
        		  } else {
					 swal.close();
        		    //swal("Cancelled", "Your Product is safe :)", "error");
        		  }
        		});
        }

    function adding_sub(id){
        console.log(id);
        var res = id.split("/");
        console.log(res[1]);
        console.log(res[2]);        
        $("#product_id_new").val(res[0]);
        $("#product_name_new").val(res[1]);
        $("#c_id_new").val(res[2]);
        $.ajax({
                 url: "<?php echo base_url();?>index.php?/controller_admin/subproductid_check",
                 type: "POST",
                 data: "",
                 cache: false,
                 success: function(data) {                     
                     $('#subproduct_id_new').val($.trim(data));                     
                 },
             });
        $('#new_subcategory').modal('show');
        //console.log(prod_name);
        //console.log(c_id);
      }

		function add_sub(){			
			/*$('#c_id').val(parent);	
			$('#product_names').val(child);*/		
			 $.ajax({
                 url: "<?php echo base_url();?>index.php?/controller_admin/subproductid_check",
                 type: "POST",
                 data: "",
                 cache: false,
                 success: function(data) {                     
                     $('#subproduct_id').val($.trim(data));
                     /*$.ajax({
                         url: "<?php echo base_url(); ?>index.php?/controller_superad/getdetails_product",
                         type: "POST",
                         data: {
                             'id': child
                         }, // {action:'$funky'}
                         datatype: "JSON",
                         cache: false,
                         success: function(data) {
                             var data = JSON.parse(data);
                             console.log(data);
                             $('#product_value').val(data['product_name']);                                              
                         },
                     });*/
                     $('#myModal2').modal('show');
                 },
             });
		}

    var formname ="#add_subproduct";
      $(formname).validate({
                doNotHideMessage:!0,errorElement:"span",errorClass:"help-block help-block-error",focusInvalid:!1,
                rules: {
                    prodcompany_id:{required:!0},
                    product_names:{required:!0},
                    subproduct_name:{minlength:4,maxlength:50,required:!0},   
                    subproduct_modal:{required:!0,minlength:10,maxlength:200},
                    //subproduct_desc:{required:!0,minlength:10,maxlength:200},
                    //subproduct_image:{required:!0},
                   // received_date   :{required:!0},
                },
            
        });
    $('#addsubproduct').click(function() {
      if ($(formname).valid()){

            $('#rowdata').empty();
            $('#rowdata_category').empty();
            var subproduct_id = $("#subproduct_id").val();
            var subproduct_name = $("#subproduct_name").val();
            var product_name = $("#product_names").val();
            //var companyid = $("#c_id").val();
            var company_id = $("#prodcompany_id").val();
            //var companyid = $("#subproduct_modal1").val();
            var subproduct_modal = $("#subproduct_modal").val();
            var subproduct_desc = $("#subproduct_desc").val();
            var subproduct_image = $('#subproduct_image').val().toString().split('.').pop().toLowerCase();
           
            if ($.inArray(subproduct_image, ['gif', 'jpg', 'jpeg', 'png']) !== -1) {
                var subproduct_image = $('#subproduct_image').prop('files')[0];
                var form_data = new FormData();
                form_data.append('subproduct_id', subproduct_id);
                form_data.append('subproduct_name', subproduct_name);
                form_data.append('subproduct_modal', subproduct_modal);
                form_data.append('subproduct_desc', subproduct_desc);
                form_data.append('product_name', product_name);
                form_data.append('subproduct_image', subproduct_image);
                form_data.append('companyid', company_id);
                $.ajax({
                    url: "<?php echo base_url(); ?>index.php?/controller_admin/insertsubproduct",
                    type: "POST",
                    data: form_data, // {action:'$funky'}
                    //datatype  :   "JSON",
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        //console.log(data);
                        if (data == 1) {
                        	swal({
                      		  title: "Sub category added successfully",                        		  
                      		  type: "success",
                      		  showCancelButton: false,
                      		  confirmButtonClass: "btn-danger",
                      		  confirmButtonText: "Ok!",
                      		  cancelButtonText: "No, cancel plx!",
                      		  closeOnConfirm: false,
                      		  closeOnCancel: false
                      		},
                      		function(isConfirm) {
                      		  if (isConfirm) {
                      		    window.location.reload();
                      		  }
                      		});
                        } else {
                            $('#rowdata_category').append(data);
                            $('#myModal2').animate({ scrollTop: 0 });
                            $('.error').show();
                        }
                    },
                });
            } else {
            	 swal('Sub category image should be a image file');
            }
        }
        // else{
        //     $('#rowdata_category').html("All Fields are Mandatory!");
        //     $('#myModal2').animate({ scrollTop: 0 });
        //     $('.error').show();
        // }
    })

    var sub_formname ="#add_subproduct_new";
      $(sub_formname).validate({
                doNotHideMessage:!0,errorElement:"span",errorClass:"help-block help-block-error",focusInvalid:!1,
                rules: {
                    //product_id_new:{required:!0},
                    product_name_new:{required:!0},
                    subproduct_name_new:{minlength:4,maxlength:50,required:!0},   
                    subproduct_modal_new:{required:!0,minlength:10,maxlength:200},
                    //subproduct_desc_new:{required:!0,minlength:10,maxlength:200},
                    //subproduct_image:{required:!0},
                   // received_date   :{required:!0},
                },            
        });

      $('#addsubproduct_new').click(function() {
        if ($(sub_formname).valid()){

            $('#rowdata_newcat').empty();
            //$('#rowdata_category').empty();
            var subproduct_id = $("#subproduct_id_new").val();
            var subproduct_name = $("#subproduct_name_new").val();
            var product_name = $("#product_id_new").val();
            //var companyid = $("#c_id").val();
            var company_id = $("#c_id_new").val();
            //var companyid = $("#subproduct_modal1").val();
            var subproduct_modal = $("#subproduct_modal_new").val();
            var subproduct_desc = $("#subproduct_desc_new").val();
            var subproduct_image = $('#subproduct_image_new').val().toString().split('.').pop().toLowerCase();
           
            if ($.inArray(subproduct_image, ['gif', 'jpg', 'jpeg', 'png']) !== -1) {
                var subproduct_image = $('#subproduct_image_new').prop('files')[0];
                var form_data = new FormData();
                form_data.append('subproduct_id', subproduct_id);
                form_data.append('subproduct_name', subproduct_name);
                form_data.append('subproduct_modal', subproduct_modal);
                form_data.append('subproduct_desc', subproduct_desc);
                form_data.append('product_name', product_name);
                form_data.append('subproduct_image_new', subproduct_image);
                form_data.append('companyid', company_id);
                $.ajax({
                    url: "<?php echo base_url(); ?>index.php?/controller_admin/insertsubproduct_new",
                    type: "POST",
                    data: form_data, // {action:'$funky'}
                    //datatype  :   "JSON",
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        //console.log(data);
                        if (data == 1) {
                          swal({
                            title: "Sub category added successfully",                             
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false
                          },
                          function(isConfirm) {
                            if (isConfirm) {
                              window.location.reload();
                            }
                          });
                        } else {
                            $('#rowdata_newcat').append(data);
                            $('#new_subcategory').animate({ scrollTop: 0 });
                            $('.error').show();
                        }
                    },
                });
            } else {
               swal('Sub category image should be a image file');
            }
        }
        // else{
        //     $('#rowdata_category').html("All Fields are Mandatory!");
        //     $('#myModal2').animate({ scrollTop: 0 });
        //     $('.error').show();
        // }
    })

    function edit_sub(id){
			$.ajax({
                url: "<?php echo base_url(); ?>index.php?/controller_superad/getdetails_subproduct",
                type: "POST",
                data: {'id': id}, // {action:'$funky'}
                datatype: "JSON",
                cache: false,
                success: function(data) {
                    var data = JSON.parse(data);
                    //console.log(data);
                    $('#id1').val(data['id']);
                    $('#subproduct_id1').val(data['cat_id']);
                    $('#subproduct_name1').val(data['cat_name']);
                    $('#subproduct_modal1').val(data['cat_modal']);
                    $('#subproduct_desc1').val(data['cat_desc']);
                    //$('#product_name1').val(data['product_name']);
                    $('#subproduct_image2').val(data['cat_image']);
                    $('#sub_pro_edit_image').attr("src",data['cat_image']);
                    $('select[name="product_name_1"] option[value="' + data['prod_id'] + '"]').attr("selected", true);
                    $('#rowdata_category_1').empty();
                    $('#edits_category').modal('show');                                  
                },
            });
	    }
		function submit_subproduct() {
            $('#rowdata_category_1').empty();
            var id1 = $("#id1").val();
            var subproduct_id1 = $("#subproduct_id1").val();
            var subproduct_name1 = $('#subproduct_name1').val();
            var product_name1 = $("#product_name_1").val();
            var subproduct_modal1 = $("#subproduct_modal1").val();
            var subproduct_desc1 = $("#subproduct_desc1").val();
            var subproduct_image2 = $("#subproduct_image2").val();
            var subproduct_image1 = $('#subproduct_image1').val().toString().split('.').pop().toLowerCase();
            if (subproduct_image1 == "") {
                $.ajax({
                    url: "<?php echo base_url(); ?>index.php?/controller_admin/edit_subproducts1",
                    type: "POST",
                    data: {
                        'id1': id1,
                        'subproduct_id1': subproduct_id1,
                        'product_name1': product_name1,
                        'subproduct_modal1': subproduct_modal1,
                        'subproduct_desc1': subproduct_desc1,
                        'subproduct_name1': subproduct_name1
                    }, // {action:'$funky'}
                    //datatype  :   "JSON",
                    success: function(data) {
                        //console.log(data);
                        data = $.trim(data);
                        if (data == "Sub category updated Successfully") {
                        	swal({
                        		  title: "Sub category updated successfully",                        		  
                        		  type: "success",
                        		  showCancelButton: false,
                        		  confirmButtonClass: "btn-danger",
                        		  confirmButtonText: "Ok!",
                        		  cancelButtonText: "No, cancel plx!",
                        		  closeOnConfirm: false,
                        		  closeOnCancel: false
                        		},
                        		function(isConfirm) {
                        		  if (isConfirm) {
                        		    window.location.reload();
                        		  }
                        		});
                        } else {
                            $('#rowdata_category_1').append(data);
                            $('#edits_category').animate({ scrollTop: 0 });
                            $('.error').show();
                        }
                    },
                });
            } else {
                if ($.inArray(subproduct_image1, ['gif', 'jpg', 'jpeg', 'png']) !== -1) {
                    var subproduct_image1 = $('#subproduct_image1').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('id1', id1);
                    form_data.append('subproduct_id1', subproduct_id1);
                    form_data.append('product_name1', product_name1);
                    form_data.append('subproduct_name1', subproduct_name1);
                    form_data.append('subproduct_modal1', subproduct_modal1);
                    form_data.append('subproduct_desc1', subproduct_desc1);
                    form_data.append('subproduct_image1', subproduct_image1);
                    $.ajax({
                        url: "<?php echo base_url(); ?>index.php?/controller_admin/edit_subproducts",
                        type: "POST",
                        data: form_data, // {action:'$funky'}
                        //datatype  :   "JSON",
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            data = $.trim(data);
                            if (data == "Sub category updated Successfully") {
                            	swal({
                          		  title: "Sub category updated successfully",                        		  
                          		  type: "success",
                          		  showCancelButton: false,
                          		  confirmButtonClass: "btn-danger",
                          		  confirmButtonText: "Ok!",
                          		  cancelButtonText: "No, cancel plx!",
                          		  closeOnConfirm: false,
                          		  closeOnCancel: false
                          		},
                          		function(isConfirm) {
                          		  if (isConfirm) {
                          		    window.location.reload();
                          		  }
                          		});
                            } else {
                                $('#rowdata_category_1').append(data);
                                $('#edits_category').animate({ scrollTop: 0 });
                                $('.error').show();
                            }
                        },
                    });
                } else {
                    swal('Sub category image should be a image file');
                }
            }
        }
        function deletes_category(id) {
        	swal({
        		  title: "Are you sure?",
        		  text: "You will not be able to recover this Sub Category!",
        		  type: "warning",
        		  showCancelButton: true,
        		  confirmButtonClass: "btn-danger",
        		  confirmButtonText: "Yes, delete it!",
        		  cancelButtonText: "No, keep it!",
        		  closeOnConfirm: false,
        		  closeOnCancel: false
        		},
        		function(isConfirm) {
        		  if (isConfirm) {
        			  $.ajax({
                          url: "<?php echo base_url(); ?>index.php?/controller_superad/deletesubproduct",
                          type: "POST",
                          data: {
                              'id': id
                          }, // {action:'$funky'}
                          //datatype  :   "JSON", 
                          cache: false,
                          success: function(data) {
                              if (data == 1) {
                            	  swal({
                            		  title: "Sub Category Deleted Successfully",                        		  
                            		  type: "success",
                            		  showCancelButton: false,
                            		  confirmButtonClass: "btn-danger",
                            		  confirmButtonText: "Ok!",
                            		  cancelButtonText: "No, cancel plx!",
                            		  closeOnConfirm: false,
                            		  closeOnCancel: false
                            		},
                            		function(isConfirm) {
                            		  if (isConfirm) {
                            		    window.location.reload();
                            		  }
                            		});
                              } else {
                            	  swal({
                            		  title: "Sub Category Not Deleted Successfully",                        		  
                            		  type: "success",
                            		  showCancelButton: false,
                            		  confirmButtonClass: "btn-danger",
                            		  confirmButtonText: "Ok!",
                            		  cancelButtonText: "No, cancel plx!",
                            		  closeOnConfirm: false,
                            		  closeOnCancel: false
                            		},
                            		function(isConfirm) {
                            		  if (isConfirm) {
                            		    window.location.reload();
                            		  }
                            		});
                              }
                          },
                      });
        		  } else {
						swal.close();
						//swal("Cancelled", "Your Product is safe :)", "error");
        		  }
        		});
        }
		</script>
        
         <script>
 $('.collapse').on('show.bs.collapse', function (e) {
    $('.collapse').not(e.target).removeClass('in');
});
 </script>
 <script>
 $('.tab-pane img').click(function(){
   $('.selected').removeClass('selected'); // removes the previous selected class
   $(this).addClass('selected'); // adds the class to the clicked image
});
 </script>
<div class="modal fade" id="more-info" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Terms and Conditions</h4>
        </div>
        <div class="modal-body">
          <p>terms and condition contents here.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
    </body>
</html>