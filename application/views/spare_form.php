<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php
			$company_id=$this->session->userdata('companyid');
               $region=$user['region'];$area=$user['area'];$location=$user['location'];
			include 'assets/lib/cssscript.php'?>
<style>
.dt-buttons{
    //background-color: #2f353b;
display:none;
}
	 .sweet-alert.showSweetAlert.visible{
					     z-index: 999999;
    						border: 1px solid #e43c21;
					             margin-top: -110px !important;
				 }
	
	.dataTables_filter{
   		 text-align: right;
	}
</style>
         </head>
         <!-- END HEAD -->
         <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header_service.php"?>
               <!-- END HEADER -->
              <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/service_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->							  
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption"> SPARE REQUEST </div>
									<ul class="nav nav-tabs">
                                             <li class="active">
                                                <a href="#assigned" data-toggle="tab">Spare Request</a>
                                             </li>
                                             <li>
                                                <a href="#accepted" data-toggle="tab">Imprest Spare Request</a>
                                             </li>
                                          </ul>
                                 </div>
                                 <div class="portlet-body">
                                          <div class="tab-content">
                                             <div class="tab-pane active" id="assigned">
                                             
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location" onChange="day();" name="role1">
                                                         <option value="" selected >All Location</option>
                                                      </select>
                                                   </div>
                                                    <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id" onChange="day();" name="role1">
                                                         <option value="" selected >All Product-Category</option>
                                                      </select>
                                                   </div>
                                                </div><span class="clearfix"></span><br>
                                                <div class="table-responsive">
                                                   <table class="table table-hover table-bordered" id="">
                                                      <thead>
                                                         <tr>
															<th>Ticket Id</th>
                                                            <th style="text-align:center">Customer Name</th>
                                                            <th style="text-align:center">Technician Id</th>
                                                            <th style="text-align:center">Product-Category</th>
                                                            <th style="text-align:center">Sub-Category</th>
                                                            <th style="text-align:center">Update Spare Qty</th> 
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_req" align="center"></tbody>
                                                   </table>
												   
												</div><div class="row">
												   <div class="col-lg-12">
											<!--		<button type="button" style="margin-left:7px; display:none;" 
 class="btn blue btn-outline btn-circle btn-md pull-right" id="download_spareform">Download</button>  

<button type="button" class="btn blue btn-outline btn-circle btn-md  pull-right" id="upload_form">Update Spare Receipt</button> 	 -->												
													</div>
													</div>
                                             </div>
                                             <div class="tab-pane" id="accepted">
                                                <div class="table-responsive">

                                                <table class="table table-hover table-bordered" id="">
                                                    <thead>
                                                       <tr>
                                                       <th style="text-align:center" >ID</th>
													<th style="text-align:center" >Technician ID</th>
													<th style="text-align:center" >Technician Name</th>
													<th style="text-align:center" >Update Imprest Qty</th>
													</tr>
                                                    </thead>
                                                        <tbody id="tbdy_attendance" align="center">
						 <?php  							
						foreach ($imprest as $row) {
							$string  = json_encode(json_decode($row['spare_array']));
								?>
								<tr>
								   <td><?php echo $row['sno']; ?></td>
									<td  id="<?php echo $row['employee_id']; ?>" onvvClick="hover(this.id,'<?php echo $row['first_name']; ?>','<?php echo $row['skill_level']; ?>','<?php echo $row['contact_number']; ?>','<?php echo $row['location']; ?>','<?php echo $row['skill_level']; ?>','<?php echo $row['today_task_count']; ?>')";><?php echo $row['employee_id']; ?></td>
<td ><?php echo $row['first_name']; ?></td>
									<td style="text-align:center">
									<button class="btn blue btn-outline btn-icon-only btn-circle" id="<?php echo $row['tech_id']; ?>" onclick=hover_spare('<?php echo $row['tech_id']; ?>',<?php echo $row['sno']; ?>,'<?php echo $string; ?>');><i class="fa fa-edit"></i></button>
									</td>
													</tr>
													<?php } ?>
												</tbody>
                                                </table></div>
<div class="row">
												   <div class="col-lg-12">
												   <?php if(!empty($imprest))
	/*
												   {?>
												   <button type="button" style="margin-left:7px; " class=" btn blue btn-outline btn-circle btn-md  pull-right" id="imprest_download_spareform">Download</button> <?php } else{?>
												   <button type="button" style="margin-left:7px; display:none;" class=" btn blue btn-outline btn-circle btn-md  pull-right" id="imprest_download_spareform">Download</button> <?php }
*/	   
													   ?>
												   
<!--<button type="button" class=" btn blue btn-outline btn-circle btn-md  pull-right" id="upload_form1">Update Imprest Spare</button> -->     
													</div>
													</div>
                                            </div>
                                             
                                          </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>
<!-- BEGIN FOOTER -->
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
            </div>
            <!--Modal Starts-->
            <!-- Modal raju -->
			
		   <div id="myModal" class="modal fade" role="dialog"> 
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modal_head">Details</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id='modal_display_edit'>
						</form>
                     </div>
                   <!--  <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" data-dismiss="modal">OK</button>
                     </div> -->
                  </div>
               </div>
            </div>	 
			 
            <div id="myModal" class="modal fade" role="dialog"> 
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modal_head">Details</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id='modal_display'>
						</form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" data-dismiss="modal">OK</button>
                     </div>
                  </div>
               </div>
            </div>
			 
			     <div id="myModalspare" class="modal fade" role="dialog"> 
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modal_head">Details</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id='modal_display'>
						</form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" data-dismiss="modal">OK</button>
                     </div>
                  </div>
               </div>
            </div>



<div id="myModal3" class="modal fade" role="dialog">
	  <div class="modal-dialog">
				<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title" id="modal_spare_head">Details</h5>
      </div>
      <div class="modal-body"id='modal_display' style="text-align:center !important">
		<div class="table-responsive">
			<table class="table table-hover table-bordered">
				<thead>
					<td class="text-center">Spare code</td>
					<td class="text-center">Spare Required</td>
					<td class="text-center">Spare Location</td>
				</thead>
				<tbody id="spare_details" align="center">
				</tbody>
			</table>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
            </div>

        </div>

			 <!-- Modal -->
            <div id="upload_tab" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Spare Receipt</h4>
                        <div class="error" style="display:none">
                           <label id="rowdata"></label>
                        </div>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                                                   <div class="form-wizard">
                                                      <div class="form-body">
                                                         <div class="row">
                                                            <div class="col-md-12">
															<div class="col-sm-3 sample_id1 text-center">
											<?php $fname='ticket_spare_update.xlsx'; ?>
								<a class="btn btn-circle purple-sharp btn-outline sbold sample-temp" style="margin-left: -10px !important;" href="<?php echo base_url(); ?>assets/templates/<?php echo $fname;?>">Sample Template</a>					
											<!--<button type="button" class="btn btn-circle btn-primary" >Sample Template</button>-->
										</div>
                                                                    <div class="col-sm-9 fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="input-group input-large">
                                                                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                                                <span class="fileinput-filename"> </span>
                                                                            </div>
																			<span class="input-group-addon btn default btn-file">
                                                                                <span class="fileinput-new" id="span-button"> Select file </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" id="fileUpload"> </span>
                                                                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                         </div>                                                         <br>
                                                      </div>
                                                      <!--<div class="form-actions">-->
                                                      <div class="row">
                                                         <div class="col-md-12" style="margin-left: 16% !important;">
                                                            <div class="col-md-6 col-md-offset-3">
                                                               
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <!--</div>-->
                                                   </div>
                                                </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" id="bulkupload" class="btn blue btn-outline btn-circle btn-md">Upload</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                     </div>
                  </div>
               </div>
            </div>
<!-- Modal -->
            <div id="upload_tab1" class="modal fade" role="dialog">
               <div class="modal-dialog modal-md">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Spare Receipt</h4>
                        <div class="error" style="display:none">
                           <label id="rowdata"></label>
                        </div>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                                                   <div class="form-wizard">
                                                      <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
															<div class="col-sm-3 text-center" id="sample_id">
											<?php $fname='impreset.xlsx'; ?>
								<a class="btn btn-circle purple-sharp btn-outline sbold sample-temp" style="margin-left: -10px !important;" href="<?php echo base_url(); ?>assets/templates/<?php echo $fname;?>">Sample Template</a>					
											<!--<button type="button" class="btn btn-circle btn-primary" >Sample Template</button>-->
										</div>
                                                                    <div class="col-sm-9 fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="input-group input-large">
                                                                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                                                <span class="fileinput-filename"> </span>
                                                                            </div>
																			<span class="input-group-addon btn default btn-file">
                                                                             <span class="fileinput-new" id="span-button"> Select file </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" id="add_excel1"> </span>
                                                                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                         </div>                                                         <br>
                                                      </div>
                                                      <!--<div class="form-actions">-->
                                                      <div class="row">
                                                         <div class="col-md-12" style="margin-left: 16% !important;">
                                                            <div class="col-md-6 col-md-offset-3">
                                                               
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <!--</div>-->
                                                   </div>
                                                </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" id="bulkupload1" class="btn blue btn-outline btn-circle btn-md">Upload</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                     </div>
                  </div>
               </div>
            </div>
		  
			<div id="sla_confirm" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="add_subproduct">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <p id="sla_detail_confirm"></p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                                               <button type="button" class="btn btn-circle red btn-outline" onClick="refresh();"><i class="fa fa-times"></i> OK</button>

                    </div>
                </div>
            </div>
        </div>	
		  
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>
		  <script>                
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
            $('#spare').addClass('open');
			  
			   $(document).ready(function() {
         $('[data-toggle="tooltip"]').tooltip(); 
				    var company_id="<?php echo $company_id;?>";
				    var region="<?php echo $region;?>";
				    var area="<?php echo $area;?>";
	/* $.ajax({
						
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'status':'11'},
         dataType: "json",
         success: function(data) {
         $('#area').html(' <option selected value="">All Area</option>');
         
         //console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         	
         }
      }
  }); */
	 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'11'},
         dataType: "json",
         success: function(data) {
			 $('#location').html(' <option selected value="">All Location</option>');
			 
			   //console.log(data);
			 for(i=0;i<data.length;i++)
			 {
				$('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
				
			 }
         }
	});
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'11'},
         dataType: "json",
         success: function(data) {
			 $('#product_id').html(' <option selected value="">All Product-Category</option>');
			 $
			  // console.log(data);
			 for(i=0;i<data.length;i++)
			 {
				$('#product_id').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
				
			 }
         }
   });
       
	/* 	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'11'},
         dataType: "json",
         success: function(data) {
         $('#region').html(' <option selected value="">All Region</option>');
         
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#region').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         	
         }
         }
         }); */
				   
         var company_id="<?php echo $company_id;?>";
         var filter=$('#day').val();
         var product_id=$('#product_id').val();
         var product_id1=$('#product_id1').val();
         var product_id2=$('#product_id2').val();
         var product_id3=$('#product_id3').val();
         var filter1=$('#day1').val();
         var filter2=$('#day2').val();
         var filter3=$('#day3').val();
		 
				    var region="<?php echo $region;?>";
				    var area="<?php echo $area;?>";
         var location=$('#location').val();
		 
				    var region1="<?php echo $region;?>";
				    var area1="<?php echo $area;?>";
         var location1=$('#location1').val();
		 var region2=$('#region2').val();
         var area2=$('#area2').val();
         var location2=$('#location2').val();
		 
				    var region3="<?php echo $region;?>";
				    var area3="<?php echo $area;?>";
         var location3=$('#location3').val();
			 
			 $('#tbody_req').empty();
			 $('.datatable3').DataTable().destroy();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_req",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region,'area':area,'location':location},
         dataType: "json",
         success: function(data) {
          // $('#tbody_req').html('');
         //console.log(data);
         if(data.length<1)
         {
         	 $('#tbody_req').empty();
			 $('.datatable3').DataTable().destroy();       
		 }
         else
         {  
$("#download_spareform").css({"display":"block"});	 
         for(i=0;i<data.length;i++)
         {
         	var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
                                                           var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
                                                           var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
                                                           var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	 
			var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");
	var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");	
var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");	
var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");	
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
			$('#tbody_req').append('<tr><td id="'+data[i].ticket_id+'"  onvclick=hover_ticket(this.id,"'+data[i].customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");>'+data[i].ticket_id+'</td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'"  onvclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");>'+data[i].employee_id+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td style="text-align:center"><!--<button class="btn blue btn-outline btn-icon-only btn-circle" onclick=show_spare("'+data[i].spare_code+'","'+data[i].quantity+'","'+data[i].spare_source+'");><i class="fa fa-eye"></i></button> --><button class="btn blue btn-outline btn-icon-only btn-circle" onclick=show_spare_edit("'+data[i].spare_code+'","'+btoa(unescape(encodeURIComponent(data[i].spare_modal)))+'","'+data[i].quantity+'","'+data[i].spare_source+'","'+data[i].ticket_id+'");><i class="fa fa-edit"></i></button></td></tr>');	
         }
         }
			  $('.datatable3').DataTable({"order": []});
         }
         });
				   
	
		            
         });
			  
	function refresh()
			{
				window.location.reload();
			}
        </script> 
			  <script type="text/javascript">
			  $('#upload_form').click(function(){
			  		 $('#upload_tab').modal('show');
			  });
				  
			  $('#upload_form1').click(function(){
			  		$('#upload_tab1').modal('show');
			  });
	   $('#bulkupload').click(function() {
                    var ext = $('#fileUpload').val().toString().split('.').pop().toLowerCase();
                    if ($.inArray(ext, ['xls', 'xlsx', 'csv']) == -1) {
                        swal('Please upload Excel file');
                            //return false;
                    } else {
                        var file_data = $('#fileUpload').prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('add_spare', file_data);
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url(); ?>index.php?/controller_service/bulk_spare',
                            contentType: false,
                            processData: false,
                            cache: false,
                            data: form_data,
                            success: function(data) {
								var data=JSON.parse(data);
								$('#sla_detail_confirm').html('');
								for(i=0;i<data.length;i++){
									$('#sla_detail_confirm').append(data[i]+"</br><hr>"); 
								}
								
								$('#sla_confirm').modal('show');
								/* $('#upload_tab').modal('hide');
								swal({
                                  title: data,                                  
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
								
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
								  else
								  {
								$('#upload_tab').modal('show');
								  }
                                }); */
                            },
                        });
                    }
                })
	   
      
         // TableManageButtons.init();
         function redirect(ticket_id,product_id,cat_id,location,tech_id)
         {
         sessionStorage.setItem('ticket_id', ticket_id);
         sessionStorage.setItem('product_id', product_id);
         sessionStorage.setItem('cat_id', cat_id);
         sessionStorage.setItem('location', location);
         sessionStorage.setItem('tech_id', tech_id);
         window.location.href = "<?php echo site_url('controller_service/assign_tech');?>";
         }
         function hover(tech_id,technician_name,tech_email,contact_number,location,skill,task_count)
         {
$('#modal_head').html('Technician Details');
location= location.replace(/\:/g," ");
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Technician ID</label><div class="col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+tech_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Name</label><div class=" col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+technician_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Email ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+tech_email+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Mobile </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+contact_number+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Skill</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+skill+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Ticket Count</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+task_count+'</div></div></div>');
         $('#myModal').modal('show');
         }
         function hover_ticket(ticket_id,customer_name,location,product_name,cat_name,problem,priority,call_tag,call_type,cust_contact)
         {
$('#modal_head').html('Ticket Details');
         var replaceSpace=problem;
         problem = problem.replace(/\:/g," ");	
         var product_name=product_name;
         product_name= product_name.replace(/\:/g," ");	
         var location=location;
         location= location.replace(/\:/g," ");	
         var cat_name=cat_name;
         cat_name= cat_name.replace(/\:/g," ");	
         var priority=priority;
         priority= priority.replace(/\:/g," ");	call_tag= call_tag.replace(/\:/g," ");	call_type= call_type.replace(/\:/g," ");	
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Ticket ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Customer Name</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+customer_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Contact Number</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cust_contact+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Product-Category </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+product_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Sub-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cat_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Call-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_tag+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Service-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_type+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Problem</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;" style=" text-align: left;">'+problem+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Priority</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+priority+'</div></div></div>');

         $('#myModal').modal('show');
         }
         function day()
         {
			  $('#tbody_req').empty();
			 $('.datatable3').DataTable().destroy();         
			 
         var company_id="<?php echo $company_id;?>";
         var filter=$('#day').val();
         var product_id=$('#product_id').val();
         
				    var region="<?php echo $region;?>";
				    var area="<?php echo $area;?>";
         var location=$('#location').val();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_req",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region,'area':area,'location':location},
         dataType: "json",
         success: function(data) {
        	 if(data.length<1)
         	{
				 $('#tbody_req').empty();
				 $('.datatable3').DataTable().destroy();         
         	}
		  else
		  {
			 for(i=0;i<data.length;i++)
			 {
				 $("#download_spareform").css({"display":"block"});	 
				var replaceSpace=data[i].prob_desc;
				var problem = replaceSpace.replace(/ /g, ":");	
															   var location=data[i].location;
				var location= location.replace(/ /g, ":");	
															   var product_name=data[i].product_name;
				var product_name= product_name.replace(/ /g, ":");	
															   var cat_name=data[i].cat_name;
				var cat_name= cat_name.replace(/ /g, ":");	 
				var priority=data[i].priority;
				var priority= priority.replace(/ /g, ":");	
	var tech_location=data[i].tech_loc;
				var tech_location= tech_location.replace(/ /g, ":");
	var call_type=data[i].call_type;
				var call_type= call_type.replace(/ /g, ":");	
	var call_tag=data[i].call_tag;
				var call_tag= call_tag.replace(/ /g, ":");		
				var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
				$('#tbody_req').append('<tr><td id="'+data[i].ticket_id+'"  onvclick=hover_ticket(this.id,"'+data[i].customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");>'+data[i].ticket_id+'</td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'"  onvclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","+tech_location+","'+data[i].skill_level+'","'+data[i].today_task_count+'");>'+data[i].employee_id+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td style="text-align:center"><button class="btn dark btn-outline btn-circle btn-sm" onclick=show_spare("'+data[i].spare_code+'","'+data[i].quantity+'","'+data[i].spare_source+'");>View</button></td></tr>');	
			 }
         }
$('.datatable3').DataTable({"order": []});
         }
         });
         }
				  
	 function show_spare(spare_code,quantity,spare_source)
		 {
			
			 var spare_codes = spare_code.split(',');
			 var spare_quantity = quantity.split(',');
			 var spare_sources = spare_source.split(',');
			 $('#modal_display').html('');
			 $('#modal_display').html('<table class="table table-hover table-bordered" id=""><thead><tr><th class="text-center">Spare Code</th><th class="text-center">Spare Quantity</th><th class="text-center">Spare Location</th></tr></thead><tbody id="spare_body" align="center">');
			 for(i=0;i<spare_codes.length;i++)
			 {
				 
			$('#spare_body').append('<tr><td>' + spare_codes[i] + '</td><td>' + spare_quantity[i]+ '</td><td>' + spare_sources[i] + '</td></tr>');
			 }
			 $('#modal_display').append('</tbody></table>');
$('#modal_head').html('View Spare Details');
			$('#myModal').modal('show');
		 }
				  
				  ////////////raju start
				  
	 function show_spare_edit(spare_code,spare_modal,quantity,spare_source,ticketid)
		 {
			  var ticketid = ticketid;
			 var spare_codes = spare_code.split(',');
			var spare_modal= decodeURIComponent(escape(window.atob(spare_modal)));	 ////////// raju
			  var spare_modal = spare_modal.split(',');
			 var spare_quantity = quantity.split(',');
			 var spare_sources = spare_source.split(',');
			
			 $('#modal_display_edit').html('');
			 $('#modal_display_edit').html('<table class="table table-hover table-bordered" id=""><thead><tr><th class="text-center">Spare Code</th><th class="text-center">Spare Modal</th><th class="text-center">Request Quantity</th><th class="text-center">Spare Location</th><th class="text-center">Stock Qty</th></tr></thead><tbody id="spare_body" align="center">');
			 for(i=0;i<spare_codes.length;i++)
			 {
				 
			$('#spare_body').append('<tr><td>' + spare_codes[i] + '</td><td>' + spare_modal[i]+ '</td><td>' + spare_quantity[i]+ '</td><td>' + spare_sources[i] + '</td><td><input type="text" width="20" name="stkqty" id="stkqty_'+spare_codes[i]+'"></td></tr>');
			 }
			 	$('#spare_body').append('<tr><td colspan="5" align="right"><input type="hidden" width="20" name="ticket_id" id="ticket_id" value="'+ticketid+'"><input type="hidden" width="20" name="spare_json" id="spare_json" value="'+spare_code+'"><a href="javascript:;" class="btn btn-circle green btn-outline button-submit spare_req_submit" style="" id="raise_buttons"> Submit<i class="fa fa-check"></i></a></td></tr>');
			 $('#modal_display_edit').append('</tbody></table>');
$('#modal_head').html('Update Spare Quantity');
			$('#myModal').modal('show');
		 }	
				  
			 ////////////raju end	  
				  
		 function download(spare_code,quantity,spare_source)
		 {
			 var spare_codes = spare_code.split(',');
			 var spare_quantity = quantity.split(',');
			 var spare_sources = spare_source.split(',');
			 textToWrite='';
			 $('#modal_display').html('<table id="download_spare" class="table table-striped"><thead><th>Spare Code</th><th>Spare Quantity</th><th>Spare Location</th></thead><tbody id="tbody_val">');
			 for(i=0;i<spare_codes.length;i++)
			 {
				 console.log(spare_codes[i]);
				$('#tbody_val').append('<tr><td>'+spare_codes[i]+'</td><td>'+quantity[i]+'</td><td>'+spare_sources[i]+'</td></tr>'); 
				
			 }	
$('#modal_display').append('</tbody></table>'); 			 
$("#download_spare").table2excel({
    // exclude CSS class
    exclude: ".noExl",
    name: "Spare Details",
    filename: "Spare_Request" //do not include extension
  });

   
		 }
		/*  function region()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region=$('#region').val();	 
			var area=$('#area').val();	 
			$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'14'},
         dataType: "json",
         success: function(data) {
         $('#area').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
         });
		 } */
		 function area()
		 {
			 var company_id="<?php echo $company_id;?>";
			
				    var region="<?php echo $region;?>";
				    var area="<?php echo $area;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'14'},
         dataType: "json",
         success: function(data) {
         $('#location').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
         });
		 }
		 function modal(ticket_id,date)
         {
			 var datetime=date.split(",");
			 var date= datetime[0].split("-");console.log(date);
			 var time=datetime[1];
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 control-label"for="inputEmail3" readonly>Ticket ID</label><div class=" col-lg-offset-3 col-sm-4">'+ticket_id+'</div></div><div class="form-group"><label  class="col-sm-3 control-label"for="inputEmail3">Preferred date Of Visit</label><div class=" col-lg-offset-3 col-sm-4"><input class="form-control " type="text" name="serv_date" parsley-trigger="change" required id="serv_date" style="margin-bottom:3%;" value="'+datetime[0]+'"></div></div><div class="form-group"><label  class="col-sm-3 control-label"for="inputEmail3">Preferred time Of Visit</label><div class=" col-lg-offset-3 col-sm-4"><input class="form-control " type="text" name="serv_time" parsley-trigger="change" required id="serv_time" style="margin-bottom:3%;" value="'+datetime[1]+'" ></div></div>');
         
			 $('#serv_date').datepicker({ dateFormat: 'yy-mm-dd' });
			 $("#serv_date" ).datepicker({defaultDate: new Date(date[0],date[1],date[2])});
	$('#serv_time').wickedpicker({
   //now: new Date(),
  // 12- or 24-hour format
  twentyFour: true,
upArrow: 'wickedpicker__controls__control-up',
now:time

});
$(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal" onclick=edit_modal("'+ticket_id+'");>OK</button>');
		 $('#myModal').modal('show');
         }
		 function del(ticket_id)
		 {
			 var company_id="<?php echo $company_id;?>";	  
           swal({
						  title: "Are you sure?",
						  text: "You will not be able to recover!",
						  type: "warning",
						  showCancelButton: true,
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Yes, Delete it!",
						  closeOnConfirm: false
						},
						function(){
			   $.ajax({
         			url: "<?php echo base_url();?>" + "index.php?/controller_service/delete_tick",
         			type: 'POST',
         			data: {'company_id':company_id,'ticket_id':ticket_id
         			},
         			success: function(data) {
         				
swal({
                                  title: data,                                  
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
                                });
					}
			   });
		  
						});
		 }
		 function edit_modal(ticket_id)
		 {	
		 var date=$('#serv_date').val();
		 var time=$('#serv_time').val();
		 var company_id="<?php echo $company_id;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/update_tkt",
         type: 'POST',
         data: {'company_id':company_id,'ticket_id':ticket_id,'date':date,'time':time},
         success: function(data) {
			
swal({
                                  title: data,                                  
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
                                });
         }
         });
		 }
      </script>
	  <!--For Spare form download-->
		<script>
			$('#download_spareform').click(function(){
				var data1 = [{a:1,b:10},{a:2,b:20}];
				var data2 = [{a:100,b:10},{a:200,b:20}];
				var opts = [{sheetid:'One',header:true},{sheetid:'Two',header:false}];				
				var company_id="<?php echo $this->session->userdata('companyid')?>";
				$.ajax({
					url			:	 "<?php echo base_url();?>" + "index.php?/controller_service/download_spareform",
					type		: 	 'POST',
					data		: 	 {'company_id':company_id},
					dataType	:	 "JSON",
					success		: 	function(data) {
									//var parsed = JSON.parse(data);
									//console.log(data);				
									saveAsXlsx(name,data);
									//var res = alasql('SELECT * INTO XLSX("spareform.xlsx",?) FROM ?',[parsed]);
								}
				});
			});
			function saveAsXlsx(name,test){	
				//alasql('SELECT * INTO XLSX("Storemaster.XLSX",{headers:false}) FROM ?',[test]);
				alasql('SELECT ticket_id as Ticket_id, employee_id as Technician_id, spare_code as Spare_code, quantity as Quantity INTO XLSX("Spare_master.xlsx",{headers:true}) FROM ?',[test]);

			}
		</script>
	  <!--For Spare form download-->	
<script>
				$('#imprest_download_spareform').click(function(){
				var data1 = [{a:1,b:10},{a:2,b:20}];
				var data2 = [{a:100,b:10},{a:200,b:20}];
				var opts = [{sheetid:'One',header:true},{sheetid:'Two',header:false}];				
				var company_id="<?php echo $this->session->userdata('companyid')?>";
				$.ajax({
					url			:	 "<?php echo base_url();?>" + "index.php?/controller_service/imprest_download_spareform",
					type		: 	 'POST',
					data		: 	 {'company_id':company_id},
					dataType	:	 "JSON",
					success		: 	function(data) {
									//var parsed = JSON.parse(data);
									//console.log(parsed);				
									saveAsXlsx1(company_id,data);
									//var res = alasql('SELECT * INTO XLSX("imprest_spareform.xlsx",?) FROM ?',[parsed]);
								}
				});
			});
			function saveAsXlsx1(name,test){	
				//alasql('SELECT * INTO XLSX("Storemaster.XLSX",{headers:false}) FROM ?',[test]);
				alasql('SELECT spare_array,employee_id INTO XLSX("Imprest_Storemaster.xlsx",{headers:true}) FROM ?',[test]);

			}

$('#bulkupload1').click(function(){
				var ext = $('#add_excel1').val().toString().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['xls', 'xlsx', 'csv']) == -1) {
                    swal('Please upload Excel file');
                    
                }else {
                    var file_data = $('#add_excel1').prop('files')[0];   
                    var form_data = new FormData();
                    form_data.append('add_excel', file_data);
                    $.ajax({                
                        type:'POST',
                        url:'<?php echo base_url(); ?>index.php?/controller_service/bulk_impreset',
                        contentType:false,
                        processData: false,
                        cache:false,
                        data:form_data,         
                        success: function (data) {
                          var data=JSON.parse(data);
								$('#sla_detail_confirm').html('');
								for(i=0;i<data.length;i++){
									$('#sla_detail_confirm').append(data[i]+"</br><hr>"); 
								}
								
								$('#sla_confirm').modal('show');
									//window.location.href="<?php echo base_url();?>"+"index.php?/controller_service/spare_form#accepted";
                        },
                    });         
                }
            })
	
$( document ).on( "click", ".spare_req_submit", function(e) {
	
	//alert("hello");
	
var spare_qty_array=[];
		var ticketid =  $('#ticket_id').val();
		var spare_json =  $('#spare_json').val();
	var spare_res = spare_json.split(",");
	//alert(ticketid);
	//alert(spare_res.length);
	for(i=0; i<spare_res.length; i++)
	{
		var spare_stk_qty =  $('#stkqty_'+spare_res[i]).val();
			if(spare_stk_qty=='')
			{
				alert('Please Enter the Stock Quantity');
				return false;
			}
	}
	
	for(i=0; i<spare_res.length; i++)
	{
		var spare_stk_qty =  $('#stkqty_'+spare_res[i]).val();
		//alert(spare_stk_qty);
		var spare = {spare_code:spare_res[i], qty:spare_stk_qty};

spare_qty_array.push(spare);			
				}
//console.log(spare_qty_array);
        $.ajax({
                type: "POST",
                data: {'spare_qty_array':spare_qty_array,'ticketid':ticketid},
                url: "<?php echo base_url(); ?>index.php?/controller_admin/update_spare_qty",
                dataType: 'json',
                success: function(response) {
                 if(response == 1){
                  	alert('Updated successfully');
					 $('#myModal').modal('hide');
				window.location.reload();	 
				 }
					else
					{
						alert('Not Updated!');
					}
                 
                }
        }); 
    });	

		
	
/*
function hover_spare(ticket_id,spare_array)
	{
//$('#modal_head').html('Spare Details');
// $('#modal_display').empty(); 
            var data = spare_array;
           // console.log(data);
			 
			  if(data == "null"){
                                $.dialogbox({
      							type:'msg',
      							content:'No Data',
      							closeBtn:true,
      							btn:['Ok.'],
      							call:[
      								function(){
      									$.dialogbox.close();
      								}
      							]
      
      						});  
                        }
                else{
                var data = JSON.parse(spare_array);
					 $('#modal_display').html('');
 $('#modal_display').html('<table class="table table-hover" id=""><thead><tr><th>Spare Code</th><th>Spare Quantity</th><th>Spare Location</th></tr></thead><tbody id="spare_body">');
				for(i=0; i<data.length; i++)
				{
				$('#spare_body').append('<tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr>');
				} 
					$('#modal_display').append('</tbody></table>');	 
$('#modal_head').html('Imprest Spare Details');                            //console.log(data);
                                                      $('#myModal').modal('show');
                                                        }
       
                          
                                }
	*/
	 function hover_spare(tech_id,impspare_table_id,spare_array)
		 {
			//console.log(spare_array);
			    var data = JSON.parse(spare_array);
			 var impspare_qty_array=[];
			//console.log(data);
			 $('#modal_display_edit').html('');
			 $('#modal_display_edit').html('<table class="table table-hover table-bordered" id=""><thead><tr><th class="text-center">Spare Code</th><th class="text-center">Request Quantity</th><th class="text-center">Spare Location</th><th class="text-center">Stock Qty</th></tr></thead><tbody id="spare_body" align="center">');
			 for(i=0;i<data.length;i++)
			 {
				 
			$('#spare_body').append('<tr><td>' + data[i].Spare_code + '</td><td>' + data[i].quantity+ '</td><td>' + data[i].spare_source + '</td><td><input type="text" width="20" name="impstkqty" id="impstkqty_'+data[i].Spare_code+'"></td><td><input type="hidden" width="20" name="imptechstkqty" id="imptechstkqty_'+data[i].Spare_code+'" value="'+data[i].quantity+'"></td></tr>');
			//var impspare = {spare_code:data[i].Spare_code};

impspare_qty_array.push(data[i].Spare_code);				 
			 }
			
//	console.log(impspare_qty_array);		 
			 	$('#spare_body').append('<tr><td colspan="4" align="right"><input type="hidden" width="20" name="impspare_table_id" id="impspare_table_id" value="'+impspare_table_id+'"><input type="hidden" width="20" name="tech_id" id="tech_id" value="'+tech_id+'"><input type="hidden" width="20" name="spare_json" id="spare_json" value="'+impspare_qty_array+'"><a href="javascript:;" class="btn btn-circle green btn-outline button-submit imp_spare_req_submit" style="" id="raise_buttons"> Submit<i class="fa fa-check"></i></a></td></tr>');
			 $('#modal_display_edit').append('</tbody></table>');
$('#modal_head').html('Update Imprest Spare Quantity');
			$('#myModal').modal('show');
		 }	

$( document ).on( "click", ".imp_spare_req_submit", function(e) {
	
	//alert("hello");
	
var spare_qty_array=[];
		var tech_id =  $('#tech_id').val();
	var impspare_table_id =  $('#impspare_table_id').val();
		var spare_json =  $('#spare_json').val();
	var spare_res = spare_json.split(",");
//	alert(impspare_table_id);
//	alert(spare_res.length);
	for(i=0; i<spare_res.length; i++)
	{
		var spare_stk_qty =  $('#impstkqty_'+spare_res[i]).val();
			if(spare_stk_qty=='')
			{
				alert('Please Enter the Stock Quantity');
				return false;
			}
	}
	for(i=0; i<spare_res.length; i++)
	{
		var service_spare_stk_qty =  $('#impstkqty_'+spare_res[i]).val();
		var tech_spare_stk_qty =  $('#imptechstkqty_'+spare_res[i]).val();
		//alert(spare_stk_qty);
		var spare = {spare_code:spare_res[i], tech_qty:tech_spare_stk_qty, service_qty:service_spare_stk_qty};

spare_qty_array.push(spare);			
				}
//console.log(spare_qty_array);
        $.ajax({
                type: "POST",
                data: {'spare_qty_array':spare_qty_array,'impspare_table_id':impspare_table_id,'tech_id':tech_id},
                url: "<?php echo base_url(); ?>index.php?/controller_admin/update_imp_spare_qty",
                dataType: 'json',
                success: function(response) {
                 if(response == 1){
                        //alert(response);
				alert('Updated successfully');
					 $('#myModal').modal('hide');
				window.location.reload();	 
				 }
					else
					{
							alert('Not Updated!');
					}
                 
                }
        }); 
    });	

	
	
		</script>  
         </body>
      </html>