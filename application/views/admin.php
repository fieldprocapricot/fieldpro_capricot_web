<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head><meta http-equiv="Content-Type" content="text/html; charset=WINDOWS-1252">
            
            <?php 
include 'assets/lib/cssscript.php'?>
         </head>
         <!-- END HEAD -->
         <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header.php"?>
               <!-- END HEADER -->
                <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/admin_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                    
                     <!-- BEGIN PAGE BASE CONTENT -->
                     <div class="row widget-row">
                        <div class="col-md-3">
                           <!-- BEGIN WIDGET THUMB -->
                           <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                              <h4 class="widget-thumb-heading">Technician</h4>
                              <div class="widget-thumb-wrap">
                                 <i class="widget-thumb-icon bg-green icon-settings"></i>
                                 <div class="widget-thumb-body1">
                                    <span class="widget-thumb-subtitle1">Subscribed <span class="widget-thumb-body-stat1" data-counter="counterup" data-value="<?php
                                       foreach ($reg_tech->result() as $row) {
                                       	?>
                                       <?php echo $row->technicians; ?>
                                       <?php }?>" style="display:inline;color:#333 !important;font-weight: 400;"><?php
                                       foreach ($reg_tech->result() as $row) {
                                       	?>
                                    <?php echo $row->technicians; ?>
                                    <?php }?></span></span>
                                    <hr style="margin:4px 0px !important">
                                    <span class="widget-thumb-subtitle1">Registered <span class="widget-thumb-body-stat1" data-counter="counterup" data-value="<?php
                                       echo $tech;
                                       ?>" style="display:inline;color:#333 !important;font-weight: 400;"><?php
                                       echo $tech;
                                       ?></span></span>                                     
                                 </div>
                              </div>
                           </div>
                           <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                           <!-- BEGIN WIDGET THUMB -->
                           <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                              <h4 class="widget-thumb-heading">Service Desk</h4>
                              <div class="widget-thumb-wrap">
                                 <i class="widget-thumb-icon bg-red icon-paper-clip"></i>
                                 <div class="widget-thumb-body1">
                                    <span class="widget-thumb-subtitle1">Subscribed <span class="widget-thumb-body-stat1" data-counter="counterup" data-value="<?php
                                       foreach ($reg_tech->result() as $row) {
                                       	?>
                                       <?php echo $row->service_desk; ?>
                                       <?php } ?>" style="display:inline;color:#333 !important;font-weight: 400;"><?php
                                       foreach ($reg_tech->result() as $row) {
                                       	?>
                                    <?php echo $row->service_desk; ?>
                                    <?php } ?></span></span>
                                    <hr style="margin:4px 0px !important">
                                    <span class="widget-thumb-subtitle1">Registered <span class="widget-thumb-body-stat1" data-counter="counterup" data-value="<?php
                                       echo $service;
                                       ?>" style="display:inline;color:#333 !important;font-weight: 400;"><?php
                                       echo $service;
                                       ?></span></span> 
                                 </div>
                              </div>
                           </div>
                           <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                           <!-- BEGIN WIDGET THUMB -->
                           <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                              <h4 class="widget-thumb-heading">Call Coordinator</h4>
                              <div class="widget-thumb-wrap">
                                 <i class="widget-thumb-icon bg-blue icon-call-in" ></i>
                                 <div class="widget-thumb-body1">
                                    <span class="widget-thumb-subtitle1">Registered</span>
                                    <hr style="margin:4px 0px !important">
                                    <span class="widget-thumb-body-stat1" data-counter="counterup" data-value="<?php
                                       echo $call;
                                       ?>"><?php
                                       echo $call;
                                       ?></span>
                                 </div>
                              </div>
                           </div>
                           <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                           <!-- BEGIN WIDGET THUMB -->
                           <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                              <h4 class="widget-thumb-heading">Manager</h4>
                              <div class="widget-thumb-wrap">
                                 <i class="widget-thumb-icon bg-purple icon-user"></i> 
                                 <div class="widget-thumb-body1">
                                    <span class="widget-thumb-subtitle1">Registered</span>
                                    <hr style="margin:4px 0px !important">
                                    <span class="widget-thumb-body-stat1" data-counter="counterup" data-value="<?php
                                       echo $manager;
                                       ?>"><?php
                                       echo $manager;
                                       ?></span>
                                 </div>
                              </div>
                           </div>
                           <!-- END WIDGET THUMB -->
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6 col-sm-6">
                           <div class="portlet light bordered">
                              <div class="portlet-title">
                                 <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark">Subscription Tracker</span>
                                 </div>
                              </div>
                              <div class="portlet-body">
                                 <div class="widget-thumb-wrap">
                                    <div class="widget-thumb-body1">
                                       <div class="row">
                                          <span class="widget-thumb-subtitle1 col-lg-6 col-xs-6 text-right" style="display:inline;color:#000">Date Of Subscription </span>
                                          <span class="widget-thumb-subtitle1 col-lg-5 col-xs-6" style="display:inline;color:#000 !important"><?php
                                             foreach ($get_details->result() as $row) {
                                             	?>
                                          <?php echo $row->start_date; ?>
                                          <?php } ?></span>
                                       </div>
                                    </div>
                                    <div class="widget-thumb-body1">
                                       <div class="row">
                                          <span class="widget-thumb-subtitle1 col-lg-6 col-xs-6 text-right" style="display:inline;color:#000">Next Renewal Date </span>
                                          <span class="widget-thumb-subtitle1 col-lg-5 col-xs-6" style="display:inline;color:#000 !important"><?php
                                             foreach ($get_details->result() as $row) { 
                                             	?>
                                          <?php echo $row->renewal_date; ?>
                                          <?php } ?></span>
                                       </div>
                                    </div>
                                   <!-- added by ramya-->
<div class="widget-thumb-body1">
<div class="row">
<span class="widget-thumb-subtitle1 col-lg-6 col-xs-6 text-right" style="display:inline;color:#000">Remaining days of Subscription </span>
<span class="widget-thumb-subtitle1 col-lg-5 col-xs-6" style="display:inline;color:#000 !important">
<span id="remaining_days"></span> 
</span>
</div>
</div>

                                 </div>
                                 <hr>
                              </div>
                              <div class="portlet-body">
                                 <div class="widget-thumb-body1">
                                    <div class="row">
                                            <!--<div class="col-lg-10 col-lg-offset-1">
                                          	<div class="col-lg-6">                                   
                                             		<button class="btn btn-circle blue btn-block btn-outline text-center" id="renew_subscription">Renew Subscription</button>
                                          	</div>
                                          	<div class="col-lg-6 form-group">                                    
                                             		<button class="btn btn-circle red btn-outline btn-block text-center" id="cancel_subscription">Cancel Subscription</button>
                                          	</div>
                                       	</div>-->
                                       	<div class="col-lg-4">									   		
                                             	<button class="btn btn-circle green-sharp btn-outline btn-block text-center" id="add_subscription">Add License</button>
                                      	 </div>
                                       	<div class="col-lg-4">									   		
                                             	<button class="btn btn-circle blue btn-block btn-outline text-center" id="renew_subscription">Renew Subscription</button>
                                       	</div>
                                       	<div class="col-lg-4 form-group">									   		
                                             	<button class="btn btn-circle red btn-outline btn-block text-center" id="cancel_subscription">Cancel Subscription</button>
                                       	</div> 
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                           <div class="portlet light bordered">
                              <div class="portlet-title">
                                 <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark">Timeline Tracker</span>
                                 </div>
                              </div>
                              <div class="portlet-body">
                                 <div class="widget-thumb-wrap">
                                    <div class="widget-thumb-body1">
                                       <div class="row">
                                          <span class="widget-thumb-subtitle1 col-lg-6 col-xs-6 text-right" style="display:inline;color:#000">Date of Spare Updation </span>
                                          <span class="widget-thumb-subtitle1 col-lg-5 col-xs-6" style="display:inline;color:#000 !important"><?php
                                             $variables=$last_spare[0];	
                                             print_r($variables['spares']);
                                             ?></span>
                                       </div>
                                    </div>
                                    <div class="widget-thumb-body1">
                                       <div class="row">
                                          <span class="widget-thumb-subtitle1 col-lg-6 col-xs-6 text-right" style="display:inline;color:#000">Date of Product Master Update </span>
                                          <span class="widget-thumb-subtitle1 col-lg-5 col-xs-6" style="display:inline;color:#000 !important"><?php
                                             $variables=$last_spare[1];	
                                             print_r($variables['product_managements']);
                                             ?></span>
                                       </div>
                                    </div>
                                    <div class="widget-thumb-body1">
                                       <div class="row">
                                          <span class="widget-thumb-subtitle1 col-lg-6 col-xs-6 text-right" style="display:inline;color:#000">Date of Contract Update</span>
                                          <span class="widget-thumb-subtitle1 col-lg-5 col-xs-6" style="display:inline;color:#000 !important"><?php
                                             $variables=$last_spare[2];	
                                             print_r($variables['all_ticketss']);
                                             ?></span>
                                       </div>
                                    </div>
                                 </div>
                                 <hr style="margin-bottom:15% !important">
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- END PAGE BASE CONTENT -->
                  </div>
                  <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
               </div>
			
            <!-- END CONTAINER -->
            
            <!--Model Dialog for Admin-->
            <div class="modal fade in" id="myModal">
               <div class="modal-dialog">
                  <form id="cust_data" class="form-horizontal" role="form">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">?</span><span class="sr-only">Close</span></button>
                           <h4 class="modal-title">Add License</h4>
                           <div class="error" style="display:none">
                              <label id="rowdata"></label>
                           </div>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="form-group col-sm-12" style="display:none">
                                 <label class="control-label col-sm-4" for="email">ID<span class="errors">
                                 </span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control" id="id1" name="id1" placeholder="" type="text" value="" readonly>
                                 </div>
                              </div>
                              <div class="form-group col-sm-12" style="display:none">
                                 <label class="control-label col-sm-4" for="email">Company Name<span class="errors">
                                 </span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control" id="company_name" name="company_name" placeholder="" type="text" value="" readonly>
                                 </div>
                              </div>
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-4" for="email">Service Desk Subscribed <span class="errors">
                                 </span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control" id="service_reg" name="service_reg" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-4" for="email">Service Desk Required<span class="errors">
                                 </span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control digit_only" id="service_need" name="service_need" placeholder="" type="text" onKeyPress="return isNumberKey(event)" value="0">
                                 </div>
                              </div>
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-4" for="email">Technician Subscribed <span class="errors">
                                 </span>
                                 </label> 
                                 <div class="col-sm-8">
                                    <input class="form-control" id="tech_reg" name="tech_reg" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-4" for="email">Technician Required <span class="errors">
                                 </span>
                                 </label>
                                 <div class="col-sm-8">
                                    <input class="form-control digit_only" id="tech_need" name="tech_need" placeholder="" type="text" onKeyPress="return isNumberKey(event)" value="0">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-circle green btn-outline" id="add_lic" name="add_lic"><i class="fa fa-check"></i> Add License</button>
                           <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="modal fade in" id="renew_licence">
               <div class="modal-dialog" >
                  <form id="renew_licences" class="form-group col-lg-10 col-lg-offset-1" role="form">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">?</span><span class="sr-only">Close</span></button>
                           <h4 class="modal-title">Renew Subscription</h4>
                        </div>
                        <div class="modal-body">
                           <div class="form-group" style="display:none">
                              <label for="users">Id</label>
                              <input type="text" class="form-control" name="company_id" id="company_id" readonly="">
                           </div>
                           <div class="form-group">
                              <label for="users">Company Name</label>
                              <input type="text" class="form-control" name="cmodal_name" id="cmodal_name" readonly="">
                           </div>
                           <div class="form-group">
                              <label for="users">Number of Service Desk</label>
                              <input type="text" class="form-control" name="cmodal_serv" id="cmodal_serv" readonly="">
                           </div>
                           <div class="form-group">
                              <label for="users">Number of Technicians</label>
                              <input type="text" class="form-control" name="cmodal_tech" id="cmodal_tech" readonly="">
                           </div>
                           <div class="form-group" style="display:none">
                              <label for="users">Expairy Date</label>
                              <input type="text" class="form-control" name="exp_date" id="exp_date" readonly="">
                           </div>
                           <div class="form-group">
                              <div class="form-group col-lg-7">
                                 <label for="users" style="margin-left:-11px">Contract Period</label>
                                 <select class="form-control" name="time_yr" parsley-trigger="change" required="" id="time_yr" style="margin-left: -5% !important;">
                                    <option selected="" disabled="">Select Subscription Plans</option>
                                    <option value="Quarterly Subscription">Quarterly Subscription</option>
                                    <option value="Half Yearly Subscription">Half Yearly Subscription</option>
                                    <option value="Annual Subscription">Annual Subscription</option>
                                 </select>
                              </div>
                              <div class="form-group col-lg-5">
                                 <label for="users">Next Renewal Date</label>
                                 <input type="text" class="form-control" name="cust_id_renew" id="cust_id_renew" readonly>
                              </div>
                           </div>


                              <div class="form-group pull-right">
                                 <input type="button" data-dismiss="modal" class="btn btn-circle green btn-outline" value="Submit" onClick="custdata();">	
                                 <button type="reset" class="btn btn-circle red btn-outline" id="cancel_butn">Cancel
                                 </button>
                              </div>
							  <span class="clearfix"></span>
							  </div>
                  
                        
                     </div>
                  </form>
               </div>
            </div>
            <div class="modal fade in" id="show_details">
               <div class="modal-dialog">
                  <form id="confirm_licences" class="form-horizontal" role="form">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close change_lic"><span aria-hidden="true">?</span><span class="sr-only">Close</span></button>
                           <h4 class="modal-title">Confirm License</h4>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="form-group col-sm-12" style="display:none">
                                 <label class="control-label col-sm-6" for="email">Id<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="admin_id" name="admin_id" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-6" for="email">Total Service Desk<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="total_service" name="total_service" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form-group col-sm-12">
                                 <label class="control-label col-sm-6" for="email">Total Technician Desk<span class="errors">
                                 *</span>
                                 </label>
                                 <div class="col-sm-6">
                                    <input class="form-control" id="total_techni" name="total_techni" placeholder="" type="text" readonly>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-circle green btn-outline" id="add_licence" name="add_licence"><i class="fa fa-check"></i> Confirm</button>
                           <button type="button" class="btn btn-circle red btn-outline change_lic" id="change_lic" name="change_lic" ><i class="fa fa-times"></i> Change</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="modal fade in" id="cancels_licence">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">�</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Cancel Subscription</h4>
                        <div class="error" style="display:none">
                            <label id="rowdata_2"></label>
                        </div>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id="cancel_licences">
                           <div class="form-group" style="display:none">
                              <label class="col-sm-2 control-label"
                                 for="cust_id_cancel" >id</label>
                              <div class="col-sm-10">
                                 <input type="text" class="form-control"
                                    name="cust_id_cancel" id="cust_id_cancel" />
                              </div>
                           </div>
                           <div class="form-group">
                              <label  class="col-sm-5 control-label"
                                 for="inputEmail3">Do you want to cancel this subscription?</label>
                              <div class="col-sm-7">
                                 <label class="radio-inline"><input type="radio" name="optional" value="completely">Complete</label>
                                 <label class="radio-inline"><input type="radio" name="optional" value="partially">Partially</label>
                              </div>
                           </div>
                           <div class="form-group partiallys">
                              <label class="col-sm-5 control-label"
                                 for="service_regs" >Service Desk Subscribed <span class="errors">
                              </span></label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" placeholder="" id="service_regs" name="service_regs" readonly/>
                              </div>
                           </div>
                           <div class="form-group partiallys">
                              <label class="col-sm-5 control-label"
                                 for="service_needs" >Service Desk Required <span class="errors">
                              </span></label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" placeholder="" id="service_needs" name="service_needs" onKeyPress="return isNumberKey(event)" />
                              </div>
                           </div>
                           <div class="form-group" style='display:none'>
                              <label class="col-sm-5 control-label"
                                 for="service_needs" >Service Desk Registered <span class="errors">
                              </span></label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" placeholder="" id="service_registere" name="service_registere" onKeyPress="return isNumberKey(event)" />
                              </div>
                           </div>
                           <div class="form-group partiallys">
                              <label class="col-sm-5 control-label"
                                 for="tech_regs" >Technician Subscribed <span class="errors">
                              </span></label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" placeholder="" id="tech_regs" name="tech_regs" readonly />
                              </div>
                           </div>
                           <div class="form-group partiallys">
                              <label class="col-sm-5 control-label"
                                 for="tech_needs" >Technician Required <span class="errors">
                              </span></label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" placeholder="" id="tech_needs" name="tech_needs" onKeyPress="return isNumberKey(event)" />
                              </div>
                           </div>
                            <div class="form-group" style='display:none'>
                              <label class="col-sm-5 control-label"
                                 for="tech_needs" >Technician Registered <span class="errors">
                              </span></label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" placeholder="" id="tech_registere" name="tech_registere" onKeyPress="return isNumberKey(event)" />
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle green btn-outline" onClick="change_block();"><i class="fa fa-check"></i> Confirm</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                     </div>
                  </div>
               </div>
            </div>
            </div>
            <!--End of Model Dialog-->
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>  
            <script>        	
               $(window).load(function(){				
               	//$('.nav.navbar-nav').find('.open').removeClass( 'open' );
               	$('#admin_dashboard').addClass('open');
                $('.partiallys').hide();
                $("#remaining_days").html("");
var sessName = '<?php echo $_SESSION['companyid']?>';
$.ajax({
url : "<?php echo base_url(); ?>index.php?/controller_admin/remaining_days",
type : "POST",
data : {'company_id' : sessName},// {action:'$funky'}
datatype : "JSON", 
cache : false,
success : function(data){
console.log(data);
$("#remaining_days").append(data);
$.ajax({
url : "<?php echo base_url(); ?>index.php?/controller_cronfunctions/remainder_mail",
type : "POST",
data : {'company_id' : sessName},// {action:'$funky'}
datatype : "JSON", 
cache : false,
success : function(data){
console.log(data); 
}
}); 
}
});


                
               });
            </script> 
            <script>
                $('input[name=optional]').on('click', function(){
                     var optional=$('input[name=optional]:checked').val(); 
                     if(optional=='partially'){
                           $('.partiallys').show();
                     }else{
                           $('.partiallys').hide();
                    }
               });
                $('#add_subscription').click(function(){
                    var sessName = '<?php echo $_SESSION['companyid']?>';
                    $('#rowdata_1').empty();
                    $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_licence",
                        type        :   "POST",
                        data        :   {'company_id' : sessName},// {action:'$funky'}
                        datatype    :  "JSON",  
                        cache       :   false,
                        success     :  function(data){
                                   //data=$.trim(data);
                                    var data=JSON.parse(data);
                                    //console.log(data);
                                    $('#id1').val(data['admin_id']);
                                    $('#company_name').val(data['company_id']);
                                    $('#service_reg').val(data['service_desk']);
                                    $('#tech_reg').val(data['technicians']);									
                                    $('#company_id').val(data['admin_id']);
                                    $('#cmodal_name').val(data['company_id']);
                                    $('#cmodal_serv').val(data['service_desk']);
                                    $('#cmodal_tech').val(data['technicians']);
                                },
                    });
                    $('#myModal').modal('show');
                 });
                  $('#renew_subscription').click(function(){
                     var sessName = '<?php echo $_SESSION['companyid']?>';
                     $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_licence",
                        type        :   "POST",
                        data        :   {'company_id' : sessName},// {action:'$funky'}
                        datatype    :  "JSON",  
                        cache       :   false,
                        success     :  function(data){
                                          //data=$.trim(data);
                                          var data=JSON.parse(data);
                                          //console.log(data);									
                                          $('#company_id').val(data['admin_id']);
                                          $('#cmodal_name').val(data['company_name']);
                                          $('#cmodal_serv').val(data['service_desk']);
                                          $('#cmodal_tech').val(data['technicians']);
                                          $('#exp_date').val(data['renewal_date']);
                                          $('#cust_id_renew').val(data['renewal_date']);
                                          $('#time_yr').val(data['subs_plan']);
                                          $('#renew_licence').modal('show');
                                       },
                     });               
                  });
                 $('#time_yr').change(function(){
                    var yr=$( "#time_yr :selected" ).text();
                    var exp_date=$( "#exp_date" ).val();
                    $.ajax({
                        url: "<?php echo base_url();?>"+"index.php?/controller_admin/renewal_date",
                        method: "POST",
                        data: {'yr': yr,'exp_date':exp_date},
                        success:function(data){
                            //console.log(data);
                            $('#cust_id_renew').val(data);
                                }
                    });			  
                  });
               function custdata(){
                var data = new FormData($('#renew_licences')[0]);
                $.ajax({	           
                type:"POST",
                url:"<?php echo base_url();?>" + "index.php?/controller_admin/renew_info",
                data: $('#renew_licences').serialize(),
                success:function(data){
					//console.log(data);
                if(data=="inserted"){							
                $('#renew_licence').modal('hide');
                swal({
                title: "Renewed Successfully!",                        		  
                type: "success",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm) {
                if (isConfirm) {
                window.location.reload();
                }
                });
                /* $.dialogbox({
                type:'msg',
                content:'Renewed Successfully!',
                closeBtn:true,
                btn:['Ok.'],
                call:[
                function(){
                $.dialogbox.close();
                $('#renew_licence').modal('hide');
                window.location.reload();
                }
                ]							
                }); */
                }
                else{														
                $('#renew_licence').modal('hide');
                swal({
                title: data,                        		  
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm) {
                if (isConfirm) {
                swal.close();								
                $('#renew_licence').modal('show');
                }
                });							
                /* $.dialogbox({
                type:'msg',
                content:data,
                closeBtn:true,
                btn:['Ok.'],
                call:[
                function(){
                $.dialogbox.close();
                }
                ]							
                }); */
                }
                }

                });
               }
               $('#cancel_butn').click(function(){
               $('#renew_licence').modal('hide');
               });
               $('#cancel_subscription').click(function(){
                  var sessName = '<?php echo $_SESSION['companyid']?>';
                  $.ajax({
                     url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_licence",
                     type        :   "POST",
                     data        :   {'company_id' : sessName},// {action:'$funky'}
                     datatype    :  "JSON",  
                     cache       :   false,
                     success     :  function(data){
                                       //data=$.trim(data);
                                       var data=JSON.parse(data);
                                       //console.log(data);									
                                       $('#cust_id_cancel').val(data['admin_id']);
                                       $('#service_regs').val(data['service_desk']);
                                       $('#tech_regs').val(data['technicians']);
                                       $('#service_registere').val(data['service_added']);
                                       $('#tech_registere').val(data['tech_added']);
                                       $('#cancels_licence').modal('show');
                                    },
                  });
               });			
               function change_block(){
                     $('#rowdata_2').empty();
                     var optional=$('input[name=optional]:checked').val();
                     var service_regs=$('#service_regs').val();
                     var service_registere=$('#service_registere').val();
                     var service_needs=$('#service_needs').val();
                     var tech_regs=$('#tech_regs').val();
                     var tech_registere=$('#tech_registere').val();
                     var tech_needs=$('#tech_needs').val();

                    /* service_regs=parseInt(service_regs);
                     service_registere=parseInt(service_registere);
                     service_needs=parseInt(service_needs);
                     tech_regs=parseInt(tech_regs);
                     tech_registere=parseInt(tech_registere);
                     tech_needs=parseInt(tech_needs);  */ 

                     if($('input[name=optional]:checked').val()){
                        if(optional == "partially"){                       
                           if(service_needs == "" && tech_needs == ""){
                                /*$('#rowdata_2').append('All Fields are mandatory');
                                $('.error').show();*/
                                $('#cancels_licence').modal('hide');
                                 swal({
                                    title: "No changes happened",                                
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Ok!",
                                    cancelButtonText: "No, cancel plx!",
                                    closeOnConfirm: false,
                                    closeOnCancel: false
                                 },
                                 function(isConfirm) {
                                    if (isConfirm) {
                                       swal.close();
                                    }
                                 });
                           }else{    

                                 if(service_needs == ""){
                                    service_needs = service_regs;
                                 }
                                 if(tech_needs == ""){
                                    tech_needs = tech_regs;
                                 }

                                 service_regs=parseInt(service_regs);
                                 service_registere=parseInt(service_registere);
                                 service_needs=parseInt(service_needs);
                                 tech_regs=parseInt(tech_regs);
                                 tech_registere=parseInt(tech_registere);
                                 tech_needs=parseInt(tech_needs);

                                 if(service_needs < service_registere || service_needs > service_regs){
                                    $('#rowdata_2').append('Service Desk need should be lesser than Service Desk already Registered and also already subscribed');
                                    $('.error').show();
                                 }else{
                                    if(tech_needs < tech_registere || tech_needs > tech_regs){
                                       $('#rowdata_2').append('Technicians need should be lesser than Technicians already Registered and also already subscribed');
                                       $('.error').show();
                                    }else{
                                       $('.error').hide();                        
                                       var id=$('#cust_id_cancel').val();
                                       $.ajax({
                                          url	     : "<?php echo base_url();?>" + "index.php?/controller_admin/set_unblock",
                                          type	     : 'POST',
                                          data	     : $('#cancel_licences').serialize(),
                                          success	  : function(data) {
                                                         if(data=="updated"){														
                                                            $('#cancels_licence').modal('hide');
                                                            swal({
                                                                title: "Your subscription cancelled successfully!",                        		  
                                                                type: "success",
                                                                showCancelButton: false,
                                                                confirmButtonClass: "btn-danger",
                                                                confirmButtonText: "Ok!",
                                                                cancelButtonText: "No, cancel plx!",
                                                                closeOnConfirm: false,
                                                                closeOnCancel: false
                                                                },
                                                                function(isConfirm) {
                                                                    if (isConfirm) {
                                                                    window.location.href='<?php echo site_url('login/logout');?>';
                                                                }
                                                            });							
                                                            }else{
                                                                $('#rowdata_2').empty();
                                                                $('#rowdata_2').append(data);
                                                                $('.error').show();
                                                            }
                                                         }
                                       }); 
                                    }
                                 }
                            }                            
                        }else{
                            $('.error').hide();                        
                            var id=$('#cust_id_cancel').val();
                            $.ajax({
                                url	: "<?php echo base_url();?>" + "index.php?/controller_admin/set_unblock",
                                type	: 'POST',
                                data	: $('#cancel_licences').serialize(),
                                success	: function(data) {
                                            if(data=="updated"){														
                                            $('#cancels_licence').modal('hide');
                                            swal({
                                                title: "Your subscription cancelled successfully!",                        		  
                                                type: "success",
                                                showCancelButton: false,
                                                confirmButtonClass: "btn-danger",
                                                confirmButtonText: "Ok!",
                                                cancelButtonText: "No, cancel plx!",
                                                closeOnConfirm: false,
                                                closeOnCancel: false
                                                },
                                                function(isConfirm) {
                                                    if (isConfirm) {
                                                    window.location.href='<?php echo site_url('login/logout');?>';
                                                }
                                            });							
                                            }
                                            else{
                                                $('#rowdata_2').empty();
                                                $('#rowdata_2').append(data);
                                                $('.error').show();
                                            }
                                         }
                            }); 
                        }
                    }else{
                        $('#rowdata_2').append('Select any one of the subscription');
                        $('.error').show();
                    }
               }
            </script>
            <script>
               window.history.forward();		
            </script>
            <script>
               function isNumberKey(evt){
                  var charCode = (evt.which) ? evt.which : event.keyCode
                  if (charCode > 31 && (charCode < 48 || charCode > 57))
                     return false;
                  return true;
               }
               $('#add_lic').click(function(){
               	$('#rowdata').empty();
               	var ser_need=$('#service_need').val();
               	var tech_need=$('#tech_need').val();
               	var service_reg=$('#service_reg').val();
               	var tech_reg=$('#tech_reg').val();
                  if(ser_need != "" || tech_need != ""){
                        var total_service = 0;
                        var total_tech = 0;
                        /*var total_service=parseInt(service_reg)+parseInt(ser_need);
                        var total_tech=parseInt(tech_reg)+parseInt(tech_need);*/
                        total_service += +service_reg;
                        total_service += +ser_need;
                        total_tech += +tech_reg;
                        total_tech += +tech_need;
                        $('#total_techni').val(total_tech);
                        $('#total_service').val(total_service);
                        $('#admin_id').val($('#id1').val());
                        $('#myModal').modal('hide');
                        $('#show_details').modal('show');

                     /*if(ser_need.match(/^\d+$/) || tech_need.match(/^\d+$/)){                        
                        
                     }else{
                        $('#rowdata').append("Service desk and Technician needed should be numeric value");
                        $('#myModal').animate({ scrollTop: 0 });
                        $('.error').show();
                     }*/
                  }else{
                     $('#myModal').modal('hide');
                     swal({
                       title: "No Changes happened",                               
                       type: "success",
                       showCancelButton: false,
                       confirmButtonClass: "btn-danger",
                       confirmButtonText: "Ok!",
                       cancelButtonText: "No, cancel plx!",
                       closeOnConfirm: false,
                       closeOnCancel: false
                     },
                     function(isConfirm) {
                       if (isConfirm) {
                           //window.location.reload();
                           swal.close();
                       }
                     });
                  }
               });
               $('.change_lic').click(function(){
               	$('#show_details').modal('hide');
               	$('#myModal').modal('show');
               });
               $('#add_licence').click(function(){
               	$.ajax({
                            url         :   "<?php echo base_url(); ?>index.php?/controller_admin/insert_adminlicence",
                            type        :   "POST",
                            data        :   $('#confirm_licences').serialize(),// {action:'$funky'}
                            datatype    :  "JSON",  
                            cache       :   false,
                            success     :  function(data){
                                           //data=$.trim(data);
               						//var data=JSON.parse(data);
               						//console.log(data);  
               						if(data=='true'){
               							$('#myModal').modal('hide');
               							$('#show_details').modal('hide');
               							swal({
                                             		  title: "License added successfully",                        		  
                                             		  type: "success",
                                             		  showCancelButton: false,
                                             		  confirmButtonClass: "btn-danger",
                                             		  confirmButtonText: "Ok!",
                                             		  cancelButtonText: "No, cancel plx!",
                                             		  closeOnConfirm: false,
                                             		  closeOnCancel: false
                                             		},
                                             		function(isConfirm) {
                                             		  if (isConfirm) {
               									window.location.reload();
                                             		  }
                                             		});
               							/* $.dialogbox({
               								type:'msg',
               								content:'Licence added succesfully',
               								closeBtn:true,
               								btn:['Ok.'],
               								call:[
               									function(){
               										$.dialogbox.close();
               										window.location.reload();
               									}
               								]							
               							}); */ 
               						}else{
               							$('#rowdata').append(data);
               							$('#edits').animate({ scrollTop: 0 });
               							$('.error').show();
               						}
                                        },
                         });				
               })
            </script>  
            <script>
               $('.digit_only').on('input', function (event) { 
               	this.value = this.value.replace(/[^0-9]/g, '');
               });
            </script>
   </body>
</html>