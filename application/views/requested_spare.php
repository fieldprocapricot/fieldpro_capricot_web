<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<?php
$company_id=$this->session->userdata('companyid');
 include 'assets/lib/cssscript.php'?>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo">
<!-- BEGIN CONTAINER -->
<div class="wrapper">
<!-- BEGIN HEADER -->
<?php include "assets/lib/header_service.php"?>
<!-- END HEADER -->
<div class="container-fluid">
<div class="page-content">
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
<h1>FieldPro Service</h1>
<ol class="breadcrumb">
<li>
<a href="<?php echo site_url('controller_service');?>">Home</a>
</li>
<li class="active">New Tickets</li>
</ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="page-content-container">
<div class="page-content-row">
<div class="page-content-col">
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet box dark">
<div class="portlet-title">
<div class="caption">
<i class="fa fa-globe"></i>New Tickets</div>
</div>
<div class="portlet-body">
<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
<select class="btn green btn-outline col-lg-2 col-lg-offset-1 pull-right" id="role1" name="role1">
<option value="" selected disabled>All Product-Category</option>
<option value="Admin">Admin</option>
<option value="Manager">Manager</option>
<option value="Call-coordinator">Call-coordinator</option>
<option value="Service Desk">Service Desk</option>
<option value="Technician">Technician</option>
</select>
</div>
<hr>
<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
<select class="btn green btn-outline col-lg-2 col-lg-offset-1 pull-right" id="role1" name="role1">
<option value="" selected disabled>All Location</option>
<option value="Admin">Admin</option>
<option value="Manager">Manager</option>
<option value="Call-coordinator">Call-coordinator</option>
<option value="Service Desk">Service Desk</option>
<option value="Technician">Technician</option>
</select>
<select class="btn green btn-outline col-lg-2 col-lg-offset-1 pull-right" id="role1" name="role1">
<option value="" selected disabled>All Area</option>
<option value="Admin">Admin</option>
<option value="Manager">Manager</option>
<option value="Call-coordinator">Call-coordinator</option>
<option value="Service Desk">Service Desk</option>
<option value="Technician">Technician</option>
</select>
<select class="btn green btn-outline col-lg-2 col-lg-offset-1 pull-right" id="role1" name="role1">
<option value="" selected disabled>All Region </option>
<option value="Admin">Admin</option>
<option value="Manager">Manager</option>
<option value="Call-coordinator">Call-coordinator</option>
<option value="Service Desk">Service Desk</option>
<option value="Technician">Technician</option>
</select>
</div>
<div class="table=responsive">
<table class="table table-hover" id="sample_2">
<thead>
<tr>
<th>Ticket Id</th>
<th>Technician Id</th>
<th>Product-Category</th>
<th>Sub-Category</th>
<th>Call-Category</th>
<th>Service-Category</th>
<th>Priority</th>
<th>Assigned Time</th>
<th>Action</th>
</tr>
</thead>
<tbody id="tbody_assigned">
</tbody>
</table>
</div>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
</div> 
<!-- END PAGE BASE CONTENT -->
</div> 
</div>
</div>

<!--Modal Starts-->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Bulk Upload</h4>
</div>
<div class="modal-body">
<div class="container">
<input type="file" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" name="add_excel" id="add_excel" value=""/>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" id="bulkupload" name="bulkupload"><i class="fa fa-upload"></i> Upload</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add User</h4>
<div class="error" style="display:none">
<label id="rowdata"></label>
</div>
</div>
<div class="modal-body">
<div class="portlet-title">
<div class="caption font-black">
<span class="caption-subject bold uppercase"> Company Details</span>
</div>
</div>
</div>
<div class="modal-footer">
<button type="submit" class="btn btn-circle blue btn-outline" id="adduser"><i class="fa fa-check"></i> Submit</button>
<button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
</div>
</div>
</div>
</div>
<!-- Modal -->
<div id="edits" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Update User</h4>
<div class="error" style="display:none">
<label id="rowdata"></label>
</div>

</div>
<div class="modal-body">
<form class="form-horizontal" id="edit_user">
<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-2 col-lg-offset-5">
<img id="user_image" src="" style="border: 1px solid; border-radius: 100%; background-color: #e4e4e4;" alt="user image" width="100%" height="100px;padding:2%">
</div>
</div>
<div class="form-group">
<div class="col-sm-2" id="head_details">
<label class="control-label" for="email">Company Details </label>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="add_user_details">
<div class="row">
<div class="form-group" style="display:none">
<label class="control-label col-sm-5" for="email">ID <span class="errors">
*
</span>: 
</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="id1" name="id1">
</div>
</div>
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Employee ID <span class="errors">
*
</span>: 
</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="emp_id1" name="emp_id1" placeholder="Employee ID" readonly>
</div>
</div>
</div>
<div class="row">
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">First Name <span class="errors">
*
</span>: 
</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="first_name1" name="first_name1" placeholder="First Name">
</div>
</div>
<div class="form-group col-lg-6" style="margin-bottom:0px">
<label class="control-label col-sm-5" for="email">Last Name<span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="last_name1" name="last_name1" placeholder="Last Name">
</div>
</div>
<div class="form-group col-lg-6" style="display:none">
<label class="control-label col-sm-5" for="email">Company ID:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="c_id1" name="c_id1" value="<?php echo $this->session->userdata('companyid');?>" readonly>
</div>
</div>
<div class="form-group" style="display:none">
<label class="control-label col-sm-5" for="email">Company Name:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="c_name1" name="c_name1" value="<?php echo $this->session->userdata('companyname');?>" readonly>
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="col-sm-2" id="head_details">
<label class="control-label" for="email">Personal Details </label>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="add_user_details">
<div class="row">
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Email ID<span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="email_id1" name="email_id1" placeholder="Email ID">
</div>
</div>
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Contact Number<span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="contact_no1" name="contact_no1" placeholder="Contact Number">
</div>
</div>
</div>
<div class="row">
<div class="form-group col-lg-6" style="margin-bottom:0px">
<label class="control-label col-sm-5" for="email">Alternative Number:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="acontact_no1" name="acontact_no1" placeholder="Alternative Number">
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="col-sm-3" id="head_details">
<label class="control-label" for="email">Permanent Address</label>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="add_user_details">
<div class="row">
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Flat / House No<span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="flatno1" name="flatno1" placeholder="Flat / House No. / Floor / Building">
</div>
</div>
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Colony / Street<span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="colony1" name="colony1" placeholder="Colony / Street / Locality">
</div>
</div>
</div>
<div class="row">
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Town/City: <span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="town1" name="town1" placeholder="Town/City">
</div>
</div>
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">State: <span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="state1" name="state1" placeholder="State">
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="col-sm-2" id="head_details">
<label class="control-label" for="email">Work Location</label>
</div>
</div>
<div class="container-fluid" id="add_user_details">
<div class="row">
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Region<span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<!--<input type="text" class="form-control" id="region1" name="region1" placeholder="Region">-->
<select class="form-control" id="region1" name="region1"> 
<option value="north">North</option>
<option value="south">South</option>
<option value="east">East</option>
<option value="west">West</option>
</select> 
</div>
</div>
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Area: <span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="area1" name="area1" placeholder="Area">
</div>
</div>
</div>
<div class="row">
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Location: <span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="location1" name="location1" placeholder="Location">
</div>
</div>
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Role<span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<select class="form-control" id="role1" name="role1">
<option value="" selected disabled>Select Role</option>
<option value="Admin">Admin</option>
<option value="Manager">Manager</option>
<option value="Call-coordinator">Call-coordinator</option>
<option value="Service Desk">Service Desk</option>
<option value="Technician">Technician</option>
</select>
</div>
</div>
<div class="form-group" style="display:none">
<label class="control-label col-sm-5" for="email">roles: <span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<input type="text" class="form-control" id="roles1" name="roles1" placeholder="roles">
</div>
</div>
</div>
</div>
<div class="form-group products1" style="display:none">
<div class="col-sm-1" id="head_details">
<label class="control-label" for="email">Product</label>
</div>
</div>
<div class="container-fluid products1" id="add_user_details" style="display:none">
<div class="row">
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Product Category<span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<select class="form-control" id="product1" name="product1">
<option value="" selected disabled>Select product</option>
<?php
foreach ($record->result() as $row) { 
?>
<option value="<?php echo $row->product_id; ?>"><?php echo $row->product_name; ?></option>
<?php } ?>
</select>
</div>
</div>
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Sub Category <span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<select class="form-control" id="category1" name="category1"> 
</select>
</div>
</div>
</div>
<div class="row">
<div class="form-group col-lg-6">
<label class="control-label col-sm-5" for="email">Skill Level<span class="errors">
*
</span>:</label>
<div class="col-sm-7">
<select class="form-control" id="skill1" name="skill1">
<option value="L1">L1</option>
<option value="L2">L2</option>
<option value="L3">L3</option>
<option value="L4">L4</option>
</select>
</div>
</div>
</div>
</div>
</form>
</div>
<div class="modal-footer">
<button type="submit" class="btn btn-primary" onclick="submit_user()"><i class="fa fa-check"></i> Update</button>
<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
</div>
</div>
</div>
</div>
<!--Modal End-->

<!-- END QUICK SIDEBAR -->
<?php include 'assets/lib/javascript.php'?> 
<script type="text/javascript">
/*$(window).load(function(){
$.ajax({
url : "<?php echo base_url();?>index.php?/controller_admin/get_users",
type : "POST",
data : "",
datatype : "JSON",
cache : false,
process : false,
success : function(data){ 
var data=JSON.parse(data);
console.log(data);

},
})
});*/
window.history.forward();
$(document).ready(function() {
$('#datatable').dataTable();
/*$('#datatable-keytable').DataTable( { keys: true } );
$('#datatable-responsive').DataTable();
$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } ); */ 
});
//TableManageButtons.init(); 
$('#role').change(function(){
var role=$('#role option:selected').val();
if(role=="Technician"){ 
$('.products').show(); 
}else{
$('.products').hide();
}
}); 
$('#role1').change(function(){
var role=$('#role1 option:selected').val();
if(role=="Technician"){ 
$('.products1').show(); 
}else{
$('.products1').hide();
}
}); 
$("#product").change(function (){
$('#category').empty();
var product_id = $('#product').val();
$.ajax({
url : "<?php echo base_url();?>index.php?/controller_admin/getcategory",
type : "POST",
data : {'product_id' : product_id},
datatype : "JSON",
cache : false,
process : false,
success : function(data){ 
var data=JSON.parse(data);
if(data == 1){
$.dialogbox({
type:'msg',
content:'No Category are entered, kindly add category to this product',
closeBtn:true,
btn:['Ok.'],
call:[
function(){
$.dialogbox.close();
//window.location.reload();
}
] 
});
//bootbox.alert("No Category are entered, kindly add category to this product");
}else{
console.log(data);
for(i=0; i<data.length;i++){
$('#category').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>'); 
}
}
},
})
});
$("#product1").change(function (){
$('#category1').empty();
var product_id = $('#product1').val();
$.ajax({
url : "<?php echo base_url();?>index.php?/controller_admin/getcategory",
type : "POST",
data : {'product_id' : product_id},
datatype : "JSON",
cache : false,
process : false,
success : function(data){ 
var data=JSON.parse(data);
if(data == 1){
$.dialogbox({
type:'msg',
content:'No Category are entered, kindly add category to this product',
closeBtn:true,
btn:['Ok.'],
call:[
function(){
$.dialogbox.close();
// window.location.reload();
}
] 
});
//bootbox.alert("No Category are entered, kindly add category to this product");
}else{
console.log(data);
for(i=0; i<data.length;i++){
$('#category1').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>'); 
}
}
},
})
});

$('#inver').change(function(){
$('#tbdy_inventory').empty(); 
var filter=$('#inver').val();
$.ajax({
url: "<?php echo base_url();?>index.php?/controller_admin/user2",
type: 'POST',
data: {'filter':filter},
dataType: "json",
success: function(data) {
$('#tbdy_inventory').html('');
//console.log(data);
if(data.length<1)
{
//console.log(data);
$('#tbdy_inventory').html('<tr><td colspan=8 style="text-align:center">No records found</td></tr>');
}
else
{ 
console.log(data);
//data=JSON.parse(data);
for(i=0;i<data.length;i++)
{ 
if(data[i].role!="Technician"){
$('#tbdy_inventory').append('<tr><td><a id='+data[i].employee_id+' onclick="edit(this.id)">'+data[i].employee_id+'</a></td><td>'+data[i].first_name+'</td><td>'+data[i].role+'</td><td>-</td><td>-</td><td style="text-align:center">-</td></tr>'); 
}else{
$('#tbdy_inventory').append('<tr><td><a id='+data[i].employee_id+' onclick="edit(this.id)">'+data[i].employee_id+'</a></td><td>'+data[i].first_name+'</td><td>'+data[i].role+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td style="text-align:center !important">'+data[i].skill_level+'</td></tr>'); 
}
}
}
}
}); 
}) 
</script>
<script> 
/*$('#dob').datepicker({
changeMonth: true,
changeYear: true,
showButtonPanel: true,
dateFormat: 'yy-mm-dd',
yearRange: "1900:+nn",
});
$('#dob_1').datepicker({
changeMonth: true,
changeYear: true,
showButtonPanel: true,
dateFormat: 'yy-mm-dd',
yearRange: "1900:+nn",
});*/
$('#adduser').click(function(){
$('#rowdata').empty();
var role=$('#role').val();
var c_id=$('#c_id').val();
var c_name=$('#c_name').val();
if(role == "Service Desk"){
$.ajax({
url : "<?php echo base_url(); ?>index.php?/controller_admin/checkservicedesk",
type : "POST",
data : {'c_name':c_name,'c_id':c_id},// {action:'$funky'}
datatype : "JSON", 
cache : false,
success : function(data){
var data=JSON.parse(data);
// console.log(data); 
if(data['service_desk'] <= data['service_added']){
$('#rowdata').append("Service desk limit is crossed, contact Super admin");
$('.error').show();
}else{
addinguser(); 
}
},
});
}else if(role=="Technician"){
$.ajax({
url : "<?php echo base_url(); ?>index.php?/controller_admin/checktech",
type : "POST",
data : {'c_name':c_name,'c_id':c_id},// {action:'$funky'}
datatype : "JSON", 
cache : false,
success : function(data){
var data=JSON.parse(data);
if(data['technicians'] <= data['tech_added']){
$('#rowdata').append("Technician limit is crossed, contact Super admin");
$('.error').show();
}else{
addinguser(); 
}
},
});
}else{
addinguser();
}
function addinguser(){
$.ajax({
url : "<?php echo base_url(); ?>index.php?/controller_admin/insertuser",
type : "POST",
data : $('#add_user').serialize(),// {action:'$funky'}
datatype : "JSON", 
cache : false,
success : function(data){
data=$.trim(data);
//console.log(data);
if(data=="User added Successfully"){
$.dialogbox({
type:'msg',
content:data,
closeBtn:true,
btn:['Ok.'],
call:[
function(){
$.dialogbox.close();
window.location.reload();
}
] 
});
/*bootbox.alert({
message: data,
callback: function () {
location.reload();
}
}) */ 
}else{
$('#rowdata').append(data);
$('#myModal1').animate({ scrollTop: 0 });
$('.error').show();
}
},
});
}
})
$('#bulkupload').click(function(){
var ext = $('#add_excel').val().toString().split('.').pop().toLowerCase();
if ($.inArray(ext, ['xls', 'xlsx', 'csv']) == -1) {
alert('Please upload Excel file');
return false;
}else {
var file_data = $('#add_excel').prop('files')[0]; 
var form_data = new FormData();
form_data.append('add_excel', file_data);
$.ajax({ 
type:'POST',
url:'<?php echo base_url(); ?>index.php?/controller_admin/bulk_user',
contentType:false,
processData: false,
cache:false,
data:form_data, 
success: function (data) { 
/*data=$.trim(data);
//console.log(data);
//var data=JSON.parse(data); 
$.dialogbox({
type:'msg',
content:data,
closeBtn:true,
btn:['Ok.'],
call:[
function(){
$.dialogbox.close();
window.location.reload();
}
] 
});*/
},
}); 
}
}) 
function edit(id){
$.ajax({
url : "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_user",
type : "POST",
data : {'id':id},// {action:'$funky'}
datatype : "JSON", 
cache : false,
success : function(data){
var data=JSON.parse(data); 
console.log(data);
$('#id1').val(data['id']);
$('#emp_id1').val(data['employee_id']);
$('#first_name1').val(data['first_name']);
$('#last_name1').val(data['last_name']);
$('#email_id1').val(data['email_id']);
$('#contact_no1').val(data['contact_number']);
$('#acontact_no1').val(data['alternate_number']);
$('#flatno1').val(data['flat_no']);
$('#colony1').val(data['street']);
$('#town1').val(data['city']);
$('#state1').val(data['state']);
//$('#region1').val(data['region']); 
$('select[name="region1"] option[value="'+data['region']+'"]').attr("selected",true);
$('#location1').val(data['location']);
$('#area1').val(data['area']);
$('#roles1').val(data['role']);
$('#user_image').attr('src',data['image']);
$('select[name="role1"] option[value="'+data['role']+'"]').attr("selected",true);
//$('select[name="skill1"] option[value="'+data['skill_level']+'"]').attr("selected",true); 
$('#skill1').val(data['skill_level']);
if(data['role']=="Technician"){
$('#product1').val(data['product']);
$.ajax({
url : "<?php echo base_url(); ?>index.php?/controller_admin/data_category",
type : "POST",
data : {'id':data['category']},// {action:'$funky'}
datatype : "JSON", 
cache : false,
success : function(data){
var data=JSON.parse(data);
//console.log(data);
$('#category1').append('<option selected value="'+data['cat_id']+'">'+data['cat_name']+'</option>'); 
} 
}); 
$('.products1').show();
}else{
$('.products1').hide();
}
$('#edits').modal('show');
},
});
} 
function submit_user(){
$('#rowdata_1').empty();
$.ajax({
url : "<?php echo base_url(); ?>index.php?/controller_admin/edituser",
type : "POST",
data : $('#edit_user').serialize(),// {action:'$funky'}
datatype : "JSON", 
cache : false,
success : function(data){
//data=$.trim(data);
//console.log(data);
if(data == "User updated Successfully"){
bootbox.alert({
message: data,
callback: function () {
location.reload();

}
}) 
}else{
$('#rowdata_1').append(data);
$('#edits').animate({ scrollTop: 0 });
$('.error').show();
}
},
});
}
function deletes(id){
bootbox.confirm({
message: "Do you want to delete this User?",
buttons: {
confirm: {
label: 'Yes',
className: 'btn-success'
},
cancel: {
label: 'No',
className: 'btn-danger'
}
},
callback: function (result) {
if(result==true){
$.ajax({
url : "<?php echo base_url(); ?>index.php?/controller_admin/deleteuser",
type : "POST",
data : {'id':id},// {action:'$funky'}
//datatype : "JSON", 
cache : false,
beforeSend: function() {
$('#loading_1').modal('show');
$('#progressbar').show();
},
complete: function(xhr) { 
$('#progressbar').hide(); 
},
success : function(data){
if(data == 1){
$.dialogbox({
type:'msg',
content:'User Deleted successfully',
closeBtn:true,
btn:['Ok.'],
call:[
function(){
$.dialogbox.close();
window.location.reload();
}
] 
});
/*bootbox.alert({
message: 'User Deleted successfully',
callback: function () {
location.reload();
}
}); */ 
}else{ 
$.dialogbox({
type:'msg',
content:'User Not Deleted, Kindly try again',
closeBtn:true,
btn:['Ok.'],
call:[
function(){
$.dialogbox.close();
window.location.reload();
}
] 
});
/*bootbox.alert({
message: 'User Not Deleted, Kindly try again',
callback: function () {
location.reload();
}
}); */ 
} 
},
});
}
}
});
}
</script> 
</body>
</html>