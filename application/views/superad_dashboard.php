<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <?php include 'assets/lib/cssscript_super.php'?>
<style>
.amcharts-chart-div a {
    display: none !important;
}
</style>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
        <!-- BEGIN CONTAINER -->
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "assets/lib/header_superad.php"?>
            <!-- END HEADER -->
           <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/superad_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row widget-row">
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading">Companies Registered</h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-green fa fa-building-o"></i>
                                    <div class="widget-thumb-body">
                                        <!--<span class="widget-thumb-subtitle">USD</span>-->
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $record?>"><?php echo $record?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading">Due for Renewal</h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-red fa fa-bookmark-o"></i>
                                    <div class="widget-thumb-body">
                                        <!--<span class="widget-thumb-subtitle">USD</span>-->
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $renew?>"><?php echo $renew?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading">Subscribed Service Desks</h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-purple icon-paper-clip"></i>
                                    <div class="widget-thumb-body">
                                        <!--<span class="widget-thumb-subtitle">USD</span>-->
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $service?>"><?php echo $service?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading">Subscribed Technicians</h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-blue icon-settings"></i>
                                    <div class="widget-thumb-body">
                                        <!--<span class="widget-thumb-subtitle">USD</span>-->
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $tech?>"><?php echo $tech?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                    </div>
					
                    <div class="row">
						 <div class="col-md-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption col-md-12">
                                        <span class="caption-subject bold uppercase font-dark col-md-6" style="line-height: 2;">Revenue Insight</span>
					<span class="col-md-3 col-sm-12 pull-right">
					     <select class="form-control pull-right" id="company_name"  onchange="change_revenue()" data-toggle="dropdown">  
					     </select>
					</span>
                                        <!--<span class="caption-helper">distance stats...</span>-->
                                    </div>
                                    <div class="actions">
                                        <!--<a class="btn btn-circle btn-icon-only btn-default" href="#">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                            <i class="icon-trash"></i>
                                        </a>-->
                                       <!-- <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>-->
                                    </div>
                                </div>
                                <div class="portlet-body">
								 <div class="actions pull-right">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                            <label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                                <input type="radio" name="options1" class="toggle" id="option11" checked value="Month To Date">Month To Date</label>
                                            <label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                <input type="radio" name="options1" class="toggle" id="option22" value="Quarter To Date">Quarter To Date</label>
                                            <label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                <input type="radio" name="options1" class="toggle" id="option33" value="Year To Date">Year To Date</label>
                                        </div>
                                  </div>
								  <div id="chartdiv" class="chart" style="height:300px"></div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption col-md-12">
                                        <span class="caption-subject bold uppercase font-dark col-md-6" style="line-height: 2;">Subscription Insight</span>
					<span class="col-md-3 pull-right">
						 <select class="form-control pull-right" id="org_subs" data-toggle="dropdown" onChange="subs_display();">
					         </select>
					</span>
                                        <!--<span class="caption-helper">distance stats...</span>-->
                                    </div>
                                    
                                </div>
                                <div class="portlet-body">
						      
							   <div class="row" style=" margin-top: 0% !important;">
							    <div class="col-md-4" id="tech_chart">
                 <span id="label" class="col-lg-8 col-lg-offset-2 text-center"><h5><strong>Technicians</strong></h5><span id=count></span></span> <div id="morris-donut-prod-ongoing" style="height: 220px; margin: 14% 4% 4% 4% !important; position: relative;"  class="m-t-10"    ></div> </div>
            <div class="col-md-4" id="subs_chart">
      <span id="label1" class="col-lg-8 col-lg-offset-2 text-center"><h5><strong>Service Desks</strong></h5><span id=count1></span></span><div id="morris-donut-ongoing" style="height: 220px; margin: 14% 4% 4% 4% !important; position: relative;"  class="m-t-10"></div>
                            </div>
                               <div class="col-md-4" id="subs_chart">
      <span id="label1" class="col-lg-8 col-lg-offset-2 text-center"><h5><strong>Total Calls</strong></h5><span id=tic_count></span></span><div id="morris-donut-totalcalls" style="height: 220px; margin: 14% 4% 4% 4% !important; position: relative;"  class="m-t-10"></div>
                            </div>
							   </div>
                                    <!--<div id="dashboard_amchart_1" class="CSSAnimationChart"></div>-->
									
                                </div>
                            </div>
                        </div>
                       
                    </div>
                
     
                    <!-- END PAGE BASE CONTENT -->
                </div>
            </div>
        </div>
                <!-- BEGIN FOOTER -->
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
</div>
		
		<?php include 'assets/lib/javascript_super.php'?>   
 <script>                
            $('.nav.navbar-nav').find('.open').removeClass( 'open' );
            $('#focus_dasboard').addClass('open');
        </script>
 <script>
	$(document).ready(function() {  

		$.ajax({
				url: "<?php echo site_url('controller_superad/choose_company')?>",
				method: "POST",
					success	: function(data){
						console.log(data);
						var result = JSON.parse(data);
						$('#sla_cname').val();
						//$('#sla_cname').append('<option selected></option>');
						for(i=0;i<result.length;i++)
							{
			   $('#sla_cname').append('<option value='+result[i]['company_id']+'>'+result[i]['company_name']+'</option>');
							}
			  var radioValue = $("input:radio[name='optionsla']:checked").val();
				//alert (radioValue);
				var company_id=$('#sla_cname').val();
				//alert(company_id);
				$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_superad/slaa",
					type: 'POST',
					data:'filter=' + radioValue + '&comp=' + company_id,
					dataType:"json",				
					success: function(info) {					
					//alert(info[0]['target']);
                                 if(info[0]['target']=="0.00" && info[0]['sla']=="0.00" && info[1]['target']=="0.00" && info[1]['sla']=="0.00" && info[2]['target']=="0.00" && info[2]['sla']=="0.00" && info[3]['target']=="0.00" && info[3]['sla']=="0.00")
                                  {
                               //  alert("null value");
                                 $('#amcharts-chart-div').html('');
                                 $('#amcharts-chart-div').html('<div style="width: 40%; margin: 0px auto; padding-top:16%;"><h3>No Data Available</h3></div>');
                                  }
                                 else
                                {
					$('#amcharts-chart-div').html('');
				    var chart = AmCharts.makeChart("amcharts-chart-div", {
	"type": "serial",
        "theme": "light",
	"categoryField": "priority",
        "legend":{
	"useGraphSettings":true
	},
	"rotate": true,
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start",
		"position": "left"
	},
	"trendLines": [],
        "gridAboveGraphs": true,
			"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
			},
	"graphs": [
		{ 
                        "balloonText": false,
			//"balloonText": "Target:[[value]]",
			"fillAlphas": 0.8,
			"id": "AmGraph-1",
			"lineAlpha": 0.2,
			"title": "Tagert",
			"type": "column",
			"valueField": "target"
		},
		{
			"balloonText": false,
			"fillAlphas": 0.8,
			"id": "AmGraph-2",
			"lineAlpha": 0.2,
			"title": "Actual",
			"type": "column",
			"valueField": "sla"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"position": "top",
			"axisAlpha": 0
		}
	],
	"allLabels": [],
	"balloon": {},
	"titles": [],
	"dataProvider":info,
    "export": {
    	"enabled": true
     },
     "legend": {}

});
   }
				    }
				});
						},
					error: function( error )
                                {
								//	alert( "Error" );
								}
		  });

		  $.ajax({
				url: "<?php echo site_url('controller_superad/choose_company')?>",
				method: "POST",
					success	: function(data){
						console.log(data);
						var result = JSON.parse(data);
						$('#org_subs').val();
						$('#org_subs').append('<option selected>Overall Subscription</option>');
						for(i=0;i<result.length;i++)
							{
								$('#org_subs').append('<option value='+result[i]['company_id']+'>'+result[i]['company_name']+'</option>');
							}
						},
					error: function( error )
                                {
								//	alert( "Error" );
								}
		  });
		  $.ajax({
				url: "<?php echo site_url('controller_superad/choose_company')?>",
				method: "POST",
					success	: function(data){
						console.log(data);
						var result = JSON.parse(data);
						$('#company_name').val();
						$('#company_name').append('<option selected value="empty">Overall Revenue</option>');
						for(i=0;i<result.length;i++)
							{
								$('#company_name').append('<option value='+result[i]['company_id']+'>'+result[i]['company_name']+'</option>');
							}
						},
					error: function( error )
                                {
								//	alert( "Error" );
								}
		  });	
        		var prod_result=[];
			var prod_result1=[];
			$.ajax({
				url: "<?php echo base_url();?>" + "index.php?/controller_superad/total_subscription",
				type: 'POST',
				success: function(data) {
					var data=JSON.parse(data);
					$('#morris-donut-prod-ongoing').html('');
					$('#morris-donut-ongoing').html(''); 
                    $('#morris-donut-totalcalls').html('');
					$("#count").html('');
                    $("#count1").html('');
					$("#tic_count").html('');

					//console.log(data);
					//var total=parseInt(data[0].service_desk) + parseInt(data[0].technician);
					$("#count").text(data[0].technician);
                    $("#count1").text(data[0].service_desk);
					$("#tic_count").text(data[0].tickets_count);
					prod_result=[
						{ label: 'Registered', value:  data[0].used_tech},
						{ label: 'Available', value:  data[0].available_t}
					];
					prod_result1=[
						{ label: 'Registered', value:  data[0].used_serv},
						{ label: 'Available', value:  data[0].available_s}
					];
                    ticket_count=[
                        { label: 'Total Calls', value: data[0].tickets_count},
                    ];
					Morris.Donut({
						element: 'morris-donut-prod-ongoing',
						data: prod_result,
						colors:['rgb(217, 61, 94)', "rgb(103, 183, 220)"]
						//colors:['#7a92a3', "#0b62a4"]
					});
					Morris.Donut({
						element: 'morris-donut-ongoing',
						data: prod_result1,
						colors:['rgb(217, 61, 94)', "rgb(103, 183, 220)"]
						//colors:['#7a92a3', "#0b62a4"]
					});
                    Morris.Donut({
                        element: 'morris-donut-totalcalls',
                        data: ticket_count,
                        colors:['rgb(103, 183, 220)']
                        //colors:['#7a92a3', "#0b62a4"]
                    });
				}
				
			});
		
		var radioValue = $("input:radio[name='options1']:checked").val();
		//var company_id=$("#sla_cname").val();
				$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_superad/dashboard",
					type: 'POST',
					data:'filter=' + radioValue,
					//data:'filter=' + radioValue + '&comp=' + company_id,
					dataType:"json",				
					success: function(info) {					
					console.log(info);
					$('#chartdiv').html('');
						var chart = AmCharts.makeChart("chartdiv", {
								"type": "serial",
								"theme": "light","startDuration":0.2,
								"legend": {
								"useGraphSettings": true,
								},
								"dataProvider":info,
								"valueAxes": [{
								"gridColor": "#FFFFFF",
								"gridAlpha": 0.2,
								"dashLength": 0,"title":"Revenue (Lakhs)"
								}],
								"gridAboveGraphs": true,
								"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
								},"graphs": [{
								"title": "Previous Year",
								"valueField": "profit",
								"bullet": "round","showBalloon": false,
								"bulletSize": 10,
								"bulletBorderColor": "#ffffff",
								"bulletBorderAlpha": 1,
								"bulletBorderThickness": 2,
								"lineThickness": 3,
								},
								{
								"bullet": "round","showBalloon": false,
								"bulletSize": 10,
								"bulletBorderColor": "#ffffff",
								"bulletBorderAlpha": 1,
								"bulletBorderThickness": 2,
								"valueField": "profit_now",
								"title": "Current Year","lineColor":"#d93d5e",
								"lineThickness": 3
								}],
								"categoryField": "Date",
								"categoryAxis": {
								"gridPosition": "start"
								},
								"legend": {}
								});
								},					  
					});
		});
 </script>
 <script>
			$(".toggle_sla").change(function () {
				var radioValue = $("input:radio[name='optionsla']:checked").val();
				//alert (radioValue);
				var company_id=$("#sla_cname").val();
				$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_superad/slaa",
					type: 'POST',
					data:'filter=' + radioValue + '&comp=' + company_id,
					dataType:"json",				
					success: function(info) {					
					console.log(info);
                    if(info[0]['target']=="0.00" && info[0]['sla']=="0.00" && info[1]['target']=="0.00" && info[1]['sla']=="0.00" && info[2]['target']=="0.00" && info[2]['sla']=="0.00" && info[3]['target']=="0.00" && info[3]['sla']=="0.00")
                                  {
                                    // alert("null value");
                                 $('#amcharts-chart-div').html('');
                                 $('#amcharts-chart-div').html('<div style="width: 42%; margin: 0px auto; padding-top:15%;"><h3>No Data Available</h3></div>');
                                 }
                            else {
					$('#amcharts-chart-div').html('');
				var chart = AmCharts.makeChart("amcharts-chart-div", {
	"type": "serial",
     "theme": "light",
	"categoryField": "priority",
         "legend":{
	"useGraphSettings":true
	},
	"rotate": true,
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start",
		"position": "left"
	},
	"trendLines": [],
         "gridAboveGraphs": true,
			"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
			},
	"graphs": [
		{
			"balloonText": false,
			"fillAlphas": 0.8,
			"id": "AmGraph-1",
			"lineAlpha": 0.2,
			"title": "Tagert",
			"type": "column",
			"valueField": "target"
		},
		{
			"balloonText": false,
			"fillAlphas": 0.8,
			"id": "AmGraph-2",
			"lineAlpha": 0.2,
			"title": "Actual",
			"type": "column",
			"valueField": "sla"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"position": "top",
			"axisAlpha": 0
		}
	],
	"allLabels": [],
	"balloon": {},
	"titles": [],
	"dataProvider":info,
    "export": {
    	"enabled": true
     }, "legend": {}

});
                   }
                   }
				});
			});
   function company_sla()
	{
		var radioValue = $("input:radio[name='optionsla']:checked").val();
		var company_id=$("#sla_cname").val();
                //alert(company_id);
					$.ajax({
					url: "<?php echo base_url();?>" + "index.php?/controller_superad/slaa",
					type: 'POST',
					data:'filter=' + radioValue + '&comp=' + company_id,
					dataType:"json",				
					success: function(info) {					
					console.log(info);
                         if(info[0]['target']=="0.00" && info[0]['sla']=="0.00" && info[1]['target']=="0.00" && info[1]['sla']=="0.00" && info[2]['target']=="0.00" && info[2]['sla']=="0.00" && info[3]['target']=="0.00" && info[3]['sla']=="0.00")
                                  {
                                     //alert("null value");
                                 $('#amcharts-chart-div').html('');
                                 $('#amcharts-chart-div').html('<div style="width: 40%; margin: 0px auto; padding-top:16%;"><h3>No Data Available</h3></div>');
                                 }
                              else
                              {
					$('#amcharts-chart-div').html('');
				var chart = AmCharts.makeChart("amcharts-chart-div", {
	"type": "serial",
     "theme": "light",
	"categoryField": "priority",
	 "legend":{
	"useGraphSettings":true
	},
	"rotate": true,
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start",
		"position": "left"
	},
	"trendLines": [],
	"gridAboveGraphs": true,
			"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
			},
	"graphs": [
		{
			"balloonText": false,
			"fillAlphas": 0.8,
			"id": "AmGraph-1",
			"lineAlpha": 0.2,
			"title": "Tagert",
			"type": "column",
			"valueField": "target"
		},
		{
			"balloonText": false,
			"fillAlphas": 0.8,
			"id": "AmGraph-2",
			"lineAlpha": 0.2,
			"title": "Actual",
			"type": "column",
			"valueField": "sla"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"position": "top",
			"axisAlpha": 0
		}
	],
	"allLabels": [],
	"balloon": {},
	"titles": [],
	"dataProvider":info,
    "export": {
    	"enabled": true
     }, "legend": {}

     });

		}		    }
				});
	
	}
	function subs_display() {	
			var prod_result=[];
			var  prod_result1=[];
             var ticket_count=[];
			var company_id=$("#org_subs").val();
			if(company_id=="Overall Subscription")
			{
				var prod_result=[];
               var prod_result1=[];
			   var ticket_count=[];
			$.ajax({
				url: "<?php echo base_url();?>" + "index.php?/controller_superad/total_subscription",
				type: 'POST',
				success: function(data) {
					var data=JSON.parse(data);
					$('#morris-donut-prod-ongoing').html('');
					$('#morris-donut-ongoing').html(''); 
                    $('#morris-donut-totalcalls').html('');
					$("#count").html('');
					$("#count1").html('');
                    $("#tic_count").html('');
					//console.log(data);
					//var total=parseInt(data[0].service_desk) + parseInt(data[0].technician);
					$("#count").text(data[0].technician);
					$("#count1").text(data[0].service_desk);
                    $("#tic_count").text(data[0].tickets_count);
					prod_result=[
						{ label: 'Registered', value:  data[0].used_tech},
						{ label: 'Available', value:  data[0].available_t}
					];
					prod_result1=[
						{ label: 'Registered', value:  data[0].used_serv},
						{ label: 'Available', value:  data[0].available_s}
					];
                    ticket_count=[
                        { label: 'Total Calls', value: data[0].tickets_count},
                    ];
					Morris.Donut({
						element: 'morris-donut-prod-ongoing',
						data: prod_result,
						colors:['rgb(217, 61, 94)', "rgb(103, 183, 220)"]
						//colors:['#7a92a3', "#0b62a4"]
					});
					Morris.Donut({
						element: 'morris-donut-ongoing',
						data: prod_result1,
						colors:['rgb(217, 61, 94)', "rgb(103, 183, 220)"]
						//colors:['#7a92a3', "#0b62a4"]
					});
                    Morris.Donut({
                        element: 'morris-donut-totalcalls',
                        data: ticket_count,
                        colors:['rgb(103, 183, 220)']
                        //colors:['#7a92a3', "#0b62a4"]
                    });
				}
				
			});
			
			}
		    else
			{
                var prod_result=[];
               var prod_result1=[];
               var ticket_count=[];
			$.ajax({
				url: "<?php echo base_url();?>" + "index.php?/controller_superad/subscribed_tech",
				type: 'POST',
				data:{'company':company_id},
				success: function(data) {
					console.log(data);
					$('#morris-donut-prod-ongoing').html('');
                    $('#morris-donut-ongoing').html('');
					$('#morris-donut-totalcalls').html('');
					$('#count').html('');
					$('#count1').html('');
                    $("#tic_count").html('');
				var data=JSON.parse(data);
					//console.log(data);
					$('#count').text(data[0].technician);
					$('#count1').text(data[0].service_desk);
                    $("#tic_count").text(data[0].tickets_count);
					//var total=parseInt(data[0].service_desk) + parseInt(data[0].serv_added);
					//var total1=parseInt(data[0].technician) + parseInt(data[0].tech_added);
					 prod_result=[
						{ label: 'Registered', value:  data[0].used_tech},
						{ label: 'Available', value:  data[0].available_t}
					 ]; 
					 prod_result1=[
						{ label: 'Registered', value:  data[0].used_serv},
						{ label: 'Available', value:  data[0].available_s}
					  ];
                    ticket_count=[
                        { label: 'Total Calls', value: data[0].tickets_count},
                    ];
					Morris.Donut({
						element: 'morris-donut-prod-ongoing',
						data: prod_result,
						colors:['rgb(217, 61, 94)', "rgb(103, 183, 220)"]
						//colors:['#7a92a3', "#0b62a4"]
					});
					 Morris.Donut({
						element: 'morris-donut-ongoing',
						data:  prod_result1,
						colors:['rgb(217, 61, 94)', "rgb(103, 183, 220)"]
						//colors:['#7a92a3', "#0b62a4"]
					}); 
                    Morris.Donut({
                        element: 'morris-donut-totalcalls',
                        data: ticket_count,
                        colors:['rgb(103, 183, 220)']
                        //colors:['#7a92a3', "#0b62a4"]
                    });
				}
			});
		}
	 
	 }
 </script>
<script>
  $(".toggle").change(function () {
  var radioValue = $("input:radio[name='options1']:checked").val();
                     //alert(radioValue);
         var company_id = $("#company_name").val();
		 if(company_id=="empty")
		 {
		$.ajax({
			url: "<?php echo base_url();?>" + "index.php?/controller_superad/dashboard",
			type: 'POST',
			data:{'filter':radioValue},
			dataType:"json",				
			success: function(info) {					
			console.log(info);
			$('#chartdiv').html('');
			var chart = AmCharts.makeChart("chartdiv", {
					"type": "serial",
					"theme": "light","startDuration":0.2,
					"legend": {
					"useGraphSettings": true,
					},
					"dataProvider":info,
					"valueAxes": [{
					"gridColor": "#FFFFFF",
					"gridAlpha": 0.2,
					"dashLength": 0,"title":"Revenue (Lakhs)"
					}],
					"gridAboveGraphs": true,
					"chartCursor": {
					"categoryBalloonEnabled": false,
					"cursorAlpha": 0,
					"zoomable": false
					},"graphs": [{
					"title": "Previous Year",
					"valueField": "profit",
					"bullet": "round","showBalloon": false,
					"bulletSize": 10,
					"bulletBorderColor": "#ffffff",
					"bulletBorderAlpha": 1,
					"bulletBorderThickness": 2,
					"lineThickness": 3,
					},
					{
					"bullet": "round","showBalloon": false,
					"bulletSize": 10,
					"bulletBorderColor": "#ffffff",
					"bulletBorderAlpha": 1,
					"bulletBorderThickness": 2,
					"valueField": "profit_now",
					"title": "Current Year","lineColor":"#d93d5e",
					"lineThickness": 3
					}],
					"categoryField": "Date",
					"categoryAxis": {
					"gridPosition": "start"
					},
					"legend": {}
					});
			},					  
		});
	}
	else {
		var radioValue = $("input:radio[name='options1']:checked").val();
                  // alert(radioValue);
         var company_id = $("#company_name").val();
		$.ajax({
			url: "<?php echo base_url();?>" + "index.php?/controller_superad/revenue_graph",
			type: 'POST',
			data:'filter=' + radioValue + '&comp=' + company_id,
			dataType:"json",				
			success: function(info) {					
			
			$('#chartdiv').html('');
			var chart = AmCharts.makeChart("chartdiv", {
					"type": "serial",
					"theme": "light","startDuration":0.2,
					"legend": {
					"useGraphSettings": true,
					},
					"dataProvider":info,
					"valueAxes": [{
					"gridColor": "#FFFFFF",
					"gridAlpha": 0.2,
					"dashLength": 0,"title":"Revenue (Lakhs)"
					}],
					"gridAboveGraphs": true,
					"chartCursor": {
					"categoryBalloonEnabled": false,
					"cursorAlpha": 0,
					"zoomable": false
					},"graphs": [{
					"title": "Previous Year",
					"valueField": "profit",
					"bullet": "round","showBalloon": false,
					"bulletSize": 10,
					"bulletBorderColor": "#ffffff",
					"bulletBorderAlpha": 1,
					"bulletBorderThickness": 2,
					"lineThickness": 3,
					},
					{
					"bullet": "round","showBalloon": false,
					"bulletSize": 10,
					"bulletBorderColor": "#ffffff",
					"bulletBorderAlpha": 1,
					"bulletBorderThickness": 2,
					"valueField": "profit_now",
					"title": "Current Year","lineColor":"#d93d5e",
					"lineThickness": 3
					}],
					"categoryField": "Date",
					"categoryAxis": {
					"gridPosition": "start"
					},
					"legend": {}
					});
			},					  
		});
	
	}
	});


</script>
 <!-- Chart code -->
 <script>
	function change_revenue()
	{
		 var radioValue = $("input:radio[name='options1']:checked").val();
                //alert(radioValue);
         var company_id = $("#company_name").val();
               //alert(company_id);
		$.ajax({
			url: "<?php echo base_url();?>" + "index.php?/controller_superad/revenue_graph",
			type: 'POST',
			data:'filter=' + radioValue + '&comp=' + company_id,
			dataType:"json",				
			success: function(info) {					
			console.log(info);
			$('#chartdiv').html('');
                        $('chartdiv').val();
			var chart = AmCharts.makeChart("chartdiv", {
					"type": "serial",
					"theme": "light","startDuration":0.2,
					"legend": {
					"useGraphSettings": true,
					},
					"dataProvider":info,
					"valueAxes": [{
					"gridColor": "#FFFFFF",
					"gridAlpha": 0.2,
					"dashLength": 0,"title":"Revenue (Lakhs)"
					}],
					"gridAboveGraphs": true,
					"chartCursor": {
					"categoryBalloonEnabled": false,
					"cursorAlpha": 0,
					"zoomable": false
					},"graphs": [{
					"title": "Previous Year",
					"valueField": "profit",
					"bullet": "round","showBalloon": false,
					"bulletSize": 10,
					"bulletBorderColor": "#ffffff",
					"bulletBorderAlpha": 1,
					"bulletBorderThickness": 2,
					"lineThickness": 3,
					},
					{
					"bullet": "round","showBalloon": false,
					"bulletSize": 10,
					"bulletBorderColor": "#ffffff",
					"bulletBorderAlpha": 1,
					"bulletBorderThickness": 2,
					"valueField": "profit_now",
					"title": "Current Year","lineColor":"#d93d5e",
					"lineThickness": 3
					}],
					"categoryField": "Date",
					"categoryAxis": {
					"gridPosition": "start"
					},
					"legend": {}
					});
			},					  
		});
	
	}
 </script>
    </body>

</html>