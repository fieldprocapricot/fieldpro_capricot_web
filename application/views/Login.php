<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <?php include 'assets/lib/cssscript.php'?>
    </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?php echo base_url();?>">
                <img src="<?php echo base_url();?>/assets/layouts/layout5/img/logos.png" alt="Logo" /> </a> 
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
          <!--  <form class="login-form" action="<?php //echo base_url('index.php?/Login/login');?>" method="post">  -->
		<!--	 <form id="login_form" class="login-form" method="post" > -->
			<div class="login-form">
                <h3 class="form-title font-green">Sign In</h3>
                <div class="alert alert-danger display-hide">
                     <!-- <button class="close" data-close="alert"></button>
                   <span> Enter any username and password. </span> -->
				
               </div>
			    <span id="loginsuccess_msg" class="error_msg" style="color:green"></span>
                <span id="loginerror_msg" class="error_msg" style="color:red"></span>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
   <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" id="username" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
  <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" id="password" name="password" /> </div>
                <div class="form-actions ">
				<div class="pull-left"><a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a></div>
				<div class="pull-right">
				 <!--<label class="rememberme check mt-checkbox mt-checkbox-outline">
                       <input type="checkbox" name="remember" value="1" />Remember 
                        <span></span>
                    </label>-->
                    <button type="button" id="weblogin1" class="btn green uppercase">Login</button>
					</div><span class="clearfix"></span> 
                </div>    
			</div>
        <!--</form> -->
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" id="forgot_password" action="" method="post">
           
                <h3 class="font-green">Forget Password ?</h3>
                <p id="init"> Enter your e-mail address below to reset your password. </p>
                <span class="error_msg" style="color:green"><p id="success_msg"></p></span>
                <span class="error_msg" style="color:red"><p id="error_msg"></p></span>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="femail" id="femail" />
                </div>
                     <span id="result"></span>
					 <span id="results" style="color:red"><p id="error_password"></p></span>	
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                    <button type="button" id="forgetpassword1" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
        </div>
          <!--loading model-->
          <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->
        <div class="copyright">  Copyright &copy; <?php echo date('Y'); ?> <a target="_blank" href="http://kaspontech.com">Kaspon Techworks Pvt. Ltd.,</a> All rights reserved.</div>
        <?php include 'assets/lib/javascript.php'?>

<script>
	$('#weblogin1').click(function(){
			var username=$.trim($("#username").val());
          var password=$.trim($("#password").val());
		//alert(username);
		//alert(password);
	
								
					$.ajax({
						url         :   "<?php echo base_url(); ?>index.php?/Login/login_web",
						type        :   "POST",
						data        :   {username:username,password:password},
						//datatype	:	"JSON",	
						cache       :   false,
						success    	: 	function(data1){	
					
                      if(data1 == "success"){
						  window.location.href = "<?php echo base_url(); ?>index.php?/Login/user_login_page"; 
                             
                            }
                            else{
                                //alert(data1);
                                $('#loginerror_msg').html(data1);
                            }
                                
                        
                         
                     
                        }
										
					});
	
			
		});


	
	$('#forgetpassword1').click(function(){
			var femail=$("#femail").val();
            $('#Searching_Modal').modal('show');
			if(femail !=""){
								
					$.ajax({
						url         :   "<?php echo base_url(); ?>index.php?/Login/Forgot_Password",
						type        :   "POST",
						data        :   $('#forgot_password').serialize(),// {action:'$funky'}
						//datatype	:	"JSON",	
						cache       :   false,
						success    	: 	function(data1){	

                           $('#Searching_Modal').modal('hide');
                            if(data1 == "success"){
                                $('#init').html("");
                                $('#success_msg').html("Password sent to your email");
                            }
                            else{
                                
                                $('#error_msg').html(data1);
                            }
                     
                        }
										
					});
				
			}else{
				$('#error_password').html('Email is mandatory');
			}			
			$('#femail').keyup(function(){
				$('#error_password').html("");
			});						
			
		});
</script>
		    </body>
</html>