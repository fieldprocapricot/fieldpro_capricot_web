<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<?php
 		$company_id=$this->session->userdata('companyid');
			$region=$user['region'];$area=$user['area'];$location=$user['location'];
         include 'assets/lib/cssscript.php'?>		 
		 <style>
			.dt-buttons{
				display:none !important;
			}
			 .dataTables_filter{
    			text-align: right;
			 }
		 </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
    <!-- BEGIN CONTAINER -->
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include "assets/lib/manager_header.php"?>
        <!-- END HEADER -->
		<div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
				  
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box dark">
                                <div class="portlet-title">
                                   <div class="caption">Imprest Spare </div>
                                    <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_actions_pending" data-toggle="tab">New Spare</a>
                                                </li>
                                                <li>
                                                    <a href="#Accepted" data-toggle="tab">Approved Spare</a>
                                                </li>
                                            </ul>
                                </div>
                                <div class="portlet-body">
										<div class="tab-content">

                                        <div class="tab-pane active" id="tab_actions_pending">
                                            <div class="table=responsive">

                                                <table class="table table-hover table-bordered datatable1" id="">
                                                    <thead>
                                                         <tr>
												<th style="text-align:center">Technician ID</th>
												<th style="text-align:center">Technician Name</th>
                                                <th style="text-align:center">Spare Required</th>
												<th style="text-align:center">Action</th>
                                            </tr>
                                                    </thead>
                                                     <tbody id="tbl_view">
						 <?php  					
						foreach ($record as $row) {
							$string  = json_encode(json_decode($row['spare_array']));
			                // $string = json_decode($row['spare_array']);
								?>
								<tr>
									<td style="text-align:center" id="<?php echo $row['tech_id']; ?>" onClick="hover_tech('<?php echo $row['employee_id']; ?>','<?php echo $row['first_name']; ?>','<?php echo $row['skill_level']; ?>','<?php echo $row['contact_number']; ?>','<?php echo $row['location']; ?>','<?php echo $row['product_name']; ?>','<?php echo $row['cat_name']; ?>')";><a><?php echo $row['employee_id']; ?></a></td>
									 <td style="text-align:center"><?php echo $row['first_name']; ?></td>
									<td style="text-align:center">
	<button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row['tech_id']; ?>" onclick=hover_spare(this.id,'<?php echo $string; ?>')><i class="fa fa-eye"></i></button>
									</td>			
									
                                    <td class="actions"  style='text-align:center !important'>
                                                                         <span>
							<button class="btn btn-circle green btn-outline btn-icon-only" id="<?php echo $row['p_id']; ?>" onClick="accepts(this.id,'<?php echo $row['tech_id']; ?>')"><i class="fa fa-check" aria-hidden="true"></i></button>
											<button class="btn btn-circle red btn-outline btn-icon-only" id="<?php echo $row['p_id']; ?>" onClick="Rejects(this.id)"><i class="fa fa-times" aria-hidden="true"></i></button>						
											</span>
                                    </td>
									<!--<td style='text-align:center !important'>
										<span class="btn-group btn-group-circle">
											<button class="btn btn-outline green btn-sm" id="<?php echo $row['tech_id']; ?>" onclick="accepts(this.id)">Accept</button>											
											<button class="btn btn-outline red btn-sm" id="<?php echo $row['tech_id']; ?>" onclick="Rejects(this.id)">Reject</button>						
											</span>
									</td>-->
								</tr>
							<?php } ?>
                      </tbody>
                                                </table>
                                            </div>
                                        </div>


                                        <div class="tab-pane fade" id="Accepted">
                                            <div class="table=responsive">

                                                <table class="table table-hover table-bordered datatable2" id="">
                                                    <thead>
                                                       <tr>
													<th style="text-align:center">Technician ID</th>
													<th style="text-align:center">Technician Name</th>
													<th style="text-align:center">Spare Required</th>
    												<th style="text-align:center">Status</th>
													</tr>
                                                    </thead>
                                                        <tbody id="tbdy_attendance">
						 <?php  							
						foreach ($records as $r) {
							$string  = json_encode(json_decode($r['spare_array']));
								?>
								<tr>
								  
									<td style="text-align:center" id="<?php echo $r['tech_id']; ?>" onClick="hover_tech('<?php echo $row['employee_id']; ?>','<?php echo $r['first_name']; ?>','<?php echo $r['skill_level']; ?>','<?php echo $r['contact_number']; ?>','<?php echo $r['location']; ?>','<?php echo $r['product_name']; ?>','<?php echo $r['cat_name']; ?>')";><a><?php echo $r['employee_id']; ?></a></td>
<td style="text-align:center"><?php echo $r['first_name']; ?></td>
									<td style="text-align:center">
									<button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $r['tech_id']; ?>" onclick=hover_spare(this.id,'<?php echo $string; ?>')><i class="fa fa-eye"></i></button>
									</td>
									 <td style="text-align:center">
                      <?php if ($r['status_comment']==0){echo 'In-Progress';}  else{echo 'Delivered';}?>
                                                                        </td>
													</tr>
													<?php } ?>
												</tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
<?php include "assets/lib/footer.php"?>
    </div>
 <div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
				<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Spare Details</h5>
      </div>
      <div class="modal-body"id='modal_display' style="text-align:center !important">
		<div class="table-responsive">
			<table class="table table-hover ">
				<thead>
					<td>Spare code</td>
					<td>Spare Required</td>
					<td>Spare Location</td>
				</thead>
				<tbody id="spare_details">
				</tbody>
			</table>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
            </div>

        </div>


	<div id="myModal1" class="modal fade" role="dialog">
	  <div class="modal-dialog">
				<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Details</h5>
      </div>
      <div class="modal-body"id='modal_tech'>
		<form class="form-horizontal" role="form" >
                 
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
            </div>

        </div>
			<div id="myModa2" class="modal fade" role="dialog">
	  <div class="modal-dialog">
				<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Details</h5>
      </div>
      <div class="modal-body"id='modal_tic'>
		<form class="form-horizontal" role="form" >
                 
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
            </div>

        </div>

        <!-- END wrapper -->
<?php include 'assets/lib/javascript.php'?>
<script>        		
       		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
        	$('#man_spares').addClass('open active');
        	$('#man_spare_im').addClass('active');
			</script>
	<script>window.history.forward();
		$(document).ready(function(){
		$('.datatable1').DataTable({"order": []});
		$('.datatable2').DataTable({"order": []});
			/*$(".datatable1").DataTable({
				dom: "Bfrtip",
				buttons: [
					{
					  extend: "copy",
					  className: "btn-sm"
					},
					{
					  extend: "csv",
					  className: "btn-sm"
					},
					{
					  extend: "excel",
					  className: "btn-sm"
					},
					{
					  extend: "pdfHtml5",
					  className: "btn-sm"
					},
					{
					  extend: "print",
					  className: "btn-sm"
					}
				]
			});*/
		});
	</script>
	
   
        <script>
            var resizefunc = [];
        </script>


        <script type="text/javascript">
            $(document).ready(function() {
                //$('#datatable').DataTable();
                /* $('#datatable-keytable').DataTable( { keys: true } );
                $('#datatable-responsive').DataTable();
                $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } ); */
            } );
            //TableManageButtons.init();

        </script>

<script>
		function accepts(id,tech_id){
			swal({
          		  title: "Are you sure?",
          		  text: "You Want to Accept this Spare !",
          		  type: "warning",
          		  showCancelButton: true,
          		  confirmButtonClass: "btn-danger",
          		  confirmButtonText: "Yes, Accept",
          		  cancelButtonText: "No, cancel",
          		  closeOnConfirm: false,
          		  closeOnCancel: false
          		},
				function(isConfirm) {
          		  if (isConfirm) {
            $.ajax({
               url: "<?php echo base_url();?>" + "index.php?/controller_manager/impresed_status",
               type: 'POST',
               data: {'id':id,'tech_id':tech_id},
               //dataType: "json",
               success: function(data) {
                   if(data=='sorry try again')
				   {
					swal("Oops!", data, "Error")
				   }
                   else{
						swal({
							  title: "Good Job!",
							  text: data,
							  type: "success",
							  showCancelButton: true,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Ok",
							  cancelButtonText: "Cancel",
							  closeOnConfirm: false,
							  closeOnCancel: false
						},
						function(isConfirm) {
							if (isConfirm) {
								swal.close();
								location.reload();
							}
										}); 
									  }
                            }
            });
         }
		 else {

                        swal.close();
						//location.reload();
                    }
          		});    
        }
         function Rejects(id){
			 swal({
          		  title: "Are you sure?",
          		  text: "You Want to Reject this Spare !",
          		  type: "warning",
          		  showCancelButton: true,
          		  confirmButtonClass: "btn-danger",
          		  confirmButtonText: "Yes, Reject",
          		  cancelButtonText: "No, cancel",
          		  closeOnConfirm: false,
          		  closeOnCancel: false
          		},
				function(isConfirm) {
          		  if (isConfirm) {
            $.ajax({
               url: "<?php echo base_url();?>" + "index.php?/controller_manager/impresed_reject_status",
               type: 'POST',
               data: {'id':id},
               //dataType: "json",
              success: function(data) {
							   			if(data == "sorry try again"){
                                              	  swal("Cancelled", "Something Went Wrong:)", "error");
                                             }
							   		  else {
										 swal({
											  title: "Rejected",
											  text: data,
											  type: "success",
											  showCancelButton: true,
											  confirmButtonClass: "btn-danger",
											  confirmButtonText: "Ok",
											  cancelButtonText: "Cancel",
											  closeOnConfirm: false,
											  closeOnCancel: false
										},
										function(isConfirm) {
											if (isConfirm) {
												swal.close();
												location.reload();
											}
										}); 
									  }
                            }
            });
         }
		else {

                        swal.close();
						location.reload();
                    }
          		});    
        }

		    
        function hover_tech(tech_id,first_name,Skill_level,contact_number,location,Product_name,cat_name)
	  {
		//cat_name = cat_name.replace(/\:/g," "); 
		  $('#modal_tech form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Technician ID</label><div class="col-sm-6 control-label">'+tech_id+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Name</label><div class="col-sm-6 control-label">'+first_name+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Skill Level</label><div class="col-sm-6 control-label">'+Skill_level+'</div></div><div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Contact Number</label><div class="col-sm-6 control-label">'+contact_number+'</div></div><div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Location</label><div class="col-sm-6 control-label">'+location+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Product Category </label><div class="col-sm-6 control-label">'+Product_name+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Sub Category</label><div class="col-sm-6 control-label">'+cat_name+'</div></div>');
		  $('#myModal1').modal('show');
	  }
		  function hover_tic(ticket_id,cus_id,cus_name,amc_id,call_type,model)
	  {
		 
		  $('#modal_tic form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Ticket ID</label><div class="col-sm-6 control-label">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Customer Id</label><div class="col-sm-6 control-label">'+cus_id+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Customer Name</label><div class="col-sm-6 control-label">'+cus_name+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >AMC Id </label><div class="col-sm-6 control-label">'+amc_id+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Contract Type</label><div class="col-sm-6 control-label">'+call_type+'</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Model</label><div class="col-sm-6 control-label">'+model+'</div></div>');
		  $('#myModa2').modal('show');
	  }	  function hover_spare(ticket_id,spare_array)
	{
         $('#spare_details').empty(); 
            var data = spare_array;
            console.log(data);
			 
			  if(data == "null"){
                                $.dialogbox({
      							type:'msg',
      							content:'No Data',
      							closeBtn:true,
      							btn:['Ok.'],
      							call:[
      								function(){
      									$.dialogbox.close();
      								}
      							]
      
      						});  
                        }
                else{
                var data = JSON.parse(spare_array);
				for(i=0; i<data.length; i++)
				{
				$('#spare_details').append('<tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr>');
				} 
						                             //console.log(data);
                                                      $('#myModal').modal('show');
                                                        }
       
                          
                                }
 function hover_spare_accept(ticket_id,spare_array)
	  {
		  $('#spare_details').empty(); 
		 var data = JSON.parse(spare_array);
		// console.log(data);

		for(i=0; i<data.length; i++)
		 {
			 $('#tbdy_attendance').append('<tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr>');
		   
		 }
		 /*$('#modal_display').html('<table><thead><th>Spare Code</th><th>Spare Required</th><th>Spare Source</th></thead>');
		 for(i=0; i<data.length; i++)
		 {
			 $('#modal_display').append('<tbody><tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr></tbody>');
		   
		 }
		$('#modal_display').append('</table>');*/
		  $('#myModal').modal('show');
		
	  }
	  function hover_ticket(ticket_id,customer_name,location,product_name,cat_name,problem,priority)
	  {
		  var replaceSpace=problem;
		problem = problem.replace(/\:/g," ");
		var location=location;
		location= location.replace(/\:/g," ");
		var product_name=product_name;
		product_name= product_name.replace(/\:/g," ");
		var cat_name=cat_name;
		cat_name= cat_name.replace(/\:/g," ");	
		  $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 control-label"for="inputEmail3">Ticket ID</label><div class="col-sm-4 control-label">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Customer Name</label><div class="col-sm-4 control-label">'+customer_name+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Location</label><div class="col-sm-4 control-label">'+location+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >-Category</label><div class="col-sm-4 control-label">'+product_name+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Sub-Category</label><div class="col-sm-4 control-label">'+cat_name+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Problem</label><div class="col-sm-4 control-label">'+problem+'</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Priority</label><div class="col-sm-4 control-label">'+priority+'</div></div></div>');
		  $('#myModal').modal('show');
	  }
	  
        var company_id="<?php echo $company_id;?>";
	   
		
		function viewimage(id){	
			$.ajax({
				url: "<?php echo base_url();?>" + "index.php?/controller_manager/view",
				type: 'POST',
				data: {'id':id},
				//dataType: "JSON",
				success: function(data) {
					var data=JSON.parse(data);
					//console.log(data['view']);	
					$("#token").attr('src', data['view']);
					$("#myModal").modal('show');
					
				}
			});
		}
		
		</script>
		 <script type="text/javascript">			
          //$('.datatable').dataTable();
          $('#datatable-keytable').DataTable( { keys: true } );
          $('#datatable-responsive').DataTable();
          $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
          var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } ); 
    </script>
	</body>
</html>