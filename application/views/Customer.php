<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php include 'assets/lib/cssscript.php'?>
            <style>
               <!--	.sweet-alert.showSweetAlert.visible{
                  z-index:99999999 !important;
                  }-->
               .fileinput-new, .fileinput-exists {
                 color: #000;
               }
               .sweet-alert.showSweetAlert.visible{
                 border:1px solid red;
               }
               .dt-buttons{
                 display:none !important;
               }
               /* .dataTables_filter
               {
                 text-align-last: right;
               } */
			     .uneditable-input {
min-width:auto !important;
}
.form-control.uneditable-input.input-fixed {
    max-width: 233px !important;
}
@media screen and (max-width: 540px) {
.form-control.uneditable-input.input-fixed {
    max-width: 163px !important;
}
}
            </style>
         </head>
         <!-- END HEAD -->
          <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header.php"?>
               <!-- END HEADER -->
               <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/admin_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">
                                       Customer Management
                                    </div>
                                    <div class="actions">
										<div class="btn-group">
                                                <a href="<?php echo base_url();?>index.php?/customerexcel/customerexcelexport" id="sample_editable_1_new" class="btn btn-circle green btn-outline" > Excel
                                                <i class="fa fa-download"></i>
                                                </a>
                                             </div>
									
									<div class="btn-group">
                                                <button id="sample_editable_1_new" class="btn btn-circle green btn-outline" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"> Bulk Upload
                                                <i class="fa fa-upload"></i>
                                                </button>
                                             </div>
									 </div>
                                 </div>
                                 <div class="portlet-body">
                                    
                                    <div class="table=responsive">
                                       <table class="table table-hover table-bordered sample_2" id="">
                                          <thead>
                                             <tr>
                                                <th class="text-center">Customer ID</th>
                                                <th>Customer Name</th>
                                                <!--<th class="text-center">Contact Number</th> -->
                                                <th style="text-align:center !important">View Contract Info</th>
                                                <th style="text-align:center !important">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody align="center">
                                             <?php
                                                foreach ($records->result() as $row) {
                                                    ?>
                                             <tr>
                                                <td><a id="<?php echo $row->a; ?>" onClick="edit(this.id)"><?php echo $row->customer_id; ?></a></td>
                                                <td align="left"><?php echo $row->customer_name; ?></td>
                                               <!-- <td><?php //echo $row->contact_number; ?></td> -->
                                                <td style="text-align:center !important"> <button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row->customer_id; ?>" onClick="view_contract(this.id)"><i class="fa fa-eye"></i></button></td>
                                                <td style="text-align:center !important">
                                                   <button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row->a; ?>" onClick="edit_details(this.id)"><i class="fa fa-edit"></i></button>
                                                   <!--<button class="btn btn-circle red btn-outline btn-sm" id="<?php echo $row->customer_id; ?>"onclick="deletes(this.id)">Delete</button>       -->
                                                </td>
                                             </tr>
                                             <?php } ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>
                     <!-- END PAGE BASE CONTENT -->
					 	<!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                  </div>
            <!--Modal Starts-->
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Bulk Upload</h4>
                     </div>
                     <div class="modal-body">
                        <form action="#" class="form-horizontal form-bordered">
                           <div class="form-body row">
                              <div class="form-group col-md-4 col-sm-12 text-center">
                                 <?php $fname='customer.xlsx'; ?>
                                 <a class="btn btn-circle purple-sharp btn-outline sbold uppercase" href="<?php echo base_url(); ?>index.php?/controller_admin/download_sampletemplate/<?php echo $fname;?>">Sample Template</a>
                              </div>
                              <div class="form-group col-md-6 col-sm-12">
                                 <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="input-group input-large">
                                          <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                                             <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                             <span class="fileinput-filename"> </span>
                                          </div>
                                          <span class="input-group-addon btn default btn-file">
                                          <span class="fileinput-new"> Select file </span>
                                          <span class="fileinput-exists"> Change </span>
                                          <input type="file" name="add_customer" id="add_customer"> </span>
                                          <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle green btn-outline" id="bulkupload" onClick="bulkupload()" name="bulkupload"><i class="fa fa-upload"></i> Upload</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
            <div id="edits" class="modal fade" role="dialog">
               <div class="modal-dialog modal-lg">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header" style="border:0">
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                        <div class="error" style="display:none">
                           <label id="rowdata_3"></label>
                        </div>
                     </div>
                     <div class="modal-body">
                        <div class="portlet box blue-hoki" id="form_wizard_1" style="margin-bottom:0">
                           <div class="portlet-title">
                              <div class="caption"><strong>Customer Details</strong></div>
                              <div class="actions" style="padding-top: 14px;"> <button type="button" class="close" data-dismiss="modal">&times;</button> </div>
                           </div>
                           <div class="portlet-body form">
                              <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                                 <div class="form-wizard">
                                    <div class="form-body">
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12" style="display:none">
                                             <label class="control-label col-md-5">Id
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="id1" name="id1" readonly required>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12" style="display:none">
                                             <label class="control-label col-md-5">Company Name
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="c_name" name="c_name" value="<?php echo $this->session->userdata('companyname');?>" readonly>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Customer Id
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="cust_id1" name="cust_id1" readonly required>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Customer Name
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="cust_name1" name="cust_name1" readonly required>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Email ID
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="email_id1" name="email_id1" placeholder="" readonly required>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Contact Number
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="contact_no1" name="contact_no1" readonly required>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Alternative Number
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="acontact_no1" name="acontact_no1" readonly placeholder="">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12" style="display:none">
                                             <label class="control-label col-md-5">Company ID
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php echo $this->session->userdata('companyid');?>" readonly>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Flat / House No
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="door_no1" name="door_no1" readonly required>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Colony / Street
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="street1" name="street1" readonly required>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Town / City
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="town1" name="town1" readonly required>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Country
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="country1" name="country1" readonly required>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">PIN Code
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="pin_code1" name="pin_code1" readonly required>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions" style="border-top:1px solid #ddd !important">
                                             <button type="button" class="btn btn-circle red btn-outline pull-right" data-dismiss="modal">Close</button>
                                    </div>
                                 </div>
                           </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            </div>
            <!-- Modal -->
            <div id="edit_info" class="modal fade" role="dialog">
               <div class="modal-dialog modal-lg">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header" style="border:0">
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                        <div class="error" style="display:none">
                           <label id="rowdata_1"></label>
                        </div>
                     </div>
                     <div class="modal-body">
                        <div class="portlet box blue-hoki" id="form_wizard_2" style="margin-bottom:0">
                           <div class="portlet-title">
                              <div class="caption"><strong>Customer Details</strong></div>
                              <div class="actions" style="padding-top: 14px;"> <button type="button" class="close" data-dismiss="modal">&times;</button> </div>
                           </div>
                           <div class="portlet-body form">
                              <form class="form-horizontal" id="submit_form_1" method="POST" novalidate="novalidate">
                                 <div class="form-wizard">
                                    <div class="form-body">
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12" style="display:none">
                                             <label class="control-label col-md-5">Id
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="table_id" name="table_id" readonly required>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12" style="display:none">
                                             <label class="control-label col-md-5">Company Name
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="c_name" name="c_name" value="<?php echo $this->session->userdata('companyname');?>" readonly>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Customer Id
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="edit_id" name="edit_id" readonly>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Customer Name
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="edit_name" name="edit_name">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Email ID
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="email" class="form-control form-control1" id="edit_mail" name="edit_mail" placeholder="">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Contact Number
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="edit_contact" name="edit_contact" >
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Alternative Number
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="edit_anumber" name="edit_anumber" placeholder="">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12" style="display:none">
                                             <label class="control-label col-md-5">Company ID
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php echo $this->session->userdata('companyid');?>" readonly>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Flat / House No
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="edit_door" name="edit_door">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Colony / Street
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="edit_str" name="edit_str">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Town / City
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="edit_town" name="edit_town">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Country
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="edit_country" name="edit_country">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">PIN Code
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="edit_pin" name="edit_pin">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!--<div class="" style="border-top:1px solid #ddd !important">
                                       <div class="row">
                                       	<div class="col-md-offset-5 col-md-7">
                                       		<button type="submit" class="btn btn-circle green btn-outline" id="editbutton">Submit</button>
                                       		<button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal">Close</button>
                                       	</div>
                                       </div>
                                       </div>-->
                                 </div>
                           </div>
                           </form>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <button type="submit" class="btn btn-circle green btn-outline" onClick="editbutton()">Submit</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
            <!--Contract modal-->
            <div id="modal_contract" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <!--<div class="modal-header" style="border:0">
                  </div>-->
               <div class="modal-body">
                  <div class="portlet box blue-hoki" id="form_wizard_1" style="margin-bottom:0">
                     <div class="portlet-title">
                        <div class="caption" style="color:#fff;">Contract Details</div>
                        <div class="actions" style="padding-top: 14px;"><button type="button" class="close" data-dismiss="modal" style="color:#1e252b !important;">&times;</button>
                        </div>
                     </div>
                     <div class="portlet-body form">
                        <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                           <div class="form-wizard">
                              <div class="form-body table-responsive">
                                 <table class="table table-hover table-bordered" id="contract_table">
                                    <thead>
                                       <tr>
                                          <th>ID</th>
                                          <th>Contract Type</th>
										   <th>Contact Number</th> 
                                          <th>Product Category</th>
                                          <th>Sub Category</th>
                                          <th>Model No</th>
										  <th>Serial No</th>
                                          <th>End Date</th>
                                       </tr>
                                    </thead>
                                    <tbody id="tbody_contract">
                                    </tbody>
                                    <!--<button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal">Close</button>-->
                                 </table>
                              </div>
                              <div class="form-actions" style="border-top:1px solid #ddd !important">
                                       <button type="button" class="btn btn-circle red btn-outline pull-right" data-dismiss="modal">Close</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>

               </div>
            </div>
            <!--Modal End-->
            <div id="sla_confirm" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Bulk Upload Details</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" id="add_subproduct">
                           <div class="form-group">
                              <div class="col-sm-12">
                                 <p id="sla_detail_confirm"></p>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="submit" class="btn btn-circle blue btn-outline"  onclick="confirm_sla()"><i class="fa fa-check"></i> Ok !</button>
                     </div>
                  </div>
               </div>
            </div>
		  	 <!--loading model-->
          <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->


            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>
            <script>
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
               $('#customer_management').addClass('open');
            </script>
            <script type="text/javascript">
               window.history.forward();
               $(document).ready(function() {
                   $('#datatable').dataTable();
                   $('.sample_2').DataTable();
               $('.dob').datepicker({ dateFormat: 'yy-mm-dd' });

                   /*$('#datatable-keytable').DataTable( { keys: true } );
                   $('#datatable-responsive').DataTable();
                   $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                   var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
                    TableManageButtons.init();*/
               });
               $("#product").change(function () {
                   $('#category').empty();
                   var product_id = $('#product').val();
                   $.ajax({
                       url         :   "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                       type        :   "POST",
                       data        :   {'product_id' : product_id},
                       datatype    :   "JSON",
                       cache       :   false,
                       process     :   false,
                       success     :   function(data){
                                           var data=JSON.parse(data);
                                           if(data == 1){
                                           	$.dialogbox({
               type:'msg',
               content:"No Category are entered, kindly add category to this product",
               closeBtn:true,
               btn:['Ok.'],
               call:[
               	function(){
               		$.dialogbox.close();
               		//window.location.reload();
               	}
               ]
               });
                                               //bootbox.alert("No Category are entered, kindly add category to this product");
                                           }else{
                                               //console.log(data);
                                               for(i=0; i<data.length;i++){
                                                   $('#category').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                               }
                                           }
                                       },
                   })
               });
               $("#product1").change(function () {
                   $('#category1').empty();
                   var product_id = $('#product1').val();
                   $.ajax({
                       url         :   "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                       type        :   "POST",
                       data        :   {'product_id' : product_id},
                       datatype    :   "JSON",
                       cache       :   false,
                       process     :   false,
                       success     :   function(data){
                                           var data=JSON.parse(data);
                                           if(data == 1){
                                               $.dialogbox({
               type:'msg',
               content:"No Category are entered, kindly add category to this product",
               closeBtn:true,
               btn:['Ok.'],
               call:[
               	function(){
               		$.dialogbox.close();
               		//window.location.reload();
               	}
               ]
               });
                                               //bootbox.alert("No Category are entered, kindly add category to this product");
                                           }else{
                                               //console.log(data);
               $('#category1').append('<option selected disabled>Select Sub Category</option>');
                                               for(i=0; i<data.length;i++){
                                                   $('#category1').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                               }
                                           }
                                       },
                   })
               });
            </script>
            <script>
function confirm_sla(){
	  if(data=""){
		//location.reload();
                      }
          else
                  {
                    location.reload();
                    }
	   }
               function custid_check(){
                   $.ajax({
                       url         :   "<?php echo base_url();?>index.php?/controller_admin/custid_check",
                       type        :   "POST",
                       data        :   "",
                       cache       :   false,
                       success     :   function(data){
                                           $('#cust_id').val($.trim(data));
                                           $('#myModal1').modal('show');
                                       },
                   })
               };
               $('#dob').datepicker();
               $('#dob_1').datepicker();
               $('#addcustomer').click(function(){
                   $('#rowdata').empty();
                   $.ajax({
                       url         :   "<?php echo base_url(); ?>index.php?/controller_admin/insertcustomer",
                       type        :   "POST",
                       data        :   $('#add_customer_1').serialize(),// {action:'$funky'}
                       //datatype  :   "JSON",
                       cache       :   false,
                       success     :   function(data){
                                           //console.log(data);
                                           data=$.trim(data);
                                           if(data == "Customer added Successfully"){
                                               $.dialogbox({
               type:'msg',
               content:data,
               closeBtn:true,
               btn:['Ok.'],
               call:[
               	function(){
               		$.dialogbox.close();
               		window.location.reload();
               	}
               ]
               });
                                               /*bootbox.alert({
                                                   message: data,
                                                   callback: function () {
                                                       location.reload();
                                                   }
                                               })   */
                                           }else{
                                               $('#rowdata').append(data);
                                               $('#myModal1').animate({ scrollTop: 0 });
                                               $('.error').show();
                                           }
                                       },
                   });
               })
               function bulkupload(){
				   
               var companyid="<?php echo $this->session->userdata('companyid');?>";
               //alert(companyid);
                   var ext = $('#add_customer').val().toString().split('.').pop().toLowerCase();
                   if ($.inArray(ext, ['xls', 'xlsx', 'csv']) == -1) {
                       $('#myModal').modal('hide');
               swal({
               title: "Error!",
               text: "Upload Excel file",
               type: "warning",
               confirmButtonClass: "btn-primary",
               confirmButtonText: "Ok.",
               closeOnConfirm: false,
               },
               function(isConfirm) {
               if (isConfirm) {
               swal.close();
               $('#myModal').modal('show');
               }
               });

                      // return false;
                   }
               else {
				     $('#Searching_Modal').modal('show');
                       var file_data = $('#add_customer').prop('files')[0];
                       var form_data = new FormData();
                       form_data.append('add_customer', file_data);
                       form_data.append('companyid', companyid);
                       $.ajax({
                           type:'POST',
                           url:'<?php echo base_url(); ?>index.php?/controller_admin/bulk_customer',
               data:form_data,
                           contentType:false,
                           processData: false,
                           cache:false,
                           success: function (data) {
							   $('#Searching_Modal').modal('hide');
                                   if(data=="2"){
               swal({
               title: 'All Fields are Mandatory',
               type: "error",
               showCancelButton: false,
               confirmButtonClass: "btn-danger",
               confirmButtonText: "Ok!",
               cancelButtonText: "No, cancel plx!",
               closeOnConfirm: false,
               closeOnCancel: false
               },
               function(isConfirm) {
               if (isConfirm) {
               window.location.reload();
               }
               });
               /* $('#rowdata').append('All Fields are Mandatory');
               $('#sla_modal').animate({ scrollTop: 0 });
               $('.error').show(); */
               }
							   else{
               var data=JSON.parse(data);
               $('#myModal').modal('hide');
               $('#sla_confirm').modal('show');
               for(i=0;i<data.length;i++){
			   //console.log(data[i]);
               $('#sla_detail_confirm').append(data[i]+"</br><hr>");
               }
               /* swal({
               title: data,
               type: "success",
               showCancelButton: false,
               confirmButtonClass: "btn-danger",
               confirmButtonText: "Ok!",
               cancelButtonText: "No, cancel plx!",
               closeOnConfirm: false,
               closeOnCancel: false
               },
               function(isConfirm) {
               if (isConfirm) {
               window.location.reload();
               }
               }); */
                     }

                           },
                       });
                   }
               }

               function view_contract(id){
               //alert(id);
               $.ajax({
                       url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_contract",
                       type        :   "POST",
                       data        :   {'id':id},// {action:'$funky'}
                       datatype    :   "JSON",
                       cache       :   false,
                       success     :   function(data){
               var data=JSON.parse(data);
                                           console.log(data);
               $('#tbody_contract').html('');
               if(data.length<1){
               swal("Oops!", "No Current Contracts Available.", "warning")
               }
               else {
               $('#modal_contract').modal('show');
               for(i=0;i<data.length;i++){
               $('#tbody_contract').append("<tr><td>"+data[i].contract_id+"</td><td>"+data[i].type_of_contract+"</td><td>"+data[i].contact_number+"</td><td>"+data[i].product_name+"</td><td>"+data[i].cat_name+"</td><td>"+data[i].model_no+"</td><td>"+data[i].serial_no+"</td><td>"+data[i].end_date+"</td></tr>");
               }
               }
               },
               });


               }
               function edit(id){
                   $.ajax({
                       url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_customer",
                       type        :   "POST",
                       data        :   {'id':id},// {action:'$funky'}
                       datatype    :   "JSON",
                       cache       :   false,
                       success     :   function(data){
                                           var data=JSON.parse(data);
                                           //console.log(data);
                                           $('#id1').val(data['a']);
                                           $('#cust_id1').val(data['customer_id']);
                                           $('#cust_name1').val(data['customer_name']);
                                           $('#email_id1').val(data['email_id']);
                                           $('#contact_no1').val(data['contact_number']);
                                           $('#acontact_no1').val(data['alternate_number']);
                                           $('#door_no1').val(data['door_num']);
                                           $('#street1').val(data['address']);
                                           $('#town1').val(data['cust_town']);
                                           $('#country1').val(data['cust_country']);
                                           $('#landmark1').val(data['base_location']);
                                           $('#pin_code1').val(data['pincode']);
                                       },
                   });
               $('#edits').modal('show');
               //alert(id);
               }
               function edit_details(id)
               {
               $.ajax({
                       url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_customer",
                       type        :   "POST",
                       data        :   {'id':id},// {action:'$funky'}
                       datatype    :   "JSON",
                       cache       :   false,
                       success     :   function(data){
                                           var data=JSON.parse(data);
                                           //console.log(data);
                                           $('#table_id').val(data['a']);
                                           $('#edit_id').val(data['customer_id']);
                                           $('#edit_name').val(data['customer_name']);
                                           $('#edit_mail').val(data['email_id']);
                                           $('#edit_contact').val(data['contact_number']);
                                           $('#edit_anumber').val(data['alternate_number']);
                                           $('#edit_door').val(data['door_num']);
                                           $('#edit_str').val(data['address']);
                                           $('#edit_town').val(data['cust_town']);
                                           $('#edit_country').val(data['cust_country']);
                                           $('#landmark1').val(data['base_location']);
                                           $('#edit_pin').val(data['pincode']);
                                       },
                   });
               $('#edit_info').modal('show');
               }
               
             
            
               function editbutton(){
                  if ($("#submit_form_1").valid())
        { 
               $('#rowdata_1').empty();
               var edit_id=$('#edit_id').val();
               var edit_name=$('#edit_name').val().trim();
               var edit_mail=$('#edit_mail').val().trim();
               var edit_contact=$('#edit_contact').val().trim();;
               var edit_door=$('#edit_door').val().trim();
               var edit_str=$('#edit_str').val().trim();
               var edit_town=$('#edit_town').val().trim();
               var edit_country=$('#edit_country').val().trim();
               var edit_pin=$('#edit_pin').val().trim();
               if(edit_door=="" || edit_id=="" || edit_mail=="" || edit_name=="" || edit_contact=="" || edit_str=="" || edit_town=="" || edit_country=="" || edit_pin==""){

               $('#rowdata_1').append('Fields are mandatory');
               $('#edit_info').animate({ scrollTop: 0 });
               $('.error').show();
               }
              else{

              var conNum = /^-?\d+\.?\d*$/; 
              if( !conNum.test(edit_contact) )   
                   {
                       swal("Please enter valid contact number");
                       return false;
                   }

               var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
               if( !emailReg.test(edit_mail) ) 
               {
                   swal('Please enter valid email');
               return false;
              }

               var pincode = /^[1-9][0-9]{5}$/;
                   if(!pincode.test(edit_pin)){
                       swal('Please enter valid pincode');
                       return false;
                   }
                   
               $.ajax({
               url         :   "<?php echo base_url(); ?>index.php?/controller_admin/submit_customer",
               type        :   "POST",
               data        :   $('#submit_form_1').serialize(),// {action:'$funky'}
               //datatype  :   "JSON",
               cache       :   false,
               success     :   function(data){
               //console.log(data);
               data=$.trim(data);
               if(data == "Customer Details Updated"){
               $('#edit_info').modal('hide');
               swal({
                 title: data,
                 type: "success",
                 showCancelButton: false,
                 confirmButtonClass: "btn-danger",
                 confirmButtonText: "Ok!",
                 cancelButtonText: "No, cancel plx!",
                 closeOnConfirm: false,
                 closeOnCancel: false
               },
               function(isConfirm) {
                 if (isConfirm) {
               	window.location.reload();
                 }
               });
               }
               else{
               $('#rowdata_1').append(data);
               $('#edit_info').animate({ scrollTop: 0 });
               $('.error').show();
               }
               },
               });
               }
}
else
{
   return false;
}
               }
               $('#form_wizard_1 .button-submit').click(function (){
               $('#rowdata_1').empty();
                   $.ajax({
                       url         :   "<?php echo base_url(); ?>index.php?/controller_admin/edit_customer",
                       type        :   "POST",
                       data        :   $('#submit_form').serialize(),// {action:'$funky'}
                       //datatype  :   "JSON",
                       cache       :   false,
                       success     :   function(data){
                                           //console.log(data);
                                           data=$.trim(data);
                                           if(data == "customer updated Successfully"){
                                           	$.dialogbox({
               type:'msg',
               content:data,
               closeBtn:true,
               btn:['Ok.'],
               call:[
               	function(){
               		$.dialogbox.close();
               		window.location.reload();
               	}
               ]
               });
                                               /*bootbox.alert({
                                                   message: data,
                                                   callback: function () {
                                                       location.reload();
                                                   }
                                               }) */
                                           }else{
                                               $('#rowdata_1').append(data);
               $('#edits').animate({ scrollTop: 0 });
                                               $('.error').show();
                                           }
                                       },
                   });
               })

               function deletes(id){
               	swal({
                 title: "Are you sure?",
                 text: "Customer Details will be permanently Deleted!",
                 showCancelButton: true,
                 confirmButtonClass: "btn-danger",
                 confirmButtonText: "Yes, Delete",
                 cancelButtonText: "No, Cancel",
                 closeOnConfirm: false,
                 closeOnCancel: false
               },
               function(isConfirm) {
                 if (isConfirm) {
               	  $.ajax({
                               url         :   "<?php echo base_url(); ?>index.php?/controller_admin/deletecustomer",
                               type        :   "POST",
                               data        :   {'id':id},// {action:'$funky'}
                               //datatype  :   "JSON",
                               cache       :   false,
                               success     :   function(data){
                                                   if(data == 1){
                swal({
                 title: "Deleted!",
                 text: "Customer data deleted Successfully.",
                 type: "success",
                 //showCancelButton: true,
                 confirmButtonClass: "btn-primary",
                 confirmButtonText: "Ok!",
                 closeOnConfirm: false,
               },
               function(isConfirm) {
               if (isConfirm) {
               	swal.close();
               	window.location.reload();
               }
               });
                                                   }
               else{
                                                 	  swal("Cancelled", "Your Customer has not been deleted Successfully:)", "error");
                // swal.close();
                                                   }
                               				}
                               })
                 }else{
                     	 // swal("Cancelled", "Your Customer has not been deleted", "error");
               swal.close();
                     }
               });

               }
            </script>
         </body>
      </html>
