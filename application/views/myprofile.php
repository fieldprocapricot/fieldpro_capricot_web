<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head><meta http-equiv="Content-Type" content="text/html; charset=WINDOWS-1252">
            
            <?php 
include 'assets/lib/cssscript.php'?>
         </head>
         <!-- END HEAD -->
         <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header.php"?>
               <!-- END HEADER -->
               <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/admin_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">

                     <!-- BEGIN PAGE BASE CONTENT -->
                     <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN PROFILE SIDEBAR -->
                                <div class="col-md-3">
                                    <!-- PORTLET MAIN -->
                                    <div class="portlet light profile-sidebar-portlet text-center">
                                        <!-- SIDEBAR USERPIC -->
                                        <div class="profile-userpic profile_img">
                                            <img src="<?php echo base_url() ?>/assets/pages/media/profile/profile_image.jpeg" class="img-responsive" alt=""> </div>
                                        <!-- END SIDEBAR USERPIC -->
                                        <!-- SIDEBAR USER TITLE -->
                                        <div class="profile-data">
                                            <div class="profile-usertitle-name"> <?php echo $this->session->userdata('username');?> </div>
                                            <div class="profile-usertitle-job">  <?php echo $this->session->userdata('role'); ?>  </div>
                                        </div>
                                        <!-- END SIDEBAR USER TITLE -->
                                      
                                    </div>
                                    <!-- END PORTLET MAIN -->
                                </div>
                                <!-- END BEGIN PROFILE SIDEBAR -->
                                <!-- BEGIN PROFILE CONTENT -->
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light ">
                                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="portlet-body">
                                                    <form role="form" action="#" class="form-horizontal">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Role</label>
																	<div class="col-md-9">
                                                                    <input type="text" placeholder="Marcus" class="form-control" value=" <?php echo $this->session->userdata('role'); ?>"/> </div></div>
                                                                
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Contact Number</label>
																	<div class="col-md-9">
                                                                    <input type="text" placeholder="+91 9500680090" class="form-control" /> </div></div>
                                                               
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Email</label>
																	<div class="col-md-9">
                                                                    <input type="mail" placeholder="demo@gmail.com" class="form-control" value="<?php echo $this->session->userdata('session_username');?>"/> 
																	</div></div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">About</label>
																	<div class="col-md-9">
                                                                    <textarea class="form-control" rows="3" placeholder=" "></textarea></div>
                                                                </div>
                                                               
                                                                <div class="pull-right">
                                                                    <a href="javascript:;" class="btn green"> Save Changes </a>
                                                                </div>
                                                            </form>
															<span class="clearfix"></span><br>
													
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PROFILE CONTENT -->
                            </div>
                        </div>
                     
                     <!-- END PAGE BASE CONTENT -->
                  </div>
                  
               </div>
			   </div>
			   <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
            </div>
            <!-- END CONTAINER -->         
            <?php include 'assets/lib/javascript.php'?>  

 
   </body>
</html>