<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php include 'assets/lib/cssscript.php'?>
            <style>
               <!-- .sweet-alert.showSweetAlert.visible{
                  z-index:99999999 !important;
                  }-->
               .fileinput-new, .fileinput-exists {
                 color: #000;
               }
               .sweet-alert.showSweetAlert.visible{
                 border:1px solid red;
               }
               .dt-buttons{
                 display:none !important;
               }
               /* .dataTables_filter
               {
                 text-align-last: right;
               } */
                 .uneditable-input {
min-width:auto !important;
}
.form-control.uneditable-input.input-fixed {
    max-width: 233px !important;
}
@media screen and (max-width: 540px) {
.form-control.uneditable-input.input-fixed {
    max-width: 163px !important;
}
}
input .control-label .required, .form-group .required {
    color: black;
}
span .required
{
    color: red;
}
span.help-block-error {
    color: red !important;
}
            </style>
         </head>
         <!-- END HEAD -->
          <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header.php"?>
               <!-- END HEADER -->
               <div class="page-container">
               <div class="page-sidebar-wrapper">
               <?php include "assets/lib/admin_sidebar.php"?>
				   
				   
               </div>
               <div class="page-content-wrapper">
                  <div class="page-content">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">
                                    <?php echo $webPageheading; ?>
                                    </div>
                                    <div class="actions">
	<a href="<?php echo base_url();?>index.php?/controller_admin/user" class='btn btn-circle blue btn-outline btn-sm'><i class='fa fa-backward' aria-hidden='true'>Back</i></a>
                                    <div class="btn-group">

                               <button id="sample_editable_1_new" class="btn btn-circle green btn-outline" onClick="create_details()"> Add & Edit <?php echo $webPageheading; ?>
                                                <i class="fa fa-plus"></i>
                                                </button>
                                             </div>
                                     </div>
                                 </div>
                                 <div class="portlet-body">
                                    
                                    <div class="table=responsive">
										
                           	<table class='table table-striped table-bordered table-hover load_data' id='clienttable'>
                                                    <thead> 
                                                        
                                                        <tr>
                                                            <th class="text-center">S.No</th>
                                                            <th class="text-center">Product</th>
															 <th class="text-center">Sub Category</th>
															 <th class="text-center">Skill Level</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                 
                                                    <tbody align="center">
                                                     
                      
                    
                                                    </tbody>
                                                </table>
                                    </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>


<div id="servicegroupform" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id="servicegroupmodaltitle"></span></h4>
      </div>
     
         <form class="form-horizontal" action="javascript:void(0);" id="submit_form" method="POST">
             <div class="modal-body">
                 <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="form-group">
                       <label class="col-md-6">Product
                             <span class="required"> * </span>
                                </label>
                        <div class="col-md-6 col-sm-6">
							 <input type="hidden" class="form-control" name="tech_list_id" id="tech_list_id" value="0" />
							 <input type="hidden" class="form-control" name="employee_id" id="employee_id" value="<?php echo $employee_id; ?>" />
					 <input type="hidden" class="form-control" name="employee_name" id="employee_name" value='<?php echo $employee_name; ?>' />
			<select name='product_id' onchange='load_subcategory(this.value);' id='product_id' class="form-control form-control1" required="" aria-required="true" >
				<option value=''>Select product</option>
				<?php foreach ($product_list->result() as $row) {?>
				<option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option>
				<?php } ?>
							</select>
                        </div>
						
                        </div>
                        </div>
			    <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="form-group">
                       <label class="col-md-6">Sub Category
                             <span class="required"> * </span>
                                </label>
                        <div class="col-md-6 col-sm-6">
                        <select name='subcategory' id='subcategory' class='form-control form-control1' aria-hidden='true' required='' >
							<option value=''>Select Sub Category</option></select>
                        </div>
                        </div>
                        </div>
          	    <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="form-group">
                       <label class="col-md-6">Skill Level
                             <span class="required"> * </span>
                                </label>
                        <div class="col-md-6 col-sm-6">
                        <select name='skill_level' id='skill_level' class='form-control form-control1' aria-hidden='true' required='' >
							<option value=''>Select Skill Level</option>
							<option value='L1'>L1</option>
							<option value='L2'>L2</option>
							<option value='L3'>L3</option>
							<option value='L4'>L4</option>
							</select>
                        </div>
                        </div>
                        </div>
           <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="form-group">
                       <label class="col-md-6">Work Type
                             <span class="required"> * </span>
                                </label>
                        <div class="col-md-6 col-sm-6">
			<select name='work_type' id='work_type' class="form-control form-control1" required="" aria-required="true" >
				<option value=''>Select Work Type</option>
				<?php foreach ($servicegroup->result() as $row) {?>
				<option value='<?php echo $row->service_group_id; ?>'><?php echo $row->service_group; ?></option>
				<?php } ?>
							</select>
                        </div>
						
                        </div>
                        </div>
			
            </div>
            </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-circle green btn-outline">Submit</button>
        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal">Cancel</button>
      </div>
        </form>
      
    </div>

  </div>
</div>
                     <!-- END PAGE BASE CONTENT -->
                        <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                  </div>
            <!--Modal Starts-->
            <!-- Modal -->
           
           
            <!-- Modal -->
           
            <!--Modal End-->
          
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>
            <script>
				
			var deleteRow; 
		$(document).ready(function(){
	//alert("hello");

        var table = $('#clienttable');
      table.dataTable({
		  autoWidth: false,
          bSort : false,
	 "ajax": {
	
            url : "<?php echo base_url();?>index.php/product_subcategory/getdetails_productcategory/<?php echo $employee_id; ?>",
           "type": "POST",
        "data": function(d){
	
       //     d.custno = $('#custno').val();
    	//	d.email = $('#custemail').val();
        }
        },
		  
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

		// dom: 'lBfrtip',
              buttons: [
                { extend: 'print', className: 'btn btn-outline' },
               
                { extend: 'pdf', className: 'btn btn-outline' },
                { extend: 'excel', className: 'btn btn-outline ' }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,
            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
		    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable



       
        });

        // handle datatable custom tools
       /* $('#sample_3_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });*/
	
		/* $('#formbtn').click(function() {
		 //alert("Hi");
	
        var oTable= table.DataTable().ajax.reload();
    });
	*/
		$('.panel-close').click(function(){
    $('.custom-content').magnificPopup('close');
 });
			
		
 });
			
                   
            function load_subcategory(product_id)
            {
                 
           
            var product_id = product_id;
        // alert(product_id);
            $.ajax({
               url         :  "<?php echo base_url();?>index.php?/controller_admin/getcategory",
               type     :  "POST",
               data     :  {'product_id' : product_id},
               datatype :  "JSON",
               cache    :  false,
               process     :  false,
               success     :  function(data){                             
                              var data=JSON.parse(data);
                              if(data == 1){
                                    swal({
                                  title: "No Category are entered, kindly add category to this product",
                                  type: "warning",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    swal.close();
                                  }
                                });
                              }else{
                                     
                                $('#subcategory').html('');
                                 for(i=0; i<data.length;i++){                                   
                                    $('#subcategory').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                 }
                              }
                           },
            })
        }
   
				
			 var formname = "#submit_form";
               $(formname).validate({
    doNotHideMessage:!0,errorElement:"span",errorClass:"help-block help-block-error",focusInvalid:!1,
    rules: {
     //   cate_desc:{required:!0},
  
    },
    messages: {                                     
          
          
    
}
});	

               $( document).on( "submit", "#submit_form", function(e) {
   // alert('submit');
				    var table = $('#clienttable');
     
       if ($(formname).valid())
        { 
        $('#servicegroupform button[type="submit"]').attr("disabled","disabled");
        //var  actionUrl = $(formname).attr('action');
        //  alert("hi");
        $(formname+" button").attr("disabled","disabled");
    //    var  actionUrl = "<?php echo base_url(); ?>index.php/travelhistory/travelhistoryInsert";
        e.preventDefault();
			var tech_list_id=$('#tech_list_id').val();
            var employee_id=$('#employee_id').val();
            var employee_name=$('#employee_name').val();
			 var product_id=$('#product_id').val();
			 var subcategory=$('#subcategory').val();
			 var skill_level=$('#skill_level').val();
			 var work_type=$('#work_type').val();
            $.ajax({
                       url         :   "<?php echo base_url();?>index.php/product_subcategory/add_tech_product_category",
                       type        :   "POST",
                       data        :   {'tech_list_id' : tech_list_id,'employee_id' : employee_id,'employee_name':employee_name,'product_id':product_id,'subcategory':subcategory,'skill_level':skill_level,'work_type':work_type},
                     //  datatype    :   "JSON",
                       cache       :   false,
                      // process     :   false,
                       success     :   function(data){
                                          // var data=JSON.parse(data);
                                          $(formname+" button").removeAttr("disabled");
                                         var obj = JSON.parse(data);
                                         // console.log(obj);
                                         // console.log(obj['message']);
                                         // console.log(obj.message);
                                        // console.log(obj[0]['message']);
                                        // console.log(obj[0].message);
                                          $('#servicegroupform').modal('hide');
                                           $('#product_id').val('');
						   				   $('#subcategory').html('');	
						                    $('#subcategory').append('<option value="">Select Sub Category</option>');
						  				  $('#skill_level').val('');	
						   				 $('#work_type').val('');		
                      alert(obj['msg']);
                                        /*  $.dialogbox({
               type:'msg',
               content:'cvhdfhd',
               closeBtn:true,
               btn:['Ok.'],
               call:[
                function(){
                    $.dialogbox.close();
                   
                }
               ]
               });*/
                                         var oTable= table.DataTable().ajax.reload();
                                       },
                   })            
        }
        else
        {               
            return false;
        }
    
      });
              
               function create_details()
                   {
                   
                    $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
});     
                   
                     $('#servicegroupform').modal('show');
                     $('#servicegroupid').val(0);
                      $('#servicegroupmodaltitle').html('Add & Edit <?php echo $webPageheading; ?>');
                   }

	function edit_details(id)
                   {
var cate_id='';					   
var cate_desc='';
var cate_type='';					   
                    $.ajax({
                       url         :   "<?php echo base_url();?>index.php/service_category/edit_category",
                       type        :   "POST",
                       data        :   {'cate_id' : id},
                       datatype    :   "JSON",
                       cache       :   false,
                      // process     :   false,
                       success     :   function(data){
                                          // var data=JSON.parse(data);
                                         var obj = JSON.parse(data);
                                         console.log(obj);
                                        // console.log(obj[0]['service_group']);
                                        console.log(obj['scid']);
						   			 cate_id=obj['scid'];
                                    cate_desc=obj['description'];
						   			cate_type=obj['cate_type'];
                                    console.log(name);
                                     $('#servicegroupmodaltitle').html('Edit <?php echo $webPageheading; ?>');
						   			 $('#cate_id').val(cate_id);	
                                    $('#cate_desc').val(cate_desc);
						            $('#cate_type').val(cate_type);
                                        // var oTable= table.DataTable().ajax.reload();
                                       },
                   }) 

                    $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
});
console.log(name);
                     $('#servicegroupform').modal('show');
                     $('#servicegroupid').val(id);
                     
                   }
    function delete_details(id){
						 var table = $('#clienttable');
               swal({
  title: "Are you sure to delete?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){
$.ajax({
                       url         :   "<?php echo base_url();?>index.php?/product_subcategory/delete_productcategory",
                       type        :   "POST",
                       data        :   {'tech_list_id' : id},
                     //  datatype    :   "JSON",
                       cache       :   false,
                      // process     :   false,
                       success     :   function(data){
                                          // var data=JSON.parse(data);
                                          $(formname+" button").removeAttr("disabled");
                                         console.log(data);
                                         var obj = JSON.parse(data);
                                         console.log(obj);
                                     if(obj['status']=='true')
                                     {
                                        swal("Deleted!", data['msg'], "success");
                                     }
                                     else
                                     {
                                        swal("Not Deleted!", data['msg'], "error");
                                     }

                                         var oTable= table.DataTable().ajax.reload();
                                       },
                   }) 

  
});

               }
            </script>
           
         </body>
      </html>