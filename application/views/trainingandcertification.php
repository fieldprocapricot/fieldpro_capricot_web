<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
        <?php include 'assets/lib/cssscript.php'?>
		<style>
			.jstree-anchor{
				min-height:110px !important;
			}
			.jstree-icon.jstree-themeicon.jstree-themeicon-custom{
				height:100px;
				width:150px;
			}
			.jstree-default .jstree-anchor {
				//line-height: 6 !important;
				margin-bottom:6%;
				text-align: center;
			}
			.jstree-icon.jstree-themeicon.jstree-themeicon-custom {
				margin: 8px 0px !important;
			}
			.jstree-node{
				margin:3px 0px;
			}
			#tree_5{
				padding:2%;
			}
			.jstree-children{
				margin:0% 2%;
			}
			.jstree-icon.jstree-themeicon.jstree-themeicon-custom{
				display:block !important;
			}
			.dt-buttons {
				display: none !important;
			}
			.sweet-alert.showSweetAlert.visible {
				z-index: 999999999 !important;
				border:1px solid red;
			}
			.fileinput-new{
				color:#000 !important;
			}
		  /* .dataTables_filter
		  {
			text-align-last: right;
		  } */
		
		  .paging_bootstrap_number{
			  float:right;
		   }
		   .pdfobject-container { height: 500px;}
 .uneditable-input {
min-width:auto !important; 
}
.form-control.uneditable-input.input-fixed {
    max-width: 233px !important;
}
@media screen and (max-width: 540px) {
.form-control.uneditable-input.input-fixed {
    max-width: 163px !important;
}
}
		</style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
	<!-- BEGIN CONTAINER -->
	<div class="page-wrapper">
		<!-- BEGIN HEADER -->
            <?php include "assets/lib/header.php"?>
            <!-- END HEADER -->
		<div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/admin_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet box dark">
								<div class="portlet-title">
									<div class="caption">Content Management</div>
									<ul class="nav nav-tabs">
												<li class="active"><a href="#portlet_tab2_1"
													data-toggle="tab" aria-expanded="true">Upload 
														Content </a></li>
												<li><a href="#portlet_tab2_2" data-toggle="tab">Assessment
														Quiz</a></li>
											</ul>
								</div>
								<div class="portlet-body">
											<div class="tab-content">
												<div class="tab-pane active" id="portlet_tab2_1">
													<!-- <div class="btn-group margin-bottom-10">
	                                                    	<button class="btn btn-circle blue btn-outline" onclick="training_check()"><i class="fa fa-plus"></i> Add Training</button>
	                                                     </div>
	                                                    <div class="btn-group margin-bottom-10">
	                                                    	<button class="btn btn-circle green btn-outline"  data-toggle="modal" data-target="#add_contents" ><i class="fa fa-plus"></i> Add Training content</button>
	                                                    </div>    -->
													<div id="div_align">
															<div class="btn-group margin-bottom-10 pull-right">
																<button class="btn btn-circle blue btn-outline"
																	onclick="training_check()">
																	<i class="fa fa-plus"></i> Create Content
																</button>
															</div><span class="clearfix"></span>
															<!--<div class="btn-group margin-bottom-10">
																<button class="btn btn-circle green btn-outline"  data-toggle="modal" data-target="#add_contents" ><i class="fa fa-plus"></i> Add Training content</button>
															</div>-->
                                                                                                                   
                                                                       <div class="table=responsive">
                                                                                    <table class="table table-hover table-bordered sample_2" id="">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th class="text-center">Training Name</th>
                                                                                                <th class="text-center">Training Type</th>
                                                                                                <th class="text-center">Product Category</th>
                                                                                                <th class="text-center">Sub Category</th>
                                                                                                <th class="text-center">Content</th>
                                                                                                <th class="text-center">Action</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody align="center">
                                                                                            <?php
                                                                                                foreach ( $r->result () as $row ) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="text-center"><?php echo $row->title; ?></td>
                                                                                                <td class="text-center"><?php echo $row->product_type; ?></td>
                                                                                                <td class="text-center"><?php echo $row->product_name; ?></td>
                                                                                                <td class="text-center"><?php echo $row->cat_name; ?></td>
                                                                                                <?php if($row->type == 'pdf'){
                                                                                                    ?>
                                                                                                    <td class="text-center pdf-icon">
                                                                                                    <a class="" id="<?php echo $row->c; ?>" onClick="view(this.id)">
                                                                                                            <i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i>
                                                                                                        </a>
                                                                                                    </td>
                                                                                                    <?php }else{?>
                                                                                                        <td class="text-center pdf-icon">
                                                                                                        <a class="" id="<?php echo $row->c; ?>" onClick="view(this.id)">
                                                                                                            <i class="fa fa-video-camera fa-2x" aria-hidden="true"></i>
                                                                                                        </a>
                                                                                                        </td>
                                                                                                    <?php }
                                                                                                ?>
                                                                                                <td class="text-center">
                                                                                                    <button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row->b; ?>" onClick="edit_training(this.id)"><i class="fa fa-edit"></i></button>
                                                                                                    <button class="btn btn-circle red btn-outline btn-icon-only" id="<?php echo $row->b; ?>" onClick="delete_trai(this.id)"><i class="fa fa-trash"></i></button>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <?php
                                                                                                }
                                                                                            ?>
                                                                                            <?php
                                                                                                foreach ( $reco->result () as $rows ) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td><?php echo $rows->title; ?></td>
                                                                                                <td><?php echo $rows->product_type; ?></td>
                                                                                                <td>-</td>
                                                                                                <td>-</td>
                                                                                                <?php if($rows->type == 'pdf'){
                                                                                                    ?>
                                                                                                    <td class="text-center pdf-icon">
                                                                                                    <a class="" id="<?php echo $rows->c; ?>" onClick="view(this.id)">
                                                                                                            <i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i>
                                                                                                        </a>
                                                                                                    </td>
                                                                                                    <?php }else{?>
                                                                                                        <td class="text-center pdf-icon">
                                                                                                        <a class="" id="<?php echo $rows->c; ?>" onClick="view(this.id)">
                                                                                                            <i class="fa fa-video-camera fa-2x" aria-hidden="true"></i>
                                                                                                        </a>
                                                                                                        </td>
                                                                                                    <?php } 
                                                                                                ?>
                                                                                                <td class="text-center">
                                                                                                    <button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $rows->a; ?>" onClick="edit_training(this.id)"><i class="fa fa-edit"></i></button>
                                                                                                    <button class="btn btn-circle red btn-outline btn-icon-only" id="<?php echo $rows->a; ?>" onClick="delete_trai(this.id)"><i class="fa fa-trash"></i></button>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <?php
                                                                                                }
                                                                                            ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>                                                    
													</div>
												</div>
												<div class="tab-pane" id="portlet_tab2_2">
													<div class="btn-group margin-bottom-10 pull-right">
														<button class="btn btn-circle blue btn-outline"
															onclick="certificate_check()">
															<i class="fa fa-plus"></i> Create Assessment
														</button>
													</div><span class="clearfix"></span>
													<div class="table=responsive">
														<table class="table table-hover table-bordered sample_2" id="">
															<thead>
																<tr>
																	<th class="text-center">Assessment Name</th>
																	<th class="text-center">Product Category</th>
																	<th class="text-center">Sub Category</th>
																	<th class="text-center">Action</th>
																</tr>
															</thead>
															<tbody align="center">
																<?php
																foreach ( $record->result () as $row ) {
																		?>
																	<tr>
																	<td><?php echo $row->certificate_name; ?></td>
																	<td>-</td>
																	<td>-</td>
																	<td class="text-center">
																		<!-- <button class="btn btn-circle blue btn-outline" id="<?php echo $row->a; ?>" onClick="edit_assessment(this.id)">edit</button> -->
																		<a id="<?php echo $row->a; ?>"
																		class="btn btn-circle blue btn-outline btn-icon-only"
																		href="<?php echo site_url('controller_admin/edits_certificates/?assessment='.$row->a);?>"><i class="fa fa-edit"></i></a>
																		<button class="btn btn-circle red btn-outline btn-icon-only"
																			id="<?php echo $row->a; ?>"
																			onClick="delete_assessment(this.id)"><i class="fa fa-times"></i></button>
																	</td>
																</tr>
				                                                    <?php } ?>
																	<?php
																	foreach ( $cer->result () as $row ) {
																			?>
																		<tr>
																	<td><?php echo $row->certificate_name; ?></td>
																	<td><?php echo $row->product_name; ?></td>
																	<td><?php echo $row->cat_name; ?></td>
																	<td class="text-center">
																		<!-- <button class="btn btn-circle blue btn-outline" id="<?php echo $row->a; ?>" onClick="edit_assessment(this.id)">edit</button> -->
																		<a id="<?php echo $row->a; ?>"
																		class="btn btn-circle blue btn-outline btn-icon-only"
																		href="<?php echo site_url('controller_admin/edits_certificates/?assessment='.$row->a);?>"><i class="fa fa-edit"></i></a>
																		<button class="btn btn-circle red btn-outline btn-icon-only"
																			id="<?php echo $row->a; ?>"
																			onClick="delete_assessment(this.id)"><i class="fa fa-times"></i></button>
																	</td>
																</tr>
				                                                    <?php } ?>
				                                                </tbody>
														</table>
													</div>
												</div>
											</div>
								</div>
							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
				</div>
				<!-- END PAGE BASE CONTENT -->
				<!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
			</div>


	<!--Modal Starts-->
	<!-- Modal -->
	<div id="training_1" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Training</h4>
					<div class="error" style="display: none">
						<label id="rowdata_t"></label>
					</div>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="insert_trai">
						<div class="form-group" style="display: none">
							<label class="control-label col-sm-4" for="email">company Id:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="company_id"
									name="company_id"
									value="<?php echo $this->session->userdata('companyid');?>"
									readonly>
							</div>
						</div>
						<div class="form-group" style='display:none'>
							<label class="control-label col-sm-4" for="email">Training ID:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="training_id"
									name="training_id" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Title:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="title_training"
									name="title_training" placeholder="Training Title">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Training Type:</label>
							<div class="col-sm-9">
								<select class="form-control" id="product_type"
									name="product_type">
									<option selected disabled>Select Training Type</option>
									<option value="generic">Generic Training</option>
									<option value="technology">Technology Training</option>
									<option value="product">Product Training</option>
								</select>
							</div>
						</div>
						<div class="form-group product_change">
							<label class="control-label col-sm-3" for="email">Product Category:</label>
							<div class="col-sm-9">
								<select class="form-control" id="product_training"
									name="product_training">
									<option selected disabled>Select Product Category</option>
                                    <?php
                                        foreach ( $records->result() as $row ) { ?>
                                            <option value="<?php echo $row->product_id; ?>"><?php echo $row->product_name; ?></option>
                                    <?php } ?>
                                </select>
							</div>
						</div>
						<div class="form-group product_change">
							<label class="control-label col-sm-3" for="email">Sub Category:</label>
							<div class="col-sm-9">
								<select class="form-control" id="category_training"
									name="category_training">
									<option selected disabled>Select Sub category</option>
								</select>
							</div>
						</div>
						<div class="form-group product_change">
							<label class="control-label col-sm-3" for="email">Model Name:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="model_name"
									name="model_name" placeholder="Model Name">
							</div>
						</div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Content Type:</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="content_type"
                                    name="content_type">
                                    <option selected="" disabled="">Select Content Type</option>
                                    <option value="pdf">PDF</option>
                                    <option value="video">VIDEO</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Upload content:</label>
                            <div class="col-sm-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group input-large">
                                        <div
                                            class="form-control uneditable-input input-fixed"
                                            data-trigger="fileinput">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                class="fileinput-filename"> </span>
                                        </div>
                                        <span class="input-group-addon btn default btn-file"> <span
                                            class="fileinput-new"> Select file </span> <span
                                            class="fileinput-exists"> Change </span> <input type="file"
                                            name="upload_con" id="upload_con">
                                        </span> <a href="javascript:;"
                                            class="input-group-addon btn red fileinput-exists"
                                            data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                                <!-- <input type="file" class="form-control" id="upload_content" name="upload_content" placeholder=""> -->
                            </div>
                        </div>
						<!--<div class="form-group">
                                                <label class="control-label col-sm-4" for="email">Reference Assestment:</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control" id="certificates_training" name="certificates_training">
                                                        <option selected disabled>Select Assestment</option>
                                                        <?php
																																																								foreach ( $record->result () as $row ) {
																																																									?>
                                                                <option value="<?php echo $row->certificate_id; ?>"><?php echo $row->certificate_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div> -->
						<!--<div class="form-group">
                                                <label class="control-label col-sm-4" for="email">Upload file:</label>
                                                <div class="col-sm-8">
                                                    <input type="file" class="form-control" id="userfile" name="userfile" placeholder="">
                                                </div>
                                            </div>-->
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-circle green btn-outline"
						id="addtraining_detail">
						<i class="fa fa-check"></i> Submit
					</button>
					<button type="button" class="btn btn-circle red btn-outline"
						data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
				</div>
			</div>
		</div>
	</div>
	<div id="edits_training" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Training</h4>
					<div class="error" style="display: none">
						<label id="rowdata_1"></label>
					</div>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="edits_trai">
						<div class="form-group" style="display: none">
							<label class="control-label col-sm-4" for="email">company Id:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="company_id_1"
									name="company_id_1"
									value="<?php echo $this->session->userdata('companyid');?>"
									readonly>
							</div>
						</div>
						<div class="form-group" style="display: none">
							<label class="control-label col-sm-4" for="email">Id:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="ids" name="ids"
									readonly>
							</div>
						</div>
						<div class="form-group" style='display:none'>
							<label class="control-label col-sm-3" for="email">Training ID:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="training_id1"
									name="training_id1" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Title:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="title_training1"
									name="title_training1" placeholder="Training Title">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Training Type:</label>
							<div class="col-sm-9">
								<select class="form-control" id="product_type1"
									name="product_type1">
									<option selected disabled>Select Training Type</option>
									<option value="generic">Generic Training</option>
									<option value="technology">Technology Training</option>
									<option value="product">Product Training</option>
								</select>
							</div>
						</div>
						<div class="form-group product_change">
							<label class="control-label col-sm-3" for="email">Product Category:</label>
							<div class="col-sm-9">
								<select class="form-control" id="product_training1"
									name="product_training1">
									<option selected disabled>Select Product</option>
                                                        <?php
																																																								foreach ( $records->result () as $row ) {
																																																									?>
                                                                <option
										value="<?php echo $row->product_id; ?>"><?php echo $row->product_name; ?></option>
                                                        <?php } ?>
                                                    </select>
							</div>
						</div>
						<div class="form-group product_change">
							<label class="control-label col-sm-3" for="email">Sub Category:</label>
							<div class="col-sm-9">
								<select class="form-control" id="category_training1"
									name="category_training1">
									<option selected disabled>Select Sub category</option>
								</select>
							</div>
						</div>
						<div class="form-group product_change">
							<label class="control-label col-sm-3" for="email">Model Name:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="model_name1"
									name="model_name1" placeholder="Model Name">
							</div>
						</div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Content Type:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="content_type_2"
                                    name="content_type_2" placeholder="Model Name" style="display:none">
                                <select class="form-control" id="content_type_1"
                                    name="content_type_1">
                                    <option selected="" disabled="">Select Content Type</option>
                                    <option value="pdf">PDF</option>
                                    <option value="video">VIDEO</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Upload content:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="edit_content_2"
                                    name="edit_content_2" style="display:none" >
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group input-large">
                                        <div
                                            class="form-control uneditable-input input-fixed"
                                            data-trigger="fileinput">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                class="fileinput-filename"> </span>
                                        </div>
                                        <span class="input-group-addon btn default btn-file"> <span
                                            class="fileinput-new"> Select file </span> <span
                                            class="fileinput-exists"> Change </span> <input type="file"
                                            name="upload_con_1" id="upload_con_1">
                                        </span> <a href="javascript:;"
                                            class="input-group-addon btn red fileinput-exists"
                                            data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                                <!-- <input type="file" class="form-control" id="upload_content" name="upload_content" placeholder=""> -->
                            </div>
                        </div>
						<!--<div class="form-group">
                                                <label class="control-label col-sm-4" for="email">Reference Assestment:</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control" id="certificates_training" name="certificates_training">
                                                        <option selected disabled>Select Assestment</option>
                                                        <?php
																																																								foreach ( $record->result () as $row ) {
																																																									?>
                                                                <option value="<?php echo $row->certificate_id; ?>"><?php echo $row->certificate_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div> -->
						<!--<div class="form-group">
                                                <label class="control-label col-sm-4" for="email">Upload file:</label>
                                                <div class="col-sm-8">
                                                    <input type="file" class="form-control" id="userfile" name="userfile" placeholder="">
                                                </div>
                                            </div>-->
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-circle green btn-outline"
						id="edittraining_d">
						<i class="fa fa-check"></i> Submit
					</button>
					<button type="button" class="btn btn-circle red btn-outline"
						data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
				</div>
			</div>
		</div>
	</div>

	<div id="edit_contents" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Training Content</h4>
					<div class="error" style="display: none">
						<label id="rowdata_content_1"></label>
					</div>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="edit_trai_content">
						<div class="form-group" style="display: none">
							<label class="control-label col-sm-3" for="email">id:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="id_1" name="id_1">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Title:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="training_titles1"
									name="training_titles1">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Training name:</label>
							<div class="col-sm-9">
								<select class="form-control" id="training_select1"
									name="training_select1">
									<option value="" selected disabled>Select Training</option>
												<?php
												foreach ( $rec->result () as $row ) {
													?>
														<option value="<?php echo $row->training_id; ?>"><?php echo $row->title; ?></option>
												<?php } ?>
											</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Content Type:</label>
							<div class="col-sm-9">
								<select class="form-control" id="content_type1"
									name="content_type1">
									<option selected="" disabled="">Select Content Type</option>
									<option value="pdf">PDF</option>
									<option value="video">VIDEO</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Upload content:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="edit_content2"
									name="edit_content2" style="display: none" />
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="input-group input-large">
										<div
											class="form-control uneditable-input input-fixed input-medium"
											data-trigger="fileinput">
											<i class="fa fa-file fileinput-exists"></i>&nbsp; <span
												class="fileinput-filename"> </span>
										</div>
										<span class="input-group-addon btn default btn-file"> <span
											class="fileinput-new"> Select file </span> <span
											class="fileinput-exists"> Change </span> <input type="file"
											name="edit_content1" id="edit_content1">
										</span> <a href="javascript:;"
											class="input-group-addon btn red fileinput-exists"
											data-dismiss="fileinput"> Remove </a>
									</div>
								</div>
								<!-- <input type="file" class="form-control" id="upload_content" name="upload_content" placeholder=""> -->
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-circle blue btn-outline"
						id="edittraining_content">
						<i class="fa fa-check"></i> Submit
					</button>
					<button type="button" class="btn btn-circle red btn-outline"
						data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>

	<div id="add_contents" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Training Content</h4>
					<div class="error" style="display: none">
						<label id="rowdata_content"></label>
					</div>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="insert_trai_content">

						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Title:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="training_titles"
									name="training_titles">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Training name:</label>
							<div class="col-sm-9">
								<select class="form-control" id="training_select"
									name="training_select">
									<option value="" selected disabled>Select Training</option>
												<?php
												foreach ( $rec->result () as $row ) {
													?>
														<option value="<?php echo $row->training_id; ?>"><?php echo $row->title; ?></option>
												<?php } ?>
											</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Content Type:</label>
							<div class="col-sm-9">
								<select class="form-control" id="content_type"
									name="content_type">
									<option selected="" disabled="">Select Content Type</option>
									<option value="pdf">PDF</option>
									<option value="video">VIDEO</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Upload content:</label>
							<div class="col-sm-9">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="input-group input-large">
										<div
											class="form-control uneditable-input input-fixed input-medium"
											data-trigger="fileinput">
											<i class="fa fa-file fileinput-exists"></i>&nbsp; <span
												class="fileinput-filename"> </span>
										</div>
										<span class="input-group-addon btn default btn-file"> <span
											class="fileinput-new"> Select file </span> <span
											class="fileinput-exists"> Change </span> <input type="file"
											name="" id="">
										</span> <a href="javascript:;"
											class="input-group-addon btn red fileinput-exists"
											data-dismiss="fileinput"> Remove </a>
									</div>
								</div>
								<!-- <input type="file" class="form-control" id="upload_content" name="upload_content" placeholder=""> -->
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-circle blue btn-outline"
						id="addtraining_content">
						<i class="fa fa-check"></i> Submit
					</button>
					<button type="button" class="btn btn-circle red btn-outline"
						data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>


	<!--Modal End-->
	<!-- For Quiz -->
	<div id="mycertificate" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Assessment</h4>
					<div class="error" style="display: none">
						<label id="rowdata"></label>
					</div>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="add_certificate">
						<div class="form-group" style="display:none">
							<label class="control-label col-sm-4" for="email">company Id:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="com_id"
									name="com_id"
									value="<?php echo $this->session->userdata('companyid');?>"
									readonly>
							</div>
						</div>
						<div class="form-group" style="display:none">
							<label class="control-label col-sm-4" for="email">Assessment Id:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="assessment_id"
									name="assessment_id" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="email">Assessment
								Name:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="certproduct_name"
									name="certproduct_name" placeholder="Assestment Name">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="email">Assessment Type:</label>
							<div class="col-sm-8">
								<select class="form-control" id="assessment_type"
									name="assessment_type">
									<option selected disabled>Select Assessment Type</option>
									<option value="generic">Generic Assessment</option>
									<option value="technology">Technology Assessment</option>
									<option value="product">Product Assessment</option>
								</select>
							</div>
						</div>
						<div class="form-group assessment_change" style="display:none">
							<label class="control-label col-sm-4" for="email">Product
								Category:</label>
							<div class="col-sm-8">
								<select class="form-control" id="product_name"
									name="product_name">
									<option value="" selected disabled>Select Product Category</option>
                                                        <?php
																																																								foreach ( $records->result () as $row ) {
																																																									?>
                                                                <option
										value="<?php echo $row->product_id; ?>"><?php echo $row->product_name; ?></option>
                                                        <?php } ?>
                                                    </select>
							</div>
						</div>
						<div class="form-group assessment_change" style="display:none">
							<label class="control-label col-sm-4" for="email">Sub Category:</label>
							<div class="col-sm-8">
								<select class="form-control" id="category" name="category">
									<option selected disabled>Select Sub Category</option>
								</select>
							</div>
						</div>
						<div class="form-group" style="display:none">
							<label class="control-label col-sm-4" for="email">Title:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="title" name="title"
									placeholder="Title">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-circle green btn-outline"
						id="addcertificate">
						<i class="fa fa-check"></i> Submit
					</button>
					<button type="button" class="btn btn-circle red btn-outline"
						data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
				</div>
			</div>
		</div>
	</div>
	<div id="edit_assessment" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Update Assessment</h4>
					<div class="error" style="display: none">
						<label id="rowdata_1"></label>
					</div>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="edit_certificate">
						<div class="form-group" style="display: none">
							<label class="control-label col-sm-4" for="email">Id:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="id1" name="id1"
									readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="email">Assessment Id:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="certificate_id1"
									name="certificate_id1" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="email">Assessment
								Name:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="certificate_name1"
									name="certificate_name1" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="email">Product name:</label>
							<div class="col-sm-8">
								<select class="form-control" id="product_name1"
									name="product_name1">
                                                        <?php
																																																								foreach ( $records->result () as $row ) {
																																																									?>
                                                                <option
										value="<?php echo $row->product_id; ?>"><?php echo $row->product_name; ?></option>
                                                        <?php } ?>
                                                    </select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="email">Category name:</label>
							<div class="col-sm-8">
								<select class="form-control" id="category1" name="category1">
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="email">TItle:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="title1"
									name="title1" placeholder="Title">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary"
						onclick="submit_certificate()">
						<i class="fa fa-check"></i> Update
					</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
				</div>
			</div>
		</div>
	</div>
	<div id="myquiz" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Quiz</h4>
				</div>
				<div class="modal-body">
					<div class="error" style="display: none">
						<label id="rowdata"></label>
					</div>
					<form class="form-horizontal" id="upload_quiz">
						<!-- <div class="form-group">
	                                                <label class="control-label col-sm-4" for="email">Assessment Id:</label>
	                                                <div class="col-sm-8">
	                                                    <input type="text" class="form-control" id="quiz_id" name="quiz_id" readonly>
	                                                </div>
	                                            </div> -->
						<div class="form-group">
							<label class="control-label col-sm-3" for="email">Assessment:</label>
							<div class="col-sm-8">
								<select class="form-control" id="trainings" name="trainings">
									<option selected disabled>Select Assessment</option>
                                                        <?php
																																																								foreach ( $record->result () as $row ) {
																																																									?>
                                                                <option
										value="<?php echo $row->certificate_id; ?>"><?php echo $row->certificate_name; ?></option>
                                                        <?php } ?>
                                                    </select>
							</div>
						</div>
						<hr>
						<div class="rowform-group">
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<label class="control-label col-sm-3" for="email">Quiz Id:</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="quiz_id"
												name="quiz_id" readonly>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<label class="control-label col-sm-3" for="email">Question:</label>
										<div class="col-sm-9">
											<textarea class="form-control" id="question" name="question"></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<label class="control-label col-sm-3" for="email">Option A:</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="option_a"
												name="option_a">
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<label class="control-label col-sm-3" for="email">Option B:</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="option_b"
												name="option_b">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<label class="control-label col-sm-3" for="email">Option C:</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="option_c"
												name="option_c">
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<label class="control-label col-sm-3" for="email">Option D:</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="option_d"
												name="option_d">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-md-offset-6 col-sm-12">
									<div class="form-group">
										<label class="control-label col-sm-3" for="email">Correct
											Answer:</label>
										<div class="col-sm-9">
											<select class="form-control" id="c_answer" name="c_answer">
												<option selected disabled>Select Correct Answer</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-md-offset-6 col-sm-12">
									<div class="form-group">
										<select id='pre-selected-options' multiple='multiple'>
											<option value='elem_1'>elem 1</option>
											<option value='elem_2'>elem 2</option>
											<option value='elem_3'>elem 3</option>
											<option value='elem_4'>elem 4</option>
											<option value='elem_100'>elem 100</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<hr>
					</form>
					<div class="form-group">
						<div class="text-right">
							<button type="submit" class="btn btn-circle dark btn-outline"
								id="add_buttons">
								<i class="fa fa-check"></i> Add Quiz
							</button>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-circle green btn-outline"
						id="addquiz">
						<i class="fa fa-check"></i> Submit
					</button>
					<button type="button" class="btn btn-circle red btn-outline"
						data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
				</div>
			</div>
		</div>
	</div>


	<!--<div id="sla_confirm" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="col-sm-12" id="sla_detail_confirm">                        
                            </div>
           
           
        </div>
    </div>-->
	<div class="modal fade bs-modal-lg sla_confirm" id="large" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Training Content</h4>
                                                </div>
                                                <div class="modal-body"> 
												 <div class="col-sm-12" id="sla_detail_confirm">                        
                                               </div>
											   </div>
                                                <div class="modal-footer">
                                                    <button type="button" id="stopvid" onClick="stop_video()" data-dismiss="modal" class="btn dark btn-outline" >Close</button>
                                    
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
	<!-- For Quiz -->
     <!--loading model-->
	 <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->
	<!-- END QUICK SIDEBAR -->
		<?php include 'assets/lib/javascript.php'?>
		<script>
       		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
        	$('#training_management').addClass('open');
		</script>
	<script>
        $(document).ready(function(){
			$('.product_change').hide();
      $('.sample_2').DataTable({"order": []});
        });
		function stop_video(){
			
			const videoElement = document.querySelector('.video-element');
			videoElement.pause();
            videoElement.currentTime = 0;
		}

		$('#product_type').change(function(){
			var product_type=$('#product_type').val();
			if(product_type=="product"){
				$('.product_change').show();
			}else{
				$('.product_change').hide();
			}
		})
		$('#product_type1').change(function(){
			var product_type=$('#product_type1').val();
			if(product_type=="product"){
				$('.product_change').show();
			}else{
				$('.product_change').hide();
			}
		})
		$('#assessment_type').change(function(){
			var product_type=$('#assessment_type').val();
			if(product_type=="product"){
				$('.assessment_change').show();
			}else{
				$('.assessment_change').hide();
			}
		})
        $("#product_training").change(function () {
            $('#category_training').empty();
            var product_id = $('#product_training').val();
            $.ajax({
                url         :   "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                type        :   "POST",
                data        :   {'product_id' : product_id},
                datatype    :   "JSON",
                cache       :   false,
                process     :   false,
                success     :   function(data){
                                    var data=JSON.parse(data);
                                    if(data == 1){
                                        swal('No Sub Category are entered, kindly add category to this product');
                                       /*  $.dialogbox({
                                            type:'msg',
                                            content:"No Category are entered, kindly add category to this product",
                                            closeBtn:true,
                                            btn:['Ok.'],
                                            call:[
                                                function(){
                                                    $.dialogbox.close();
                                                    //window.location.reload();
                                                }
                                            ]
                                        }); */
                                        //bootbox.alert("No Category are entered, kindly add category to this product");
                                        $('#category_training').empty();
                                    }else{
                                        //console.log(data);
                                        $('#category_training').append('<option selected disabled>Select Sub Category</option>')
                                        for(i=0; i<data.length;i++){
                                            $('#category_training').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                        }
                                    }
                                },
            })
        });

        $("#product_training1").change(function () {
            $('#category_training1').empty();
            var product_id = $('#product_training1').val();
            $.ajax({
                url         :   "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                type        :   "POST",
                data        :   {'product_id' : product_id},
                datatype    :   "JSON",
                cache       :   false,
                process     :   false,
                success     :   function(data){
                                    var data=JSON.parse(data);
                                    if(data == 1){
                                        swal('No Sub Category are entered, kindly add category to this product');
                                       /*  $.dialogbox({
                                            type:'msg',
                                            content:"No Category are entered, kindly add category to this product",
                                            closeBtn:true,
                                            btn:['Ok.'],
                                            call:[
                                                function(){
                                                    $.dialogbox.close();
                                                    //window.location.reload();
                                                }
                                            ]
                                        }); */
                                        //bootbox.alert("No Category are entered, kindly add category to this product");
                                        $('#category_training1').empty();
                                    }else{
                                        //console.log(data);
                                        $('#category_training1').append('<option selected disabled>Select Sub Category</option>')
                                        for(i=0; i<data.length;i++){
                                            $('#category_training1').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                        }
                                    }
                                },
            })
        });
        $("#product_name1").change(function () {
            $('#category1').empty();
            var product_id = $('#product_name1').val();
            $.ajax({
                url         :   "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                type        :   "POST",
                data        :   {'product_id' : product_id},
                datatype    :   "JSON",
                cache       :   false,
                process     :   false,
                success     :   function(data){
                                    var data=JSON.parse(data);
                                    if(data == 1){
                                    	 swal('No Sub Category are entered, kindly add category to this product');
                                       // bootbox.alert("No Category are entered, kindly add category to this product");
                                        $('#category1').empty();
                                    }else{
                                        //console.log(data);
                                        for(i=0; i<data.length;i++){
                                            $('#category1').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                        }
                                    }
                                },
            })
        });

        $("#product_name").change(function () {
            $('#category').empty();
            var product_id = $('#product_name').val();
            $.ajax({
                url         :   "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                type        :   "POST",
                data        :   {'product_id' : product_id},
                datatype    :   "JSON",
                cache       :   false,
                process     :   false,
                success     :   function(data){
                                    var data=JSON.parse(data);
                                    if(data == 1){
                                    	 swal('No Sub Category are entered, kindly add category to this product');
                                       // bootbox.alert("No Category are entered, kindly add category to this product");
                                        $('#category').empty();
                                    }else{
                                        //console.log(data);
                                        $('#category').append('<option selected disabled>Select Sub Category</option>');
                                        for(i=0; i<data.length;i++){
                                            $('#category').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                        }
                                    }
                                },
            })
        });
			function add_quizs(id){
        		$('#myquiz').modal('show');
				/* $.ajax({
                    type        :"POST",
                    url         :"<?php echo site_url('controller_admin/check_assesment');?>",
                    data        :{'id':id},
                    datatype    :"JSON",
                    cache       :false,
                    success     :function(data)
                                {
                        			var data=JSON.parse(data);
                                     $('#assessments').val(data['certificate_name']);
                                     $('#assessments').text(data['certificate_id']);
                                    $.ajax({
                                        url         :   "<?php echo base_url();?>?index.php?/controller_admin/quiz_check",
                                        type        :   "POST",
                                        data        :   "",
                                        cache       :   false,
                                        success     :   function(data){
                                                            //console.log(data);
                                                            $('#quiz_id').val($.trim(data));
                                                        },
                                    })
                                    $('#myquiz').modal('show');
                                }
               });*/
			}
			$('#add_buttons').click(function(){
				$("#upload_quiz").append("<div class='rowform-group'><div class='row'><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Quiz Id:</label><div class='col-sm-9'><input type='text' class='form-control' id='quiz_id' name='quiz_id' readonly></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Question:</label><div class='col-sm-9'><textarea class='form-control' id='question' name='question'></textarea></div></div></div></div><div class='row'><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option A:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_a' name='option_a'></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option B:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_b' name='option_b'></div></div></div></div><div class='row'><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option C:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_c' name='option_c'></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option D:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_d' name='option_d'></div></div></div></div><div class='row'><div class='col-md-6 col-md-offset-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Correct Answer:</label><div class='col-sm-9'><select class='form-control' id='c_answer' name='c_answer'><option selected disabled>Select Correct Answer</option></select></div></div></div></div></div><hr>");
			});
        </script>
	<script>
        	var company_id="<?php echo $this->session->userdata('companyid');?>";
        	$.ajax({
                url         :   "<?php echo base_url(); ?>index.php?/controller_admin/gettrainin_g",
                type        :   "POST",
                data        :   {'company_id':company_id},// {action:'$funky'}
                datatype  	:   "JSON",
                async		:	false,
                success     :   function(data){
                	 //$("#tree_5").jstree("destroy");
                	// var data=JSON.parse(data);
                	 //console.log(data);
	                 },
            });



	        /*$.ajax({
	            url         :   "<?php echo base_url();?>index.php?/controller_admin/gettrainin_g",
	            type        :   "POST",
	            data        :   {'company_id':company_id},
	            dataType	:	"json",
	            success     :   function(data){
		            				//var datas=[];
	                                datas=data;
	                            },
	        });
	        console.log(datas);*/
			$("#tree_5").jstree({
				"core" : {
					"themes" : {
						"responsive": false
					},
					// so that create works
					"check_callback" : true,
					'data':{
						'url':'<?php echo base_url(); ?>index.php?/controller_admin/gettrainin_g',
						//'plugins' : [ "wholerow", "checkbox" ],
						'dataType':'json',
					},
				},
				"types" : {
					"default" : {
						"icon": "http://www.freeiconspng.com/uploads/training-icon-30.png"
					},
					"file" : {
						"icon": "http://www.freeiconspng.com/uploads/training-icon-30.png"
					}
				},
				"state" : { "key" : "demo2" },
				"plugins" : ["themes", "json", "grid", "dnd", "contextmenu", "search", "types"],
				"contextmenu":{
				    "items": function($node) {
				        var tree = $("#tree_5").jstree(true);
				        return {
				            "Create": {
				                "separator_before": false,
				                "separator_after": false,
				                "label": "Create",
				                "action": function (obj) {
				                	var checked,selected_node,chosen_nodes,datastring,get_storedid,parent,child,g_parents,g_child_d,g_child;
									var company_id
				                	checked =  $("#tree_5").jstree(true).get_selected(true);
				                	selected_node= JSON.stringify(checked);
									chosen_nodes=JSON.parse(selected_node);
									console.log(chosen_nodes);
									parent=chosen_nodes[0]['parent'];
									g_parents=chosen_nodes[0]['parents'].length;
									if(g_parents==1){
										/* parent=chosen_nodes[0]['parents'][0];
										add_prod(parent); */
										parent=chosen_nodes[0]['id'];
										training_check(parent);
									}else if(g_parents==2){
										/* parent=chosen_nodes[0]['parents'][1];
										child=chosen_nodes[0]['parents'][0];
										add_sub(parent,child); */
										parent=chosen_nodes[0]['parents'][0];
										child=chosen_nodes[0]['id'];
										add_train_content(parent,child);
									}
				                }
				            },
				            "Rename": {
				                "separator_before": false,
				                "separator_after": false,
				                "label": "Edit",
				                "action": function (obj) {
				                	var checked,selected_node,chosen_nodes,datastring,get_storedid,parent,child,g_parents,g_child_d,g_child;
									var company_id
				                	checked =  $("#tree_5").jstree(true).get_selected(true);
				                	selected_node= JSON.stringify(checked);
									chosen_nodes=JSON.parse(selected_node);
									console.log(chosen_nodes);
									parent=chosen_nodes[0]['parent'];
									g_parents=chosen_nodes[0]['parents'].length;
									if(g_parents==1){
										swal("Sorry Can't able to Edit this Company");
									}
									else if(g_parents==2){
										id=chosen_nodes[0]['id'];
										edit_training(id);
									}else{
										id=chosen_nodes[0]['id'];
										edit_train_content(id);
									}
				                }
				            },
				            "Remove": {
				                "separator_before": false,
				                "separator_after": false,
				                "label": "Delete",
				                "action": function (obj) {
				                	var checked,selected_node,chosen_nodes,datastring,get_storedid,parent,child,g_parents,g_child_d,g_child;
									var company_id
				                	checked =  $("#tree_5").jstree(true).get_selected(true);
				                	selected_node= JSON.stringify(checked);
									chosen_nodes=JSON.parse(selected_node);
									console.log(chosen_nodes);
									parent=chosen_nodes[0]['parent'];
									g_parents=chosen_nodes[0]['parents'].length;
									if(g_parents==1){
										swal("Sorry Can't able to Delete this Company");
									}
									else if(g_parents==2){
										id=chosen_nodes[0]['id'];
										delete_trai(id);
									}else{
										id=chosen_nodes[0]['id'];
										delete_trai_content(id);
									}
				                }
				            }
				        };
				    }
				}
			});
		</script>
	<!--For Product -->
	<script>
			function training_check(){
				$('#insert_trai')[0].reset();
	            $.ajax({
	                url         :   "<?php echo base_url();?>index.php?/controller_admin/training_check",
	                type        :   "POST",
	                data        :   "",
	                cache       :   false,
	                success     :   function(data){
	                                    //console.log(data);
	                                    $('#training_id').val($.trim(data));
	                                    $('#training_1').modal('show');
	                                },
	            })
	        };
	        $('#addtraining_detail').click(function(){

                $('#rowdata_t').empty();
                var file_type = $('#upload_con').val().toString().split('.').pop().toLowerCase();
                if(file_type != ""){
                    var training_id = $('#training_id').val();
                    var company_id = $('#company_id').val();
                    var title_training = $('#title_training').val();
                    var product_type = $('#product_type option:selected').val();
                    var product_training = $('#product_training').val();
                    var category_training = $('#category_training').val();
                    var model_name = $('#model_name').val();
                    var content_type = $('#content_type option:selected').val();
					$('#Searching_Modal').modal('show');

                    $('#rowdata_t').empty();
                    $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/check_trai",
                        type        :   "POST",
                        data        :   $('#insert_trai').serialize(),// {action:'$funky'}
                        //datatype  :   "JSON",
                        content_type:   false,
                        processData :   false,
                        success     :   function(data){
                                        if(data == 1){
											$('#Searching_Modal').modal('hide');
                                            $('#rowdata_t').append("Database already had same Training for this Training Type");
                                            $('.error').show();
                                        }else{
                                            
                                            var upload_content = $('#upload_con').prop('files')[0];
                                            var data = new FormData();
                                            data.append('training_id',training_id);
                                            data.append('company_id',company_id);
                                            data.append('title_training',title_training);
                                            data.append('product_type',product_type);
                                            data.append('product_training',product_training);
                                            data.append('category_training',category_training);
                                            data.append('model_name',model_name);
                                            data.append('content_type',content_type);
                                            data.append('file_type',file_type);
                                            data.append('upload_content',upload_content); 
											$('#Searching_Modal').modal('show');

                                            $.ajax({
                                                type        :   "POST",
                                                url         :   "<?php echo site_url('controller_admin/insert_trai');?>",
                                                data        :   data,
                                                //mimeType    :   "multipart/form-data",
                                                contentType :   false,
                                                processData :   false,
                                                success     :   function(data)
                                                                {   $('#Searching_Modal').modal('hide');                                              
                                                                    if(data == "Training added Successfully"){
                                                                        $('#training_1').modal('hide');
                                                                        swal({
                                                                              title: data,
                                                                              type: "success",
                                                                              showCancelButton: false,
                                                                              confirmButtonClass: "btn-danger",
                                                                              confirmButtonText: "Ok!",
                                                                              cancelButtonText: "No, cancel plx!",
                                                                              closeOnConfirm: false,
                                                                              closeOnCancel: false
                                                                            },
                                                                            function(isConfirm) {
                                                                              if (isConfirm) {
                                                                                window.location.reload();
                                                                              }
                                                                            });
                                                                    }else{
                                                                        $('#rowdata_t').append(data);
                                                                        $('.error').show();
                                                                    }
                                                                }
                                           });

                                            /*$.ajax({
                                                url         :   "<?php echo base_url(); ?>index.php?/controller_admin/insert_trai",
                                                type        :   "POST",
                                                data        :   $('#insert_trai').serialize(),// {action:'$funky'}
                                                datatype    :   "JSON",
                                                cache       :   false,
                                                success     :   function(data){
                                                                    //var data=JSON.parse(data);
                                                                    console.log(data);
                                                                    if(data == "Training added Successfully"){
                                                                        swal({
                                                                              title: data,
                                                                              type: "success",
                                                                              showCancelButton: false,
                                                                              confirmButtonClass: "btn-danger",
                                                                              confirmButtonText: "Ok!",
                                                                              cancelButtonText: "No, cancel plx!",
                                                                              closeOnConfirm: false,
                                                                              closeOnCancel: false
                                                                            },
                                                                            function(isConfirm) {
                                                                              if (isConfirm) {
                                                                                window.location.reload();
                                                                              }
                                                                            });
                                                                    }else{
                                                                        $('#rowdata_t').append(data);
                                                                        $('.error').show();
                                                                    }
                                                                },
                                            });*/
                                        }
                                    }
                    });
                }else{
                    $('#rowdata_t').append('Training content is mandatory');
                    $('.error').show();
                }                
			});
	        function edit_training(id){
				$('.product_change').hide();
				$.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_training",
                    type        :   "POST",
                    data        :   {'id':id},// {action:'$funky'}
                    datatype    :   "JSON",
                    cache       :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
										if(data['product_type']=="generic" || data['product_type']=="technology"){
											$('#ids').val(data['a']);
											$('#training_id1').val(data['training_id']);
											$('#title_training1').val(data['b']);
											$('#product_type1').val(data['product_type']);
                                            $('#content_type_1').val(data['type']);
                                            $('#content_type_2').val(data['type']);
                                            $('#edit_content_2').val(data['link']);
											$('#company_id_1').val(data['company_id']);
											$('#edits_training').modal('show');
										}else{
											$('#ids').val(data['a']);
											$('#training_id1').val(data['training_id']);
											$('#title_training1').val(data['b']);
											$('#product_type1').val(data['product_type']);
											$('#model_name1').val(data['model_name']);
											$('#company_id_1').val(data['company_id']);
                                            $('#content_type_1').val(data['type']);
                                            $('#content_type_2').val(data['type']);
                                            $('#edit_content_2').val(data['link']);
											//$('#product_training1').val(data['product_name']);
											$('select[name="product_training1"] option[value="'+data['prod_id']+'"]').attr("selected",true);
											$.ajax({
                                                url         :   "<?php echo base_url(); ?>index.php?/controller_admin/data_category",
                                                type        :   "POST",
                                                data        :   {'id':data['category']},// {action:'$funky'}
                                                datatype    :   "JSON",
                                                cache       :   false,
                                                success     :   function(data){
                                                                    var data=JSON.parse(data);
                                                                    //console.log(data);
                                                                    $('#category_training1').append('<option selected value="'+data['cat_id']+'">'+data['cat_name']+'</option>');
                                                                }
                                            });
											$('.product_change').show();
											$('#edits_training').modal('show');
										}
                                    },
                });
            }

	        $('#edittraining_d').click(function(){

				$('#rowdata_1').empty();
                var ids = $("#ids").val();
                var company_id_1 = $("#company_id_1").val();
                var training_id1 = $("#training_id1").val();
                var title_training1 = $("#title_training1").val();                
                var product_type1 = $("#product_type1").val();
                var product_training1 = $("#product_training1").val();
                var category_training1 = $("#category_training1").val();
                var model_name1 = $("#model_name1").val();
                var content_type_1 = $("#content_type_1").val();
                var content_type_2 = $("#content_type_2").val();                
                var edit_content_2 = $("#edit_content_2").val();
                var upload_con_1 = $('#upload_con_1').val().toString().split('.').pop().toLowerCase();
				$('#Searching_Modal').modal('show');
                if (upload_con_1 == "") {
                    upload_con_1 = edit_content_2;
                    content_type_1 = content_type_2;

                    $.ajax({
                        url         :       "<?php echo base_url(); ?>index.php?/controller_admin/edit_trai_1",
                        type        :       "POST",
                        data        :       {
                                                'ids': ids,
                                                'company_id_1': company_id_1,
                                                'training_id1': training_id1,
                                                'title_training1': title_training1,
                                                'product_type1': product_type1,
                                                'product_training1': product_training1,
                                                'category_training1': category_training1,
                                                'model_name1': model_name1,
                                                'content_type_1': content_type_1,
                                                'upload_con_1': upload_con_1
                                            }, // {action:'$funky'}
                        //datatype  :   "JSON",
                        cache       :       false,
                        success     :       function(data) {
							                    $('#Searching_Modal').modal('hide');
                                                data = $.trim(data);
                                                if (data == "Training updated Successfully") {
                                                    $('#edits_training').modal('hide'); 
                                                    swal({
                                                          title: "Training updated Successfully",                                  
                                                          type: "success",
                                                          showCancelButton: false,
                                                          confirmButtonClass: "btn-danger",
                                                          confirmButtonText: "Ok!",
                                                          cancelButtonText: "No, cancel plx!",
                                                          closeOnConfirm: false,
                                                          closeOnCancel: false
                                                        },
                                                        function(isConfirm) {
                                                          if (isConfirm) {
                                                            window.location.reload();
                                                          }
                                                        });
                                                } else {
                                                    $('#rowdata_1').append(data);
                                                    $('#edits_training').animate({ scrollTop: 0 });
                                                    $('.error').show();
                                                }
                                        },
                    });
                } else {
                    if(content_type_1 == 'pdf'){   
                        var filetype = $('#upload_con_1').val().toString().split('.').pop().toLowerCase();                     
                        if ($.inArray(upload_con_1, ['pdf']) !== -1) {
                            var upload_con_1 = $('#upload_con_1').prop('files')[0];
                            var form_data = new FormData();
                            form_data.append('ids', ids);
                            form_data.append('company_id_1', company_id_1);
                            form_data.append('training_id1', training_id1);
                            form_data.append('title_training1', title_training1);
                            form_data.append('product_type1', product_type1);
                            form_data.append('product_training1', product_training1);
                            form_data.append('category_training1', category_training1);
                            form_data.append('model_name1', model_name1);
                            form_data.append('content_type_1', content_type_1);
                            form_data.append('filetype', filetype);
                            form_data.append('upload_con_1', upload_con_1);
							$('#Searching_Modal').modal('show');

                            $.ajax({
                                url         :   "<?php echo base_url(); ?>index.php?/controller_admin/edit_trai_2",
                                type        :   "POST",
                                data        :   form_data,
                                contentType :   false,
                                processData :   false,
                                success     :   function(data) {
									               $('#Searching_Modal').modal('hide');
                                                    data = $.trim(data);
                                                    if (data == "Training updated Successfully") {
                                                        $('#edits_training').modal('hide'); 
                                                        swal({
                                                              title: "Training updated Successfully",                                  
                                                              type: "success",
                                                              showCancelButton: false,
                                                              confirmButtonClass: "btn-danger",
                                                              confirmButtonText: "Ok!",
                                                              cancelButtonText: "No, cancel plx!",
                                                              closeOnConfirm: false,
                                                              closeOnCancel: false
                                                            },
                                                            function(isConfirm) {
                                                              if (isConfirm) {
                                                                window.location.reload();
                                                              }
                                                            });
                                                    } else {
                                                        $('#rowdata_1').append(data);
                                                        $('#edits_training').animate({ scrollTop: 0 });
                                                        $('.error').show();
                                                    }
                                                },
                            });
                        }else{
                           $('#rowdata_1').append('Training content should be matched with content type');
                           $('.error').show();
                        }
                    }else{
                        if ($.inArray(upload_con_1, ['mp4','divx', '3gp', 'wmv']) !== -1) {
                            var upload_con_1 = $('#upload_con_1').prop('files')[0];
                            var form_data = new FormData();
                            form_data.append('ids', ids);
                            form_data.append('company_id_1', company_id_1);
                            form_data.append('training_id1', training_id1);
                            form_data.append('title_training1', title_training1);
                            form_data.append('product_type1', product_type1);
                            form_data.append('product_training1', product_training1);
                            form_data.append('category_training1', category_training1);
                            form_data.append('model_name1', model_name1);
                            form_data.append('content_type_1', content_type_1);
                            form_data.append('upload_con_1', upload_con_1);
							$('#Searching_Modal').modal('show');

                            $.ajax({
                                url         :   "<?php echo base_url(); ?>index.php?/controller_admin/edit_trai_2",
                                type        :   "POST",
                                data        :    form_data,
                                contentType: false,
                                processData: false,
                                success     :   function(data) {
									              $('#Searching_Modal').modal('hide');
                                                    data = $.trim(data);
                                                    if (data == "Training updated Successfully") {
                                                        $('#edits_training').modal('hide'); 
                                                        swal({
                                                              title: "Training updated Successfully",                                  
                                                              type: "success",
                                                              showCancelButton: false,
                                                              confirmButtonClass: "btn-danger",
                                                              confirmButtonText: "Ok!",
                                                              cancelButtonText: "No, cancel plx!",
                                                              closeOnConfirm: false,
                                                              closeOnCancel: false
                                                            },
                                                            function(isConfirm) {
                                                              if (isConfirm) {
                                                                window.location.reload();
                                                              }
                                                            });
                                                    } else {
                                                        $('#rowdata_1').append(data);
                                                        $('#edits_training').animate({ scrollTop: 0 });
                                                        $('.error').show();
                                                    }
                                                },
                            });
                        }else{
                           $('#rowdata_1').append('Training content should be matched with content type');
                           $('.error').show();
                        }
                    }
                }
			});


	        function delete_trai(id) {
	        	swal({
	        		  title: "Are you sure?",
	        		  text: "You will not be able to recover this Training!",
	        		  type: "warning",
	        		  showCancelButton: true,
	        		  confirmButtonClass: "btn-danger",
	        		  confirmButtonText: "Yes, Delete It!",
	        		  cancelButtonText: "No, Keep It!",
	        		  closeOnConfirm: false,
	        		  closeOnCancel: false
	        		},
	        		function(isConfirm) {
	        		  if (isConfirm) {
	        			  $.ajax({
	                          url: "<?php echo base_url(); ?>index.php?/controller_admin/deletetraining",
	                          type: "POST",
	                          data: {
	                              'id': id
	                          }, // {action:'$funky'}
	                          //datatype  :   "JSON",
	                          cache: false,
	                          success: function(data) {
	                              if (data == 1) {
	                            	  swal({
	                            		  title: "Training Deleted Successfully",
	                            		  type: "success",
	                            		  showCancelButton: false,
	                            		  confirmButtonClass: "btn-danger",
	                            		  confirmButtonText: "Ok!",
	                            		  cancelButtonText: "No, cancel plx!",
	                            		  closeOnConfirm: false,
	                            		  closeOnCancel: false
	                            		},
	                            		function(isConfirm) {
	                            		  if (isConfirm) {
	                            		    window.location.reload();
	                            		  }
	                            		});
	                              } else {
	                            	  swal({
	                            		  title: "Training Not Deleted Successfully",
	                            		  type: "error",
	                            		  showCancelButton: false,
	                            		  confirmButtonClass: "btn-danger",
	                            		  confirmButtonText: "Ok!",
	                            		  cancelButtonText: "No, cancel plx!",
	                            		  closeOnConfirm: false,
	                            		  closeOnCancel: false
	                            		},
	                            		function(isConfirm) {
	                            		  if (isConfirm) {
	                            		    window.location.reload();
	                            		  }
	                            		});
	                              }
	                          },
	                      });
	        		  } else {
	        		    swal.close();
	        		  }
	        		});
	        }

	        function add_train_content(){
				$('#insert_trai_content')[0].reset();
		        $('#add_contents').modal('show');
		    }

	        $('#addtraining_content').click(function(){
				$('#rowdata_content').empty();
                var file_type = $('#upload_content').val().toString().split('.').pop().toLowerCase();
                var training_select = $('#training_select').val();
                var training_titles = $('#training_titles').val();
                var content_type = $('#content_type').val();
                var data = new FormData();
                var upload_content = $('#upload_content').prop('files')[0];
                data.append('training_titles',training_titles);
                data.append('training_select',training_select);
                data.append('content_type',content_type);
                data.append('upload_content',upload_content);
                data.append('file_type',file_type);
				$('#Searching_Modal').modal('show');
				$.ajax({
					type        :"POST",
					url         :"<?php echo site_url('controller_admin/inserttraining');?>",
					data        :data,
					mimeType    : "multipart/form-data",
					contentType : false,
					cache       : false,
					processData : false,
					success     :function(data)
								{
									$('#Searching_Modal').modal('hide');
									if(data == "Training content successfully uploaded"){
										swal({
		                            		  title: data,
		                            		  type: "success",
		                            		  showCancelButton: false,
		                            		  confirmButtonClass: "btn-danger",
		                            		  confirmButtonText: "Ok!",
		                            		  cancelButtonText: "No, cancel plx!",
		                            		  closeOnConfirm: false,
		                            		  closeOnCancel: false
		                            		},
		                            		function(isConfirm) {
		                            		  if (isConfirm) {
		                            		    window.location.reload();
		                            		  }
		                            		});
									}else{
										$('#rowdata_content').append(data);
										$('.error').show();
									}
								}
			   });
			});
	        function edit_train_content(id){
				$.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_content",
                    type        :   "POST",
                    data        :   {'id':id},// {action:'$funky'}
                    datatype    :   "JSON",
                    cache       :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
                                       
                                        $('#id_1').val(data['id']);
                                        $('#training_titles1').val(data['title']);
                                        $('#training_select1').val(data['training_id']);
                                        $('#content_type1').val(data['type']);
                                        $('#edit_content2').val(data['link']);
                                        //$('select[name="product_name_1"] option[value="' + data['prod_id'] + '"]').attr("selected", true);
                                        $('#edit_contents').modal('show');

                                    },
                });
            }
            $('#edittraining_content').click(function(){

            	/* $('#rowdata_content').empty();
                var file_type = $('#upload_content').val().toString().split('.').pop().toLowerCase();
                var training_select = $('#training_select').val();
                var training_titles = $('#training_titles').val();
                var content_type = $('#content_type').val();
                var data = new FormData();
                var upload_content = $('#upload_content').prop('files')[0];
                data.append('training_titles',training_titles);
                data.append('training_select',training_select);
                data.append('content_type',content_type);
                data.append('upload_content',upload_content);
                data.append('file_type',file_type);
				$.ajax({
					type        :"POST",
					url         :"<?php echo site_url('controller_admin/inserttraining');?>",
					data        :data,
					mimeType    : "multipart/form-data",
					contentType : false,
					cache       : false,
					processData : false,
					success     :function(data)
								{
									console.log(data);
									if(data == "Training content successfully uploaded"){
										swal({
		                            		  title: data,
		                            		  type: "success",
		                            		  showCancelButton: false,
		                            		  confirmButtonClass: "btn-danger",
		                            		  confirmButtonText: "Ok!",
		                            		  cancelButtonText: "No, cancel plx!",
		                            		  closeOnConfirm: false,
		                            		  closeOnCancel: false
		                            		},
		                            		function(isConfirm) {
		                            		  if (isConfirm) {
		                            		    window.location.reload();
		                            		  }
		                            		});
									}else{
										$('#rowdata_content').append(data);
										$('.error').show();
									}
								}
			   });*/



            	$('#rowdata_1').empty();
                var id1 = $("#id_1").val();
                var training_titles1 = $("#training_titles1").val();
                var training_select1 = $('#training_select1').val();
                var content_type1 = $("#content_type1").val();
                var edit_content2 = $("#edit_content2").val();
                var edit_content1 = $('#edit_content1').val().toString().split('.').pop().toLowerCase();
				$('#Searching_Modal').modal('show');
                if (edit_content1 == "") {
                    $.ajax({
                        url: "<?php echo base_url(); ?>index.php?/controller_admin/edittraining1",
                        type: "POST",
                        data: {
                            'id1': id1,
                            'training_titles1': training_titles1,
                            'training_select1': training_select1,
                            'content_type1': content_type1,
                            'edit_content2': edit_content2,
                        }, // {action:'$funky'}
                        //datatype  :   "JSON",
                        success: function(data) {
                            $('#Searching_Modal').modal('hide');
                            data = $.trim(data);
                            if (data == "Training content updated successfully") {
                            		swal({
	                          		  title: data,
	                          		  type: "success",
	                          		  showCancelButton: false,
	                          		  confirmButtonClass: "btn-danger",
	                          		  confirmButtonText: "Ok!",
	                          		  cancelButtonText: "No, cancel plx!",
	                          		  closeOnConfirm: false,
	                          		  closeOnCancel: false
	                          		},
	                          		function(isConfirm) {
	                          		  if (isConfirm) {
	                          		    window.location.reload();
	                          		  }
	                          		});
                            } else {
                                $('#rowdata_content_1').append(data);
                                $('#edits_category').animate({ scrollTop: 0 });
                                $('.error').show();
                            }
                        },
                    });
                } else {
                    var file_type = $('#edit_content1').val().toString().split('.').pop().toLowerCase();
                    var id = $('#id_1').val();
	                var training_select = $('#training_select1').val();
	                var training_titles = $('#training_titles1').val();
	                var content_type = $('#content_type1').val();
	                var data = new FormData();
	                var upload_content = $('#edit_content1').prop('files')[0];
	                data.append('id',id);
	                data.append('training_titles',training_titles);
	                data.append('training_select',training_select);
	                data.append('content_type',content_type);
	                data.append('upload_content',upload_content);
	                data.append('file_type',file_type);
					$('#Searching_Modal').modal('show');
					$.ajax({
						type        :"POST",
						url         :"<?php echo site_url('controller_admin/edittraining');?>",
						data        :data,
						mimeType    : "multipart/form-data",
						contentType : false,
						cache       : false,
						processData : false,
						success     :function(data)
									{
										$('#Searching_Modal').modal('hide');
										if(data == "Training content updated successfully"){
											swal({
			                            		  title: data,
			                            		  type: "success",
			                            		  showCancelButton: false,
			                            		  confirmButtonClass: "btn-danger",
			                            		  confirmButtonText: "Ok!",
			                            		  cancelButtonText: "No, cancel plx!",
			                            		  closeOnConfirm: false,
			                            		  closeOnCancel: false
			                            		},
			                            		function(isConfirm) {
			                            		  if (isConfirm) {
			                            		    window.location.reload();
			                            		  }
			                            		});
										}else{
											$('#rowdata_content_1').append(data);
											$('.error').show();
										}
									}
				   });
                }
            });

            function delete_trai_content(id) {
	        	swal({
	        		  title: "Are you sure?",
	        		  text: "You will not be able to recover this Training Content!",
	        		  type: "warning",
	        		  showCancelButton: true,
	        		  confirmButtonClass: "btn-danger",
	        		  confirmButtonText: "Yes, Delete It!",
	        		  cancelButtonText: "No, Keep It!",
	        		  closeOnConfirm: false,
	        		  closeOnCancel: false
	        		},
	        		function(isConfirm) {
	        		  if (isConfirm) {
	        			  $.ajax({
	                          url: "<?php echo base_url(); ?>index.php?/controller_admin/delete_trai_content",
	                          type: "POST",
	                          data: {
	                              'id': id
	                          }, // {action:'$funky'}
	                          //datatype  :   "JSON",
	                          cache: false,
	                          success: function(data) {
	                              if (data == 1) {
	                            	  swal({
	                            		  title: "Training Content Deleted Successfully",
	                            		  type: "success",
	                            		  showCancelButton: false,
	                            		  confirmButtonClass: "btn-danger",
	                            		  confirmButtonText: "Ok!",
	                            		  cancelButtonText: "No, cancel plx!",
	                            		  closeOnConfirm: false,
	                            		  closeOnCancel: false
	                            		},
	                            		function(isConfirm) {
	                            		  if (isConfirm) {
	                            		    window.location.reload();
	                            		  }
	                            		});
	                              } else {
	                            	  swal({
	                            		  title: "Training Not Deleted Successfully",
	                            		  type: "error",
	                            		  showCancelButton: false,
	                            		  confirmButtonClass: "btn-danger",
	                            		  confirmButtonText: "Ok!",
	                            		  cancelButtonText: "No, cancel plx!",
	                            		  closeOnConfirm: false,
	                            		  closeOnCancel: false
	                            		},
	                            		function(isConfirm) {
	                            		  if (isConfirm) {
	                            		    window.location.reload();
	                            		  }
	                            		});
	                              }
	                          },
	                      });
	        		  } else {
	        		    swal.close();
	        		  }
	        		});
	        }

		</script>
	<!--For Product -->
	<!-- For Assessment -->
	<script>
	        function certificate_check(){
				$('#add_certificate')[0].reset();
	            $.ajax({
	                url         :   "<?php echo base_url();?>index.php?/controller_admin/certificate_check",
	                type        :   "POST",
	                data        :   "",
	                cache       :   false,
	                success     :   function(data){
	                                    console.log(data);
	                                    $('#assessment_id').val($.trim(data));
	                                    $('#mycertificate').modal('show');
	                                },
	            })
	        };
	        $('#addcertificate').click(function(){
                $('#rowdata').empty();
                $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/insertcertificate",
                        type        :   "POST",
                        data        :   $('#add_certificate').serialize(),// {action:'$funky'}
                        datatype    :   "JSON",
                        success     :   function(data){
							            $('#mycertificate').modal('hide');
                                            if(data == 1){
                                            	 swal({
           	                            		  title: "Assessment added Successfully",
           	                            		  type: "success",
           	                            		  showCancelButton: false,
           	                            		  confirmButtonClass: "btn-danger",
           	                            		  confirmButtonText: "Ok!",
           	                            		  cancelButtonText: "No, cancel plx!",
           	                            		  closeOnConfirm: false,
           	                            		  closeOnCancel: false
           	                            		},
           	                            		function(isConfirm) {
           	                            		  if (isConfirm) {
           	                            		    window.location.reload();
           	                            		  }
           	                            		});
                                            }else{
                                                $('#rowdata').append(data);
                                                $('.error').show();
                                            }
                                        },
                    });

            });
	        /*function edit_assessment(id){
                $.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_certificate",
                    type        :   "POST",
                    data        :   {'id':id},// {action:'$funky'}
                    datatype    :   "JSON",
                    cache       :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
                                        $('#id1').val(data['a']);
                                        $('#certificate_id1').val(data['certificate_id']);
                                        $('#certificate_name1').val(data['certificate_name']);
                                        $('#title1').val(data['title']);
                                        $('select[name="product_name1"] option[value="'+data['product_name']+'"]').attr("selected",true);
                                        $.ajax({
                                            url         :   "<?php echo base_url(); ?>index.php?/controller_admin/data_category",
                                            type        :   "POST",
                                            data        :   {'id':data['cat_id']}, // {action:'$funky'}
                                            datatype    :   "JSON",
                                            cache       :   false,
                                            success     :   function(data){
                                                                var data=JSON.parse(data);
                                                                //console.log(data);
                                                                $('#category1').append('<option selected value="'+data['cat_id']+'">'+data['cat_name']+'</option>');

                                                            }
                                        });
                                        $('#edit_assessment').modal('show');
                                        },
                });
            }*/
	        function delete_assessment(id){
	        	swal({
          		  title: "Are you sure?",
          		  text: "You will not be able to recover this Assessment!",
          		  type: "warning",
          		  showCancelButton: true,
          		  confirmButtonClass: "btn-danger",
          		  confirmButtonText: "Yes, Delete It!",
          		  cancelButtonText: "No, Keep It!",
          		  closeOnConfirm: false,
          		  closeOnCancel: false
          		},
          		function(isConfirm) {
          		 if (isConfirm) {
          			  $.ajax({
                            url         :   "<?php echo base_url(); ?>index.php?/controller_admin/deletecertificate",
                            type        :   "POST",
                            data        :   {'id':id},// {action:'$funky'}
                            //datatype  :   "JSON",
                            cache       :   false,
                            success     :   function(data){
                                                if(data == 1){
                                                    swal({
                                                        title: "Your Assessment has been deleted Successfully",
                                                        type: "success",
                                                        showCancelButton: false,
                                                        confirmButtonClass: "btn-danger",
                                                        confirmButtonText: "Ok!",
                                                        cancelButtonText: "No, cancel plx!",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: false
                                                      },
                                                      function(isConfirm) {
                                                        if (isConfirm) {
                                                              window.location.reload();
                                                        }
                                                      });
                                                }else{
                                                     swal({
                                                        title: "Your Assessment has not been deleted Successfully",
                                                        type: "error",
                                                        showCancelButton: false,
                                                        confirmButtonClass: "btn-danger",
                                                        confirmButtonText: "Ok!",
                                                        cancelButtonText: "No, cancel plx!",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: false
                                                      },
                                                      function(isConfirm) {
                                                        if (isConfirm) {
                                                              window.location.reload();
                                                        }
                                                      });
                                                }
                            				}
                            })
					}else{
						swal.close();
					}
          		});
            }
		</script>
		<script>
		function view(id){
			$('#sla_detail_confirm').empty();
			$.ajax({
	            url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_view_trai",
	            type        :   "POST",
	            data        :   {'id':id},// {action:'$funky'}
	            datatype  :   "JSON",
	            cache       :   false,
	            success     :   function(data){
	            					var data = JSON.parse(data)	            					
	            					if(data['type'] == "pdf"){	            						
	            						//$('.modal-content').css("height","400px");
										$("#stopvid").css("display","none");
	            						PDFObject.embed(data['link'], "#sla_detail_confirm");
	            					}else if(data['type']=="video"){
	            						$('#sla_detail_confirm').append('<video class="video-element" width="100%" height="240" autoplay controls><source src="'+data['link']+'" type="video/mp4">Your browser does not support the video tag.</video>')
	            					}
	            					$('.sla_confirm').modal('show');
	            }
	        });
		}
		</script>
	<!-- For Assessment -->
</body>
</html>
