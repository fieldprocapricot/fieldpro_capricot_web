<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <?php $company_id=$this->session->userdata('companyid');
		 include 'assets/lib/cssscript.php'?>
	<style>
		.dt-buttons{
			display:none !important;
		}
		.dataTables_filter {
    		text-align: right;
		}
		.dashboard-stat .details{
    		position: relative !important;   right: 15px !important;
    padding-right: 29px !important;
    padding-left: 30px !important;
		}.dashboard-stat.dashboard-stat-v2 .visual {
     padding-top: 14px;
    margin-bottom: 40px; 
}
.dashboard-stat .visual {
    width: 80px;
     height: 18px !important;
    display: block;
    float: left;
    padding-top: 10px;
    padding-left: 15px;
    margin-bottom: 15px;
    font-size: 35px;
    line-height: 35px;
}.portlet .dashboard-stat:last-child {
    margin-bottom: 0;
    padding-bottom: 19px !important;
}
	</style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
    <!-- BEGIN CONTAINER -->
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include "assets/lib/manager_header.php"?>
        <!-- END HEADER -->
       <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
				  
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box dark">
                                <div class="portlet-title">
									<div class="caption">Reports</div>
									<ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_actions_pending" data-toggle="tab">Attendance</a>
                                                </li>
                                                <li>
                                                    <a href="#Accepted" data-toggle="tab">Revenue</a>
                                                </li>
                                                <li>
                                                    <a href="#C_sat" data-toggle="tab">C-Sat</a>
                                                </li>
                                                <li>
                                                    <a href="#Spare" data-toggle="tab">Spare</a>
                                                </li>
                                                <li>
                                                    <a href="#C-sat" data-toggle="tab">Productivity</a>
                                                </li>
												 <li>
                                                    <a href="#frm_details" data-toggle="tab">Field Return Material</a>
                                                </li>
                                            </ul>
                                </div>
                                <div class="portlet-body">

										<div class="portlet-body">
											<div class="tab-content">
								 <div class="tab-pane active" id="tab_actions_pending">
									<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location" onChange="att_day();" name="role1">
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="area"  onchange=" att_day();" name="role1">
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="region" onChange="att_day();" name="role1">
													  <option value=''>All Region</option>
													   <option value='north'>North</option>
													   <option value='south'>South</option>
													   <option value='east'>East</option>
													   <option value='west'>West</option>
                                                      </select>
                                                   </div> 
												   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id" onChange="att_day();" name="role1">
                                                     
                                                      </select>
                                                   </div>

                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="days_filter" onChange="att_day();" name="role1">
													  <option value=''>Today</option>
													   <option value='week'>This Week</option>
													   <option value='month'>This Month</option>
                                                      </select>
                                                   </div>

                                                </div>
									 <br><br><br>
										
								<div class="table=responsive">

                                                <table class="table table-hover table-bordered datatable1">
                                                    <thead id="modal_atten">
                                                        <tr>
                                                            <th class="text-center">Technician Id</th>
                                                            <th class="text-center">Technician Name</th>
                                                            <th class="text-center">Location</th>
                                                            <th class="text-center">Availability</th>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">In Time</th>
                                                            <th class="text-center">Out Time</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbdy_attendance" align="center">
                                                         <?php foreach ($rec->result() as $row) { ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $row->employee_id; ?></td>
                                                            <td>
                                                                <?php echo $row->first_name; ?></td>
                                                            <td>
                                                                <?php echo $row->location; ?></td>
                                                            <td>
                                                                <?php if($row->availability==1){
																	 echo "Available";
																}else{
																	 echo "Unavailable";
																} ?></td>
                                                                <td><?php echo $row->date; ?></td>
                                                                <td>
                                                                    <?php echo $row->in_time; ?></td>
                                                                <td>
                                                                    <?php echo $row->out_time; ?></td>
                                                        </tr>
                                                        <?php } ?> 
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
									
										<div class="tab-pane fade" id="Accepted">
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
<div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
<select class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="product_id1" onChange="amc_new_day();">
<option value="" selected>All Product-Category</option>
</select>
</div>
<div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
<select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="location1" onChange=" amc_new_day();">
<option value="" selected>All Location</option>
</select>
</div>
<div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
<select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="area1"  onchange="amc_new_day();">
<option value="" selected>All Area</option>
</select>
</div>
<div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
<select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="region1" onChange="amc_new_day();">

	<option value=''>All Region</option>
													   <option value='north'>North</option>
													   <option value='south'>South</option>
													   <option value='east'>East</option>
													   <option value='west'>West</option>
</select>
</div>
<div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
  <select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="amc_new_day" parsley-trigger="change" id="amc_new_day" onChange="amc_new_day();">
                           <option value='today'>Today</option>
                           <option value='week'>This Week</option>
                           <option value='month'>This Month</option>
                           <option value='year'>This Year</option>
                           <option selected value='all'>All Tickets</option>
                        </select>

</div>
<div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
<select class="form-control col-lg-2 col-lg-offset-1 pull-right" name="revenue" id="revenue" onChange="amc_new_day();" >
                           <option value='amc_ren'>AMC Contract</option>
                           <option value='service_bill'>Service Billing</option>
                           <option value='spre_bill'>Spare Billing </option>
                        </select>
						</div>
</div>
											<br><br><br>
                                            <div class="table=responsive">

                                                <table class="table table-hover table-bordered datatable_s">
                                                    <thead id="modal_tech">
                                                        <tr>
                                                            <th class="text-center">Customer Id</th>
                                                            <th class="text-center">Technician-Id</th>
                                                            <th class="text-center">AMC Type</th>
                                                            <th class="text-center">Total Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbl_billing" align="center">
													 <?php foreach ($billing->result() as $row) { $string=json_encode(json_decode($row[ 'requested_spare'])); $string=json_encode(json_decode($string)); $string=preg_replace( '/\s+/', '_', $string ); ?>
                                                        
                                                        <tr>
                                                            <td>
                                                                <?php echo $row->customer_id; ?></td>
                                                            <td>
                                                                <?php echo $row->employee_id; ?></td>
                                                            <td>
                                                                <?php echo $row->contract_type; ?></td>
                                                            <td>
                                                                <?php echo $row->price; ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
											
										<div class="tab-pane fade" id="C_sat">
                                                <div class="row">
                                                   <div class="col-lg-5 col-md-offset-1 col-sm-6 col-xs-12">
                                                      <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                                         <div class="visual">
                                                         </div>
                                                         <div class="details">
                                                            <div class="desc">
                                                               <h4 align='center'><u>TARGET</u></h4>
                                                            </div>
                                                            <div class="desc" style="text-align:left" id="sla_complaince">
                                                            </div>
                                                            <div class="desc " style="text-align:left"  id="cust_feed"></div>
                                                            <div class="desc " style="text-align:left"  id="cust_rate"></div>
                                                            <div class="desc " style="text-align:left"  id=""></div>
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <div class="col-lg-5  col-sm-6 col-xs-12">
                                                      <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                                         <div class="visual">
                                                         </div>
                                                         <div class="details">
                                                            <div class="desc">
                                                               <h4 align='center'><u>ACTUAL</u></h4>
                                                            </div>
                                                            <div class="desc" style="text-align:left" id="sla_complaince1">
                                                            </div>
                                                            <div class="desc " style="text-align:left"  id="cust_feed1"></div>
                                                            <div class="desc " style="text-align:left"  id="cust_rate1"></div>
                                                            <div class="desc " style="text-align:left"  id=""></div>
                                                         </div>
                                                      </a>
                                                   </div>
                                                </div>
                                               <br>
					
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="product_id3" onChange="csat_day();">
                                                         <option value="" selected>All Product-Category</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
                                                      <select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="location3" onChange=" csat_day();">
                                                         <option value="" selected>All Location</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
                                                      <select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="area3"  onchange="area3(); csat_day();">
                                                         <option value="" selected>All Area</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
                                                      <select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="region3" onChange="region3(); csat_day();">
                                                       <option value=''>All Region</option>
													   <option value='north'>North</option>
													   <option value='south'>South</option>
													   <option value='east'>East</option>
													   <option value='west'>West</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
                                                      <select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="csat_day" onChange="csat_day();">
                                                         <option value='today'>Today</option>
                                                         <option value='week'>This Week</option>
                                                         <option value='month'>This Month</option>
                                                         <option value='year'>This Year</option>
                                                         <option selected value='all'>All Tickets</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                
                                                <div class="table=responsive" style="padding-top: 5%;">
                                                   <table class="table table-hover table-bordered" id="datatable3">
                                                      <thead>
                                                         <tr>
                                                            <th class="text-center">Customer id</th>
                                                            <th class="text-center">Ticket id</th>
                                                            <th class="text-center">Technician id</th>
                                                            <th class="text-center">Customer Feedback</th>
                                                            <th class="text-center">Customer Rating</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbdy_csat" align="center">
                                                         <?php if(!empty($completed->result()))
                                                            {
                                                            	foreach ($completed->result() as $row) { ?>
                                                         <tr>
                                                            <td >
                                                               <?php echo $row->cust_id; ?>
                                                            </td>
                                                            <td>
                                                               <?php echo $row->ticket_id; ?>
                                                            </td>
                                                            <td>
                                                               <?php echo $row->employee_id; ?>
                                                            </td>
                                                              <td text-align="center">   <?php if( $row->cust_feedback==''){ ?><?php echo '-'; ?>
															 <?php }
																		else { ?><?php echo $row->cust_feedback; } ?>
															 </td>
                                                            <td>
                                                               <?php if($row->cust_rating!='0' || $row->cust_rating!=''){echo $row->cust_rating.'/10';}else{echo 'No Rating';}?>
                                                            </td>
                                                         </tr>
                                                         <?php }
                                                        }?>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
						
										<div class="tab-pane fade" id="Spare">
												<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location2" onChange="inver();" name="role1">
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="area2"  onchange="inver();" name="role1"> 
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="region2" onChange="inver();"name="role1">
													  <option value=''>All Region</option>
													   <option value='north'>North</option>
													   <option value='south'>South</option>
													   <option value='east'>East</option>
													   <option value='west'>West</option>                                                     </select>
                                                   </div> <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id2" onChange="inver();" name="role1">
                                                      
													</select>
                                                   </div>

<div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
  <select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="inver" onChange="inver();">
                           <option value='c_today'>Today</option>
                           <option value='c_weekly'>This Week</option>
                           <option value='c_monthly'>This Month</option>
                           <option value='c_yearly'>This Year</option>
						   <option selected value='c_all'>All Tickets</option>
                        </select>
</div>
<div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
  <select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="spare_bill" id="spare_bill" onChange="inver();">
                           <option value='billed'>Spare Billed</option>
                           <option value='warrenty'>Spare Consumed </option>
                        </select>
</div>
</div>

<br><br><br>
                                            <div class="table=responsive">

                                                <table class="table table-hover table-bordered" id="datatab2">
                                                    <tr>
                                                        <th class="text-center">Ticket Id</th>
                                                        <th class="text-center">Technician Id</th>
                                                        <th class="text-center">Spare Details</th>
                                                        <th class="text-center">Warranty  Type</th>
                                                        <th class="text-center">Amount</th>
                                                    </tr>
                                                    <tbody id="tbdy_inventory" align="center">
		<?php foreach ($inven as $row) { $string=json_encode(json_decode($row[ 'requested_spare'])); ?>                                                        <tr>
                                                            <td>
                                                                <?php echo $row[ 'ticket_id']; ?>
                                                            </td>
                                                            <td>
                                                                <?php if(!empty($row['employee_id'])){
																		echo $row[ 'employee_id']; }
																else{
																		echo "-";
																}?>
                                                            </td>
                                                            <td>
      <button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row['ticket_id']; ?>" onclick='hover_spare(this.id, <?php echo $string; ?>);'><i class="fa fa-eye"></i></button>
                                                            </td>
                                                            <td>
                                                                <?php if(!empty($row['call_type'])){
																	echo $row['call_type']; }
																else{
																	echo "-";
																}?>
                                                            </td>
                                                            <td>
                                                                <?php if(!empty($row['spare_cost'])){
																	echo $row['spare_cost']; }
																else{
																	echo "-";
																}?>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
												
										
                                        <div class="tab-pane fade" id="C-sat">
			
                    					<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
										<a class="dashboard-stat dashboard-stat-v2 blue" href="#">
											<div class="visual">
												<i class="fa fa-comments"></i>
											</div>
											<div class="details">
												<div class="number">
							<span data-counter="counterup" data-value="<?php echo $result2 ; ?>"></span>
												</div>
												<div class="desc"> Call Closed  </div>

											</div>
										</a>
									
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
										<a class="dashboard-stat dashboard-stat-v2 red" href="#">
											<div class="visual">
												<i class="fa fa-bar-chart-o"></i>
											</div>
											<div class="details">
												<div class="number">
							  <span data-counter="counterup" data-value="<?php echo $result1; ?>"></span>%</div>
												<div class="desc"> % SLA Compliance </div>

											</div>
										</a>
</div>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
										<a class="dashboard-stat dashboard-stat-v2 green" href="#">
											<div class="visual">
												<i class="fa fa-shopping-cart"></i>
											</div>
											<div class="details">
												<div class="number">
													<span data-counter="counterup" data-value="<?php echo $result3; ?>"></span>%
												</div>
												<div class="desc">% of calls Escalated </div>

											</div>
										</a>
</div>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
										<span data-counter="counterup" data-value="<?php echo $result4; ?>"></span>%
                                    </div>
                                    <div class="desc">% of First Call Closure </div>
 
                                </div>
                            </a>
                        </div>
                   					 </div>
									</div>
												
										<div class="tab-pane fade" id="frm_details">
										 <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                       <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12 pull-right">
                                                 <select   class="form-control col-lg-2 col-lg-offset-1 pull-right" name="day" parsley-trigger="change" id="frm_filter" onChange="frm_inver();">
                                                         <option value='today'>Today</option>
                                                         <option value='week'>This Week</option>
                                                         <option value='month'>This Month</option>
                                                         <option value='year'>This Year</option>
                                                         <option selected value='all'>All Tickets</option>
                                                      </select>
											     </div>
											</div>
														<br><br><br>
											 <div class="table=responsive">

                                                <table class="table table-hover table-bordered datatable" id="">
                                                    <thead>
														<tr>
															<th class="text-center">Ticket Id</th>
															<th class="text-center">Technician Id</th>
															<th class="text-center">FRM Details</th>
															<th class="text-center">Date of FRM</th>
															<th class="text-center">Ticket Amount</th>
                                                            <th class="text-center">Status</th>
													 	</tr>
													 </thead>
													    <tbody id="tbdy_frm" align="center">
                                              <?php if(!empty($frminven))
                                                    {
						foreach ($frminven as $row) { 
				$frmstring=json_encode($row[ 'drop_spare']); //$frmstring=json_encode($frmstring);?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $row[ 'ticket_id']; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $row[ 'employee_id']; ?>
                                                            </td>
                                                            <td>
 <button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row['ticket_id']; ?>" onclick='hover_frmspare(this.id, <?php echo $frmstring; ?>);'><i class="fa fa-eye"></i></button>
                                                            </td>
                                                            <td>
                                                                <?php echo $row[ 'drop_of_date']; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $row[ 'total']; ?>

                                                            </td>
                                                            <td>
                                                                <?php echo $row[ 'frm_status']; ?>
                                                                
                                                            </td>
                                                        </tr>
                                                        <?php }
														}?>
                                                    </tbody>
                                                   <!-- <tbody id="tbdy_frm">
                                                        <tr>
                                                            <td>
                                                               Tk9081
                                                            </td>
                                                            <td>
                                                               Tech_1908
                                                            </td>
                                                            <td>
																Only 2 spares
                           									  </td>
                                                            <td>
                                                                2017-08-17
                                                            </td>
                                                            <td>
                                                              1800
                                                            </td>
                                                        </tr>
                                                       
                                                    </tbody>-->
                                                </table>

                                            </div>
                                        </div>
	</div>
                                        </div>
										</div>
                                                                     
                                
                        <!-- END EXAMPLE TABLE PORTLET-->
 						</div>   
                    </div>
               
            </div>
          
       	</div>
    <?php include 'assets/lib/footer.php'?>
</div>




    <!-- END QUICK SIDEBAR -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Spare Details</h4>
                </div>
                <div class="modal-body" id='modal_display' style="text-align:center !important">
                    <div class="table-responsive">
                        <table class="table table-hover" id="sample">
                            <thead>
                                <td>Spare code</td>
                                <td>Spare Required</td>
                                <td>Spare Location</td>
                            </thead>
                            <tbody id="spare_details"></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
	
	 <div id="frm_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">FRM Details</h4>
                </div>
                <div class="modal-body" id='modal_display' style="text-align:center !important">
                    <div class="table-responsive">
                        <table class="table table-hover" id="sample">
                            <thead>
                                <td>Spare code</td>
                                <td>Quanity Returned</td>
                                <td>Spare Location</td>
                            </thead>
                            <tbody id="tbdy_frminventory"></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
	
<!-- Modal -->
            <div id="myModal2" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Details</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id='modal_display1'>
						</form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" data-dismiss="modal">OK!</button>
                     </div>
                  </div>
               </div>
            </div>
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Details</h5>
                </div>
                <div class="modal-body" id='modal_tech'>
                    <form class="form-horizontal" role="form">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END wrapper -->
    <?php include 'assets/lib/javascript.php'?>
<script>
        		
       		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
        	$('#man_report').addClass('open');
			//$('#modal_atten').Datatable();
		
</script>

    <script type="text/javascript">
        window.history.forward();
		//$('#tbdy_inventory').html('<tr><td colspan=5><b><i>No records found</i></b></td></tr>');
        $(document).ready(function() {
		$('.datatable1').DataTable({"order": []});
		$('.datatable_s').DataTable({"order": []});
		$('.datatab2').DataTable({"order": []});
		$('.datatable').DataTable({"order": []});
		$('#datatable3').DataTable({"order": []});
				
	   var company_id="<?php echo $company_id;?>";
		$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_techarea",
         type: 'POST',
         data: {'company_id':company_id},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
				 $('#area').html(' <option selected value="">All Area</option>');
				 console.log(data);
				 for(i=0;i<data.length;i++)
				 {
					$('#area').append('<option value="'+data[i].area+'">'+data[i].area+'</option>');
				 }
			 }
			 else{
				  $('#area').html(' <option selected value="">No results</option>');
			 }
		 }
         });
			
		$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_area",
         type: 'POST',
         data: {'company_id':company_id},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#area1').html(' <option selected value="">All Area</option>');
         $('#area2').html(' <option selected value="">All Area</option>');
         $('#area3').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area1').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         	$('#area2').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         	$('#area3').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
			 else{
				   $('#area1').html(' <option selected value="">No results</option>');
				   $('#area2').html(' <option selected value="">No results</option>');
				   $('#area3').html(' <option selected value="">No results</option>');
			 }
		 }
         });
			
		/*$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_area",
         type: 'POST',
         data: {'company_id':company_id},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#area1').html(' <option selected value="">All Area</option>');
         $('#area2').html(' <option selected value="">All Area</option>');
         $('#area3').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area1').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         	$('#area2').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         	$('#area3').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
			 else{
				   $('#area1').html(' <option selected value="">No results</option>');
				   $('#area2').html(' <option selected value="">No results</option>');
				   $('#area3').html(' <option selected value="">No results</option>');
			 }
		 }
         });*/
			
			
		 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_techlocation",
         type: 'POST',
         data: {'company_id':company_id},
         dataType: "json",
         success: function(data) {
         $('#location').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
         });
			
			 $('#product_id').empty();
			 $('#product_id1').empty();
			 $('#product_id2').empty();
			 $('#product_id3').empty();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_techproduct",
         type: 'POST',
         data: {'company_id':company_id},
         dataType: "json",
         success: function(data) {
			
			 	 if(data.length>0){
         $('#product_id').html(' <option selected value="">All Product-Category</option>');
         for(i=0;i<data.length;i++)
         {
         	$('#product_id').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
     else {
		    $('#product_id').html(' <option selected value="">No results</option>');
	    }
		 }
         });
			
		/* $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_techregion",
         type: 'POST',
         data: {'company_id':company_id},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
			 $('#region').html(' <option selected value="">All Region</option>');
			var res=$.trim(data['region']);
			 for(i=0;i<data.length;i++)
			 {
				$('#region').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
			 }
			 }
			 else{
				  $('#region').html(' <option selected value="">No results</option>');
			 }
		 }
         }); */
			
		/* $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_region",
         type: 'POST',
         data: {'company_id':company_id},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
			 $('#region1').html(' <option selected value="">All Region</option>');
			 $('#region2').html(' <option selected value="">All Region</option>');
			 $('#region3').html(' <option selected value="">All Region</option>');
			var res=$.trim(data['region']);
				
			 for(i=0;i<data.length;i++)
			 {
				$('#region1').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
				$('#region2').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
				$('#region3').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
			 }
			 }
			 else{
				  $('#region1').html(' <option selected value="">No results</option>');
				  $('#region2').html(' <option selected value="">No results</option>');
				  $('#region3').html(' <option selected value="">No results</option>');
			 }
		 }
         }); */
		
	    $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_product",
                type: 'POST',
                data: {
                    'company_id': company_id
                },
                dataType: "json",
                success: function(data) {
					if(data.length>0){
                    //alert(data);
                     $('#product_id1').html(' <option selected  value="">All Products</option>');
                     $('#product_id2').html(' <option selected  value="">All Products</option>');
                     $('#product_id3').html(' <option selected  value="">All Products</option>');
                    	for (i = 0; i < data.length; i++) {
							$('#product_id1').append('<option value="' + data[i].product_id + '">' + data[i].product_name + '</option>');
							$('#product_id2').append('<option value="' + data[i].product_id + '">' + data[i].product_name + '</option>');
							$('#product_id3').append('<option value="' + data[i].product_id + '">' + data[i].product_name + '</option>');
							$('#product_id4').append('<option value="' + data[i].product_id + '">' + data[i].product_name + '</option>');
                    	}
                	}
					else{
						$('#product_id1').html(' <option selected value="">No results</option>');
				  		$('#product_id2').html(' <option selected value="">No results</option>');
				   		$('#product_id3').html(' <option selected value="">No results</option>');
					}
				}
            });
			
	    $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_location",
                type: 'POST',
                data: {
                    'company_id': company_id
                },
                dataType: "json",
                success: function(data) {
					if(data.length>0){
						 $('#location1').html(' <option selected  value="">All Location</option>');
						 $('#location2').html(' <option selected  value="">All Location</option>');
						 $('#location3').html(' <option selected  value="">All Location</option>');
						console.log(data);
						for (i = 0; i < data.length; i++) {
							$('#location1').append('<option value="' + data[i].location + '">' + data[i].location + '</option>');
							$('#location2').append('<option value="' + data[i].location + '">' + data[i].location + '</option>');
							$('#location3').append('<option value="' + data[i].location + '">' + data[i].location + '</option>');
						   // $('#area4').append('<option value="' + data[i].location + '">' + data[i].location + '</option>');
						}
					}
					else{
						   $('#location1').html(' <option selected value="">No results</option>');
						   $('#location2').html(' <option selected value="">No results</option>');
				  		   $('#location3').html(' <option selected value="">No results</option>');
						}
                }
            }); 
    

		 var filter = $('#day').val();
                   var product_id = $('#product_id').val();
                   var product_id1 = $('#product_id1').val();
                   var product_id2 = $('#product_id2').val();
                   var product_id3 = $('#product_id3').val();
                   var filter1 = $('#day1').val();
                   var filter2 = $('#day2').val();
                   var filter3 = $('#day3').val();
                   var region = $('#region').val();
                   var area = $('#area').val();
                   var location = $('#location').val();
                   var region1 = $('#region1').val();
                   var area1 = $('#area1').val();
                   var location1 = $('#location1').val();
                   var region2 = $('#region2').val();
                   var area2 = $('#area2').val();
                   var location2 = $('#location2').val();
                   var region3 = $('#region3').val();
                   var area3 = $('#area1').val();
                   var location3 = $('#location3').val();
                  
                   $.ajax({
                       url: "<?php echo base_url();?>" + "index.php?/controller_manager/sla_complaince",
                       type: 'POST',
                       data: {
                           'company_id': company_id
                       },
                       dataType: "json",
                       success: function(data) {
               console.log(data);
               var sla=data[0].sla;
               var feed=data[0].feed;
               var rating=data[0].rating;
               if(sla[0].percent==0)
               {
               $('#sla_complaince1').html( '<div class="col-lg-11" style="text-align:left;"> SLA Compliance % :</div><div style="text-align:right;" data-counter="counterup" class="col-lg-1" data-value="'+sla[0].percent+'" >'+sla[0].percent+'</div>');
               $('#sla_complaince').html( '<div  class="col-lg-11" style="text-align:left;"> SLA Compliance % :</div><div style="text-align:right;" data-counter="counterup" class="col-lg-1"  data-value="'+sla[0].target_sla+'" >'+sla[0].target_sla+'</div>');
               }
               else
               {
               $('#sla_complaince1').html( '<div class="col-lg-11" style="text-align:left;"> SLA Compliance % :</div> <div style="text-align:right;" class="col-lg-1" data-counter="counterup" data-value="'+sla[0].percent+'" >'+sla[0].percent+'</div>');
               $('#sla_complaince').html( '<div class="col-lg-11" style="text-align:left;"> SLA Compliance % : </div><div style="text-align:right;" class="col-lg-1" data-counter="counterup" data-value="'+sla[0].target_sla+'" >'+sla[0].target_sla+'</div>');
               }
               if(feed[0].value==0)
               {
               $('#cust_feed1').html( '<div class="col-lg-11" style="text-align:left;"> Average C-SAT Feedback Obtained % :</div><div style="text-align:right;" class="col-lg-1" data-counter="counterup" data-value="'+feed[0].value+'" >'+feed[0].value+'</div>');
               $('#cust_feed').html( '<div class="col-lg-11" style="text-align:left;"> Average C-SAT Feedback Obtained % :</div><div style="text-align:right;" data-counter="counterup" class="col-lg-1" data-value="'+feed[0].value1+'" >'+feed[0].value1+'</div>');
               }
               else
               {
               $('#cust_feed1').html( '<div class="col-lg-11"  style="text-align:left;"> Average C-SAT Feedback Obtained % :</div><div style="text-align:right;" class="col-lg-1" data-counter="counterup" data-value="'+feed[0].value+'" >'+feed[0].value+'</div>');
               $('#cust_feed').html( '<div class="col-lg-11" style="text-align:left;"> Average C-SAT Feedback Obtained % :</div><div style="text-align:right;" class="col-lg-1" data-counter="counterup" data-value="'+feed[0].value1+'" >'+feed[0].value1+'</div>');
               }
               if(rating[0].value==0)
               {
               $('#cust_rate1').html( '<div class="col-lg-11" style="text-align:left;"> Average C-SAT Rating % :</div><div style="text-align:right;" class="col-lg-1" data-counter="counterup" data-value="'+rating[0].value+'" >'+rating[0].value+'</div>');
               $('#cust_rate').html( '<div class="col-lg-11" style="text-align:left;"> Average C-SAT Rating % :</div><div style="text-align:right;" class="col-lg-1" data-counter="counterup" data-value="'+rating[0].value1+'" >'+rating[0].value1+'</div>');
               }
               else
               {
               $('#cust_rate1').html( '<div class="col-lg-11" style="text-align:left;"> Average C-SAT Rating % :</div> <div class="col-lg-1" style="text-align:right;" data-counter="counterup" data-value="'+rating[0].value+'" >'+rating[0].value+'</div>');
               $('#cust_rate').html( '<div class="col-lg-11" style="text-align:left;"> Average C-SAT Rating % :</div> <div   class="col-lg-1"style="text-align:right;" data-counter="counterup" data-value="'+rating[0].value1+'" >'+rating[0].value1+'</div>');
               }
                       }
                   });
		});
				   
            function area() {
                var company_id = "<?php echo $company_id;?>";
                var region = $('#region').val();
                var area = $('#area').val();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_location",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'region': region,
                        'area': area
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#location').html(' <option selected  value="">All Location</option>');
                        console.log(data);
                        for (i = 0; i < data.length; i++) {
                            $('#location').append('<option value="' + data[i].location + '">' + data[i].location + '</option>');
                        }
                    }
                });
            }
		
 		function frm_inver() {
            $('#tbdy_frm').empty();
	 			$('.datatable').DataTable().Destroy();
          	var company_id="<?php echo $this->session->userdata('companyid');?>";
            var filter = $('#frm_filter').val();
	
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/change_frm",
                type: 'POST',
                data: {
                    'filter': filter,
                    'comp': company_id
                },
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if (data.length < 1) {                       
                        $('#tbdy_frm').html('<tr><td colspan=5><b><i>No records found</i></b></td></tr>');
						 //$('#tbdy_frm').empty();
						 $('.datatable').DataTable({"order": []});
                    } 
				else {
                        for (i = 0; i < data.length; i++) {
                          //alert(data[i].drop_spare);
                            var data1 = JSON.stringify(data[i].drop_spare);
                            data1 = JSON.parse(data1);
                            var spare = JSON.stringify(data1);
                            var spare3 = spare.replace(/\//g, "");
                            var spare2 = spare3.replace(/ /g, "");
                            var spare4 = spare2.replace(/\n/g, "");
                            var spare1 = spare4.replace(/\//g, "");
                            console.log(spare1);
							//var sparee=JSON.parse(spare1);
							//console.log(sparee);
                           // var cat_name = data[i].cat_name;
                            //var cat_name = cat_name.replace(/ /g, ":");
                            $('#tbdy_frm').append('<tr><td>' + data[i].ticket_id + '</td><td>' + data[i].employee_id + '</td><td><button class="btn btn-circle coral btn-outline btn-sm" id="' + data[i].ticket_id + '" onclick=hover_frmspare(this.id,' + spare1 + ');>View</button></td><td>' + data[i].drop_of_date + '</td><td>' + data[i].total + '</td></tr>');

                        }
				
                    }
						$('.datatable').DataTable({"order": []});
                }
            });
        }
		
            function region1() {
                var company_id = "<?php echo $company_id;?>";
                var region = $('#region1').val();
                var area = $('#area1').val();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_area",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'region': region,
                        'area': area
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#area1').html(' <option selected  value="">All Area</option>');
                        console.log(data);
                        for (i = 0; i < data.length; i++) {
                            $('#area1').append('<option value="' + data[i].town + '">' + data[i].town + '</option>');
                        }
                    }
                });
            }

            function area1() {
                var company_id = "<?php echo $company_id;?>";
                var region = $('#region1').val();
                var area = $('#area1').val();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_location",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'region': region,
                        'area': area
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#location1').html(' <option selected  value="">All Location</option>');
                        console.log(data);
                        for (i = 0; i < data.length; i++) {
                            $('#location1').append('<option value="' + data[i].location + '">' + data[i].location + '</option>');
                        }
                    }
                });
            }

            function region2() {
                var company_id = "<?php echo $company_id;?>";
                var region = $('#region2').val();
                var area = $('#area2').val();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_area",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'region': region,
                        'area': area
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#area2').html(' <option selected  value="">All Area</option>');
                        console.log(data);
                        for (i = 0; i < data.length; i++) {
                            $('#area2').append('<option value="' + data[i].town + '">' + data[i].town + '</option>');
                        }
                    }
                });
            }

            function area2() {
                var company_id = "<?php echo $company_id;?>";
                var region = $('#region2').val();
                var area = $('#area2').val();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_location",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'region': region,
                        'area': area
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#location2').html(' <option selected  value="">All Location</option>');
                        console.log(data);
                        for (i = 0; i < data.length; i++) {
                            $('#location2').append('<option value="' + data[i].location + '">' + data[i].location + '</option>');
                        }
                    }
                });
            }

            function region3() {
                var company_id = "<?php echo $company_id;?>";
                var region = $('#region3').val();
                var area = $('#area3').val();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_area",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'region': region,
                        'area': area
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#area3').html(' <option selected  value="">All Area</option>');
                        console.log(data);
                        for (i = 0; i < data.length; i++) {
                            $('#area3').append('<option value="' + data[i].town + '">' + data[i].town + '</option>');
                        }
                    }
                });
            }

            function area3() {
                var company_id = "<?php echo $company_id;?>";
                var region = $('#region3').val();
                var area = $('#area3').val();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_location",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'region': region,
                        'area': area
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#location3').html(' <option selected  value="">All Location</option>');
                        console.log(data);
                        for (i = 0; i < data.length; i++) {
                            $('#location3').append('<option value="' + data[i].location + '">' + data[i].location + '</option>');
                        }
                    }
                });
            }
            /* function day() {
                var company_id = "<?php echo $company_id;?>";
                var filter = $('#day').val();
                var product_id = $('#product_id').val();
                var region = $('#region').val();
                var area = $('#area').val();
                var location = $('#location').val();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_assigned",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'filter': filter,
                        'product_id': product_id,
                        'region': region,
                        'area': area,
                        'location': location
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#tbody_assigned').html('');
                        console.log(data);
                        if (data.length < 1) {
                            $('#tbody_assigned').html('<tr><td colspan=9><b><i>No records found</i></b></td></tr>');
                        } else {

                            for (i = 0; i < data.length; i++) {
                                var replaceSpace = data[i].prob_desc;
                                var problem = replaceSpace.replace(/ /g, ":");
                                var location = data[i].location;
                                var location = location.replace(/ /g, ":");
                                var product_name = data[i].product_name;
                                var product_name = product_name.replace(/ /g, ":");
                                var cat_name = data[i].cat_name;
                                var cat_name = cat_name.replace(/ /g, ":");
                                var param2 = data[i].customer_name + '&' + data[i].location + '&' + data[i].product_name + '&' + data[i].cat_name + '&' + problem;
                                $('#tbody_assigned').append('<tr><td id="' + data[i].ticket_id + '" style="cursor: pointer;" onclick=hover_ticket(this.id,"' + data[i].customer_name + '","' + location + '","' + product_name + '","' + cat_name + '","' + problem + '");>' + data[i].ticket_id + '</td><td id="' + data[i].employee_id + '" style="cursor: pointer;" onclick=hover(this.id,"' + data[i].first_name + '","' + data[i].email_id + '","' + data[i].contact_number + '","' + data[i].skill_level + '","' + data[i].today_task_count + '");>' + data[i].employee_id + '</td><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td>' + data[i].assigned_time + '</td><td><button id="' + data[i].ticket_id + '" class="btn btn-default red-stripe" onclick=redirect(this.id,"' + data[i].product_id + '","' + data[i].cat_id + '","' + location + '","' + data[i].tech_id + '");>Re-assign</button></td></tr>');
                            }
                        }
                    }
                });
            } */
           
        $('#revenue').change(function() {
            $('#amc_new_day').empty();
            $('#amc_new_day').append($("<option value='today'>Today</option>"));
            $('#amc_new_day').append($("<option value='week'>Weekly</option>"));
            $('#amc_new_day').append($("<option value='month'>Monthly</option>"));
            $('#amc_new_day').append($("<option value='year'>Yearly</option>"));
            $('#amc_new_day').append($("<option selected value=''>All Tickets</option>"));
            var company_id = "<?php echo $company_id;?>";
            var filter = $('#amc_new_day option:selected').val();
            var revenue_value = $('#revenue option:selected').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/amc_day",
                type: 'POST',
                data: {
                    'company_id': company_id,
                    'filter': filter,
                    'revenue': revenue_value
                },
                dataType: "json",
                success: function(data) {
                    
                }
            });

        });
		function amc_new_day()
        {
			$('.datatable_s').DataTable().destroy();
            $('#modal_tech').empty();
            $('#tbl_billing').empty();
			
            var company_id = "<?php echo $company_id;?>";
            var filter = $('#amc_new_day option:selected').val();
            var revenue_value = $('#revenue option:selected').val();
            var product_id = $('#product_id1').val();
			var region = $('#region1').val();//alert(region);
			var area = $('#area1').val();
			var location = $('#location1').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/amc_day",
                type: 'POST',
                data: {
                    'company_id': company_id,
                    'filter': filter,
                    'revenue': revenue_value,
					'product_id':product_id,
					'region':region,
					'area':area,
					'location':location
                },
                success: function(data) {
                   var data = JSON.parse(data);
                    if (revenue_value== "amc_new" || revenue_value == "amc_ren") {
                        $('#modal_tech').append('<tr><th>Customer ID</th><th>Technician Id</th><th>Amc Type</th><th>Amount</th></tr>');
                        for (var i = 0; i < data.length; i++) {
                            $('#tbl_billing').append("<tr><td>" + data[i].cust_id + "</td><td>" + data[i].employee_id + "</td><td>" + data[i].call_type + "</td><td>" + data[i].total_amount + "</td></tr>");
                        }
                    } else if (revenue_value == "service_bill") {
                        $('#modal_tech').append('<tr><th>Ticket ID</th><th>Tech ID</th><th>Amount</th></tr>');
                        for (var i = 0; i < data.length; i++) {
                            $('#tbl_billing').append("<tr><td>" + data[i].ticket_id + "</td><td>" + data[i].employee_id + "</td><td>" + data[i].total_amount + "</td></tr>");
                        }
                    } else if (revenue_value == "spre_bill") {
                        $('#modal_tech').append('<tr><th>Ticket ID</th><th>Tech ID</th><th>Requested Spare(s)</th><th>Amount</th></tr>');
                        for (var i = 0; i < data.length; i++) {
                            /* var requested = JSON.stringify(data[i]['requested_spare']);
                            var req = JSON.parse(requested); */
                            // var data1 = JSON.stringify(data[i].requested_spare);
								/*	 data1 = JSON.parse(data1);
									var spare = JSON.stringify(data1);
									var spare3 = spare.replace(/\//g, "");
									var spare2 = spare3.replace(/ /g, "");
									var spare4 = spare2.replace(/\n/g, "");
									var spare1 = spare4.replace(/\//g, "");
									console.log(spare1); */
							 var data1 = data[i].requested_spare;
						
							var spare1 = data1.replace(/\\/g, "\\");
						
                            var spare2 = spare1.replace(/ /g, ""); 
						
                          /*
							 var spare = JSON.stringify(data1);spare=$.trim(spare);
                         var spare3 = spare.replace(/\//g, "");
                            var spare2 = spare3.replace(/ /g, "");
                            var spare4 = spare2.replace(/\n/g, "");
                            var spare1 = spare4.replace(/\//g, "");
                            var spare1 = spare.replace(/\\/g, "");
							var spare1 = spare1.replace(/\n/g, "")
                            var spare1 = spare1.replace(/"/g, "'"); 
                            var spare1 = spare1.replace(/ /g, ""); */
							
                           console.log(spare2);
							spare1=JSON.parse(spare1);
                            console.log(spare1);
							spare1=JSON.stringify(spare1);
                           
                          /* $('#tbl_billing').append("<tr><td>" + data[i].ticket_id + "</td><td>" + data[i].employee_id + "</td><td>" + data[i].spare1 + "</td><td>" + data[i].total_amount + "</td></tr>"); */
							
							 $('#tbl_billing').append('<tr><td>' + data[i].ticket_id + '</td><td id="' + data[i].employee_id + '" style="cursor: pointer;" onclick=hover_tech(this.id);>' + data[i].employee_id + '</td><td><button class="btn btn-circle coral btn-outline btn-sm" id="' + data[i].ticket_id + '" onclick=hover_spare(this.id,' + spare1 + ');>View</button></td><td>' + data[i].spare_cost + '</td></tr>');

                        }
                    }
					$('.datatable_s').DataTable({"order": []});
                }
            });
        }
       
function csat_day()
			   {
				    var company_id="<?php echo $company_id;?>";
				    var region3 = $('#region3').val();
					//alert(region3);
					var area3 = $('#area3').val();
					//alert(area3);
					var location3 = $('#location3').val();
					//alert(location3);
					var csat_day = $('#csat_day').val();
					//alert(csat_day);
				    var product_id3 = $('#product_id3').val();
					//alert(product_id3);
					$.ajax({
                       url: "<?php echo base_url();?>" + "index.php?/controller_manager/display_csat",
                       type: 'POST',
                       data: {
                           'company_id': company_id,'product_id':product_id3,'region':region3,'area':area3,'location':location3,'day':csat_day
                       },
                       dataType: "json",
                       success: function(data) {
                           //alert(data);
                           console.log(data);
                           if(data.length<1)
							 {
							 		//$('#tbdy_csat').html('<tr><td colspan=9>No records found</td></tr>');
							 		$('#tbdy_csat').empty();

							 }
							 else
							 {
							 $('#tbdy_csat').empty();
							 for(i=0;i<data.length;i++)
							 {
								var replaceSpace=data[i].prob_desc;
								var problem = replaceSpace.replace(/ /g, ":");	
								var location=data[i].location;
								var location= location.replace(/ /g, ":");	
								var product_name=data[i].product_name;
								var product_name= product_name.replace(/ /g, ":");	
								var cat_name=data[i].cat_name;
								var cat_name= cat_name.replace(/ /g, ":");	 
								var priority=data[i].priority;
								var priority= priority.replace(/ /g, ":");	
								var tech_location=data[i].tech_loc;
								var tech_location= tech_location.replace(/ /g, ":");	
								var call_tag=data[i].call_tag;
								var call_tag= call_tag.replace(/ /g, ":");
								var call_type=data[i].call_type;
								var call_type= call_type.replace(/ /g, ":");	
								if(data[i].cust_feedback=='')
								{
								data[i].cust_feedback='-';
								}
										
								$('#tbdy_csat').append('<tr><td>'+data[i].customer_id+'</td><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+data[i].customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'");>'+data[i].ticket_id+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");>'+data[i].employee_id+'</td><td>'+data[i].cust_feedback+'</td><td>'+data[i].cust_rating+'/10</td></tr>');	
							 }
                       }
					   }
                   });
			   }
	     function hover(tech_id,technician_name,tech_email,contact_number,location,skill,task_count)
         {
location= location.replace(/\:/g," ");	
         $('#modal_display1').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Technician ID</label><div class="col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+tech_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Name</label><div class=" col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+technician_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Email ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+tech_email+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Mobile </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+contact_number+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Skill</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+skill+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Ticket Count</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+task_count+'</div></div></div>');
         $('#myModal2').modal('show');
         }
         function hover_ticket(ticket_id,customer_name,location,product_name,cat_name,problem,priority,call_tag,call_type)
         {
         var replaceSpace=problem;
         problem = problem.replace(/\:/g," ");	
         var product_name=product_name;
         product_name= product_name.replace(/\:/g," ");	
         var location=location;
         location= location.replace(/\:/g," ");	
         var cat_name=cat_name;
         cat_name= cat_name.replace(/\:/g," ");	
         var priority=priority;
         priority= priority.replace(/\:/g," ");	call_tag= call_tag.replace(/\:/g," ");	call_type= call_type.replace(/\:/g," ");	
         $('#modal_display1').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Ticket ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Customer Name</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+customer_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Product-Category </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+product_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Sub-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cat_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Call-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_tag+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Service-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_type+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Problem</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;" style=" text-align: left;">'+problem+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Priority</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+priority+'</div></div></div>');
         $('#myModal2').modal('show');
         }
function region3() {
                       var company_id = "<?php echo $company_id;?>";
                       var region = $('#region3').val();
                       var area = $('#area3').val();
                       $.ajax({
                           url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_area",
                           type: 'POST',
                           data: {
                               'company_id': company_id,
                               'region': region,
                               'area': area
                           },
                           dataType: "json",
                           success: function(data) {
                               $('#area3').html(' <option selected  value="">Area</option>');
                               console.log(data);
                               for (i = 0; i < data.length; i++) {
                                   $('#area3').append('<option value="' + data[i].town + '">' + data[i].town + '</option>');
                               }
                           }
                       });
                   }
               
                   function area3() {
                       var company_id = "<?php echo $company_id;?>";
                       var region = $('#region3').val();
                       var area = $('#area3').val();
                       $.ajax({
                           url: "<?php echo base_url();?>" + "index.php?/controller_manager/load_location",
                           type: 'POST',
                           data: {
                               'company_id': company_id,
                               'region': region,
                               'area': area
                           },
                           dataType: "json",
                           success: function(data) {
                               $('#location3').html(' <option selected  value="">Location</option>');
                               console.log(data);
                               for (i = 0; i < data.length; i++) {
                                   $('#location3').append('<option value="' + data[i].location + '">' + data[i].location + '</option>');
                               }
                           }
                       });
                   }
               
        function day() {
            var company_id = "<?php echo $company_id;?>";
            var filter = $('#day').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/escallated2",
                type: 'POST',
                data: {
                    'company_id': company_id,
                    'filter': filter
                },
                dataType: "json",
                success: function(data) {
                    $('#tbdy_escallated').html('');
                    console.log(data);
                    if (data.length < 1) {
                        console.log(data);
                      //  $('#tbdy_escallated').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
                    } else {
                        console.log(data);
                        for (i = 0; i < data.length; i++) {
                            $('#tbdy_escallated').append('<tr><td>' + data[i].employee_id + '</td><td>' + data[i].first_name + '</td><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td>' + data[i].ticket_id + '</td></tr>');
                        }
                    }
                }
            });
        }
function frm_inver() {
            $('#tbdy_frm').empty();
          	var company_id="<?php echo $this->session->userdata('companyid');?>";
            var filter = $('#frm_filter').val();
	
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/change_frm",
                type: 'POST',
                data: {
                    'filter': filter,
                    'comp': company_id
                },
                dataType: "json",
                success: function(data) {
     $('#tbdy_frm').html('');
                    console.log(data);
                    if (data.length < 1) {
                        console.log(data);
                        $('#tbdy_frm').html('<tr><td colspan=5><b><i>No records found</i></b></td></tr>');
                    } 
				else {
                        console.log(data);

                        for (i = 0; i < data.length; i++) {
                          //alert(data[i].drop_spare);
                            var data1 = JSON.stringify(data[i].drop_spare);
                            data1 = JSON.parse(data1);
                            var spare = JSON.stringify(data1);
                            var spare3 = spare.replace(/\//g, "");
                            var spare2 = spare3.replace(/ /g, "");
                            var spare4 = spare2.replace(/\n/g, "");
                            var spare1 = spare4.replace(/\//g, "");
                            console.log(spare1);
							//var sparee=JSON.parse(spare1);
							//console.log(sparee);
                           // var cat_name = data[i].cat_name;
                            //var cat_name = cat_name.replace(/ /g, ":");
                            $('#tbdy_frm').append('<tr><td>' + data[i].ticket_id + '</td><td>' + data[i].employee_id + '</td><td><button class="btn btn-circle coral btn-outline btn-sm" id="' + data[i].ticket_id + '" onclick=hover_frmspare(this.id,' + spare1 + ');>View</button></td><td>' + data[i].drop_of_date + '</td><td>' + data[i].total + '</td></tr>');

                        }
                    }
                }
            });
        }
        function hover_tech(tech_id, first_name, skill_level, product_name, cat_name, problem) {
            cat_name = cat_name.replace(/\:/g, " ");
            $('#modal_tech form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Technician ID</label><div class="col-sm-6 control-label">' + tech_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Name</label><div class="col-sm-6 control-label">' + first_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Skill Level</label><div class="col-sm-6 control-label">' + skill_level + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Product Category </label><div class="col-sm-6 control-label">' + product_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Sub Category</label><div class="col-sm-6 control-label">' + cat_name + '</div></div>');
            $('#myModal1').modal('show');
        }

     function hover_spare(ticket_id,requested_spare)
	{
         $('#spare_details').empty(); 
            var data = requested_spare;
          //  console.log(data);
			 
			  if(data == "null"){
                               swal("No data");
                        }
                else{
               // var data = JSON.parse(data);
				 var spare = JSON.stringify(data);
                            var spare3 = spare.replace(/\//g, "");
                            var spare2 = spare3.replace(/ /g, "");
                            var spare4 = spare2.replace(/\n/g, "");
                            var spare1 = spare4.replace(/\//g, "");
							var spare1 = JSON.parse(spare1);
                            //console.log(spare1.length);
				for(i=0;i<data.length;i++)
				{
					//data[0].Spare_code;
				$('#spare_details').append('<tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr>');
				} 
						                             //console.log(data);
                                                      $('#myModal').modal('show');
                                                        }
       
                          
		}
		 function hover_frmspare(ticket_id, frm_spare) {
            $('#tbdy_frminventory').empty();
            //var data = requested_spare;
		var data = frm_spare;
			 //alert(data);
			 var data=JSON.parse(data);
                 console.log(data);
                for (i = 0; i < data.length; i++) {
                    $('#tbdy_frminventory').append('<tr><td>' + data[i].Spare_code + '</td><td>' + data[i].quantity + '</td><td>' + data[i].spare_source + '</td></tr>');
                }
                $('#frm_modal').modal('show');
        }


        function inver() {
            $('#tbdy_inventory').empty();
			$('.datatab2').DataTable().destroy();
            var company_id = "<?php echo $company_id;?>";
            var filter = $('#inver').val();
            var product_id = $('#product_id2').val();
			var region = $('#region2').val();
			var area = $('#area2').val();
			var location = $('#location2').val();
			var spare_bill = $('#spare_bill').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/inventory2",
                type: 'POST',
                data: {
					'companyid':company_id,
                    'filter': filter,
                    'spare_bill': spare_bill,
                    'product_id': product_id,
                    'region': region,
                    'area': area,
                    'location': location
                },
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if (data.length < 1) {
                         $('#tbdy_inventory').empty();
         // $('#tbdy_inventory').html('<tr><td colspan=5><b><i>No records found</i></b></td></tr>');
                    } else {
                        console.log(data);
                        //	data=JSON.parse(data);

                        for (i = 0; i < data.length; i++) {
                           var data1 = data[i].requested_spare;
						//	data1 = JSON.stringify(data1);
						//	data1 = JSON.parse(data1);
						//	data1 = JSON.stringify(data1);
						//	data1 = JSON.parse(data1);
                         //  data1 = JSON.stringify(data1);
						//	data1 = $.trim(data1);
						//	data1 = JSON.parse(data1);
						//	 var spare = JSON.stringify(data1);console.log(spare);
							var spare1 = data1.replace(/\\/g, "\\");
							//var spare1 = spare1.replace(/\n/g, "")
                           // var spare2 = spare1.replace(/"/g, "'"); 
                            var spare2 = spare1.replace(/ /g, ""); 
						
                          /*
							 var spare = JSON.stringify(data1);spare=$.trim(spare);
                         var spare3 = spare.replace(/\//g, "");
                            var spare2 = spare3.replace(/ /g, "");
                            var spare4 = spare2.replace(/\n/g, "");
                            var spare1 = spare4.replace(/\//g, "");
                            var spare1 = spare.replace(/\\/g, "");
							var spare1 = spare1.replace(/\n/g, "")
                            var spare1 = spare1.replace(/"/g, "'"); 
                            var spare1 = spare1.replace(/ /g, ""); */
							
                           console.log(spare2);
							spare1=JSON.parse(spare1);
                            console.log(spare1);spare1=JSON.stringify(spare1);
                            var cat_name = data[i].cat_name;
                         var cat_name = cat_name.replace(/ /g, ":");
                            $('#tbdy_inventory').append('<tr><td>' + data[i].ticket_id + '</td><td id="' + data[i].employee_id + '" style="cursor: pointer;" onclick=hover_tech(this.id,"' + data[i].first_name + '","' + data[i].skill_level + '","' + data[i].product_name + '","' + cat_name + '");>' + data[i].employee_id + '</td><td><button class="btn btn-circle coral btn-outline btn-sm" id="' + data[i].ticket_id + '" onclick=hover_spare(this.id,' + spare1 + ');>View</button></td><td>' + data[i].call_type + '</td><td>' + data[i].spare_cost + '</td></tr>');

                        }
                    }
					$('.datatab2').DataTable({"order": []});
                }
            }); 
        }

        function com_day() {
            $('#tbdy_completed').empty();
            var company_id = "<?php echo $company_id;?>";
			//alert(company_id);
            var product_id = $('#product_id').val();
			//alert(company_id);
            var region = $('#region').val();
			//alert(company_id);
            var area = $('#area').val();
            var location = $('#location').val();
            var filter = $('#com_day').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/completed2",
                type: 'POST',
                data: {
                    'filter': filter
                },
                dataType: "json",
                success: function(data) {
                    $('#tbdy_completed').html('');
                    console.log(data);
                    if (data.length < 1) {
                        console.log(data);
                        $('#tbdy_completed').html('<tr><td colspan=5><b><i>No records found</i></b></td></tr>');
                    } else {
                        console.log(data);
                        for (i = 0; i < data.length; i++) {
                            var set = data[i].cust_rating;
                            if (set == 1) {
                                var star = '*';
                            } else if (set == 2) {
                                var star = '* *';
                            } else if (set == 3) {
                                var star = '* * *';
                            } else if (set == 4) {
                                var star = '* * * *';
                            } else if (set == 5) {
                                var star = '* * * * *';
                            }

                            $('#tbdy_completed').append('<tr><td style="text-align:center">' + data[i].cust_id + '</td><td style="text-align:center">' + data[i].ticket_id + '</td><td  style="text-align:center"id="' + data[i].employee_id + '" style="cursor: pointer;" onclick=hover_tech(this.id,"' + data[i].first_name + '","' + data[i].skill_level + '","' + data[i].product_name + '","' + data[i].cat_name + '");>' + data[i].employee_id + '</td><td style="text-align:center">' + star + '</td><td style="text-align:center">' + data[i].cust_feedback + '</td></tr>');
                        }
                    }
                }
            });
        }
		 function att_day() {
			 $('.datatable1').DataTable().destroy();
             $('#tbdy_attendance').empty();
            var company_id = "<?php echo $company_id;?>";
			//alert(company_id);
            var product_id = $('#product_id').val();
			//alert(company_id);
            var region = $('#region').val();
			//alert(company_id);
            var area = $('#area').val();
            var location = $('#location').val();
            var days_filter = $('#days_filter').val();
              $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/controll_attendance",
                type: 'POST',
                data: {
                    'company_id': company_id,
                    'product_id': product_id,
                    'region': region,
                    'area': area,
                    'location': location,
                    'days_filter' : days_filter
                },
                dataType: "json",
                success: function(data) {
         
         if(data.length<1)
         {
         $('#tbdy_attendance').empty();
         }
         else
         {	       
            $('#tbdy_attendance').empty();  

        //  $('#modal_atten').html('');
         
        //  $('#modal_atten').append('<tr><th>Technician Id</th><th>Technician Name</th><th>Location</th><th>Availability</th><th>Date</th<th>In Time</th><th>Out Time</th></tr>');	 
		      for(i=0;i<data.length;i++)        
		 {
			 if(data[i].available==1)
			 {
				 available="Available";
			 }
			 else{
				 available="Unavailable";
			 }
         $('#tbdy_attendance').append('<tr><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover_tech(this.id,"'+data[i].first_name+'","'+data[i].skill_level+'","'+data[i].product_name+'","'+data[i].cat_name+'");>'+data[i].employee_id+'</td><td>'+data[i].first_name+'</td><td>'+data[i].current_location+'</td><td>'+available+'</td><td>'+data[i].date+'</td><td>'+data[i].in_time+'</td><td>'+data[i].out_time+'</td>');
         }
         } 
					$('.datatable1').DataTable({"order": []});
         } 
       }); 
		 }
</script>
</body>
</html>
