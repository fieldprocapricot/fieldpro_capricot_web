<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php 
               $company_id=$this->session->userdata('companyid');
               include 'assets/lib/cssscript.php'?>
            <style>
               .jstree-anchor{
               min-height:110px !important;
               }
               .jstree-icon.jstree-themeicon.jstree-themeicon-custom{
               height:100px;
               width:150px;
               }
               .jstree-default .jstree-anchor {
               //line-height: 6 !important;
               margin-bottom:10%;
               margin-top:2%;
               text-align: center;
               }
               .jstree-icon.jstree-themeicon.jstree-themeicon-custom {
               margin: 8px 0px !important;
               }
               .jstree-node{
               //margin:3px 0px;
               }
               #tree_5{
               padding:2%;
               }
               .jstree-children{
               margin:0% 2%;
               }
               .jstree-icon.jstree-themeicon.jstree-themeicon-custom{
               display:block !important;
               }
               .sweet-alert.showSweetAlert.visible{
               z-index: 999999999 !important;
               }
               .jstree-default .jstree-anchor {
               line-height: 1 !important;
               }
               .jstree-checkbox{
               float: left !important;
               }	
               .dt-buttons{
               display:none !important;
               }
               
               .fileinput-new{
               color:#000 !important;
               }	
               .portlet-title3 {
               text-align: center;
               background: #32c5d2;
               color: #fff;
               padding-top: 6px !important;
               }
               .close {
               width: 18px;
               height: 18px;
               }
               .portlet.light.bordered {
               border: 1px solid #16aeae !important;
               }
               .selected{
               box-shadow:0px 12px 22px 1px #333;
               }
               .portlet.box.blue-hoki {
               border: 1px solid #869ab3;
               /*border-top: 0;*/
               }
               /*.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover, .nav > li > a:hover {
               background:none;
               }*/
               #frame{
               height: 150px; /* equals max image height */
               width: 160px;
               border: 1px solid #0a1824;
               white-space: nowrap;
               text-align: center; 
               margin: 1em 0;
               }
               .helper{
               display: inline-block;
               height: 100%;
               vertical-align: middle;
               }
               #frame img{
               vertical-align: middle;
               max-height: 150px;
               max-width: 160px;
               width:100%;
               height:auto;
               }
               .product-checkbox2{
               margin: 0 4px -22px 0px !important;
               }
            </style>
            <link href="assets/global/plugins/jquerysctipttop.css" rel="stylesheet" type="text/css" />
         </head>
         <!-- END HEAD -->
         <body class="page-header-fixed page-sidebar-closed-hide-logo">
            <!-- BEGIN CONTAINER -->
            <div class="wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header.php"?>
               <!-- END HEADER -->
               <div class="container-fluid">
                  <div class="page-content">
                     <!-- BEGIN BREADCRUMBS -->
                     <!-- END BREADCRUMBS -->
                     <!-- BEGIN PAGE BASE CONTENT -->
                     <div class="page-content-container">
                        <div class="page-content-row">
                           <div class="page-content-col">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">SLA Mapping</div>
                                    <div class="tools"> </div>
                                 </div>
                                 <div class="portlet-body">
                                    <div class="portlet light bordered">
                                       <div class="portlet-title tabbable-line">
                                          <div class="caption">
                                          </div>
                                          <ul class="nav nav-tabs">
                                             <li class="active">
                                                <a href="#portlet_tab1" data-toggle="tab"> SLA Mapping </a>
                                             </li>
                                             <li>
                                                <a href="#portlet_tab2" data-toggle="tab"> Last Update </a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="portlet-body">
                                          <div class="tab-content">
                                             <div class="tab-pane active" id="portlet_tab1">
                                                <div class="table-toolbar">
                                                   <div class="row pull-right">
                                                      <div class="col-md-6">
                                                         <div class="btn-group">
                                                            <button id="sample_editable_1_new" class="btn btn-circle green-haze btn-outline btn-md" onclick="update_check();">
                                                            <i class="fa fa-level-up"></i> Update SLA
                                                            </button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="container-fluid" id="div_align">
                                                   <div class="col-md-10 col-md-offset-1">
                                                      <div class="portlet box blue-hoki">
                                                         <div class="portlet-title">
                                                            <div class="caption pull-right">
                                                               <h5 style="margin:0"></h5>
                                                            </div>
                                                         </div>
                                                         <div class="portlet-body">
                                                            <div class="row">
                                                               <div class="col-md-4 col-md-offset-8 col-sm-12">
                                                                  <!-- <select class="form-control" id="target">
                                                                     </select> -->
                                                               </div>
                                                            </div>
                                                            <!--<div id="tree_5" class="tree-demo"></div>-->
                                                            <div class="tabbable tabs-left company_div">
                                                               <div class="tab-content tab-height" id="category_append" style="display:none"></div>
                                                               <div class="col-sm-3 parent-left">
                                                                  <ul class="nav nav-tabs product-tab-left " id="company">
                                                                  </ul>
                                                               </div>
                                                               <div class="col-sm-9 tab-sla-page">
                                                                  <div class="tab-content tab-height" id="product_append">
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="tab-pane" id="portlet_tab2">
                                                <div class="table=responsive">
                                                   <table class="table table-hover" id="sample_2">
                                                      <thead>
                                                         <tr>
                                                            <th style="text-align:center">Priority Level</th>
                                                            <th>Product Category</th>
                                                            <th>Sub Category</th>
                                                            <th>Last Update</th>
                                                            <th>Action</th>
                                                            <!-- 
                                                               <th>Editable Rights</th> -->
                                                         </tr>
                                                      </thead>
                                                      <tbody id="sla_views">
                                                         <?php
                                                            foreach ($get_sla->result() as $row) {
                                                                    ?>
                                                         <tr>
                                                            <td style="text-align:center"><a onclick=view_ref(this.id,"<?php echo $row->response_time?>","<?php echo $row->resolution_time?>","<?php echo $row->acceptance_time?>","<?php echo $row->SLA_Compliance_Target?>","<?php echo $row->mttr_target?>");><?php
                                                               if($row->b=="all"){
                                                               	echo "All Priority Levels";
                                                               }
                                                               else{
                                                               echo $row->b;} ?></a></td>
                                                            <td><?php
                                                               if($row->product=="all"){
                                                               	echo "All Product Categories";
                                                               }	
                                                               else{														echo $row->product_name; 
                                                               }?></td>
                                                            <td><?php 
                                                               if($row->category=="all"){
                                                               	echo "All Sub Categories";
                                                               	}
                                                               	else
                                                               	{
                                                               echo $row->cat_name;
                                                               	}															?></td>
                                                            <td><?php echo $row->d; ?></td>
                                                            <td><span><?php if( $row->editable_rights=='1')
                                                               {?><button class="btn btn-circle dark btn-outline btn-sm" id="<?php echo $row->ref_id?>" onclick=edit_ref(this.id,"<?php echo $row->response_time?>","<?php echo $row->resolution_time?>","<?php echo $row->acceptance_time?>","<?php echo $row->SLA_Compliance_Target?>","<?php echo $row->mttr_target?>");>Edit</button></span>
                                                               <span><button class="btn btn-circle red btn-outline btn-sm"  onclick=delete_ref("<?php echo $row->ref_id?>");>Delete</button></span><?php }
                                                                  else{
                                                                  	echo "No Editable Rights";
                                                                  	}?>
                                                            </td>
                                                         </tr>
                                                         <?php } ?>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>
                     <!-- END PAGE BASE CONTENT -->
                  </div>
                  <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
               </div>
            </div>
            <!--Modal Starts-->
            <!-- Modal -->
            <div id="myModal2" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Sub Category</h4>
                        <div class="error" style="display:none">
                           <label id="rowdata_category"></label>
                        </div>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" id="add_subproduct">
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">Sub Category Id:</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="subproduct_id" name="subproduct_id" readonly>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">product Name:</label>
                              <div class="col-sm-8">
                                 <select class="form-control" id="product_names" name="product_names">
                                    <option value="" selected disabled>Select Product</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">Sub Category Name:</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="subproduct_name" name="subproduct_name" placeholder="Sub Category Name">
                              </div>
                           </div>
                           <div class="form-group" style="display:none">
                              <label class="control-label col-sm-4" for="email">Sub Category Modal No:</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="subproduct_modal" name="subproduct_modal" placeholder="Sub Category Modal No">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">Sub Category Description:</label>
                              <div class="col-sm-8">
                                 <textarea class="form-control" rows="5" id="subproduct_desc" name="subproduct_desc" style="resize:none"></textarea>
                              </div>
                           </div>
                           <div class="form-group" style="display:none">
                              <label class="control-label col-sm-4" for="email">company id:</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="companyid" name="companyid" value="<?php echo $this->session->userdata('companyid');?>" />
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-4" for="email">Sub Category Image:</label>
                              <div class="col-sm-8">
                                 <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group input-large">
                                       <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                          <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                          <span class="fileinput-filename"> </span>
                                       </div>
                                       <span class="input-group-addon btn default btn-file">
                                       <span class="fileinput-new"> Select file </span>
                                       <span class="fileinput-exists"> Change </span>
                                       <input name="subproduct_image" id="subproduct_image" type="file"> </span>
                                       <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="submit" class="btn btn-circle blue btn-outline" id="addsubproduct"><i class="fa fa-check"></i> Submit</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div id="myModal_sla" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">SLA Mapping</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id=''>
                           <div class="form-wizard">
                              <div class="form-body">
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Priority Level</label>
                                    <div class="col-lg-8">
                                       <select class="form-control" id="priority_1" name="priority_1">
                                          <option value="" selected disabled>Select Priority Level</option>
                                          <option value="P1">P1</option>
                                          <option value="P2">P2</option>
                                          <option value="P3">P3</option>
                                          <option value="P4">P4</option>
                                          <option value="all">All Priority</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Resolution Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="reso_hour_1" name="reso_hour_1">
                                          <option value="" selected disabled>Select Resolution Hour</option>
                                          <?php 
                                             for($i=00;$i<=11;$i++){                                                			
                                             ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="reso_minute_1" name="reso_minute_1">
                                          <option value="" selected disabled>Select Resolution Minute</option>
                                          <?php 
                                             for($i=00;$i<=59;$i++){                                                			
                                             ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Response Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="res_hour_1" name="res_hour_1">
                                          <option value="" selected disabled>Select Response Hour</option>
                                          <?php 
                                             for($i=00;$i<=11;$i++){                                                			
                                             ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="res_minute_1" name="res_minute_1">
                                          <option value="" selected disabled>Select Response Minute</option>
                                          <?php 
                                             for($i=00;$i<=59;$i++){                                                			
                                             ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Acceptance Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="acc_hour_1" name="acc_hour_1">
                                          <option value="" selected disabled>Select Acceptance Hour</option>
                                          <?php 
                                             for($i=00;$i<=11;$i++){                                                			
                                             ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="acc_minute_1" name="acc_minute_1">
                                          <option value="" selected disabled>Select Acceptane Minute</option>
                                          <?php 
                                             for($i=00;$i<=59;$i++){                                                			
                                             ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">SLA Complaince Target (in %)</label>
                                    <div class="col-lg-8">
                                       <select class="form-control" id="sla_target_1" name="sla_target_1">
                                          <option value="" selected disabled>Select Sla Target</option>
                                          <?php 
                                             for($i=00;$i<=100;$i++){                                                			
                                             ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php } ?>
                                       </select>
                                       <!-- <input type="text" class="form-control" id="sla_target" name="sla_target" placeholder="Accepts only Digits "></input> -->
                                    </div>
                                 </div>
                                 <span class="clearfix"></span>							 
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">MTTR Target</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="mttr_target_hour_1" name="mttr_target_hour_1">
                                          <option value="" selected disabled>MTTR Target Hour</option>
                                          <?php 
                                             for($i=00;$i<=11;$i++){                                                			
                                             ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="mttr_target_minute_1" name="mttr_target_minute_1">
                                          <option value="" selected disabled>MTTR Target Minute</option>
                                          <?php 
                                             for($i=00;$i<=59;$i++){                                                			
                                             ?>
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" onclick="check('priority_1','reso_hour_1','reso_minute_1','res_hour_1','res_minute_1','acc_hour_1','acc_minute_1','sla_target_1','mttr_target_hour_1','mttr_target_minute_1');" >Submit</button>
                     </div>
                  </div>
               </div>
            </div>
            <div id="sla_modal" class="modal fade" role="dialog">
               <div class="modal-dialog" style="width:1200px">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-body">
                        <div class="portlet light bordered" id="form_wizard_1" style="margin-bottom:0">
                           <div class="portlet-title portlet-title3">
                              <label class="control-label" style="font-size: 20px !important;">Set SLA Target</label>
                              <button type="button" class="close pull-right" data-dismiss="modal">&times;</button> 
                              <div class="error" style="display:none">
                                 <label id="rowdata"></label>
                              </div>
                           </div>
                           <div class="portlet-body form">
                              <form class="form-horizontal" id="submit_form">
                                 <div class="form-wizard">
                                    <div class="form-body">
                                       <div class="form-group">
                                          <label class="control-label col-lg-3">Sub Category</label>
                                          <div class="col-lg-8">
                                             <input type="text" class="form-control" value="" id="cate_id" name="cate_id" readonly></input>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="control-label col-lg-3">Product Category</label>
                                          <div class="col-lg-8">
                                             <input type="text" class="form-control" value="" id="prod_id" name="prod_id" readonly></input>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="control-label col-lg-3">Company ID</label>
                                          <div class="col-lg-8">
                                             <input type="text" class="form-control" value="" id="company_id" name="company_id" readonly></input>
                                          </div>
                                       </div>
                                       <span class="clearfix"></span>
                                       <div class="col-sm-1"></div>
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <label class="control-label col-lg-6">Priority Level</label>
                                             <div class="col-lg-6">
                                                <input type="text" class="form-control" value="P1" readonly ></input>
                                                <?php /*?>
                                                <select class="form-control" id="priority" name="priority">
                                                   <option value="" selected disabled>Select Priority Level</option>
                                                   <?php 
                                                      foreach($priority_level->result() as $row){
                                                      ?>	
                                                   <option value="<?php echo $row->priority_level ?>"><?php echo $row->priority_level ?></option>
                                                   <?php }
                                                      ?>
                                                </select>
                                                <?php */?>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class="control-label col-lg-6">Resolution Time</label>
                                             <div class="col-lg-3">
                                                <select class="form-control" id="reso_hour" name="reso_hour">
                                                   <option value="" selected disabled>Select Resolution Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-3">
                                                <select class="form-control" id="reso_minute" name="reso_minute">
                                                   <option value="" selected disabled>Select Resolution Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class="control-label col-lg-6">Response Time</label>
                                             <div class="col-lg-3">
                                                <select class="form-control" id="res_hour" name="res_hour">
                                                   <option value="" selected disabled>Select Response Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-3">
                                                <select class="form-control" id="res_minute" name="res_minute">
                                                   <option value="" selected disabled>Select Response Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class="control-label col-lg-6">Acceptance Time</label>
                                             <div class="col-lg-3">
                                                <select class="form-control" id="acc_hour" name="acc_hour">
                                                   <option value="" selected disabled>Select Acceptance Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-3">
                                                <select class="form-control" id="acc_minute" name="acc_minute">
                                                   <option value="" selected disabled>Select Acceptane Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class="control-label col-lg-6">SLA Complaince Target (in %)</label>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="sla_target" name="sla_target">
                                                   <option value="" selected disabled>Select Sla Target</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                                <!-- <input type="text" class="form-control" id="sla_target" name="sla_target" placeholder="Accepts only Digits "></input> -->
                                             </div>
                                          </div>
                                          <span class="clearfix"></span>							 
                                          <div class="form-group">
                                             <label class="control-label col-lg-6">MTTR Target</label>
                                             <div class="col-lg-3">
                                                <select class="form-control" id="mttr_target_hour" name="mttr_target_hour">
                                                   <option value="" selected disabled>MTTR Target Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-3">
                                                <select class="form-control" id="mttr_target_minute" name="mttr_target_minute">
                                                   <option value="" selected disabled>MTTR Target Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-2">
                                          <div class="form-group">
                                             <div class="col-lg-12">
                                                <input type="text" class="form-control" value="P2" readonly ></input>
                                                <?php /*?>
                                                <select class="form-control" id="priority" name="priority">
                                                   <option value="" selected disabled>Select Priority Level</option>
                                                   <?php 
                                                      foreach($priority_level->result() as $row){
                                                      ?>	
                                                   <option value="<?php echo $row->priority_level ?>"><?php echo $row->priority_level ?></option>
                                                   <?php }
                                                      ?>
                                                </select>
                                                <?php */?>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="reso_hour" name="reso_hour">
                                                   <option value="" selected disabled>Select Resolution Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="reso_minute" name="reso_minute">
                                                   <option value="" selected disabled>Select Resolution Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="res_hour" name="res_hour">
                                                   <option value="" selected disabled>Select Response Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="res_minute" name="res_minute">
                                                   <option value="" selected disabled>Select Response Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="acc_hour" name="acc_hour">
                                                   <option value="" selected disabled>Select Acceptance Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="acc_minute" name="acc_minute">
                                                   <option value="" selected disabled>Select Acceptane Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12">
                                                <select class="form-control" id="sla_target" name="sla_target">
                                                   <option value="" selected disabled>Select Sla Target</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                                <!-- <input type="text" class="form-control" id="sla_target" name="sla_target" placeholder="Accepts only Digits "></input> -->
                                             </div>
                                          </div>
                                          <div style="margin-top:30px;"></div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="mttr_target_hour" name="mttr_target_hour">
                                                   <option value="" selected disabled>MTTR Target Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="mttr_target_minute" name="mttr_target_minute">
                                                   <option value="" selected disabled>MTTR Target Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-2">
                                          <div class="form-group">
                                             <div class="col-lg-12">
                                                <input type="text" class="form-control" value="P3" readonly ></input>
                                                <?php /*?>
                                                <select class="form-control" id="priority" name="priority">
                                                   <option value="" selected disabled>Select Priority Level</option>
                                                   <?php 
                                                      foreach($priority_level->result() as $row){
                                                      ?>	
                                                   <option value="<?php echo $row->priority_level ?>"><?php echo $row->priority_level ?></option>
                                                   <?php }
                                                      ?>
                                                </select>
                                                <?php */?>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="reso_hour" name="reso_hour">
                                                   <option value="" selected disabled>Select Resolution Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="reso_minute" name="reso_minute">
                                                   <option value="" selected disabled>Select Resolution Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="res_hour" name="res_hour">
                                                   <option value="" selected disabled>Select Response Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="res_minute" name="res_minute">
                                                   <option value="" selected disabled>Select Response Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="acc_hour" name="acc_hour">
                                                   <option value="" selected disabled>Select Acceptance Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="acc_minute" name="acc_minute">
                                                   <option value="" selected disabled>Select Acceptane Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12">
                                                <select class="form-control" id="sla_target" name="sla_target">
                                                   <option value="" selected disabled>Select Sla Target</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	?>
                                                </select>
                                                <!-- <input type="text" class="form-control" id="sla_target" name="sla_target" placeholder="Accepts only Digits "></input> -->
                                             </div>
                                          </div>
                                          <div style="margin-top:30px;"></div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="mttr_target_hour" name="mttr_target_hour">
                                                   <option value="" selected disabled>MTTR Target Hour</option>
                                                   <?php 
                                                     for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="mttr_target_minute" name="mttr_target_minute">
                                                   <option value="" selected disabled>MTTR Target Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-2">
                                          <div class="form-group">
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="reso_hour" name="reso_hour">
                                                   <option value="" selected disabled>Select Resolution Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="reso_minute" name="reso_minute">
                                                   <option value="" selected disabled>Select Resolution Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="res_hour" name="res_hour">
                                                   <option value="" selected disabled>Select Response Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="res_minute" name="res_minute">
                                                   <option value="" selected disabled>Select Response Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="acc_hour" name="acc_hour">
                                                   <option value="" selected disabled>Select Acceptance Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="acc_minute" name="acc_minute">
                                                   <option value="" selected disabled>Select Acceptane Minute</option>
                                                   <?php 
                                                      for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12">
                                                <select class="form-control" id="sla_target" name="sla_target">
                                                   <option value="" selected disabled>Select Sla Target</option>
                                                   <?php 
                                                      for($i=00;$i<=100;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                                <!-- <input type="text" class="form-control" id="sla_target" name="sla_target" placeholder="Accepts only Digits "></input> -->
                                             </div>
                                          </div>
                                          <div style="margin-top:30px;"></div>
                                          <div class="form-group">
                                             <div class="col-lg-6">
                                                <select class="form-control" id="mttr_target_hour" name="mttr_target_hour">
                                                   <option value="" selected disabled>MTTR Target Hour</option>
                                                   <?php 
                                                      for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                             <div class="col-lg-6">
                                                <select class="form-control" id="mttr_target_minute" name="mttr_target_minute">
                                                   <option value="" selected disabled>MTTR Target Minute</option>
                                                   <?php 
                                                     for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }	 ?>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <!--<hr>-->
                                       <span class="clearfix"></span>
                                       <div class="form-group">
                                          <label class="control-label col-lg-6">Editable Rights to Admin</label>
                                          <div class="col-lg-6">
                                             <label class="radio-inline">
                                             <input type="radio" name="editable_right" value="yes">Yes
                                             </label>
                                             <label class="radio-inline">
                                             <input type="radio" name="editable_right" value="no">No
                                             </label>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       <div class="row">
                                          <div class="col-md-12">
                                             <div class="col-md-7 pull-right">
                                                <button type="button" class="btn btn-outline green btn-circle btn-md" id="submit_sla">Submit</button>
                                                <button type="cancel" class="btn red-haze btn-outline btn-circle btn-md" data-dismiss="modal">Cancel</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="view_sla" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" id="view_prioritys">
                           <div class="form-body">
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Response Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_time" name="r_time">
                                          <?php 
                                             for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										  ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_time_minute" name="reso_minute_1">
                                          <?php 
                                             for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										  ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Resolution Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_levels" name="res_hour_1">
                                          <?php 
                                             for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_levels_minute" name="res_minute_1">
                                          <?php 
                                             for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Acceptance Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="a_time" name="acc_hour_1">
                                          <?php 
                                             for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="a_time_minute" name="acc_minute_1">
                                          <?php 
                                             for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">SLA Complaince Target (in %)</label>
                                    <div class="col-lg-8">
                                       <select class="form-control" id="sla_targetss" name="sla_target_1">
                                          <?php 
                                             for($i=00;$i<=100;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                       <!-- <input type="text" class="form-control" id="sla_target" name="sla_target" placeholder="Accepts only Digits "></input> -->
                                    </div>
                                 </div>
                                 <span class="clearfix"></span>							 
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">MTTR Target</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="m_targets" name="mttr_target_hour_1">
                                          <?php 
                                             for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="m_targets_minute" name="mttr_target_minute_1">
                                          <?php 
                                             for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" onclick="edit_sla_modal();" >Submit</button><button type="cancel" class="btn red-haze btn-outline btn-circle btn-md" data-dismiss="modal">Cancel</button>
                        <input type="hidden" class="btn btn-circle blue btn-outline btn-sm" id="hidden_ref_id" >
                     </div>
                  </div>
               </div>
            </div>
            <div id="view_ref" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" id="view_prioritys">
                           <div class="form-body">
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Response Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_time" name="r_time">
                                          <?php 
                                             for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										  ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_time_minute" name="reso_minute_1">
                                          <?php 
                                             for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										  ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Resolution Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_levels" name="res_hour_1">
                                          <?php 
                                             for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="r_levels_minute" name="res_minute_1">
                                          <?php 
                                             for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">Acceptance Time</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="a_time" name="acc_hour_1">
                                          <?php 
                                             for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="a_time_minute" name="acc_minute_1">
                                          <?php 
                                             for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">SLA Complaince Target (in %)</label>
                                    <div class="col-lg-8">
                                       <select class="form-control" id="sla_targetss" name="sla_target_1">
                                          <?php 
                                             for($i=00;$i<=100;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                       <!-- <input type="text" class="form-control" id="sla_target" name="sla_target" placeholder="Accepts only Digits "></input> -->
                                    </div>
                                 </div>
                                 <span class="clearfix"></span>							 
                                 <div class="form-group">
                                    <label class="control-label col-lg-4">MTTR Target</label>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="m_targets" name="mttr_target_hour_1">
                                          <?php 
                                             for($i=00;$i<=11;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                    <div class="col-lg-4">
                                       <select class="form-control" id="m_targets_minute" name="mttr_target_minute_1">
                                          <?php 
                                             for($i=00;$i<=59;$i++){  if($i<10){
												?>
                                          <option value="0<?php print_r($i) ?>">0<?php print_r($i) ?></option>
                                          <?php }else{ ?> 
                                          <option value="<?php print_r($i) ?>"><?php print_r($i) ?></option>
                                          <?php }
											 }										   ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                        </form>
                     </div>
                     <div class="modal-footer"><button type="cancel" class="btn red-haze btn-outline btn-circle btn-md" data-dismiss="modal">OK</button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <!--Modal End-->
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?>  
            <script>        		
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
               $('#sla_admin').addClass('open'); 
            </script>
            <script>
			function edit_ref(ref_id,response_time,resolution_time,acceptance_time,SLA_Compliance_Target,mttr_target)
               {
				   var res_time=response_time.split(":");
				   var resol_time=resolution_time.split(":");
				   var acc_time=acceptance_time.split(":");
				   var mttr_t=mttr_target.split(":");
				   $('#r_time').append('<option value="'+res_time[0] +'" selected >'+res_time[0]+'</option>');
				   $('#r_time_minute').append('<option value="'+res_time[1] +'" selected >'+res_time[1]+'</option>');
				   $('#r_levels').append('<option value="'+resol_time[0] +'" selected >'+resol_time[0]+'</option>');
				   $('#r_levels_minute').append('<option value="'+resol_time[1] +'" selected >'+resol_time[1]+'</option>');
				   $('#a_time').append('<option value="'+acc_time[0] +'" selected >'+acc_time[0]+'</option>');
				   $('#a_time_minute').append('<option value="'+acc_time[1] +'" selected >'+acc_time[1]+'</option>');
				   $('#sla_targetss').append('<option value="'+SLA_Compliance_Target +'" selected >'+SLA_Compliance_Target+'</option>');
				   $('#m_targets').append('<option value="'+mttr_t[0] +'" selected >'+mttr_t[0]+'</option>');
				   $('#m_targets_minute').append('<option value="'+mttr_t[1] +'" selected >'+mttr_t[1]+'</option>');
				   $('#hidden_ref_id').val(ref_id);
				   $('#view_sla').modal('show');
			   }
			function view_ref(ref_id,response_time,resolution_time,acceptance_time,SLA_Compliance_Target,mttr_target)
               {
				   var res_time=response_time.split(":");
				   var resol_time=resolution_time.split(":");
				   var acc_time=acceptance_time.split(":");
				   var mttr_t=mttr_target.split(":");
				   $('#r_time').append('<option value="'+res_time[0] +'" selected >'+res_time[0]+'</option>');
				   $('#r_time_minute').append('<option value="'+res_time[1] +'" selected >'+res_time[1]+'</option>');
				   $('#r_levels').append('<option value="'+resol_time[0] +'" selected >'+resol_time[0]+'</option>');
				   $('#r_levels_minute').append('<option value="'+resol_time[1] +'" selected >'+resol_time[1]+'</option>');
				   $('#a_time').append('<option value="'+acc_time[0] +'" selected >'+acc_time[0]+'</option>');
				   $('#a_time_minute').append('<option value="'+acc_time[1] +'" selected >'+acc_time[1]+'</option>');
				   $('#sla_targetss').append('<option value="'+SLA_Compliance_Target +'" selected >'+SLA_Compliance_Target+'</option>');
				   $('#m_targets').append('<option value="'+mttr_t[0] +'" selected >'+mttr_t[0]+'</option>');
				   $('#m_targets_minute').append('<option value="'+mttr_t[1] +'" selected >'+mttr_t[1]+'</option>');
				   $('#hidden_ref_id').val(ref_id);
				   $('#view_ref').modal('show');
			   }
			function edit_sla_modal()
               {
				   var company_id="<?php echo $company_id;?>";
				   reso_hour_1=$('#r_levels').val();
                   reso_minute_1=$('#r_levels_minute').val();
                   res_hour_1=$('#r_time').val();
                   res_minute_1=$('#r_time_minute').val();
                   acc_hour_1=$('#a_time').val();
                   acc_minute_1=$('#a_time_minute').val();
                   mttr_target_hour_1=$('#m_targets').val();
                   mttr_target_minute_1=$('#m_targets_minute').val();
				  /* if($('#r_levels').val()<10)
                           	  {
                           		  reso_hour_1='0'+$('#r_levels').val();
                           	  }
                           	  if($('#r_levels_minute').val()<10)
                           	  {
                           		  reso_minute_1='0'+$('#r_levels_minute').val();
                           	  }
                           	  if($('#r_time_minute').val()<10)
                           	  {
                           		  res_minute_1='0'+$('#r_time_minute').val();
                           	  }
                           	  if($('#r_time').val()<10)
                           	  {
                           		  res_hour_1='0'+$('#r_time').val();
                           	  }
                           	  if($('#a_time').val()<10)
                           	  {
                           		  acc_hour_1='0'+$('#a_time').val();
                           	  }
                           	  if($('#a_time_minute').val()<10)
                           	  {
                           		  acc_minute_1='0'+$('#a_time_minute').val();
                           	  }
                           	  if($('#m_targets').val()<10)
                           	  {
                           		 mttr_target_hour_1='0'+$('#m_targets').val();
                           	  }
                           	  if($('#m_targets_minute').val()<10)
                           	  {
                           		  mttr_target_minute_1='0'+$('#m_targets_minute').val();
                           	  }*/
                           	  resolution=reso_hour_1+':'+reso_minute_1+':00';
                           	  response=res_hour_1+':'+res_minute_1+':00';
                           	  acceptance=acc_hour_1+':'+acc_minute_1+':00';
                           	  sla_compliance=$('#sla_targetss').val();
                           	  mttr=mttr_target_hour_1+':'+mttr_target_minute_1+':00';
                           	  ref_id=$('#hidden_ref_id').val();
                   $.ajax({
					   url 		:   "<?php echo base_url(); ?>index.php?/controller_superad/submit_edit_sla",
					   type     :   "POST",
					   data     :   {'company_id':company_id,'ref_id':ref_id,'response':response,'resolution':resolution,'acceptance':acceptance,'sla_compliance':sla_compliance,'mttr':mttr},// {action:'$funky'}
					   datatype :   "JSON", 
					   cache    :   false,
					   success  :   function(data)
						{
							data=data.trim();
							$('#view_sla').modal('hide');
							if(data=="SLA Updated Successfully")
							{
								swal({
							  title: data,                                  
							  type: "success",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Ok",
							  cancelButtonText: "No,Cancel",
							  closeOnConfirm: false,
							  closeOnCancel: false
							},
							function(isConfirm) {
							  if (isConfirm) {
								window.location.reload();
							  }
							});
							}
							else
							{
								swal({
							  title: data,                                  
							  type: "success",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Ok",
							  cancelButtonText: "No,Cancel",
							  closeOnConfirm: false,
							  closeOnCancel: false
							},
							function(isConfirm) {
							  if (isConfirm) {
								  swal.close();
								$('#view_sla').modal('show');
							  }
							});
							}
						}
				});
			   }
			   
               function delete_ref(ref_id)
               {
               	swal({
               			  title: "Are you sure? You want to Delete",
               			  type: "warning",
               			  showCancelButton: true,
               			  confirmButtonColor: "#DD6B55",
               			  confirmButtonText: "Yes, Delete!",
               			  cancelButtonText: "Cancel",
                          closeOnConfirm: false,
                       //   closeOnCancel: false
               			},
               			function(isConfirm){
						   if (isConfirm) {
							var company_id="<?php echo $company_id;?>";
						   $.ajax({
							   url         :   "<?php echo base_url(); ?>index.php?/controller_superad/delet_sla",
							   type        :   "POST",
							   data        :   {'company_id':company_id,'ref_id':ref_id},
							   cache       :   false,
							   success     :   function(data)
								{
									swal({
									  title: data,                                  
									  type: "success",
									  showCancelButton: false,
									  confirmButtonClass: "btn-danger",
									  confirmButtonText: "Ok",
									  cancelButtonText: "No,Cancel",
									  closeOnConfirm: false,
									  closeOnCancel: false
									},
									function(isConfirm){
										if (isConfirm) {
										window.location.reload();
										}
									});
								}
							});
						}
					});
               }
                           function update_check()
                           {
                           	var selected = [];
                           	$('.company_div  input:checked').each(function() {
                           		selected.push($(this).attr('name'));
                           	});
                           	if(selected.length<=0)
                           	{
                           		swal({
                           			  title: "Please select data before Update",                                  
                           			  type: "success",
                           			  showCancelButton: false,
                           			  confirmButtonClass: "btn-danger",
                           			  confirmButtonText: "Ok",
                           			  cancelButtonText: "No,Cancel",
                           			  closeOnConfirm: false,
                           			  closeOnCancel: false
                           			});
                           	}
                           	else{
                           	$('#myModal_sla').modal('show');
                           	}
                           }
                           
                           function onclick_company(company_id)
                           {
                           if($('input[name="company/'+company_id+'"]:checked').length > 0)
                           {
                           	$(".nothings :checkbox").prop("checked", true);
                           	$("#subcategory :checkbox").prop("checked", true);//$(":checkbox.nothings").attr("checked", true);
                           }
                           else{
                           $(".nothings :checkbox").prop("checked", false);
                           $("#subcategory :checkbox").prop("checked", false);
                           }			
                           }
                           function onclick_product(prod_id){var company_id="<?php echo $company_id;?>";
                           	if($('input[name="#product/'+prod_id+' :checked').length <=0)
                           	   {
                           		   $('input[name="company/'+company_id+'"]:checkbox').prop("checked", false);
                           		   $("#subcategory :checkbox").prop("checked", false);
                           	   }
                           }
                           
                           function onclick_cat(cat_id,prod_id){
                           	var company_id="<?php echo $company_id;?>";
                           	if($('input[name="#sategry/'+cat_id+' :checked').length <=0)
                           	   {
                           		   $('input[name="product/'+prod_id+'"]:checkbox').prop("checked", false);
                           		   $('input[name="company/'+company_id+'"]:checkbox').prop("checked", false);
                           	   }
                           }
                                       $(document).ready(function(){
                           	   var company_id="<?php echo $company_id;?>";
                                           $.ajax({
                                               url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_details_company",
                                               type        :   "POST",
                                               data        :   {'company_id':company_id},
                                               datatype  	:   "JSON", 
                                               cache       :   false,
                                               success     :   function(data)
                           			{
                                                var data=JSON.parse(data);
                           			if(data.length<=0)
                           			{
                           				$('#portlet_tab1').html("No unmapped products available");
                           			}
                           			for(i=0;i<data.length;i++)
                           			{
                           				
                                                    $('#company').append("<li id="+data[i].company_id+" onclick=onclick_company('"+data[i].company_id+"');><div class='product-checkbox2'><input type='checkbox' class='product_checkbox' value='"+data[i].company_id+"' name='company/"+data[i].company_id+"'></div><a href='."+data[i].company_id+"' data-toggle='tab'><div id='frame'><span class='helper'></span><img src="+data[i].company_logo+" height=250 /></div></a></li>");
                                                    $('ul#company li:first-child').addClass('active');
                                                    $('#product_append').append("<div class='product_append "+data[i].company_id+"' id><p class='main-category'>Product Category</p></div>");
                           				var c_id=data[i].company_id;
                                                    $.ajax({
                           					   url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_product_company",
                           					   type        :   "POST",
                           					   data        :   {'company_id':data[i].company_id},// {action:'$funky'}
                           					   datatype    :   "JSON", 
                           					   cache       :   false,
                           					   success     :   function(data)
                           						{
                           						   var data=JSON.parse(data);
                           							if(data.length==0)
                           							{
                           								$("."+c_id).html("<div align='center' style='padding-top:24%;'><p class='main-category'>SLA Mapped for all Product Categories</p></div>");
                           							}
                           							else
                           							{
                           						   //console.log(data[0].company_logo);
                           						   for(i=0;i<data.length;i++)
                           						   {
                           							   $("."+data[i].company_id+"").append("<div class='col-sm-4 nothings "+data[i].product_id+"' onclick=onclick_product('"+data[i].product_id+"');><div class='product-checkbox' ><input type='checkbox' value='"+data[i].product_id+"' name='product/"+data[i].product_id+"' ></div><a id='"+data[i].product_id+"product_display' data-toggle='collapse' data-parent='#accordion' href='#"+data[i].product_id+"'><img src="+data[i].product_image+" class='img-responsive'  /></a><div class='caption catg-title2'><p>"+data[i].product_name+"</p></div></div>");
                           							   $("#"+data[i].product_id+"product_display").click(function()
                           							   {
                           								   var sub_load=$(this).attr('href');     
                           								   var sub_load = sub_load.replace('#', '');
                           								   $('.nothings').toggle();
                           								   $("."+sub_load).toggle();
                           								   if($('#subcategory').length>0)
                           								   {
                           									   $('span.clearfix').remove();
                           									   $('#subcategory').remove();
                           								   }
                           								   else
                           								   {
                           									   $("<span class='clearfix'></span><div class='col-sm-12' id='subcategory'><div id="+sub_load+" class='sub-products'><p class='main-category'>Sub Category</p></div></div>" ).insertAfter( "."+sub_load );
                           									   $.ajax({
                           										   url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_subproduct_company",
                           										   type        :   "POST",
                           										   data        :   {'prod_id':sub_load},// {action:'$funky'}
                           										   datatype    :   "JSON", 
                           										   cache       :   false,
                           										   success     :   function(data)
                           											{
                           											   var data=JSON.parse(data);
                           											   //console.log(data);
                           											   for(i=0;i<data.length;i++)
                           											   {
                           												   if($("."+sub_load+" :checkbox").prop("checked", true))
                           												   {
                           													   $("#"+data[i].prod_id).append("<div class='col-sm-4' id="+data[i].cat_id+" onclick=onclick_cat('"+data[i].cat_id+"','"+sub_load+"');><div class='product-checkbox'><input type='checkbox'  value="+data[i].cat_id+" name='sategry/"+data[i].cat_id+"' checked='checked'></div><a data-toggle='' data-parent='' href=''><img src="+data[i].cat_image+" class='img-responsive'  /></a><div class='caption catg-title2'><p>"+data[i].cat_name+"</p></div>");
                           												   }
                           												   else
                           												   {
                           													   $("#"+data[i].prod_id).append("<div class='col-sm-4' id="+data[i].cat_id+"><div class='product-checkbox'><input type='checkbox'  value="+data[i].cat_id+" name='sategry/"+data[i].cat_id+"'></div><a data-toggle='' data-parent='' href=''><img src="+data[i].cat_image+" class='img-responsive'  /></a><div class='caption catg-title2'><p>"+data[i].cat_name+"</p></div>");
                           												   }
                           												                                                                                                               
                           												}
                           											 }
                           									   });
                           								   }
                           							   });
                           						   }
                           						   
                           					   $("#product_append div:first").addClass("active");
                           					   }
                           					   },
                           				   });
                           				   //$('#product_append').append("");
                           			   }
                           		   },
                                           }); 
                                       });
                                      function check(priority_1,reso_hour_1,reso_minute_1,res_hour_1,res_minute_1,acc_hour_1,acc_minute_1,sla_target_1,mttr_target_hour_1,mttr_target_minute_1)
                             {
                           	  reso_hour_1=$('#reso_hour_1').val();
                           	  reso_minute_1=$('#reso_minute_1').val();
                           	  res_hour_1=$('#res_hour_1').val();
                           	  res_minute_1=$('#res_minute_1').val();
                           	  acc_hour_1=$('#acc_hour_1').val();
                           	  acc_minute_1=$('#acc_minute_1').val();
                           	  mttr_target_hour_1=$('#mttr_target_hour_1').val();
                           	  mttr_target_minute_1=$('#mttr_target_minute_1').val();
                           	  priority=$('#priority_1').val();
                           	/*  if($('#reso_hour_1').val()<10)
                           	  {
                           		  reso_hour_1='0'+$('#reso_hour_1').val();
                           	  }
                           	  if($('#reso_minute_1').val()<10)
                           	  {
                           		  reso_minute_1='0'+$('#reso_minute_1').val();
                           	  }
                           	  if($('#res_minute_1').val()<10)
                           	  {
                           		  res_minute_1='0'+$('#res_minute_1').val();
                           	  }
                           	  if($('#res_hour_1').val()<10)
                           	  {
                           		  res_hour_1='0'+$('#res_hour_1').val();
                           	  }
                           	  if($('#acc_hour_1').val()<10)
                           	  {
                           		  acc_hour_1='0'+$('#acc_hour_1').val();
                           	  }
                           	  if($('#acc_minute_1').val()<10)
                           	  {
                           		  acc_minute_1='0'+$('#acc_minute_1').val();
                           	  }
                           	  if($('#mttr_target_hour_1').val()<10)
                           	  {
                           		 mttr_target_hour_1='0'+$('#mttr_target_hour_1').val();
                           	  }
                           	  if($('#mttr_target_minute_1').val()<10)
                           	  {
                           		  mttr_target_minute_1='0'+$('#mttr_target_minute_1').val();
                           	  }*/
                           	  resolution=reso_hour_1+':'+reso_minute_1+':00';
                           	  response=res_hour_1+':'+res_minute_1+':00';
                           	  acceptance=acc_hour_1+':'+acc_minute_1+':00';
                           	  sla_compliance=$('#sla_target_1').val();
                           	  mttr=mttr_target_hour_1+':'+mttr_target_minute_1+':00';
                           	  if(priority=='' || reso_hour_1=='' || reso_minute_1=='' || res_hour_1=='' || res_minute_1=='' || acc_hour_1=='' || acc_minute_1=='' || sla_target_1=='' || mttr_target_hour_1=='' || mttr_target_minute_1=='' || priority==null || reso_hour_1==null || reso_minute_1==null || res_hour_1==null || res_minute_1==null || acc_hour_1==null || acc_minute_1==null || sla_target_1==null || mttr_target_hour_1==null || mttr_target_minute_1==null)
                           	  {
                           		  swal({
                           			  title: "All fields are mandatory",                                  
                           			  type: "success",
                           			  showCancelButton: false,
                           			  confirmButtonClass: "btn-danger",
                           			  confirmButtonText: "Ok",
                           			  cancelButtonText: "No,Cancel",
                           			  closeOnConfirm: false,
                           			  closeOnCancel: false
                           			},
                           			function(isConfirm) {
                           				swal.close();
                           			});
                           	  }
                           	  else
                           	  {
                           	var selected = [];
                           	$('.company_div  input:checked').each(function() {
                           		selected.push($(this).attr('name'));
                           	});
                           	if(selected.length<=0)
                           	{
                           		swal({
                           			  title: "Please select data before Update",                                  
                           			  type: "success",
                           			  showCancelButton: false,
                           			  confirmButtonClass: "btn-danger",
                           			  confirmButtonText: "Ok",
                           			  cancelButtonText: "No,Cancel",
                           			  closeOnConfirm: false,
                           			  closeOnCancel: false
                           			},
                           			function(isConfirm) {
                           			});
                           	}
                           	else
                           	{
                           		selected=selected.sort();
                           		console.log(selected);
                           		for(i=0;i<selected.length;i++)
                           		{
                           			var value=selected[i].split('/');console.log(value);
                           			
                           			if(value[0]=='company')
                           			{
                           			
                           				$.ajax({
                           					   url 		:   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                           					   type     :   "POST",
                           					   data     :   {'company_id':value[1],'product_id':'all','cat_id':'all','priority':priority,'response':response,'resolution':resolution,'acceptance':acceptance,'sla_compliance':sla_compliance,'mttr':mttr},// {action:'$funky'}
                           					   datatype :   "JSON", 
                           					   cache    :   false,
                           					   success  :   function(data)
                           						{
                           							data=data.trim();
                           							$('#myModal_sla').modal('hide');
                           							if(data=="SLA Updated Successfully")
                           							{
                           								swal({
                           							  title: data,                                  
                           							  type: "success",
                           							  showCancelButton: false,
                           							  confirmButtonClass: "btn-danger",
                           							  confirmButtonText: "Ok",
                           							  cancelButtonText: "No,Cancel",
                           							  closeOnConfirm: false,
                           							  closeOnCancel: false
                           							},
                           							function(isConfirm) {
                           							  if (isConfirm) {
                           								window.location.reload();
                           							  }
                           							});
                           							}
                           							else
                           							{
                           								swal({
                           							  title: data,                                  
                           							  type: "success",
                           							  showCancelButton: false,
                           							  confirmButtonClass: "btn-danger",
                           							  confirmButtonText: "Ok",
                           							  cancelButtonText: "No,Cancel",
                           							  closeOnConfirm: false,
                           							  closeOnCancel: false
                           							},
                           							function(isConfirm) {
                           							  if (isConfirm) {
                           								  swal.close();
                           								$('#myModal_sla').modal('show');
                           							  }
                           							});
                           							}
                           						}
                           				});
                           				return false;
                           			}
                           			else if(value[0]=='product')
                           			{
                           				$.ajax({
                           					   url 		:   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                           					   type     :   "POST",
                           					   data     :   {'company_id':'','product_id':value[1],'cat_id':'all','priority':priority,'response':response,'resolution':resolution,'acceptance':acceptance,'sla_compliance':sla_compliance,'mttr':mttr},// {action:'$funky'}
                           					   datatype :   "JSON", 
                           					   cache    :   false,
                           					   success  :   function(data)
                           						{
                           							data=data.trim();
                           							$('#myModal_sla').modal('hide');
                           							if(data=="SLA Updated Successfully")
                           							{
                           								swal({
                           							  title: data,                                  
                           							  type: "success",
                           							  showCancelButton: false,
                           							  confirmButtonClass: "btn-danger",
                           							  confirmButtonText: "Ok",
                           							  cancelButtonText: "No,Cancel",
                           							  closeOnConfirm: false,
                           							  closeOnCancel: false
                           							},
                           							function(isConfirm) {
                           							  if (isConfirm) {
                           								window.location.reload();
                           							  }
                           							});
                           							}
                           							else
                           							{
                           								swal({
                           							  title: data,                                  
                           							  type: "success",
                           							  showCancelButton: false,
                           							  confirmButtonClass: "btn-danger",
                           							  confirmButtonText: "Ok",
                           							  cancelButtonText: "No,Cancel",
                           							  closeOnConfirm: false,
                           							  closeOnCancel: false
                           							},
                           							function(isConfirm) {
                           							  if (isConfirm) {
                           								  swal.close();
                           								$('#myModal_sla').modal('show');
                           							  }
                           							});
                           							}
                           						}
                           				});
                           			}
                           			else if(value[0]=='sategry')
                           			{
                           				
                           				$.ajax({
                           					   url 		:   "<?php echo base_url(); ?>index.php?/controller_superad/submit_sla",
                           					   type     :   "POST",
                           					   data     :   {'company_id':'','product_id':'','cat_id':value[1],'priority':priority,'response':response,'resolution':resolution,'acceptance':acceptance,'sla_compliance':sla_compliance,'mttr':mttr},// {action:'$funky'}
                           					   datatype :   "JSON", 
                           					   cache    :   false,
                           					   success  :   function(data)
                           						{
                           							data=data.trim();
                           							$('#myModal_sla').modal('hide');
                           							if(data=="SLA Updated Successfully")
                           							{
                           								swal({
                           							  title: data,                                  
                           							  type: "success",
                           							  showCancelButton: false,
                           							  confirmButtonClass: "btn-danger",
                           							  confirmButtonText: "Ok",
                           							  cancelButtonText: "No,Cancel",
                           							  closeOnConfirm: false,
                           							  closeOnCancel: false
                           							},
                           							function(isConfirm) {
                           							  if (isConfirm) {
                           								window.location.reload();
                           							  }
                           							});
                           							}
                           							else
                           							{
                           								swal({
                           							  title: data,                                  
                           							  type: "success",
                           							  showCancelButton: false,
                           							  confirmButtonClass: "btn-danger",
                           							  confirmButtonText: "Ok",
                           							  cancelButtonText: "No,Cancel",
                           							  closeOnConfirm: false,
                           							  closeOnCancel: false
                           							},
                           							function(isConfirm) {
                           							  if (isConfirm) {
                           								  swal.close();
                           								$('#myModal_sla').modal('show');
                           							  }
                           							});
                           							}
                           						}
                           				});
                           			}
                           		}
                           	}
                             }
                             }
                                    
                        
            </script> 
            <script>
               $('.collapse').on('show.bs.collapse', function (e) {
                  $('.collapse').not(e.target).removeClass('in');
               });
            </script>
            <script>
               $('.tab-sla-page img').click(function(){
                 $('.selected').removeClass('selected'); // removes the previous selected class
                 $(this).addClass('selected'); // adds the class to the clicked image
               });
            </script>
            <script>
               $('.product-checkbox2').click(function(){
                   alert($(this).val());
               });
            </script>
            <script type="text/javascript" src="assets/global/plugins/checkbox.js" ></script>
         </body>
      </html>