<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <?php $company_id=$this->session->userdata('companyid');
			$region=$user['region'];$area=$user['area'];$location=$user['location'];
include 'assets/lib/cssscript.php'?>
    <style>
		
        .dt_buttons {
            display: none;
        }
		.dataTables_filter{
			text-align-last: right;
		}
    </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
    <!-- BEGIN CONTAINER -->
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include "assets/lib/manager_header.php"?>
        <!-- END HEADER -->
		<div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
				  
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box dark">
                                <div class="portlet-title">
                                    <div class="caption">Spare Request </div>
									<ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_actions_pending" data-toggle="tab">New Spares</a>
                                                </li>
                                                <li>
                                                    <a href="#Accepted" data-toggle="tab">Approved Spares</a>
                                                </li>
                                                <li>
                                                    <a href="#Rejected" data-toggle="tab">Rejected Spares</a>
                                                </li>
                                            </ul>
                                </div>
                                <div class="portlet-body">                                    
                                    <div class="tab-content">

                                        <div class="tab-pane active" id="tab_actions_pending">
                                            <div class="table=responsive">

                                                 <table class="table table-hover table-bordered datatable" id="">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center">Ticket ID</th>
                                                            <th style="text-align:center">Technician ID</th>
                                                            <th style="text-align:center">Spare Required</th>
                                                            <th style="text-align:center">Contract Type</th>
                                                            <!-- <th style="text-align:center">Chargeable</th> -->
                                                            <th style="text-align:center">Amount</th>
                                                            <th style="text-align:center">Customer Approval</th>
                                                            <th style="text-align:center">Action</th>
                                                        </tr>
                                                    </thead>
 <tbody id="tbody_spare_request" align="center"></tbody>                                             
												</table>
                                            </div>
                                        </div>


                                        <div class="tab-pane fade" id="Accepted">
                                            <div class="table=responsive">

                                                 <table class="table table-hover table-bordered datatable1" id="">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center">Ticket ID</th>
                                                            <th style="text-align:center">Technician ID</th>
                                                            <th style="text-align:center">Spare Required</th>
                                                            <!-- <th style="text-align:center">Chargeable</th> -->
                                                            <th style="text-align:center">Amount</th>
                                                            <th style="text-align:center">Customer Approval</th>
                                                        </tr>
                                                    </thead>
                                              <tbody id="tbody_spare_accepted" align="center"></tbody>
												</table>
                                            </div>
                                        </div>


                                        <div class="tab-pane fade" id="Rejected">
                                            <div class="table=responsive">

                                               <table class="table table-hover table-bordered datatable2" id="">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center">Ticket ID</th>
                                                            <th style="text-align:center">Technician ID</th>
                                                            <th style="text-align:center">Spare Required</th>
                                                            <!-- <th style="text-align:center">Chargeable</th> -->
                                                            <th style="text-align:center">Amount</th>
                                                            <th style="text-align:center">Customer Approval</th>

                                                        </tr>
                                                    </thead>
                                                     <tbody id="tbody_spare_rejected" align="center"></tbody>
                                                    </table>

                                            </div>
                                        </div>
                                    </div>
                            </div>
							</div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
<?php include "assets/lib/footer.php"?>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
				<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Spare Details</h5>
      </div>
      <div class="modal-body"id='modal_display' style="text-align:center !important">
		<div class="table-responsive">
			<table class="table table-hover table-bordered" style="margin-bottom: 4px;">
				<thead>
					<td>Spare code</td>
					<td>Model</td>
					<td>Product</td>
					<td>Sub-Product</td>
					<td>Spare Required</td>
					<td>Spare Location</td>
                    <td>Chargeable</td>
				</thead>
				<tbody id="spare_details">
				</tbody>
			</table>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
            </div>

        </div>


    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Technician Details</h5>
                </div>
                <div class="modal-body" id='modal_tech'>
                    <form class="form-horizontal" role="form">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>

    </div>
    <div id="myModa2" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Tickets Details</h5>
                </div>
                <div class="modal-body" id='modal_tic'>
                    <form class="form-horizontal" role="form">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>

    </div>

    <!-- END wrapper -->
    <?php include "assets/lib/javascript.php"?>
    <script>
        $('.nav.navbar-nav').find('.open').removeClass('open');
        $('#man_spares').addClass('open active');
        $('#man_spare_re').addClass('active');
		/*  $(document).ready(function() {
		  $('.sample_2').DataTable({"order": []});
		  $('.datatable').DataTable({"order": []});
		  $('.datatable1').DataTable({"order": []});
		  $('.datatable2').DataTable({"order": []});
			  
		  });*/
    </script>

    <script>
        var resizefunc = [];
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
           $('.datatable').dataTable({
        "order": []
    }); $('.datatable1').dataTable({
        "order": []
    }); $('.datatable2').dataTable({
        "order": []
    });
		
 var company_id="<?php echo $company_id;?>";
 var region="<?php echo $region;?>";
 var area="<?php echo $area;?>";
			
				 $("#tbody_spare_request").empty();
			  $('.datatable').DataTable().destroy();       
		
		$.ajax({
			 
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/disply_Spare",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area},
         dataType: "json",
         success: function(data) {
     //   alert(data);
         if(data.length<1)
         {
			  $("#tbody_spare_request").empty();
			  $('.datatable').DataTable().destroy();         
         //$('#tbody_spare_request').html('<trstyle="text-align:center"><td colspan=6>No records found</td></tr>');			 
         }
         else
         {
			  //$('.sample_2').DataTable().destroy();
         for(i=0;i<data.length;i++)
         {
			var  customer_name=btoa(unescape(encodeURIComponent(data[i].customer_name)));
			 var  call_type=btoa(unescape(encodeURIComponent(data[i].call_type)));
			 var  model=btoa(unescape(encodeURIComponent(data[i].model)));
			 var string=btoa(unescape(encodeURIComponent(data[i].requested_spare)));
			var first_name=btoa(unescape(encodeURIComponent(data[i].first_name)));
			var location=btoa(unescape(encodeURIComponent(data[i].location)));
			var product_name=btoa(unescape(encodeURIComponent(data[i].product_name)));
			var cat_name=btoa(unescape(encodeURIComponent(data[i].cat_name)));
			 var spare_cost=btoa(unescape(encodeURIComponent(data[i].spare_cost)));
			
			 if (data[i].cust_acceptance==1)
			 {
				 var cust_acc="Yes";
			 } else
			 {
				 var cust_acc="No";
			 }
         
           $('#tbody_spare_request').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_tic(this.id,"'+data[i].cust_id+'","'+customer_name+'","'+data[i].amc_id+'","'+call_type+'","'+model+'");><a>'+data[i].ticket_id+'</a></td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover_tech(this.id,"'+first_name+'","'+data[i].skill_level+'","'+data[i].contact_number+'","'+location+'","'+product_name+'","'+cat_name+'");><a>'+data[i].employee_id+'</a></td><td style="text-align:center"><button id="'+data[i].employee_id+'" class="btn btn-circle blue btn-outline btn-icon-only" onclick=hover_spare(this.id,"'+string+'");><i class="fa fa-eye"></i></button></td><td style="text-align:center">'+data[i].call_type+'</td><td style="text-align:center">'+data[i].spare_cost+'</td><td style="text-align:center">'+cust_acc+'</td><td style="text-align:center"><button id="'+data[i].ticket_id+'" class="btn btn-circle green btn-outline btn-icon-only" onclick=accepts(this.id);><i class="fa fa-check"></i></button><button id="'+data[i].ticket_id+'" class="btn btn-circle red btn-outline btn-icon-only" onclick=Rejects(this.id);><i class="fa fa-times"></i></button></td></tr>');	
         
         }
			
			
         }
			  $('.datatable').DataTable({"order": []});
         }
         });
			
		
			
     $("#tbody_spare_accepted").empty();
	      $('.datatable1').DataTable().destroy();
	 $.ajax({
			 
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/disply_accept_Spare",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area},
         dataType: "json",
         success: function(data) {
         $("#tbody_spare_accepted").empty();
			   $('.datatable1').DataTable().destroy();
			 console.log(data);
         if(data.length<1)
         {
        $("#tbody_spare_accepted").empty();
			  $('.datatable1').DataTable().destroy();
         }
			 
         else
         {
         for(i=0;i<data.length;i++)
         {
			 var  customer_name=btoa(unescape(encodeURIComponent(data[i].customer_name)));
			 var  call_type=btoa(unescape(encodeURIComponent(data[i].call_type)));
			 var  model=btoa(unescape(encodeURIComponent(data[i].model)));
			 var string=btoa(unescape(encodeURIComponent(data[i].requested_spare)));
			var first_name=btoa(unescape(encodeURIComponent(data[i].first_name)));
			var location=btoa(unescape(encodeURIComponent(data[i].location)));
			var product_name=btoa(unescape(encodeURIComponent(data[i].product_name)));
			var cat_name=btoa(unescape(encodeURIComponent(data[i].cat_name)));
			 var spare_cost=btoa(unescape(encodeURIComponent(data[i].spare_cost)));
			
			 if (data[i].cust_acceptance==1)
			 {
				 var cust_acc="Yes";
			 } else
			 {
				 var cust_acc="No";
			 }
         
           $('#tbody_spare_accepted').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_tic(this.id,"'+data[i].cust_id+'","'+customer_name+'","'+data[i].amc_id+'","'+call_type+'","'+model+'");><a>'+data[i].ticket_id+'</a></td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover_tech(this.id,"'+first_name+'","'+data[i].skill_level+'","'+data[i].contact_number+'","'+location+'","'+product_name+'","'+cat_name+'");><a>'+data[i].employee_id+'</a></td><td style="text-align:center"><button id="'+data[i].employee_id+'" class="btn btn-circle blue btn-outline btn-icon-only" onclick=hover_spare(this.id,"'+string+'");><i class="fa fa-eye"></i></button></td><td style="text-align:center">'+data[i].spare_cost+'</td><td style="text-align:center">'+cust_acc+'</td></tr>');	
         
              
         }
       

         }
			   $('.datatable1').DataTable({"order": []});
         }
         });
		
          $("#tbody_spare_rejected").empty();
			 $('.datatable2').DataTable().destroy(); 
	    $.ajax({
			 
         url: "<?php echo base_url();?>" + "index.php?/controller_manager/disply_reject_Spare",
         type: 'POST',
		 data: {'company_id':company_id,'region':region,'area':area},
         dataType: "json",
         success: function(data) {
         if(data.length<1)
         {
         //$('#tbody_spare_rejected').html('<tr style="text-align:center"><td colspan=6>No records found</td></tr>');
			 $("#tbody_spare_rejected").empty();
			 $('.datatable2').DataTable().destroy();
         }
         else
         {
         for(i=0;i<data.length;i++)
         {
      var  customer_name=btoa(unescape(encodeURIComponent(data[i].customer_name)));
			 var  call_type=btoa(unescape(encodeURIComponent(data[i].call_type)));
			 var  model=btoa(unescape(encodeURIComponent(data[i].model)));
			 var string=btoa(unescape(encodeURIComponent(data[i].requested_spare)));
			var first_name=btoa(unescape(encodeURIComponent(data[i].first_name)));
			var location=btoa(unescape(encodeURIComponent(data[i].location)));
			var product_name=btoa(unescape(encodeURIComponent(data[i].product_name)));
			var cat_name=btoa(unescape(encodeURIComponent(data[i].cat_name)));
			 var spare_cost=btoa(unescape(encodeURIComponent(data[i].spare_cost)));
			
			 if (data[i].cust_acceptance==1)
			 {
				 var cust_acc="Yes";
			 } else
			 {
				 var cust_acc="No";
			 }
         
			
		   $('#tbody_spare_rejected').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_tic(this.id,"'+data[i].cust_id+'","'+customer_name+'","'+data[i].amc_id+'","'+call_type+'","'+model+'");><a>'+data[i].ticket_id+'</a></td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover_tech(this.id,"'+first_name+'","'+data[i].skill_level+'","'+data[i].contact_number+'","'+location+'","'+product_name+'","'+cat_name+'");><a>'+data[i].employee_id+'</a></td><td style="text-align:center"><button id="'+data[i].employee_id+'" class="btn btn-circle blue btn-outline btn-icon-only" onclick=hover_spare(this.id,"'+string+'");><i class="fa fa-eye"></i></button></td><td style="text-align:center">'+data[i].spare_cost+'</td><td style="text-align:center">'+cust_acc+'</td></tr>');	
        	 
         
         }		 

         }
			   $('.datatable2').DataTable({"order": []});
         }
         });
					
			
			
			
			
			
			
        });
        //TableManageButtons.init();
    </script>

    <script>
        function accepts(id) {
            swal({
                    title: "Are you sure?",
                    text: "You Want to Accept this Spare(s) !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Accept",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: "<?php echo base_url();?>" + "index.php?/controller_manager/statuschange",
                            type: 'POST',
                            data: {
                                'id': id
                            },
                            //dataType: "json",
                            success: function(data) {
                                $('#tbl_view').html('');
                                console.log(data);
                                /* if (data.length < 1) {
                                        $('#tbl_view').html('<tr><td colspan=9><b><i>No records found</i></b></td></tr>');
                                    } else {
                                        for (i = 0; i < data.length; i++) {
                                            var cat_name = data[i].cat_name;
                                            var cat_name = cat_name.replace(/ /g, ":");
                                            $('#tbl_view').append('<tr><td style="text-align:center" id="' + data[i].ticket_id + '" style="cursor: pointer;" onclick=hover_ticket(this.id,"' + data[i].customer_name + '","' + data[i].location + '","' + data[i].product_name + '","' +cat_name + '","' + problem + '","' + data[i].priority + '");>' + data[i].ticket_id + '</td><td style="text-align:center" id="' + data[i].employee_id + '" style="cursor: pointer;" onclick=hover(this.id,"' + data[i].first_name + '","' + data[i].email_id + '","' + data[i].contact_number + '","' + data[i].skill_level + '","' + data[i].today_task_count + '");>' + data[i].employee_id + '</td><td style="text-align:center">' + data[i].product_name + '</td><td style="text-align:center">' + data[i].cat_name + '</td><td style="text-align:center">' + data[i].spare_code + '</td><td style="text-align:center">' + data[i].quantity + '</td><td style="text-align:center">' + data[i].spare_source + '</td></tr>');
                                        
										}
                                    } */
                                if (data == "Something went Wrong") {
                                    swal("Cancelled", "Something Went Wrong:)", "error");
                                } else {
                                    swal({
                                            title: "Alert",
                                            text: data,
                                            type: "success",
                                            showCancelButton: true,
                                            confirmButtonClass: "btn-danger",
                                            confirmButtonText: "Ok",
                                            cancelButtonText: "Cancel",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                swal.close();
                                                location.reload();
                                            }
                                        });
                                }
                            }
                        });
                    } else {

                        swal.close();
						location.reload();
                    }
                });
        }

        function Rejects(id) {
            swal({
                    title: "Are you sure?",
                    text: "You Want to Reject this Spare !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Reject",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: "<?php echo base_url();?>" + "index.php?/controller_manager/rejectstatus",
                            type: 'POST',
                            data: {
                                'id': id
                            },
                            //dataType: "json",
                            success: function(data) {
                                if (data == "sorry try again") {
                                    swal("Cancelled", "Something Went Wrong:)", "error");
                                } else {
                                    swal({
                                            title: "Rejected",
                                            text: data,
                                            type: "success",
                                            showCancelButton: true,
                                            confirmButtonClass: "btn-danger",
                                            confirmButtonText: "Ok",
                                            cancelButtonText: "Cancel",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                swal.close();
                                                location.reload();
                                            }
                                        });
                                }
                            }
                        });
                    } else {

                        swal.close();
                    }
                });
        }


        function inver() {
            $('#tbdy_attendance').empty();
            var company_id = "<?php echo $company_id;?>";
            var filter = $('#inver').val();

            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/spare_accept",
                type: 'POST',
                data: {
                    'filter': filter
                },
                dataType: "json",
                success: function(data) {
                    $('#tbdy_attendance').html('');
                    console.log(data);
                    if (data.length < 1) {
                        console.log(data);
                        $('#tbdy_attendance').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
                    } else {
                        console.log(data);
                        //	data=JSON.parse(data);

                        for (i = 0; i < data.length; i++) {
                            var data1 = JSON.stringify(data[i].requested_spare);
                            data1 = JSON.parse(data1);
                            var spare = JSON.stringify(data1);
                            var spare3 = spare.replace(/\//g, "");
                            var spare2 = spare3.replace(/ /g, "");
                            var spare4 = spare2.replace(/\n/g, "");
                            var spare1 = spare4.replace(/\//g, "");
                            console.log(spare1);
                            var cat_name = data[i].cat_name;
                            var cat_name = cat_name.replace(/ /g, ":");
                            var set = data[i].cust_acceptance;
                            if (set == 0) {
                                var star = 'Yes';
                            } else {
                                var star = 'No';
                            }
                            var sat = data[i].is_it_chargeable;
                            if (sat == 0) {
                                var charge = 'Yes';
                            } else {
                                var charge = 'No';
                            }
                            $('#tbdy_attendance').append('<tr><td style="text-align:center">' + data[i].ticket_id + '</td><td style="text-align:center" id="' + data[i].employee_id + '" style="cursor: pointer;" onclick=hover_tech(this.id,"' + data[i].first_name + '","' + data[i].skill_level + '","' + data[i].contact_number + '","' + data[i].location + '","' + data[i].product_name + '","' + cat_name + '");>' + data[i].employee_id + '</td><td style="text-align:center"><button class="btn btn-default red-stripe" id="' + data[i].ticket_id + '" onclick=hover_spare(this.id,' + spare1 + ');>View</button></td><td style="text-align:center">' + charge + '</td><td style="text-align:center">' + data[i].spare_cost + '</td><td style="text-align:center">' + star + '</td></tr>');

                        }
                    }
                }
            });
        }

        function reject() {
            $('#tbdy_attend').empty();
            var company_id = "<?php echo $company_id;?>";
            var filter = $('#reject').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/spare_reject",
                type: 'POST',
                data: {
                    'filter': filter
                },
                dataType: "json",
                success: function(data) {
                    $('#tbdy_attend').html('');
                    console.log(data);
                    if (data.length < 1) {
                        console.log(data);
                        $('#tbdy_attend').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
                    } else {
                        console.log(data);
                        //	data=JSON.parse(data);

                        for (i = 0; i < data.length; i++) {
                            var data1 = JSON.stringify(data[i].requested_spare);
                            data1 = JSON.parse(data1);
                            var spare = JSON.stringify(data1);
                            var spare3 = spare.replace(/\//g, "");
                            var spare2 = spare3.replace(/ /g, "");
                            var spare4 = spare2.replace(/\n/g, "");
                            var spare1 = spare4.replace(/\//g, "");
                            console.log(spare1);
                            var cat_name = data[i].cat_name;
                            var cat_name = cat_name.replace(/ /g, ":");

                            var set = data[i].cust_acceptance;
                            if (set == 0) {
                                var star = 'Yes';
                            } else {
                                var star = 'No';
                            }
                            var sat = data[i].is_it_chargeable;
                            if (sat == 0) {
                                var charge = 'Yes';
                            } else {
                                var charge = 'No';
                            }
                            $('#tbdy_attend').append('<tr><td style="text-align:center">' + data[i].ticket_id + '</td><td style="text-align:center" id="' + data[i].employee_id + '" style="cursor: pointer;" onclick=hover_tech(this.id,"' + data[i].first_name + '","' + data[i].skill_level + '","' + data[i].contact_number + '","' + data[i].location + '","' + data[i].product_name + '","' + cat_name + '");>' + data[i].employee_id + '</td><td style="text-align:center"><button class="btn btn-default red-stripe" id="' + data[i].ticket_id + '" onclick=hover_spare(this.id,' + spare1 + ');>View</button></td><td style="text-align:center">' + charge + '</td><td style="text-align:center">' + data[i].spare_cost + '</td><td style="text-align:center">' + star + '</td></tr>');

                        }
                    }
                }
            });
        }

        function hover_tech(tech_id, first_name, Skill_level, contact_number, location, Product_name, cat_name) {
			
			var first_name=decodeURIComponent(escape(window.atob(first_name)));
			var location=decodeURIComponent(escape(window.atob(location)));
			var Product_name=decodeURIComponent(escape(window.atob(Product_name)));
			var cat_name=decodeURIComponent(escape(window.atob(cat_name)));
		
            //cat_name = cat_name.replace(/\:/g," "); 
            $('#modal_tech form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Technician ID</label><div class="col-sm-6 control-label">' + tech_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Name</label><div class="col-sm-6 control-label">' + first_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Skill Level</label><div class="col-sm-6 control-label">' + Skill_level + '</div></div><div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Contact Number</label><div class="col-sm-6 control-label">' + contact_number + '</div></div><div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Location</label><div class="col-sm-6 control-label">' + location + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Product Category </label><div class="col-sm-6 control-label">' + Product_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Sub Category</label><div class="col-sm-6 control-label">' + cat_name + '</div></div>');
            $('#myModal1').modal('show');
        }

        function hover_tic(ticket_id, cus_id, cus_name, amc_id, call_type, model) {
			var  cus_name=decodeURIComponent(escape(window.atob(cus_name)));
			 var  call_type=decodeURIComponent(escape(window.atob(call_type)));
			 var  model=decodeURIComponent(escape(window.atob(model)));
			

            $('#modal_tic form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Ticket ID</label><div class="col-sm-6 control-label">' + ticket_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Customer Id</label><div class="col-sm-6 control-label">' + cus_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Customer Name</label><div class="col-sm-6 control-label">' + cus_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >AMC Id </label><div class="col-sm-6 control-label">' + amc_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Contract Type</label><div class="col-sm-6 control-label">' + call_type + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Model</label><div class="col-sm-6 control-label">' + model + '</div></div>');
            $('#myModa2').modal('show');
        }
     function hover_spare(ticket_id,spare_array)
	{
		var spare_array=decodeURIComponent(escape(window.atob(spare_array)));
         $('#spare_details').empty(); 
            var data = spare_array;
           
			 
			  if(data == "null"){
                               /* $.dialogbox({
      							type:'msg',
      							content:'No Data',
      							closeBtn:true,
      							btn:['Ok.'],
      							call:[
      								function(){
      									$.dialogbox.close();
      								}
      							]
      
      						});  */
							swal("No Spares")
                        }
                else{
                var data = JSON.parse(spare_array);
				var prod_name="";
				var cat_name="";
				var sp_model="";	
				for(i=0; i<data.length; i++)
				{
				
				  $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/spare_prod_sub",
                type: 'POST',
				async: false,
                data: {
                    'sparecode': data[i].Spare_code
                },
            //   dataType: "JSON",
                success: function(data1) {
                   var spare = JSON.parse(data1);
					//console.log(spare[0]['product_name']);
					prod_name=spare[0].product_name;
					cat_name=spare[0].cat_name;
					sp_model=spare[0].spare_modal;
					//console.log(prod_name);
					//console.log(cat_name);
                
                }
            });	
					//console.log(prod_name);
					//alert(prod_name);
					
				$('#spare_details').append('<tr><td>'+data[i].Spare_code+'</td><td>'+sp_model+'</td><td>'+prod_name+'</td><td>'+cat_name+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td><td>'+data[i].is_it_chargeable+'</td></tr>');
				} 
						                             //console.log(data);
                                                      $('#myModal').modal('show');
                                                        }
       
                          
                                }

        function hover_spare_accept(ticket_id, requested_spare) {
            $('#spare_details').empty();
            var data = JSON.parse(requested_spare);
            // console.log(data);

            for (i = 0; i < data.length; i++) {
                $('#tbdy_attendance').append('<tr><td>' + data[i].Spare_code + '</td><td>' + data[i].quantity + '</td><td>' + data[i].spare_source + '</td></tr>');

            }
            /*$('#modal_display').html('<table><thead><th>Spare Code</th><th>Spare Required</th><th>Spare Source</th></thead>');
		 for(i=0; i<data.length; i++)
		 {
			 $('#modal_display').append('<tbody><tr><td>'+data[i].Spare_code+'</td><td>'+data[i].quantity+'</td><td>'+data[i].spare_source+'</td></tr></tbody>');

		 }
		$('#modal_display').append('</table>');*/
            $('#myModal').modal('show');

        }

        function hover_ticket(ticket_id, customer_name, location, product_name, cat_name, problem, priority) {
            var replaceSpace = problem;
            problem = problem.replace(/\:/g, " ");
            var location = location;
            location = location.replace(/\:/g, " ");
            var product_name = product_name;
            product_name = product_name.replace(/\:/g, " ");
            var cat_name = cat_name;
            cat_name = cat_name.replace(/\:/g, " ");
            $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 control-label"for="inputEmail3">Ticket ID</label><div class="col-sm-4 control-label">' + ticket_id + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Customer Name</label><div class="col-sm-4 control-label">' + customer_name + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Location</label><div class="col-sm-4 control-label">' + location + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >-Category</label><div class="col-sm-4 control-label">' + product_name + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Sub-Category</label><div class="col-sm-4 control-label">' + cat_name + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Problem</label><div class="col-sm-4 control-label">' + problem + '</div></div><div class="form-group"><label class="col-sm-3 control-label"for="inputPassword3" >Priority</label><div class="col-sm-4 control-label">' + priority + '</div></div></div>');
            $('#myModal').modal('show');
        }

        var company_id = "<?php echo $company_id;?>";

        function viewimage(id) {
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/view",
                type: 'POST',
                data: {
                    'id': id
                },
                //dataType: "JSON",
                success: function(data) {
                    var data = JSON.parse(data);
                    //console.log(data['view']);	
                    $("#token").attr('src', data['view']);
                    $("#myModal").modal('show');

                }
            });
        }
    </script>
    <script type="text/javascript">
        //$('.datatable').dataTable();
        $('#datatable-keytable').DataTable({
            keys: true
        });
        $('#datatable-responsive').DataTable();
        /*  $('#datatable-scroller').DataTable({
             ajax: "assets/plugins/datatables/json/scroller-demo.json",
             deferRender: true,
             scrollY: 380,
             scrollCollapse: true,
             scroller: true
         }); */
        var table = $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });
    </script>
</body>

</html>