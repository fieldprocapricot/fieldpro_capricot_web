<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common extends CI_Model{
	public function get_method($array,$method)
	{
		$methodname = $method['methodname'];	
		$inputResult = web_inputNode($array);
		$sproc = "CALL $methodname($inputResult)";
		$query = $this->db->query($sproc,$array);	
		if($method['sp_next_result'] == 1):
		$query->sp_next_result();
		endif;
		return $query;
	}
	public function create_method($array,$method)
	{
		$out = $method['out'];
		$outPosition = $method['outPosition'];
		$methodname = $method['methodname'];
		$inputResult = web_inputNode($array);
		if($outPosition == 'last'): $sproc = "CALL $methodname($inputResult,@$out)"; else: $sproc = "CALL $methodname(@$out,$inputResult)";	endif;
		$this->db->query($sproc,$array);	
		$query = $this->db->query("SELECT @$out as $out");
		if($method['sp_next_result'] == 1):
		$query->sp_next_result();
		endif;
		return $query;
	}
	public function create_multi_method($array,$method)
	{
		$out = $method['out'];
		$out1 = $method['out1'];
		$outPosition = $method['outPosition'];
		$methodname = $method['methodname'];
		$inputResult = web_inputNode($array);
		if($outPosition == 'last'): $sproc = "CALL $methodname($inputResult,@$out,@$out1)"; else: $sproc = "CALL $methodname(@$out,@$out1,$inputResult)";	endif;
		$this->db->query($sproc,$array);	
		$query = $this->db->query("SELECT @$out as $out, @$out1 as $out1");
		if($method['sp_next_result'] == 1):
		$query->sp_next_result();
		endif;
		return $query;
	}
	///create method and get method is same but two method cretion reason identify to easy for developer in what procedure using in ctrl 
	public function update_method($array,$method) 
	{
		$out = $method['out'];
		$outPosition = $method['outPosition'];
		$methodname = $method['methodname'];
		$inputResult = web_inputNode($array);
		if($outPosition == 'last'): $sproc = "CALL $methodname($inputResult,@$out)"; else: $sproc = "CALL $methodname(@$out,$inputResult)";	endif;
		$this->db->query($sproc,$array);	
		$query = $this->db->query("SELECT @$out as $out");
		if($method['sp_next_result'] == 1):
		$query->sp_next_result();
		endif;
		return $query;
	}
public function delete_method($array,$method)
	{
		$methodname = $method['methodname'];
		$inputResult = web_inputNode($array);
		$sproc = "CALL $methodname($inputResult)";
		$query = $this->db->query($sproc,$array);	
		if($method['sp_next_result'] == 1):
		$query->sp_next_result();
		endif;
		return $query;
	}	
	
public function deliver_response($array){
		//header("HTTP/1.1 ".$array['status']);
		$response['methodName'] = $array['methodName'];
		$response['status'] = $array['status'];
		$response['Data'] = $array['Data'];
		$response['ResponseCode'] = $array['ResponseCode'];
		$response['ResponseMessage'] = $array['ResponseMessage'];
		$response['custom'] = $array['custom'];
		echo $json_response = json_encode($response);
	}
	
public function batch_email($array) 
	{
		
		//$this->email->clear(TRUE);
		//$config = array ('mailtype' => 'html','charset'  => 'utf-8','priority' => '1');
		 $this->load->library('email');
		$config = array();
		$config['protocol']='smtp';
        $config['smtp_host']='email-smtp.us-east-1.amazonaws.com';
        $config['smtp_port']='465';
        $config['smtp_timeout']='300';
        $config['smtp_user']='AKIAQVP5N6THGN3WW5WY';
        $config['smtp_pass']='BDr3VEpIbSqqFBuzQ291YtrMRfufJgXQAiZDLNsHMXtD';
	    $config['smtp_crypto'] = 'ssl';
        $config['charset']='utf-8';
        $config['newline']="\r\n";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
	
		$this->email->from($array['from_email'], $array['from_name']); 
		$this->email->to($array['to_email']);
		if($array['cc'] != '')
		{
		$this->email->cc($array['cc']); 
		}
		if (array_key_exists('bcc', $array)) 
		{
		if($array['bcc'] != ''):
		$this->email->bcc($array['bcc']); 
		endif;
		}
		$this->email->subject($array['subject']);
		$this->email->message($array['message']);  
		if($array['attach'] == 1)
		{
		$this->email->attach($array['path']);
		}
		if($this->email->send())
		{
			//echo 'success';
		}
		else
		{
			//echo 'failed';
		}
		return TRUE;
	}	
	
public function ticket_status($array){

 $procedure = "SELECT
A.status_id,
A.status_name

FROM status A 
WHERE A.status_id='".$array['current_status']."' ORDER BY A.status_id DESC;";
$query = $this->db->query($procedure);
// $str = $this->db->last_query();
// print_r($str);
// die;
return $query;
	}	  

public function work_status($array){

$procedure = "SELECT
A.status_id,
A.status_name

FROM status A 
WHERE A.status_id IN (8,9,10,14) ORDER BY A.status_id ASC;";
$query = $this->db->query($procedure);
return $query;
	}	  
	public function summary_report_filter($array){
		$next = true;
		$next_sub = false;
		$conquery = '';
		$subquery = '';

		if($array['from_date'] != NULL){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(date(A.last_update)>='".$array['from_date']."' and date(A.last_update)<= '".$array['to_date']."')";
		}
	
		if($array['product'] != '0'){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
				$conquery .= $and ."(A.product_id= '".$array['product']."')";
		}
		if($array['service_group'] != '1'){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(A.work_type='".$array['service_group']."')";
		}
	if($array['call_status'] == '0')
	{
		
		
	}
	else
	{
		if($array['call_status'] == '1'){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			
			$conquery .= $and ."(A.current_status NOT IN (12,8))";
		}
	else if($array['call_status'] == '2')
	{
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
		$conquery .= $and ."(A.current_status IN (12))";
	}
	else if($array['call_status'] == '3')
	{
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
		$conquery .= $and ."(A.current_status IN (8))";
	}
	else
	{
	}
	}
	if($array['visit_call'] == '0')
	{
		
	}
	else
	{
		if($array['visit_call'] != '>3')
		{
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."((SELECT COUNT(C.ticket_history_id) as visit_count FROM ticket_history C WHERE C.ticket_id=A.ticket_id and status = 7)='".$array['visit_call']."')";
		}
		else
		{
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."((SELECT COUNT(C.ticket_history_id) as visit_count FROM ticket_history C WHERE C.ticket_id=A.ticket_id and status = 7)>'3')";
		}
	}
	
//JOIN Queries are changed into subqueries due to summary report page loading(added on 01-08-2024 by muthu)
$procedure = "SELECT
A.ticket_id,
A.tech_id,
A.product_id,
 (SELECT C.product_name FROM product_management C WHERE C.product_id = A.product_id LIMIT 1) as product_name,
A.cat_id,
(SELECT D.cat_name FROM category_details D WHERE D.cat_id = A.cat_id LIMIT 1) as cat_name,
A.model,
A.serial_no,
A.cust_id,
(SELECT B.customer_name FROM customer B WHERE B.customer_id=A.cust_id and B.serial_no=A.serial_no LIMIT 1) as customer_name,
(SELECT CONCAT(T.first_name,' ',T.last_name) FROM technician T WHERE T.technician_id=A.tech_id LIMIT 1) as technician_name,
(SELECT B.invoice_date FROM customer B WHERE B.customer_id=A.cust_id and B.contact_number=A.contact_no LIMIT 1) as invoice_date,
(SELECT B.delivery_date FROM customer B WHERE B.customer_id=A.cust_id and B.contact_number=A.contact_no LIMIT 1) as delivery_date,
(SELECT B.vertical FROM customer B WHERE B.customer_id=A.cust_id and B.contact_number=A.contact_no LIMIT 1) as vertical,
(SELECT COUNT(F.ticket_history_id) FROM ticket_history F WHERE F.ticket_id=A.ticket_id and F.resolution_summary is NOT NULL) as visit_count,
(SELECT H.ap_value FROM fp_service_activities H WHERE H.ticket_id=A.ticket_id and H.service_id=A.work_type LIMIT 1) as ap_value,
(SELECT H.activite_id FROM fp_service_activities H WHERE H.ticket_id=A.ticket_id and H.service_id=A.work_type LIMIT 1) as activite_id,
(SELECT H.service_id FROM fp_service_activities H WHERE H.ticket_id=A.ticket_id and H.service_id=A.work_type LIMIT 1) as service_id,
A.current_status,
A.work_type,
A.raised_time,
A.town,
A.cust_rating,
A.cust_feedback,
A.ticket_end_time,
A.last_update,
(SELECT E.service_group FROM service_group E WHERE E.service_group_id = A.work_type LIMIT 1) as service_group,
A.company_feedback


FROM all_tickets A 
LEFT JOIN product_management C ON C.product_id=A.product_id
LEFT JOIN category_details D ON D.cat_id=A.cat_id
LEFT JOIN  service_group E ON E.service_group_id=A.work_type
WHERE A.work_type NOT IN(7,9) AND A.company_id='".$array['companyid']."' $conquery ORDER BY A.ticket_id DESC;";
$query = $this->db->query($procedure);

return $query;
}	  

public function service_checklist_status($activite_id,$service_id,$tck_id){ // to get ap value based on service group

   	$this->db->select('activite_id,ticket_id,ap_value,service_id');
	$this->db->from('fp_service_activities');
	$this->db->where('ticket_id',$tck_id);
	$this->db->where('service_id',$service_id);
	$this->db->where('activite_id',$activite_id);
	$query = $this->db->get();
	return $query;
	}	


//Technician report filter


public function technician_report_filter($array){
	$next = true;
	$conquery = '';

	if($array['from_date'] != NULL){
		if($next): $and = ' AND '; else: $and = ''; endif;
		$next = true;
		$conquery .= $and ."(date(A.last_update)>='".$array['from_date']."' and date(A.last_update)<= '".$array['to_date']."')";
	}

	if($array['technician'] != '0'){
		if($next): $and = ' AND '; else: $and = ''; endif;
		$next = true;
			$conquery .= $and ."(A.tech_id= '".$array['technician']."')";
	}

//JOIN Queries are changed into subqueries due to technician report page loading(added on 01-08-2024 by muthu)
$procedure = "SELECT
A.ticket_id,
A.cust_id,
A.product_id,
A.raised_time,
A.assigned_time,
A.acceptance_time,
A.ticket_start_time,
A.ticket_end_time,
A.closed_time,
A.cust_preference_date,
(SELECT C.product_name FROM product_management C WHERE C.product_id = A.product_id LIMIT 1) AS product_name, 
A.cat_id,
(SELECT D.cat_name FROM category_details D WHERE D.cat_id = A.cat_id LIMIT 1) AS cat_name,
A.model,
A.prob_desc,
A.resolution_summary,
A.serial_no,
(SELECT B.customer_name FROM customer B WHERE B.customer_id=A.cust_id  AND B.serial_no=A.serial_no  LIMIT 1) as customer_name,
A.current_status,
A.work_type,
A.last_update,
A.company_feedback,
A.cust_rating,
A.cust_feedback,
(SELECT E.first_name FROM technician E WHERE E.technician_id = A.tech_id LIMIT 1) AS first_name, 
(SELECT F.service_group FROM service_group F WHERE F.service_group_id = A.work_type LIMIT 1) AS service_group, 
(SELECT G.start_time FROM reimbursement G WHERE G.ticket_id = A.ticket_id LIMIT 1) AS start_time,
(SELECT G.end_time FROM reimbursement G WHERE G.ticket_id = A.ticket_id LIMIT 1) AS end_time 

FROM all_tickets A 

WHERE A.work_type NOT IN(7,9) AND (A.current_status NOT IN (0,1)) AND A.company_id='".$array['companyid']."' $conquery GROUP BY A.ticket_id ORDER BY A.ticket_id DESC;";

$query = $this->db->query($procedure);	
return $query;
}	

//ctpl report

public function ctpl_report_filter($array){
	$next = true;
		$next_sub = false;
		$conquery = '';
		$subquery = '';

		if($array['from_date'] != NULL){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(date(A.last_update)>='".$array['from_date']."' and date(A.last_update)<= '".$array['to_date']."')";
		}
	
		if($array['product'] != '0'){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
				$conquery .= $and ."(A.product_id= '".$array['product']."')";
		}
	if($array['call_status'] == '0')
	{
		
		
	}
	else
	{
		if($array['call_status'] == '1'){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			
			$conquery .= $and ."(A.current_status NOT IN (12,8))";
		}
	else if($array['call_status'] == '2')
	{
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
		$conquery .= $and ."(A.current_status IN (12))";
	}
	else if($array['call_status'] == '3')
	{
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
		$conquery .= $and ."(A.current_status IN (8))";
	}
	else
	{
	}
	}


$procedure = "SELECT
A.ticket_id,
A.cust_id,
A.ticket_end_time,
A.closed_time,
A.cust_preference_date,
C.product_name,
A.cat_id,
D.cat_name,
A.model,
A.prob_desc,
A.resolution_summary,
A.serial_no,
(SELECT B.customer_name FROM customer B WHERE B.customer_id=A.cust_id  AND B.serial_no=A.serial_no  LIMIT 1) as customer_name,
(SELECT B.invoice_date FROM customer B WHERE B.customer_id=A.cust_id and B.contact_number=A.contact_no LIMIT 1) as invoice_date,
(SELECT B.delivery_date FROM customer B WHERE B.customer_id=A.cust_id and B.contact_number=A.contact_no LIMIT 1) as delivery_date,
(SELECT COUNT(H.ticket_history_id) FROM ticket_history H WHERE H.ticket_id=A.ticket_id and H.resolution_summary is NOT NULL) as visit_count,
(SELECT I.ap_value FROM fp_service_activities I WHERE I.ticket_id=A.ticket_id and I.service_id=A.work_type LIMIT 1) as ap_value,
(SELECT I.activite_id FROM fp_service_activities I WHERE I.ticket_id=A.ticket_id and I.service_id=A.work_type LIMIT 1) as activite_id,
(SELECT I.service_id FROM fp_service_activities I WHERE I.ticket_id=A.ticket_id and I.service_id=A.work_type LIMIT 1) as service_id,
(SELECT B.vertical FROM customer B WHERE B.customer_id=A.cust_id and B.contact_number=A.contact_no LIMIT 1) as vertical,
A.current_status,
A.work_type,
A.town,
A.cust_feedback,
A.cust_rating,
A.last_update,
E.first_name,
F.service_group,
G.start_time,
G.end_time,
A.raised_time


FROM all_tickets A 
LEFT JOIN product_management C ON C.product_id=A.product_id
LEFT JOIN category_details D ON D.cat_id=A.cat_id
LEFT JOIN technician E ON E.technician_id=A.tech_id
LEFT JOIN  service_group F ON F.service_group_id=A.work_type
LEFT JOIN reimbursement G ON G.ticket_id=A.ticket_id
WHERE A.work_type =9  AND A.company_id='".$array['companyid']."' $conquery GROUP BY A.ticket_id ORDER BY A.ticket_id DESC;";

$query = $this->db->query($procedure);
return $query;
}	

//KRA KPI report filter

public function kra_kpi_report_filter($array){
	$next = true;
	$conquery = '';

	if($array['from_date'] != NULL){ // date filter
		if($next): $and = ' AND '; else: $and = ''; endif;
		$next = true;
		$conquery .= $and ."(date(A.last_update)>='".$array['from_date']."' and date(A.last_update)<= '".$array['to_date']."')";
	}

	if($array['technician'] != '0'){ // technician filter
		if($next): $and = ' AND '; else: $and = ''; endif;
		$next = true;
			$conquery .= $and ."(A.tech_id= '".$array['technician']."')";
	}



$procedure = "SELECT
A.ticket_id,
A.cust_id,
A.raised_time,
A.assigned_time,
A.acceptance_time,
A.ticket_start_time,
A.ticket_end_time,
A.closed_time,
A.cust_preference_date,
A.current_status,
A.work_type,
A.raised_time,
A.ticket_end_time,
A.last_update,
E.first_name,
E.employee_id,
F.service_group,
E.technician_id

FROM all_tickets A 
LEFT JOIN technician E ON E.technician_id=A.tech_id
LEFT JOIN  service_group F ON F.service_group_id=A.work_type
WHERE A.work_type NOT IN(7,9) AND A.company_id='".$array['companyid']."' $conquery GROUP BY A.tech_id ORDER BY A.ticket_id DESC;";
// $str = $this->db->last_query();
// print_r($str);
// die;
$query = $this->db->query($procedure);
return $query;
}

//ctpl kra list query

public function ctpl_kra_kpi_report_filter($array){
	$next = true;
	$conquery = '';

	if($array['from_date'] != NULL){ // date filter
		if($next): $and = ' AND '; else: $and = ''; endif;
		$next = true;
		$conquery .= $and ."(date(A.last_update)>='".$array['from_date']."' and date(A.last_update)<= '".$array['to_date']."')";
	}


$procedure = "SELECT
A.ticket_id,
A.cust_id,
A.raised_time,
A.assigned_time,
A.acceptance_time,
A.ticket_start_time,
A.ticket_end_time,
A.closed_time,
A.cust_preference_date,
A.current_status,
A.work_type,
A.raised_time,
A.ticket_end_time,
A.last_update,
E.first_name,
E.employee_id,
F.service_group,
E.technician_id

FROM all_tickets A 
LEFT JOIN technician E ON E.technician_id=A.tech_id
LEFT JOIN  service_group F ON F.service_group_id=A.work_type
WHERE A.work_type=9 AND A.tech_id='".$array['technician']."' AND A.company_id='".$array['companyid']."' $conquery GROUP BY A.tech_id ORDER BY A.ticket_id DESC;";
$query = $this->db->query($procedure);
return $query;
}


// E2E resolution
//(E2E) [90% of the calls s'd be closed in 3 days]
//Target is 90%
//Formula=E2E/90%

function end_to_end_difference($calc_data)
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id'];

	$where  = array(
		"all_tickets.tech_id" => $tech_id, // technician id
		// "all_tickets.current_status" => 8,  // Completed status
		"all_tickets.company_id" => $calc_data['companyid'] // Company id
	);
	$curr_status = "(all_tickets.current_status IN (8,12))"; //completed and closed status

	$date_selected = "(date(all_tickets.last_update)>='".$calc_data['from_date']."' and date(all_tickets.last_update)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,raised_time,ticket_end_time,last_update,company_id,current_status,tech_id');
		$this->db->from('all_tickets');
		$this->db->where($where); 
		$this->db->where($curr_status); 
		$this->db->where($date_selected); // date filter selected
		$query = $this->db->get();  // technician list with completed status
		$result = $query->result_array();
		
		if (!empty($result)) {
			$overall_tickets = $query->num_rows();  // total calls
			$obtained_target             = array(); 

			foreach ($result as $row) {
				$strStart = $row['raised_time'];  // ticket acceptance time
				$strEnd   = $row['ticket_end_time'];  // ticket end time
				$dteEnd   = new DateTime($strEnd);
				$dteStart = new DateTime($strStart);
				$dteDiff  = $dteEnd->diff($dteStart);
				$d = $dteDiff->format("%d"); // 3 days limit target
				$d = intval($d);

				if ($d <= 3) {   // target less  than 3 days
						array_push($obtained_target, 
							$d
						);
				}
			}
			
			if($overall_tickets > 0)
			{
				$calc = count($obtained_target) / $overall_tickets;  // Actual value
			}
			else 
			{
				$calc = count($obtained_target);  // Actual value
			}
			
			$actual1 = number_format($calc, 2, '.', '');
			$actual = $actual1 * 100;
			if($actual1 != 0)
			{
				$calc1 = ($actual / 90) * 100; // 90 is target value
				$percentage = (round($calc1)).'%';  // Average value

			}
			else{
				$percentage = 0;
			}


			array_push($final_result,
				$actual.'%',
				$percentage
			);
		} 
		else {
			array_push($final_result, 
				"-",
				"-"
			);
		}
	return $final_result;
}

// Avg calls per day 
// Here engineer needs to attend the call and update the resolution summary then only it will consider the calls for the day.
// If engg completed 5 calls per day with resolution then 5/4*100 will be 125%. Please clarify do we need count the total calls per day 

function average_calls($calc_data)
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id']; // Technician id
	$curr_status = "(all_tickets.current_status IN (8,9,10,12,14,15))";	// Completed,escalated,spare requested, work in progress and closed status.
	$where  = array(
		"all_tickets.tech_id" => $tech_id,
		"all_tickets.company_id" => $calc_data['companyid'],
	);
	$date_selected = "(date(all_tickets.last_update)>='".$calc_data['from_date']."' and date(all_tickets.last_update)<= '".$calc_data['to_date']."')";  // tickets dates

	$attendance_date_selected = "(date(attendance.last_update)>='".$calc_data['from_date']."' and date(attendance.last_update)<= '".$calc_data['to_date']."')"; //attendance dates
	
	$in_out_time =  "((attendance.in_time)!='00:00:00' and (attendance.out_time)!= '00:00:00')"; //in and out time 

		$this->db->select('ticket_id,tech_id,company_id,last_update,resolution_summary,ticket_start_time,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where);
		$this->db->where($curr_status);
		$this->db->where('all_tickets.resolution_summary is NOT NULL', NULL, FALSE); // resolution summary not null status in tickets table
		$this->db->where($date_selected); // filter date selected
		$query = $this->db->get();  
		$result = $query->result_array();

		$overall_result = count($result);

		

		if (!empty($result)) {
			$overall_tickets = $query->num_rows();
			$ar1             = array();  // no of calls fixed will be stored

			$this->db->select('technician_id,company_id,in_time,out_time');
			$this->db->from('attendance');
			$this->db->where('attendance.technician_id',$tech_id);
			$this->db->where($attendance_date_selected);
			$this->db->where($in_out_time);
			$attendance_query = $this->db->get(); 
			$attendance_result = $attendance_query->result_array();
			$total_attendance = $attendance_query->num_rows();

			$overall_attendance = count($attendance_result);
			
			if($overall_attendance > 0)
			{
				$actual =  (int)$overall_result / (int)$overall_attendance; // actual value
			}
			else 
			{
				$actual =  (int)$overall_result; // actual value
			}
			
			$actual_final = number_format($actual, 2, '.', '');

			if($actual != 0)
			{
				$calc = ($actual_final / 4)*100; 
				$percentage = (round($calc)).'%'; //average
			}
			else 
			{
				$percentage = 0;
			}
			
			

			array_push($final_result,
				$actual_final, // Actual value
				$percentage // average value
			);
		} 
		else {
			array_push($final_result, 
				"-",
				"-"
			);
		}

	return $final_result;

}

// First time fix

// The system needs to calculate the number of calls completed within the first visit in between dates. The number of visits calculated based on a resolution summary.

// Formula(FTF) = (Number of calls completed first visit / Total number of calls completed) * 100

function first_time_fix_calc($calc_data)
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id'];
	$curr_status = "(all_tickets.current_status IN (8,9,10,14,12))"; // completed, escalated, spare requested and work in progress status.
	$where  = array(
		"all_tickets.tech_id" => $tech_id, // technician id
		"all_tickets.company_id" => $calc_data['companyid'], // company id
		"all_tickets.work_type !=" => 8 // Rca Call
	);

	$date_selected = "(date(all_tickets.last_update)>='".$calc_data['from_date']."' and date(all_tickets.last_update)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,tech_id,current_status,company_id,last_update,ticket_start_time,ticket_end_time,resolution_summary');
		$this->db->from('all_tickets');
		$this->db->where($where);
		$this->db->where($curr_status);
		$this->db->where($date_selected); // filter date selected
		$this->db->where('all_tickets.resolution_summary is NOT NULL', NULL, FALSE);
		$query = $this->db->get();  // technician calls list with completed status
		$result = $query->result_array();

		if (!empty($result)) {
			$overall_tickets = $query->num_rows(); // total calls
			$obtained_target             = array();

			foreach ($result as $row) {

				$where_ticket = array(
					'ticket_history.ticket_id' => $row['ticket_id'],
					'ticket_history.technician_id' => $row['tech_id'],
				);

				$this->db->select('ticket_id,technician_id,status,company_id,resolution_summary');
				$this->db->from('ticket_history');
				$this->db->where($where_ticket);
				$this->db->where('ticket_history.resolution_summary is NOT NULL', NULL, FALSE);
				$history_query = $this->db->get();
				$history_result = $history_query->num_rows();

				if($history_result == 1) //if single visit
				{
					array_push($obtained_target, 
							$history_result
						);
				}

			} //end foreach

			$calc = (count($obtained_target) / $overall_tickets); 
			// $formula = round($calc);  // Actual value
			$formula = number_format($calc, 2, '.', '');

			if($calc != 0)
			{
				$calc1 = ($formula)*100;
				$percentage = (round($calc1)).'%';
			}
			else 
			{
				$percentage = 0;
			}


			array_push($final_result,
				$formula, // Actual 
				$percentage // Average
			);
		} 
		else {
			array_push($final_result, 
				"-",
				"-"
			);
		}
	return $final_result;

}

//pm calls 

function pm_calls_target_calc($calc_data,$username)
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id'];
	$curr_status = "(all_tickets.current_status IN (8,12))";	// completed and closed calls status
	$where  = array(
		"all_tickets.tech_id" => $tech_id, // technician id
		"all_tickets.company_id" => $calc_data['companyid'], // company id
		"all_tickets.work_type" => 4 // pm calls work type
	);

	$date_selected = "(date(all_tickets.ticket_end_time)>='".$calc_data['from_date']."' and date(all_tickets.ticket_end_time)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,tech_id,work_type,current_status,company_id,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where);
		$this->db->where($curr_status); // ticket status
		$this->db->where($date_selected);
		$query = $this->db->get();  // technician calls list with completed and closed status
		$result = $query->result_array();


		if (!empty($result)) {
			$overall_tickets = $query->num_rows(); // total pm calls by technician
			$obtained_target = array();

			// $this->db->select('ticket_id,tech_id,work_type,current_status,company_id,ticket_end_time');
			// $this->db->from('all_tickets');
			// $this->db->where('all_tickets.company_id', $calc_data['companyid']); // ticket status
			// $this->db->where('all_tickets.work_type',4);
			// $this->db->where($date_selected);
			// $pm_query = $this->db->get(); 
			// $overall_pm_tickets = $pm_query->num_rows(); // total pm calls in tickets table

			foreach ($result as $row) {

				$this->db->select('pm_calls_target');
				$this->db->from('user');
				$this->db->where('company_id',$calc_data['companyid']);
				$this->db->where('email_id',$username);
				$target_query = $this->db->get();   // pm calls target query from user table
				$row_array = $target_query->row_array();
				$target_res = $row_array['pm_calls_target']; // pm calls target value in manager 
				
			}
			
			// $calc = $overall_tickets / $target_res;  // Formula = (total no. of pm calls completed/target)* 100
			// $formula = number_format($calc, 2, '.', ''); 

				if($overall_tickets != 0)
				{
					$avg_calc = ($overall_tickets / $target_res)*100; 
					$percentage = (round($avg_calc)).'%';  // Average
				}
				else 
				{
					$percentage = 0;
				}
		

			array_push($final_result,
				$overall_tickets, // Actual value
				$percentage // Average value
			);
		} 
		else {
			array_push($final_result, 
				"-",
				"-"
			);
		}

	return $final_result;

}

function pm_call_target($company_id,$username) // pm calls target in target column in data list
{
	$this->db->select('pm_calls_target');
	$this->db->from('user');
	$this->db->where('company_id',$company_id);
	$this->db->where('email_id',$username);
	$target_query = $this->db->get();
	$row_array = $target_query->row_array();
	$target_res = $row_array['pm_calls_target'];
	return $target_res;
}

// Parts per serisl no
//  The system needs to calculate the number of spare parts used per serial number in between the dates.

function parts_per_serial_calc($calc_data) 
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id']; // technician id
	$curr_status = "(all_tickets.current_status IN (8,12))";	// Completed and closed calls
	$where  = array(
		"all_tickets.tech_id" => $tech_id,
		"all_tickets.company_id" => $calc_data['companyid'],
		"all_tickets.spare_requested" => 1 // spare requested status in all+_ticktes table
	);

	$date_selected = "(date(all_tickets.last_update)>='".$calc_data['from_date']."' and date(all_tickets.last_update)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,tech_id,work_type,serial_no,current_status,company_id,ticket_end_time,requested_spare,spare_requested,spare_quantity');
		$this->db->from('all_tickets');
		$this->db->where($where);
		$this->db->where($curr_status);  // calls status
		$this->db->where($date_selected); // filter date selected
		$this->db->group_by('serial_no'); 
		$query = $this->db->get();  // technician calls list with completed and closed status
		$result = $query->result_array();

		if (!empty($result)) {
			$overall_tickets = $query->num_rows(); // total calls
			$obtained_target             = array();

			$total_spare_consumed = 0; // total quantity of spares 
			foreach ($result as $row) {
				$spare_qty = $row['spare_quantity']; // spare quantity in tickets table
				$total_spare_consumed = $spare_qty + $total_spare_consumed; 
			}


			// Formula = (total no. of parts used/total no. of spare calls completed)
			
			$calc = $total_spare_consumed / $overall_tickets; 
			$calc_deci = number_format($calc, 2, '.', ''); 
			if($calc != 0)
			{
				$avg =  ((($calc-1.12)/(1.12)) * 100); // actual value
				$formula = number_format($avg, 2, '.', ''); 
				$percent_avg = 100 - ($avg);
				$percentage = (round($percent_avg)).'%'; // avarage (%)
			}
			else 
			{
				$percentage = 0;
			}

			array_push($final_result,
				$calc_deci, // Actual value
				$percentage // Average value
			);
		} 
		else {
			array_push($final_result, 
				"-",
				"-"
			);
		}
	

	return $final_result;

}

// Calls per serial number
// This report is used to get the number of repeated calls in the same serial number.
// This is applicable only for the CM , gcsn work type.

function calls_per_serial_calc($calc_data)
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id']; // technican id
	$where  = array(
		"all_tickets.tech_id" => $tech_id,
		"all_tickets.company_id" => $calc_data['companyid'],
	);
	$call_type = "(all_tickets.work_type NOT IN (4,8))"; // cm and gcsn work type

	$date_selected = "(date(all_tickets.assigned_time)>='".$calc_data['from_date']."' and date(all_tickets.assigned_time)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,tech_id,work_type,serial_no,current_status,company_id,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where);
		$this->db->where($date_selected); // filter date selected
		$this->db->where($call_type); // work type
		$this->db->group_by('serial_no');
		$query = $this->db->get();  // technician calls list with cm/gcsn work type 
		$result = $query->result_array();


		$this->db->select('ticket_id,tech_id,work_type,serial_no,current_status,company_id,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where);
		$this->db->where($date_selected); // filter date selected
		$this->db->where($call_type);
		$serial_query = $this->db->get();  // technician calls list with cm/gcsn work type for serial no
		$serial_result = $query->result_array();

		if (!empty($result))
		{
			$overall_tickets = $query->num_rows(); //total no. unique serial no. for above calls
			$overall_tickets_serial = $serial_query->num_rows(); //	Total no. of tickets (Except PM & RCA call type)

			//Actual value= total no. of tickets (except PM call & RCA Call)/Total no. of unique serial no. for above calls


			$calc = $overall_tickets_serial / $overall_tickets; 
			$calc_deci = number_format($calc, 2, '.', ''); 
			if($calc != 0)
			{
				$avg =  ((($calc-1)/(1)) * 100); // actual value
				$formula = number_format($avg, 2, '.', ''); 
				$percent = 100 - ($avg);
				$percentage = (round($percent)).'%'; // avarage (%)
			}
			else 
			{
				$percentage = 0;
			}
			
			array_push($final_result,
					$calc_deci, // Actual value
					$percentage // Average value
				);

		}
		else
		{
			array_push($final_result, 
				"-",
				"-"
			);
		}

	return $final_result;
}


function mtr_calc($calc_data) // mtr calc for prakash manager
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id'];

	$where  = array(
		"all_tickets.tech_id" => $tech_id, // technician id $tech_id
		"all_tickets.company_id" => $calc_data['companyid'] // Company id
	);
	$curr_status = "(all_tickets.current_status IN (8,12))"; //completed and closed status

	$date_selected = "(date(all_tickets.last_update)>='".$calc_data['from_date']."' and date(all_tickets.last_update)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,raised_time,ticket_end_time,last_update,company_id,current_status,tech_id');
		$this->db->from('all_tickets');
		$this->db->where($where); 
		$this->db->where($curr_status); 
		$this->db->where($date_selected); // date filter selected
		$query = $this->db->get();  // technician list with completed status
		$result = $query->result_array();


		if (!empty($result)) {
			$overall_tickets = $query->num_rows();  // total calls
			$obtained_target = array(); 
			$total_hrs = 0; // initializing total hrs of completed time
			foreach ($result as $row) {
				$strStart = $row['ticket_start_time'];  // ticket start time
				$strEnd   = $row['ticket_end_time'];  // ticket end time
				$dteEnd   = new DateTime($strEnd);
				$dteStart = new DateTime($strStart);
				$dteDiff  = $dteEnd->diff($dteStart);
				$hrs = $dteDiff->format("%h");  //hrs
				$mins = $dteDiff->format("%i"); //mins
				if($mins > 0)
				{
					$mins = ((int)$mins) / 60; // getting minutes in decimal
					$total_mins = number_format($mins, 2, '.', ''); 
				}
				else 
				{
					$mins = 0;
				}
				
				$total_time = (int)$hrs + $mins; //total hrs
				$total_hrs = $total_time + $total_hrs; //adding all the hours
				
			}

			$actual = intval($total_hrs/$overall_tickets); // final actual result

			if ($actual > 3) // target greater  than 3 hrs for ctpl calls
			{   
				$obtained_target = (3/$actual); // 3-target for mtr
				$obtained_target = number_format($obtained_target, 2, '.', '');
			}
			else 
			{
				$obtained_target = '100%';
				
			}


			array_push($final_result,
				$actual.' hrs', // Actual
				$obtained_target // Average
			);
		} 
		else {
			array_push($final_result, 
				"-",
				"-"
			);
		}
	return $final_result;
}


function ctpl_mtr_calc($calc_data) // mtr calc for ctpl manager
{
	$json  = array();
	$final_result    = array();
	$tech_id = $calc_data['technician_id'];

	$where  = array(
		"all_tickets.tech_id" => $tech_id, // technician id $tech_id
		"all_tickets.company_id" => $calc_data['companyid'] // Company id
	);
	$curr_status = "(all_tickets.current_status IN (8,12))"; //completed and closed status

	$date_selected = "(date(all_tickets.last_update)>='".$calc_data['from_date']."' and date(all_tickets.last_update)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,raised_time,ticket_end_time,last_update,company_id,current_status,tech_id');
		$this->db->from('all_tickets');
		$this->db->where($where); 
		$this->db->where($curr_status); 
		$this->db->where($date_selected); // date filter selected
		$query = $this->db->get();  // technician list with completed status
		$result = $query->result_array();
		
		if (!empty($result)) {
			$overall_tickets = $query->num_rows();  // total calls
			$obtained_target = array(); 
			$total_hrs = 0; // initializing total hrs of completed time
			foreach ($result as $row) {
				$strStart = $row['raised_time'];  // ticket raised time
				$strEnd   = $row['ticket_end_time'];  // ticket end time
				$dteEnd   = new DateTime($strEnd);
				$dteStart = new DateTime($strStart);
				$dteDiff  = $dteEnd->diff($dteStart);
				$hrs = $dteDiff->format("%h");  //hrs
				$mins = $dteDiff->format("%i"); //mins
				if($mins > 0)
				{
					$mins = ((int)$mins) / 60; // getting minutes in decimal
					$total_mins = number_format($mins, 2, '.', ''); 
				}
				else 
				{
					$mins = 0;
				}
				
				$total_time = (int)$hrs + $mins; //total hrs
				$total_hrs = $total_time + $total_hrs; //adding all the hours
				
			}

			$actual = intval($total_hrs/$overall_tickets); // final actual result

			if ($actual > 2) // target greater  than 2 hrs for ctpl calls
			{   
				$obtained_target = (2/$actual); // 2-target for mtr
			}
			else 
			{
				$obtained_target = '100%';
				
			}


			array_push($final_result,
				$actual.' hrs', // Actual
				$obtained_target // Average
			);
		} 
		else {
			array_push($final_result, 
				"-",
				"-"
			);
		}
	return $final_result;
}


function mtc_calc($calc_data) // mtc calc for prakash manager
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id'];

	$where  = array(
		"all_tickets.tech_id" => $tech_id, // technician id $tech_id company51_Tech_0553
		"all_tickets.company_id" => $calc_data['companyid'] // Company id
	);
	$curr_status = "(all_tickets.current_status IN (8,12))"; //completed and closed status

	$date_selected = "(date(all_tickets.last_update)>='".$calc_data['from_date']."' and date(all_tickets.last_update)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,raised_time,ticket_end_time,last_update,company_id,current_status,tech_id');
		$this->db->from('all_tickets');
		$this->db->where($where); 
		$this->db->where($curr_status); 
		$this->db->where($date_selected); // date filter selected
		$query = $this->db->get();  // technician list with completed status
		$result = $query->result_array();


		if (!empty($result)) {
			$overall_tickets = $query->num_rows();  // total calls
			$obtained_target = array(); 
			$total_hrs = 0; // initializing total hrs of completed time
			foreach ($result as $row) {
				$strStart = $row['raised_time'];  // ticket raised time
				$strEnd   = $row['ticket_end_time'];  // ticket end time
				// $strStart = '2020-02-01 11:52:23';  // ticket raised time
				// $strEnd   = '2020-02-03 2:01:50'; 
				$dteEnd   = new DateTime($strEnd);
				$dteStart = new DateTime($strStart);
				$dteDiff  = $dteEnd->diff($dteStart);
				$days = $dteDiff->format("%a");  //days
				$hrs = $dteDiff->format("%h");  //hrs
				$mins = $dteDiff->format("%i"); //mins
				if($mins > 0)
				{
					$day_hrs = ((int)$days) * 24; 
					$mins = ((int)$mins) / 60; // getting minutes in decimal
					$total_mins = number_format($mins, 2, '.', ''); 
				}
				else 
				{
					$mins = 0;
				}
				
				$total_time = $day_hrs + (int)$hrs + $mins; //total hrs
				$total_hrs = $total_time + $total_hrs; //adding all the hours
			}


			$actual = intval($total_hrs/$overall_tickets); // final actual result

			if ($actual > 24
			) // target greater  than 24 hrs for ctpl calls
			{   
				$obtained_target = 100-(($actual-24)/24);

				$obtained_target = $obtained_target*100;
				$average_value = number_format($obtained_target, 2, '.', ''); 
			}
			else 
			{
				$average_value = '100%';
			}


			array_push($final_result,
				$actual.' hrs', // Actual
				$average_value // Average
			);
		} 
		else {
			array_push($final_result, 
				"-",
				"-"
			);
		}
	return $final_result;
}



function mta_calc($calc_data) // mta calc for prakash manager
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id'];

	$where  = array(
		"all_tickets.tech_id" => $tech_id, // technician id 
		"all_tickets.company_id" => $calc_data['companyid'] // Company id
	);
	$curr_status = "(all_tickets.current_status IN (8,12))"; //completed and closed status

	$date_selected = "(date(all_tickets.last_update)>='".$calc_data['from_date']."' and date(all_tickets.last_update)<= '".$calc_data['to_date']."')";

		$this->db->select('all_tickets.ticket_id,all_tickets.raised_time,all_tickets.ticket_end_time,all_tickets.last_update,all_tickets.company_id,all_tickets.current_status,all_tickets.tech_id,reimbursement.start_time,reimbursement.end_time');
		$this->db->from('all_tickets');
		$this->db->join('reimbursement','all_tickets.ticket_id=reimbursement.ticket_id');
		$this->db->where($where); 
		$this->db->where($curr_status); 
		$this->db->where($date_selected); // date filter selected
		$query = $this->db->get();  // technician list with completed status
		$result = $query->result_array();


		if (!empty($result)) {
			$overall_tickets = $query->num_rows();  // total calls
			$obtained_target = array(); 
			$total_hrs = 0; // initializing total hrs of completed time
			foreach ($result as $row) {
				$strStart = $row['start_time'];  // travel start time
				$strEnd   = $row['end_time'];  // travel end time
				$dteEnd   = new DateTime($strEnd);
				$dteStart = new DateTime($strStart);
				$dteDiff  = $dteEnd->diff($dteStart);
				$hrs = $dteDiff->format("%h");  //hrs
				$mins = $dteDiff->format("%i"); //mins
				if($mins > 0)
				{
					$mins = ((int)$mins) / 60; // getting minutes in decimal
					$total_mins = number_format($mins, 2, '.', ''); 
				}
				else 
				{
					$mins = 0;
				}
				
				$total_time = (int)$hrs + $mins; //total hrs
				$total_hrs = $total_time + $total_hrs; //adding all the hours
				
			}

			$actual = number_format($total_hrs/$overall_tickets,2); // final actual result

			if ($actual > 4) // target greater  than 2 hrs for ctpl calls
			{   
				$obtained_target = 100-(($actual-4)/4); // 4-target for mta
				$obtained_target = $obtained_target*100;
				$average_value = number_format($obtained_target, 2, '.', ''); 
			}
			else 
			{
				$average_value = '100%';
			}


			array_push($final_result,
				$actual.' hrs', // Actual
				$average_value // Average
			);
		} 
		else 
		{
			array_push($final_result, 
				"-",
				"-"
			);
		}
	return $final_result;
}





function ctpl_mtc_calc($calc_data) //mtc calc for ctpl manager
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id'];

	$where  = array(
		"all_tickets.tech_id" => $tech_id, // technician id $tech_id company51_Tech_0553
		"all_tickets.company_id" => $calc_data['companyid'] // Company id
	);
	$curr_status = "(all_tickets.current_status IN (8,12))"; //completed and closed status

	$date_selected = "(date(all_tickets.last_update)>='".$calc_data['from_date']."' and date(all_tickets.last_update)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,raised_time,ticket_end_time,last_update,company_id,current_status,tech_id');
		$this->db->from('all_tickets');
		$this->db->where($where); 
		$this->db->where($curr_status); 
		$this->db->where($date_selected); // date filter selected
		$query = $this->db->get();  // technician list with completed status
		$result = $query->result_array();
		
		if (!empty($result)) {
			$overall_tickets = $query->num_rows();  // total calls
			$obtained_target = array(); 
			$total_hrs = 0; // initializing total hrs of completed time
			foreach ($result as $row) {
				$strStart = $row['raised_time'];  // ticket raised time
				$strEnd   = $row['ticket_end_time'];  // ticket end time
				$dteEnd   = new DateTime($strEnd);
				$dteStart = new DateTime($strStart);
				$dteDiff  = $dteEnd->diff($dteStart);
				$hrs = $dteDiff->format("%h");  //hrs
				$mins = $dteDiff->format("%i"); //mins
				if($mins > 0)
				{
					$mins = ((int)$mins) / 60; // getting minutes in decimal
					$total_mins = number_format($mins, 2, '.', ''); 
				}
				else 
				{
					$mins = 0;
				}
				
				$total_time = (int)$hrs + $mins; //total hrs
				$total_hrs = $total_time + $total_hrs; //adding all the hours
				
			}

			$actual = intval($total_hrs/$overall_tickets); // final actual result

			if ($actual > 24) // target greater  than 24 hrs for ctpl calls
			{   
				$obtained_target = (24/$actual); // 24-target for mtr
			}
			else 
			{
				$obtained_target = '100%';
			}


			array_push($final_result,
				$actual.' hrs', // Actual
				$obtained_target // Average
			);
		} 
		else {
			array_push($final_result, 
				"-",
				"-"
			);
		}
	return $final_result;
}



function kra_customer_feedback($calc_data) // customer feedback // calculation same for both ctpl and prakash manager
{
	$json  = array();
	$final_result    = array();

	$tech_id = $calc_data['technician_id']; // technican id
	$where  = array(
		"all_tickets.tech_id" => $tech_id,
		"all_tickets.company_id" => $calc_data['companyid'],
	);
	$curr_status = "(all_tickets.current_status IN (12,8))"; // completed and closed calls

	$date_selected = "(date(all_tickets.assigned_time)>='".$calc_data['from_date']."' and date(all_tickets.assigned_time)<= '".$calc_data['to_date']."')";

		$this->db->select('ticket_id,tech_id,work_type,serial_no,current_status,company_id,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where);
		$this->db->where($date_selected); // filter date selected
		$this->db->where($curr_status); // work type
		$query = $this->db->get();  //completed and closed query
		$result = $query->result_array(); 
		$overall_tickets = count($result);

		// $feedback_status = "(all_tickets.cust_feedback)='Completely Satisfied' or (all_tickets.cust_feedback) = 'Satisfied'"; 

		$feedback_status = "(all_tickets.cust_feedback IN ('Completely Satisfied','Satisfied'))";

		$this->db->select('ticket_id,tech_id,work_type,serial_no,current_status,company_id,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where);
		$this->db->where($date_selected); 
		$this->db->where($curr_status);
		$this->db->where($feedback_status); 
		$feedback_query = $this->db->get();  //satisfied and completly satisfied in the completed and closed calls
		$serial_result = $feedback_query->result_array();
		$satisfied_tickets = count($serial_result);
		

		if (!empty($result))
		{
			//$overall_tickets = $query->num_rows(); // no of completed and closed calls
			//$satisfied_tickets = $feedback_query->num_rows(); //no of satisfied and completly satisfied in overall tickets

			$calc =(int) $satisfied_tickets /(int) $overall_tickets; 
			$calc_deci = number_format($calc, 2, '.', ''); // Actual value
			if($calc != 0)
			{
				//$avg = ($calc_deci/90) * 100;
				$avg = ($calc_deci)/(90/100) ;
				$avg_format = number_format($avg, 2, '.', ''); 
				$per = $avg_format * 100;
				$percentage = (round($per)).'%';
			}
			else 
			{
				$percentage = 0;
			}
			
			array_push($final_result,
					$calc_deci, // Actual value
					$percentage // Average value
				);

		}
		else
		{
			array_push($final_result, 
				"-",
				"-"
			);
		}

	return $final_result;
}






public function aging_report_filter($array){
		$next = true;
		$next_sub = false;
		$conquery = '';
		$subquery = '';

	/*	if($array['from_date'] != NULL){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(date(A.last_update)>='".$array['from_date']."' and date(A.last_update)<= '".$array['to_date']."')";
		}
	*/
		if($array['aging_dur'] == '<=1'){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(datediff(date(A.raised_time),date(A.ticket_end_time))<=1)";
		}
	elseif($array['aging_dur'] == '>3<=7')
	{
		if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(datediff(date(A.raised_time),date(A.ticket_end_time))>3 and datediff(date(A.raised_time),date(A.ticket_end_time))<=7)";
	}
	elseif($array['aging_dur'] == '>7')
	{
		if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(datediff(date(A.raised_time),date(A.ticket_end_time))>7)";
	}
	else
	{
	}
		if($array['service_group'] != '1'){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(A.work_type='".$array['service_group']."')";
		}
	
		if($array['work_status'] != '0'){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(A.current_status='".$array['work_status']."')";
		}
	

	

 $procedure = "SELECT
A.ticket_id,
A.product_id,
C.product_name,
A.cat_id,
D.cat_name,
A.model,
A.serial_no,
A.cust_id,
(SELECT B.customer_name FROM customer B WHERE B.customer_id=A.cust_id and B.contact_number=A.contact_no LIMIT 1) as customer_name,
(SELECT COUNT(C.ticket_history_id) as visit_count FROM ticket_history C WHERE C.ticket_id=A.ticket_id) as visit_count,
A.current_status,
A.work_type,
A.raised_time,
A.ticket_end_time,
A.last_update


FROM all_tickets A 
LEFT JOIN product_management C ON C.product_id=A.product_id
LEFT JOIN category_details D ON D.cat_id=A.cat_id
WHERE A.work_type!=7 AND A.raised_time BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) AND NOW() AND A.current_status IN (8,9,10,14) AND A.company_id='".$array['companyid']."' $conquery ORDER BY A.ticket_id DESC;";


		$query = $this->db->query($procedure);
		return $query;
	}	  

public function ticket_excel($array){
		$next = true;
		$next_sub = false;
		$conquery = '';
		$subquery = '';
$companyid='company51';
$bill_status=1;	
	
		if($array['dateof'] != ''){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			
		  // $conquery .= $and ."(date(A.last_update)>='2019-05-05' AND date(A.last_update)<='2019-05-07')";				
		  $conquery .= $and ."(A.last_update>='".$array['dateof']."' AND A.last_update<='".$array['currdate']."')";
			 
		}
	
		
	

$procedure = "SELECT
A.id,
A.ticket_id,
A.amc_id,
A.cust_id,
(SELECT customer_name FROM customer AA WHERE AA.customer_id=A.cust_id AND AA.product_serial_no=A.product_id AND AA.component_serial_no=A.cat_id LIMIT 1) customer_name,
(SELECT email_id FROM customer AA WHERE AA.customer_id=A.cust_id AND AA.product_serial_no=A.product_id AND AA.component_serial_no=A.cat_id LIMIT 1) email_id,
A.tech_id,
(SELECT employee_id FROM technician BB WHERE BB.technician_id=A.tech_id LIMIT 1) employee_id,
A.prev_tech_id,
A.product_id,
C.product_name,
A.cat_id,
D.cat_name,
A.model,
A.serial_no,
A.quantity,
A.contract_period,
A.contact_no,
A.alternate_no,
A.door_no,
A.address,
A.town,
A.city,
A.state,
A.country,
A.location,
A.latitude,
A.longitude,
A.landmark,
A.pincode,
A.region,
A.previous_contract_id,
A.prob_desc,
A.call_tag,
E.call,
A.cust_category,
A.escalated_description,
A.image,
A.audio,
A.video,
A.suggested_skill_level,
A.sug_tech_name,
A.call_type,
A.priority,
A.resolution_time,
A.response_time,
A.target_aceptance_time,
A.current_status,
A.otp_code,
A.previous_status,
A.reason,
A.work_type,
F.service_group as wt_name,
A.prev_work_type,
G.service_group as prev_wt_name,
A.poa,
A.schedule_next,
A.cust_acceptance,
A.avalability,
A.is_it_chargeable,
A.lead_time,
A.spare_cost,
A.service_charge,
A.tax,
A.product_gst,
A.service_gst,
A.requested_spare,
A.frm,
A.drop_spare,
A.drop_of_date,
A.drop_of_location,
A.spare_array,
A.cust_preference_date,
A.purchase_date,
A.estimation_time,
A.raised_time,
A.assigned_time,
A.acceptance_time,
A.total_amount,
A.bill_no,
A.bill_status,
A.view,
A.mode_of_payment,
A.check_no,
A.check_date,
A.transaction_id,
A.ticket_start_time,
A.ticket_end_time,
A.closed_time,
A.status,
A.rating_key,
A.cust_feedback,
A.cust_rating,
A.cust_reason,
A.company_id,
A.reached_cust_location,
A.resolution_summary,
A.signature,
A.last_update

FROM all_tickets A 
LEFT JOIN product_management C ON C.product_id=A.product_id
LEFT JOIN category_details D ON D.cat_id=A.cat_id
LEFT JOIN call_tag E ON E.id=A.call_tag
LEFT JOIN service_group F ON F.service_group_id=A.work_type
LEFT JOIN service_group G ON G.service_group_id=A.prev_work_type
WHERE A.tech_id!='company51_Tech_0007' AND A.work_type NOT IN(7,9) AND A.current_status IN (8,9,10,12,14) AND A.company_id='".$companyid."' $conquery ORDER BY A.id DESC;";

		$query = $this->db->query($procedure);
		return $query;
	}	  

public function knowledgebase_excel($array){
		$next = true;
		$next_sub = false;
		$conquery = '';
		$subquery = '';
$companyid='company51';
	
		if($array['dateof'] != ''){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(date(A.last_update)='".$array['dateof']."')";
		}
	
		
	

$procedure = "SELECT
A.id,
A.product,
C.product_name,
A.category,
A.sub_category,
D.cat_name,
D.cat_desc,
A.problem,
A.solution,
A.image,
A.video,
A.technician_id,
A.ticket_id,
A.company_id,
A.region,
A.area,
A.status,
A.last_update


FROM knowledge_base A 
LEFT JOIN product_management C ON C.product_id=A.product
LEFT JOIN category_details D ON D.cat_id=A.category
WHERE A.product!='' AND A.status=1 AND A.company_id='".$companyid."' $conquery ORDER BY A.id DESC;";


		$query = $this->db->query($procedure);
		return $query;
	}	  

public function spare_excel($array){
		$next = true;
		$next_sub = false;
		$conquery = '';
		$subquery = '';
$companyid='company51';
	
		if($array['dateof'] != ''){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(date(A.last_update)='".$array['dateof']."')";
		}
	
		
	

$procedure = "SELECT
A.id,
A.spare_id,
A.spare_code,
A.spare_desc,
A.spare_name,
A.spare_modal,
A.image,
A.spare_source,
A.price,
A.quantity_purchased,
A.quantity_used,
A.product,
C.product_name,
A.category,
D.cat_name,
A.company_id,
A.region,
A.area,
A.quantity,
A.p_date,
A.ep_date,
A.expiry_date,
A.tech_id,
A.last_update


FROM spare A 
LEFT JOIN product_management C ON C.product_id=A.product
LEFT JOIN category_details D ON D.cat_id=A.category
WHERE A.company_id='".$companyid."' $conquery ORDER BY A.id DESC;";


		$query = $this->db->query($procedure);
		return $query;
	}	  

public function inventory_export_excel(){
		$next = true;
		$next_sub = false;
		$conquery = '';
		$subquery = '';
$procedure = "SELECT
A.spare_code,
A.spare_name,
A.image,
A.product,
C.product_name,
A.spare_modal
FROM spare A 
LEFT JOIN product_management C ON C.product_id=A.product;";


		$query = $this->db->query($procedure);
		return $query;
	}	  

	public function customer_export_excel($array){
		$next = true;
		$next_sub = false;
		$conquery = '';
		$subquery = '';
$companyid='company51';
	
		if($array['dateof'] != ''){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(date(A.last_update)='".$array['dateof']."')";
		}
	
		
	

$procedure = "SELECT
A.customer_id,
A.customer_name,
A.email_id,
A.contact_number,
A.alternate_number,
B.product_name,
C.cat_name,
A.door_num,
A.address,
A.cust_town,
A.landmark,
A.region,
A.city,
A.state,
A.cust_country,
A.pincode,
A.type_of_contract,
A.serial_no,
A.model_no,
A.priority,
A.warrenty_expairy_date,
A.contract_duration,
A.start_date,
A.end_date,
A.invoice_date,
A.delivery_date




FROM customer A 
LEFT JOIN product_management B ON B.product_id=A.product_serial_no
LEFT JOIN category_details C ON C.cat_id=A.component_serial_no
WHERE A.company_id='".$companyid."' ORDER BY A.id ASC;";


		$query = $this->db->query($procedure);
		return $query;
	}	  

public function product_subproduct_excel($array){
		$next = true;
		$next_sub = false;
		$conquery = '';
		$subquery = '';
$companyid='company51';
	
		if($array['dateof'] != ''){
			if($next): $and = ' AND '; else: $and = ''; endif;
			$next = true;
			$conquery .= $and ."(date(A.last_update)='".$array['dateof']."')";
		}
	
		
	

$procedure = "SELECT
A.product_id,
A.product_name,
A.product_modal,
A.product_desc,
B.cat_id,
B.cat_name,
B.cat_modal,
B.cat_desc


FROM product_management A 
LEFT JOIN category_details B ON B.prod_id=A.product_id
WHERE A.company_id='".$companyid."' ORDER BY A.id ASC;";


		$query = $this->db->query($procedure);
		return $query;
	}	  



}
?>
