<?php
date_default_timezone_set('Asia/Kolkata');
class Customer extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor  
        parent::__construct();
    }
    public function company()
    {
        $this->db->select('company_id,company_name');
        $this->db->from('company');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
   public function load_tickets($cust_id, $company_id)
{
    $this->db->select('
        all_tickets.ticket_id, 
        all_tickets.tech_id as tech_name, 
        all_tickets.product_id as product_name, 
        all_tickets.cat_id as cat_name, 
        all_tickets.contact_no,
        all_tickets.door_no, 
        all_tickets.address as street, 
        all_tickets.town, 
        all_tickets.city, 
        all_tickets.state, 
        all_tickets.country,
        all_tickets.landmark, 
        all_tickets.pincode,
        all_tickets.prob_desc, 
        all_tickets.call_tag as call_category,
        all_tickets.call_type as service_category,
        all_tickets.current_status, 
        all_tickets.cust_preference_date, 
        all_tickets.raised_time,
        all_tickets.total_amount,
        all_tickets.bill_no,
        all_tickets.ticket_start_time,
        all_tickets.ticket_end_time
    ');

    $this->db->from('all_tickets');
    $this->db->where('all_tickets.company_id', $company_id);
    $this->db->where('all_tickets.cust_a	id', $cust_id);
    $this->db->group_by('all_tickets.ticket_id');

    $query = $this->db->get();
    return $query->result_array();
}

    public function load_service_category()
    {
        $this->db->select('amc_type,id');
        $this->db->from('amc_type');
      //  $this->db->where('company_id', $company_id);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_cust_service_category($cust_id,$company_id)
    {
        $this->db->select('amc_type.amc_type,amc_type.id,customer.customer_id');
        $this->db->from('amc_type,customer');
        $this->db->where('customer.customer_id', $cust_id);
        $this->db->where('customer.company_id', $company_id);
        $this->db->where('amc_type.amc_type=customer.type_of_contract');
        $this->db->group_by('customer.type_of_contract');
        $query  = $this->db->get();
	    $re= $query->result_array();
        return $re;
    }
    public function load_call_tags($company_id)
    {
        $this->db->select('call,id');
        $this->db->from('call_tag');
       // $this->db->where('company_id', $company_id);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_product($company_id)
    {
        $this->db->select('product_id,product_name');
        $this->db->from('product_management');
        $this->db->where('company_id', $company_id);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
	public function load_customer_details($company_id,$cust_id)
	{
		$this->db->select('*');
        $this->db->from('product_management,customer,category_details,amc_type');
        $this->db->where('customer.customer_id',$cust_id);
        $this->db->where('customer.product_serial_no=product_management.product_id');
        $this->db->where('customer.component_serial_no=category_details.cat_id');
        $this->db->where('amc_type.amc_type=customer.type_of_contract');
        $this->db->where('customer.company_id', $company_id);
        $this->db->group_by('customer.id');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
	}
    public function load_cust_product($company_id,$cust_id)
    {
        $this->db->select('product_management.product_id,product_management.product_name');
        $this->db->from('product_management,customer');
        $this->db->where('customer.customer_id',$cust_id);
        $this->db->where('customer.product_serial_no=product_management.product_id');
        $this->db->where('customer.company_id', $company_id);
        $this->db->group_by('customer.product_serial_no');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_cust_category($company_id,$cust_id,$product_id)
    {
        $this->db->select('category_details.cat_id,category_details.cat_name');
        $this->db->from('customer,category_details');
        $this->db->where('customer.customer_id',$cust_id);
        $this->db->where('category_details.prod_id',$product_id);
        $this->db->where('customer.component_serial_no=category_details.cat_id');
        $this->db->where('customer.company_id', $company_id);
        $this->db->group_by('customer.component_serial_no');
        $query  = $this->db->get();
		
        $result = $query->result_array();
        return $result;
    }
    public function load_model($cust_id,$company_id,$product_id,$cat_id)
    {
        $this->db->select('customer.model_no,customer.serial_no,customer.type_of_contract');
        $this->db->from('customer');
        $this->db->where('customer.customer_id',$cust_id);
        $this->db->where('customer.product_serial_no',$product_id);
        $this->db->where('customer.component_serial_no',$cat_id);
        $this->db->where('customer.company_id', $company_id);
        $this->db->group_by('customer.customer_id');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_category($company_id, $product_id)
    {
        $this->db->select('cat_id,cat_name');
        $this->db->from('category_details');
        $this->db->where('company_id', $company_id);
        $this->db->where('prod_id', $product_id);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function check_cust($emailid, $contact_no, $product_id, $cat_id, $model_no, $serial_no, $company_id)
    {
        $where = array(
            "email_id" => $emailid,
            "contact_number" => $contact_no,
            "product_serial_no" => $product_id,
            "component_serial_no" => $cat_id,
            "model_no" => $model_no,
            "serial_no" => $serial_no,
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function get_cust_id($company_id)
    {
        $where = array(
            "company_id" => $company_id
        );
        $this->db->select('id');
        $this->db->from('customer');
        $this->db->where($where);
        $this->db->order_by("id", "desc");
        $this->db->limit(1);
        $query  = $this->db->get();
        $result = $query->row_array();
        $newid  = $result['id'] + 1;
        $new_id = "Cust_000" . $newid;
        return $new_id;
    }
    public function register($data)
    {
        $this->db->insert("customer", $data);
        return $this->login_details($data);
        //return TRUE;
    }
    public function login_details($data)
    {
        $insert = array(
            'user_type' => "Customer",
            'username' => $data['email_id'],
            'companyid' => $data['company_id']
        );
        $this->db->insert('login', $insert);
        return true;
    }
    public function check_login_30_july_2019($username)
    {
        $where = array(
            'username' => $username
        );
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->row_array();
        return $rowcount['password'];
    }
	
	  public function check_login($username)
    {
        $where = array(
			 'user_type' => "Customer",
            'username' => $username
        );
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->row_array();
        return $rowcount['password'];
    }
	
    public function get_userid($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('customer_id');
        $this->db->from('customer');
        $this->db->where($where);
		 $this->db->group_by('customer_id'); 
        $this->db->order_by('customer_id', 'desc'); 
        $query = $this->db->get();
        $res   = $query->result_array();
        return $res[0]['customer_id'];
    }
    public function get_mobile($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('contact_number');
        $this->db->from('customer');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->result_array();
        return $res[0]['contact_number'];
    }
    public function get_location($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('cust_town');
        $this->db->from('customer');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->result_array();
        return $res[0]['cust_town'];
    }
    public function get_username($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where($where);
		$this->db->group_by('email_id'); 
        $this->db->order_by('email_id', 'desc'); 
        $query = $this->db->get();
        $res   = $query->result_array();
        return $res;
    }
    public function store_id($device_token, $reg_id, $username)
    {
        if ($device_token != '') {
            $data  = array(
                'device_token' => $device_token,
                'reg_id' => ''
            );
            $where = array(
                'username' => $username
            );
            $this->db->where($where);
            $this->db->update('login', $data);
            return true;
        } else {
            $data  = array(
                'device_token' => '',
                'reg_id' => $reg_id
            );
            $where = array(
                'username' => $username
            );
            $this->db->where($where);
            $this->db->update('login', $data);
            return true;
        }
        
    } 
	public function add_product($cust_id,$company_id,$product_id,$cat_id,$model_no,$serial_no,$contract_type,$purchase_date,$retailer_name,$start_date,$end_date,$contract_duration,$warrenty_expairy_date)
    {
        $data=array("cust_id"=>$cust_id,
		"company_id"=>$company_id,
		"product_id"=>$product_id,
		"cat_id"=>$cat_id,
		"model_no"=>$model_no,
		"serial_no"=>$serial_no,
		"contract_type"=>$contract_type,
		"purchase_date"=>$purchase_date,
		"retailer_name"=>$retailer_name,
		"contract_duration" =>$contract_duration,
        "warrenty_expairy_date" =>  $warrenty_expairy_date,
        "start_date" => $start_date,
        "end_date"=> $end_date
				   );
		
            $this->db->insert('customer_contract', $data);
            return "Inserted Successfully";
        }
        
    
	public function get_company($username)
    {
		$this->db->select('company_id');
        $this->db->from('customer');
        $this->db->where('email_id', $username);
        $this->db->group_by('customer_id');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result[0]['company_id'];
    }
        
/////////// raju
	
  public function email_check_customer($email_id){
    $this->db->select('username');
    $this->db->from('login');
    $this->db->where('username',$email_id);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return 1;
    }else{
       return 0;
    }

  }	
//////////////Megala //// For creating existing customers into login table and mail function/////////////////
 public function get_customer()
    {
        //$this->db->distinct();
        $this->db->select('email_id,company_id,customer_name');
        $this->db->from('customer');
        $this->db->group_by("email_id");
       // $this->db->where('cust_type',0);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function create_login_customer($data)
    {
        
        $insert = array(
            'user_type' => "Customer",
            'username' => $data['email_id'],
            'companyid' => $data['company_id'],
            'password' => 'qjNjmS6/JcSMHj7x0DS424jxz/XV1qoHXLov/U/heoIwWVEngrbtOi4lF7Jz/XmGCYWqALX5S+B4DBRcWsdnLQ=='
        );
        $this->db->select('username');
        $this->db->from('login');
        $this->db->where('username',$data['email_id']);
        $query  = $this->db->get();
        $result = $query->row_array();
        $count = count($result);
        if($count == 0)
        {
            $this->db->insert('login', $insert);
         
        }
        return true;
    }
    public function send_customer_mail()
    {
       
     
        // $this->db->select('customer.email_id','customer.customer_name');
        // $this->db->from('customer');
        // $this->db->join('login', 'login.username = customer.email_id');
        // $this->db->where('login.email_sent',0);
        $procedure = "SELECT A.username, B.customer_name FROM login A JOIN customer B ON B.email_id =A.username WHERE A.email_sent =0 AND B.cust_type = 0 GROUP BY A.username";
        $query = $this->db->query($procedure);
        // $query  = $this->db->get();
       //  $result = $query->row_array();

         $result = $query->result_array();
        $count = count($result);
        if($count != 0)
        {
        foreach($result as $data){
            $this->sendmail($data['username'],$data['customer_name']);
        }
    }
        return true;
    }
public function sendmail($email_id,$custname)
  {	
      
      $this->load->helper('url');
       $this->load->library('encrypt');
      $this->load->database();
  
        $alphabet = "abcdefghijklmnpqrstuwxyzABCDEFGHIJKLMNPQRSTUWXYZ0123456789";
  $pass = array(); //remember to declare $pass as an array
  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
  for ($i = 0; $i < 6; $i++) {
      $n = rand(0, $alphaLength);
      $pass[] = $alphabet[$n];
  }

              $passwordplain = ""; 
              $passwordplain  = implode($pass);
             $newpass['password'] = $this->encrypt->encode($passwordplain);	
     /**       $app_url=base_url()."Capricot-APK/customer-releaseapk.apk";			  
$mail_message=" <!doctype html>
              <html>
              <head>
              <meta name='viewport' content='width=device-width' />
              <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
              <style>
              p,
              ul,
              ol {
              font-family: sans-serif;
              font-size: 14px;
              font-weight: normal;
              margin: 0;
              Margin-bottom: 15px; }
              p li,
              ul li,
              ol li {
              list-style-position: inside;
              margin-left: 5px; }
              a {
              color: #3498db;
              text-decoration: underline; }
              </style>
              <body>
              <p>Dear <b>$custname</b>,</p>
              <p>Welcome to Arkance.</b></p>
              <p>Thank you for registered with Arkance. Please find the access details.</p> 
              <p>Get the Arkance Customer App via -  <strong><a href='$app_url'>Arkance Customer App</a></strong></p>
              <p>Username : <strong>$email_id</strong></p>
              <p>Password : <strong>$passwordplain</strong></p>
              <p>Thanks & regards,</p> <p>Arkance Team.</p>

              </body>
              </html>";
   
             $config   = Array(
             'protocol' => 'smtp',
				  'smtp_host' => 'email-smtp.us-east-1.amazonaws.com',
				  'smtp_port' => '465',
				  'smtp_timeout' => '30',
				  'smtp_user' => 'AKIAQVP5N6THGN3WW5WY',
				  'smtp_pass' => 'BDr3VEpIbSqqFBuzQ291YtrMRfufJgXQAiZDLNsHMXtD',
				  'mailtype' => 'html',
				  'smtp_crypto' => 'tls',
				  'wordwrap' => TRUE,
				  'charset' => 'utf-8',
				  'crlf' => "\r\n",
				 'newline'=> "\r\n"
        );
		 $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('hwsupport@arkance.world', 'Arkance');
        // $this->email->to('senthilkumar.a@kaspontech.com');
        $this->email->to($mail_id);
        $this->email->subject('Arkance - Customer Registration Details');
        $this->email->message($mail_message);
        $this->email->set_newline("\r\n");
	 $result= true;//$this->email->send();
     if($result) 
     {	**/
      //  $data=array('email_sent'=>1 , 'password'=>$this->encrypt->encode($passwordplain),'plain_password'=>$passwordplain);	
$data=array('password'=>$this->encrypt->encode($passwordplain),'plain_password'=>$passwordplain);	
        $this->db->where('username', $email_id);
        $this->db->update('login', $data);  
       echo $email_id." Updated Successfully";
       echo "<br>";
         
        
    /**  }
     else
     {
        //echo $this->email->print_debugger();
          echo $email_id." Mail Not Sent Successfully";
          echo "<br>";
     }**/
     
  }	
    
}
