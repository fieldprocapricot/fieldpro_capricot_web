<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Capricotexcelimportexport extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		include APPPATH . 'third_party/excel_reader2_google.php';
	}	

///// Raju Start
	
		public function ticketexcel_fp_sap_old_work($array)
		{
			$this->load->model('common');	
			$this->load->library('excel');
			
			
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Ticket');
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'Customer ID');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Customer Name');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Email');

			

			$this->excel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('#333');
				
			//make the font become bold
			
		   for($col = ord('A'); $col <= ord('C'); $col++){
					//set column dimension
					$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
					 //change the font size
					//$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
			}
			
			/*********************************************************************/
			//Select Query Start
			$ticket = $this->common->ticket_excel($array);
		
			$exceldata=array();
			if($ticket->num_rows() > 0){
			$i=0;
 			foreach ($ticket->result() as $row){
				
				$exceldata[$i] = array($row->customer_id,$row->customer_name,$row->email_id);
			
                                $i++;
                             
			}
				
	
					$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');
                     
                $date = date("d-m-Y");
				$filename= 'assets/excel/ticket/capricot/ticket_'.$date.'.xlsx'; //save our workbook as this file name
				//header('Content-Type: application/vnd.ms-excel'); //mime type
				//header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
				//header('Cache-Control: max-age=0'); //no cache
				//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
				//if you want to save it as .XLSX Excel 2007 format
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
   			header('Content-Disposition: attachment;filename="'.$filename.'"');
  			header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
				//force user to download the Excel file without writing it to server's HD
				$objWriter->save($filename);                                

                        
			 }
			 else{
				 echo "No data";
				 //$this->excel->getActiveSheet()->setCellValue('A2', 'Sorry!! No data available');
				// $this->excel->getActiveSheet()->mergeCells('A2:L2');
				 //set aligment to center for that merged cell (A1 to C1)
				// $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			 }
				
		}	
	
	
      	public function ticketexcel_fp_sap($array)
		{
			$this->load->model('common');	
			$this->load->library('excel');
			
			
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Ticket');
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'Ticket ID');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Customer ID');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Customer Name');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Email');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Contact No');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Door No');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Street');
			$this->excel->getActiveSheet()->setCellValue('H1', 'Town');
			$this->excel->getActiveSheet()->setCellValue('I1', 'City');
			$this->excel->getActiveSheet()->setCellValue('J1', 'Pincode');
			$this->excel->getActiveSheet()->setCellValue('K1', 'Technician ID');
			$this->excel->getActiveSheet()->setCellValue('L1', 'Product ID');
			$this->excel->getActiveSheet()->setCellValue('M1', 'Sub Category');
			$this->excel->getActiveSheet()->setCellValue('N1', 'Model');
			$this->excel->getActiveSheet()->setCellValue('O1', 'Serial Number');
			$this->excel->getActiveSheet()->setCellValue('P1', 'Latitude');
			$this->excel->getActiveSheet()->setCellValue('Q1', 'Longitude');
			$this->excel->getActiveSheet()->setCellValue('R1', 'Problem description');
			$this->excel->getActiveSheet()->setCellValue('S1', 'Call Category');
			$this->excel->getActiveSheet()->setCellValue('T1', 'Escalated Description');
			$this->excel->getActiveSheet()->setCellValue('U1', 'Suggested Skill Level');
			$this->excel->getActiveSheet()->setCellValue('V1', 'Suggested Technician Name');
			$this->excel->getActiveSheet()->setCellValue('W1', 'Contract type');
			$this->excel->getActiveSheet()->setCellValue('X1', 'Current Status');
			$this->excel->getActiveSheet()->setCellValue('Y1', 'Reason');
			$this->excel->getActiveSheet()->setCellValue('Z1', 'service group');
			$this->excel->getActiveSheet()->setCellValue('AA1', 'Previous Service Group');
			$this->excel->getActiveSheet()->setCellValue('AB1', 'Schedule Next');
			$this->excel->getActiveSheet()->setCellValue('AC1', 'Spare Chargeable');
			$this->excel->getActiveSheet()->setCellValue('AD1', 'Lead time');
			$this->excel->getActiveSheet()->setCellValue('AE1', 'Spare Cost');
			$this->excel->getActiveSheet()->setCellValue('AF1', 'Service Charge');
			$this->excel->getActiveSheet()->setCellValue('AG1', 'Product GST');
			$this->excel->getActiveSheet()->setCellValue('AH1', 'Service GST');
			$this->excel->getActiveSheet()->setCellValue('AI1', 'Field return material');
			$this->excel->getActiveSheet()->setCellValue('AJ1', 'Drop Spare');
			$this->excel->getActiveSheet()->setCellValue('AK1', 'Drop of Date');
			$this->excel->getActiveSheet()->setCellValue('AL1', 'Drop Location');
			$this->excel->getActiveSheet()->setCellValue('AM1', 'Raised Time');
			$this->excel->getActiveSheet()->setCellValue('AN1', 'Assigned Time');
			$this->excel->getActiveSheet()->setCellValue('AO1', 'Acceptance Time');
			$this->excel->getActiveSheet()->setCellValue('AP1', 'Total Amount');
			$this->excel->getActiveSheet()->setCellValue('AQ1', 'Bill No');
			$this->excel->getActiveSheet()->setCellValue('AR1', 'Mode of Payment');
			$this->excel->getActiveSheet()->setCellValue('AS1', 'Cheque No');
			$this->excel->getActiveSheet()->setCellValue('AT1', 'Cheque Date');
	     	$this->excel->getActiveSheet()->setCellValue('AU1', 'Ticket Start Time');
			$this->excel->getActiveSheet()->setCellValue('AV1', 'Ticket End Time');
			$this->excel->getActiveSheet()->setCellValue('AW1', 'Closed Time');
			$this->excel->getActiveSheet()->setCellValue('AX1', 'Customer Feedback');
			$this->excel->getActiveSheet()->setCellValue('AY1', 'Customer Rating');
			$this->excel->getActiveSheet()->setCellValue('AZ1', 'Customer Reason');
			$this->excel->getActiveSheet()->setCellValue('BA1', 'Resolution Summary');
			$this->excel->getActiveSheet()->setCellValue('BB1', 'Case ID');
			$this->excel->getActiveSheet()->setCellValue('BC1', 'Service Report');
			

			$this->excel->getActiveSheet()->getStyle('A1:BC1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A1:BC1')->getFill()->getStartColor()->setARGB('#333');
				
			//make the font become bold
			
		   for($col = ord('A'); $col <= ord('BC1'); $col++){
					//set column dimension
					$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
					 //change the font size
					//$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
			}
			
			/*********************************************************************/
			//Select Query Start
			$ticket = $this->common->ticket_excel($array);
	
			$exceldata=array();
			if($ticket->num_rows() > 0){
			$i=0;
 			foreach ($ticket->result() as $row){
				
				  $c_status= $row->current_status;
				//  $c_status = str_replace('_', ' ', $c_status);
				  if($c_status==1)
				  {
					  $c_status='newly assigned';
				  }
				 	else if($c_status==2)
				  {
					   $c_status='accepted';
				  }
				    else if($c_status==3)
				  {
					   $c_status='reject';
				  }
				    else if($c_status==4)
				  {
					   $c_status='ongoing';
				  }
				    else if($c_status==5)
				  {
					   $c_status='travel start status';
				  }
				     else if($c_status==6)
				  {
					   $c_status='travel end status';
				  }
				     else if($c_status==7)
				  {
					   $c_status='ticket start';
				  }
				     else if($c_status==8)
				  {
					   $c_status='completed';
				  }
				     else if($c_status==9)
				  {
					   $c_status='escalated';
				  }
				     else if($c_status==10)
				  {
					   $c_status='work in progress';
				  }
				      else if($c_status==11)
				  {
					   $c_status='spare accepted';
				  }
				      else if($c_status==12)
				  {
					   $c_status='closed';
				  }
				      else if($c_status==13)
				  {
					   $c_status='reimbursement';
				  }
				      else if($c_status==14)
				  {
					   $c_status='spare request';
				  }
				      else if($c_status==15)
				  {
					   $c_status='escalate';
				  }
				      else if($c_status==16)
				  {
					   $c_status='renew contract';
				  }
				     else if($c_status==17)
				  {
					   $c_status='reimbursement request';
				  }
				     else if($c_status==18)
				  {
					   $c_status='spare delivered';
				  }
				     else if($c_status==19)
				  {
					   $c_status='contract rejected';
				  }
				     else if($c_status==20)
				  {
					   $c_status='contract accepted';
				  }
				     else if($c_status==50)
				  {
					   $c_status='Otp verified';
				  }
				  else
				  {
					  $c_status='not assign';
				  }

				
				
				 if($c_status!=10 || $c_status!=14)
				 {
				  $report_pdf=base_url().'index.php?/webservice_v2/web_mail_pdf/'.$row->ticket_id;
				 }
				else
				{
					$report_pdf=='';
				}

				
				if($row->schedule_next!='' && $row->schedule_next!='0000-00-00 00:00:00')
				{
				$schedule_next = strtotime($row->schedule_next);
	            $schedule_next = date('d-m-Y', $schedule_next);
				}
				else
				{
					$schedule_next =='';
				}
				if($row->lead_time!='')
				{
				$lead_time = strtotime($row->lead_time);
	            $lead_time = date('d-m-Y', $lead_time);
				}
					else
				{
					$lead_time =='';
				}
				
				$dropofdate = strtotime($row->drop_of_date);
	            $dropofdate = date('d-m-Y', $dropofdate);
				if($dropofdate!='' && $dropofdate!='01-01-1970')
				{
				$drop_of_date = strtotime($row->drop_of_date);
	            $drop_of_date = date('d-m-Y H:i:s', $drop_of_date);
				}
				else
				{
					$drop_of_date='';
				}
				
				$raised_time = strtotime($row->raised_time);
	            $raised_time = date('d-m-Y H:i:s', $raised_time);
				
				$assigned_time = strtotime($row->assigned_time );
	            $assigned_time = date('d-m-Y H:i:s', $assigned_time);
				
				if($row->acceptance_time!='' && $row->acceptance_time!='0000-00-00 00:00:00')
				{
				$acceptance_time = strtotime($row->acceptance_time);
	            $acceptance_time = date('d-m-Y H:i:s', $acceptance_time);
					}
					else
					{
						$acceptance_time='';
					}
				
				if($row->check_date!='')
				{	
				$check_date = strtotime($row->check_date);
	            $check_date = date('d-m-Y', $check_date);
				}
					else
					{
						$check_date ='';
					}
				
				$ticket_start_time = strtotime($row->ticket_start_time);
	            $ticket_start_time = date('d-m-Y H:i:s', $ticket_start_time);
				
				
				
				if($row->ticket_end_time!='' && $row->ticket_end_time!='0000-00-00 00:00:00')
				{
				$ticket_end_time = strtotime($row->ticket_end_time);
	            $ticket_end_time = date('d-m-Y H:i:s', $ticket_end_time);
				}
				else
				{
					$ticket_end_time='';
				}
				
				
				
				if($row->closed_time!='' && $row->closed_time!='0000-00-00 00:00:00' && $row->closed_time!=NULL)
				{
				$closed_time = strtotime($row->closed_time);
	            $closed_time = date('d-m-Y H:i:s', $closed_time);
				}
				else
				{
					$closed_time='';
				}
				
				if($row->cust_feedback=='Completely Satisfied')
				  {
					  $cust_feedback=1;
				  }
				else if($row->cust_feedback=='Satisfied')
				  {
					   $cust_feedback=2;
				  }
				else if($row->cust_feedback=='Dis-satisfied')
				  {
					   $cust_feedback=3;
				  }
				else if($row->cust_feedback=='Customer Not Rated')
				  {
					   $cust_feedback=5;
				  }
				else
				{
					 $cust_feedback=4;   ///// No feedback
				}
				
				if($row->cust_rating==1 || $row->cust_rating==2 || $row->cust_rating==3 || $row->cust_rating==4 || $row->cust_rating==5 )
				{
					$cust_rating=1;
				}
				else if($row->cust_rating==6 || $row->cust_rating==7 || $row->cust_rating==8)
				{
					$cust_rating=2;
				}
				else if($row->cust_rating==9 || $row->cust_rating==10)
				{
					$cust_rating=3;
				}
				else
				{
					$cust_rating=4;
				}
				
				
				$exceldata[$i] = array($row->ticket_id,$row->cust_id,$row->customer_name,$row->email_id,$row->contact_no,$row->door_no,$row->address,$row->town,$row->city,$row->pincode,$row->employee_id,$row->product_name,$row->cat_name,$row->model,$row->serial_no,$row->latitude,$row->longitude,$row->prob_desc,$row->call,$row->escalated_description,$row->suggested_skill_level,$row->sug_tech_name,$row->call_type,$c_status,$row->reason,$row->wt_name,$row->prev_wt_name,$schedule_next,$row->is_it_chargeable,$lead_time,$row->spare_cost,$row->service_charge,$row->product_gst,$row->service_gst,$row->frm,$row->drop_spare,$drop_of_date,$row->drop_of_location,$raised_time,$assigned_time,$acceptance_time,$row->total_amount,$row->bill_no,$row->mode_of_payment,$row->check_no,$check_date,$ticket_start_time,$ticket_end_time,$closed_time,$cust_feedback,$cust_rating,$row->cust_reason,$row->resolution_summary,$row->landmark,$report_pdf);
			
                                $i++;
                             
			}

				
	
					$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');
                     
           	if($array['export_status']==1)
			{
				$var='before3pm_';
			}
			else
			{
				$var='after3pm_';
			}

			
				$date = date("d-m-Y");
				$filename= 'assets/excel/ticket/capricot/ticket_'.$var.$date.'.xlsx'; //save our workbook as this file name
				
		
				//header('Content-Type: application/vnd.ms-excel'); //mime type
				//header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			   // header('Cache-Control: max-age=0'); //no cache
				//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
				//if you want to save it as .XLSX Excel 2007 format
			
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
   			header('Content-Disposition: attachment;filename="'.$filename.'"');
  			header('Cache-Control: max-age=0');
			
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
				//force user to download the Excel file without writing it to server's HD
				$objWriter->save($filename);                                   
                        
			 }
			 else{
				 echo "No data";
				
			 }
		
		}	
	          
    	public function knowledgebaseexcel_fp_sap($array)
		{
			$this->load->model('common');	
			$this->load->library('excel');
			
			
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Knowledge Base');
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'Product');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Sub Category');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Problem');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Solution');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Technician ID');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Region');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Area');
			$this->excel->getActiveSheet()->setCellValue('H1', 'Image');
			$this->excel->getActiveSheet()->setCellValue('I1', 'Video');
			$this->excel->getActiveSheet()->setCellValue('J1', 'Status');
			$this->excel->getActiveSheet()->setCellValue('K1', 'category Description');
		

			$this->excel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A1:K1')->getFill()->getStartColor()->setARGB('#333');
				
			//make the font become bold
			
		   for($col = ord('A'); $col <= ord('K1'); $col++){
					//set column dimension
					$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
					 //change the font size
					//$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
			}
			
			/*********************************************************************/
			//Select Query Start
			$kb = $this->common->knowledgebase_excel($array);
				
			$exceldata=array();
			if($kb->num_rows() > 0){
			$i=0;
 			foreach ($kb->result() as $row){
				
		$exceldata[$i] = array($row->product_name,$row->cat_name,$row->problem,$row->solution,$row->technician_id,$row->region,$row->area,$row->image,$row->video,$row->status,$row->cat_desc);
			
                                $i++;
                             
			}
				
	
					$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');
            
			  $date = date("d-m-Y");
				$filename= 'assets/excel/knowledgebase/capricot/knowledgebase_'.$date.'.xlsx'; //save our workbook as this file name
			//	header('Content-Type: application/vnd.ms-excel'); //mime type
			//	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			//	header('Cache-Control: max-age=0'); //no cache
				//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
				//if you want to save it as .XLSX Excel 2007 format
			
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
   			header('Content-Disposition: attachment;filename="'.$filename.'"');
  			header('Cache-Control: max-age=0');
			
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
				//force user to download the Excel file without writing it to server's HD
				$objWriter->save($filename);       	
              
			 }
			 else{
				 echo "No data";
				// $this->excel->getActiveSheet()->setCellValue('A2', 'Sorry!! No data available');
				// $this->excel->getActiveSheet()->mergeCells('A2:H1');
				 //set aligment to center for that merged cell (A1 to C1)
				// $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			 }
			  
				
		}	
	       
		public function spareexcel_fp_sap($array)
		{
			$this->load->model('common');	
			$this->load->library('excel');
			
			
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Spare');
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'Spare Code');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Spare Description');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Spare Name');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Spare Modal');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Spare Source');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Quantity Purchased');
			$this->excel->getActiveSheet()->setCellValue('H1', 'Quantity Used');
			$this->excel->getActiveSheet()->setCellValue('I1', 'Product');
			$this->excel->getActiveSheet()->setCellValue('J1', 'Category');
			$this->excel->getActiveSheet()->setCellValue('K1', 'Purchase Date');
			$this->excel->getActiveSheet()->setCellValue('L1', 'Expiry Date');
		

			$this->excel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A1:L1')->getFill()->getStartColor()->setARGB('#333');
			//	$this->excel->getActiveSheet()->getStyle('A1:L1')->setQuotePrefix(false);
			
			//make the font become bold
			
		   for($col = ord('A'); $col <= ord('L1'); $col++){
					//set column dimension
					$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
					 //change the font size
					//$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
			}
			
			/*********************************************************************/
			//Select Query Start
			$spare = $this->common->spare_excel($array);
	//	print_r($spare->result());
	//		die;
			$exceldata=array();
			if($spare->num_rows() > 0){
			$i=0;
 			foreach ($spare->result() as $row){
			/*	
			$date='08-04-2019';
				$test = strtotime($date);
	         echo  $test = date('Y-m-d', $test);
			 */
				$p_date = strtotime($row->p_date);
	            $p_date = date('d-m-Y', $p_date);
				$expiry_date = strtotime($row->expiry_date);
	            $expiry_date = date('d-m-Y', $expiry_date);
				$exceldata[$i] = array($row->spare_code,$row->spare_desc,$row->spare_name,$row->spare_modal,$row->spare_source,$row->price,$row->quantity_purchased,$row->quantity_used,$row->product_name,$row->cat_name,$p_date,$expiry_date);
				
			
       $i++;
  
			}
			
					
					$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');
				
					$date = date("d-m-Y");
				$filename= 'assets/excel/spare/capricot/spare_'.$date.'.xlsx'; //save our workbook as this file name
			//	header('Content-Type: application/vnd.ms-excel'); //mime type
			//	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			//	header('Cache-Control: max-age=0'); //no cache
				//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
				//if you want to save it as .XLSX Excel 2007 format
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
   			header('Content-Disposition: attachment;filename="'.$filename.'"');
  			header('Cache-Control: max-age=0');
			
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
				//force user to download the Excel file without writing it to server's HD
				$objWriter->save($filename); 
                        
			 }
			 else{
				 echo "No data";
				// $this->excel->getActiveSheet()->setCellValue('A2', 'Sorry!! No data available');
				// $this->excel->getActiveSheet()->mergeCells('A2:J1');
				 //set aligment to center for that merged cell (A1 to C1)
				// $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			 }
			
		}	
	
	
	public function customerexcel_fp_sap($array)
		{
			$this->load->model('common');	
			$this->load->library('excel');
			
			
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Customer');
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'Customer ID');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Customer Name');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Email');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Contact Number');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Door number');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Address');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Town');
			$this->excel->getActiveSheet()->setCellValue('H1', 'Region');
			$this->excel->getActiveSheet()->setCellValue('I1', 'City');
			$this->excel->getActiveSheet()->setCellValue('J1', 'State');
			$this->excel->getActiveSheet()->setCellValue('K1', 'Type of Contract');
			$this->excel->getActiveSheet()->setCellValue('L1', 'Product');
			$this->excel->getActiveSheet()->setCellValue('M1', 'Sub Category');
			$this->excel->getActiveSheet()->setCellValue('N1', 'Serial No');
			$this->excel->getActiveSheet()->setCellValue('O1', 'Model No');
			$this->excel->getActiveSheet()->setCellValue('P1', 'Start date');
			$this->excel->getActiveSheet()->setCellValue('Q1', 'Contract Duration');
			$this->excel->getActiveSheet()->setCellValue('R1', 'Warrenty expired Date');
			$this->excel->getActiveSheet()->setCellValue('S1', 'Invoice Date');
			$this->excel->getActiveSheet()->setCellValue('T1', 'Delivery Date');
		

			$this->excel->getActiveSheet()->getStyle('A1:T1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A1:T1')->getFill()->getStartColor()->setARGB('#333');
			//	$this->excel->getActiveSheet()->getStyle('A1:L1')->setQuotePrefix(false);
			
			//make the font become bold
			
		   for($col = ord('A'); $col <= ord('T1'); $col++){
					//set column dimension
					$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
					 //change the font size
					//$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
			}
			
			/*********************************************************************/
			//Select Query Start
			$spare = $this->common->customer_export_excel($array);
	//	print_r($spare->result());
	//		die;
			$exceldata=array();
			if($spare->num_rows() > 0){
			$i=0;
 			foreach ($spare->result() as $row){
					
				$exceldata[$i] = array($row->customer_id,$row->customer_name,$row->email_id,$row->contact_number,$row->door_num,$row->address,$row->cust_town,$row->region,$row->city,$row->state,$row->type_of_contract,$row->product_name,$row->cat_name,$row->serial_no,$row->model_no,$row->start_date,$row->contract_duration,$row->warrenty_expairy_date,$row->invoice_date,$row->delivery_date);
				
			
       $i++;
  
			}

					$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');
					
					$date = date("d-m-Y");
				//$filename= 'assets/excel/customer/customer_export_'.$date.'.xlsx'; //save our workbook as this file name
				$filename= 'customer_export_'.$date.'.xlsx'; //save our workbook as this file name
				
			//	header('Content-Type: application/vnd.ms-excel'); //mime type
			//	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			//	header('Cache-Control: max-age=0'); //no cache
				//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
				//if you want to save it as .XLSX Excel 2007 format
			header('Content-Type: application/vnd.ms-excel');
   			header('Content-Disposition: attachment;filename="'.$filename.'"');
  			header('Cache-Control: max-age=0');
			header("Pragma: no-cache"); 
			header("Expires: 0");
			
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
				//force user to download the Excel file without writing it to server's HD
				$objWriter->save('php://output'); 
			
                        
			 }
			 else{
				 echo "No data";
				// $this->excel->getActiveSheet()->setCellValue('A2', 'Sorry!! No data available');
				// $this->excel->getActiveSheet()->mergeCells('A2:J1');
				 //set aligment to center for that merged cell (A1 to C1)
				// $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			 }
			
		}	
	

	public function prodsubexcel_fp_sap($array)
		{
			$this->load->model('common');	
			$this->load->library('excel');
			
			
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Product');
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'Product ID');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Name');
		    $this->excel->getActiveSheet()->setCellValue('C1', 'Product Modal');
		    $this->excel->getActiveSheet()->setCellValue('D1', 'Product Description');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Sub Product ID');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Sub Product Name');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Sub-Product Description');
		    $this->excel->getActiveSheet()->setCellValue('H1', 'Sub-Product Modal');
		
		

			$this->excel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A1:H1')->getFill()->getStartColor()->setARGB('#333');
			//	$this->excel->getActiveSheet()->getStyle('A1:L1')->setQuotePrefix(false);
			
			//make the font become bold
			
		   for($col = ord('A1'); $col <= ord('H1'); $col++){
					//set column dimension
					$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
					 //change the font size
					//$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
			}
			
			/*********************************************************************/
			//Select Query Start
			$spare = $this->common->product_subproduct_excel($array);
	//	print_r($spare->result());
	//		die;
			$exceldata=array();
			if($spare->num_rows() > 0){
			$i=0;
 			foreach ($spare->result() as $row){
			/*	
			$date='08-04-2019';
				$test = strtotime($date);
	         echo  $test = date('Y-m-d', $test);
			 */
				$p_date = strtotime($row->p_date);
	            $p_date = date('d-m-Y', $p_date);
				$expiry_date = strtotime($row->expiry_date);
	            $expiry_date = date('d-m-Y', $expiry_date);
				$exceldata[$i] = array($row->product_id,$row->product_name,$row->product_modal,$row->product_desc,$row->cat_id,$row->cat_name,$row->cat_desc,$row->cat_modal);
				
			
       $i++;
  
			}
			
					
					$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');
				
					$date = date("d-m-Y");
				$filename= 'assets/excel/customer/product_'.$date.'.xlsx'; //save our workbook as this file name
			//	header('Content-Type: application/vnd.ms-excel'); //mime type
			//	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			//	header('Cache-Control: max-age=0'); //no cache
				//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
				//if you want to save it as .XLSX Excel 2007 format
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
   			header('Content-Disposition: attachment;filename="'.$filename.'"');
  			header('Cache-Control: max-age=0');
			
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
				//force user to download the Excel file without writing it to server's HD
				$objWriter->save($filename); 
                        
			 }
			 else{
				 echo "No data";
				// $this->excel->getActiveSheet()->setCellValue('A2', 'Sorry!! No data available');
				// $this->excel->getActiveSheet()->mergeCells('A2:J1');
				 //set aligment to center for that merged cell (A1 to C1)
				// $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			 }
			
		}	
		
	
	
public function integeration_knowledge($data)
    {
        $this->db->insert('knowledge_base', $data);
        return true;
    }
	
public function check_spare($sparecode){
        $this->db->select('spare_code');
        $this->db->from('spare');
        $this->db->where('spare_code',$sparecode);
        $datas=$this->db->get();
        if($datas->num_rows()>0){
          $datas=$datas->row_array();
          return $datas;
        }else{
          return 0;
        }
     }	
public function integeration_insert_spare($data)
    {
    $this->db->insert('spare', $data);
    return true;
    }	
	
public function integeration_edit_spare($data,$spare_code)
    {
    $this->db->where('spare_code',$spare_code);
    $this->db->update('spare', $data);
      return true;
    }	

 public function integeration_insertcustomer($data)
    {
    $this->db->insert('customer', $data);
    return true;
	}	
	
public function integeration_insertlogin($data)  // Aarthi
    {
    $this->db->insert('login', $data);
    return true;
	}
 public function get_company_name($c_id)  //Aarthi
 {
	 $where = array(
		 'company_id' =>$c_id,
	 );
	 $this->db->select('company_name');
	 $this->db->from('company');
	 $this->db->where($where);
	 $query = $this->db->get();
	 $res = $query->row_array();	   
    return $res['company_name'];
	// return $query;

 }

//  public function update_login($email_id,$data){  //Aarthi

// 	$where=array(
// 		'username' =>$email_id,
// );
//      $this->db->where($where);
// 	$this->db->update('login', $data);
// 	$str = $this->db->last_query();
// 	print_r($str);
// 	die;
//     return true;

//  }



	

 public function integeration_editcustomer($data,$customer_id,$product,$category)
    {
	  $where=array('customer_id'=>$customer_id,'product_serial_no'=>$product,'component_serial_no'=>$category);
 	 
     $this->db->where($where);
    $this->db->update('customer', $data);
    return true;
    }	  
	
public function get_customer_login($array)
    {
	$where=array('email_sent'=>0,'user_type'=>'Customer');
		$this->db->select('username');
        $this->db->from('login');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }	

public function get_customer_name($emailid)
    {
	$where=array('email_id'=>$emailid);
		$this->db->select('customer_name');
        $this->db->from('customer');
        $this->db->where($where);
        $query  = $this->db->get();
        $res = $query->row_array();
        return $res;
    }	

	
	
public function update_customer_email($data,$emailid)
    {
	  $where=array('username'=>$emailid,'user_type'=>'Customer','email_sent'=>0);
 	 
     $this->db->where($where);
    $this->db->update('login', $data);
    return true;
    }	
	
}
?>