<?php
class Model_service extends CI_Model
{
    public function ticket_insight($company_id, $filter, $region, $area, $location)
    {
        $res = array();
        //  if ($filter == '' || $filter == 'today') {
        switch ($filter) {
            case "":
            case "today":
                $where = array(
                    'company_id' => $company_id
                );
                $this->db->select('*');
                $this->db->from('product_management');
                $this->db->where($where);
                $query1     = $this->db->get();
                $query      = $query1->result_array();
                $sub_where5 = array(
                    'company_id' => $company_id
                );
                $this->db->select('amc_type');
                $this->db->from('amc_type');
                $this->db->where($sub_where5);
                $amc_query1 = $this->db->get();
                $amc_query  = $amc_query1->result_array();
                foreach ($query as $row) {
                    $product_id = $row['product_id'];
                    
                    $res1 = array();
                    $res2 = array();
                    $res3 = array();
                    foreach ($amc_query as $amc) {
                        $i         = 0;
                        $now       = date('Y-m-d H:i:s');
                        $start     = date('Y-m-d 00:00:00');
                        $sub_where = array(
                            'all_tickets.company_id' => $company_id,
                            'product_id' => $product_id,
                            'call_type' => $amc['amc_type'],
                            'region' => $region,
                            'town' => $area,
                            'raised_time<=' => $now,
                            'raised_time>=' => $start
                        );
                        $this->db->select('call_type,count(*) as count1');
                        $this->db->from('all_tickets');
                        $this->db->where($sub_where);
                        $sub_query1 = $this->db->get();
                        $sub_query  = $sub_query1->result_array();
                        $count      = 0;
                        foreach ($sub_query as $sq) {
                            if ($i == 0) {
                                $count = $sq['count1'];
                                array_push($res2, array(
                                    'product' => $row['product_name'],
                                    $amc['amc_type'] => $count
                                ));
                            } else {
                                $res1 = array(
                                    $amc['amc_type'] => $count
                                );
                            }
                        }
                        
                        $sub_where6 = array(
                            'all_tickets.company_id' => $company_id,
                            'product_id' => $product_id,
                            'raised_time<=' => $now,
                            'raised_time>=' => $start,
                            'region' => $region,
                            'town' => $area
                        );
                        $this->db->select('*');
                        $this->db->from('all_tickets');
                        $this->db->where($sub_where6);
                        $sub_query6 = $this->db->get();
                        $sub_query7 = $sub_query6->num_rows();
                        $total      = array(
                            'Total' => $sub_query7
                        );
                        $res3       = $res2 + $res1 + $total;
                        if (!is_array($res3)) {
                            return FALSE;
                        }
                        
                        $result = array();
                        foreach ($res3 as $key => $value) {
                            if (is_array($value)) {
                                $result = array_merge($result, ($value));
                            } else {
                                $result[$key] = $value;
                            }
                        }
                        
                        $i++;
                    }
                    
                    array_push($res, $result);
                }
                
                $result5 = array();
                $result5 = array_column($amc_query, 'amc_type');
                array_push($res, $result5);
                break;
            // } else if ($filter == 'week') {
            case "week":
                $where = array(
                    'company_id' => $company_id
                );
                $this->db->select('*');
                $this->db->from('product_management');
                $this->db->where($where);
                $query1     = $this->db->get();
                $query      = $query1->result_array();
                $sub_where5 = array(
                    'company_id' => $company_id
                );
                $this->db->select('amc_type');
                $this->db->from('amc_type');
                $this->db->where($sub_where5);
                $amc_query1 = $this->db->get();
                $amc_query  = $amc_query1->result_array();
                foreach ($query as $row) {
                    $product_id = $row['product_id'];
                    
                    $res1 = array();
                    $res2 = array();
                    $res3 = array();
                    foreach ($amc_query as $amc) {
                        $i   = 0;
                        $now = date('Y-m-d H:i:s');
                        
                        $first_day_of_the_week = 'Monday';
                        $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
                        if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                            $week = date('Y-m-d', strtotime('today'));
                        }
                        $sub_where = array(
                            'all_tickets.company_id' => $company_id,
                            'product_id' => $product_id,
                            'call_type' => $amc['amc_type'],
                            'raised_time<=' => $now,
                            'raised_time>=' => $week,
                            'region' => $region,
                            'town' => $area
                        );
                        $this->db->select('call_type,count(*) as count1');
                        $this->db->from('all_tickets');
                        $this->db->where($sub_where);
                        $sub_query1 = $this->db->get();
                        $sub_query  = $sub_query1->result_array();
                        $count      = 0;
                        foreach ($sub_query as $sq) {
                            if ($i == 0) {
                                $count = $sq['count1'];
                                array_push($res2, array(
                                    'product' => $row['product_name'],
                                    $amc['amc_type'] => $count
                                ));
                            } else {
                                $res1 = array(
                                    $amc['amc_type'] => $count
                                );
                            }
                        }
                        
                        $sub_where6 = array(
                            'all_tickets.company_id' => $company_id,
                            'product_id' => $product_id,
                            'raised_time<=' => $now,
                            'raised_time>=' => $week,
                            'region' => $region,
                            'town' => $area
                        );
                        $this->db->select('*');
                        $this->db->from('all_tickets');
                        $this->db->where($sub_where6);
                        $sub_query6 = $this->db->get();
                        $sub_query7 = $sub_query6->num_rows();
                        $total      = array(
                            'Total' => $sub_query7
                        );
                        $res3       = $res2 + $res1 + $total;
                        if (!is_array($res3)) {
                            return FALSE;
                        }
                        
                        $result = array();
                        foreach ($res3 as $key => $value) {
                            if (is_array($value)) {
                                $result = array_merge($result, ($value));
                            } else {
                                $result[$key] = $value;
                            }
                        }
                        
                        $i++;
                    }
                    
                    array_push($res, $result);
                }
                
                $result5 = array();
                $result5 = array_column($amc_query, 'amc_type');
                array_push($res, $result5);
                
                break;
            case "month":
                //	} else if ($filter == 'month') {
                $where = array(
                    'company_id' => $company_id
                );
                $this->db->select('*');
                $this->db->from('product_management');
                $this->db->where($where);
                $query1     = $this->db->get();
                $query      = $query1->result_array();
                $sub_where5 = array(
                    'company_id' => $company_id
                );
                $this->db->select('amc_type');
                $this->db->from('amc_type');
                $this->db->where($sub_where5);
                $amc_query1 = $this->db->get();
                $amc_query  = $amc_query1->result_array();
                foreach ($query as $row) {
                    $product_id = $row['product_id'];
                    
                    $res1 = array();
                    $res2 = array();
                    $res3 = array();
                    foreach ($amc_query as $amc) {
                        $i          = 0;
                        $start_date = date("Y-m-01 00:00:00");
                        $d          = new DateTime($start_date);
                        $end_date   = $d->format('Y-m-t 23:00:00');
                        $sub_where  = array(
                            'all_tickets.company_id' => $company_id,
                            'product_id' => $product_id,
                            'call_type' => $amc['amc_type'],
                            'raised_time>=' => $start_date,
                            'raised_time<=' => $end_date,
                            'region' => $region,
                            'town' => $area
                        );
                        $this->db->select('call_type,count(*) as count1');
                        $this->db->from('all_tickets');
                        $this->db->where($sub_where);
                        $sub_query1 = $this->db->get();
                        $sub_query  = $sub_query1->result_array();
                        $count      = 0;
                        foreach ($sub_query as $sq) {
                            if ($i == 0) {
                                $count = $sq['count1'];
                                array_push($res2, array(
                                    'product' => $row['product_name'],
                                    $amc['amc_type'] => $count
                                ));
                            } else {
                                $res1 = array(
                                    $amc['amc_type'] => $count
                                );
                            }
                        }
                        
                        $sub_where6 = array(
                            'all_tickets.company_id' => $company_id,
                            'product_id' => $product_id,
                            'raised_time>=' => $start_date,
                            'raised_time<=' => $end_date,
                            'region' => $region,
                            'town' => $area
                        );
                        $this->db->select('*');
                        $this->db->from('all_tickets');
                        $this->db->where($sub_where6);
                        $sub_query6 = $this->db->get();
                        $sub_query7 = $sub_query6->num_rows();
                        $total      = array(
                            'Total' => $sub_query7
                        );
                        $res3       = $res2 + $res1 + $total;
                        if (!is_array($res3)) {
                            return FALSE;
                        }
                        
                        $result = array();
                        foreach ($res3 as $key => $value) {
                            if (is_array($value)) {
                                $result = array_merge($result, ($value));
                            } else {
                                $result[$key] = $value;
                            }
                        }
                        
                        $i++;
                    }
                    
                    array_push($res, $result);
                }
                
                $result5 = array();
                $result5 = array_column($amc_query, 'amc_type');
                array_push($res, $result5);
                break;
            case "year":
                
                //	   } else if ($filter == 'year') {
                $where = array(
                    'company_id' => $company_id
                );
                $this->db->select('*');
                $this->db->from('product_management');
                $this->db->where($where);
                $query1     = $this->db->get();
                $query      = $query1->result_array();
                $sub_where5 = array(
                    'company_id' => $company_id
                );
                $this->db->select('amc_type');
                $this->db->from('amc_type');
                $this->db->where($sub_where5);
                $amc_query1 = $this->db->get();
                $amc_query  = $amc_query1->result_array();
                foreach ($query as $row) {
                    $product_id    = $row['product_id'];
                    $now           = date('Y-m-d H:i:s');
                    $current_month = date('M'); //$current_month='Apr';
                    if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                        $start_of_yr = date('Y-04-01 00:00:00', strtotime("-1 year")); //April 1st calculation
                    } else {
                        $start_of_yr = date('Y-04-01 00:00:00', strtotime("today")); //April 1st calculation
                    }
                    
                    
                    $res1 = array();
                    $res2 = array();
                    $res3 = array();
                    foreach ($amc_query as $amc) {
                        $i         = 0;
                        $sub_where = array(
                            'all_tickets.company_id' => $company_id,
                            'product_id' => $product_id,
                            'call_type' => $amc['amc_type'],
                            'raised_time<=' => $now,
                            'raised_time>=' => $start_of_yr,
                            'region' => $region,
                            'town' => $area
                        );
                        $this->db->select('call_type,count(*) as count1');
                        $this->db->from('all_tickets');
                        $this->db->where($sub_where);
                        $sub_query1 = $this->db->get();
                        $sub_query  = $sub_query1->result_array();
                        $count      = 0;
                        foreach ($sub_query as $sq) {
                            if ($i == 0) {
                                $count = $sq['count1'];
                                array_push($res2, array(
                                    'product' => $row['product_name'],
                                    $amc['amc_type'] => $count
                                ));
                            } else {
                                $res1 = array(
                                    $amc['amc_type'] => $count
                                );
                            }
                        }
                        
                        $sub_where6 = array(
                            'all_tickets.company_id' => $company_id,
                            'product_id' => $product_id,
                            'raised_time<=' => $now,
                            'raised_time>=' => $start_of_yr,
                            'region' => $region,
                            'town' => $area
                        );
                        $this->db->select('*');
                        $this->db->from('all_tickets');
                        $this->db->where($sub_where6);
                        $sub_query6 = $this->db->get();
                        $sub_query7 = $sub_query6->num_rows();
                        $total      = array(
                            'Total' => $sub_query7
                        );
                        $res3       = $res2 + $res1 + $total;
                        if (!is_array($res3)) {
                            return FALSE;
                        }
                        
                        $result = array();
                        foreach ($res3 as $key => $value) {
                            if (is_array($value)) {
                                $result = array_merge($result, ($value));
                            } else {
                                $result[$key] = $value;
                            }
                        }
                        
                        $i++;
                    }
                    
                    array_push($res, $result);
                }
                
                $result5 = array();
                $result5 = array_column($amc_query, 'amc_type');
                array_push($res, $result5);
                //}
                break;
        }
        
        echo json_encode($res);
    }
}