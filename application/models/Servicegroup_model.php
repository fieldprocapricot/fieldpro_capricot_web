<?php 
   class Servicegroup_model extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }
      public function get_servicegroup($company_id)
    {
        $where = array(
            'company_id' => $company_id,
        );
        $this->db->select('*');
        $this->db->from('service_group');
        $this->db->where($where);
     //   $this->db->where('service_group !=', 'All');
        $query = $this->db->get();
        $data  = $query->result_array();
        return $data;
    }
    public function insertservicegroup($company_id,$servicegroupid,$servicegroupname)
    {
    	$response=array();
        $now = date('Y-m-d H:i:s');
        /*$array = array('company_id' => $company_id);
        $this->db->select('*');
        $this->db->from('service_group');
        $this->db->where($array);
        $query1 = $this->db->get();
        $data  = $query1->result_array();
        if(count($data)==0)
        {
            $insert = array(
                'service_group' =>"All",
                'company_id' => $company_id,
                'create_date' => $now,
                'update_date'=>$now
            );
            $queryexec=$this->db->insert('service_group', $insert);
        }*/

    	$array = array('company_id' => $company_id, 'service_group_id !=' => $servicegroupid, 'service_group' => $servicegroupname);
    	 $this->db->select('*');
        $this->db->from('service_group');
    	$this->db->where($array);
        $query1 = $this->db->get();
        $data  = $query1->result_array();
        if(count($data)==0)
        {
    	if($servicegroupid==0)
    	{

    		 $insert = array(
            'service_group' =>$servicegroupname,
            'company_id' => $company_id,
            'create_date' => $now,
            'update_date'=>$now
        );
        $queryexec=$this->db->insert('service_group', $insert);
        if($queryexec)
        {
        	$response=array('status'=>'success','message'=>'Service group added successfully');
        }
        else
        {
        	$response=array('status'=>'error','message'=>'Unable to add Service group');
        }
    	}
    	else
    	{
    		 $data  = array(
                'service_group' => $servicegroupname,
                'update_date' => $now
            );
            $where = array(
                'service_group_id' => $servicegroupid,
                'company_id' => $company_id
            );
            $this->db->where($where);
            $queryexec=$this->db->update('service_group', $data);
             if($queryexec)
        {
        	$response=array('status'=>'success','message'=>'Service group updated successfully');
        }
        else
        {
        	$response=array('status'=>'error','message'=>'Unable to update Service group');
        }
    	}
    }
    else
{
	$response=array('status'=>'error','message'=>'Service group with same name already exists');
}

    	return $response;
	 
   }
   public function deleteservicegroup($company_id,$servicegroupid)
   {
   	$response=array();
   	$where = array(
                'service_group_id' => $servicegroupid,
                'company_id' => $company_id
            );
       $this->db->where($where);
      
$queryexec=$this->db->delete('service_group');
 if($queryexec)
        {
        	$response=array('status'=>'success','message'=>'Service group deleted successfully');
        }
        else
        {
        	$response=array('status'=>'error','message'=>'Unable to delete Service group');
        }
        return $response;
   }
   public function editservicegroup($company_id,$servicegroupid)
   {
   		$where = array(
                'service_group_id' => $servicegroupid,
                'company_id' => $company_id
            );
   		/*$this->db->select('*');
        $this->db->from('service_group');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();*/

   		$this->db->select('service_group');
       $this->db->where($where);
     
   	$this->db->from('service_group');
   $query = $this->db->get();
        $data  = $query->result_array();
        return $data;
   
   }
    }
?>