 <?php 
   class Techlogin1 extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }


  public function check_login($username)  
      {
$where=array('username'=>$username,'user_type'=>'Technician');			
$this->db->select('*');    
			$this->db->from('login');
			$this->db->where($where);    
			$query = $this->db->get();	
                  $rowcount= $query->row_array();	   
         return $rowcount['password'];  
      }


public function get_userid($username)
      {
$where=array('email_id'=>$username);			
$this->db->select('technician_id');    
			$this->db->from('technician');
			$this->db->where($where);    
			$query = $this->db->get();	
$res = $query->row_array();	   
         return $res['technician_id'];  
      }

    public function get_mobile($username)
      {
       $where=array('email_id'=>$username);			
       $this->db->select('contact_number');    
	 $this->db->from('technician');
	 $this->db->where($where);    
	 $query = $this->db->get();	
       $res = $query->row_array();	   
       return $res['contact_number'];  
      }

      public function get_email_id($username)
      {
       $where=array('email_id'=>$username);			
       $this->db->select('email_id');    
	 $this->db->from('technician');
	 $this->db->where($where);    
	 $query = $this->db->get();	
       $res = $query->row_array();	   
       return $res['email_id'];  
      }


public function get_location($username)
      {
$where=array('email_id'=>$username);			
$this->db->select('location');    
			$this->db->from('technician');
			$this->db->where($where);    
			$query = $this->db->get();	
$res = $query->row_array();	   
         return $res['location'];  
      }

public function get_companyname($username)
      {
$where=array('email_id'=>$username);			
$this->db->select('companyname');    
			$this->db->from('technician');
			$this->db->where($where);    
			$query = $this->db->get();	
$res = $query->row_array();	   
         return $res['companyname'];  
      }

      public function get_companyid($username)
      {
$where=array('email_id'=>$username);			
$this->db->select('company_id');    
			$this->db->from('technician');
			$this->db->where($where);    
			$query = $this->db->get();	
$res = $query->row_array();	   
         return $res['company_id'];  
      }   
      

public function get_username($username)
      {
$where=array('email_id'=>$username);			
$this->db->select('first_name');    
			$this->db->from('technician');
			$this->db->where($where);    
			$query = $this->db->get();	
$res = $query->row_array();	   
         return $res['first_name'];  
      }

public function get_tech_avail($username)
      {
$where=array('email_id'=>$username);			
$this->db->select('availability');    
			$this->db->from('technician');
			$this->db->where($where);    
			$query = $this->db->get();	
$res = $query->row_array();	   
         return $res['availability'];  
      }	   
	   
public function get_rating($userid)
      {
$where=array('tech_id'=>$userid);			
$this->db->select('tech_rate');    
			$this->db->from('reward');
			$this->db->where($where);    
			$query = $this->db->get();	
$res = $query->row_array();	
if(!empty($res))
{   
         return $res['tech_rate'];  
}
else
{
return 0;
}
      }
public function get_image($username)
      {
$where=array('email_id'=>$username);			
$this->db->select('image');    
			$this->db->from('technician');
			$this->db->where($where);    
			$query = $this->db->get();	
$res = $query->row_array();	   
         return $res['image'];  
      }

public function store_id($device_token,$reg_id,$username)
      {
if($device_token!='')
{
$data=array('device_token'=>$device_token,'reg_id'=>'');
$where=array('username'=>$username,'user_type'=>'Technician');
$this->db->where($where);			
$this->db->update('login',$data);
return true;
}
else
{
$data=array('device_token'=>'','reg_id'=>$reg_id);			
$where=array('username'=>$username,'user_type'=>'Technician');
$this->db->where($where);			
$this->db->update('login',$data);
return true;
}   
        
      }
}
?> 