 <?php 
 //date_default_timezone_set('Asia/Calcutta');
   class AutoAssign extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }
public function get_company()
  {
$this->db->select('*');
$this->db->from('admin_user');
$query1 = $this->db->get();
$query=$query1->result_array();
return $query;
} public function get_tech_cat($tech_id)
    {
        $where = array(
            'technician_id' => $tech_id
        );
        $this->db->select('*,technician.company_id as c_id,street');
        $this->db->from('technician');
        $this->db->join('category_details', 'category_details.cat_id=technician.category');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        if (!empty($result)) {
            $cat_name   = $result[0]['cat_name'];
            $cat_id     = $result[0]['cat_id'];
            $company_id = $result[0]['c_id'];
            $json       = array();
            array_push($json, array(
                "cat_name" => $cat_name,
                "cat_id" => $cat_id,
                "company_id" => $company_id,
                "address" => $result[0]['current_location']
            ));
            return $json;
        }
    }
	    public function get_tech_product($tech_id)
    {
        $where = array(
            'technician_id' => $tech_id
        );
        $this->db->select('*,technician.company_id as c_id');
        $this->db->from('technician');
        $this->db->join('product_management', 'product_management.product_id=technician.product');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        if (!empty($result)) {
            $product_name = $result[0]['product_name'];
            $product_id   = $result[0]['product_id'];
            $company_id   = $result[0]['c_id'];
            $json         = array();
            array_push($json, array(
                "product_name" => $product_name,
                "product_id" => $product_id,
                "company_id" => $company_id
            ));
            return $json;
        }
    }
 public function get_related_tkts($company_id,$product_id,$cat_id)
  {
$today=date("Y-m-d");
	  $data=array();
	  $where=array('company_id'=>$company_id,'product_id'=>$product_id,'cat_id'=>$cat_id);
	  $this->db->select('DATE_FORMAT(cust_preference_date,"%H:%i:%s") AS time,ticket_id,id,product_id,cat_id,call_tag,call_type,cust_category,address,location');    
			$this->db->from('all_tickets');
			$this->db->like('cust_preference_date', $today);
			$this->db->order_by('cust_preference_date');
			$this->db->where($where);
$this->db->group_start();
			$this->db->where("(current_status=0 OR current_status=3)");
$this->db->group_end();
			$query1 = $this->db->get();
			$query=$query1->result_array();
            
			return $query;
}
  public function get_tkts($company_id)
  {
	  $today=date("Y-m-d");
	  $data=array();
	  $where=array('company_id'=>$company_id);
	  $this->db->select('DATE_FORMAT(cust_preference_date,"%H:%i:%s") AS time,ticket_id,id,product_id,cat_id,call_tag,call_type,cust_category,address,location');    
			$this->db->from('all_tickets');
			$this->db->like('cust_preference_date', $today);
			$this->db->order_by('cust_preference_date');
			$this->db->where($where);
$this->db->group_start();
			$this->db->where("(current_status=0 OR current_status=3)");
$this->db->group_end();
			$query1 = $this->db->get();
			$query=$query1->result_array();
			return $query;
  }
  public function check_tech()
  {
	  $today=date("Y-m-d");
	  $data=array();
	  $this->db->select('DATE_FORMAT(cust_preference_date,"%H:%i:%s") AS time,ticket_id,id,product_id,cat_id,location');    
			$this->db->from('all_tickets');
			$this->db->like('cust_preference_date', $today);
			$this->db->where('current_status',0);
			$query1 = $this->db->get();	
		$query=$query1->result_array();
		return $query;
  }
	public function get_nearbylocation($latitude,$longitude,$product,$category)
	{
		$where=array('today_task_count<'=>1,'product'=>$product,'category'=>$category,'availability'=>1);			
		$this->db->select('technician_id, today_task_count,product,category,( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
		$this->db->from('technician');
		$this->db->having('distance < 2');
		$this->db->order_by('today_task_count');
		$this->db->where($where);
		$this->db->limit(1);		
		$query1 = $this->db->get();	
		$query=$query1->result_array();
		if(!empty($query))
		{
			return $query;
		}
		else
		{
			$where=array('today_task_count<'=>1,'product'=>$product,'availability'=>1,'category'=>$category);			
			$this->db->select('technician_id,today_task_count,product,category, ( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
			$this->db->from('technician');
			$this->db->having('distance < 4');
			$this->db->where($where); 
			$this->db->order_by('today_task_count');
			$this->db->limit(1);
			$query11 = $this->db->get();	
			$query2=$query11->result_array();
			if(!empty($query2))
			{
				return $query2;
			}
			else
			{
				$where=array('today_task_count<'=>1,'product'=>$product,'availability'=>1,'category'=>$category);			
				$this->db->select('technician_id,today_task_count ,product,category,( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
				$this->db->from('technician');
				$this->db->having('distance < 8');
				$this->db->order_by('today_task_count');
				$this->db->where($where); 
				$this->db->limit(1);
				$res = $this->db->get();	
				$query3=$res->result_array();
				if(!empty($query3))
				{
					return $query3;
				}
				else
			{
				$where=array('today_task_count<'=>1,'product'=>$product,'category'=>$category,'availability'=>1);			
				$this->db->select('technician_id,today_task_count,product,category, ( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
				$this->db->from('technician');
				$this->db->having('distance > 8');
				$this->db->order_by('today_task_count');
				$this->db->where($where); 
				$this->db->limit(1);
				//$this->db->orderby('distance');
				$res1 = $this->db->get();	
				$query4=$res1->result_array();
				if(!empty($query4))
				{
					return $query4;
				}
			}
			}
		}
	}
public function escalate_assign($ticket,$tech,$resolution,$response,$priority,$acceptance_time)
	{
		 $this->db->where('ticket_id',$ticket); 
		$timezone = date_default_timezone_get();
 $date = date('Y-m-d H:i:s');
		// $date=Date('Y-m-d H-i-sa');
     $dbdata = array(
          "tech_id" =>$tech,
          "current_status" =>1,
          "previous_status" =>9,
		  "assigned_time" =>$date,
		  "resolution_time"=>$resolution,
		  "response_time"=>$response,
		  "priority"=>$priority,"target_aceptance_time"=>$acceptance_time
     ); 
     $this->db->update('all_tickets', $dbdata);
	 return true;
	}
	public function assign($ticket,$tech,$resolution,$response,$priority,$acceptance_time)
	{
		 $this->db->where('ticket_id',$ticket); 
		$timezone = date_default_timezone_get();
 $date = date('Y-m-d H:i:s');
		// $date=Date('Y-m-d H-i-sa');
     $dbdata = array(
          "tech_id" =>$tech,
          "current_status" =>1,
          "previous_status" =>0,
		  "assigned_time" =>$date,
		  "resolution_time"=>$resolution,
		  "response_time"=>$response,
		  "priority"=>$priority,"target_aceptance_time"=>$acceptance_time
     ); 
     $this->db->update('all_tickets', $dbdata);
	 return true;
	}
	
	public function update_tech_task($tech,$today_task_count,$task_count)
	{
		$count=$today_task_count+1;
		$tcount=$task_count+1;
		$this->db->where('technician_id',$tech); 
     $dbdata = array(
          "today_task_count" =>$count,
          "task_count" =>$tcount
     ); 
     $this->db->update('technician', $dbdata);
	 return true;
	}
	public function update_prev_tech($ticket,$tech)
	{
		$this->db->where('ticket_id',$ticket); 
     $dbdata = array(
          "prev_tech_id" =>$tech
     ); 
     $this->db->update('all_tickets', $dbdata);
	 return true;
	}
	public function auto_reassign($company_id)
	{
		$data  = array();
		$data1  = array();
        $date  = date("Y-m-d");
		$where4 = array(
            'all_tickets.current_status' => 1
        );
		$this->db->select('*,all_tickets.last_update as last,all_tickets.assigned_time as assigned');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('priority', 'priority.priority_level=customer.priority');
        $this->db->where($where4);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $query1 = $this->db->get();
        $query  = $query1->result_array();
        foreach ($query as $row) {
			$assigned=new DateTime($row['assigned']);
			$last=new DateTime($row['last']);
			$interval = $assigned->diff($last);
			$elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
			if($elapsed<=$row['response_time'])
			{
			}
			else
			{
				$address   = $row['address'];
				$town   = $row['town'];
				$city   = $row['city'];
				$state   = $row['state'];
				$new_add=$address.','.$town.','.$city.','.$state;
				$datetime1 = $row['last'];
				$tech_id = $row['tech_id'];
				$ticket_id = $row['ticket_id'];
				$product = $row['product_id'];
				$category = $row['cat_id'];
				$date1 = date('Y-m-d', strtotime($datetime1));
				$time1 = date('H:i:s', strtotime($datetime1));
				// Get JSON results from this request
				$geo   = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($new_add) . '&sensor=false');
				
				// Convert the JSON to an array
				$geo = json_decode($geo, true);
				
				if ($geo['status'] == 'OK') {
					// Get Lat & Long
					$latitude  = $geo['results'][0]['geometry']['location']['lat'];
					$longitude = $geo['results'][0]['geometry']['location']['lng'];
				}
				$where=array('today_task_count<'=>4,'product'=>$product,'category'=>$category,'availability'=>1,'technician_id!='=>$tech_id);			
		$this->db->select('technician_id, today_task_count,product,category,( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) *sin( radians( current_lat ) ) ) ) AS distance');    
		$this->db->from('technician');
		$this->db->having('distance < 2');
		$this->db->order_by('today_task_count');
		$this->db->where($where);
		$this->db->limit(1);		
		$query1 = $this->db->get();	
		$query=$query1->result_array();
		if(!empty($query))
				{
				   foreach($query as $row1)
				    {
				      $resu=$this->assign($ticket_id,$row1['technician_id'],$row1['resolution_time'],$row1['response_time'],$row1['priority'],$row1['target_aceptance_time']);
				      $resu1=$this->update_tech_task($row1['technician_id'],$row1['today_task_count']);
				      $resu2=$this->update_prev_tech($ticket_id,$tech_id);
				    }
				}
		else
		{
			$where=array('today_task_count<'=>4,'product'=>$product,'category'=>$category,'availability'=>1,'technician_id!='=>$tech_id);			
			$this->db->select('technician_id,today_task_count,product,category, ( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
			$this->db->from('technician');
			$this->db->having('distance < 4');
			$this->db->where($where); 
			$this->db->order_by('today_task_count');
			$this->db->limit(1);
			$query11 = $this->db->get();	
			$query2=$query11->result_array();
			if(!empty($query2))
				{
					foreach($query2 as $row2)
					{
					$resu=$this->assign($ticket_id,$row2['technician_id'],$row1['resolution_time'],$row1['response_time'],$row1['priority'],$row1['target_aceptance_time']);
					$resu1=$this->update_tech_task($row1['technician_id'],$row1['today_task_count']);
					$resu2=$this->update_prev_tech($ticket_id,$tech_id);
					}
				}
			else
			{
				$where=array('today_task_count<'=>4,'product'=>$product,'category'=>$category,'availability'=>1,'technician_id!='=>$tech_id);			
				$this->db->select('technician_id,today_task_count ,product,category,( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
				$this->db->from('technician');
				$this->db->having('distance < 8');
				$this->db->order_by('today_task_count');
				$this->db->where($where); 
				$this->db->limit(1);
				$res = $this->db->get();	
				$query3=$res->result_array();
				if(!empty($query3))
				{
					foreach($query3 as $row1)
					{
					$resu=$this->assign($ticket_id,$row1['technician_id']);
					$resu1=$this->update_tech_task($row1['technician_id'],$row1['today_task_count']);
					$resu2=$this->update_prev_tech($ticket_id,$tech_id);
					}
				}
				else
			{
				$where=array('today_task_count<'=>4,'product'=>$product,'category'=>$category,'availability'=>1,'technician_id!='=>$tech_id);			
				$this->db->select('technician_id,today_task_count,product,category, ( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
				$this->db->from('technician');
				$this->db->having('distance > 8');
				$this->db->order_by('today_task_count');
				$this->db->where($where); 
				$this->db->limit(1);
				//$this->db->orderby('distance');
				$res1 = $this->db->get();	
				$query4=$res1->result_array();
				if(!empty($query4))
				{
					foreach($query4 as $row1)
					{
					$resu=$this->assign($ticket_id,$row1['technician_id']);
					$resu1=$this->update_tech_task($row1['technician_id'],$row1['today_task_count']);
					$resu2=$this->update_prev_tech($ticket_id,$tech_id);
					}
				}
			}
			}
		}
			}
		}
		return true;
	}
public function auto_escalate($ticket_id,$tech_id)
	{
		$data  = array();
		$data1  = array();
        $date  = date("Y-m-d");
		$where4 = array(
			'all_tickets.ticket_id'=>$ticket_id,
			'all_tickets.tech_id'=>$tech_id
        );
		$this->db->select('*,all_tickets.address as all_add,all_tickets.town as all_town,all_tickets.city as all_city,all_tickets.state as all_state,all_tickets.last_update as last,all_tickets.assigned_time as assigned');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->where($where4);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->group_by('customer.customer_id');
        $query1 = $this->db->get();
        $query  = $query1->result_array();
        foreach ($query as $row) {
			$assigned=new DateTime($row['assigned']);
			$last=new DateTime($row['last']);
			$interval = $assigned->diff($last);
			$elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
				$address   = $row['all_add'];
				$town   = $row['all_town'];
				$city   = $row['all_city'];
				$state   = $row['all_state'];
				$new_add=$address.','.$town.','.$city.','.$state;
				$datetime1 = $row['last'];
				$tech_id = $row['tech_id'];
				$ticket_id = $row['ticket_id'];
				$product = $row['product_id'];
				$category = $row['cat_id'];
				$date1 = date('Y-m-d', strtotime($datetime1));
				$time1 = date('H:i:s', strtotime($datetime1));
				// Get JSON results from this request
				$geo   = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($new_add) . '&sensor=false');
				
				// Convert the JSON to an array
				$geo = json_decode($geo, true);
				
				if ($geo['status'] == 'OK') {
					// Get Lat & Long
					$latitude  = $geo['results'][0]['geometry']['location']['lat'];
					$longitude = $geo['results'][0]['geometry']['location']['lng'];
				}
				$where=array('today_task_count<'=>1,'product'=>$product,'category'=>$category,'availability'=>1,'technician_id!='=>$tech_id);			
		$this->db->select('technician_id, today_task_count,product,category,( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
		$this->db->from('technician');
		$this->db->having('distance < 2');
		$this->db->order_by('today_task_count');
		$this->db->where($where);
		$this->db->limit(1);		
		$query1 = $this->db->get();
		$query=$query1->result_array();
		if(!empty($query))
				{
					foreach($query as $row1)
					{
					$resu=$resu=$this->escalate_assign($ticket_id,$row1['technician_id'],$row['resolution_time'],$row['response_time'],$row['priority'],$row['target_aceptance_time']);
					$resu1=$this->update_tech_task($row1['technician_id'],$row1['today_task_count']);
					$resu2=$this->update_prev_tech($ticket_id,$tech_id);
					$resu3=$this->update_escalate_prev_status($ticket_id);
					}
				}
		else
		{
			$where=array('today_task_count<'=>1,'product'=>$product,'category'=>$category,'availability'=>1,'technician_id!='=>$tech_id);			
			$this->db->select('technician_id,today_task_count,product,category, ( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
			$this->db->from('technician');
			$this->db->having('distance < 4');
			$this->db->where($where); 
			$this->db->order_by('today_task_count');
			$this->db->limit(1);
			$query11 = $this->db->get();	
			$query2=$query11->result_array();
			if(!empty($query2))
				{
					foreach($query2 as $row2)
					{
					$resu=$resu=$this->escalate_assign($ticket_id,$row2['technician_id'],$row['resolution_time'],$row['response_time'],$row['priority'],$row['target_aceptance_time']);
					$resu1=$this->update_tech_task($row2['technician_id'],$row2['today_task_count']);
					$resu2=$this->update_prev_tech($ticket_id,$tech_id);
					$resu3=$this->update_escalate_prev_status($ticket_id);
					}
				}
			else
			{
				$where=array('today_task_count<'=>1,'product'=>$product,'category'=>$category,'availability'=>1,'technician_id!='=>$tech_id);			
				$this->db->select('technician_id,today_task_count ,product,category,( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
				$this->db->from('technician');
				$this->db->having('distance < 8');
				$this->db->order_by('today_task_count');
				$this->db->where($where); 
				$this->db->limit(1);
				$res = $this->db->get();	
				$query3=$res->result_array();
				if(!empty($query3))
				{
					foreach($query3 as $row3)
					{
					$resu=$resu=$this->escalate_assign($ticket_id,$row3['technician_id'],$row['resolution_time'],$row['response_time'],$row['priority'],$row['target_aceptance_time']);
					$resu1=$this->update_tech_task($row3['technician_id'],$row3['today_task_count']);
					$resu2=$this->update_prev_tech($ticket_id,$tech_id);
					$resu3=$this->update_escalate_prev_status($ticket_id);
					}
				}
				else
			{
				$where=array('today_task_count<'=>1,'product'=>$product,'category'=>$category,'availability'=>1,'technician_id!='=>$tech_id);			
				$this->db->select('technician_id,today_task_count,product,category, ( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( current_lat ) ) *	cos( radians( current_long ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * 	sin( radians( current_lat ) ) ) ) AS distance');    
				$this->db->from('technician');
				$this->db->having('distance > 8');
				$this->db->order_by('today_task_count');
				$this->db->where($where); 
				$this->db->limit(1);
				//$this->db->orderby('distance');
				$res1 = $this->db->get();	
				$query4=$res1->result_array();
				if(!empty($query4))
				{
					foreach($query4 as $row4)
					{
					$resu=$resu=$this->escalate_assign($ticket_id,$row4['technician_id'],$row['resolution_time'],$row['response_time'],$row['priority'],$row['target_aceptance_time']);
					$resu1=$this->update_tech_task($row4['technician_id'],$row4['today_task_count']);
					$resu2=$this->update_prev_tech($ticket_id,$tech_id);
					$resu3=$this->update_escalate_prev_status($ticket_id);
					}
				}
			}
			}
		}
		}
		return true;
	}
	public function update_escalate_prev_status($ticket)
	{
		$this->db->where('ticket_id',$ticket); 
     $dbdata = array(
          "previous_status" =>9,
          "current_status" =>1
     ); 
     $this->db->update('all_tickets', $dbdata);
	 return true;
	}
	public function auto_spare_approval($company_id)
	{
		$data  = array();
		$data1  = array();
        $date  = date("Y-m-d");
		$where4 = array(
            'all_tickets.current_status' => 14
        );
		$query=array();
		$this->db->select('*,all_tickets.last_update as last,all_tickets.assigned_time as assigned');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('priority', 'priority.priority_level=customer.priority');
        $this->db->where($where4);
        $query1 = $this->db->get();
        $query  = $query1->result_array();
		$wait=0;
        foreach ($query as $row)
		{
			$spare_array=json_decode($row['requested_spare'],true);
			foreach($spare_array as $sp)
			{
				$spare_code=$sp['Spare_code'];
				$where = array(
					'spare_code' =>$spare_code
				);
				$remains=0;
				$this->db->select('*');
				$this->db->from('spare');
				$this->db->where($where);
				$query2 = $this->db->get();
				$query3  = $query2->result_array();
				$quantity_purchased=$query3[0]['quantity_purchased'];
				$quantity_used=$query3[0]['quantity_used'];
				echo $remains=$quantity_purchased-$quantity_used;
				if($remains<=0)
				{
					if($wait==0)
					{
						$wait=1;
					}
				}
			}
			echo $wait;
			if($wait==0)
			{
				$spare_array1=json_decode($row['requested_spare'],true);
				foreach($spare_array1 as $sp1)
				{
					$spare_code1=$sp1['Spare_code'];
					$quantity=$sp1['quantity'];
					$where1 = array(
						'spare_code' =>$spare_code1
					);
					$this->db->select('quantity_used,quantity_purchased');
					$this->db->from('spare');
					$this->db->where($where1);
					$query4 = $this->db->get();
					$query5  = $query4->result_array();
					$quan_used=$query5[0]['quantity_used'];
					$final_used=$quan_used+$quantity;
                                        $quan_pur=$query5[0]['quantity_purchased'];
					$final_purscased=($quan_pur)-($quantity);
					$update_data=array('quantity_used'=>$final_used,'quantity_purchased'=>$final_purscased);
					$this->db->where($where1);
					$this->db->update('spare',$update_data);
				}
				$where2=array(
					'ticket_id'=>$row['ticket_id']
				);
				$update_data1=array('current_status'=>11);
				$this->db->where($where2);
				$this->db->update('all_tickets',$update_data1);
			}
		}
		return true;
	}
public function auto_tech_task($company_id)
	{
		$count=0;
     $dbdata = array(
          "today_task_count" =>$count
     ); 
$where=array('company_id'=>$company_id	
				);
$this->db->where($where);
     $this->db->update('technician', $dbdata);
	 return true;
	}
public function auto_tech($company_id,$tech_id)
	{
		$count=0;
     $dbdata = array(
          "task_count" =>$count
     ); 
$where=array('company_id'=>$company_id,'technician_id'=>$tech_id
				);
$this->db->where($where);
     $this->db->update('technician', $dbdata);
	 return true;
	}
public function auto_tech_task1($company_id,$tech_id)
	{
		$count=0;
     $dbdata = array(
          "today_task_count" =>$count
     ); 
$where=array('company_id'=>$company_id,'technician_id'=>$tech_id
				);
$this->db->where($where);
     $this->db->update('technician', $dbdata);
	 return true;
	}

   }
?> 