<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory_export extends CI_Model{
    	
public function inventoryexcel_fp_sap()
{
    $this->load->model('common');	
    $this->load->library('excel');
    
    
    $this->excel->setActiveSheetIndex(0);
    //name the worksheet
    $this->excel->getActiveSheet()->setTitle('Inventory');
    //set cell A1 content with some text
    $this->excel->getActiveSheet()->setCellValue('A1', 'Spare_code');
    $this->excel->getActiveSheet()->setCellValue('B1', 'Spare_name');
    $this->excel->getActiveSheet()->setCellValue('C1', 'Product Category');
    $this->excel->getActiveSheet()->setCellValue('D1', 'Product Model No');
    $this->excel->getActiveSheet()->setCellValue('E1', 'Spare Image');

    $this->excel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true);
    $this->excel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setARGB('#333');
    //	$this->excel->getActiveSheet()->getStyle('A1:L1')->setQuotePrefix(false);
    
    //make the font become bold
    
   for($col = ord('A'); $col <= ord('E1'); $col++){
            //set column dimension
            $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
             //change the font size
            //$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
    }
    
    /*********************************************************************/
    //Select Query Start
    $spare = $this->common->inventory_export_excel();
//	print_r($spare->result());
//		die;
    $exceldata=array();
    if($spare->num_rows() > 0){
    $i=0;
     foreach ($spare->result() as $row){
            
        $exceldata[$i] = array($row->spare_code,$row->spare_name,$row->product_name,$row->spare_modal,$row->image,);
        
    
$i++;

    }

            $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');
            
           // $date = date("d-m-Y");
        //$filename= 'assets/excel/customer/customer_export_'.$date.'.xlsx'; //save our workbook as this file name
       // $filename= 'inventory_export_''.xlsx'; //save our workbook as this file name
        
    //	header('Content-Type: application/vnd.ms-excel'); //mime type
    //	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    //	header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
    header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment;filename="Inventory_details.xls"');
      header('Cache-Control: max-age=0');
    header("Pragma: no-cache"); 
    header("Expires: 0");
    
    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output'); 
    
                
     }
     else{
         echo "No data";
        // $this->excel->getActiveSheet()->setCellValue('A2', 'Sorry!! No data available');
        // $this->excel->getActiveSheet()->mergeCells('A2:J1');
         //set aligment to center for that merged cell (A1 to C1)
        // $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
     }
    
}	
}
?>