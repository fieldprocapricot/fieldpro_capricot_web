<?php
   class Admin_model extends CI_Model
   {
      function __construct()
      {
         // Call the Model constructor
         parent::__construct();
      }
public function load_notify($company_id)
    {
        $where = array(
            'company_id' => $company_id,
            'role' => 'Admin'
        );
        $this->db->select('*');
        $this->db->from('notification');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
        return $data;
    }
	public function get_licence($company_id){
		$this->db->select('*');
		$this->db->from('admin_user');
		$this->db->where('company_id',$company_id);
		$this->db->order_by("last_update", "DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row_array();
	}
	public function insert_adminlicence($data,$id){
		$this->db->where('admin_id',$id);
		$this->db->update('admin_user',$data);
		return true;
	}
  public function get_employee($datass,$datas)
  {
    $where=array(
        'login.username'=>$datass,
        'login.companyid'=>$datas
      );
   //data is retrive from this query
    $this->db->select('*');
    $this->db->from('login');
	  $this->db->join('user', 'user.email_id = login.username');
    $this->db->where($where);
    $query = $this->db->get();
  // $query = $this->db->get('Users');
    return $query;
  }
  public function get_details_user($datass,$datas){
    $where=array(
      'login.username'=>$datass,
      'login.companyid'=>$datas
    );
      $this->db->select('*');
    $this->db->from('login');
	  $this->db->join('user', 'user.email_id = login.username');
    $this->db->where($where);
    $query = $this->db->get();
    return $query->row_array();
  }

 public function submit_customer($data,$cust_id)
	{
   
		 // Fetch the old email before updating
     $this->db->select('email_id');
     $this->db->where('customer_id', $cust_id);
     $query = $this->db->get('customer');
     $old_email = $query->row()->email_id ?? null;
 
     // Update the customer table
     $this->db->where('customer_id', $cust_id);
     $this->db->update('customer', $data);
 
     // If email is updated and old email exists, update the login table
     if (!empty($old_email) && isset($data['email_id']) && !empty($data['email_id'])) {
         $login_update = ['username' => $data['email_id']];
         $this->db->where('username', $old_email);
         $this->db->update('login', $login_update);
     }

		return true;
	}
public function get_details($datas){
      $this->db->select('*');
    $this->db->from('admin_user');
    $this->db->join('user', 'user.email_id = admin_user.ad_mailid');
    $this->db->where('user.company_id',$datas);
    $this->db->where('admin_user.company_id',$datas);
	$this->db->order_by("user.last_update", "DESC");
    $this->db->limit(1);
    $query = $this->db->get();
    return $query;
}
      public function last_spare($datas){
        $querys=array();
      $this->db->select('last_update as spares');
    $this->db->from('spare');
    $this->db->where('company_id',$datas);
    $this->db->order_by("last_update", "DESC");
    $this->db->limit(1);
    $query = $this->db->get();
    $spare = $query->result_array();
if(empty($spare))
{
$spare=array(array("spares"=>"No Spare available"));
}

      $this->db->select('last_update as product_managements');
    $this->db->from('product_management');
    $this->db->where('company_id',$datas);
    $this->db->order_by("last_update", "DESC");
    $this->db->limit(1);
    $query1 = $this->db->get();
    $product_management = $query1->result_array();
if(empty($product_management))
{
$product_management=array(array("product_managements"=>"No Product available"));
}
      $this->db->select('last_update as all_ticketss');
    $this->db->from('all_tickets');
    $this->db->where('company_id',$datas);
    $this->db->order_by("last_update", "DESC");
    $this->db->limit(1);
    $query2 = $this->db->get();
    $all_tickets = $query2->result_array();
	if(empty($all_tickets))
{
$all_tickets=array(array("all_ticketss"=>"No Tickets available"));
}
    return array_merge($spare,$product_management,$all_tickets);
    }
      public function reg_tech($datas){
      $this->db->select('technicians,service_desk');
    $this->db->from('admin_user');
    $this->db->join('user', 'user.email_id = admin_user.ad_mailid');
    $this->db->where('user.company_id',$datas);
$this->db->order_by("user.last_update", "DESC");
$this->db->limit(1);
    $query = $this->db->get();
    return $query;
    }
      public function reg_service($datas){
      $this->db->select('*');
    $this->db->from('admin_user');
    $this->db->join('user', 'user.email_id = login.ad_mailid');
    $this->db->where('company_id',$datas);
    $query = $this->db->get();
    return $query->num_rows();
    }
    public function count_tech($datas){
      $this->db->select('*');
    $this->db->from('technician');
    $this->db->where('company_id',$datas);
    $this->db->group_by('technician_id');
    $query = $this->db->get();
    return $query->num_rows();
    }
    public function count_service($datas){
      $this->db->select('*');
    $this->db->from('user');
    $this->db->where('role','Service_Desk');
    $this->db->where('company_id',$datas);
    $query = $this->db->get();
    return $query->num_rows();
    }
    public function count_call($datas){
      $this->db->select('*');
    $this->db->from('user');
    $this->db->where('role','Call-coordinator');
    $this->db->where('company_id',$datas);
    $query = $this->db->get();
    return $query->num_rows();
    }
    public function count_manager($datas){
      $this->db->select('*');
    $this->db->from('user');
    $this->db->where('role','Service_Manager');
    $this->db->where('company_id',$datas);
    $query = $this->db->get();
    return $query->num_rows();
    }

   public function getdetails_sparecode($data,$com)
	 {
		 
		 $where=array('spare_code'=>$data,'spare.company_id'=>$com);
	     $this->db->select('*');
		$this->db->from('spare');
		$this->db->join('product_management', 'product_management.product_id = spare.product');
		$this->db->join('category_details', 'category_details.cat_id = spare.category');
		$this->db->where($where);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	 }

    public function getuser($data)
    {
     $result=array();
     $where=array('product'=>"",'category'=>"",'skill'=>"");
     $this->db->select('*,technician.id as a');
     $this->db->group_by('technician.employee_id');
     $this->db->from('technician');
     $this->db->join('product_management', 'product_management.product_id = technician.product');
	  $this->db->join('category_details', 'category_details.cat_id = technician.category');
     $this->db->where('technician.company_id',$data);
	  $this->db->order_by("a", "desc");
     $query = $this->db->get();
     array_push($result,array('technician'=>$query->result_array()));
     $this->db->select('*');
     $this->db->from('user');
     $this->db->where('user.company_id',$data);
     $this->db->order_by('id','desc');
     $query1=$this->db->get();
     array_push($result,array('user'=>$query1->result_array()));
     return json_encode($result,true);
    }
      public function view_tech($company_id)
    {
		//$company_id = $this->session->userdata('companyid');
        $this->db->select('*');
        $this->db->from('technician');  
   $this->db->where('technician.company_id',$company_id);
        $query = $this->db->get();
		$result = $query->result_array();
		return $result;

    }
    public function disply_admin2($filter)
    {
     if($filter=='c_today'){
       $where=array('region'=>'north');
       $this->details_users($where);
     }
     else if($filter=='c_weekly'){
       $where=array('region'=>'south');
       $this->details_users($where);
     }
     else if($filter=='c_monthly'){
       $where=array('region'=>'east');
       $this->details_users($where);
     }
     else if($filter=='c_yearly'){
       $where=array('region'=>'west');
       $this->details_users($where);
     }
     else{
       $where=array();
       $this->details_users($where);
     }
    }
  public function details_users($where){
     $result=array();
     $this->db->select('*, user.id as a');
     $this->db->from('user');
     $this->db->where($where);
     $query = $this->db->get();
     $query = $query->result_array();
     //array_push($result,array('user'=>$query));
     $this->db->select('*, technician.id as b');
     $this->db->from('technician');
     $this->db->join('product_management', 'product_management.product_id = technician.product');
     $this->db->join('category_details', 'category_details.cat_id = technician.category');
     $this->db->where($where);
     $query1 = $this->db->get();
     $query1 = $query1->result_array();
     //array_push($result,array('technician'=>$query1));
     $result=array_merge($query,$query1);
     print_r(json_encode($result));
  }
    /*public function get_users(){
    $this->db->select('*,user.id as a');
    $this->db->from('user');
    $this->db->join('product_management', 'product_management.product_id = user.product');
    $this->db->join('category_details', 'category_details.cat_id = user.category');
      $this->db->order_by("a", "desc");
    $query = $this->db->get();
    return $query->result();
    }*/
    public function checkservicedesk($data)
    {
		$this->db->select('*');
		$this->db->from('admin_user');
		$this->db->where($data);
		$this->db->order_by("admin_id", "DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		 if ($query->num_rows() > 0)
		  {
			return $query->row_array();
		  }
			else {return NULL;}
    }
    public function service_added_check($c_id){
     $where=array(
      'company_id' => $c_id,
     );
       $this->db->select('service_added');
     $this->db->from('admin_user');
     $this->db->where($where);
     $query = $this->db->get()->row()->service_added;
     return $query;

    }
	   
	 public function service_count_check($c_id,$role){
      $where=array(
      'companyid' => $c_id,
	  'user_type' => $role	 
		 	
     );
		 
		$this->db->select('count(companyid) as cnt_emp');
        $this->db->from('login');
        $this->db->where($where);
		 $this->db->group_by("login.companyid");
        $query = $this->db->get();
        $resul = $query->row_array();
        $tot_cnt = $resul['cnt_emp']; 

    }   
	   
    public function updateservice_added_check($c_id,$datass){
     $where=array(
      'company_id' => $c_id
     );
     $this->db->where($where);
     $this->db->update('admin_user',$datass);
     return true;
       /*$this->db->select('service_added');
     $this->db->from('admin_user');
     $query = $this->db->get()->row()->service_added;
     return $query;*/

    }
    public function technician_added_check($c_name,$c_id){
     $where=array(
      'company_id' => $c_id,
     );
      $this->db->select('tech_added');
     $this->db->from('admin_user');
     $this->db->where($where);
	 $this->db->order_by('company_id','desc');
	 $this->db->limit(1);
     $query = $this->db->get();
	 $sql = $this->db->last_query();

      $query = $query->row_array();
     return $query;
    }
	   
	 	 public function technician_count_check($c_id,$role){
     $where=array(
      'companyid' => $c_id,
	  'user_type' => $role	 
		 	
     );
		 
		$this->db->select('count(companyid) as cnt_emp');
        $this->db->from('login');
        $this->db->where($where);
		 $this->db->group_by("login.companyid");
        $query = $this->db->get();
        $resul = $query->row_array();
        $tot_cnt = $resul['cnt_emp']; 
		
    return $tot_cnt;

    }     
	   
	   
    public function updatetechnician_added_check($c_id,$datas){
     $where=array(
      'company_id' => $c_id
     );
     $this->db->where($where);
     $this->db->update('admin_user',$datas);
     return true;
       /*$this->db->select('service_added');
     $this->db->from('admin_user');
     $query = $this->db->get()->row()->service_added;
     return $query;*/

    }
    public function userid_check(){
     $this->db->select('*');
     $this->db->from('user');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     $id = $query->result_array();
     if(!empty($id)){
      return $id[0]['id'];
    }else{
      return 0;
    }
    }
    public function insertuser($data)
    {
    $this->db->insert('user', $data);
    return $this->login_details($data);
    //return TRUE;
    }
    public function login_details($data){  //////////////////////// raju
       $insert=array(
         'user_type'=>$data['role'],
         'username'=>$data['email_id'],
         'companyid'=>$data['company_id'],
         'companyname'=>$data['companyname']
       );
     $this->db->insert('login', $insert);
     return true;
    }
    public function getuserdata($cellData,$cellData1)
    {
      $where=array(
      'email_id'=>$cellData,
      'contact_number'=>$cellData1,
      );
     $this->db->select('*');
     $this->db->from('user');
     $this->db->where('email_id',$cellData)->where('contact_number',$cellData1);
       //$this->db->order_by("id", "desc");
     $query = $this->db->get();
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else {return NULL;}
    }
    public function getdetails_user($data)
    {
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where('user_id',$data);
      //$this->db->order_by("id", "desc");
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->result_array();
    }
      $this->db->select('technician.*,product_management.product_name,category_details.cat_name,service_group.service_group');
      $this->db->from('technician');
	  $this->db->join('product_management', 'product_management.product_id = technician.product', 'left');
	  $this->db->join('category_details', 'category_details.prod_id = technician.product and category_details.cat_id = technician.category', 'left');
		
		 $this->db->join('service_group', 'service_group.service_group_id = technician.work_type', 'left');
      $this->db->where('technician_id',$data);
      //$this->db->order_by("id", "desc");
      $query = $this->db->get();
	$sql=$this->db->last_query(); 
	//print_r($sql);
	//die;	
      if ($query->num_rows() > 0)
      {
        return $query->result_array();
      }
        else {return NULL;}
    }
    public function check_roles($role,$emp_id)
    {
    $where=array(
      'employee_id'=>$emp_id,
      'role'=>$role,
    );
    if($role == "Technician"){
      $this->db->select('role');
      $this->db->from('technician');
      $this->db->where($where);
      //$this->db->order_by("id", "desc");
      $query = $this->db->get();
      if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else {return NULL;}
    }else{
      $this->db->select('role');
      $this->db->from('user');
      $this->db->where($where);
      //$this->db->order_by("id", "desc");
      $query = $this->db->get();
      if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else {return NULL;}
    }
    }
    public function data_roles($role,$emp_id)
    {
    $where=array(
      'employee_id'=>$emp_id
    );
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where($where);
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
    else{
      $this->db->select('*');
      $this->db->from('technician');
      $this->db->where($where);
      $query = $this->db->get();
      if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else {return NULL;}
    }
    }
    public function edituser($data,$emp_id,$email_copy_id1,$role)
    {
    if($role == "Technician"){
      $this->db->where('employee_id',$emp_id);
      $this->db->update('technician', $data);
      return $this->login_tech_change($data,$email_copy_id1);
    }else{
      $this->db->where('employee_id',$emp_id);
      $this->db->update('user', $data);
      return $this->login_tech_change($data,$email_copy_id1);
    }
    }
    public function edit_insert_user($data,$emp_id,$role,$c_id)
    {
		$where=array('employee_id'=>$emp_id,'company_id'=>$c_id);
		if($role == "Technician"){
		  $this->db->insert('technician',$data);
		  if(1==1){
			$this->db->where($where);
			$this->db->delete('user');
		  }
		  return $this->login_change($data);
		}else{
		  $this->db->insert('user',$data);
		  $this->db->where($where);
		  $this->db->delete('technician');
		  return $this->login_change($data);
		}
    }
	public function login_change($data){
		$where=array('username'=>$data['email_id'],'companyid'=>$data['company_id']);
		$datas=array(
			'user_type'=>$data['role'],
		);
		$this->db->where($where);
		$this->db->update('login',$datas);
		return true;
	}
 
	public function login_tech_change($data,$email_copy_id1){
		$where=array('username'=>$email_copy_id1,'companyid'=>$data['company_id']);
		$datas=array(
			'user_type'=>$data['role'],
			'username'=>$data['email_id']
		);
		$this->db->where($where);
		$this->db->update('login',$datas);
		return true;
	}
    
	   
	   public function deleteuser($id)
    {      
      /*$this->db->where('id',$id);
      $this->db->delete('user');
      return true;*/
      $this->db->select('*');
      $this->db->from('user');
      $this->db->where('user_id',$id);
      $query=$this->db->get();
      $res=$query->row_array();

      if($query->num_rows()>0){
        $this->db->where('user_id',$id);
        $this->db->delete('user');
if(1==1)
{
$this->db->where('username',$res['email_id']);
        $this->db->delete('login');

}
        return true;
      }else{
        $this->db->select('*');
        $this->db->from('technician');
        $this->db->where('technician_id',$id);
        $query=$this->db->get();
      $res=$query->row_array();

        if($query->num_rows()>0){
          $this->db->where('technician_id',$id);
          $this->db->delete('technician');

if(1==1)
{
$this->db->where('username',$res['email_id']);
        $this->db->delete('login');

}

          return true;
        }  
      }
    }
    public function gettechnician()
    {
    $this->db->select('*,technician.id as a');
    $this->db->from('technician');
    $this->db->join('product_management', 'product_management.id = technician.product');
    $this->db->join('category_details', 'category_details.id = technician.category');
    $this->db->order_by("a", "desc");
    $query = $this->db->get();
    return $query;
    }
    public function checktech($data){
       $this->db->select('*');
     $this->db->from('admin_user');
     $this->db->where($data);
	$this->db->order_by("admin_id", "DESC");
	$this->db->limit(1);
     //$this->db->where('technicians <= tech_added');
     $query = $this->db->get();
     if ($query->num_rows() > 0)
     {
      //return TRUE;
      return $query->row_array();
     }
      else {return 0;}

    }
    public function techid_check(){
     $this->db->select('*');
     $this->db->from('technician');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     $id = $query->result_array();
      if(!$id==""){
        return $id[0]['id'];
      }else{
        return 0;
      }
    }
   public function inserttechnician($datas)
    {
    $this->db->insert('technician', $datas);
	return $this->login_details($datas);
    //return TRUE;
    }
	   
	   
    public function inserttechnician_temporary($datas)
    {
    $this->db->insert('technician', $datas);
	//return $this->login_details($datas);
    return true;
    }
    public function gettechdata($cellData,$cellData1)
    {
      $where=array(
      'email_id'=>$cellData,
      'contact_number'=>$cellData1,
      );
     $this->db->select('*');
     $this->db->from('user');
     $this->db->where('email_id',$cellData)->where('contact_number',$cellData1);
       //$this->db->order_by("id", "desc");
     $query = $this->db->get();
     if ($query->num_rows() > 0)
      {
        return $query->count(row_array());
      }
        else {return NULL;}
    }
     public function getdetails_technician($data)
    {
    $this->db->select('*');
    $this->db->from('technician');
    $this->db->where('id',$data);
      //$this->db->order_by("id", "desc");
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
      else {return NULL;}
    }
    public function edit_technician($data,$emp_id)
    {
    $this->db->where('employee_id',$emp_id);
    $this->db->update('technician', $data);
    return true;
    }
    public function deletetechnician($id)
    {
    $this->db->where('id',$id);
    $this->db->delete('technician');
    return true;
    }
	   public function technicianbasedtechid($techid)
	   {
		   $this->db->where('technician_id',$techid);
    $this->db->delete('technician');
	   }
    public function getcustomer()
    {
     $this->db->select('*,customer.id as a');
     $this->db->from('customer');
       $this->db->join('product_management', 'product_management.product_id = customer.product_serial_no');
     $this->db->join('category_details', 'category_details.cat_id = customer.component_serial_no');
       $this->db->order_by("a", "desc");
     $query = $this->db->get();
     return $query;
    }
    public function getcustomer1($datas)
    {

     $this->db->select('*,customer.id as a');
     $this->db->from('customer');
	 $this->db->group_by('customer_id');
	 //$this->db->limit(1);
      // $this->db->join('product_management', 'product_management.product_id = customer.product_serial_no');
    // $this->db->join('category_details', 'category_details.cat_id = customer.component_serial_no');
       $this->db->where("company_id",$datas);
	     //$this->db->where('cust_type',0);
       $this->db->order_by("a", "asc");
     $query = $this->db->get();
     return $query;
    }
    public function custid_check(){
     $this->db->select('*');
     $this->db->from('customer');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     $id = $query->result_array();
      if(!$id==""){
        return $id[0]['id'];
      }else{
        return 0;
      }
    }
    public function insertcustomer($data)
    {
    $this->db->insert('customer', $data);
    return true;
    }
	public function bulk_spare($data,$product,$category,$datass,$company_id,$s_code,$s_desc,$name,$modal,$source,$price,$qty,$start_date,$end_date)
	{
    $where=array();
    $where=array(
      'spare_code'=>$s_code,
      'p_date'=>$start_date,
      );
		 $query = $this->db->select('spare_code')
             ->from('spare')
            
             ->where('spare_code',$s_code)
             ->where('p_date',$start_date)
             ->get();

             if($query->num_rows()>0){
                   $query= $query->row_array();
	           return "already exist";

            }
		    else{
		     $query1 = $this->db->select('product_id')
                     ->from('product_management')
                     ->where('company_id',$company_id)
                     ->where('product_name',$product)
                     ->get();

             if($query1->num_rows()>0){
                    $query1= $query1->row_array();

                   $query2 = $this->db->select('cat_id')
                  ->from('category_details')
                  ->where('prod_id',$query1['product_id'])
                  ->where('company_id',$company_id)
                  ->where('cat_name',$category)
                  ->get();
                   if($query2->num_rows()>0){
                          $query2= $query2->row_array();

                        $data1=array(
									'spare_id'=>$datass,
									'spare_code'=>$s_code,
									'spare_desc'=>$s_desc,
									'spare_name'=>$name,
									'spare_modal'=>$modal,
									'spare_source'=>$source,
									'price'=>$price,
									'quantity_purchased'=>$qty,
									'product'=> $query1['product_id'],
									'category'=>$query2['cat_id'],
									'p_date'=>$start_date,
									'ep_date'=>$end_date,
					  				'expiry_date'=>$end_date,
									'company_id'=>$company_id
								);
					 
                    $this->db->insert('spare', $data1);
		                return true;

                 }
				 else{
					 return "category does not exist";
				 }
	       }
		else {
			return "product does not exist";
		}
	 }
	}
	   
    public function getcustdata($cellData1,$cellData2)
    {
      $where=array(
      'email_id'=>$cellData1,
      'contact_number'=>$cellData2,
      );
     $this->db->select('*');
     $this->db->from('customer');
     $this->db->where($where);
       //$this->db->order_by("id", "desc");
     $query = $this->db->get();
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else {return NULL;}
    }
    public function getcustid($cellData1,$cellData2)
    {
      $where=array(
      'email_id'=>$cellData1,
      'contact_number'=>$cellData2,
      );
     $this->db->select('*');
     $this->db->from('customer');
     $this->db->where($where);
       //$this->db->order_by("id", "desc");
     $query = $this->db->get();
     $res = $query->row_array();
     if ($query->num_rows() > 0)
      {
        return $res['customer_id'];
      }
        else {return 0;}
    }
    public function checkcustid($cust_id)
    {
      $where=array(
      'customer_id'=>$cust_id
      );
     $this->db->select('*');
     $this->db->from('customer');
     $this->db->where($where);
       //$this->db->order_by("id", "desc");
     $query = $this->db->get();
     $res = $query->row_array();
     if ($query->num_rows() > 0)
      {
        return true;
      }
        else {return false;}
    }
    public function produ_checking($product_check)
    {
     $this->db->select('*');
     $this->db->from('product_management');
     $this->db->where('product_id',$product_check);
     $query = $this->db->get();
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else {return NULL;}
    }
    public function produ_checking1($product_check,$c_id)
    {
     $this->db->select('*');
     $this->db->from('product_management');
     $this->db->where('LOWER(product_name)', strtolower($product_check));
     $this->db->where('company_id',$c_id);
     $query = $this->db->get();
	//	print_r($query);
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else 
		{
			return NULL;
		}
    }
    public function subcategory_serials($subcategory_serial)
    {
     $this->db->select('*');
     $this->db->from('category_details');
     $this->db->where('cat_id',$subcategory_serial);
     $query = $this->db->get();
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else {return NULL;}
    }
    public function subcategory_serials1($subcategory_serial,$product)
    {
     $this->db->select('*');
     $this->db->from('category_details');
     $this->db->where('LOWER(cat_name)', strtolower($subcategory_serial));
     $this->db->where('prod_id',$product);     
     $query = $this->db->get();
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else {return NULL;}
    }
     public function getdetails_customer($data)
    {
    $this->db->select('*,customer.id as a');
    $this->db->from('customer');
    $this->db->join('product_management', 'product_management.product_id = customer.product_serial_no');
    $this->db->join('category_details', 'category_details.cat_id = customer.component_serial_no');
    $this->db->where('customer.id',$data);
      //$this->db->order_by("id", "desc");
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
      else {return NULL;}
    }
    public function getdetails_contract($data)
	 {
	     $this->db->select('contract_id,type_of_contract,email_id,contact_number,alternate_number,product_serial_no,component_serial_no,model_no,serial_no,warrenty_expairy_date,start_date,end_date,
              product_management.product_name,category_details.cat_name');
		$this->db->from('customer');
		$this->db->join('product_management', 'product_management.product_id = customer.product_serial_no');
		$this->db->join('category_details', 'category_details.cat_id = customer.component_serial_no');
		$this->db->where('customer_id',$data);
		//$this->db->where('cust_type',0);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	 }
    public function edit_customer($data,$id)
    {
    $this->db->where('id',$id);
    $this->db->update('customer', $data);
    return true;
    }
    public function deletecustomer($id)
    {
    $this->db->where('id',$id);
    $this->db->delete('customer');
    return true;
    }
    public function getattendance()
    {
     $this->db->select('*');
     $this->db->from('attendance');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     return $query;
    }
    public function getproduct($datas)
    {
     $this->db->select('*');
     $this->db->from('product_management');
	 $this->db->where('company_id',$datas);
     $this->db->order_by("id", "desc");
     $query = $this->db->get();
     return $query;
    }
      public function get_servicegroup($company_id)
    {
        $where = array(
            'company_id' => $company_id,
        );
        $this->db->select('*');
        $this->db->from('service_group');
        $this->db->where($where);
        $this->db->or_where('service_group','All');
        $query = $this->db->get();
        
        return $query;
    }

    public function get_servicegroup_ticket($company_id)
    {
        $where = array(
            'company_id' => $company_id,
        );
        $this->db->select('*');
        $this->db->from('service_group');
        $this->db->where($where);
        $query = $this->db->get();    
        return $query;
    }

    public function get_servicegroupuser($company_id)
    {
        $where = array(
            'company_id' => $company_id,
        );
        $this->db->select('*');
        $this->db->from('service_group');
        $this->db->where($where);
        
        $query = $this->db->get();
        
        return $query;
    }
    /*public function getproduct_s()
    {
		//$stack = array("orange", "banana");
		$this->db->select('*,product_management.id as a');
		$this->db->from('product_management');
		//$this->db->join('category_details', 'category_details.prod_id = product_management.product_id');
		$this->db->order_by("a", "desc");
		$query = $this->db->get();
		$result=$query->result_array();
		foreach ($result as $row) {
			$data[] = $row;
		}
		return $data;
    }*/
    /*public function getcategory_s($prod_id_1)
    {
		//$stack = array("orange", "banana");
		$this->db->select('*');
		$this->db->from('category_details');
		//$this->db->join('category_details', 'category_details.prod_id = product_management.product_id');
		$this->db->where("prod_id", $prod_id_1);

		$query = $this->db->get();
		$result=$query->result_array();
		/* foreach ($result as $row) {
			$data[] = $row;
		}
		return $result;
    }*/
    public function gettrainin_g()
    {
    	//$stack = array("orange", "banana");
    	$this->db->select('*');
    	$this->db->from('training');
    	//$this->db->join('category_details', 'category_details.prod_id = product_management.product_id');
    	$this->db->order_by("id", "desc");
    	$query = $this->db->get();
    	$result=$query->result_array();
    	foreach ($result as $row) {
    		$data[] = $row;
    	}
    	return $result;
    }
    public function gettrainin_content($prod_id_1)
    {
    	//$stack = array("orange", "banana");
    	$this->db->select('*');
    	$this->db->from('training_link');
    	$this->db->where("training_id", $prod_id_1);
    	$query = $this->db->get();
    	$result=$query->result_array();
    	/* foreach ($result as $row) {
    	 $data[] = $row;
    	 } */
    	return $result;
    }
    public function productid_check(){
     $this->db->select('*');
     $this->db->from('product_management');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     $id = $query->result_array();
      if(!$id==""){
        return $id[0]['id'];
      }else{
        return 0;
      }
    }
    public function insertproduct($data)
    {
    $this->db->insert('product_management', $data);
    return true;
    }
    public function insert_imageforspare($data,$scode)
	{
		$this->db->where('spare_code',$scode);
		$this->db->update('spare',$data);
		return true;

	}
    public function getdetails_product($data)
    {
    $this->db->select('*');
    $this->db->from('product_management');
    $this->db->where('id',$data);
      //$this->db->order_by("id", "desc");
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
      else {return NULL;}
    }
    public function edit_product($data,$id)
    {
    $this->db->where('id',$id);
    $this->db->update('product_management', $data);
    return true;
    }
    public function del_prod($id){
      $this->db->where('prod_id',$id);
      $this->db->delete('category_details');
      return true; 
    }
    public function deleteproduct($id)
    {
      $this->db->where('product_id',$id);
      $this->db->delete('product_management');
      return true;
    }
	public function get_subproduct_details($id){
		$this->db->select('*');
		$this->db->from('category_details');
		$this->db->where('prod_id',$id);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return 1;
		}
	}
    public function getsubproduct($datas)
    {
     $this->db->select('*,category_details.id as a');
     $this->db->from('category_details');
     $this->db->join('product_management', 'product_management.product_id = category_details.prod_id');
	 $this->db->where('category_details.company_id',$datas);
     $this->db->order_by("category_details.id", "desc");
     $query = $this->db->get();
     return $query;
    }
    public function subproductid_check(){
     $this->db->select('*');
     $this->db->from('category_details');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     $id = $query->result_array();
      if(!$id==""){
        return $id[0]['id'];
      }else{
        return 0;
      }
    }
    public function insertsubproduct($data)
    {
    $this->db->insert('category_details', $data);
    return true;
    }
    public function getdetails_subproduct($data)
    {
    $this->db->select('*');
    $this->db->from('category_details');
    $this->db->where('id',$data);
      //$this->db->order_by("id", "desc");
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
      else {return NULL;}
    }
    public function getproducts()
    {
     $this->db->select('*');
     $this->db->from('product_management');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     return $query;
    }
    public function edit_subproducts($data,$id)
    {
    $this->db->where('id',$id);
    $this->db->update('category_details', $data);
    return true;
    }
    public function deletesubproduct($id)
    {
    $this->db->where('id',$id);
    $this->db->delete('category_details');
    return true;
    }
    public function getspare($data)
    {
      $this->db->select('*,spare.id as a');
      $this->db->from('spare');
      $this->db->join('product_management', 'product_management.product_id = spare.product');
      $this->db->join('category_details', 'category_details.cat_id = spare.category');
      $this->db->where("spare.company_id", $data);
      $this->db->order_by("a", "desc");
      $query = $this->db->get();
      return $query;
    }
    public function spareid_check(){
     $this->db->select('*');
     $this->db->from('spare');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     $id = $query->result_array();
      if(!$id==""){
        return $id[0]['id'];
      }else{
        return 0;
      }
    }
     public function getcategory($data){
       $this->db->select('*');
     $this->db->from('category_details');
       $this->db->where("prod_id", $data);
     $query = $this->db->get();
     if ($query->num_rows() > 0)
      {
        return $query->result_array();
      }
        else {return 1;}
     }
    public function insertspare($data)
    {
    $this->db->insert('spare', $data);
    return true;
    }
    public function getdetails_spare($data)
    {
    $this->db->select('*');
    $this->db->from('spare');
    $this->db->where('id',$data);
      //$this->db->order_by("id", "desc");
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
      else {return NULL;}
    }
    public function edit_spare($data,$id1)
    {
    $this->db->where('id',$id1);
    $this->db->update('spare', $data);
      return true;
    }

    public function fetch_spareinfo($rid,$sid)
	{
         $where=array('id'=>$rid,
					  'spare_id'=>$sid
					 );
		  $this->db->select("price,quantity_purchased");
          $this->db->from('spare');
          $this->db->where($where);
          $query=$this->db->get();
          $ans= $query->row_array();
          return $ans;
   }
    public function deletespare($id)
    {
    $this->db->where('id',$id);
    $this->db->delete('spare');
    return true;
    }
    public function getattendances()
    {
     $this->db->select('technician_id,date');
     $this->db->from('attendance');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     return $query->result();
    }
    public function getdate($year,$month)
    {
     $where= array('YEAR(date)' => $year, 'MONTH(date)' => $month);
     $this->db->select('technician_id');
     $this->db->from('attendance');
     $this->db->where($where);
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     //return $query->result();
     $d=cal_days_in_month(CAL_GREGORIAN,$month,$year);
     if ($query->num_rows() > 0)
     {
      $count = $query->num_rows();
      $count = ($d / 100) * $count;
      $value = $query->result();
      return array(
        'records' => $value,
          'count' => count($value),
      );
     }
      else {return NULL;}
    }
    public function getcertificate($datas){
  		$this->db->select('*,certificate.id as a');
  		$this->db->from('certificate');
  		/*$this->db->join('product_management', 'product_management.product_id = certificate.product_id');
  		$this->db->join('category_details', 'category_details.cat_id = certificate.cat_id');*/
  		$this->db->where('certificate.certificate_type != ','product');
      $this->db->where('certificate.company_id',$datas);
  		$this->db->order_by("a", "desc");
  		$query = $this->db->get();
  		//$query = $query->result_array();
  		return $query;
    }

    /*Check the assesment is already exist or not based on certificate id */
    public function certificate_check(){
     $this->db->select('*');
     $this->db->from('certificate');
     $this->db->order_by("id", "desc");
     $query = $this->db->get();
     $id = $query->result_array();
     if(!$id==""){
      return $id[0]['id'];
     }else{
      return 0;
     }
    }

    
  public function insertcertificate($data){
    $this->db->insert('certificate',$data);
    return true;
  }
  public function getdetails_certificate_s($id){
    $this->db->select('*,certificate.id as a');
    $this->db->from('certificate');
    $this->db->join('product_management', 'product_management.product_id = certificate.product_id');
    $this->db->join('category_details', 'category_details.cat_id = certificate.cat_id');
    $this->db->where('certificate.id',$id);
    $query = $this->db->get();
    return $query->row_array();
  }
  public function edits_certificates($id){
  	$this->db->select('*,certificate.id as a');
  	$this->db->from('certificate');
    $this->db->where('certificate.id',$id);
  	$query = $this->db->get();
        $query = $query->row_array();
        if($query['certificate_type']=='product'){
          $this->db->select('*,certificate.id as a');
          $this->db->from('certificate');
          $this->db->join('product_management', 'product_management.product_id = certificate.product_id');
          $this->db->where('certificate.id',$id);
          $query = $this->db->get();
        	$query = $query->row_array();
			return $query;
		}
	   else
	   {
          $this->db->select('*,certificate.id as a');
          $this->db->from('certificate');
		  $this->db->where('certificate.id',$id);
          $query = $this->db->get();
          $query = $query->row_array();
		   return $query;
		}
		
    }  	
  
  public function updatecertificate($data,$id){
    $this->db->where('id',$id);
    $this->db->update('certificate',$data);
    return true;
  }
  public function deletecertificate($id){
    $this->db->where('id',$id);
    $this->db->delete('certificate');
    return true;
  }
  public function gettraining($datas){
	/*$result=array();
    $this->db->select('*,training.id as a,training.title as b');
    $this->db->from('training');
    $this->db->join('product_management', 'product_management.product_id = training.product');
    $this->db->join('category_details', 'category_details.cat_id = training.category');
    //$this->db->join('certificate', 'certificate.certificate_id = training.certificate_id');
    $this->db->where('product_type','technology');
    $this->db->order_by("training.id", "desc");
    $query = $this->db->get();
    array_push($result,array('training_prod'=>$query->result_array()));*/
    $where = '(training.product_type="generic" or training.product_type = "technology")';
    $this->db->select('*,training.training_id as a, training.id as b,training_link.id as c');
    $this->db->from('training');
    $this->db->order_by("training.id", "desc");
    $this->db->join('training_link', 'training_link.training_id = training.training_id');
    $this->db->where($where);
    $this->db->where('company_id',$datas);
    $query1 = $this->db->get();
    return $query1;
    /*array_push($result,array('training'=>$query1->result_array()));
    return json_encode($result,true);*/
  }
   public function gettraining_1($datas){
    $this->db->select('*,training.id as a,training.training_id as b,training_link.id as c');
    $this->db->from('training');
    $this->db->join('product_management', 'product_management.product_id = training.product');
    $this->db->join('category_details', 'category_details.cat_id = training.category');
    $this->db->join('training_link', 'training_link.training_id = training.training_id');
    $this->db->where('training.product_type','product');
    $this->db->where('training.company_id',$datas);
    $this->db->order_by("a", "desc");
    $query = $this->db->get();
    //$query = $query->result_array();
    return $query;
    }
  public function getcertificate_1($datas){
    $this->db->select('*,certificate.id as a');
    $this->db->from('certificate');
    $this->db->join('product_management', 'product_management.product_id = certificate.product_id');
    $this->db->join('category_details', 'category_details.cat_id = certificate.cat_id');
    $this->db->where('certificate.certificate_type','product');
    $this->db->where('certificate.company_id',$datas);
    $this->db->order_by("a", "desc");
    $query = $this->db->get();
    //$query = $query->result_array();
    return $query;
    }
  public function training_check(){
     $this->db->select('*');
     $this->db->from('training');
       $this->db->order_by("id", "desc");
     $query = $this->db->get();
     $id = $query->result_array();
     if(!$id==""){
      return $id[0]['id'];
     }else{
      return 0;
     }
  }
  public function edit_training($data,$training_ids1)
  {
    $this->db->where('training_id',$training_ids1);
    $this->db->update('training', $data);
    return true;
  }
  public function deletetraining($id){
    $this->db->where('training.training_id',$id);
    $this->db->delete('training');
    return true;
  }
  public function getcertificates($product,$category){
    $where=array(
      'product_id' => $product, 'cat_id' => $category
     );
    $this->db->select('*');
    $this->db->from('certificate');
    $this->db->where($where);
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->result_array();
    }
      else {return 1;}
	}
	public function insert_traini($data){
		$this->db->insert('training',$data);
		return true;
	}
	public function edit_traini($data,$id){
		$this->db->where('training.training_id',$id);
		$this->db->update('training',$data);
		return true;
	}
  public function inserttraining($data,$type){
    $this->db->insert('training', $data);
    $training=array(
      'training_id'=>$data['training_id'],
      'title'=>$data['title'],
      'link'=>$data['video_link'],
      'type'=>$type,
    );
    $this->training($training);
  }
  public function training($training){
    $this->db->insert('training_link', $training);
    echo '1';
  }
  public function inserttraining_content($datas){
	$this->db->insert('training_link', $datas);
	return true;
  }
  public function edittraining_content($datas,$id){
  	$this->db->where('training_id',$id);
  	$this->db->update('training_link', $datas);
  	return true;
  }
  public function getquiz($datas){
    $this->db->select('*, quiz.id as a');
    $this->db->from('quiz');
    $this->db->join('certificate', 'certificate.certificate_id = quiz.certificate_id');
    $this->db->where('certificate.company_id',$datas);
    $this->db->order_by('a', "desc");
    $query = $this->db->get();
    //$query = $query->result_array();
    return $query;
  }
  /* public function getquiz(){
    $this->db->select('*,quiz.id as a');
    $this->db->from('quiz');
    $this->db->join('training', 'training.training_id = quiz.training_id');
    $query = $this->db->get();
    //$query1 =$query->result_array();
    return $query1;
  }  */
  public function quiz_check(){
    $this->db->select('*');
    $this->db->from('quiz');
      $this->db->order_by("id", "desc");
    $query = $this->db->get();
    $id = $query->result_array();
    if(!$id==""){
      return $id[0]['id'];
    }else{
      return 0;
    }
  }

  public function question_check($question,$certificate_id,$c_id){
    $where=array(
		'certificate_id'=>$certificate_id,'question'=>$question
    );
    $this->db->select('*');
    $this->db->from('quiz');
    $this->db->where($where);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return 0;
    }else{
     
     return TRUE;
    }
  }
  public function getdetails_training($id)
  {
    $this->db->select('*,training.id as a,training.title as b');
    $this->db->from('training');
    $this->db->join('training_link', 'training_link.training_id = training.training_id');
    $this->db->where('training.training_id', $id);    
	  $this->db->group_by('training.training_id');
    $query = $this->db->get();
	//$query=$query->row_array();
    if ($query->num_rows() > 0)
    {
		$query=$query->row_array();
		if($query['product_type']=="generic" || $query['product_type']=="technology"){
			return $query;
		}else{
			$this->db->select('*,training.id as a,training.title as b');
			$this->db->from('training');
			$this->db->join('product_management', 'product_management.product_id = training.product');
			$this->db->join('category_details', 'category_details.cat_id = training.category');
      $this->db->join('training_link', 'training_link.training_id = training.training_id');
			$this->db->where('training.training_id', $id);
	  $this->db->group_by('training.training_id');
			$querys = $this->db->get();
			$querys=$querys->row_array();
			return $querys;
		}
    }
      else {return NULL;}
  }
  public function get_training_details($id)
  {
    $this->db->select('*');
    $this->db->from('training_link');
    $this->db->where('training_id', $id);
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
		return $query->result_array();
    }
      else {return NULL;}
  }
  public function getdetails_train_content($id)
  {
  	$this->db->select('*');
  	$this->db->from('training_link');
  	$this->db->where('id', $id);
  	$query = $this->db->get();
  	if ($query->num_rows() > 0)
  	{
  		return $query->row_array();
  	}
  	else {return NULL;}
  }
  public function gettrainings($datas){
    $this->db->select('*');
    $this->db->from('training');
    $this->db->where('company_id',$datas);
    $this->db->group_by('training_id');    
    $query=$this->db->get();
    //$query=$query->row_array();
    return $query;
  }
  public function gettrainings_quiz($datas){
    $this->db->select('*');
    $this->db->from('training');
	$this->db->where('company_id',$datas);
    $this->db->group_by('training_id');
    $query=$this->db->get();
    //$query=$query->row_array();
    return $query;
  }
  public function addquiz($data){
    $this->db->insert('quiz',$data);
    return true;
  }
  public function getdetails_quiz($id){
    $this->db->select('*');
    $this->db->from('quiz');
    //$this->db->join('training x', 'a.training_id = x.training_id');
    $this->db->where('id',$id);
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
      else {return NULL;}
  }
  public function check_assesment($id){
  	$this->db->select('*');
  	$this->db->from('certificate');
  	//$this->db->join('training x', 'a.training_id = x.training_id');
  	$this->db->where('id',$id);
  	$query = $this->db->get();
  	if ($query->num_rows() > 0)
  	{
  		return $query->row_array();
  	}
  	else {return NULL;}
  }
  public function updatequiz($data,$id1){
    $this->db->where('id',$id1);
    $this->db->update('quiz',$data);
    return true;
  }
  public function deletequiz($id){
    $this->db->where('id',$id);
    $this->db->delete('quiz');
    return true;
  }
  public function delete_training($id){
    $this->db->where('training_link.training_id',$id);
    $this->db->delete('training_link');
    return true;
  }
  public function usernumber_check($role,$contact_no,$c_name,$c_id){
    $where=array(
      'contact_number'=>$contact_no,
    );
    $this->db->select('company_id,contact_number');
    $this->db->from('technician');
    $this->db->where($where);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return true;
    }else{
      $this->db->select('company_id,contact_number');
      $this->db->from('user');
      $this->db->where($where);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return true;
      }else{
        return NULL;
      }
    }
    /*if($role=="Technician"){
      $this->db->select('contact_number');
      $this->db->from('technician');
      $this->db->where('contact_number',$contact_no);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return true;
      }else{
        return NULL;
      }
    }else{
      $this->db->select('contact_number');
      $this->db->from('user');
      $this->db->where('contact_number',$contact_no);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return true;
      }else{
        return NULL;
      }
    }*/

  }
  public function empid_check($c_id,$emp_id){
    $where=array(
		'employee_id'=>$emp_id,'company_id'=>$c_id
    );
    $this->db->select('*');
    $this->db->from('technician');
    $this->db->where($where);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return TRUE;
    }else{
      $this->db->select('company_id,employee_id');
      $this->db->from('user');
      $this->db->where($where);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return TRUE;
      }else{
        return NULL;
      }
    }
  }
  public function useremail_check($role,$email_id,$c_name,$c_id){
    $where=array(
      'email_id'=>$email_id,
    );
    $this->db->select('company_id,email_id');
    $this->db->from('technician');
    $this->db->where($where);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return true;
    }else{
      $this->db->select('company_id,email_id');
      $this->db->from('user');
      $this->db->where($where);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return true;
      }else{
        return NULL;
      }
    }
    /*if($role=="Technician"){
      $this->db->select('email_id');
      $this->db->from('technician');
      $this->db->where('email_id',$email_id);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return true;
      }else{
        return NULL;
      }
    }else{
      $this->db->select('email_id');
      $this->db->from('user');
      $this->db->where('email_id',$email_id);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return true;
      }else{
        return NULL;
      }

    }*/
  }
  public function editemail_check($email_id,$emp_id){
    $where = array('employee_id !=' => $emp_id, 'email_id' => $email_id);
    $this->db->select('employee_id,email_id');
    $this->db->from('user');
    $this->db->where($where);
    $query=$this->db->get();
    /*$query = $query->result_array();
    return $query;*/
    if($query->num_rows()>0){
      return true;
    }else{
      $this->db->select('employee_id,email_id');
      $this->db->from('technician');
      $this->db->where($where);
      $query=$this->db->get();
      /*$query = $query->result_array();
      return $query;*/
      if($query->num_rows()>0){
        return true;
      }else{
        return NULL;
      }
    }

  }
  public function editphone_check($contact_no,$emp_id){
    $where = array('employee_id !=' => $emp_id, 'contact_number' => $contact_no);
    $this->db->select('employee_id,contact_number');
    $this->db->from('user');
    $this->db->where($where);
    $query=$this->db->get();
    /*$query = $query->result_array();
    return $query;*/
    if($query->num_rows()>0){
      return true;
    }else{
      $this->db->select('employee_id,contact_number');
      $this->db->from('technician');
      $this->db->where($where);
      $query=$this->db->get();
      /*$query = $query->result_array();
      return $query;*/
      if($query->num_rows()>0){
        return true;
      }else{
        return NULL;
      }
    }

  }
  public function view_technician_image($id){
    $this->db->select('image');
    $this->db->from('technician');
    $this->db->where('id',$id);
    $query=$this->db->get();
    $query = $query->row_array();
    return $query;
  }
  public function view_training_images($id){
    $this->db->select('*');
    $this->db->from('training');
    $this->db->where('id',$id);
    $query=$this->db->get();
    $query = $query->row_array();
    return $query;
  }
  public function view_spare_images($id){
    $this->db->select('id,image');
    $this->db->from('spare');
    $this->db->where('spare_code',$id);
    $query=$this->db->get();
    $query = $query->row_array();
    return $query;
  }
  public function view_category_images($id){
    $this->db->select('cat_image');
    $this->db->from('category_details');
    $this->db->where('id',$id);
    $query=$this->db->get();
    $query = $query->row_array();
    return $query;
  }
  public function view_product_images($id){
    $this->db->select('product_image');
    $this->db->from('product_management');
    $this->db->where('id',$id);
    $query=$this->db->get();
    $query = $query->row_array();
    return $query;
  }
  public function mobile_check_tech($contact_no){
    $this->db->select('contact_number');
    $this->db->from('technician');
    $this->db->where('contact_number',$contact_no);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return true;
    }else{
      return NULL;
    }

  }
  public function email_check_tech($email_id){
    $this->db->select('email_id');
    $this->db->from('technician');
    $this->db->where('email_id',$email_id);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return true;
    }else{
      return NULL;
    }

  }
  public function editemail_check_tech($email_id,$id){
    $where = array('id !=' => $id, 'email_id' => $email_id);
    $this->db->select('id,email_id');
    $this->db->from('technician');
    $this->db->where($where);
    $query=$this->db->get();
    /*$query = $query->result_array();
    return $query;*/
    if($query->num_rows()>0){
      return true;
    }else{
      return NULL;
    }
  }
  public function editphone_check_tech($contact_no,$id){
    $where = array('id !=' => $id, 'contact_number' => $contact_no);
    $this->db->select('id,contact_number');
    $this->db->from('technician');
    $this->db->where($where);
    $query=$this->db->get();
    /*$query = $query->result_array();
    return $query;*/
    if($query->num_rows()>0){
      return true;
    }else{
      return NULL;
    }
  }
  public function mobile_check_customer($contact_no,$companyid){
    $this->db->select('contact_number');
    $this->db->from('customer');
    $this->db->where('contact_number',$contact_no);
    $this->db->where('company_id',$companyid);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return true;
    }else{
      return NULL;
    }

  }
  public function email_check_customer($email_id,$companyid){
    $this->db->select('email_id');
    $this->db->from('customer');
    $this->db->where('email_id',$email_id);
    $this->db->where('company_id',$companyid);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return true;
    }else{
      return NULL;
    }

  }
  public function editemail_check_customer($email_id,$id){
    $where = array('id !=' => $id, 'email_id' => $email_id);
    $this->db->select('id,email_id');
    $this->db->from('customer');
    $this->db->where($where);
    $query=$this->db->get();
    /*$query = $query->result_array();
    return $query;*/
    if($query->num_rows()>0){
      return true;
    }else{
      return NULL;
    }
  }
  public function editphone_check_customer($contact_no,$id){
    $where = array('id !=' => $id, 'contact_number' => $contact_no);
    $this->db->select('id,contact_number');
    $this->db->from('customer');
    $this->db->where($where);
    $query=$this->db->get();
    /*$query = $query->result_array();
    return $query;*/
    if($query->num_rows()>0){
      return true;
    }else{
      return NULL;
    }
  }
  public function getdetails_technicians($data)
    {
    $this->db->select('*');
    $this->db->from('technician');
    $this->db->where('technician_id',$data);
      //$this->db->order_by("id", "desc");
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
      else {return NULL;}
    }
  public function data_category($id){
    $this->db->select('*');
    $this->db->from('category_details');
    $this->db->where('cat_id',$id);
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
      else {return NULL;}
  }
    function sla_get($company_id)
    {
        $this->db->select('*,sla_combination.id as a,sla_combination.priority_level as b,sla_combination.ref_id as c,sla_mapping.last_update as d');
        $this->db->from('sla_combination');
        $this->db->join('sla_mapping', 'sla_mapping.ref_id = sla_combination.ref_id');
        $this->db->join('product_management', 'product_management.product_id = sla_combination.product or sla_combination.product ="all"');
        $this->db->join('category_details', 'category_details.cat_id = sla_combination.category or sla_combination.category="all"');
        $this->db->join('company', 'company.company_id = sla_combination.company_id');
        $this->db->where('company.company_id', $company_id);
        $this->db->order_by('a', 'desc');
        $this->db->group_by('sla_combination.ref_id', 'desc');
        $query = $this->db->get();
	
        return $query;
    }
public function get_details_company($company_id){
		$resu= array();
		$this->db->select('company.company_id,company.company_logo,company_name');
        $this->db->from('company,sla_combination');
		$this->db->where('company.company_id=sla_combination.company_id ');
		$this->db->where('company.company_id',$company_id);
        $this->db->group_by("company.company_id ");
        $query=$this->db->get();
        $res= $query->result_array();
		if(!empty($res))
		{
			foreach($res as $r)
			{
				$this->db->select('company.company_id,company.company_logo,company_name');
				$this->db->from('company,sla_combination');
				$this->db->where('(sla_combination.product="all" and sla_combination.category="all"  and sla_combination.priority_level="all"  and sla_combination.company_id="'.$r['company_id'].'" ) ');
		$this->db->where('company.company_id',$company_id);
				$this->db->group_by("company.company_id ");
				$query=$this->db->get();
				$re=$query->result_array();
				if(!empty($re))
				{
					foreach($re as $r1)
					{
						$this->db->select('company.company_id,company.company_logo,company_name');
						$this->db->from('company');
						$this->db->where('company.company_id!=',$r1['company_id']);
		$this->db->where('company.company_id',$company_id);
						$this->db->group_by("company.company_id ");
						$query=$this->db->get();
						array_push($resu,$query->result_array());
					}
				}
				else
				{
$this->db->select('company.company_id,company.company_logo,company_name');
				$this->db->from('company,sla_combination');
				$this->db->where('(sla_combination.product="all" and sla_combination.category="all"   and sla_combination.priority_level="all"  and  sla_combination.company_id="'.$r['company_id'].'" ) ');
		$this->db->where('company.company_id',$company_id);
				$this->db->group_by("company.company_id ");
				$query=$this->db->get();
				$re=$query->result_array();
					if(count($re)<4)
					{
					$this->db->select('company.company_id,company.company_logo,company_name');
					$this->db->from('company,sla_combination');
					$this->db->where('sla_combination.company_id',$r['company_id']);
		$this->db->where('company.company_id',$company_id);
					$this->db->group_by("company.company_id ");
					$query=$this->db->get();
					array_push($resu,$query->result_array());
					}
					else
					{
						$this->db->select('company.company_id,company.company_logo,company_name');
						$this->db->from('company');
						$this->db->where('company.company_id!=',$r['company_id']);
		$this->db->where('company.company_id',$company_id);
						$this->db->group_by("company.company_id ");
						$query=$this->db->get();
						array_push($resu,$query->result_array());
					}
				}
			}
		}
		else
		{
			$this->db->select('company.company_id,company.company_logo,company_name');
			$this->db->from('company');
		$this->db->where('company.company_id',$company_id);
			$this->db->group_by("company.company_id ");
			$query=$this->db->get();
			array_push($resu,$query->result_array());
		}
		return $resu[0];
    }
 public function add_score($data){
    $this->db->insert('score_update',$data);
    return true;
      }
     public function sla_adh($data){
    $this->db->insert('priority',$data);
    return true;
      }
     public function add_contracts($data){
      $this->db->insert('amc_type',$data);
     return true;
      }
      public function edit_contracts($data,$id){
        $this->db->where('id',$id);
        $this->db->update('amc_type',$data);
        return true;
      }
      public function amc_check($amc_type,$datas){
        $where=array(
          'amc_type'=>$amc_type,
          'company_id'=>$datas,
        );
        $this->db->select('*');
        $this->db->from('amc_type');
        $this->db->where($where);
        $query=$this->db->get();
        if ($query->num_rows() > 0)
    {
      return $query->row_array();
    }
      else {return NULL;}
    }
public function deletecontract($id){
$this->db->where('id',$id);
    $this->db->delete('amc_type');
    return true;
}
  public function edit_amctype($id){
    $where=array(
          'id'=>$id,
        );
        $this->db->select('*');
        $this->db->from('amc_type');
        $this->db->where($where);
        $query=$this->db->get();
        if ($query->num_rows() > 0)
    {
      return $query->result_array();
    }
      else {return NULL;}
  }
  public function add_price($data){
    $this->db->insert('amc_price',$data);
    return true;
      }
    public function edit_sla($data,$priority_level)
    {
    $this->db->where('priority_level',$priority_level);
    $this->db->update('priority',$data);
    return true;
    }
     public function disply_inventory()
    {
        $company_id = $this->session->userdata('companyid');
      $where = array(
          "category_details.company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('category_details');
        $this->db->where($where);
        $query = $this->db->get();

        return $query;
    }
	public function disply($datas)
    {
      	$this->db->select('*,sla_combination.id as a,sla_combination.priority_level as b,sla_combination.ref_id as c');
        $this->db->from('sla_combination');
        $this->db->join('sla_mapping', 'sla_mapping.company_id = sla_combination.company_id AND sla_combination.ref_id = sla_mapping.ref_id');
        $this->db->join('product_management', 'product_management.product_id = sla_combination.product');
        $this->db->join('category_details', 'category_details.cat_id = sla_combination.category');
        $this->db->join('company', 'company.company_id = sla_combination.company_id');
      	$this->db->where('sla_combination.company_id',$datas);
        $query = $this->db->get();
        return $query;
    }
public function priority_level(){
		$this->db->select('*');
		$this->db->from('priority');
		$query=$this->db->get();
		return $query;
	}
	public function disply_inven($datas)
    {
      $where = array(
          "amc_type.company_id" => $datas
        );
        $this->db->select('*,last_update as last_update');
        $this->db->from('amc_type');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    } public function getdetails_sla($id)
    {
    $where=array('sla_mapping.id'=>$id);
        $this->db->select('*,sla_mapping.id as a,sla_mapping.priority_level as priority');
        $this->db->from('sla_mapping');
        $this->db->join('sla_combination', 'sla_combination.company_id = sla_mapping.company_id AND sla_combination.ref_id = sla_mapping.ref_id');
        //$this->db->join('sla_combination','sla_combination.company_id = sla_mapping.company_id');
       $this->db->where($where);
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      return $query->result_array();
    }
      else {return NULL;}
    }
    public function checking($data1,$ref_id,$company_id){
    $where=array(
    'product'=>$data1['product'],
    'category'=>$data1['category'],
                   'cust_category'=>$data1['cust_category'],
                   'service_category'=>$data1['service_category'],
                   'call_category'=>$data1['call_category'],
                   'company_id'=>$company_id,
    );
    $this->db->select('*');
    $this->db->from('sla_combination');
    $this->db->where($where);
    $this->db->where_not_in('ref_id',$ref_id);
    $query=$this->db->get();
    if($query->num_rows()>0){
    return $query->result_array();
    }else{
    return NULL;
    }
  }
  public function update_sla($data,$ref_id,$company_id){
    $where=array('ref_id'=>$ref_id,'company_id'=>$company_id);
    $this->db->where($where);
    $this->db->update('sla_mapping',$data);
    return true;
  }
  public function update_sla1($data1,$ref_id,$company_id){
    $where=array('ref_id'=>$ref_id,'company_id'=>$company_id);
    $this->db->where($where);
    $this->db->update('sla_combination',$data1);
    return true;
  }
     public function deletesla($id)
    {
    $this->db->where('id',$id);
    $this->db->delete('priority');
    return true;
    }
    public function check_company($id)
    {
    $this->db->select('*');
    $this->db->from('score_update');
    $this->db->where('company_id',$id);
    $query = $this->db->get();
    $count=$query->num_rows();
    return $count;
    }
    public function update_score($data,$company_id)
    {
    $this->db->where('company_id',$company_id);
    $this->db->update('score_update',$data);
    return true;
    }

    public function password_check($id,$usernames,$old_pass){
        $where=array(
            'id'=>$id,
            'username'=>$usernames,
          );
      $this->db->select('password');
      $this->db->from('login');
      $this->db->where($where);
      $query=$this->db->get();
      if ($query->num_rows() > 0)
      {
        $res = $query->row_array();
        $db_password = $this->encrypt->decode($res['password']);
        if($db_password==$old_pass){
          return TRUE;
        }else{
          return NULL;
        }
      }
        else {return NULL;}
    }
    public function password_change($id,$values){
      $this->db->where('id', $id);
      $this->db->update('login',$values);
      return true;
    }
	public function login_user($data){
	  $where=array('username'=>$data['username'],'companyid'=>$data['companyid']);
	  $this->db->where($where);
	  $this->db->update('login',$data);
	  return true;
	}
	public function getcompany_s($datas){
		$data;
		$this->db->select('*, admin_user.company_id as a, admin_user.company_name as b');
		$this->db->from('admin_user');
		$this->db->join('company', 'company.company_id = admin_user.company_id');
		$this->db->where('admin_user.company_id',$datas);
		$this->db->order_by('admin_user.last_update','desc');
		$this->db->limit(1);
		$query = $this->db->get();
		$result=$query->result_array();
		foreach ($result as $row) {
			$data[] = $row;
		}
		return $data;
	}
	public function getcompany_ss(){
		$this->db->select('*, admin_user.company_id as a, admin_user.company_name as b');
		$this->db->from('admin_user');
		$this->db->join('company', 'company.company_id = admin_user.company_id');
		$query = $this->db->get();
		$result=$query;
		return $result;
	}
	public function getproduct_s($company)
	{
		//$stack = array("orange", "banana");
		$this->db->select('*,product_management.id as a');
		$this->db->from('product_management');
		//$this->db->join('category_details', 'category_details.prod_id = product_management.product_id');
		$this->db->order_by("a", "desc");
		$this->db->where("company_id", $company);
		$query = $this->db->get();
		$result=$query->result_array();
		return $result;
	}
	public function getcategory_s($prod_id_1)
	{
		//$stack = array("orange", "banana");
		$this->db->select('*');
		$this->db->from('category_details');
		//$this->db->join('category_details', 'category_details.prod_id = product_management.product_id');
		$this->db->where("prod_id", $prod_id_1);

		$query = $this->db->get();
		$result=$query->result_array();
		/* foreach ($result as $row) {
		 $data[] = $row;
		 } */
		return $result;
	}
	public function get_quiz_s($id){
		$this->db->select('*');
		$this->db->from('quiz');
		$this->db->where('certificate_id',$id);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function updated_quizes($data_assessment,$id){
		$this->db->where('id',$id);
		$this->db->update('certificate', $data_assessment);
		return true;
	}
	public function delete_certificate_id($assess_id){
		$datas=array('certificate_id'=>'');
		$this->db->where('certificate_id',$assess_id);
		$this->db->update('training',$datas);
	}
	public function insert_certificate_id($assess_id,$selecteds){
		$where=array(
			'training_id'=>$selecteds,
		);
		$data=array('certificate_id'=>$assess_id);
		$this->db->where($where);
		$this->db->update('training',$data);
	}
	public function get_refnumber($parent){
		$this->db->select('editable_rights');
		$this->db->from('sla_mapping');
		$this->db->where('company_id',$parent);
		$query=$this->db->get();
		return $query->row_array();
	}
	public function update_slac($data_combination,$data_mapping,$company_id){
		$this->db->where('company_id',$company_id);
		$this->db->update('sla_combination',$data_combination);
		return $this->update_slam($data_mapping,$company_id);
	}
	public function update_slam($data_mapping,$company_id){
		$this->db->where('company_id',$company_id);
		$this->db->update('sla_mapping',$data_mapping);
		return true;
	}
	public function get_training_s($id){
		$this->db->select('*');
		$this->db->from('training');
		$this->db->where('certificate_id',$id);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function product_check($product,$c_id){
		$query = $this->db->select('*')
             ->from('product_management')
             ->where('company_id',$c_id)
             ->like('product_name',$product,'after')
             ->get();
            if($query->num_rows()>0){
                    return $query->row_array();
            }else{
                    return NULL;
            }
	}
	public function subcategory_check($prodct_check,$category,$c_id){
		$where=array('company_id'=>$c_id,'prod_id'=>$prodct_check['product_id']);
		$query = $this->db->select('*')
             ->from('category_details')
             ->where($where)
             ->like('cat_name',$category,'after')
             ->get();
			if($query->num_rows()>0){
				return $query->row_array();
			}else{
				return NULL;
			}
	}
	public function set_unblock($data,$admin_id){
            $this->db->where('admin_id',$admin_id);
            $this->db->update('admin_user',$data);
            return true;
	}
	public function renew_info($update,$id)
	{
		$this->db->where('admin_id',$id);
        $this->db->update('admin_user',$update);
		return true;
	}

        public function getdetails_productcategory($id){
            $this->db->select('*');
            $this->db->from('technician');
            $this->db->join('product_management', 'product_management.product_id = technician.product');
            $this->db->join('category_details', 'category_details.cat_id = technician.category');
            $this->db->where('technician.technician_id',$id);
            $query=$this->db->get();
            if($query->num_rows()>0){
                return $query->result_array();
            }else{
                return null;
            }
        }
       
	    public function productsubproduct_view($id){
            $this->db->select('technician.id,technician.skill_level,technician.employee_id,technician.technician_id,product_management.product_id,product_management.product_name,category_details.cat_id,category_details.cat_name');
            $this->db->from('technician');
            $this->db->join('product_management', 'product_management.product_id = technician.product');
            $this->db->join('category_details', 'category_details.cat_id = technician.category');
            $this->db->where('technician.employee_id',$id);
            $query=$this->db->get();            
          
                return $query;
           
        }
         
	   
	   public function selected_training($id,$prod_id,$cat_id,$company_id){
          if($id=='product'){
            $where=array('training.product_type'=>$id,'product'=>$prod_id,'category'=>$cat_id,'company_id'=>$company_id);
            $this->db->select('*');
            $this->db->from('training');
            $this->db->where($where);
            $query=$this->db->get();
            if($query->num_rows()>0){
                return $query->result_array();
            }else{
                return null;
            }
          }else if($id=='generic') {
             $where=array('training.product_type'=>$id,'company_id'=>$company_id); 
             $this->db->select('*');
              $this->db->from('training');
              $this->db->where($where);
              $query=$this->db->get();
              if($query->num_rows()>0){
                  return $query->result_array();
              }else{
                  return null;
              }
          }else{
            $where=array('training.product_type'=>$id,'company_id'=>$company_id);
            $this->db->select('*');
            $this->db->from('training');
            $this->db->where($where);
            $query=$this->db->get();
            if($query->num_rows()>0){
                return $query->result_array();
            }else{
                return null;
            } 
          }
            
        }
        public function bulk_quiz($datas){
            $this->db->insert('quiz',$datas);
            return true;
        }
        public function check_count($emp_id){
            $this->db->select('*');
            $this->db->from('technician');
            $this->db->where('employee_id',$emp_id);
            $query=$this->db->get();
            return $query->num_rows();
        }
        public function insert_user($data)  ////////// raju
		{   
			//echo $data['email_id'];
			$this->db->insert('technician',$data);
		 $where=array('technician_id'=>$data['technician_id']);
           $this->db->select('email_id');
     $this->db->from('technician');
     $this->db->where($where);
     $query = $this->db->get();
     $user_email = $query->row_array();
	$email_id=$user_email['email_id'];
	 $where=array('username'=>$email_id);		
	 $datass=array('username'=>$data['email_id']);
	  $this->db->where($where);
     $this->db->update('login',$datass);	
          
            return true;
        }
        public function get_product_company1($company_id)
         {
            $this->db->select('*');
            $this->db->from('product_management');
            //$this->db->join('category_details', 'category_details.prod_id = product_management.product_id');
            $this->db->where('product_management.company_id', $company_id);
           $this->db->order_by("last_update",'desc');
           // $this->db->group_by("product_management.product_id");
            $query51 = $this->db->get();
         //print_r($query51);
//die();
            $r551    = $query51->result_array();
            return $r551;
         }
         public function get_subproduct_company1($prod_id)
         {
              $this->db->select('*');
              $this->db->from('category_details');
              $this->db->where('prod_id', $prod_id);
              $this->db->group_by("category_details.cat_id");
              $query = $this->db->get();
              $res   = $query->result_array();
              return $res;
         }
     public function exs_pdt($product_name,$company){
      $where=array('product_name'=>$product_name,'company_id'=>$company);
      $this->db->select('*');
      $this->db->from('product_management');
      $this->db->where($where);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return $query->row_array();
      }else{
        return NULL;
      }
     }
     public function exs_subpdt($product_name,$subproduct_name,$companyid){
      $where=array('prod_id'=>$product_name,'cat_name'=>$subproduct_name,'company_id'=>$companyid);
      $this->db->select('*');
      $this->db->from('category_details');
      $this->db->where($where);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return $query->row_array();
      }else{
        return NULL;
      }
     }
     public function gettraining_content($datas){
       $where=array('training.company_id'=>$datas);
        $this->db->select('*,training.id as a,training.training_id as b,training_link.id as c');
        $this->db->from('training_link');
        $this->db->join('training', 'training.training_id = training_link.training_id');
        $this->db->where($where);
        $this->db->order_by("a", "desc");
        $query = $this->db->get();
        return $query;
     }
     public function checksubcategory($name,$product_name,$companyid){
        $where=array('company_id'=>$companyid,'prod_id'=>$product_name,'cat_name'=>$name);
        $this->db->select('*');
        $this->db->from('category_details');
        $this->db->where($where);
        $query=$this->db->get();
        if($query->num_rows()>0){
          return 1;
        }else{
          return NULL;
        }
     }
     public function getdetails_content($id){
        $this->db->select('*,training_link.id as a');
        $this->db->from('training_link');
        $this->db->join('training', 'training.training_id = training_link.training_id');
        $this->db->where('training_link.id',$id);
        $query=$this->db->get();
        if($query->num_rows()>0){
          return $query->row_array();          
        }else{
          return NULL;
        }
     }
     public function get_view_trai($id){
        $this->db->select('link,type');
        $this->db->from('training_link');
        $this->db->where('id',$id);
        $query=$this->db->get();
        if($query->num_rows()>0){
          return $query->row_array();
        }else{
          return NULL;
        }
     }
     public function check_trai($name,$product_type,$company_id){
        $where=array('title'=>$name,'product_type'=>$product_type,'company_id'=>$company_id);
        $this->db->select('*');
        $this->db->from('training');
        $this->db->where($where);
        $query=$this->db->get();
        if($query->num_rows()>0){
          return 1;
        }else{
          return NULL;
        }
     }
     public function check_rights($id){
        $selected="";
        $where=array('company_id'=>$id);
        $this->db->select('ref_id');
        $this->db->from('sla_combination');
        $this->db->where($where);
        $query=$this->db->get();
        if($query->num_rows()>0){
          $query=$query->result_array();  
          foreach($query as $ques){
            $wheres=array('ref_id'=>$ques['ref_id']);
            $this->db->select('*');
            $this->db->from('sla_mapping');
            $this->db->where($wheres);
            $ques=$this->db->get();
            $selected[] = $ques->row_array();
          }          
        }else{
          $selected[] = false;
        }
        return $selected;
     }
     public function check_rights_1($company_id){
      $this->db->select('editable_rights');
      $this->db->from('sla_mapping');
      $this->db->where('company_id',$company_id);
      $query=$this->db->get();
      if($query->num_rows()>0){
        $query=$query->result_array();        
        if(in_array('no', array_column($query, 'editable_rights'))){
          return 1;
        }else{
          return 0;
        }
      }else{
        return 0;
      }
     }

     public function check_rights1($prod_id){
        $selected="";
        $where=array('product'=>$prod_id);
        $this->db->select('ref_id');
        $this->db->from('sla_combination');
        $this->db->where($where);
        $query=$this->db->get();
        if($query->num_rows()>0){
          $query=$query->result_array();  
          foreach($query as $ques){
            $wheres=array('ref_id'=>$ques['ref_id']);
            $this->db->select('*');
            $this->db->from('sla_mapping');
            $this->db->where($wheres);
            $ques=$this->db->get();
            $selected[] = $ques->row_array();
          }          
        }else{
          $selected[] = false;
        }
        return $selected;
     }
     public function check_rights_2($company_id){
      $this->db->select('editable_rights');
      $this->db->from('sla_mapping');
      $this->db->where('company_id',$company_id);
      $query=$this->db->get();
      if($query->num_rows()>0){
        $query=$query->result_array();        
        if(in_array('no', array_column($query, 'editable_rights'))){
          return 1;
        }else{
          return 0;
        }
      }else{
        return 0;
      }
     }

     public function check_rights2($prod_id){
        $selected="";
        $where=array('category'=>$prod_id);
        $this->db->select('ref_id');
        $this->db->from('sla_combination');
        $this->db->where($where);
        $query=$this->db->get();
        if($query->num_rows()>0){
          $query=$query->result_array();  
          foreach($query as $ques){
            $wheres=array('ref_id'=>$ques['ref_id']);
            $this->db->select('*');
            $this->db->from('sla_mapping');
            $this->db->where($wheres);
            $ques=$this->db->get();
            $selected[] = $ques->row_array();
          }          
        }else{
          $selected[] = false;
        }
        return $selected;
     }
     public function submit_edit_sla($company_id, $ref_id,$response_day,$response,$resolution_day,$resolution,$acceptance_day,$acceptance,$sla_compliance, $mttr)
     {
          $data_edit = array(
			  'response_day' => $response_day,						
            'response_time' => $response,
			'resolution_day' => $resolution_day,
			'resolution_time'=>$resolution,
			'acceptance_day' => $acceptance_day,
			'acceptance_time'=>$acceptance,
            'SLA_Compliance_Target' => $sla_compliance,
               'mttr_target' => $mttr
          );
		 
          //$this->db->where('company_id', $company_id);
          $this->db->where('ref_id', $ref_id);
          $this->db->update('sla_mapping', $data_edit);
          return true;
     }
	 
     public function check_prod($product){
        $this->db->select('product_id');
        $this->db->from('product_management');
        $this->db->where('LOWER(product_name)', strtolower($product));
        $datas=$this->db->get();
        if($datas->num_rows()>0){
          $datas=$datas->row_array();
          return $datas;
        }else{
          return 0;
        }
     }
      public function check_worktype($companyid,$worktype)
     {
     // $result=array();
      $this->db->select('service_group_id');
        $this->db->from('service_group');
      $wk_type=strtolower($worktype);
		  if($wk_type!='all')
		  {
		   $this->db->where('company_id',$companyid);
        $this->db->where('LOWER(service_group)',$wk_type);
		  }
		  else
		  {
        $this->db->where('LOWER(service_group)',$wk_type);
		  }
       
        $datas=$this->db->get();
        if($datas->num_rows()>0){
         $datas=$datas->row_array();
          return $datas;
        }else{
          return 0;
        }
     }
	   
	   public function check_tech_id($companyid,$empid)
     {
     // $result=array();
      $this->db->select('technician_id');
        $this->db->from('technician');
  	   $this->db->where('company_id',$companyid);
      $this->db->where('employee_id',$empid);
       
        $datas=$this->db->get();
        if($datas->num_rows()>0){
         $datas=$datas->row_array();
          return $datas;
        }else{
          return 0;
        }
     } 
	   
     public function check_produ($category,$check_prod){ 
		//return $check_prod;
		//die;
        $this->db->select('cat_id');
        $this->db->from('category_details');
        $this->db->where('LOWER(cat_name)', strtolower($category)); 
        $this->db->where('prod_id',$check_prod['product_id']);
        $datas=$this->db->get();
        if($datas->num_rows()>0){
          $datas=$datas->row_array();
          return $datas;
        }else{
          return 0;
        }
     }
	 
  public function check_slas($id,$prod_id){
    $selected = array();
    $where1=array('company_id'=>$id,'product'=>'all','category'=>'all');
    $where2=array('company_id'=>$id,'product'=>$prod_id,'category'=>'all');
    $where3=array('company_id'=>$id,'product'=>$prod_id,'category !='=>'all');
    $this->db->select('*');
    $this->db->from('sla_combination');
    $this->db->where($where1);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return 1;
    }else{
      $this->db->select('*');
      $this->db->from('sla_combination');
      $this->db->where($where3);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return 2;
      }else{
        $this->db->select('ref_id');
        $this->db->from('sla_combination');
        $this->db->where($where2);
        $query=$this->db->get();
        if($query->num_rows()>0){
          $query=$query->result_array();
          foreach($query as $ques){
            $this->db->select('*');
            $this->db->from('sla_mapping');
            $this->db->where('ref_id',$ques['ref_id']);
            $datass=$this->db->get();
            if($datass->num_rows()>0){
              $datass=$datass->row_array();
              $selected[] = $datass;
            }else{
              $selected[] = 0;
            }
          }
        }else{
          $selected[] = 0;
        }
      }     
    }
    return $selected;
  }
  public function check_slas1($id,$prod_id){
    $selected = '';
    $where1=array('company_id'=>$id,'product'=>'all','category'=>'all');  
    $this->db->select('*');
    $this->db->from('sla_combination');
    $this->db->where($where1);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return 1;
    }else{
      $this->db->select('prod_id');
      $this->db->from('category_details');
      $this->db->where('cat_id',$prod_id);
      $query=$this->db->get();
      if($query->num_rows()>0){
        $query=$query->row_array();
        foreach($query as $q){          
          $where1=array('company_id'=>$id,'product'=>$q,'category'=>'all');
          $this->db->select('ref_id');
          $this->db->from('sla_combination');
          $this->db->where($where1);
          $dat=$this->db->get();        
          if($dat->num_rows()>0){
            return 1;         
          }else{  
            $dat=$dat->result_array();
            $where2=array('company_id'=>$id,'product'=>$q,'category'=>$prod_id);
            $this->db->select('ref_id');
            $this->db->from('sla_combination');
            $this->db->where($where2);
            $datass=$this->db->get();
            if($datass->num_rows()>0){
              $datass=$datass->result_array();
              foreach($datass as $d){
                $this->db->select('*');
                $this->db->from('sla_mapping');
                $this->db->where('ref_id',$d['ref_id']);
                $datass=$this->db->get();
                if($datass->num_rows()>0){
                  $datass=$datass->row_array();
                  $selected[] = $datass;
                }else{
                  $selected[] = 0;
                }
              }
            }else{
              return 0;
            }
          }
        }       
      }else{
        return null;    
      }
    }
    return $selected;     
  }
  public function sla_company_id($prod_id){
    $this->db->select('company_id');
    $this->db->from('category_details');
    $this->db->where('cat_id',$prod_id);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return $query->row_array();
    }else{
      return 0;
    }
  }
  public function sla_company_id1($prod_id){
    $this->db->select('company_id');
    $this->db->from('product_management');
    $this->db->where('product_id',$prod_id);
    $query=$this->db->get();
    if($query->num_rows()>0){
      return $query->row_array();
    }else{
      return 0;
    }
  }
  public function check_slas2($id){
    $selected = '';
    $where=array('company_id'=>$id,'product != '=>'all');
    $where1=array('company_id'=>$id,'category != '=>'all');
    $where2=array('company_id'=>$id,'product'=>'all','category'=>'all');
    $this->db->select('*');
    $this->db->from('sla_combination');
    $this->db->where('company_id',$id);
    $q=$this->db->get(); 
    if($q->num_rows()>0){
      $this->db->select('*');
      $this->db->from('sla_combination');
      $this->db->where($where);
      $query=$this->db->get();
      if($query->num_rows() > 0){
        return 1;
      }else{
        $this->db->select('*');
        $this->db->from('sla_combination');
        $this->db->where($where1);
        $query=$this->db->get();
        if($query->num_rows()>0){
          return 1;
        }else{
          $this->db->select('*');
          $this->db->from('sla_combination');
          $this->db->where($where2);
          $q=$this->db->get();
          if($q->num_rows() > 0){
            $q=$q->result_array();
            foreach($q as $d){
              $this->db->select('*');
              $this->db->from('sla_mapping');
              $this->db->where('ref_id',$d['ref_id']);
              $datass=$this->db->get();
              if($datass->num_rows()>0){
                $datass=$datass->row_array();
                $selected[] = $datass;
              }else{
                $selected[] = 0;
              }
            }
            return $selected;
          }else{
             return 0; 
          }
        }
      }
    }else{
      return 0;
    }
  }
  public function delete_rec($id){
    $this->db->select('ref_id');
    $this->db->from('sla_combination');
    $this->db->where('company_id',$id);
    $query=$this->db->get();
    if($query->num_rows()>0){
      $query=$query->result_array();
      return $query;
      /*foreach($query as $q){
        $this->db->select('*');
        $this->db->from('sla_mapping');
        $this->db->where('ref_id',$q['ref_id']);
        $dq=$this->db->get();
        if($dq->num_rows()>0){
          $this->db->where('ref_id',$q['ref_id']);
          $this->db->delete('sla_mapping');         
        }
      }*/
    }else{
      return null;  
    }
  } 
	  public function delete_recd($id){
		$this->db->select('ref_id');
		$this->db->from('sla_combination');
		$this->db->where('company_id',$id);
		$query=$this->db->get();
		if($query->num_rows()>0){
		  $query=$query->result_array();
		  return $query;
		}else{
		  return null;  
		}
	  }
	  public function dele($d){
		$this->db->where('ref_id',$d['ref_id']);
		$this->db->delete('sla_mapping');
		return true;
	  }
	  public function deles($d){
		$this->db->where('ref_id',$d['ref_id']);
		$this->db->delete('sla_combination');
		return true;  
	  }
	  public function delete_recds($id){
		$this->db->select('ref_id');
		$this->db->from('sla_combination');
		$this->db->where('product',$id);
		$query=$this->db->get();
		if($query->num_rows()>0){
		  $query=$query->result_array();
		  return $query;
		}else{
		  return null;  
		}
	  } 
	  public function dele_s($d){
		$this->db->where('ref_id',$d['ref_id']);
		$this->db->delete('sla_mapping');
		return true;
	  }
	  public function deles_s($d){
		$this->db->where('ref_id',$d['ref_id']);
		$this->db->delete('sla_combination');
		return true;
	  }
	  
	  public function check_productss($id,$product_name,$company){    
  		$where=array(
        			'product_id!='=>$id, 
       		              'LOWER(product_name)'=> strtolower($product_name),
        			'company_id'=>$company
      		);
  		$this->db->select('*');
  		$this->db->from('product_management');
  		$this->db->where($where);
  		$que=$this->db->get();
      			//print_r($que);
      			//die();
  		if($que->num_rows()>0){
  		  return 1;
  		}else{
  		  return 0;
  		}
	  }

	   
	   
	   
    public function get_spareloc($datas){
      $this->db->select('*');
      $this->db->from('spare_loc');
    //  $this->db->where('company_id',$datas);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return $query;
      }else{
        return null;
      }
    }
    public function get_spare_loca($id,$company_id){
      $this->db->select('*');
      $this->db->from('spare_location');
      $this->db->where('spare_location',$id);
       $this->db->where('company_id',$company_id);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return $query->row_array();
      }else{
        return null;
      }
    }
    public function update_spare_locations($select_option,$location_id,$day_del,$companyids){
      $where=array('spare_location'=>$select_option,'company_id'=>$companyids);
      $update=array('days_to_deliver'=>$day_del);
      $insert=array(
        'spare_location'=>$select_option,
        'days_to_deliver'=>$day_del,
        'company_id'=>$companyids,
      );

      $this->db->select('*');
      $this->db->from('spare_location');
      $this->db->where($where);
      $query=$this->db->get();
      if($query->num_rows() > 0){
        $this->db->where($where);
        $this->db->update('spare_location', $update);
        return true;
      }else{
        $this->db->insert('spare_location',$insert);
        return true;
      }
    }
    public function get_count($assess_id){
      $this->db->select('*');
      $this->db->from('quiz');
      $this->db->where('certificate_id',$assess_id);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return $query->num_rows();
      }else{
        return null;
      }
    }
    public function check_tr($product_id,$cat_id,$certificate_type,$com_id){
      $where=array('product'=>$product_id,'category'=>$cat_id,'product_type'=>$certificate_type,'company_id'=>$com_id);
      $this->db->select('*');
      $this->db->from('training');
      $this->db->where($where);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return true;
      }else{
        return null;
      }
    }
    public function check_t($certificate_type,$com_id){
      $where=array('product_type'=>$certificate_type,'company_id'=>$com_id);
      $this->db->select('*');
      $this->db->from('training');
      $this->db->where($where);
      $query=$this->db->get();
      if($query->num_rows()>0){
        return true;
      }else{
        return null;
      }
    } 
	    public function company_ifslaexists($id){
		$where=array('company_id'=>$id,'product'=>'all','category'=>'all');
		$this->db->select('*');
		$this->db->from('sla_combination');
		$this->db->where($where);
		$data=$this->db->get();
		if($data->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}  
	public function product_ifslaexists($id,$prod_id){
		$where=array('product'=>$prod_id,'category'=>'all','company_id'=>$id);
		$this->db->select('*');
		$this->db->from('sla_combination');
		$this->db->where($where);
		$data=$this->db->get();
		if($data->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function category_ifslaexists($id,$prod_id){
		$this->db->select('prod_id');
		$this->db->from('category_details');
		$this->db->where('cat_id',$prod_id);
		$query=$this->db->get();
		if($query->num_rows()>0){
			$query=$query->row_array();
			foreach($query as $q){ 
				$where=array('product'=>$q,'category'=>$prod_id);
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where($where);
				$data=$this->db->get();
				if($data->num_rows()>0){
					return true;
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
		
	}
	   public function select_product_with_cat($prod_id){
		$this->db->select('prod_id');
		$this->db->from('category_details');
		$this->db->where('cat_id',$prod_id);
		$query=$this->db->get();
		if($query->num_rows()>0){
			$query = $query->row_array();
			return $query['prod_id'];
		}else{
			return NULL;
		}
	}
	public function check_tech($tech_id_add,$company_id)
	{
		$this->db->select('technician_id');
		$this->db->from('technician');
		$this->db->where('employee_id',$tech_id_add);
		$this->db->where('company_id',$company_id);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return $query->row_array();
		}else{
			return NULL;
		}
	}
public function remaining_days($datas){
$today_date = date("Y-m-d");
$this->db->select('*');
$this->db->from('admin_user');
//$this->db->join('user', 'user.email_id = admin_user.ad_mailid');
//$this->db->where('user.company_id',$datas);
$this->db->where('admin_user.company_id',$datas);
//$this->db->order_by("user.last_update", "DESC");
$this->db->limit(1);
$query = $this->db->get();
$answer = $query->row_array();
//print_r($answer);
$date1_ts = strtotime($today_date);
$date2_ts = strtotime($answer['renewal_date']);
$diff = $date2_ts - $date1_ts;
$result = round($diff / 86400);
return $result.' Days';
//$diff = date_diff($answer['start_date'],$answer['renewal_date']);
//return $result = $diff->format("%a").' Days'; 
}
	   
	   
///////raju

 public function checkserialno($serial_no,$customer_id)     ////////// used in web and API
    {
      $where=array(
      'serial_no'=>$serial_no,
	  'customer_id'=>$customer_id
      );
     $this->db->select('id,serial_no');
     $this->db->from('customer');
     $this->db->where($where);
       //$this->db->order_by("id", "desc");
     $query = $this->db->get();
    //  $str = $this->db->last_query();
    //  print_r($str);
    //  die;
     $res = $query->row_array();
     if ($query->num_rows() > 0)
      {
        return 1;
      }
        else 
		{
			return 0;
		}
    }	   
	   
 public function checkserialno_prod_cat($serial_no,$product,$category,$customer_id)     ////////// used in web and API
    {
      $where=array(
      'serial_no'=>$serial_no,
	  'product_serial_no'=>$product,
	  'component_serial_no'=>$category,
	  'customer_id'=>$customer_id
      );
     $this->db->select('id,serial_no');
     $this->db->from('customer');
     $this->db->where($where);
       //$this->db->order_by("id", "desc");
     $query = $this->db->get();
	 $sql = $this->db->last_query();
	// print_r($sql);
	// die;
     $res = $query->row_array();
     if ($query->num_rows() > 0)
      {
        return 1;
      }
        else 
		{
			return 0;
		}
    }	


    public function integeration_insertlogin($data)  // Aarthi
    {
    $this->db->insert('login', $data);
    return true;
	}


    public function checkusername($c_id,$email_id)     //////////Aarthi
    {
      $where_username=array(
      'companyid'=>$c_id,
      'username' =>$email_id
      );
     $this->db->select('username');
     $this->db->from('login');
     $this->db->where($where_username);
     $query = $this->db->get();
	 $sql = $this->db->last_query();
	// print_r($sql);
	// die;
     $res = $query->row_array();
     if ($query->num_rows() > 0)
      {
        return 1;
      }
        else 
		{
			return 0;
		}
    }	

    public function get_cust_type($customer_id){  //Aarthi to check the customer type

      $where = array(
        'customer_id' => $customer_id,
        'cust_type' =>0,
      );
      $this->db->select('cust_type');
      $this->db->from('customer');
      $this->db->where($where);
      $query = $this->db->get();
      $res = $query->row_array();
      if ($query->num_rows() > 0)
      {
        return 1;
      }
        else 
		{
			return 0;
    }
    
    }


     
	   
  public function check_aval_prod($id){    
  		$where=array(
        			'product_serial_no'=>$id
       		       
      		);
  		$this->db->select('customer_id,product_serial_no');
  		$this->db->from('customer');
  		$this->db->where($where);
  		$que=$this->db->get();
      		
  		if($que->num_rows()>0){
  		  return 1;
  		}else{
  		  return 0;
  		}
	  }

 public function check_aval_sub_prod($id){    
  		$where=array(
        			'component_serial_no'=>$id
       		       
      		);
  		$this->db->select('customer_id,component_serial_no');
  		$this->db->from('customer');
  		$this->db->where($where);
  		$que=$this->db->get();
      		
  		if($que->num_rows()>0){
  		  return 1;
  		}else{
  		  return 0;
  		}
	  }

  public function check_aval_servicegroup($id){    
  		$where=array(
        			'work_type'=>$id
       		       
      		);
  		$this->db->select('ticket_id,cust_id');
  		$this->db->from('all_tickets');
  		$this->db->where($where);
  		$que=$this->db->get();
      		
  		if($que->num_rows()>0){
  		  return 1;
  		}else{
  		  return 0;
  		}
	  }

 public function update_spare_qty($spare_qty_array,$ticketid)     ////////// used in web 
    {
    $cnt_array=count($spare_qty_array);
	// print_r($spare_qty_array);
	 $i=0;
	  foreach($spare_qty_array as $spa){
          $where=array('spare_code'=>$spa['spare_code']);
           $this->db->select('spare_code,quantity_purchased,quantity_used');
     $this->db->from('spare');
     $this->db->where($where);
     $query = $this->db->get();
     $spr = $query->row_array();
		 $pur_tot_qty=$spr['quantity_purchased']+$spa['qty']; 
		$tot_qty=$spr['quantity_used']+$spa['qty'];
	 $datass=array('quantity_purchased'=>$pur_tot_qty,'quantity_used'=>$tot_qty);
	  $this->db->where($where);
     $this->db->update('spare',$datass);	  
		$i++;  
          }  
	 
	
     if ($i==$cnt_array)
      {
	
		   $data=array('current_status'=>18);
        $this->db->where('ticket_id', $ticketid);
        $this->db->where('requested_spare!=', '');
         $this->db->update('all_tickets', $data);
		 
        return 1;
      }
        else 
		{
			return 0;
		}
    }		   

public function update_imp_spare_qty_28_mar_2019($spare_qty_array,$impspare_table_id,$companyid,$tech_id)     ////////// used in web 
    {
    $cnt_array=count($spare_qty_array);
	// print_r($spare_qty_array);
	 $i=0;
	  foreach($spare_qty_array as $spa){
          $where=array('spare_code'=>$spa['spare_code']);
           $this->db->select('spare_code,quantity_purchased,quantity_used');
     $this->db->from('spare');
     $this->db->where($where);
     $query = $this->db->get();
     $spr = $query->row_array();
		$pur_tot_qty=$spr['quantity_purchased']+$spa['qty'];
		$tot_qty=$spr['quantity_used']+$spa['qty'];
	 $datass=array('quantity_purchased'=>$pur_tot_qty,'quantity_used'=>$tot_qty);
	  $this->db->where($where);
     $this->db->update('spare',$datass);	  
		$i++;  
          }  
	 
	
     if ($i==$cnt_array)
      {
		 
		  $changes=array("status"=>2);
           $this->db->where('id',$impspare_table_id);
            $this->db->update('personal_spare',$changes); 
		 
        return 1;
      }
        else 
		{
			return 0;
		}
    }		   

public function update_imp_spare_qty($spare_qty_array,$impspare_table_id,$companyid,$tech_id)     ////////// used in web 
    {
    $cnt_array=count($spare_qty_array);
	// print_r($spare_qty_array);
	 $i=0;
	$date_on=date('Y-m-d H:i:s');
	  foreach($spare_qty_array as $spa){
		   $qtydatass=array('personal_table_id'=>$impspare_table_id,'spare_code'=>$spa['spare_code'],'tech_qty'=>$spa['tech_qty'],'service_qty'=>$spa['service_qty'],'company_id'=>$companyid,'tech_id'=>$tech_id,'create_on'=>$date_on);
         
       $this->db->insert('imprest_spare_approved_qty', $qtydatass);	  
		$i++;  
          }  
	 
	
     if ($i==$cnt_array)
      {
		 
		  $changes=array("status"=>2);
           $this->db->where('id',$impspare_table_id);
            $this->db->update('personal_spare',$changes); 
		 
        return 1;
      }
        else 
		{
			return 0;
		}
    }
	
	  public function call_type_check($call_type,$c_id)
    {
     $this->db->select('amc_type,company_id');
     $this->db->from('amc_type');
     $this->db->where('LOWER(amc_type.amc_type)', strtolower($call_type));
    // $this->db->where('company_id',$c_id);
     $query = $this->db->get();
	//	print_r($query);
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else 
		{
			return NULL;
		}
    }  
	   
	   public function call_tag_check($call_tag,$c_id)
    {
     $this->db->select('id,call,company_id');
     $this->db->from('call_tag');
     $this->db->where('LOWER(call_tag.call)', strtolower($call_tag));
     $this->db->where('company_id',$c_id);
     $query = $this->db->get();
	//	print_r($query);
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else 
		{
			return NULL;
		}
    }  
	   
	public function work_type_check($work_type,$c_id)
    {
     $this->db->select('service_group_id,service_group');
     $this->db->from('service_group');
     $this->db->where('LOWER(service_group.service_group)', strtolower($work_type));
     $this->db->where('company_id',$c_id);
     $query = $this->db->get();
	//	print_r($query);
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else 
		{
			return NULL;
		}
    }   
	
 public function get_customer_detail_bulk($serial_no,$productid,$categoryid,$cust_id)
    {
    $where=array(
      'serial_no'=>$serial_no,
	  'product_serial_no'=>$productid,
	  'component_serial_no'=>$categoryid,
	  'customer_id'=>$cust_id
      );
     $this->db->select('id,customer_id,customer_name,email_id,contact_number,alternate_number,door_num,address,cust_town,landmark,region,city,state,cust_country, 	pincode,type_of_contract,product_serial_no,component_serial_no,model_no,serial_no,priority,warrenty_expairy_date,contract_id,contract_value,contract_duration,start_date,end_date,last_update,company_id,invoice_date,delivery_date');
     $this->db->from('customer');
     $this->db->where($where);
       //$this->db->order_by("id", "desc");
     $query = $this->db->get();
	//	print_r($query);
     if ($query->num_rows() > 0)
      {
        return $query->row_array();
      }
        else 
		{
			return NULL;
		}
    }   

    public function get_company_name($c_id)  //Aarthi
    {
      $where = array(
        'company_id' =>$c_id,
      );
      $this->db->select('company_name');
      $this->db->from('company');
      $this->db->where($where);
      $query = $this->db->get();
      $res = $query->row_array();	   
       return $res['company_name'];
     // return $query;
   
    }
	   
	   
 }
?>
