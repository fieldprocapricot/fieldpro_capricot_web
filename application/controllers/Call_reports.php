<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Call_reports extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index()
	{
			 if ($this->session->userdata('user_logged_in'))
			 {
				 
		$this->data['webPageheading'] = ' Call Report';
		
	$company_id = $this->session->userdata('companyid');
		
		      $array = array('company_id'=>$company_id);
	 $this->data['service_group'] = $this->common->get_method($array,array('methodname'=>'GET_SERVICE_GROUP_ALL','sp_next_result'=>0));		 

			$this->load->view('reports/call_report',$this->data);
		}
		else{
	 redirect(base_url());
		}
	}
	
	public function call_report_all($id)
     {

		 if ($this->session->userdata('user_logged_in'))
		 {
				
			 
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));
		 $company_id = $this->session->userdata('companyid');
	
          $array = array('serviceid'=>$id);
		  $results = $this->common->get_method($array,array('methodname'=>'GET_CALL_REPORT','sp_next_result'=>0));
			 
			/*
Logic calcualation :
-------------------
Calls B/O from previous week = Total Calls from previous week with out the status of closed and completed.- (bo_prev_week)

B/O calls closed this week = Total previous call closed in this week - (bo_closed_this_week)

Current week's Calls Logged  = Calls logged in this week with out the status of closed and completed. - (open_curr_week)

Current week's Closed Calls - Calls completed by technician by this week (completed & closed) - (closed_curr_week)

B/O calls Open for this week = Calls B/O from previous week - B/O calls closed this week  : (bo_prev_week - bo_closed_this_week )= a

Current week's Open Calls = Current week's Calls Logged - Current week's Closed Calls  : (open_curr_week - closed_curr_week) = b

Calls C/F =  B/O calls Open for this week + Current week's Open Calls : (a+b)
			
			*/
			 
		  $data = array();
			 $i=0;
			 $tot_1="0";
			 $tot_2="0";
			 $tot_3="0";
			 $tot_4="0";
			 $tot_5="0";
			 $tot_6="0";
			 $tot_7="0";
			 $bo_open_this_week='0';
			 $curr_week_open_call='0';
			 $cf_call='0';
			
          foreach($results->result() as $row) {
			 
			$bo_open_this_week=$row->bo_prev_week-$row->bo_closed_this_week;
			if($bo_open_this_week=='')
			{
				$bo_open_this_week='0';
			}
			  else
			  {
				  $bo_open_this_week=abs($bo_open_this_week);
			  
			  }
	
			$curr_week_open_call=$row->open_curr_week-$row->closed_curr_week;
			 // $curr_week_open_call=$row->open_curr_week;
			  if($curr_week_open_call=='')
			{
				$curr_week_open_call='0';
			}
			  else
			  {
				  $curr_week_open_call=$curr_week_open_call;
			  }
			$cf_call=$bo_open_this_week+$curr_week_open_call;
			if($cf_call=='')
			{
				$cf_call='0';
			}
			  else
			  {
				  $cf_call=$cf_call;
			  }
			$tot_1+=$row->bo_prev_week;
			$tot_2+=$row->bo_closed_this_week;  
			$tot_3+=$row->open_curr_week;   
			$tot_4+=$row->closed_curr_week;
			$tot_5+=$bo_open_this_week; 
			$tot_6+=$curr_week_open_call;  
			$tot_7+=$cf_call;    
				$i=$i+1;
			
               $data[] = array(
				  $row->service_group, 
				  $row->bo_prev_week,
				  $row->bo_closed_this_week,
				  $row->open_curr_week,
				  $row->closed_curr_week,
				  $bo_open_this_week, 
				  $curr_week_open_call, 
				  $cf_call
               );
          }
			if($tot_1=='')
			{
				$tot_1='0';
			}
			  else
			  {
				  $tot_1=$tot_1;
			  }
			 	if($tot_2=='')
			{
				$tot_2='0';
			}
			  else
			  {
				  $tot_2=$tot_2;
			  }
			 	if($tot_1=='')
			{
				$tot_1='0';
			}
			  else
			  {
				  $tot_1=$tot_1;
			  }
			 	if($tot_3=='')
			{
				$tot_3='0';
			}
			  else
			  {
				  $tot_3=$tot_3;
			  }
			 	if($tot_4=='')
			{
				$tot_4='0';
			}
			  else
			  {
				  $tot_4=$tot_4;
			  }
			  	if($tot_5=='')
			{
				$tot_5='0';
			}
			  else
			  {
				  $tot_5=$tot_5;
			  }
			   	if($tot_6=='')
			{
				$tot_6='0';
			}
			  else
			  {
				  $tot_6=$tot_6;
			  }
			   	if($tot_7=='')
			{
				$tot_7='0';
			}
			  else
			  {
				  $tot_7=$tot_7;
			  }
			  $data[]=array('<b>Total</b>',$tot_1,$tot_2,$tot_3,$tot_4,$tot_5,$tot_6,$tot_7);

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $results->num_rows(),
                 "recordsFiltered" => $results->num_rows(),
                 "data" => $data
            );
		
		  echo json_encode($output);
		 }
			 else{
			 redirect(base_url());
			 }
     }
			
	
}