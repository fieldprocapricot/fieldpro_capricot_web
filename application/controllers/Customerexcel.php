<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customerexcel extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->model('common');	
		$this->load->library('encrypt');
		$this->load->model('capricotexcelimportexport');	
	
		
	}

	public function index()
	{
			
	}

		public function customerexcelexport()
	{
		$this->load->model('capricotexcelimportexport');	
		
					
					$dateof = date('Y-m-d');
			
					$array = array('dateof' => $dateof);
					$this->capricotexcelimportexport->customerexcel_fp_sap($array);
		
	}
	
	/////////////////Made changes  for customer creation in login //// Aarthi///////////////
 public function customerexcelimport()
    {

	   $this->load->model('Admin_model');
      $this->load->model('New_amc');
	$this->load->library('excel');
	$this->load->model('capricotexcelimportexport');	
$c_id='company51';
		
		$date = date("d-m-Y");
    $path='assets/excel/customer/sap/customer_'.$date.'.xlsx';
    $fileuse=$_SERVER['DOCUMENT_ROOT'].'/fieldpro-capricot/assets/excel/customer/sap/customer_'.$date.'.xlsx';
     
	if (!file_exists($fileuse))
	{   

echo 'customer_'.$date.'.xlsx file not found';                      
}
	  else
	  {

		$inputFileName = $path; 
$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
	  
$cc=1;	


for($i=2;$i<=$arrayCount;$i++)
{ 
     

               $custid=$allDataInSheet[$i]["A"];
                $name =$allDataInSheet[$i]["B"];
                $email_id=$allDataInSheet[$i]["C"];  ///2
                $contact=$allDataInSheet[$i]["D"];
                $alt_num=$allDataInSheet[$i]["E"];
                $door_no=$allDataInSheet[$i]["F"];
                $street=$allDataInSheet[$i]["G"];
                $town=$allDataInSheet[$i]["H"];
                $city=$allDataInSheet[$i]["I"];
                $state=$allDataInSheet[$i]["J"];
                $country=$allDataInSheet[$i]["K"];
                $product_check=$allDataInSheet[$i]["L"];
                $modal_no=$allDataInSheet[$i]["M"];
                $serial_no=$allDataInSheet[$i]["N"];
                $subcategory=$allDataInSheet[$i]["O"];   //14
                $pincode=$allDataInSheet[$i]["P"];
                $priority=$allDataInSheet[$i]["Q"];
                $support_type=$allDataInSheet[$i]["R"];   ///17
                $start_date=$allDataInSheet[$i]["S"];
                $contract_duration=$allDataInSheet[$i]["T"];
				$invoice_date=$allDataInSheet[$i]["U"];
	            $deliver_date=$allDataInSheet[$i]["V"];
	           	$vertical=$allDataInSheet[$i]["W"]; 
                $customer_id=$custid;
                $mobile_customer = $contact;
	
		$start_date = strtotime($start_date);
	$start_date = date('Y-m-d', $start_date);
	
		$invoice_date = strtotime($invoice_date);
	$invoice_date = date('Y-m-d', $invoice_date);	
	
	$deliver_date = strtotime($deliver_date);
	$deliver_date = date('Y-m-d', $deliver_date);
           
                $email = filter_var($email_id, FILTER_SANITIZE_EMAIL);
			
               
                if (filter_var($email, FILTER_VALIDATE_EMAIL))
                {

                

               
                            $product_array = $this->Admin_model->produ_checking1($product_check, $c_id);   
                            $product = $product_array['product_id'];
                            if (empty($product_array))
                            {
                               // array_push($result, ("Invalid Product-Category " . $datas[11]));
                            }
                            else
                            {
								
                                $category_array = $this->Admin_model->subcategory_serials1($subcategory, $product, $c_id);  
                                $category = $category_array['cat_id'];
                                if (empty($category_array))
                                {
                                  //  array_push($result, ("Invalid Sub-Category " . $datas[14]));
                                }
                                else
                                {
                                    if(!is_numeric($contract_duration))    ///////////  not
                                    {
                                      //  array_push($result, ("Invalid contract duration for " . $datas[2] ));
                                    }
                                    else if ($modal_no == "" || $serial_no == "")
                                    {
                                      //  array_push($result, ("Model/Serial No. is mandatory for " . $datas[2] . "'s product"));
                                    }
                                    else
                                    {
                                      
                                      
                                $contractdetailsarray=$this->New_amc->getcontractdetails($c_id,$support_type);
                              
                  
								$contractvalue='';
								$contract_period='';
								$warrantyexpirydate='';
								$end_date='';
       
          $end_date = date('Y-m-d', strtotime("+".$contract_duration." months", strtotime($start_date))); 
           $warrantyexpirydate= $end_date;
       if(count($contractdetailsarray)==0)
        {
       // array_push($result, ("Enter proper Contract type for customer ".$datas[0]));
        }
        else
        {
			// $serialcheck= $this->Admin_model->checkserialno($serial_no,$customer_id);
        $serialcheck= $this->Admin_model->checkserialno_prod_cat($serial_no,$product,$category,$customer_id);
				
		    if($serialcheck == 0)
                    {
                      
						
                                            $data = array(
                                                'customer_id' => trim($customer_id),
                                                'customer_name' => trim($name),
                                                'email_id' => trim($email_id),
                                                'contact_number' => trim($contact),
                                                'alternate_number' => trim($alt_num),
                                                'door_num' => $door_no,
                                                'address' => $street,
                                                'cust_town' => trim($town),
                                                'city' => trim($city),
                                                'state' => trim($state),
                                                'cust_country' => trim($country),
                                                'product_serial_no' => $product,
                                                'model_no' => $modal_no,
                                                'serial_no' => trim($serial_no),
                                                'component_serial_no' => $category,
                                                'pincode' => trim($pincode),
                                                'priority' => $priority,
                                                'type_of_contract' => trim($support_type),
                                                'contract_value' =>$contractvalue,
                                                'contract_duration'=>$contract_duration,
                                                'warrenty_expairy_date' =>  $warrantyexpirydate,
                                                'start_date' => $start_date,
                                                'end_date'=> $end_date,
												                        'invoice_date' => $invoice_date,
                                                'delivery_date'=> $deliver_date,
												                        'vertical'=> $vertical,
                                                'company_id' => $c_id
                                            );

                                           
//echo $flag;
                                            //  print_r($data);
                                            //  die;
                                          
                                       
          $insert = $this->capricotexcelimportexport->integeration_insertcustomer($data);
          $updatecontract=$this->New_amc->updatecontractdetails($customer_id,$c_id);

          if($insert){

            $user_check = $this->Admin_model->checkusername($c_id,$email_id);

            if($user_check == 0)  // to check the user already exist or not in login table
            {

            $user_type = 'Customer';
            $c_name = $this->capricotexcelimportexport->get_company_name($c_id);
            // $passwordplain  = rand(10000,99999);
            // $pass = $this->encrypt->encode($passwordplain);


            $alphabet = "abcdefghijklmnpqrstuwxyzABCDEFGHIJKLMNPQRSTUWXYZ0123456789";
            $pass = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 6; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
        
                $passwordplain = "";
                $passwordplain  = implode($pass);
                $newpass = $this->encrypt->encode($passwordplain);

            $data = array(

              'user_type' => $user_type,
              'username' => trim($email_id),
              'companyid' => $c_id,
              'companyname' => $c_name,
              'password' => $newpass,
          );

            $insert_login = $this->capricotexcelimportexport->integeration_insertlogin($data);

            if($insert_login){
            $app_url=base_url()."Capricot-APK/customer-releaseapk.apk";			  
$mail_message=" <!doctype html>
              <html>
              <head>
              <meta name='viewport' content='width=device-width' />
              <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
              <style>
              p,
              ul,
              ol {
              font-family: sans-serif;
              font-size: 14px;
              font-weight: normal;
              margin: 0;
              Margin-bottom: 15px; }
              p li,
              ul li,
              ol li {
              list-style-position: inside;
              margin-left: 5px; }
              a {
              color: #3498db;
              text-decoration: underline; }
              </style>
              <body>
              <p>Dear <b>$custname</b>,</p>
              <p>Welcome to Arkance.</b></p>
              <p>Thank you for registered with Arkance. Please find the access details.</p> 
              <p>Get the Arkance Customer App via -  <strong><a href='$app_url'>Arkance Customer App</a></strong></p>
              <p>Username : <strong>$email_id</strong></p>
              <p>Password : <strong>$passwordplain</strong></p>
              <p>Thanks & regards,</p> <p>Arkance Team.</p>

              </body>
              </html>";
   
             $config   = Array(
             'protocol' => 'smtp',
				  'smtp_host' => 'email-smtp.us-east-1.amazonaws.com',
				  'smtp_port' => '465',
				  'smtp_timeout' => '30',
				  'smtp_user' => 'AKIAQVP5N6THGN3WW5WY',
				  'smtp_pass' => 'BDr3VEpIbSqqFBuzQ291YtrMRfufJgXQAiZDLNsHMXtD',
				  'mailtype' => 'html',
				  'smtp_crypto' => 'ssl',
				  'wordwrap' => TRUE,
				  'charset' => 'utf-8',
				  'crlf' => "\r\n",
				 'newline'=> "\r\n"
        );
		 $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('hwsupport@arkance.world', 'Arkance');
        $this->email->to($mail_id);
       // $this->email->to('senthilkumar.a@kaspontech.com');
        $this->email->subject('Arkance - Customer Registration Details');
        $this->email->message($mail_message);
        $this->email->set_newline("\r\n");
	 $result= $this->email->send();
     if($result) 
     {	
        $data=array('email_sent'=>1 , 'password'=>$this->encrypt->encode($passwordplain),'plain_password'=>$passwordplain);	
        $this->db->where('username', $email_id);
        $this->db->update('login', $data);  
       echo $email_id." Mail Sent Successfully";
       echo "<br>";
         
        
      }
     else
     {
echo $this->email->print_debugger();
          echo $email_id." Mail Not Sent Successfully";
          echo "<br>";
     }

            }
            else{
            

            }
        }
        else{
          echo "username already exists in login table";
        }

          }
       

                  
                  
                  

					   ///  array_push($result, ("Customer " . $datas[1] . " added successfully"));		
					}
                else
              {
                  ///  array_push($result, ("Serial No " . $datas[13] . " already exist!!!"));
              }		
							
						
			     ///  array_push($result, ("Customer " . $datas[1] . " update successfully"));
				
                					  }
                                       
                                    }
                                }
                            }
                        
					

                }
				  else
                {
                  ///  array_push($result, ("Email " . $datas[2] . " is not a valid email"));
                }
               
             $e++;
            }
        
	  }
      
    }

	
			
	
}
