<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_category extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index($id)
	{
			 if ($this->session->userdata('user_logged_in'))
			 {
		
		
		$this->data['webPageheading'] = ' - Checklist';
		$this->data['service_id'] = $id;		
		$this->data['service_group_name'] = $_GET['name'];			 
			$this->load->view('service_category/service_category',$this->data);
		}
		else{
	 redirect(base_url());
		}
	}
	
	public function service_category_all($id)
     {
	
		 if ($this->session->userdata('user_logged_in'))
		 {
				
			 
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));
		
          $array = array('serviceid'=>$id);
		  $results = $this->common->get_method($array,array('methodname'=>'GET_SERVICE_CATEGORY_ALL','sp_next_result'=>0));
          $data = array();
			 $i=0;
			
          foreach($results->result() as $row) {
			 
			  $button="";
			  if($row->cate_type==1)
			  {
				  $cate='Text';
			  }
			  else
			  {
				   $cate='Yes/No';
			  }
		
			    $button = "<button class='btn btn-circle blue btn-outline btn-sm'  onclick='edit_details(" . $row->scid . ")'><i class='fa fa-pencil' aria-hidden='true'></i></button>";

			    $button = $button . "<button class='btn btn-circle red btn-outline btn-sm'  onclick='delete_details(" . $row->scid . ")'><i class='fa fa-trash' aria-hidden='true'></i></button>";
			
				$i=$i+1;
			
               $data[] = array(
				   $i,
				    ucfirst($row->description),
				    $cate,
				    web_Date($row->create_date),
				  $button
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $results->num_rows(),
                 "recordsFiltered" => $results->num_rows(),
                 "data" => $data
            );
		
		  echo json_encode($output);
		 }
			 else{
			 redirect(base_url());
			 }
     }
			
	
	public function add_update_category()
    {
		if ($this->input->is_ajax_request()) {
			 if ($this->session->userdata('user_logged_in'))
			{
				$cate_id = $this->input->post('cate_id');
				$serviceid = $this->input->post('serviceid');
				$servicename = $this->input->post('servicename');
				$cate_desc = $this->input->post('cate_desc');
				$cate_type =$this->input->post('cate_type');
				$delete = 0;
				
				$array = array('delete' => $delete,'sevcateid' => $cate_id,'serviceid' => $serviceid, 'cate_desc'=>$cate_desc, 'cate_type'=>$cate_type);
                  
				
			$query = $this->common->create_method($array,array('methodname'=>'CREATE_UPDATE_DELETE_CATEGORY','sp_next_result'=>0,'outPosition'=>'last','out'=>'p_cateid'));
				if($query){
					$row = $query->row();
					     
						$cid = $row->p_cateid;
					if($cate_id==0)
					{
					echo json_encode(array('status'=>'true','msg'=>'Saved Successfully'));
					}
					else
					{
					echo json_encode(array('status'=>'true','msg'=>'Updated Successfully'));
					}
				}
				else{
					echo json_encode(array('status'=>'problem','msg'=>'Something went problem. Please try again.'));
				}
			}
			else{
				echo json_encode(array('status'=>'sessionout'));
			}
		}
		else{
			exit('No direct script access allowed');
		}
		
	}	
		
public function edit_category()
	{
				 if ($this->session->userdata('user_logged_in'))
				 {
						
			$cate_id = $this->input->post('cate_id');
				
			  $array = array('cate_id'=>$cate_id); 
		 $cate = $this->common->get_method($array,array('methodname'=>'GET_SERVICE_CATEGORY','sp_next_result'=>0));	
		$scate=$cate->row();
		
		  print_r(json_encode($scate));		
	
		}
		else{
			echo 'sessionout';
		}
	}

	public function delete_category()
	{
		if ($this->input->is_ajax_request()) {
		 if ($this->session->userdata('user_logged_in'))
		 {
			
	
	$cate_id = $this->input->post('cate_id');
				$serviceid = '';
				$servicename = '';
				$cate_desc = '';
				$cate_type ='';
				$delete = 1;
				
				$array = array('delete' => $delete,'sevcateid' => $cate_id,'serviceid' => $serviceid, 'cate_desc'=>$cate_desc, 'cate_type'=>$cate_type);
                  
				
			$query = $this->common->create_method($array,array('methodname'=>'CREATE_UPDATE_DELETE_CATEGORY','sp_next_result'=>0,'outPosition'=>'last','out'=>'p_cateid'));
			if($query){
					$row = $query->row();
					     
						$cateid = $row->p_cateid;
				
					echo json_encode(array('status'=>'true','msg'=>"Deleted Successfully"));
				}
				else{
					echo json_encode(array('status'=>'problem','msg'=>'Something went problem. Please try again.'));
				}
					}
			else{
				echo json_encode(array('status'=>'sessionout'));
			}
		}
		else{
			exit('No direct script access allowed');
		}
	}
	
	
	
}