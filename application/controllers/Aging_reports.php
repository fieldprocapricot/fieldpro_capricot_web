<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aging_reports extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index()
	{
			 if ($this->session->userdata('user_logged_in'))
			 {
				 
		$this->data['webPageheading'] = ' Aging Report';
		
				
		
	$company_id = $this->session->userdata('companyid');
		
		      $array = array('company_id'=>$company_id);
	 $this->data['service_group'] = $this->common->get_method($array,array('methodname'=>'GET_SERVICE_GROUP_ALL','sp_next_result'=>1));		
		 $array = array('company_id'=>$company_id);
	 $this->data['product_all'] = $this->common->get_method($array,array('methodname'=>'GET_PRODUCT_ALL','sp_next_result'=>1));		
	
				   $array = array('company_id'=>$company_id);
		$this->data['work_status'] = $this->common->work_status($array);
	

			$this->load->view('reports/aging_report',$this->data);
		}
		else{
	 redirect(base_url());
		}
	}
	
	public function aging_report_all()
     {

		 if ($this->session->userdata('user_logged_in'))
		 {
			  $company_id = $this->session->userdata('companyid');
				$startdate=$this->input->post('startdate');
			 $sdate = str_replace('/', '-', $startdate);
 $startdate = date('Y-m-d', strtotime($sdate));
			/*	$enddate=$this->input->post('enddate');	
			  $edate = str_replace('/', '-', $enddate);
 $enddate = date('Y-m-d', strtotime($edate));*/
			 	$aging_dur=$this->input->post('aging_dur');	
			 $service_group=$this->input->post('service_group');
			 $work_status=$this->input->post('work_status');
			
			 
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));
	
		//	Array ( [where_cond] => A.current_status IN (8) [from_date] => 2019-02-01 [to_date] => 2019-03-15 )  
	
          $array = array('companyid'=>$company_id,'from_date'=>$startdate,'aging_dur'=>$aging_dur,'service_group'=>$service_group,'work_status'=>$work_status);
		 $agingss = $this->common->aging_report_filter($array);	
		  $data = array();
			 $i=0;
		

          foreach($agingss->result() as $row) {
			 
			  
			  if($row->current_status=='0')
			  {
		
				  $c_status= 'Not Assign';
			  }
			  else
			  {
				   $array = array('current_status'=>$row->current_status);
		 $curr_status = $this->common->ticket_status($array);
		$curstatus = $curr_status->row();
				  $c_status= $curstatus->status_name;
				  $c_status = str_replace('_', ' ', $c_status);
			/*	  if($c_status==1)
				  {
					  $c_status='newly assigned';
				  }
				 	else if($c_status==2)
				  {
					   $c_status='accepted';
				  }
				    else if($c_status==3)
				  {
					   $c_status='reject';
				  }
				    else if($c_status==4)
				  {
					   $c_status='ongoing';
				  }
				    else if($c_status==5)
				  {
					   $c_status='travel start status';
				  }
				     else if($c_status==6)
				  {
					   $c_status='travel end status';
				  }
				     else if($c_status==7)
				  {
					   $c_status='ticket start';
				  }
				     else if($c_status==8)
				  {
					   $c_status='completed';
				  }
				     else if($c_status==9)
				  {
					   $c_status='escalated';
				  }
				     else if($c_status==10)
				  {
					   $c_status='work in progress';
				  }
				      else if($c_status==11)
				  {
					   $c_status='spare accepted';
				  }
				      else if($c_status==12)
				  {
					   $c_status='closed';
				  }
				      else if($c_status==13)
				  {
					   $c_status='reimbursement';
				  }
				      else if($c_status==14)
				  {
					   $c_status='spare request';
				  }
				      else if($c_status==15)
				  {
					   $c_status='escalate';
				  }
				      else if($c_status==16)
				  {
					   $c_status='renew contract';
				  }
				     else if($c_status==17)
				  {
					   $c_status='reimbursement request';
				  }
				     else if($c_status==18)
				  {
					   $c_status='spare delivered';
				  }
				     else if($c_status==19)
				  {
					   $c_status='contract rejected';
				  }
				     else if($c_status==20)
				  {
					   $c_status='contract accepted';
				  }
				     else if($c_status==50)
				  {
					   $c_status='Otp verified';
				  }
				  else
				  {
					  $c_status='not assign';
				  }
				  */
			  }
		
				$i=$i+1;
			
               $data[] = array(
				  $i, 
				   $row->customer_name,
				  $row->product_name,
				  date('d/m/Y', strtotime($row->raised_time)),
				 date('d/m/Y', strtotime($row->last_update)),
				$c_status
               );
          }
		

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $agingss->num_rows(),
                 "recordsFiltered" => $agingss->num_rows(),
                 "data" => $data
            );
		
			 
		  echo json_encode($output);
		 }
			 else{
			 redirect(base_url());
			 }
     }
			
	
}