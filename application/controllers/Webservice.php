<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');
//error_reporting(E_ALL);
class Webservice extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->view('exam');
    }
    public function test_mail()
    {
        $this->load->library('email');
        $this->load->helper('url');
        $this->load->database();
        echo $to='sahithya.s@kaspontech.com';
$ticket_id="Tk_0074";
        echo $username = base_url() . "index.php?/Webservice/Feedback/?user=$to&ticket_id=$ticket_id";
        $config   = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'us2.smtp.mailhostbox.com',
            'smtp_port' => 25,
            'smtp_user' => 'Fieldpro@kaspontech.com',
            'smtp_pass' => 'Field@2012',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('Fieldpro@kaspontech.com', 'Capricot');
        $this->email->to($to);
        $this->email->subject('Login Credentials');
        $this->email->message($username);
        $this->email->set_newline("\r\n");
        $this->email->send();
        if($this->email->send())
        {
        echo 'Email sent successfully';
        }
        else
        {
            echo "Not delivered";
        //show_error($this->email->print_debugger());
        }
    }

    /*Displaying reimbursement request details  */
    public function display_reimbursement()
    {
        
        $this->load->helper('download');
        $this->load->library('cezpdf');
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id != "") {
            $result = $this->Punch_in->display_reimbursement($tech_id);
            $json   = array();
            foreach ($result as $row) {
                array_push($json, array(
                    'Ticket_Id' => $row['ticket_id'],
                    'source_address' => $row['source_Address'],
                    'destination_address' => $row['destin_Address'],
                    'mode' => $row['mode_of_travel'],
                    'km' => $row['no_of_km_travelled'],
                    'Travel_charge' => $row['travelling_charges']
                ));
                $img_dir  = 'assets/images/pdf/' . $row['ticket_id'] . $row['technician_id'] . '.pdf';
                $img_dir1 = stripslashes($img_dir);
                file_put_contents($img_dir1, $this->cezpdf->ezStream());
            }
            $col_names = array(
                'Ticket_Id' => 'Ticket ID',
                'source_address' => 'Source Address',
                'destination_address' => 'Destination Address',
                'mode' => 'Mode of Travel',
                'km' => 'No of Kilometers',
                'Travel_charge' => 'Ticket Charge'
            );
            
            $data = $this->cezpdf->ezTable($json, $col_names, 'Reimbursement List', array(
                'width' => 550
            ));
            $this->cezpdf->ezStream();
            
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician ID"
            );
        }
        echo json_encode($json);
    }
    

    /*Mobile dashboard datas */
    public function home()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->home($tech_id);
        return $result;
    }

    public function chat_list()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $json = '';
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician ID"
            );
        } else {
            $result = $this->Punch_in->chat_list($tech_id);
            $json   = array(
                "status" => 1,
                "result" => $result
            );
        }
        echo json_encode($json);
    }

    /*Send spare details based on spare code */
    public function spare_view()
    {
        $this->load->helper('url');
        $this->load->database();
        $spare_code = $this->input->post('spare_code');
        $this->load->model('Punch_in');
        if ($spare_code != "") {
            $result = $this->Punch_in->spare_view($spare_code);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        } else {
            $json = array(
                "status" => 0,
                "result" => "Please Provide Spare Code"
            );
            echo json_encode($json);
        }
    }

   /* Retrive spare details based on spare code*/ 
    public function spare_search()
    {
        $this->load->helper('url');
        $this->load->database();
        $spare_code = $this->input->post('spare_code');
        $this->load->model('Punch_in');
        if ($spare_code == '') {
            $json = array(
                "status" => 0,
                "result" => "Please provide spare code"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->spare_search($spare_code);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }

    /*Get product category based on technician id */
    public function get_category()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->get_category($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }
    
     public function get_spare_29_may_2019()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->get_spare($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }
    

    public function get_spare()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $spare_type = $this->input->post('spare_type');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->get_spare($tech_id,$spare_type);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $noresult=array(array('msg'=>'No Records found'));
                $json = array(
                    "status" => 0,
                    "result" => $noresult
                );
                echo json_encode($json);
            }
        }
    }
    
    /* Get amc details from amc_type table, based on technician id*/
    public function get_amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->get_amc($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }

  public function get_service_category()              ////////////////////  raju
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $service_id = $this->input->post('service_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->service_category($service_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $noresult=array(array('msg'=>'No Records found'));
                $json = array(
                    "status" => 1,
                    "result" => $noresult
                );
                echo json_encode($json);
            }
        }
    }   
    
public function get_contract_type()                 ////////////////  raju
    {
        $this->load->helper('url');
        $this->load->database();
       $this->load->model('Punch_in');
        $tech_id = $this->input->post('tech_id');
        $company_id = $this->Punch_in->get_company($tech_id);
 
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->contract_type($company_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }   

public function get_spare_type()                 ////////////////  raju
    {
        $this->load->helper('url');
        $this->load->database();
       $this->load->model('Punch_in');
        $company_id = $this->input->post('comp_id');
      
        if ($company_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->spare_type($company_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }   


    
public function get_customer_contract_type()                 ////////////////  raju
    {
        $this->load->helper('url');
        $this->load->database();
       $this->load->model('Punch_in');
        $company_id = $this->input->post('comp_id');
      
        if ($company_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->contract_type($company_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }   

public function get_customer_call_type()                 ////////////////  raju
    {
        $this->load->helper('url');
        $this->load->database();
       $this->load->model('Punch_in');
        $company_id = $this->input->post('comp_id');
      
        if ($company_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->customer_call_type($company_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }   


    
   public function get_work_type()                 ////////////////  raju
    {
        $this->load->helper('url');
        $this->load->database();
       $this->load->model('Punch_in');
       $company_id = $this->input->post('comp_id');
      
 
        if ($company_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->work_type($company_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }   
        

  /*Send product list of technicians */
    public function product()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->product($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }
    
       public function product_customer()   ////////////////////  raju
    {
        $this->load->helper('url');
        $this->load->database();
        $customer_contactno = $this->input->post('customer_contactno');   
        $this->load->model('Punch_in');
        if ($customer_contactno == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide customer contact number"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->product_customer($customer_contactno);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }

public function product_subcategory()   ////////////////////  raju
    {
        $this->load->helper('url');
        $this->load->database();
        $product_id = $this->input->post('product_id');   
        $this->load->model('Punch_in');
        if ($product_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide product id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->product_subcategory($product_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }



   /*Send category id, category name which have relationship with product */
    public function category()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id    = $this->input->post('tech_id');
        $product_id = $this->input->post('product_id');
        $this->load->model('Punch_in');
        if ($tech_id == '' || $product_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->category($tech_id, $product_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }

    /*Store knowledgebase details, image, video from mobile */
    public function upload()
    {

        $this->load->helper('url');
        $this->load->database();
        $string_prob = preg_replace('/\s+/', '', $this->input->post('problem_desc'));
        
        $this->load->model('Punch_in');
        $r = $this->Punch_in->get_tech_product($this->input->post('tech_id'));
      
        foreach ($r as $row) {
            $product    = $row['product_id'];
            $company_id = $row['company_id'];
        }

      //  $baseurl = base_url() . 'assets/images/knowledge_base/';
        $baseurl ='assets/images/knowledge_base/';
        if (isset($_FILES["fileUpload_image"]['tmp_name'])) {
            $img_name=time();
            $image      = $_FILES["fileUpload_image"];
            $output_dir = "./assets/images/knowledge_base/";
            $img_dir    = $baseurl . 'image/' . $img_name . '.jpg';
            
        if (isset($_FILES["fileUpload_image"]['name'])) {
            move_uploaded_file($_FILES["fileUpload_image"]["tmp_name"], $output_dir.'image/'.$img_name.'.jpg');
        }   
            
        } else {
            $img_dir = "";
        }
        if (isset($_FILES["fileUpload_video"]['tmp_name'])) {
            $vid_name=time();
            $video      = $_FILES["fileUpload_video"];
            $output_dir = "./assets/images/knowledge_base/";
            $video_dir  = $baseurl . 'video/' . $vid_name . '.mp4';
         if (isset($_FILES["fileUpload_video"]['name'])) 
         {
           move_uploaded_file($_FILES["fileUpload_video"]["tmp_name"], $output_dir.'video/'.$vid_name.'.mp4');
            
        ///echo "no".$_FILES["fileUpload_video"]["error"];;
    
        }   
        } else {
            $video_dir = "";
        }
      
       
        //$prod_cat = $this->Punch_in->get_prod_cat($product, $this->input->post('category'));
        $prod_cat = $this->Punch_in->get_prod_cat($product, $this->input->post('category'));
        //print_r($prod_cat);
        
            $regioncitydata=$this->Punch_in->gettechdetails($this->input->post('tech_id'));
            $region=$regioncitydata['region'];
            $area=$regioncitydata['area'];
           
            $data   = array(
                'technician_id' => $this->input->post('tech_id'),
                'ticket_id' => $this->input->post('ticket_id'),
                'product' => $this->input->post('category'),
                'company_id' => $company_id,
                'category' => $this->input->post('sub_category'),
                'sub_category' => $this->input->post('sub_category'),
                'problem' => $this->input->post('problem_desc'),
                'solution' => $this->input->post('solution'),
                'image' => $img_dir,
                'video' => $video_dir,
                'region'=>$region,
                'area'=>$area
            );
        
        //print_r($data);

            $result = $this->Punch_in->upload($data);
            if ($result == 1) {
                $employee_id = $this->Punch_in->get_emp_id($this->input->post('tech_id'));
                $notify      = 'New Knowledge base request raised by ' . $employee_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $this->input->post('tech_id'));
                $json = '';
                $json = array(
                    "status" => "1",
                    "msg" => "Uploaded Successfully!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => "0",
                    "msg" => "Error during Upload!!!"
                );
            }
        
        echo json_encode($json);
    }

    /*It will fetch and return the knowledgebase detail based on key entered by mobile uesr */
    public function search()
    {
        $this->load->helper('url');
        $this->load->database();
        $key = $this->input->post('key');
        $sub = $this->input->post('sub');
        $baseurl=base_url();
        $this->load->model('Punch_in');
        $result = $this->Punch_in->search($key, $sub, $baseurl);
        if (!empty($result)) {
            $json = '';
            $json = array(
                "status" => 1,
                "result" => $result
            );
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "No result found!!!"
            );
        }
        echo json_encode($json);
    }

    /*This API change the technician stattus to punched in, it auto fetch the location by google map api */
    public function punch_in()
    {
        $this->load->helper('url');
        
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $type    = $this->input->post('punch_type');
        
        if ($tech_id != "" || $type != "" || $current_location != "") {
            $this->load->model('Punch_in');
            $tech_check = $this->Punch_in->check_tech($tech_id);
            if ($tech_check > 0) {
                $latitude  = $this->input->post('latitude');
                $longitude = $this->input->post('longitude');
                //$latitude="26.754347";
                //$longitude="81.001640";
                //sleep(1);
                
                // $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false';
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($latitude) . ',' . trim($longitude) . '&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI';
                $ch  = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $output1 = json_decode($response);
                
                $status1 = $output1->status;
                if ($status1 == 'OK' && !empty($output1->results[1])) {
                    //Get address from json data
                    $current_location = ($status1 == "OK") ? $output1->results[1]->formatted_address : '';
                    
                    if ($type == 'punch_in') {
                        $result = $this->Punch_in->check_punch_in($tech_id);
                        if ($result == 0) {
                            $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
                            $res        = $this->Punch_in->punch_in($tech_id, $company_id);
                            if ($res == 1) {
                                $res3 = $this->Punch_in->available($tech_id, $current_location);
                                $json = '';
                                $json = array(
                                    "status" => 1,
                                    "msg" => "Punched in Successfully!!!"
                                );
                            }
                        } else {
                            $json = '';
                            $json = array(
                                "status" => 0,
                                "msg" => "Error during Punch in!!!"
                            );
                        }
                    } else if ($type == 'punch_out') {
                        $result = $this->Punch_in->check_punch_out($tech_id);
                        if ($result != 0) {
                            $res = $this->Punch_in->punch_out($tech_id, $result);
                            if ($res == 1) {
                                $res3 = $this->Punch_in->nt_available($tech_id, $current_location);
                                $json = '';
                                $json = array(
                                    "status" => 1,
                                    "msg" => "Punched out Successfully!!!"
                                );
                            }
                        } else {
                            $json = '';
                            $json = array(
                                "status" => 0,
                                "msg" => "Error during Punch out!!!"
                            );
                        }
                    }
                }
                
                else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Unable to fetch Current Location!!!"
                    );
                }
            }
            
            else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Provide proper technician id!!!"
                );
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all credentials!!!"
            );
        }
        
        echo json_encode($json);
    }
    

    public function punch_in_tech()
    {
        $this->load->helper('url');
        
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $type    = 'punch_in';
        
        if ($tech_id != "" || $type != "") {
            $this->load->model('Punch_in');
            $tech_check = $this->Punch_in->check_tech($tech_id);
            if ($tech_check > 0) {
                $latitude  = $this->input->post('latitude');
                $longitude = $this->input->post('longitude');
                //$latitude="26.754347";
                //$longitude="81.001640";
                //sleep(1);
                
                // $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false';
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($latitude) . ',' . trim($longitude) . '&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI';
                $ch  = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $output1 = json_decode($response);
                
                $status1 = $output1->status;
                 if ($latitude!="" && $longitude!="") {
               // if ($status1 == 'OK' && !empty($output1->results[1])) {
                    //Get address from json data
                    $current_location = ($status1 == "OK") ? $output1->results[1]->formatted_address : '';
                    
                    
                
                        $result = $this->Punch_in->check_punch_in($tech_id);
                        if ($result == 0) {
                            $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
                            $res        = $this->Punch_in->punch_in($tech_id, $company_id);
                            if ($res == 1) {
                                $res3 = $this->Punch_in->available($tech_id, $current_location);
                                $json = '';
                                $json = array(
                                    "status" => 2,
                                    "msg" => "Punched in Successfully!!!"
                                );
                            }
                       } else {
                            $json = '';
                            $json = array(
                                "status" => 0,
                                "msg" => "Error during Punch in!!!"
                            );
                        }
                        
                 
              }
                
                else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "We are unable to fetch your current location. Please try after some time.!!!"
                    );
                }
            }
            
            else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Provide proper technician id!!!"
                );
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all credentials!!!"
            );
        }
        
        echo json_encode($json);
    }
    
    public function punch_out_tech()
    {
        $this->load->helper('url');
        
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $type    = 'punch_out';
        
        if ($tech_id != "" || $type != "") {
            $this->load->model('Punch_in');
            $tech_check = $this->Punch_in->check_tech($tech_id);
            if ($tech_check > 0) {
                $latitude  = $this->input->post('latitude');
                $longitude = $this->input->post('longitude');
                //$latitude="26.754347";
                //$longitude="81.001640";
                //sleep(1);
                
                // $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false';
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($latitude) . ',' . trim($longitude) . '&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI';
                $ch  = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $output1 = json_decode($response);
                
                $status1 = $output1->status;
                if ($status1 == 'OK' && !empty($output1->results[1])) {
                    //Get address from json data
                    $current_location = ($status1 == "OK") ? $output1->results[1]->formatted_address : '';
                    
           
                        $result = $this->Punch_in->check_punch_out($tech_id);
                        if ($result != 0) {
                            $res = $this->Punch_in->punch_out($tech_id, $result);
                           if ($res == 1) {
                                $res3 = $this->Punch_in->nt_available($tech_id, $current_location);
                                $json = '';
                                $json = array(
                                    "status" => 2,
                                    "msg" => "Punched out Successfully!!!"
                                );
                           }
                        } else {
                            $json = '';
                            $json = array(
                                "status" => 0,
                                "msg" => "Error during Punch out!!!"
                            );
                        }
                        
                   
                }
                
                else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Unable to fetch Current Location!!!"
                    );
                }
            }
            
            else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Provide proper technician id!!!"
                );
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all credentials!!!"
            );
        }
        
        echo json_encode($json);
    }
    
    
    public function get_new_tkts()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->get_new_tkts($tech_id);
        return $result;
    }

    /*Send new assigned tickets to technicians */
    public function android_get_new_tkts()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->android_get_new_tkts($tech_id);
        return $result;
    }

    /*Send new assigned tickets to relevant technicians based on current date */
    public function tickets_forthe_day()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->tickets_forthe_day($tech_id);
        return $result;
    }

    /*Send accepted tickets, work progress tickets */
    public function ongoing_tickets()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->ongoing_tickets($tech_id);
        return $result;
    }


    /*It change the customer prefference date and schedule next date */
    public function reschedule_tickets()

    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $ticket_id = $this->input->post('ticket_id');
        $company_id = $this->input->post('company_id');
        $new_date = $this->input->post('reschedule_date');
        $this->load->model('Punch_in');
        if ($tech_id != "" && $ticket_id != "" && $company_id != "" && $new_date!="") {
        $result = $this->Punch_in->reschedule_tickets($tech_id,$company_id,$ticket_id,$new_date);
       
        if($result==1){
            $json = '';
            $json = array(
                "status" => 1,
                "msg" => "Ticket rescheduled"
            );
        }  
        else{
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Already ticket scheduled in given date!"
            );
        }
    }
    else{
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Provide all details!"
        );
    }
    echo json_encode($json);
}
      
    
/*It is change the ticket status to accepted */
    public function accept_ticket()
    {
        //$this->load->helper('url');
        $this->load->database();
        $tech_id   = $this->input->post('tech_id');
        $ticket_id = $this->input->post('ticket_id');
        $reason    = $this->input->post('reason');
        $this->load->model('Punch_in');
        if ((!empty($ticket_id)) || (!empty($ticket_id))) {
            $result      = $this->Punch_in->accept_ticket($tech_id, $ticket_id, $reason);
            $company_id  = $this->Punch_in->get_company($this->input->post('tech_id'));
            $employee_id = $this->Punch_in->get_emp_id($this->input->post('tech_id'));
            
            if ($reason == 'Accept') {
                $notify = $ticket_id . ' is accepted by ' . $employee_id;
            } else {
                $notify = $ticket_id . ' is rejected by ' . $employee_id;
            }
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
            if (!empty($result)) {
                $json = '';
                $json = $result;
                echo $json;
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error !!!"
            );
            echo json_encode($json);
        }
    }

    /*It change the travel status to changed and store the address, mode of travel, estimation time */
    public function start_travel()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id = $this->input->post('ticket_id');
        $this->load->model('Punch_in');     
        $start_time = date("Y-m-d");
        $mode_of_travel = $this->input->post('mode_of_travel');
        $company_id = $this->Punch_in->get_company($this->input->post('technician_id'));
        $travelling_charges =0;

        //storing address source
        $lat = $this->input->post('source_lat');
        $long =  $this->input->post('source_long');
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($long) . '&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($response);
        $status = $output->status;
        $source_Address = ($status == "OK") ? $output->results[0]->formatted_address : '';

        //Storing address destination
       if($mode_of_travel=="Two Wheeler"){
        $get_travel_charge = $this->Punch_in->get_travel_charge($company_id);
           $travelling_charges = $get_travel_charge['two_whel'] * $this->input->post('no_of_km_travelled');
       }

      if($mode_of_travel=="Four Wheeler"){
         $get_travel_charge = $this->Punch_in->get_travel_charge($company_id);
         $travelling_charges = $get_travel_charge['four_whel'] * $this->input->post('no_of_km_travelled');
       }

        $data       = array(
            'technician_id' => $this->input->post('technician_id'),
            'ticket_id' => $this->input->post('ticket_id'),
            'source_lat' => $this->input->post('source_lat'),
            'source_long' => $this->input->post('source_long'),
            'no_of_km_travelled' => $this->input->post('no_of_km_travelled'),
            'estimated_time' => $this->input->post('estimated_time'),
            'mode_of_travel' => $this->input->post('mode_of_travel'),
            'start_time' => $start_time,
            'last_update' => date('Y-m-d H:i:s'),
            'status' => 5,
            'company_id' => $company_id,
            'source' => $source_Address,
            'travelling_charges' => $travelling_charges
        );
        $this->load->model('Punch_in');
        if ($this->input->post('technician_id') != '' || $this->input->post('ticket_id') != '' || $this->input->post('source_lat') != '' || $this->input->post('source_long') != '' || $this->input->post('no_of_km_travelled') != '' || $this->input->post('estimated_time') != '' || $this->input->post('mode_of_travel') != '' || $this->input->post('start_time') != '') 
        {
         //   $result = $this->Punch_in->start_travel($data, $this->input->post('technician_id'));
            $result = $this->Punch_in->start_travel($data);
            if ($result == 1) {
                $this->load->model('Punch_in');
                $travel_status = array(
                    'current_status' => 5,
                    'mode_of_travel' => $this->input->post('mode_of_travel'),
                    'last_update' => date('Y-m-d H:i:s'),
                    'ticket_start_time' => date('Y-m-d H:i:s')
                );
                $res = $this->Punch_in->change_travel_status($travel_status, $ticket_id);
                if ($res == 1) {
                    $json = '';
                    $json = array(
                        "status" => 1,
                        "msg" => "Travel Successfully started!!!"
                    );
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Error!!!"
                    );
                }
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all required details!!!"
            );
        }
        echo json_encode($json);
        
    }
    
 public function start_travel_customer_location()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id = $this->input->post('ticket_id');
        $this->load->model('Punch_in');     
      
    $tkt_det = $this->Punch_in->get_ticket_details($ticket_id);
        $cust_details   = $this->Punch_in->get_info_customer($tkt_det['cust_id'],$tkt_det['serial_no']);    
    $new_add=$cust_details['address'].','.$cust_details['cust_town'].','.$cust_details['landmark'].','.$cust_details['city'];
    $latitude=0; 
    $longitude=0;
    
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_add).'&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI');
    $geo = json_decode($geo, true);  // Convert the JSON to an array
//print_r($geo);exit;
       if ($geo['status'] == 'OK') {
              $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
              $longitude = $geo['results'][0]['geometry']['location']['lng'];
       }
       else{
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI');
        $geo = json_decode($geo, true); 
        if ($geo['status'] == 'OK') {
            $latitude = $geo['results'][0]['geometry']['location']['lat'];   
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
       } 
        else if($geo['status'] == 'ZERO_RESULTS'){
        $msg = "Non-existent address, please try again!";
         // exit();   

        }
        else if($geo['status'] == 'OVER_DAILY_LIMIT'){
         $msg = "Google API error,  you are over your quota!";
    
      //  exit();   

       }
        else if($geo['status'] == 'REQUEST_DENIED'){
        $msg = "Google API error,  your request was denied!";

      //  exit();   

       }
        else if($geo['status'] == 'INVALID_REQUEST'){
        $msg = "Google API error,  address, components or latlng missing!";
    
      //  exit();   

       }
        else if($geo['status'] == 'UNKNOWN_ERROR'){
        $msg = "Google API error,  please try again!";
    
     //   exit();   

     }

       
     }
                    
                    
                    $json = '';
                    $json = array(
                        "status" => 1,
                        "customer_latitude" => $latitude,
                        "customer_longitude" => $longitude,
                        "msg" => "Customer Location"
                    );
               
         
    
        echo json_encode($json);
        
    }


    /*End travel will fetch the destination address and store, also changed the status to end */
    public function end_travel()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id     = $this->input->post('ticket_id');
        $technician_id = $this->input->post('technician_id');
        $reached_loc  = $this->input->post('reached_cust_location');
        $end_time      = date("Y-m-d");

        //destinationaddress
        $lat1 = $this->input->post('dest_lat');
        $long1 = $this->input->post('dest_long');
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat1) . ',' . trim($long1) . '&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $output1 = json_decode($response);
        $status1 = $output1->status;
        $address = ($status1 == "OK") ? $output1->results[1]->formatted_address : '';

        $data          = array(
            'end_time' => $end_time,
            'last_update' => date('Y-m-d H:i:s'),
            'dest_lat' => $this->input->post('dest_lat'),
            'dest_long' => $this->input->post('dest_long'),
            'destination' => $address
        );
        $this->load->model('Punch_in');
        if ($this->input->post('technician_id') != '' || $this->input->post('ticket_id') != '' || $this->input->post('start_time') != '' || $reached_loc != '') {
            $result = $this->Punch_in->end_travel($data, $ticket_id, $technician_id);
            if ($result == 1) {
                $this->load->model('Punch_in');
                $travel_status = array(
                    'current_status' => 6,
                    'last_update' => date('Y-m-d H:i:s'),
                    'reached_cust_location' => $reached_loc

                );
                $res  = $this->Punch_in->change_travel_status($travel_status, $ticket_id);
                if ($res == 1) {
                    $json = '';
                    $json = array(
                        "status" => 1,
                        "msg" => "Reached Successfully!!!"
                    );
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Error!!!"
                    );
                }
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all required details!!!"
            );
        }
        echo json_encode($json);
        
    }

    /*Store the submitted bill images from mobile */
    public function bill()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id      = $this->input->post('ticket_id');
        $technician_id  = $this->input->post('technician_id');
        $travel_charges = $this->input->post('travel_charges');
        $file           = '';
        $img_dir        = "";
        if (!empty($_FILES["myFile"])) {
            $file_name = $_FILES["myFile"]["name"];
            $temp_name = $_FILES['myFile']['tmp_name'];
            $filename  = end((explode(".", $file_name)));
            $img_dir   = 'assets/images/bill/' . $ticket_id . '.' . $filename;
            $img_dir1  = stripslashes($img_dir);
            move_uploaded_file($temp_name, $img_dir1);
        }
        
        $this->load->model('Punch_in');
        if ($technician_id == '' || $ticket_id == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        } else {
            $company_id = $this->Punch_in->get_company($this->input->post('technician_id'));
           
            $res        = $this->Punch_in->bill($technician_id, $ticket_id, $img_dir, $travel_charges, $company_id);
            $json       = '';
            if ($res == 1) {
                $json = '';
                $json = array(
                    "status" => 1,
                    "msg" => "Travel expense updated successfully!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        echo json_encode($json);
    }
    

    public function reimbursement_request()
    {
        $this->load->helper('url');
        $this->load->database();
        $technician_id = $this->input->post('tech_id');
        $start_date    = $this->input->post('start_date');
        $start_date    = date("Y-m-d", strtotime($start_date));
        $end_date      = $this->input->post('end_date');
        $end_date      = date("Y-m-d ", strtotime($end_date));
        $comment       = "";
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($technician_id == '' || $start_date == '' || $end_date == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        } else {
            $this->load->model('Punch_in');
            $res  = $this->Punch_in->reimbursement_request($technician_id, $start_date, $end_date, $comment, $company_id);
            $json = '';
            if ($res == 1) {
                $employee_id = $this->Punch_in->get_emp_id($this->input->post('tech_id'));
                $notify      = $employee_id . ' applied new reimbursement claim request!!!';
                $role        = "Manager";
                $key         = "";
                $this->load->model('Model_service');
                $this->Model_service->update_notify($notify, $company_id, $role, $technician_id, $key);
                $json = '';
                $json = array(
                    "status" => 1,
                    "msg" => "Reimbursement Request done!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        echo json_encode($json);
    }

    /*Store the ticekt start time, and status change */
    public function ticket_start()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id    = $this->input->post('tech_id');
        $ticket_id  = $this->input->post('ticket_id');
        $start_time = $this->input->post('start_time');
        $this->load->model('Punch_in');
        if ($tech_id == '' || $ticket_id == '' || $start_time == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        } else {
            $result = $this->Punch_in->ticket_start($tech_id, $ticket_id, $start_time);
            if ($result == 1) {
                $json = '';
                $json = array(
                    "status" => 1,
                    "msg" => "Ticket Started!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        echo json_encode($json);
    }

    /*This changed to ticket status to completed, which means waiting for customer feedback */
    public function complete_ticket()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id   = $this->input->post('tech_id');
        $ticket_id = $this->input->post('ticket_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($tech_id);
        
        if ($tech_id == '' || $ticket_id == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->complete_ticket($tech_id, $ticket_id, $company_id);
            if (!empty($result)) {
                return $result;
            }
        }
        
    }

    public function show_skill()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $result = $this->Punch_in->show_skill();
        return $result;
    }

    /* Technician escalating ticket with image, audio data*/
    public function submit_escalate()
    {
        $this->load->helper('url');
        $this->load->library('upload');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id = $this->input->post('ticket_id');
        $tech_id   = $this->input->post('tech_id');
        $img_dir1  = '';
        $audio1    = '';
        $video1    = '';
        if (!empty($_FILES["myFile"])) {
            $file_name = $_FILES["myFile"]["name"];
            $temp_name = $_FILES['myFile']['tmp_name'];
            $filename  = end((explode(".", $file_name)));
            $img_dir   = 'assets/images/escalated_image/' . $ticket_id .'.'. $filename;
            $img_dir1  = stripslashes($img_dir);
            move_uploaded_file($temp_name, $img_dir1);
        }
        if (!empty($_FILES["myAudio"])) {
            $file_name1 = $_FILES["myAudio"]["name"];
            $temp_name1 = $_FILES['myAudio']['tmp_name'];
            $filename1  = end((explode(".", $file_name1)));
            $audio      = 'assets/images/escalated_audio/' . $ticket_id . '.'. $filename1;
            $audio1     = stripslashes($audio);
            move_uploaded_file($temp_name1, $audio1);
        }
        if (!empty($_FILES["myVideo"])) {
            $file_name2 = $_FILES["myVideo"]["name"];
            $temp_name2 = $_FILES['myVideo']['tmp_name'];
            $filename2  = end((explode(".", $file_name2)));
            $video      = 'assets/images/escalated_video/' . $ticket_id . '.'. $filename2;
            $video1     = stripslashes($video);
            move_uploaded_file($temp_name2, $video1);
        }
        if ($img_dir1 != '' && $audio1 != '' && $video1 != '') {
            $data = array(
                'image' => $img_dir1,
                'audio' => $audio1,
                'video' => $video1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 == '' && $audio1 != '' && $video1 != '') {
            $data = array(
                'audio' => $audio1,
                'video' => $video1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 != '' && $audio1 == '' && $video1 != '') {
            $data = array(
                'image' => $img_dir1,
                'video' => $video1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 != '' && $audio1 != '' && $video1 == '') {
            $data = array(
                'image' => $img_dir1,
                'audio' => $audio1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 != '' && $audio1 == '' && $video1 == '') {
            $data = array(
                'image' => $img_dir1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 == '' && $audio1 != '' && $video1 == '') {
            $data = array(
                'audio' => $audio1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 == '' && $audio1 == '' && $video1 != '') {
            $data = array(
                'video' => $video1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 == '' && $audio1 == '' && $video1 == '') {
            $data = array(
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        }
        $result = $this->Punch_in->submit_escalate($ticket_id, $tech_id, $data);
        if ($result == 1) {
            $company_id  = $this->Punch_in->get_company($tech_id);
            $employee_id = $this->Punch_in->get_emp_id($tech_id);
            $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
            $ticket_history = $this->Punch_in->ticket_history($ticket_id, $tech_id,$company_id,$ticket_details['cust_id'], $ticket_details['mode_of_payment'], $ticket_details['total_amount'],$ticket_details['current_status']);
           
            
            $notify = $ticket_id . ' is escalated by ' . $employee_id;
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'MAnager', $tech_id);
           
            $json = '';
            $json = array(
                "status" => 1,
                "msg" => "You have successfully escalated the ticket!!!"
            );
           // $this->mail_pdf($ticket_id,$ticket_details['cust_id'],$company_id);
            $res1=$this->Punch_in->update_tech_task($tech_id);  
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        }
        
        echo json_encode($json);
    }

/*Update the ticket status to work in progress*/    
    public function work_in_progress()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id = $this->input->post('ticket_id');
        $tech_id   = $this->input->post('tech_id');
        $data      = array(
            'prev_tech_id' => $this->input->post('tech_id'),
            'reason' => $this->input->post('reason'),
            'poa' => $this->input->post('poa'),
            'schedule_next' => $this->input->post('schedule_next'),
            'cust_acceptance' => $this->input->post('cust_acceptance'),
            'estimation_time' => $this->input->post('estimate_time'),
            'current_status' => 10,
            'requested_spare' => $this->input->post('spare'),
            'spare_array' => $this->input->post('spare'),
            'cust_preference_date' => $this->input->post('schedule_next'),
            'lead_time' => $this->input->post('schedule_next')
        );
        $this->load->model('Punch_in');
        if ($this->input->post('tech_id') == '' || $this->input->post('ticket_id') == '' || $this->input->post('reason') == '' || $this->input->post('poa') == '' || $this->input->post('schedule_next') == '' || $this->input->post('cust_acceptance') == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all fields!!!"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->work_in_progress($data, $ticket_id);
            if ($result == 1) {
                $company_id  = $this->Punch_in->get_company($tech_id);
                $employee_id = $this->Punch_in->get_emp_id($tech_id);
                $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
                $ticket_history = $this->Punch_in->ticket_history($ticket_id, $tech_id,$company_id,$ticket_details['cust_id'], $ticket_details['mode_of_payment'], $ticket_details['total_amount'],$ticket_details['current_status']);
              
                $notify      = $ticket_id . ' is made  inprogress  by ' . $employee_id . ' on ' . $this->input->post('schedule_next');
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                $json = '';
                $json = array(
                    "status" => "1",
                    "msg" => "Your request has been updated successfully!!!"
                );
                
                //$this->mail_pdf($ticket_id,$ticket_details['cust_id'],$company_id);
            } else {
                $json = '';
                $json = array(
                    "status" => "0",
                    "msg" => "Error!!!"
                );
            }
            
            $res1=$this->Punch_in->update_tech_task($tech_id);  
            echo json_encode($json);
        }
    }
    
    /*Feedback form */
    public function Feedback()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->view('Feedback');
    }
    
    /*Close the ticket after customer feedback, and store the ticekt history */
    public function close_tkt()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('model_service');
        $this->load->model('Punch_in');
        $ticket_id=$this->input->post('ticket_id');
        $company_id=$this->Punch_in->get_company_tkt($ticket_id);
        $cust_feedback=$this->input->post('password');
        $cust_Rating=$this->input->post('c_password');
        $ticket_close_time  = date("Y-m-d H:i:s");
        $cust_reason="";
        $result=$this->model_service->close_tkt($company_id, $ticket_id,$cust_feedback,$cust_Rating,$cust_reason,$ticket_close_time);
        if($result==1)
        {   //ticket_history
            $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
            $ticket_history = $this->Punch_in->ticket_history($ticket_id, $ticket_details['tech_id'],$company_id,$ticket_details['cust_id'], $ticket_details['mode_of_payment'], $ticket_details['total_amount'],$ticket_details['current_status']);
            return true;
        }
        else
        {
            return false;
        }
    }
    /*
    Submit the spare amount, ticekt images, location
    
    Generate OTP and send to customer
    
    */
    public function submit_complete()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id      = $this->input->post('ticket_id');
        $tech_id        = $this->input->post('tech_id');
        $service_charge = $this->input->post('service_charge');
        $spare_charge   = $this->input->post('spare_charge');
        $tax            = $this->input->post('tax');
        $product_gst    = $this->input->post('product_gst');
        $service_gst    = $this->input->post('service_gst');
        $total_charge   = $this->input->post('total_charge');
        $signature      = '';
        
    
        if (!empty($_FILES["myFile"])) {
            $file_name = $_FILES["myFile"]["name"];
            $temp_name = $_FILES['myFile']['tmp_name'];
            $tmp = explode('.', $file_name);
            $filename  =end($tmp);
            $img_dir   = 'assets/images/' . 'completed_' . $ticket_id . '.'. $filename;
            $signature = stripslashes($img_dir);
            move_uploaded_file($temp_name, $signature);
        }
        else
        {
            $signature ='';
        }

        $frm              = $this->input->post('frm');
        $drop_of_date     = $this->input->post('drop_of_date');
        $drop_of_date     = date("Y-m-d H:i:s", strtotime($drop_of_date));
        $drop_of_location = $this->input->post('drop_of_location');
        $ticket_end_time  = date("Y-m-d H:i:s");
        $company_id       = $this->Punch_in->get_company($tech_id);
        $check_no=$this->input->post('check_no');
        if($check_no=='')
        {
          $checkdate='';
        }
        else
        {
          $checkdate=$this->input->post('check_date');
        }
        if($total_charge=='0')
        {
          $bill_status='0';
        }
        else
        {
           $bill_status='1';
        }
        if ($signature != '') {
            $data = array(
                'company_id' => $company_id,
                'ticket_id' => $ticket_id,
                'tech_id' => $tech_id,
                'service_charge' => $service_charge,
                'spare_amount' => $spare_charge,
                'tax' => $tax,
                'product_gst' => $product_gst,
                'service_gst' => $service_gst,
                'total' => $total_charge,
                'signature' => $signature,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $checkdate,
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time
            );
        } else {
            $data = array(
                'company_id' => $company_id,
                'ticket_id' => $ticket_id,
                'tech_id' => $tech_id,
                'service_charge' => $service_charge,
                'spare_amount' => $spare_charge,
                'tax' => $tax,
                'product_gst' => $product_gst,
                'service_gst' => $service_gst,
                'total' => $total_charge,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $checkdate,
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time
            );
        }

        $result = $this->Punch_in->submit_complete($ticket_id, $tech_id, $data);
        if ($result == 1) {
            $product     = $this->Punch_in->get_tech_product($tech_id);
            $company_id  = $product[0]['company_id'];
            $employee_id = $this->Punch_in->get_emp_id($tech_id);
            $notify      = $ticket_id . ' is completed by ' . $employee_id;
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
          
            $json = '';
            $code = mt_rand(10001,99999);
            $data = array(
                'service_charge' => $service_charge,
                'spare_cost' => $spare_charge,
                'tax' => $tax,
                'product_gst' => $product_gst,
                'service_gst' => $service_gst,
                'total_amount' => $total_charge,
                'bill_status' => $bill_status,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $checkdate,
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time,
                'last_update' =>$ticket_end_time,
                'signature' =>$signature,
                //'current_status' => 8,
                'otp_code'=> $code
            );
           
            

          
            $res  = $this->Punch_in->submit_complete1($ticket_id, $tech_id, $data,$cust_mobile,$code);  ////// all_tickets table
           if ($res == 1) {
                $json = array(
                    "status" => 1,
                    "OTP" => $code,
                    "msg" => "Completed Successfully done!!!"
                );
               echo json_encode($json);
                $cst  = $this->Punch_in->get_det_cust($ticket_id);
                $cust_id=$cst['customer_id'];
                 $to=$cst['email_id'];
        /*$emailcontent = "Dear Customer, ";
      //  $emailcontent.='<br>';
        $emailcontent.="If you are satisfied with our service please share this OTP with technician ";
        $emailcontent.='<br>';
        $emailcontent.= $code;
        $emailcontent.='<br>Thanks & Regards';
        $emailcontent.='<br>Capricot Team';  
        */
               $emailcontent=" <!doctype html>
                <html>
                <head>
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
               
                <body>
                <p>Dear Customer,</p>
                <p>Welcome to Capricot.</b></p>
                <p>If you are satisfied with our service.</p>
                <p>Please share below OTP with Technician.</p>
                <p>$code</p> 
                <p>Thanks & regards,</p> <p>Capricot Team.</p>

                </body>
                </html>";
                 
     //  $emailcontent = "Dear Customer if you satisfy with our service please share this OTP with technician - ".$code;  ////raju
          $config   = Array(
            'protocol' => 'smtp',
                  'smtp_host' => 'smtp.office365.com',
                  'smtp_port' => '587',
                  'smtp_timeout' => '30',
                  'smtp_user' => 'hwsupport@capricot.com',
                  'smtp_pass' => 'Support14',
                  'mailtype' => 'html',
                  'smtp_crypto' => 'tls',
                  'wordwrap' => TRUE,
                  'charset' => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('hwsupport@capricot.com', 'Capricot');
        $this->email->to($to);
        $this->email->subject('Capricot - Customer OTP Verfication');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
    //   $this->email->send();
       
            }
           
            $this->load->model('Punch_in');
            $res1=$this->Punch_in->update_tech_task($tech_id);
            
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        echo json_encode($json);    
        }
        
        
         $cus_mo  = $this->Punch_in->get_det_cust($ticket_id);
            $cust_mobile = $cus_mo['contact_number'];
            $cust_id = $cus_mo['customer_id'];
    if ($cust_mobile != " " || $cust_mobile != NULL) {  
$this->load->model('Pushnotify');       
$number=$cust_mobile;
$message="Dear Customer, if you satisfied with our service please share this OTP with technician - ".$code;
$smsvalue = array('message'=>$message,'number'=>$number);
$this->Pushnotify->sms_send_mobile($smsvalue);  
 }  
    }

    /*
    Return the consumed spare items

    Send Feedback form to customer
    */
    public function frm_complete_ticket(){
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id      = $this->input->post('ticket_id');
        $tech_id        = $this->input->post('tech_id');      
        $frm            = $this->input->post('frm');
        $frm_status     = $this->input->post('frm_status');
   
        $drop_of_date     = $this->input->post('drop_of_date');
        $drop_of_date     = date("Y-m-d H:i:s", strtotime($drop_of_date));
        $drop_spare       = $this->input->post('drop_spare');
        $drop_of_location = $this->input->post('drop_of_location');
        $company_id       = $this->Punch_in->get_company($tech_id);
        
        if($frm_status == 'now'){
            $fstatus = "RETURNED";
        }
        elseif($frm_status == 'later'){
            $fstatus = "PENDING";
        }
        else{
            $fstatus = "NO FRM";
        }

        $billing_data = array(
            'frm' => $frm,
            'drop_spare' => $drop_spare,
            'drop_of_date' => $drop_of_date,
            'drop_of_location' => $drop_of_location,
            'frm_status'    => $fstatus
           
        );

        $ticket_data = array(
            'frm' => $frm,
            'drop_spare' => $drop_spare,
            'drop_of_date' => $drop_of_date,
            'drop_of_location' => $drop_of_location,
            'current_status' =>8
        );

        $result = $this->Punch_in->frm_complete_ticket($ticket_id, $tech_id, $company_id, $billing_data, $ticket_data);
      
        if($result == 1){
      /*  $keytext=uniqid();
        $key = $ticket_id."".md5($keytext)."".time();
        $getdetails=$this->Punch_in->storeratingkey($ticket_id,$company_id,$key);
        $to=$getdetails['email_id'];
        $tonumber=$getdetails['contact_number'];
      */
            /*$emailcontent = "Dear Customer, ";
        $emailcontent.='<br>';
        $emailcontent.="please take some time to rate our service in the following link ";
        $emailcontent.='<br>';
        $emailcontent.=base_url() . "index.php?/Controller_service/Feedback/?token=".$key;
        $emailcontent.='<br>Thanks & Regards';
        $emailcontent.='<br>Capricot Team';  
        */
            
        /*     $url=base_url().'index.php?/Controller_service/Feedback/?token='.$key;
    $emailcontent=" <!doctype html>
                <html>
                <head>
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
               
                <body>
                <p>Dear Customer,</p>
                <p>Welcome to Capricot.</b></p>
                <p>please take some time to rate our service.</p>
                <p>Click the below link.</p>
                <p>$url</p> 
                <p>Thanks & regards,</p> <p>Capricot Team.</p>

                </body>
                </html>";   

        $mobilemessage ="Dear Customer, ";
        $mobilemessage.="Please take some time to rate our service in the following link ";
        $mobilemessage.= base_url() . "index.php?/Controller_service/Feedback/?token=".$key;
        $mobilemessage.=' Thanks & Regards';
        $mobilemessage.=' Capricot Team';   
        */

       /* $config   = Array(
             'protocol' => 'smtp',
                  'smtp_host' => 'smtp.office365.com',
                  'smtp_port' => '587',
                  'smtp_timeout' => '30',
                  'smtp_user' => 'hwsupport@capricot.com',
                  'smtp_pass' => 'Support14',
                  'mailtype' => 'html',
                  'smtp_crypto' => 'tls',
                  'wordwrap' => TRUE,
                  'charset' => 'utf-8'
        );
         $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('hwsupport@capricot.com', 'Capricot');
        $this->email->to($to);
        $this->email->subject('Capricot - Customer Feedback');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
        
        */
         
        /*  $config   = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'kaspondevelopers@gmail.com',
            'smtp_pass' => 'Kaspon@123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );  
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('kaspondevelopers@gmail.com', 'Capricot');
        $this->email->to($to);
        $this->email->subject('Capricot - Customer Feedback');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
      
        $this->email->send();
        */
            /*
        $apiKey = urlencode('2EodOA4pMpk-bmz7D8FaLwCWJ2n6XBK4fdXN2hU8hR');
        $sender = urlencode('TXTLCL');     
       // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $tonumber, "sender" => $sender, "message" => rawurlencode($mobilemessage));
      
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        */
            
   
             //ticket_history
             $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
             $ticket_history = $this->Punch_in->ticket_history($ticket_id, $tech_id,$company_id,$ticket_details['cust_id'], $ticket_details['mode_of_payment'], $ticket_details['total_amount'],$ticket_details['current_status']);
            
            $json = '';
            $json = array(
                "status" => 1,
                "msg" => "Success"
            );
            //$this->mail_pdf($ticket_id, $ticket_details['cust_id'], $company_id);
        }
        else{
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Failure"
            );
        }

        echo json_encode($json);
/*
if ($tonumber != " " || $tonumber != NULL) {    
$this->load->model('Pushnotify');           
$number=$tonumber;
$message=$mobilemessage;
$smsvalue = array('message'=>$message,'number'=>$number);
$this->Pushnotify->sms_send_mobile($smsvalue);  
 }      
     */     
        
    }


    /*
    Return the consumed materials

    Status changed to RETURNED
    */
    public function return_pending_frm(){

        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
       $ticket_id      = $this->input->post('ticket_id');
        $tech_id        = $this->input->post('tech_id');      
        $frm_status     = $this->input->post('frm_status'); 
        
        //$drop_spare     = $this->input->post('drop_spare'); 
        $company_id     = $this->Punch_in->get_company($tech_id); 
        

        if ($ticket_id == '' || $tech_id == '' || $frm_status =='' ) {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Please provide all details!!!"
            );
        } 
        else{
         $result = $this->Punch_in->return_pending_frm($ticket_id, $tech_id, $company_id);
        

         if($result == 1){
            $json = '';
            $json = array(
                "status" => 1,
                "msg" => "Success"
            );
        }
        else{
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Failure"
            );
        }
     

        }
        echo json_encode($json);

    }

    /*Get ticket details based on technician */
    public function get_tech_tkt()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $start   = $this->input->post('start');
        $end     = $this->input->post('end');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->get_tech_tkt($tech_id, $start, $end);
        $json   = '';
        $json   = array(
            "status" => 1,
            "result" => $result
        );
        echo json_encode($json);
    }

    /*Send ticket history details to pdf download */
    public function pdf_report()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        $tech_id    = $this->input->post('tech_id');
        $from       = $this->input->post('from');
        $to         = $this->input->post('to');
        $result     = $this->Punch_in->pdf_report($tech_id, $company_id, $from, $to);
        $json       = '';
        $json1      = '';
    
     // print_r($result);
    //  die;
      
          if (!empty($result)) {
            

           $table ='<html>
           <head>
           <style>
           table.bottomBorder { 
            border-collapse: collapse; 
          }
          table.bottomBorder td, 
          table.bottomBorder th { 
            border-bottom: 1px solid yellowgreen; 
            padding: 10px; 
            text-align: left;
          }

           </style>
           </head> 
           <strong>Capricot - Service Report</strong>
           <br/>
           <table>         
           <tr>
           <td>
           Employee ID:
           </td>
           <td>
           <strong>'.$result[0]['employee_id'].'</strong>
           </td>
           </tr>
           <tr>
           <td>
           Technician name:
           </td>
           <td>
           <strong>'.$result[0]['first_name'].'</strong>
           </td>
           </tr>
           </table>
           <br/>

           <table class="bottomBorder" align="center">
         
           <thead>
             <tr style="border: 1px solid black; background-color: #01B6AD">
               <th style="border: 1px solid black">Date</th>
               <th style="border: 1px solid black">Ticket id</th>
               <th style="border: 1px solid black">Customer name</th>
               <th style="border: 1px solid black">Status</th>
             </tr>
           </thead>
           <tbody>';
           foreach ($result as $row) {
            if($row['status']==10 )
            {
                $status='Work in progress';
               
            }
            elseif($row['status']==11)
            {
                $status="Spare accepted";
              
            }
            elseif($row['status']==12)
            {
                $status ="Closed";
                
            }
            elseif($row['status']==14)
            {
                $status="Spare requested";
               
            }
            elseif($row['status']==15)
            {
                $status="Escalate";
               
            }
            elseif($row['status']==18)
            {
                $status="Spare Delivered";
            }
            elseif($row['status']==8)
            {
                $status="Completed";
               
            }
            elseif($row['status']==7)
            {
                $status="Ticket Start";
               
            }
            
            $table = $table . ' <tr>
                                <td style="border: 1px solid black">'.$row['date'].'</td>
                                <td style="border: 1px solid black">'.$row['ticket_id'].'</td>
                                <td style="border: 1px solid black">'.$row['customer_name'].'</td>
                                <td style="border: 1px solid black">'.$status.'</td>
                               </tr>';
            
           }
           $table = $table . '</tbody></table></html>';

            $json = array(
                "status" => 1,
                "result" => $table
            );
        
           
        }
        else{
            $table = '<html><body><form><p>Employee ID:'.$result[0]['employee_id'].'</p><p> Technician name:'.$result[0]['first_name'].'</p><br/><br/></form><table style="width:100%;border: 0px solid black; border-collapse: collapse;">
                <tr style="border: 1px solid black; background-color: #01B6AD">
              <th style="border: 1px solid black">Date</th>
               <th style="border: 1px solid black">Ticket id</th>
               <th style="border: 1px solid black">Customer name</th>
               <th style="border: 1px solid black">Status</th>
            </tr>';
                
            $table = $table.'<tr style="border: 1px solid black; background-color: #eee;"><td colspan="4" style="border: 1px solid black">No data found</td></tr>';
                          
            $table = $table.'</table></body> </html>';
        }

      
        $json = array(
            "status" => 1,
            "result" => $table
        );
        echo json_encode($json);
    }


    /*Send Reimbursement history to download in pdf format */
    public function reimbursement_report()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        $tech_id    = $this->input->post('tech_id');
        $from       = $this->input->post('from');
        $to         = $this->input->post('to');
        $result     = $this->Punch_in->reimbursement_report($tech_id, $company_id, $from, $to);
        $json       = '';
        $json1      = '';
        if (!empty($result)) {
            
            $json1 = '<html><body><form><p>Employee ID:'. $result[0]['employee_id']  .'</p><p> Technician name:' . $$result[0]['first_name'] . '</p><br/><br/> </form> <table style="width:100%;border: 0px solid black; border-collapse: collapse;"> <tr style="border: 1px solid black; background-color: #eee;"> <th style="border: 1px solid black">S.No</th><th style="border: 1px solid black">Date</th><th style="border: 1px solid black">Ticket ID</th><th style="border: 1px solid black">Customer Name</th> <th style="border: 1px solid black">From</th> <th style="border: 1px solid black">To</th>          <th style="border: 1px solid black">Distance</th>          <th style="border: 1px solid black">Mode of Travel</th>          <th style="border: 1px solid black">Amount</th>         </tr>   ';
            
            // foreach ($result as $row) {
                
            //     $json1 = $json1 . '    <tr style="border: 1px solid black; background-color: #eee;"><td style="border: 1px solid black">' . $row['reim_id'] . '</td> <td style="border: 1px solid black">' . $row['date'] . '</td><td style="border: 1px solid black">' . $row['ticket_id'] . '</td> <td style="border: 1px solid black">' . $row['customer_name'] . '</td> <td style="border: 1px solid black">' . $row['source_Address'] . '</td> <td style="border: 1px solid black">' . $row['destin_Address'] . '</td> <td style="border: 1px solid black">' . $row['no_of_km_travelled'] . '</td> <td style="border: 1px solid black">' . $row['mode_of_travel'] . '</td> <td style="border: 1px solid black">' . $row['travelling_charges'] . '</td></tr>  ';
                
            // }
            // $json1 = $json1 . '</table> </body> </html>';


            $table ='<html>
            <head>
            <style>
           table.bottomBorder { 
            border-collapse: collapse; 
          }
          table.bottomBorder td, 
          table.bottomBorder th { 
            border-bottom: 1px solid yellowgreen; 
            padding: 10px; 
            text-align: left;
          }

           </style>

            </head> 

            <strong>Capricot - Reimbursement Report</strong>
            <br/>
            <table>         
            <tr>
            <td>
            Employee ID:
            </td>
            <td>
            <strong>'.$result[0]['employee_id'].'</strong>
            </td>
            </tr>
            <tr>
            <td>
            Technician name:
            </td>
            <td>
            <strong>'.$result[0]['first_name'].'</strong>
            </td>
            </tr>
            </table>
            <br/>
            <table style="table-layout:fixed;border-collapse:collapse;">
            <thead>
            <tr style="border: 1px solid black; background-color: #01B6AD">
            <th style="border: 1px solid black">S.No</th>
            <th style="border: 1px solid black">Date</th>
            <th style="border: 1px solid black">Ticket ID</th>
            <th style="border: 1px solid black">Customer Name</th>
            <th style="border: 1px solid black">From</th>
            <th style="border: 1px solid black">To</th>
            <th style="border: 1px solid black">Distance</th>
            <th style="border: 1px solid black">Mode of travel</th>
            <th style="border: 1px solid black">Amount</th>
             </tr>
            </thead>
            <tbody>';
            $i=1;
            $sum =0;
            foreach ($result as $row) {
                 
             $table = $table.' <tr>
                                 <td style="border: 1px solid black">'.$i.'</td>
                                 <td style="border: 1px solid black">'.$row['date'].'</td>
                                 <td style="border: 1px solid black; width: 80px">'.$row['ticket_id'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word;width: 80px">'.$row['customer_name'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word; width: 100px">'.$row['source_Address'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word; width: 100px">'.$row['destin_Address'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word">'.$row['no_of_km_travelled'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word; width: 80px">'.$row['mode_of_travel'].'</td>
                                 <td style="border: 1px solid black">'.$row['travelling_charges'].'</td>                               
                                </tr>';
                                 $sum+= $row['travelling_charges'];
                                $i++;
             
            }
            $table = $table . '</tbody></table>
            <p id="total" style="text-align: right"><strong>Total Amount(INR):</strong>  '.$sum.' </p>
            </html>';
        }
        else{
            $table = '<html><body><table style="width:100%;border: 0px solid black; border-collapse: collapse;"> <tr style="border: 1px solid black; background-color: #eee;"> <th style="border: 1px solid black">S.No</th><th style="border: 1px solid black">Date</th><th style="border: 1px solid black">Ticket ID</th><th style="border: 1px solid black">Customer Name</th> <th style="border: 1px solid black">From</th> <th style="border: 1px solid black">To</th>          <th style="border: 1px solid black">Distance</th>          <th style="border: 1px solid black">Mode of Travel</th><th style="border: 1px solid black">Amount</th></tr>';
                
            $table = $table.'<tr style="border: 1px solid black; background-color: #eee;"><td colspan="4" style="border: 1px solid black">No data found</td></tr>';
                          
            $table = $table.'</table></body> </html>';
        }
        $json = array(
            "status" => 1,
            "result" => $table
        );
        echo json_encode($json);
    }
    
    /* 
    Retrive and send the AMC details
    Generate AMC id
    Generate Customers
    If AMC already exist, it will update*/
    public function amc_08_may_2019()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $amc_details = array();
        $amc_details=$this->input->post('amc_details'); 
       // echo 'hello'.$amc_details;
       // echo $amc_details['amc_type'];
        $array1=json_decode($amc_details);
        $json = '';

       // echo ''.$array1[0]->amc_type;
        $arrayfinal=get_object_vars($array1);
      //  print_r($arrayfinal);
        $inputarray=array();
      // echo  count($arrayfinal);
        for($j=0;$j<count($array1);$j++)
        {
           
            $inputarray[]=get_object_vars($array1[$j]);
        }
     
        foreach($inputarray as $amc)
        {
          
            $amc_id     = $this->Punch_in->select_amc();
            $ids        = $this->Punch_in->get_ids($amc['product'], $amc['category']);
            $product_id = $ids[0]['product_id'];
           
            $cat_id     = $ids[0]['cat_id'];
            $company_id = $this->Punch_in->get_company($amc['tech_id']);
            $myStr      =$amc['amc_type'];
            $tic_id=$amc['ticket_id'];
            $result     = substr($myStr, 0, 4);
            $gen_amc_id = $result . '_' . $amc_id;
            $start_date = date('Y-m-d');
            $noof_month = $amc['contract_period'];
            $end_date = date('Y-m-d', strtotime('+'.$noof_month.' month'));
            if ($amc['tech_id'] == '' || $amc['amc_type'] == '' || $amc['contract_period'] == '' || $amc['product'] == '' 
            || $amc['category'] == '' || $amc['quantity'] == '' || $amc['total_amount'] == '' || $amc['mode_of_payment'] == '') {
               
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide all details for ".$amc['tech_id']
                );
            } else {
                if ($amc['cust_id'] == '') {
                $this->load->model('model_man');
                $userid = $this->model_man->get_userid($amc['email_id']);
                $contract_value = $amc['total_amount'];
                if (count($userid) == 0) {
                    $cutomer = array(
                        'customer_name' => $amc['cust_name'],
                        'email_id' => $amc['email_id'],
                        'contact_number' =>$amc['contact_number'],
                        'address' => $amc['street'],
                        //'product_serial_no' => $product_id,
                        //'component_serial_no' => $cat_id,
                        'product_serial_no' =>$amc['product'],
                        'component_serial_no' =>$amc['category'],
                        'serial_no' => $amc['serial_no'],
                        'model_no' =>$amc['model_no'],
                        'door_num' => $amc['door_no'],
                        'cust_town' =>$amc['city'],
                        'cust_country' =>$amc['country'],
                        'company_id' => $company_id,
                        'type_of_contract'=>$amc['amc_type'],
                        'start_date'=>$start_date,
                        'end_date'=>$end_date,
                        'contract_value' => $contract_value
                    );
                    
                   
                    $this->load->model('Punch_in');
                    $gen_cust = $this->Punch_in->gen_customer($cutomer);
                    $amc1      = $this->Punch_in->select_amc();
                    $this->load->model('New_amc');
                    if($tic_id=="")
                    {
                        $ticket_id  = $this->New_amc->tick_id($company_id);
                    }
                    else
                    {
                        $ticket_id=$tic_id;
                    }
                    $check_date = date("Y-m-d H:i:s", strtotime($amc['check_date']));
                    $data       = array(
                        'ticket_id' => $ticket_id,
                        'amc_id' => $gen_amc_id,
                        'tech_id' => $amc['tech_id'],
                        'call_type' => $amc['amc_type'],
                        'contact_no' =>$amc['contact_number'],
                        'cust_id' => $gen_cust,
                        'address' => $amc['street'],
                        'door_no' => $amc['door_no'],
                        'town' =>$amc['city'],
                        'country' => $amc['country'],
                        'contract_period' => $amc['contract_period'],
                        'product_id' => $amc['product'],
                        'cat_id' => $amc['category'],
                        'quantity' => $amc['quantity'],
                        'total_amount' => $amc['total_amount'],
                        'mode_of_payment' => $amc['mode_of_payment'],
                        'check_no' => $amc['check_no'],
                        'check_date' => $check_date,
                        'transaction_id' =>$amc['transaction_id'],
                        'serial_no' => $amc['serial_no'],
                        'model' =>$amc['model_no'],
                        'current_status' => 12,
                        'company_id' => $company_id
                    );
                    
                   
                    
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Email ID already registered!!!"
                    );
                }
            } else {
                $this->load->model('Punch_in');
                $amc1 = $this->Punch_in->select_amc();
                $this->load->model('New_amc');
               
                if($tic_id=="")
                {
                    $ticket_id  = $this->New_amc->tick_id($company_id);
                }
                    else
                    {
                        $ticket_id=$tic_id;
                    }
                   
                    $data      = array(
                    'ticket_id' => $ticket_id,
                    'amc_id' => $gen_amc_id,
                    'tech_id' => $amc['tech_id'],
                    'call_type' => $amc['amc_type'],
                    'contact_no' => $amc['contact_number'],
                    'cust_id' => $amc['cust_id'],
                    'address' =>$amc['street'],
                    'door_no' => $amc['door_no'],
                    'town' => $amc['city'],
                    'country' => $amc['country'],
                    'contract_period' => $amc['contract_period'],
                    'product_id' =>$amc['product'],
                    'cat_id' =>$amc['category'],
                    'quantity' => $amc['quantity'],
                    'total_amount' =>$amc['total_amount'],
                    'mode_of_payment' =>$amc['mode_of_payment'],
                    'check_no' => $amc['check_no'],
                    'check_date' => $amc['check_date'],
                    'transaction_id' => $amc['transaction_id'],
                    'serial_no' => $amc['serial_no'],
                    'model' => $amc['model_no'],
                    'current_status' => 12,
                    'company_id' => $company_id
                );
            }
           
            $result = $this->Punch_in->amc($data,$ticket_id);
            if ($result == 1) {
                
                $result=$this->Punch_in->updte_amc($tic_id);
                $tech_id     = $amc['tech_id'];
                $employee_id = $this->Punch_in->get_emp_id($tech_id);
                $notify      = 'New Contract raised  by ' . $employee_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                $json = array(
                    "status" => 1,
                    "msg" => "AMC Successfully done!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
            
        }
        echo json_encode($json);
    }
    
      /* 
    Retrive and send the AMC details
    Generate AMC id
    Generate Customers
    If AMC already exist, it will update*/
    public function amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
       
      
        $product=$this->input->post('product'); 
        $category=$this->input->post('category'); 
        $tech_id=$this->input->post('tech_id'); 
        $amc_type=$this->input->post('amc_type'); 
        $ticket_id=$this->input->post('ticket_id'); 
        $amc_worktype=$this->input->post('work_type'); 
        $contract_period=$this->input->post('contract_period');
        $quantity=$this->input->post('quantity');
        $total_amount=$this->input->post('total_amount');
        $mode_of_payment=$this->input->post('mode_of_payment');
        
        $cust_id=$this->input->post('cust_id');
        $email_id=$this->input->post('email_id');
        $cust_name=$this->input->post('cust_name');
        
        $contact_number=$this->input->post('contact_number');
        $street=$this->input->post('street');
        $serial_no=$this->input->post('serial_no');
        $model_no=$this->input->post('model_no');
        $door_no=$this->input->post('door_no');
        $city=$this->input->post('city');
        $country=$this->input->post('country');
        
        $check_no=$this->input->post('check_no');
        $check_date=$this->input->post('check_date');
        $transaction_id=$this->input->post('transaction_id');
     
      //  foreach($inputarray as $amc)
     //   {
          
            $amc_id     = $this->Punch_in->select_amc();
            $ids        = $this->Punch_in->get_ids($product, $category);
            $product_id = $ids[0]['product_id'];    
            $cat_id     = $ids[0]['cat_id'];
            $company_id = $this->Punch_in->get_company($tech_id);
            $myStr      =$amc_type;
            $tic_id=$ticket_id;
            $result     = substr($myStr, 0, 4);
            $gen_amc_id = $result . '_' . $amc_id;
            $start_date = date('Y-m-d');
            $noof_month = $contract_period;
            $end_date = date('Y-m-d', strtotime('+'.$noof_month.' month'));
            if ($tech_id == '' || $amc_type == '' || $contract_period == '' || $product == '' 
            || $category == '' || $quantity == '' || $total_amount == '' || $mode_of_payment == '') {
               
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide all details for ".$tech_id
                );
            } else
            {
     
                
                if($amc_worktype==7)
                    {
                        $custamctype=1;
                    $contractamctype=1;
                    }
                    else
                    {
                        $custamctype=0;
                        $contractamctype=0;
                    }
                
                $this->load->model('model_man');
                $customer_id = $this->model_man->get_customer_prod_cat_serial($cust_id,$product_id,$cat_id,$serial_no);
                $contract_value = $total_amount;
                if (count($customer_id) == 0) {
                    $cutomer = array(
                        'customer_id' => $cust_id,
                        'customer_name' => $cust_name,
                        'email_id' => $email_id,
                        'contact_number' => $contact_number,
                        'address' => $street,
                        'product_serial_no' =>$product_id,
                        'component_serial_no' =>$cat_id,
                        'serial_no' =>$serial_no,
                        'model_no' =>$model_no,
                        'door_num' =>$door_no,
                        'cust_town' =>$city,
                        'cust_country' =>$country,
                        'company_id' => $company_id,
                        'type_of_contract'=>$amc_type,
                        'start_date'=>$start_date,
                        'end_date'=>$end_date,
                        'contract_value' => $contract_value,
                        'cust_type' => $custamctype,
                        'contract_status' => 0
                    );
                   
                //  print_r($cutomer);
                //  die;
                   
                    $this->load->model('Punch_in');
                    $gen_cust = $this->Punch_in->gen_customer_ticket($cutomer);
                    $amc1      = $this->Punch_in->select_amc();
                    $this->load->model('New_amc');
                    if($tic_id=="")
                    {
                        $ticket_id  = $this->New_amc->tick_id($company_id);
                    }
                    else
                    {
                        $ticket_id=$tic_id;
                    }
                    if($check_date=='')
                    {
                        $check_date = '';
                    }
                    else
                    {
                    $check_date = date("Y-m-d H:i:s", strtotime($check_date));
                    }
                    
                     $ticket_end_time = date('Y-m-d H:i:s');
                    
                    $data       = array(
                        'ticket_id' => $ticket_id,
                        'amc_id' => $gen_amc_id,
                        'tech_id' => $tech_id,
                        'contact_no' => $contact_number,
                        'cust_id' => $cust_id,
                        'address' => $street,
                        'door_no' => $door_no,
                        'town' =>$city,
                        'country' => $country,
                        'work_type'=>$amc_worktype,
                         'call_type'=>$amc_type,
                        'contract_period' => $contract_period,
                        'product_id' => $product_id,
                        'cat_id' => $cat_id,
                        'quantity' => $quantity,
                        'total_amount' => $total_amount,
                        'mode_of_payment' => $mode_of_payment,
                        'check_no' => $check_no,
                        'check_date' => $check_date,
                        'transaction_id' =>$transaction_id,
                        'serial_no' => $serial_no,
                        'model' =>$model_no,
                        'ticket_end_time' =>$ticket_end_time,
                        'last_update' =>$ticket_end_time,
                        'current_status' => 12,
                        'company_id' => $company_id
                    );
                    
                   
                    
                } 
                else
                {
                    
                    $this->load->model('Punch_in');
                $gen_cust = $this->Punch_in->update_customer_serail_ticket($cust_id,$product_id,$cat_id,$serial_no);
                $amc1 = $this->Punch_in->select_amc();
                $this->load->model('New_amc');
               
                if($tic_id=="")
                {
                    $ticket_id  = $this->New_amc->tick_id($company_id);
                }
                    else
                    {
                        $ticket_id=$tic_id;
                    }
                   if($check_date=='')
                    {
                        $check_date = '';
                    }
                    else
                    {
                    $check_date = date("Y-m-d H:i:s", strtotime($amc['check_date']));
                    }
                    $data      = array(
                        
                    'ticket_id' => $ticket_id,
                    'amc_id' => $gen_amc_id,
                    'tech_id' => $tech_id,
                    'contact_no' => $contact_number,
                    'cust_id' => $cust_id,
                    'address' =>$street,
                    'door_no' => $door_no,
                    'town' => $city,
                    'country' => $country,
                    'work_type'=>$amc_worktype,
                    'call_type'=>$amc_type,
                    'contract_period' => $contract_period,
                    'product_id' =>$product_id,
                    'cat_id' =>$cat_id,
                    'quantity' => $quantity,
                    'total_amount' =>$total_amount,
                    'mode_of_payment' =>$mode_of_payment,
                    'check_no' => $check_no,
                    'check_date' => $check_date,
                    'transaction_id' => $transaction_id,
                    'serial_no' => $serial_no,
                    'model' => $model_no,
                    'ticket_end_time' =>$ticket_end_time,   
                    'last_update' =>$ticket_end_time,   
                    'current_status' => 12,
                    'company_id' => $company_id
                );
                    
                }
        /*    } else
                {
                $this->load->model('Punch_in');
                $amc1 = $this->Punch_in->select_amc();
                $this->load->model('New_amc');
               
                if($tic_id=="")
                {
                    $ticket_id  = $this->New_amc->tick_id($company_id);
                }
                    else
                    {
                        $ticket_id=$tic_id;
                    }
                   
                    $data      = array(
                    'ticket_id' => $ticket_id,
                    'amc_id' => $gen_amc_id,
                    'tech_id' => $amc['tech_id'],
                    'call_type' => $amc['amc_type'],
                    'contact_no' => $amc['contact_number'],
                    'cust_id' => $amc['cust_id'],
                    'address' =>$amc['street'],
                    'door_no' => $amc['door_no'],
                    'town' => $amc['city'],
                    'country' => $amc['country'],
                    'contract_period' => $amc['contract_period'],
                    'product_id' =>$amc['product'],
                    'cat_id' =>$amc['category'],
                    'quantity' => $amc['quantity'],
                    'total_amount' =>$amc['total_amount'],
                    'mode_of_payment' =>$amc['mode_of_payment'],
                    'check_no' => $amc['check_no'],
                    'check_date' => $amc['check_date'],
                    'transaction_id' => $amc['transaction_id'],
                    'serial_no' => $amc['serial_no'],
                    'model' => $amc['model_no'],
                    'current_status' => 12,
                    'company_id' => $company_id
                );
            }
           */
            $result = $this->Punch_in->amc($data,$ticket_id);
            if ($result == 1) {
                
               // $result=$this->Punch_in->updte_amc($tic_id);
                $tech_id     = $tech_id;
                $employee_id = $this->Punch_in->get_emp_id($tech_id);
                $notify      = 'New Contract raised  by ' . $employee_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                $json = array(
                    "status" => 1,
                    "msg" => "AMC Successfully done!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
            
      //  }
        echo json_encode($json);
    }
    
    
    /*
    Send the AMC details to technicians
    */
    public function search_amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $search_var = $this->input->post('amc_id');
        $company_id = $this->input->post('company_id');
        //$search_var='Che/Part/sep/2017/0001';
        $this->load->model('Punch_in');
        $result = $this->Punch_in->search_amc($search_var,$company_id);
        if (!empty($result)) {
            $json = array(
                "status" => 1,
                "result" => $result
            );
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Invalid AMC id!!!"
            );
        }
        echo json_encode($json);
    }


/*Check the AMC is already exist or not */
public function find_amc(){
    $this->load->helper('url');
    $this->load->database();
    $search_var = $this->input->post('contact_number');
    $company_id = $this->input->post('company_id');
    $this->load->model('Punch_in');
    $result = $this->Punch_in->find_amc($search_var,$company_id);
    if($result == 1){
        $json = array(
            "status" => 1,
            "result" => "AMC already exist"
        );
    }
    else{
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Not exist"
        );
    }
    echo json_encode($json);
}

/* Send the AMC amount details */
    public function amc_price()
    {
        $this->load->helper('url');
        $this->load->database();
        $cat_name = $this->input->post('cat_name');
        $quantity = $this->input->post('quantity');
        $tech_id  = $this->input->post('tech_id');
        $amc_type = $this->input->post('amc_type');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->amc_price($cat_name, $tech_id, $amc_type, $quantity);
        if (!empty($result)) {
            $json = array(
                "status" => 1,
                "total_amount" => $result
            );
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Invalid category!!!"
            );
        }
        echo json_encode($json);
    }

    /* Update the existing AMC */
    public function update_amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $search_var      = $this->input->post('amc_id');
        $tech_id         = $this->input->post('tech_id');
        $contract_period = $this->input->post('contract_period');
        $type_cont       = $this->input->post('type_cont');
        $total_amount    = $this->input->post('total_amount');
        $mode_of_payment = $this->input->post('mode_of_payment');
        $check_no        = $this->input->post('check_no');
        $check_date      = $this->input->post('check_date');
        $search_var      = $this->input->post('amc_id');
        $this->load->model('Punch_in');
        $resu       = $this->Punch_in->search_amc($search_var);
        $ids        = $this->Punch_in->get_ids($this->input->post('product'), $this->input->post('category'));
        $product_id = $ids[0]['product_id'];
        $cat_id     = $ids[0]['cat_id'];
        if (!empty($resu)) {
            foreach ($resu as $row) {
                $data   = array(
                    'tech_id' => $tech_id,
                    'call_type' => $row['type_cont'],
                    'contact_no' => $row['contact_num'],
                    'address' => $row['street'],
                    'door_no' => $row['door_no'],
                    'town' => $row['town'],
                    'country' => $row['country'],
                    'contract_period' => $contract_period,
                    'product_id' => $product_id,
                    'cat_id' => $cat_id,
                    'quantity' => $row['quantity'],
                    'total_amount' => $total_amount,
                    'bill_no' => $row['bill_no'],
                    'mode_of_payment' => $mode_of_payment,
                    'check_no' => $check_no,
                    'check_date' => $check_date,
                    'transaction_id' => $this->input->post('transaction_id'),
                    'serial_no' => $this->input->post('serial_no'),
                    'model' => $this->input->post('model_no'),
                    'current_status' => 12
                );
                $result = $this->Punch_in->update_amc($data, $search_var);
                if ($result == 1) {
                    $tech_id     = $this->input->post('tech_id');
                    $company_id  = $this->Punch_in->get_company($tech_id);
                    $employee_id = $this->Punch_in->get_emp_id($tech_id);
                    $notify      = ' Contract updated  by ' . $employee_id;
                    $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                    $json = array(
                        "status" => 1,
                        "msg" => "AMC Successfully done!!!"
                    );
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Error!!!"
                    );
                }
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Invalid AMC id!!!"
            );
        }
        echo json_encode($json);
        
    }

   /*Renew existing AMC */
    public function renew_amc()
    {
        $this->load->helper('url');
        $this->load->database();
          $this->load->model('New_amc');
        $search_var      = $this->input->post('amc_id');
        $tech_id         = $this->input->post('tech_id');
        $contract_period = $this->input->post('contract_period');
        $type_cont       = $this->input->post('type_cont');
        $total_amount    = $this->input->post('total_amount');
        $mode_of_payment = $this->input->post('mode_of_payment');
        $check_no        = $this->input->post('check_no');
        $check_date      = $this->input->post('check_date');
        $search_var      = $this->input->post('amc_id');
        $this->load->model('Punch_in');
        $new_amc_id = $this->Punch_in->select_amc();
        $ids        = $this->Punch_in->get_ids($this->input->post('product'), $this->input->post('category'));
        $product_id = $ids[0]['product_id'];
        $cat_id     = $ids[0]['cat_id'];
        $ids        = $this->Punch_in->get_ids($this->input->post('product'), $this->input->post('category'));
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        $resu       = $this->Punch_in->search_amc($search_var,$company_id);
        
        if (!empty($resu)) {
            foreach ($resu as $row) {
                $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
              
                $ticket_id = $this->New_amc->tick_id($company_id);
                $data      = array(
                    'ticket_id' => $ticket_id,
                    'amc_id' => $search_var,
                    'tech_id' => $tech_id,
                    'cust_id' => $row['customer_id'],
                    'call_type' => $row['type_cont'],
                    'contact_no' => $row['contact_num'],
                    'address' => $row['street'],
                    'door_no' => $row['door_no'],
                    'town' => $row['town'],
                    'country' => $row['country'],
                    'contract_period' => $contract_period,
                    //'product_id' => $product_id,
                    //'cat_id' => $cat_id,
                    'product_id'=> $product_id,
                    'cat_id'=> $cat_id,
                    'quantity' => $row['quantity'],
                    'total_amount' => $total_amount,
                    'mode_of_payment' => $mode_of_payment,
                    'check_no' => $check_no,
                    'check_date' => $check_date,
                    'transaction_id' => $this->input->post('transaction_id'),
                    'serial_no' => $this->input->post('serial_no'),
                    'model' => $this->input->post('model_no'),
                    'previous_status' => 16,
                    'current_status' => 12,
                    'company_id' => $company_id
                );
                
                $result = $this->Punch_in->amc($data,$ticket_id);
                if ($result == 1) {
                    $tech_id     = $this->input->post('tech_id');
                    $company_id  = $this->Punch_in->get_company($tech_id);
                    $employee_id = $this->Punch_in->get_emp_id($tech_id);
                    $notify      = ' Contract renewed  by ' . $employee_id;
                    $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                    $json = array(
                        "status" => 1,
                        "msg" => "AMC Successfully done!!!"
                    );
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Error!!!"
                    );
                }
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Invalid AMC id!!!"
            );
        }
        echo json_encode($json);
        
    }
  
    /*Send the training assessment details */
    public function training()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->training($tech_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }

/*
Send the quiz and score details, 

If already technician attended the quiz, it will send score, or else it will send score =0
*/
    public function certificate()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->certificate($tech_id);
            

            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }

    /*Send the spare array to technicians */
    public function request_spare()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id = $this->input->post('ticket_id');
        $tech_id   = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        //$get_availability=$this->Punch_in->get_availability($data,$ticket_id);
        $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
        if($ticket_details['work_type']==3 || $ticket_details['work_type']==4)
        {
            $prev_wk_type=$ticket_details['work_type'];
            $wk_type=5;  ///// GCSN Call Changed
        }
        else
        {
            $prev_wk_type=0;
            $wk_type=$ticket_details['work_type'];
        }
        $data = array(
            'requested_spare' => $this->input->post('spare'),
            'spare_array' => $this->input->post('spare'),
            'avalability' => $this->input->post('avalability'),
            'lead_time' => $this->input->post('lead_time'),
            'schedule_next' => date("Y-m-d",strtotime($this->input->post('lead_time'))),
            'cust_preference_date' => date("Y-m-d",strtotime($this->input->post('lead_time'))),
            'spare_cost' => $this->input->post('price'),
            'cust_acceptance' => $this->input->post('cust_acceptance'),
            'is_it_chargeable' => $this->input->post('is_it_chargeable'),
            'prev_work_type' => $prev_wk_type,
            'work_type' => $wk_type,
            'spare_requested' => 1,
            'current_status' => 14
        );
        $this->load->model('Punch_in');
        if ($ticket_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please provide ticket id!!!"
            );
        } else {
            $result = $this->Punch_in->request_spare($data, $ticket_id);
            if ($result == 1) {
                $company_id = $this->Punch_in->get_company_tkt($ticket_id);
                
                $ticket_history = $this->Punch_in->ticket_history($ticket_id, $tech_id,$company_id,$ticket_details['cust_id'], $ticket_details['mode_of_payment'], $ticket_details['total_amount'],$ticket_details['current_status']);
               
                $notify     = 'New Spare Request raised for the ticket ' . $ticket_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);

                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "msg" => "Spare Requested!!!"
                );
                //$this->mail_pdf($ticket_id,$ticket_details['cust_id'],$company_id);
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        
            $res1=$this->Punch_in->update_tech_task($tech_id);  
            echo json_encode($json1);
    }

    /*Technician re request the spare rejected by manager*/
    public function rerequest_spare()
    {  
        $this->load->helper('url');
        $this->load->database();
        $ticket_id = $this->input->post('ticket_id');
        $tech_id   = $this->input->post('tech_id');
        $company_id = $this->input->post('company_id');
        $this->load->model('Punch_in'); 
        if ($ticket_id == '' || $tech_id == ''  || $company_id == '' ) {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please provide all details!"
            );  
        }
        else
        {
        $result = $this->Punch_in->rerequest_spare($ticket_id,$tech_id,$company_id);

        if ($result == 1) {
            $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "msg" => "Spare Re-Requested"
                ); 
        }
        else{
            $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "Error"
                );
        }

        }
        echo json_encode($json1);
        
    }

    /*Submit the quiz details from mobile */
    public function submit_quiz()
    { 
        $this->load->helper('url');
        $this->load->database();
        //$company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
       
        $data = array(
            'certificate_id' => $this->input->post('quiz_id'),
            'tech_id' => $this->input->post('tech_id'),
            'score' => $this->input->post('score'),
            'status' => $this->input->post('status'),
            'company_id'=> $this->input->post('company_id')
        );
        $this->load->model('Punch_in');
        if ($this->input->post('quiz_id') == '' || $this->input->post('tech_id') == '' || $this->input->post('score') == ''  || $this->input->post('company_id') == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all required details!!!"
            );
        } else {
            $result = $this->Punch_in->submit_quiz($data);
            if ($result == 1) {
                $json = '';
                $json = array(
                    "status" => 1,
                    "msg" => "Submitted!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        echo json_encode($json);
        
    }
    
    /*Send notification to mobile */
    public function get_notify()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->get_notify($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }

    /*
    Its request to manager for personal spares
    */
    public function personal_spare_request()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id     = $this->input->post('tech_id');
        $spare_array = $this->input->post('spare_array');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '' || empty($spare_array)) {
            $json = array(
                "status" => 0,
                "msg" => "Please provide all details"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->personal_spare_request($tech_id, $spare_array, $company_id);
            if ($result == 1) {
                $employee_id = $this->Punch_in->get_emp_id($tech_id);
                $notify      = 'New Imprest Spare Request raised by ' . $employee_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                $json = array(
                    "status" => 1,
                    "result" => "Request Done"
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Error"
                );
                echo json_encode($json);
            }
        }
    }

    /*Change the personal spare request status */
    public function personal_spare_update()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $id      = $this->input->post('id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '' || $id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide all details"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->personal_spare_update($tech_id, $id, $company_id);
            if ($result == 1) {
                $json = array(
                    "status" => 1,
                    "result" => "Updated "
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Error"
                );
                echo json_encode($json);
            }
        }
    }

    /*
    
    To get the imprest spare list of a specific technician*/
    public function onhand()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
       
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->onhand($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }
    
    /*To get the requested impresst spare details with status */
    public function requested_personal()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->requested_personal($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }

    /* To get all the spare list of each tickets */
    public function spare_array_tkt()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->spare_array_tkt($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }

    /* To get the report of reimbursement request raised by technicians */
    public function submitted_claims()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->submitted_claims($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }
    /*Used to raise spare transfer request to manager */
    public function personal_spare_transfer()
    {
        $this->load->helper('url');
        $this->load->database();
        $from_tech_id     = $this->input->post('from_tech_id');
        $to_tech_id = $this->input->post('to_tech_id');
        $spare_array = $this->input->post('spare_array');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('from_tech_id'));
        if ($from_tech_id == '' || $to_tech_id == '' || empty($spare_array)) {
            $json = array(
                "status" => 0,
                "msg" => "Please provide all details"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->personal_spare_transfer($from_tech_id,$to_tech_id, $spare_array, $company_id);
            if ($result == 1) {
                $employee_id = $this->Punch_in->get_emp_id($from_tech_id);
                $notify      = 'Imprest Spare Transfer raised by ' . $employee_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $from_tech_id);
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $from_tech_id);
                $json = array(
                    "status" => 1,
                    "result" => "Request Done"
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Error"
                );
                echo json_encode($json);
            }
        }
    }
    /* To get the technician's first name and employee_id*/
  public function tech_select()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id     = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
            $product=$this->Punch_in->get_tech_product($tech_id);
            $product_id=$product[0]['product_id'];
            $cat=$this->Punch_in->get_tech_cat($tech_id);
            $cat_id=$cat[0]['cat_id'];
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide tech_id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->tech_select($tech_id, $company_id, $product_id, $cat_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
            echo json_encode($json1);
        }
    }
    
  public function escalate_tech_select()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id     = $this->input->post('tech_id');
        $ticket_id     = $this->input->post('ticket_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
         $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
        $skill_level=$this->Punch_in->get_tech_skill_level($tech_id);
           // $product=$this->Punch_in->get_tech_product($tech_id);
            $product_id=$ticket_details['product_id'];
          //  $cat=$this->Punch_in->get_tech_cat($tech_id);
            $cat_id=$ticket_details['cat_id'];
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide tech_id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->escalate_tech_select($tech_id, $company_id, $product_id, $cat_id, $skill_level);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
            echo json_encode($json1);
        }
    }
        
    
    /*To validate the OTP entered by technician */
    public function verify_otp()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id      = $this->input->post('ticket_id');
        $tech_id        = $this->input->post('tech_id'); 
        $otp_mobile     = $this->input->post('otp');
        $service_id     = $this->input->post('service_id');
        $company_id     = $this->Punch_in->get_company($tech_id);
         $last_update = date("Y-m-d H:i:s");
         $data_otp = array(
                'last_update' =>$last_update,
                 'current_status' => 50
            );
   
        $res = $this->Punch_in->verify_otp($ticket_id,$tech_id,$otp_mobile,$data_otp);
      
        if ($res == 1)
         {
        
           $json = array(
                "status" => 1,
                "msg" => "OTP verified successfully"
            );
            
         echo json_encode($json);   
            
 $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
 $this->mail_pdf($ticket_id,$ticket_details['cust_id'],$company_id,$service_id);
        
        $keytext=uniqid();
        //$key=$ticket_id."".md5($keytext)."".time();
        $arr=explode("_", $ticket_id);
        $key=$otp_mobile.$arr[1];   
        $getdetails=$this->Punch_in->storeratingkey($ticket_id,$ticket_details['cust_id'],$ticket_details['serial_no'],$company_id,$key);
        $to=$getdetails['email_id'];
        $tonumber=$getdetails['contact_number'];
      
            /*$emailcontent = "Dear Customer, ";
        $emailcontent.='<br>';
        $emailcontent.="please take some time to rate our service in the following link ";
        $emailcontent.='<br>';
        $emailcontent.=base_url() . "index.php?/Controller_service/Feedback/?token=".$key;
        $emailcontent.='<br>Thanks & Regards';
        $emailcontent.='<br>Capricot Team';  
        */
            
             $url=base_url().'index.php?/fb/fb/?tk='.$key;
    $emailcontent=" <!doctype html>
                <html>
                <head>
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
               
                <body>
                <p>Dear Customer,</p>
                <p>Welcome to Capricot.</b></p>
                <p>Kindly rate our service.</p>
                <p></p>
                Click the below link.
                <p></p>
                $url
                <p></p>
                Thanks & regards,
                <p>Capricot Team.</p>

                </body>
                </html>";   

        $mobilemessage ="Dear Customer, ";
        $mobilemessage.="Kindly rate our service below link ";
        $mobilemessage.= base_url()."index.php?/fb/fb/?tk=".$key;
        //$mobilemessage.=' Thanks & Regards';
        //$mobilemessage.=' Capricot Team';     

        $config   = Array(
             'protocol' => 'smtp',
                  'smtp_host' => 'smtp.office365.com',
                  'smtp_port' => '587',
                  'smtp_timeout' => '30',
                  'smtp_user' => 'hwsupport@capricot.com',
                  'smtp_pass' => 'Support14',
                  'mailtype' => 'html',
                  'smtp_crypto' => 'tls',
                  'wordwrap' => TRUE,
                  'charset' => 'utf-8'
        );
         $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('hwsupport@capricot.com', 'Capricot');
        $this->email->to($to);
        $this->email->subject('Capricot - Customer Feedback');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
        $this->email->send();
        
        
        /* 
            $this->load->library('email');
              $config['protocol']='smtp';
              $config['smtp_host']='ssl://smtp.gmail.com';
              $config['smtp_port']='465';
              $config['smtp_timeout']='300';
              $config['smtp_user']='kaspondevelopers@gmail.com';
              $config['smtp_pass']='Kaspon@123';
              $config['charset']='utf-8';
              $config['newline']="\r\n";
              $config['wordwrap'] = TRUE;
              $config['mailtype'] = 'html';
              $this->email->initialize($config);
              $this->email->from('kaspondevelopers@gmail.com', 'Capricot');
              $this->email->to($tech_email);
              $this->email->subject('Capricot - Customer Feedback');
              $this->email->message($emailcontent);
              $this->email->send();
        
    
        */
      
      
            /*
        $apiKey = urlencode('2EodOA4pMpk-bmz7D8FaLwCWJ2n6XBK4fdXN2hU8hR');
        $sender = urlencode('TXTLCL');     
       // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $tonumber, "sender" => $sender, "message" => rawurlencode($mobilemessage));
      
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        */      
            
        
         
if ($tonumber != " " || $tonumber != NULL) {    
$this->load->model('Pushnotify');           
$number=$tonumber;
$message=$mobilemessage;
$smsvalue = array('message'=>$message,'number'=>$number);
$this->Pushnotify->sms_send_mobile($smsvalue);  
 }      
         }
         else
         {
            $json = array(
                "status" => 0,
                "msg" => "Error in OTP Verification"
            );  
        
         echo json_encode($json);

             
         }
        
   }

 public function submit_completetest()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id      = $this->input->post('ticket_id');
        $tech_id        = $this->input->post('tech_id');
        $service_charge = $this->input->post('service_charge');
        $spare_charge   = $this->input->post('spare_charge');
        $tax            = $this->input->post('tax');
        $total_charge   = $this->input->post('total_charge');
        $signature      = '';
        if (!empty($_FILES["myFile"])) {
            $file_name = $_FILES["myFile"]["name"];
            $temp_name = $_FILES['myFile']['tmp_name'];
            $filename  = end((explode(".", $file_name)));
            $img_dir   = 'assets/images/' . 'completed_' . $ticket_id . '.'. $filename;
            $signature = stripslashes($img_dir);
            move_uploaded_file($temp_name, $signature);
        }
        $frm              = $this->input->post('frm');
        $drop_of_date     = $this->input->post('drop_of_date');
        $drop_of_date     = date("Y-m-d h:i:s", strtotime($drop_of_date));
        $drop_of_location = $this->input->post('drop_of_location');
        $ticket_end_time  = date("Y-m-d H:i:s");
        $company_id       = $this->Punch_in->get_company($tech_id);
        if ($signature != '') {
            $data = array(
                'company_id' => $company_id,
                'ticket_id' => $ticket_id,
                'tech_id' => $tech_id,
                'service_charge' => $service_charge,
                'spare_amount' => $spare_charge,
                'tax' => $tax,
                'total' => $total_charge,
                'signature' => $signature,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $this->input->post('check_date'),
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time
            );
        } else {
            $data = array(
                'company_id' => $company_id,
                'ticket_id' => $ticket_id,
                'tech_id' => $tech_id,
                'service_charge' => $service_charge,
                'spare_amount' => $spare_charge,
                'tax' => $tax,
                'total' => $total_charge,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $this->input->post('check_date'),
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time
            );
        }
        $result = $this->Punch_in->submit_complete($ticket_id, $tech_id, $data);
        if ($result == 1) {
            $product     = $this->Punch_in->get_tech_product($tech_id);
            $company_id  = $product[0]['company_id'];
            $employee_id = $this->Punch_in->get_emp_id($tech_id);
            $notify      = $ticket_id . ' is completed by ' . $employee_id;
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
            $json = '';
            $code = mt_rand(1001,9999);
            $data = array(
                'service_charge' => $service_charge,
                'spare_cost' => $spare_charge,
                'tax' => $tax,
                'total_amount' => $total_charge,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $this->input->post('check_date'),
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time,
                //'current_status' => 8,
                'otp_code'=> $code
            );
            $cus_mo  = $this->Punch_in->get_det_cust($ticket_id);
            $cust_mobile = $cus_mo['contact_number'];
            $res  = $this->Punch_in->submit_complete1($ticket_id, $tech_id, $data,$cust_mobile,$code);
            if ($res == 1) {
                $json = array(
                    "status" => 1,
                    "OTP" => $code,
                    "msg" => "Your ticket has been completed successfully!!!"
                );
                $cst  = $this->Punch_in->get_det_cust($ticket_id);
                $cust_id=$cst['customer_id'];
                 $to=$cst['email_id'];
//$to="srihari.s@kaspontech.com";

/* $emailcontent = "Dear Customer,";
        $emailcontent.='<br>';
        $emailcontent.="If you are satisfy with our service please share this OTP with technician ";
        $emailcontent.='<br>';
        $emailcontent.= $code;
        $emailcontent.='<br>Thanks & Regards';
        $emailcontent.='<br>Capricot Team';  
        */

 $emailcontent=" <!doctype html>
                <html>
                <head>
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
               
                <body>
                <p>Dear Customer,</p>
                <p>Welcome to Capricot.</b></p>
                <p>If you are satisfied with our service.</p>
                <p>Please share below OTP with Technician.</p>
                <p>$code</p> 
                <p>Thanks & regards,</p> <p>Capricot Team.</p>

                </body>
                </html>";


      //  $emailcontent = "Dear Customer, <br> If you satisfy with our service please share this OTP with technician - ".$code;
         $config   = Array(
             'protocol' => 'smtp',
                  'smtp_host' => 'smtp.office365.com',
                  'smtp_port' => '587',
                  'smtp_timeout' => '30',
                  'smtp_user' => 'hwsupport@capricot.com',
                  'smtp_pass' => 'Support14',
                  'mailtype' => 'html',
                  'smtp_crypto' => 'tls',
                  'wordwrap' => TRUE,
                  'charset' => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('hwsupport@capricot.com', 'Capricot');
        $this->email->to($to);
        $this->email->subject('Capricot - Customer OTP Verfication');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
        echo "to ".$to."  ".$this->email->send();
        
            }
          
        } 
        
      //  echo json_encode($json);
    }
public function testencryption()
{
     $ticket_id="Tk_0828";
    $company_id="company1";
    $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        //  $this->load->library('email');
   
$keytext=uniqid();
$key = $ticket_id."".md5($keytext)."".time();
$getdetails=$this->Punch_in->storeratingkey($ticket_id,$company_id,$key);
         $to=$getdetails['email_id'];
      //   $tonumber=$getdetails['contact_number'];
        // $to="srihari.s@kaspontech.com";
        // echo $to;
         $tonumber=$getdetails['contact_number'];
        $emailcontent = "Dear Customer,";
        $emailcontent=$emailcontent.'<br>';
        $emailcontent=$emailcontent."please take some time to rate our service in the following link";
        $emailcontent=$emailcontent.'<br>';
        $emailcontent=$emailcontent."".base_url() . "index.php?/Controller_service/Feedback/?token=".$key;
        $config   = Array(
            'protocol' => 'smtp',
                  'smtp_host' => 'smtp.office365.com',
                  'smtp_port' => '587',
                  'smtp_timeout' => '30',
                  'smtp_user' => 'hwsupport@capricot.com',
                  'smtp_pass' => 'Support14',
                  'mailtype' => 'html',
                  'smtp_crypto' => 'tls',
                  'wordwrap' => TRUE,
                  'charset' => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('hwsupport@capricot.com', 'Capricot');
        $this->email->to($to);
        $this->email->subject('Capricot Ticket Feedback');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
      //  $this->email->send();
        $this->email->send();
$apiKey = urlencode('2EodOA4pMpk-bmz7D8FaLwCWJ2n6XBK4fdXN2hU8hR');
       // $numbers = array($cust_mobile);
        $sender = urlencode('TXTLCL');
        //$message = rawurlencode($array['string']);
       // $message = rawurlencode("Dear Customer if you satisfy with our service please share this OTP with technician - ".$code);
     //   $tonumber = implode(',', $tonumber);
       
        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $tonumber, "sender" => $sender, "message" => $emailcontent);
      
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
// Will only print the email headers, excluding the message subject and body
       echo  $this->email->print_debugger(array('headers'));
        echo "  sent to  ".$to;
}
public function remainder_mail(){
            $today_date = date("Y-m-d");
            $this->db->select('*');
            $this->db->from('admin_user');
            $this->db->group_by('admin_user.company_id');
            $query = $this->db->get();
            $answer = $query->result_array();
            //print_r($answer);
            foreach($answer as $row){
                $date1_ts = strtotime($today_date);
                $date2_ts = strtotime($row['renewal_date']);
                $diff = $date2_ts - $date1_ts;
                $result = round($diff / 86400);
                //echo "for";
                  if($result>="0" && $result<="5"){
                                    //echo "if";    
                                    // echo $row['ad_mailid'];
                        //echo $row['company_id'];
                      $mail_id = $row['ad_mailid'];
                            $company_name = $row['company_name'];
                      $renewal_date = $row['renewal_date'];
                      
                        $this->load->helper('url');
                        $this->load->database();
                        $username = "
                            <!doctype html>
                            <html>
                            <head>
                            <meta name='viewport' content='width=device-width' />
                            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                            <style>
                            p,
                            ul,
                            ol {
                            font-family: sans-serif;
                            font-size: 14px;
                            font-weight: normal;
                            margin: 0;
                            Margin-bottom: 15px; }
                            p li,
                            ul li,
                            ol li {
                            list-style-position: inside;
                            margin-left: 5px; }
                            a {
                            color: #3498db;
                            text-decoration: underline; }
                            </style>
                            <body>
                            <p>Dear <b>".$company_name."</b> & Team,</p>
                            <p>Warning mail!!!</b></p>
                            <p>Your Subscription expires soon!! </p> 
                            <p>Your Company's subscription expires on <strong>".$renewal_date."</strong></p>
                            <p>Only ".$result ." Days left for expiry</p>
                            <p>Best regards,</p> <p>Capricot Team.</p>.
                            
                            </body>
                            </html>";
                    $config   = Array(
            'protocol' => 'smtp',
                  'smtp_host' => 'smtp.office365.com',
                  'smtp_port' => '587',
                  'smtp_timeout' => '30',
                  'smtp_user' => 'hwsupport@capricot.com',
                  'smtp_pass' => 'Support14',
                  'mailtype' => 'html',
                  'smtp_crypto' => 'tls',
                  'wordwrap' => TRUE,
                  'charset' => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('hwsupport@capricot.com', 'Capricot');
                        $this->email->to("ramyariotz22@gmail.com");
                        $this->email->subject('Expiry of Subscription regd');
                        $this->email->message($username);
                        $this->email->set_newline("\r\n");
                        //$this->email->send();
                      $this->email->send();
//echo "mail sent";
// Will only print the email headers, excluding the message subject and body
       //echo  $this->email->print_debugger(array('headers'));
                      
                  }                     
            }
            return true;
            //return $result.' Days';
        }

        /* Function to mail new auto generated password to user's email*/
public function ForgotPassword_Customer()
        {   $this->load->helper('url');
            $this->load->database(); 
            $this->load->model('Punch_in');
            //$json1 = array();
              $email = $this->input->post('email');  
            $findemail = $this->Punch_in->ForgotPassword_cus($email);  
         
              if($findemail){
                
               $this->Punch_in->sendpassword($findemail);        
                }else{
                    $json1 = array(
                        "status" => 0,
                        "msg" => "No Email Found!!!"
                    );

                    echo json_encode($json1);
           }
          
        }

        /* Function to validate old password and then change new password */
        public function change_password_cus()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $json1 = array();
        $old_pass = $this->input->post('oldpass');
        $new_pass = $this->input->post('newpass');
        $cus_email = $this->input->post('email');
        $check_old_email = $this->Punch_in->checkOldPass($cus_email,$old_pass);

        if($check_old_email == 1)
        {
         $save_new_pass = $this->Punch_in->save_new_pass($cus_email,$new_pass);  
         if($save_new_pass == 1) {
            $json1 = array(
                "status" => 1,
                "msg" => "Password changed successfully"
            ); 
         }
         else{
            $json1 = array(
                "status" => 0,
                "msg" => "Error in update password"
            ); 
         }
         
        }
        
        else{
            $json1 = array(
                "status" => 0,
                "msg" => "No Email Found"
            );  
        }
        echo json_encode($json1);
    
    }

    
public function testunion()
{
     $this->load->helper('url');
   $this->load->library('email');
    $data['ticket_data']=array(
        "ticket_id"=>"Ticket",
        "AMCID"=>"AMC",
        "Contracttype"=>"Labour"
    );

    $baseurl = base_url() .'assets/layouts/layout/img/logos.png';
   $emailTemplate='<html>
<head>
    <style>
    table {
    border-collapse: collapse;
    width: 70%;
}
th{
   
    text-align: center;
    border-bottom: 0px solid #ddd;
    border-top: 0px solid #ddd;
    border-left: 0px solid #ddd;
    border-right: 0px solid #ddd;
}
 td {
    padding: 8px;
    text-align: left;
    border-bottom: 0px solid #ddd;
    border-top: 0px solid #ddd;
    border-left: 0px solid #ddd;
    border-right: 0px solid #ddd;
}
    </style>
    
</head>
<body>
    <table  border="0" width="70%" >
 <tr>
<td colspan="2" width="100%" style="background-color: #d8fdee;text-align: center;"><img src="'.$baseurl.'"></td>
</tr>

<tr>
<td colspan="2">Hi Hari</td>
</tr>
<tr>
<td colspan="2">Your request for renewal of the contract is under process.
  Kindly find the details below. </td>
</tr>
 
                  <tr>
                    <td width="50%" align="left" >Ticket Id:</td>
                    <td width="50%" align="left" >TK_0900</td>
                  </tr>
                  <tr>
                    <td width="50%" align="left" >AMC contract Id</td>
                    <td width="50%" align="left" >TK_0900</td>
                    
                  </tr>
                  <tr>
                    <td width="50%" align="left" >Contract Type:</td>
                    <td width="50%" align="left" >Labour</td>
                  </tr>

<tr>
<td colspan="2">Thanks and Regards,</td>
</tr>

<tr>
<td colspan="2">Capricot Team</td>
</tr>
</table>
</body>
</html>';
  
$config['protocol']='smtp';
$config['smtp_host']='ssl://smtp.gmail.com';
$config['smtp_port']='465';
$config['smtp_timeout']='300';
$config['smtp_user']='kaspondevelopers@gmail.com';
$config['smtp_pass']='Kaspon@123';
$config['charset']='utf-8';
$config['newline']="\r\n";
$config['wordwrap'] = TRUE;
$config['mailtype'] = 'html';
$this->email->clear(TRUE);
$this->email->initialize($config);
$this->email->from('kaspondevelopers@gmail.com', 'Capricot');
$this->email->to("srihariram777@hotmail.com");
$this->email->subject('Notification Mail');
$this->email->set_mailtype("html");
$this->email->message($emailTemplate);

$this->email->send();
}

/*To cross check distance between the customer and the technician address are  within 100 meters */
public function getDistance_26_july_2019()
{  
    $latitude1 = $this->input->post('latitude1');
    $longitude1 = $this->input->post('longitude1');
    $latitude2 = $this->input->post('latitude2');
    $longitude2 = $this->input->post('longitude2');
   
    $earth_radius = 6371;

    $dLat = deg2rad( $latitude2 - $latitude1 );  
    $dLon = deg2rad( $longitude2 - $longitude1 );  

    $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);  
    $c = 2 * asin(sqrt($a));  
    $d = $earth_radius * $c;  
    $meters = $d * 1000;
    $km  = floor($meters / 1000);
    if( $meters < 100 ) {
        
        $json2 = '';
        $json2 = array(
            "status" => 1,
            "meter" =>$meters,
            //"km" =>$km,
            "msg" => "Within $meters meters radius"
        );
        echo json_encode($json2); 
        
    } else {
      
        $json2 = '';
        $json2 = array(
            "status" => 0,
            "meter" =>$meters,
            //"km" =>$km,
            "msg" => "You are $meters meter away from the destination place"
        );
        echo json_encode($json2);
    }  
}

public function getDistance() {  
    $latitude1 = $this->input->post('latitude1');
    $longitude1 = $this->input->post('longitude1');
    $latitude2 = $this->input->post('latitude2');
    $longitude2 = $this->input->post('longitude2');
    $url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$latitude1.",".$longitude1."&destination=".$latitude2.",".$longitude2."&key=AIzaSyCZ3g8ja1ueR60JK8TJSkJ8fM6xcldmYfA";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['routes'][0]['legs'][0]['distance']['text'];
    $value = $response_a['routes'][0]['legs'][0]['distance']['value'];  
    $time = $response_a['routes'][0]['legs'][0]['duration']['text'];    
        
    $kilom=explode(" ",$dist);
    //$km=trim($kilom[0]);  
    $km=$value/1000;
    $meters = $value;
    if( $meters <= 100 ) {
        
        $json2 = '';
        $json2 = array(
            "status" => 1,
            "meter" =>$meters,
            "km" =>round($km),
            "msg" => "Within $meters meters radius"
        );
        echo json_encode($json2); 
        
    } else {
      
        $json2 = '';
        $json2 = array(
            "status" => 0,
            "meter" =>$meters,
            "km" =>round($km),
            "msg" => "You are $meters meters away from the destination place"
        );
        echo json_encode($json2);
    }  
}


/*To manage the list of consumed imprest spare details for a specific ticket */
public function imprest_spare_track()
{
    $this->load->helper('url');
    $this->load->database();
    $tech_id     = $this->input->post('tech_id');
    $ticket_id   = $this->input->post('ticket_id');
    $spare_array = $this->input->post('spare_array');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    if ($tech_id == '' || empty($spare_array)) {
        $json = array(
            "status" => 0,
            "msg" => "Please provide all details"
        );
        echo json_encode($json);
    } else {
        $spare_cost='';
        $result = $this->Punch_in->imprest_spare_track($tech_id,$ticket_id, $spare_array, $company_id,$spare_cost);
        if ($result == 1) {
            $employee_id = $this->Punch_in->get_emp_id($tech_id);
            $notify      = 'Imprest Spare consumed by ' . $employee_id;
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
            $json = array(
                "status" => 1,
                "result" => "Success"
            );
            echo json_encode($json);
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Error"
            );
            echo json_encode($json);
        }
    }
}

/*To get the list of spare transfered by the technician */
public function spare_transfer_list()
{
    $this->load->helper('url');
    $this->load->database();
    $tech_id = $this->input->post('tech_id');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    if ($tech_id == '') {
        $json = array(
            "status" => 0,
            "msg" => "Please provide technician id"
        );
        echo json_encode($json);
    }
    else {
        $result = $this->Punch_in->spare_transfer_list($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No data found"
                );
            }
            echo json_encode($json1);
    }
}

/*To get the list of spare received by the technician */ 
public function spare_received_list()
{
    $this->load->helper('url');
    $this->load->database();
    $tech_id     = $this->input->post('tech_id');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    if ($tech_id == '') {
        $json = array(
            "status" => 0,
            "msg" => "Please provide technician id"
        );
        echo json_encode($json);
    }
    else {
        $result = $this->Punch_in->spare_received_list($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No data found"
                );
            }
            echo json_encode($json1);
    }
}

/*To update the transfered status of the manager approved spare transfer request */
public function accept_transfered_spare(){

    $this->load->helper('url');
    $this->load->database();
    $spare_id   =$this->input->post('spare_id');
    $tech_id     = $this->input->post('tech_id');
    $to_tech_id = $this->input->post('to_tech_id');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    
 
    $result = $this->Punch_in->accept_transfered_spare($spare_id,$tech_id, $to_tech_id,$company_id);
    if ($result == 1) {
        $tech_id     = $this->input->post('tech_id');
        $employee_id = $this->Punch_in->get_emp_id($tech_id);
        $notify      = 'Spare accepted  by ' . $employee_id;
        $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
        $this->Punch_in->update_notify($notify, $company_id, 'Technician', $tech_id);
        $json = array(
            "status" => 1,
            "msg" => "Spare accepted"
        );
    } else {
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Error"
        );
    }
    echo json_encode($json);

}

/*To update the received status of the from technician transferred spares*/
public function accept_received_spare(){

    $this->load->helper('url');
    $this->load->database();
    $spare_id   =$this->input->post('spare_id');
    $tech_id     = $this->input->post('tech_id');
    $from_tech_id = $this->input->post('from_tech_id');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    
 
    $result = $this->Punch_in->accept_received_spare($spare_id,$tech_id, $from_tech_id,$company_id);
    if ($result == 1) {
        $tech_id     = $this->input->post('tech_id');
        $employee_id = $this->Punch_in->get_emp_id($tech_id);
        $notify      = 'Spare received  by ' . $employee_id;
        $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
        $this->Punch_in->update_notify($notify, $company_id, 'Technician', $tech_id);
        $json = array(
            "status" => 1,
            "msg" => "Spare received"
        );
    } else {
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Error"
        );
    }
    echo json_encode($json);

}

/*To update the profile image of the technician */

public function profile_image_update()
{  
   
    $this->load->helper('url');
    $this->load->library('upload');
    $this->load->database();
    $this->load->model('Punch_in');
   
    $company_id = $this->input->post('company_id');
    $tech_id   = $this->input->post('tech_id');
   
    if (!empty($_FILES["myimage"])) {
        $file_name = $_FILES["myimage"]["name"];
        $temp_name = $_FILES['myimage']['tmp_name'];
        //$filename  = end((explode(".", $file_name)));
        $img_dir   = 'technician_img/' . $tech_id .'_'. $file_name;
        $img_dir1  = stripslashes($img_dir);
        if(move_uploaded_file($temp_name, $img_dir1)){
            
         $result = $this->Punch_in->profile_image_upload($company_id,$tech_id,$img_dir1);
         if ($result == 1) {
           
           $image_url = $this->Punch_in->profile_image_link($company_id, $tech_id); 

            $json = '';
            $json = array(
                "status" => 1,
                "msg" => "Profile image updated successfully",
                "updated_image" => base_url().$image_url
            );
         }else{
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error in insert image data"
            );
         }

          
        }
        else{
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Something error in image upload, try again"
            );
        }
    }
    else{
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Image may empty"
        );
    }

    echo json_encode($json);
    
}

/*To get the list of pending frm details */
public function pending_frm()
{
    $this->load->helper('url');
    $this->load->library('upload');
    $this->load->database();
    $this->load->model('Punch_in');  
    $json   = array();      
    $tech_id   = $this->input->post('tech_id');

    if ($tech_id != "") {
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        $result = $this->Punch_in->pending_frm($tech_id,$company_id);
        if (!empty($result)) {
            $json = array(
                "status" => 1,
                "result" => $result
            );
        }
        
    } else {
        $json = array(
            "status" => 0,
            "result" => "Please provide technician ID"
        );
    }
    echo json_encode($json);
    
    
}

/*To get the locations of all ticekts */
public function load_location()
{
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Punch_in');  
    $json   = array();    
    $tech_id =  $this->input->post('tech_id');
    if ($tech_id != "") {
    $company_id = $this->Punch_in->get_company($tech_id);
    $tech_data=$this->Punch_in->gettechdetails($tech_id);
    $region=$tech_data['region'];
    $area=$tech_data['area'];
    
    $result = $this->Punch_in->load_location($company_id,$region,$area);
    if (!empty($result)) {
        $json = array(
            "status" => 1,
            "result" => $result
        );
    }
} else {
    $json = array(
        "status" => 0,
        "result" => "Please provide technician ID"
    );
}

    echo json_encode($json);
}

/*To display travel charge details of each and every tickets of the technician */
public function travel_update_list()
{
  
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Punch_in');  
    $json   = array(); 
    $tech_id =  $this->input->post('tech_id');
    if ($tech_id != "") {
    $company_id = $this->Punch_in->get_company($tech_id);    
    $result = $this->Punch_in->travel_update_list($company_id,$tech_id);
    if (!empty($result)) {
        $json = array(
            "status" => 1,
            "result" => $result
        );
    }
} else {
    $json = array(
        "status" => 0,
        "result" => "Please provide technician ID"
    );
}

    echo json_encode($json);
}

public function submit_resolution_summary(){

    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Punch_in');
    $ticket_id      = $this->input->post('ticket_id');
    $tech_id        = $this->input->post('tech_id');      
    $summary     = $this->input->post('resolution_summary'); 
    
    $company_id = $this->Punch_in->get_company($tech_id); 

    if ($ticket_id == '' ) {
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Please provide all details!!!"
        );
    } 
    else{
     $result = $this->Punch_in->resolution_summary($ticket_id, $summary, $company_id);
    

     if($result == 1){
        $json = '';
        $json = array(
            "status" => 1,
            "msg" => "Success"
        );
        $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
        //$this->mail_pdf($ticket_id,$ticket_details['cust_id'],$company_id);
    }
    else{
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Failure"
        );
    }
 

    }
    echo json_encode($json);

}



//////// raju
/*create the services activities*/    
    public function create_service_activities()
    {
        $this->load->helper('url');
        $this->load->database();
          $this->load->model('Punch_in');
        $ticket_id = $this->input->post('ticket_id');
        $tech_id   = $this->input->post('tech_id');
        $service_id   = $this->input->post('service_id');
        $product_name = $this->input->post('product_name');
    $service_activities = json_encode($this->input->post('service_activities'),TRUE);   
$service_activities = json_decode($this->input->post('service_activities'),TRUE);
        
    $summary     = $this->input->post('resolution_summary');    
       
      
        if ($this->input->post('tech_id') == '' || $this->input->post('ticket_id') == '' || $this->input->post('service_id') == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all fields!!!"
            );
            echo json_encode($json);
        } else {
          
  if ($service_activities!='') 
    {
             //    $employee_id = $this->Punch_in->get_emp_id($tech_id);
             //   $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
    foreach($service_activities as $key=>$val)
    {
     
        $activities = $this->Punch_in->create_service_activities($ticket_id,$tech_id,$service_id,$val['activite_id'],$val['product_id'], $val['ap_value'], $val['remarks']);
        
     }
             $company_id = $this->Punch_in->get_company($tech_id);   
             $result = $this->Punch_in->resolution_summary($ticket_id, $summary, $company_id);   
               if($result == 1){
                   //  $ticket_details = $this->Punch_in->get_ticket_details($ticket_id);
                //   $this->mail_pdf($ticket_id,$ticket_details['cust_id'],$company_id,$service_id);
               }
          
                $json = '';
                $json = array(
                    "status" => "1",
                    "msg" => "Your request has been updated successfully!!!"
                );
                
               
            } else {
                $json = '';
                $json = array(
                    "status" => "0",
                    "msg" => "Error!!!"
                );
            }
            
           // $res1=$this->Punch_in->update_tech_task($tech_id);  
            echo json_encode($json);
        }
    }
    
public function mail_pdf($ticket_id,$cust_id,$company_id,$service_id)
    {
        //Load the library $ticket_id,$cust_id,$company_id

      
        $this->load->library('html2pdf');
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
    
        
        $this->html2pdf->folder('./assets/pdfs/');
        $this->html2pdf->filename('Capricot_summary_report.pdf');
        $this->html2pdf->paper('a4', 'portrait');

/*$company_id='company51';  
$ticket_id='Tk_0125';
$service_id='2';
    $cust_id='Cust_0014';
    */
        $ticket_details = $this->Punch_in->get_ticket_info($ticket_id);
       // $cust_details   = $this->Punch_in->get_info_customer($cust_id);
        $cust_details   = $this->Punch_in->get_info_customer($cust_id,$ticket_details['serial_no']);
        $company_details = $this->Punch_in->get_det_company($company_id);
        $activite_details = $this->Punch_in->get_ticket_activities($ticket_id);
    //print_r($activite_details);
    //foreach($activite_details as $row)
    //{
//      echo $row['activite_id'];
//  }
//  die;

        //to print the call status in pdf
        if($ticket_details['status_name'] =="completed")
        {
            $status = "Completed";

        }elseif($ticket_details['status_name'] =="escalated")
        {
            $status="Escalated";
        }
        elseif($ticket_details['status_name'] =="work_in_progress")
        {
            $status="Work in progress";
        }
        elseif($ticket_details['status_name'] =="spare_request")
        {
            $status="Spare Requested";
        }
        elseif($ticket_details['status_name'] =="escalate")
        {
            $status="Escalate";
        }
        elseif($ticket_details['status_name'] =="otp_verified")
        {
            $status="Completed";
        }
        else{
            $status="";
        }
      
       
        $data = array(
            'ticket_id' => $ticket_id,
            'rsummary' => $ticket_details['resolution_summary'],
            'prob_descip' => $ticket_details['prob_desc'],
            'case_id' => $ticket_details['landmark'],
            'ticket_date'=>date('d-m-Y',strtotime($ticket_details['last_update'])),
            'product_id'=>$ticket_details['product_name'],
            'model'=>$ticket_details['model'],
            'serial_no'=>$ticket_details['serial_no'],
            'service_group_name'=>$ticket_details['service_group'],
            'status_name'=>$status,
            'technician_name'=>$this->Punch_in->get_emp_name($ticket_details['tech_id']),
            'signature'=>$ticket_details['signature'],
                    
            'customer_name' =>$cust_details['customer_name'],
            'customer_door_no'=> $cust_details['door_num'],
            'cust_address'=> $cust_details['address'],
            'cust_town'=> $cust_details['cust_town'],
            'cust_city'=> $cust_details['city'],
            'cus_pincode'=>$cust_details['pincode'],
            'cust_email'=> $cust_details['email_id'],
            'cust_mobile' => $cust_details['contact_number'],
            'company_name'=> $company_details['company_name'],
            'company_logo'=> $company_details['company_logo'],
            'company_dno'=> $company_details['building_no'],
            'company_address'=>$company_details['base_address'],
            'company_town'=>$company_details['town'],
            'company_city'=>$company_details['city'],
            'company_state'=>$company_details['state'],
            'company_contact'=>$company_details['company_contact'],
            'company_pincode'=>$company_details['pincode'],
            'service_id'=>$service_id,
            'activities'=>$activite_details
           
        );
        //Load html view
        $pdf=$this->html2pdf->html($this->load->view('pdf_mail', $data, true));
        
        //Check that the PDF was created before we send it
        if($path = $this->html2pdf->create('save')) {
            $config   = Array(
                 'protocol' => 'smtp',
                  'smtp_host' => 'smtp.office365.com',
                  'smtp_port' => '587',
                  'smtp_timeout' => '30',
                  'smtp_user' => 'hwsupport@capricot.com',
                  'smtp_pass' => 'Support14',
                  'mailtype' => 'html',
                  'smtp_crypto' => 'tls',
                  'wordwrap' => TRUE,
                  'charset' => 'utf-8'
            );

          /*  $emailcontent = "Dear Customer,";
            $emailcontent=$emailcontent.'<br>';
            $emailcontent=$emailcontent."Please find your Capricot ticket summary for <b>$ticket_id</b> ";
            $emailcontent=$emailcontent.'<br>';
            $emailcontent=$emailcontent."Contact support@kaspontechworks.com if you have any queries.";
            $emailcontent=$emailcontent.'<br>';
            $emailcontent=$emailcontent.'<br>';
            $emailcontent=$emailcontent.'<br>';
            $emailcontent=$emailcontent.'Thank you,';
            $emailcontent=$emailcontent.'<br>';
            $emailcontent=$emailcontent.'Capricot Team.';
            */
            
            $emailcontent=" <!doctype html>
                <html>
                <head>
                 <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
               
                <body>
                <p>Dear Customer,</p>
                <p>Welcome to Capricot.</p>
               Please find your Capricot ticket summary for <b>$ticket_id</b>
               <p> </p>
               If any queries please mail to : hwsupport@capricot.com
               <p> </p>
               <p>Thanks & regards,</p> <p>Capricot Team.</p>

                </body>
                </html>"; 
            
            
            $this->load->library('email',$config);
            $this->email->from('hwsupport@capricot.com', 'Capricot');
            $this->email->to($cust_details['email_id']);            
            $this->email->subject($company_details['company_name'].'-'.'Service call report'.'-'.$ticket_id);
            $this->email->message($emailcontent);
            $this->email->set_newline("\r\n");  
            $this->email->attach($path);
            $this->email->send();
            
           // echo $this->email->print_debugger();
            
                        
        }
        
    } 
    

public function web_mail_pdf($ticket_id)
    {
       //Load the library $ticket_id,$cust_id,$company_id

      
        $this->load->library('pdf');
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in_v2');
       
   
$tkt_det = $this->Punch_in_v2->get_ticket_details($ticket_id);
$company_id=$tkt_det['company_id']; 
$ticket_id=$tkt_det['ticket_id'];
$service_id=$tkt_det['work_type'];
$cust_id=$tkt_det['cust_id'];
    
        $ticket_details = $this->Punch_in_v2->get_ticket_info($ticket_id);
        $cust_details   = $this->Punch_in_v2->get_info_customer($cust_id,$ticket_details['serial_no']);
        $company_details = $this->Punch_in_v2->get_det_company($company_id);
        $activite_details = $this->Punch_in_v2->get_ticket_activities($ticket_id);
	//print_r($activite_details);
	//print_r($ticket_details);
		foreach($activite_details as $row)
        {
			$service_id=$row['service_id'];
        }
        
    //     //to print the call status in pdf
    //     if($ticket_details['current_status'] ==12)
    //     {
    //         $status = "Closed";

    //     }elseif($ticket_details['current_status'] ==15)
    //     {
    //         $status="Escalated";
    //     }
    //     elseif($ticket_details['current_status'] ==10)
    //     {
    //         $status="Work in progress";
    //     }
    //     elseif($ticket_details['current_status'] ==14)
    //     {
    //         $status="Spare Requested";
    //     }
    //    elseif($ticket_details['current_status'] ==50)
    //     {
    //         $status="OTP Verify";
    //     }
    //     else{
    //         $status="";
    //     }

        if($ticket_details['current_status'] ==10)
        {
            $data = array(
                'ticket_id' => $ticket_id,
                'rsummary' => $ticket_details['resolution_summary'],
                'prob_descip' => $ticket_details['prob_desc'],
                'case_id' => $ticket_details['landmark'],
                'ticket_date'=>date('d-m-Y',strtotime($ticket_details['last_update'])),
                'product_id'=>$ticket_details['product_name'],
                'model'=>$ticket_details['model'],
                'serial_no'=>$ticket_details['serial_no'],
                'service_group_name'=>$ticket_details['service_group'],
                'status_name'=> "Work in Progress",
                'technician_name'=>$this->Punch_in_v2->get_emp_name($ticket_details['tech_id']),
                'signature'=>$ticket_details['signature'],
                'service_image' => $ticket_details['service_attach_image'],     
                'customer_name' =>$cust_details['customer_name'],
                'customer_door_no'=> $cust_details['door_num'],
                'cust_address'=> $cust_details['address'],
                'cust_town'=> $cust_details['cust_town'],
                'cust_city'=> $cust_details['city'],
                'cus_pincode'=>$cust_details['pincode'],
                'cust_email'=> $cust_details['email_id'],
                'cust_mobile' => $cust_details['contact_number'],
                'company_name'=> $company_details['company_name'],
                'company_logo'=> $company_details['company_logo'],
                'company_dno'=> $company_details['building_no'],
                'company_address'=>trim($company_details['base_address']),
                'company_town'=>$company_details['town'],
                'company_city'=>$company_details['city'],
                'company_state'=>$company_details['state'],
                'company_contact'=>$company_details['company_contact'],
                'company_pincode'=>$company_details['pincode'],
                'service_id'=>$service_id,
                'activities'=>$activite_details
               
            );

        }
        else if($ticket_details['current_status'] == 15)
        {
            $data = array(
                'ticket_id' => $ticket_id,
                'rsummary' => $ticket_details['resolution_summary'],
                'prob_descip' => $ticket_details['prob_desc'],
                'case_id' => $ticket_details['landmark'],
                'ticket_date'=>date('d-m-Y',strtotime($ticket_details['last_update'])),
                'product_id'=>$ticket_details['product_name'],
                'model'=>$ticket_details['model'],
                'serial_no'=>$ticket_details['serial_no'],
                'service_group_name'=>$ticket_details['service_group'],
                'status_name'=>"Escalated",
                'technician_name'=>$this->Punch_in_v2->get_emp_name($ticket_details['tech_id']),
                'signature'=>$ticket_details['signature'],
                'service_image' => $ticket_details['escalate_image'],     
                'customer_name' =>$cust_details['customer_name'],
                'customer_door_no'=> $cust_details['door_num'],
                'cust_address'=> $cust_details['address'],
                'cust_town'=> $cust_details['cust_town'],
                'cust_city'=> $cust_details['city'],
                'cus_pincode'=>$cust_details['pincode'],
                'cust_email'=> $cust_details['email_id'],
                'cust_mobile' => $cust_details['contact_number'],
                'company_name'=> $company_details['company_name'],
                'company_logo'=> $company_details['company_logo'],
                'company_dno'=> $company_details['building_no'],
                'company_address'=>$company_details['base_address'],
                'company_town'=>$company_details['town'],
                'company_city'=>$company_details['city'],
                'company_state'=>$company_details['state'],
                'company_contact'=>$company_details['company_contact'],
                'company_pincode'=>$company_details['pincode'],
                'service_id'=>$service_id,
                'activities'=>$activite_details
               
            );

        }

        else if($ticket_details['current_status'] == 8)
        {
            $data = array(
                'ticket_id' => $ticket_id,
                'rsummary' => $ticket_details['resolution_summary'],
                'prob_descip' => $ticket_details['prob_desc'],
                'case_id' => $ticket_details['landmark'],
                'ticket_date'=>date('d-m-Y',strtotime($ticket_details['last_update'])),
                'product_id'=>$ticket_details['product_name'],
                'model'=>$ticket_details['model'],
                'serial_no'=>$ticket_details['serial_no'],
                'service_group_name'=>$ticket_details['service_group'],
                'status_name'=>"Completed",
                'technician_name'=>$this->Punch_in_v2->get_emp_name($ticket_details['tech_id']),
                'signature'=>$ticket_details['signature'],
                'service_image' => $ticket_details['service_attach_image'],     
                'customer_name' =>$cust_details['customer_name'],
                'customer_door_no'=> $cust_details['door_num'],
                'cust_address'=> $cust_details['address'],
                'cust_town'=> $cust_details['cust_town'],
                'cust_city'=> $cust_details['city'],
                'cus_pincode'=>$cust_details['pincode'],
                'cust_email'=> $cust_details['email_id'],
                'cust_mobile' => $cust_details['contact_number'],
                'company_name'=> $company_details['company_name'],
                'company_logo'=> $company_details['company_logo'],
                'company_dno'=> $company_details['building_no'],
                'company_address'=>$company_details['base_address'],
                'company_town'=>$company_details['town'],
                'company_city'=>$company_details['city'],
                'company_state'=>$company_details['state'],
                'company_contact'=>$company_details['company_contact'],
                'company_pincode'=>$company_details['pincode'],
                'service_id'=>$service_id,
                'activities'=>$activite_details
               
            );

        }
        else if($ticket_details['current_status'] == 14)
        {
            $data = array(
                'ticket_id' => $ticket_id,
                'rsummary' => $ticket_details['resolution_summary'],
                'prob_descip' => $ticket_details['prob_desc'],
                'case_id' => $ticket_details['landmark'],
                'ticket_date'=>date('d-m-Y',strtotime($ticket_details['last_update'])),
                'product_id'=>$ticket_details['product_name'],
                'model'=>$ticket_details['model'],
                'serial_no'=>$ticket_details['serial_no'],
                'service_group_name'=>$ticket_details['service_group'],
                'status_name'=>"Spare Requested",
                'technician_name'=>$this->Punch_in_v2->get_emp_name($ticket_details['tech_id']),
                'signature'=>$ticket_details['signature'],
                'service_image' => $ticket_details['service_attach_image'],     
                'customer_name' =>$cust_details['customer_name'],
                'customer_door_no'=> $cust_details['door_num'],
                'cust_address'=> $cust_details['address'],
                'cust_town'=> $cust_details['cust_town'],
                'cust_city'=> $cust_details['city'],
                'cus_pincode'=>$cust_details['pincode'],
                'cust_email'=> $cust_details['email_id'],
                'cust_mobile' => $cust_details['contact_number'],
                'company_name'=> $company_details['company_name'],
                'company_logo'=> $company_details['company_logo'],
                'company_dno'=> $company_details['building_no'],
                'company_address'=>$company_details['base_address'],
                'company_town'=>$company_details['town'],
                'company_city'=>$company_details['city'],
                'company_state'=>$company_details['state'],
                'company_contact'=>$company_details['company_contact'],
                'company_pincode'=>$company_details['pincode'],
                'service_id'=>$service_id,
                'activities'=>$activite_details
               
            );

        }

        else if($ticket_details['current_status'] == 50)
        {
            $data = array(
                'ticket_id' => $ticket_id,
                'rsummary' => $ticket_details['resolution_summary'],
                'prob_descip' => $ticket_details['prob_desc'],
                'case_id' => $ticket_details['landmark'],
                'ticket_date'=>date('d-m-Y',strtotime($ticket_details['last_update'])),
                'product_id'=>$ticket_details['product_name'],
                'model'=>$ticket_details['model'],
                'serial_no'=>$ticket_details['serial_no'],
                'service_group_name'=>$ticket_details['service_group'],
                'status_name'=>"Otp Verified",
                'technician_name'=>$this->Punch_in_v2->get_emp_name($ticket_details['tech_id']),
                'signature'=>$ticket_details['signature'],
                'service_image' => $ticket_details['service_attach_image'],     
                'customer_name' =>$cust_details['customer_name'],
                'customer_door_no'=> $cust_details['door_num'],
                'cust_address'=> $cust_details['address'],
                'cust_town'=> $cust_details['cust_town'],
                'cust_city'=> $cust_details['city'],
                'cus_pincode'=>$cust_details['pincode'],
                'cust_email'=> $cust_details['email_id'],
                'cust_mobile' => $cust_details['contact_number'],
                'company_name'=> $company_details['company_name'],
                'company_logo'=> $company_details['company_logo'],
                'company_dno'=> $company_details['building_no'],
                'company_address'=>$company_details['base_address'],
                'company_town'=>$company_details['town'],
                'company_city'=>$company_details['city'],
                'company_state'=>$company_details['state'],
                'company_contact'=>$company_details['company_contact'],
                'company_pincode'=>$company_details['pincode'],
                'service_id'=>$service_id,
                'activities'=>$activite_details
               
            );

        }

        else if($ticket_details['current_status'] == 12)
        {
            $data = array(
                'ticket_id' => $ticket_id,
                'rsummary' => $ticket_details['resolution_summary'],
                'prob_descip' => $ticket_details['prob_desc'],
                'case_id' => $ticket_details['landmark'],
                'ticket_date'=>date('d-m-Y',strtotime($ticket_details['last_update'])),
                'product_id'=>$ticket_details['product_name'],
                'model'=>$ticket_details['model'],
                'serial_no'=>$ticket_details['serial_no'],
                'service_group_name'=>$ticket_details['service_group'],
                'status_name'=>"Closed",
                'technician_name'=>$this->Punch_in_v2->get_emp_name($ticket_details['tech_id']),
                'signature'=>$ticket_details['signature'],
                'service_image' => $ticket_details['service_attach_image'],     
                'customer_name' =>$cust_details['customer_name'],
                'customer_door_no'=> $cust_details['door_num'],
                'cust_address'=> $cust_details['address'],
                'cust_town'=> $cust_details['cust_town'],
                'cust_city'=> $cust_details['city'],
                'cus_pincode'=>$cust_details['pincode'],
                'cust_email'=> $cust_details['email_id'],
                'cust_mobile' => $cust_details['contact_number'],
                'company_name'=> $company_details['company_name'],
                'company_logo'=> $company_details['company_logo'],
                'company_dno'=> $company_details['building_no'],
                'company_address'=>$company_details['base_address'],
                'company_town'=>$company_details['town'],
                'company_city'=>$company_details['city'],
                'company_state'=>$company_details['state'],
                'company_contact'=>$company_details['company_contact'],
                'company_pincode'=>$company_details['pincode'],
                'service_id'=>$service_id,
                'activities'=>$activite_details
               
            );

        }
//print_r($data); exit;
/**  Load html content **/  
		$this->pdf->load_Html($this->load->view('pdf_mail', $data, true));
		$this->pdf->setPaper('A4', 'portrait');
        //Load html view
        $this->pdf->render();
   		$this->pdf->stream("service_report.pdf", array("Attachment"=>0));
		      
    } 
    
    
  public function get_tech_avail()              ////////////////////  raju
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
         $this->load->model('Punch_in');
    
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result=$this->Punch_in->get_tech_avail($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "tech_avail" => $result,
                    "result" => "Success"
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "tech_avail" => 'Technician ID not match',
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }       
        
 public function fcm_check()
    {
      
    
     $device_token=$this->input->post('device_token');

if($device_token != " " || $device_token != NULL) { 
     $this->load->model('Pushnotify');
$fcmkey=$device_token;
$software=1;
$message='Test in Capricot';
$click_action='ticket';
$fcmkeypush = array('reg_token'=>$fcmkey,'message'=>$message,'software'=>$software,'click_action'=>$click_action);
$this->Pushnotify->fcmpushNotifications($fcmkeypush);   
 }
    }   

public function sms_check()
    {
      
    
     $message=$this->input->post('message');
    $number=$this->input->post('number');

if($number != " " || $number != NULL) { 
     $this->load->model('Pushnotify');

$smsvalue = array('message'=>$message,'number'=>$number);
$this->Pushnotify->sms_send_mobile($smsvalue);  
 }
    }   



    
 public function smtp_email_check()
    {
       $this->load->helper('url');
     $to=$this->input->post('email');
    
     /*$code='34234';
    $emailcontent=" <!doctype html>
                <html>
                <head>
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
               
                <body>
                <p>Dear Customer,</p>
                <p>Welcome to Capricot.</b></p>
                <p>If you are satisfied with our service.</p>
                <p>Please share below OTP with Technician.</p>
                <p>$code</p> 
                <p>Thanks & regards,</p> <p>Capricot Team.</p>

                </body>
                </html>";
                */

 /*  $url=base_url().'index.php?/Controller_service/Feedback/?token=45645646';
    $emailcontent=" <!doctype html>
                <html>
                <head>
                <meta name='viewport' content='width=device-width' />
                 <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
               
                <body>
                <p>Dear Customer,</p>
                Welcome to Capricot.
                <p> </p>
                please take some time to rate our service.
                <p> </p>
                Click the below link.
                <p> </p>
             $url
               <p> </p>
                <p>Thanks & regards,</p> <p>Capricot Team.</p>

                </body>
                </html>";
*/

/*   $ticket_id='TK_1883';
    $emailcontent=" <!doctype html>
                <html>
                <head>
                 <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
               
                <body>
                <p>Dear Customer,</p>
                <p>Welcome to Capricot.</p>
               Please find your Capricot ticket summary for <b>$ticket_id</b>
               <p> </p>
               If any queries please mail to : support@kaspontechworks.com
               <p> </p>
               <p>Thanks & regards,</p> <p>Capricot Team.</p>

                </body>
                </html>"; 
                */
     
 $tick_id='TK_234';
$prob_desc='sffds sfsfs sfsfs ffsf sdf';    
$cont='AMC';     
$url='';     
        $emailcontent=" <!doctype html>
                <html>
                <head>
                 <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
               
                <body>
                <p>Dear Customer,</p>
                <p>Welcome to Capricot.</p>
                <p>Your ticket has been raised successfully.</p>
                <p>Kindly find the details below.</p>
               <b> Ticket Id:</b> $tick_id
               <p> </p>
                <b>Problem description:</b> $prob_desc
               <p> </p>
               <b>Contract Type:</b> $cont
               <p> </p>";
               if($url!='')
               {
                   
                "<img src=".base_url()."$url width='250' height='150'>";
               }
                  else
                  {
                  }
            $emailcontent.="<p> </p>
               <p>Thanks & regards,</p> <p>Capricot Team.</p>

                </body>
                </html>"; 

     
          $config   = Array(
            'protocol' => 'smtp',
                  'smtp_host' => 'smtp.office365.com',
                  'smtp_port' => '587',
                  'smtp_timeout' => '30',
                  'smtp_user' => 'hwsupport@capricot.com',
                  'smtp_pass' => 'Support14',
                  'mailtype' => 'html',
                  'smtp_crypto' => 'tls',
                  'wordwrap' => TRUE,
                  'charset' => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('hwsupport@capricot.com', 'Capricot');
        $this->email->to($to);
        $this->email->subject('Capricot - Customer OTP Verfication');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
       
    $result = $this->email->send();
     
       if($result) 
       {
            echo " Mail Send Successfully";
        //  echo "<br>";
       }
    else
    {
          echo " Mail Not Send Successfully";
    }
    
}

        
    
}
