<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fb extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');	
		
	}

	public function index()
	{
			
	}
	
 public function fb()
    {
        $this->load->helper('url');
	  $this->load->model('Punch_in_v2');
        $this->load->database();
        
              $tokenid=$this->input->get('tk');
        $arraydata=$this->Punch_in_v2->getfeedbackcustomerdata($tokenid);
        $company_details = $this->Punch_in_v2->get_det_company($arraydata['company_id']);
        $data['ticket_id']=$arraydata['ticket_id'];
        $data['primaryid']=$arraydata['id'];
          $data['emailid']=$arraydata['email_id'];
          $data['company_logo'] = $company_details['company_logo'];
    $data['ticketcurrentstatus']=$arraydata['current_status'];
    $data['work_type'] = $arraydata['work_type'];
        //$data['token'] = $tokenid;
        $this->load->view('Feedback',$data);
    }
	
			
	
}