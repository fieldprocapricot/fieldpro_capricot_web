<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Techlogin extends CI_Controller {
	public function __construct() {
			
			parent::__construct();	
			$this->load->library('encrypt');			
		 	$this->load->helper(array('url','cookie'));
			$this->load->helper('form');						
			$this->load->library('form_validation');
		}
	public function index()
	{
	$this->load->helper('url');
    $this->load->database();
$username=$this->input->post('username');
 $password=$this->input->post('password');
$device_token=$this->input->post('device_token');
$reg_id=$this->input->post('reg_id');
 $encrypted_password=$password;
$this->load->model('Techlogin1');
$result=$this->Techlogin1->check_login($username);
$result_1=$this->encrypt->decode($result);		
if($result_1==$encrypted_password)
{
if($device_token!='' || $reg_id!='')
{
$this->load->model('Techlogin1');
$result2=$this->Techlogin1->store_id($device_token,$reg_id,$username);
if($result2==1)
{
 $userid=$this->Techlogin1->get_userid($username);
 $name=$this->Techlogin1->get_username($username);
$tech_avail=$this->Techlogin1->get_tech_avail($username);	
 $tech_rate=$this->Techlogin1->get_rating($userid);
 $image=$this->Techlogin1->get_image($username);
 $mobile=$this->Techlogin1->get_mobile($username);
 $email_id=$this->Techlogin1->get_email_id($username);
 $location=$this->Techlogin1->get_location($username);
 $companyname = $this->Techlogin1->get_companyname($username);
 $companyid = $this->Techlogin1->get_companyid($username);
 $json = array("status" =>1, "msg" => "Logged in Successfully!!!","tech_id"=>$userid,"tech_name"=>$name,"tech_rate"=>$tech_rate,"base_url"=>base_url(), "image"=>base_url() . $image,"mobile"=>$mobile,"email"=>$email_id, "location"=>$location,"company_name"=>$companyname,"company_id"=>$companyid,"tech_avail"=>$tech_avail);
}
else
{
$json = array("status" => 0, "msg" => "Error storing device token/Register ID!!!");
}
}
else
{
$json = array("status" => 0, "msg" => "Please provide Device Token/Register ID!!!");
}
}
else
{
$json = array("status" => 0, "msg" => "Please provide correct details!!!");
}
echo json_encode($json);
	}
public function push_notification()
{
$this->load->helper('url');
    $this->load->database();
$deviceToken=$this->input->post('device');
$passphrase = 'Kaspon123';
$message ="You have assigned a new ticket in tambaram"; 
$url ="";

//if (!$message || !$url)
 //   exit('Example Usage: $php newspush.php \'Breaking News!\' \'https://raywenderlich.com\'' . "\n");

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', '/home/kasponte/public_html/fieldpro/fps/application/controllers/FieldProService_APNS.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(
  'ssl://gateway.sandbox.push.apple.com:2195', $err,
  $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
  exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
  'alert' => $message,
  'sound' => 'default',
  'link_url' => $url,
  'category' => "NEWS_CATEGORY",
  );

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
  echo 'Message not delivered' . PHP_EOL;
else
  echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);
}
}
