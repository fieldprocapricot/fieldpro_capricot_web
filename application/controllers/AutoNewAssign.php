<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AutoNewAssign extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        $this->load->database();
    }
    public function autoassign()
    {
        $resu = array();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('AutoAssign');
        $company_i = $this->input->post('company_id');
        $tech_id   = $this->input->post('tech_id');
        if ($tech_id != '') {
            $this->load->model('Punch_in');
            $company_i  = $this->Punch_in->get_company($tech_id);
            $product    = $this->Punch_in->get_tech_product($tech_id);
            $product_id = $product[0]['product_id'];
            $cat        = $this->Punch_in->get_tech_cat($tech_id);
            $cat_id     = $cat[0]['cat_id'];
            $company_id = $company_i;
            $tkts       = $this->AutoAssign->get_related_tkts($company_id, $product_id, $cat_id);
            if (!empty($tkts)) {
                foreach ($tkts as $row) {
                    $data = array();
                    
                    $this->load->model('AutoAssign');
                    $address   = $row['address'];
                    $location  = $row['location'];
                    $product1  = $product_id;
                    $category1 = $cat_id;
                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('product', 'all');
                    $this->db->group_end();
                    $query  = $this->db->get();
                    $result = $query->result_array();
                    if (!empty($result)) {
                        $product = 'all';
                    } else {
                        $product = $row['product_id'];
                    }
                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('category', 'all');
                    $this->db->group_end();
                    $query1  = $this->db->get();
                    $result1 = $query1->result_array();
                    if (!empty($result1)) {
                        $category = 'all';
                    } else {
                        $category = $row['cat_id'];
                    }
                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('cust_category', 'all');
                    $this->db->group_end();
                    $query2  = $this->db->get();
                    $result2 = $query2->result_array();
                    if (!empty($result2)) {
                        $cust_category = 'all';
                    } else {
                        $cust_category = $row['cust_category'];
                    }
                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('call_category', 'all');
                    $this->db->group_end();
                    $query3  = $this->db->get();
                    $result3 = $query3->result_array();
                    if (!empty($result3)) {
                        $call_category = 'all';
                    } else {
                        $call_category = $row['call_tag'];
                    }
                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('service_category', 'all');
                    $this->db->group_end();
                    $query4  = $this->db->get();
                    $result4 = $query4->result_array();
                    if (!empty($result4)) {
                        $service_category = 'all';
                    } else {
                        $service_category = $row['call_type'];
                    }
                    $where8 = array(
                        'company_id' => $company_id,
                        'product' => $product,
                        'category' => $category,
                        'cust_category' => $cust_category,
                        'service_category' => $service_category,
                        'call_category' => $call_category
                    );
                    $this->db->select('ref_id');
                    $this->db->from('sla_combination');
                    $this->db->where($where8);
                    $query5  = $this->db->get();
                    $result5 = $query5->result_array();
                    if (!empty($result5)) {
                        $ref_id = $result5[0]['ref_id'];
                        $this->db->select('resolution_time,response_time,priority_level,acceptance_time');
                        $this->db->from('sla_mapping');
                        $this->db->where('ref_id', $ref_id);
                        $query6  = $this->db->get();
                        $result6 = $query6->result_array();
                        
                        if (!empty($result6)) {
							
                            $resolution      = date('H:i:s', strtotime($result6[0]['resolution_time']));
                            $response        = date('H:i:s', strtotime($result6[0]['response_time']));
                            $priority        = $result6[0]['priority_level'];
                            $acceptance_time = $result6[0]['acceptance_time'];
                            
                            // Get JSON results from this request
                            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
                            // Convert the JSON to an array
                            $geo = json_decode($geo, true);
                            if ($geo['status'] == 'OK') {
                                // Get Lat & Long
                                $latitude  = $geo['results'][0]['geometry']['location']['lat'];
                                $longitude = $geo['results'][0]['geometry']['location']['lng'];
                                if ($latitude <= 0 || $longitude <= 0) {
                                    $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($location) . '&sensor=false');
                                    // Convert the JSON to an array
                                    $geo = json_decode($geo, true);
                                    if ($geo['status'] == 'OK') {
                                        // Get Lat & Long
                                        $latitude  = $geo['results'][0]['geometry']['location']['lat'];
                                        $longitude = $geo['results'][0]['geometry']['location']['lng'];
                                        $this->load->model('AutoAssign');
                                        $tech = $this->AutoAssign->get_nearbylocation($latitude, $longitude, $product1, $category1);
                                        if (!empty($tech)) {
                                            foreach ($tech as $row1) {
                                                if ($row1['technician_id'] == $tech_id) {
                                                    $this->load->model('AutoAssign');
                                                    echo $result = $this->AutoAssign->assign($row['ticket_id'], $row1['technician_id'], $resolution, $response, $priority, $acceptance_time);
                                                    if ($result == 1) {
                                                        $res1   = $this->AutoAssign->update_tech_task($row1['technician_id'], $row1['today_task_count'], $row1['task_count']);
                                                        $this->load->model('Model_service');
                                                        $notify = $row['ticket_id'] . " is newly assigned to you!!!";
                                                        $role   = "Technician";
                                                        $key    = "new";
                                                        $this->Model_service->update_notify($notify, $company_id, $role, $row1['technician_id'], $key);
                                                        $this->load->model('Punch_in');
                                                        $this->Punch_in->get_notify($row1['technician_id']);
                                                        $tech_id=$row['technician_id'];
                                                       $this->load->model('Pushnotify');
														$token=$this->Pushnotify->get_tech_token($tech_id);
														$device_token=$token['device_token'];
														$reg_id=$token['reg_id'];
														if($device_token=="")
														{

														$this->Pushnotify->send_notification($reg_id,$notify,"New Ticket Assigned");
														}
														else
														{
														$this->Pushnotify->iOS($notify,"New Ticket Assigned", $device_token);
														}
                                                        $resu = array(
                                                            "status" => "1",
                                                            "msg" => "Tickets assigned"
                                                        );
                                                    } else {
                                                        $resu = array(
                                                            "status" => "0",
                                                            "msg" => "Not assigned"
                                                        );
                                                    }
                                                }
                                            }
                                        } else {
                                            $resu = array(
                                                "status" => "0",
                                                "msg" => "Error No Tickets available"
                                            );
                                        }
                                        
                                    }
                                } else {
                                    $this->load->model('AutoAssign');
                                    $tech = $this->AutoAssign->get_nearbylocation($latitude, $longitude, $product1, $category1);
                                    if (!empty($tech)) {
                                        foreach ($tech as $row1) {
                                            if ($row1['technician_id'] == $tech_id) {
                                                $this->load->model('AutoAssign');
                                                $result = $this->AutoAssign->assign($row['ticket_id'], $row1['technician_id'], $resolution, $response, $priority, $acceptance_time);
                                                if ($result == 1) {
                                                    $res1   = $this->AutoAssign->update_tech_task($row1['technician_id'], $row1['today_task_count'], $row1['task_count']);
                                                    $this->load->model('Model_service');
                                                    $notify = $row['ticket_id'] . " is newly assigned to you!!!";
                                                    $role   = "Technician";
                                                    $key    = "new";
                                                    
                                                     $this->Model_service->update_notify($notify, $company_id, $role, $row1['technician_id'], $key);
                                                    $this->load->model('Punch_in');
                                                    $this->Punch_in->get_notify($row1['technician_id']);
                                                    $tech_id=$row1['technician_id'];
                                                       $this->load->model('Pushnotify');
														$token=$this->Pushnotify->get_tech_token($tech_id);
														$device_token=$token['device_token'];
														$reg_id=$token['reg_id'];
														if($device_token=="")
														{

														$this->Pushnotify->send_notification($reg_id,$notify,"New Ticket Assigned");
														}
														else
														{
														$this->Pushnotify->iOS($notify,"New Ticket Assigned", $device_token);
														}
                                                    $resu = array(
                                                        "status" => "1",
                                                        "msg" => "Tickets assigned"
                                                    );
                                                } else {
                                                    $resu = array(
                                                        "status" => "0",
                                                        "msg" => "Not assigned"
                                                    );
                                                }
                                            }
                                        }
                                    } else {
                                        $resu = array(
                                            "status" => "0",
                                            "msg" => "Error No Tickets available"
                                        );
                                    }
                                }
                            }
                            
                        }
                    }
                }
            } else {
                echo "No New Tickets available";
            }
        } else {
            $company = $this->AutoAssign->get_company();
            foreach ($company as $cmp) {
                
                $company_id = $cmp['company_id'];
                $tkts       = $this->AutoAssign->get_tkts($company_id);
                if (!empty($tkts)) {
                    foreach ($tkts as $row) {
                        $data = array();
                        $this->load->model('AutoAssign');
                        $address  = $row['address'];
                        $location = $row['location'];
                        if ($tech_id != '') {
                            $product1  = $product_id;
                            $category1 = $cat_id;
                        } else {
                            $product1  = $row['product_id'];
                            $category1 = $row['cat_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('cust_category', 'all');
                        $this->db->group_end();
                        $query2  = $this->db->get();
                        $result2 = $query2->result_array();
                        if (!empty($result2)) {
                            $cust_category = 'all';
                        } else {
                            $cust_category = $row['cust_category'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('call_category', 'all');
                        $this->db->group_end();
                        $query3  = $this->db->get();
                        $result3 = $query3->result_array();
                        if (!empty($result3)) {
                            $call_category = 'all';
                        } else {
                            $call_category = $row['call_tag'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('service_category', 'all');
                        $this->db->group_end();
                        $query4  = $this->db->get();
                        $result4 = $query4->result_array();
                        if (!empty($result4)) {
                            $service_category = 'all';
                        } else {
                            $service_category = $row['call_type'];
                        }

                        $where8 = array(
                            'company_id' => $company_id,
                           // 'product' => $product,
                            //'category' => $category,
                           // 'cust_category' => $cust_category,
                           // 'service_category' => $service_category,
                           // 'call_category' => $call_category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time,priority_level,acceptance_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            if (!empty($result6)) {
                                $resolution      = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                $response        = date('H:i:s', strtotime($result6[0]['response_time']));
                                $priority        = $result6[0]['priority_level'];
                                $acceptance_time = date('H:i:s', strtotime($result6[0]['acceptance_time']));
                                
                                
                                // Get JSON results from this request
                                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
                                // Convert the JSON to an array
                                $geo = json_decode($geo, true);
                                if ($geo['status'] == 'OK') {
                                    // Get Lat & Long
                                    $latitude  = $geo['results'][0]['geometry']['location']['lat'];
                                    $longitude = $geo['results'][0]['geometry']['location']['lng'];
                                    if ($latitude <= 0 || $longitude <= 0) {
                                        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($location) . '&sensor=false');
                                        // Convert the JSON to an array
                                        $geo = json_decode($geo, true);
                                        if ($geo['status'] == 'OK') {
                                            // Get Lat & Long
                                            $latitude  = $geo['results'][0]['geometry']['location']['lat'];
                                            $longitude = $geo['results'][0]['geometry']['location']['lng'];
                                            $this->load->model('AutoAssign');
                                            $tech = $this->AutoAssign->get_nearbylocation($latitude, $longitude, $product1, $category1);
                                            if (!empty($tech)) {
                                                foreach ($tech as $row1) {
                                                    $this->load->model('AutoAssign');
                                                    $result = $this->AutoAssign->assign($row['ticket_id'], $row1['technician_id'], $resolution, $response, $priority, $acceptance_time);
                                                    if ($result == 1) {
                                                        $res1 = $this->AutoAssign->update_tech_task($row1['technician_id'], $row1['today_task_count'], $row1['task_count']);
                                                        array_push($resu, array(
                                                            "status" => "1",
                                                            "result" => "Assigned"
                                                        ));
                                                         $this->load->model('Model_service');
                                                        $notify = $row['ticket_id'] . " is newly assigned to you!!!";
                                                        $role   = "Technician";
                                                        $key    = "new";
                                                          $this->Model_service->update_notify($notify, $company_id, $role, $row1['technician_id'], $key);
                                                        $this->load->model('Punch_in');
                                                        $this->Punch_in->get_notify($row1['technician_id']);
                                                        $tech_id=$row1['technician_id'];
                                                       $this->load->model('Pushnotify');
														$token=$this->Pushnotify->get_tech_token($tech_id);
														$device_token=$token['device_token'];
														$reg_id=$token['reg_id'];
														if($device_token=="")
														{

														$this->Pushnotify->send_notification($reg_id,$notify,"New Ticket Assigned");
														}
														else
														{
														$this->Pushnotify->iOS($notify,"New Ticket Assigned", $device_token);
														}
                                                    } else {
                                                        $resu = array(
                                                            "status" => "0",
                                                            "msg" => "Not assigned"
                                                        );
                                                    }
                                                }
                                            } else {
                                                $resu = array(
                                                    "status" => "0",
                                                    "msg" => "Error No Technicians available"
                                                );
                                            }
                                            
                                        }
                                    } else {
                                        $this->load->model('AutoAssign');
                                        $tech = $this->AutoAssign->get_nearbylocation($latitude, $longitude, $product1, $category1);
                                        if (!empty($tech)) {
                                            foreach ($tech as $row1) {
                                                $this->load->model('AutoAssign');
                                                $this->load->model('Pushnotify');
                                                $result = $this->AutoAssign->assign($row['ticket_id'], $row1['technician_id'], $resolution, $response, $priority, $acceptance_time);
                                                if ($result == 1) {
                                                    $res1 = $this->AutoAssign->update_tech_task($row1['technician_id'], $row1['today_task_count'], $row1['task_count']);
                                                    array_push($resu, array(
                                                        "status" => "1",
                                                        "result" => "Assigned"
                                                    ));
                                                     $this->load->model('Model_service');
                                                    $notify = $row['ticket_id'] . " is newly assigned to you!!!";
                                                    $role   = "Technician";
                                                    $key    = "new";
                                                     $tech_id=$row1['technician_id'];
                                                       $this->load->model('Pushnotify');
														$token=$this->Pushnotify->get_tech_token($tech_id);
														$device_token=$token['device_token'];
														$reg_id=$token['reg_id'];
														if($device_token=="")
														{

														$this->Pushnotify->send_notification($reg_id,$notify,"New Ticket Assigned");
														}
														else
														{
														$this->Pushnotify->iOS($notify,"New Ticket Assigned", $device_token);
														}
                                                      $this->Model_service->update_notify($notify, $company_id, $role, $row1['technician_id'], $key);
                                                    $this->load->model('Punch_in');
                                                    $this->Punch_in->get_notify($row1['technician_id']);
                                                } else {
                                                    $resu = array(
                                                        "status" => "0",
                                                        "msg" => "Not assigned"
                                                    );
                                                }
                                            }
                                        } else {
                                            $resu = array(
                                                "status" => "0",
                                                "msg" => "Error No Technicians available"
                                            );
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                } else {
                    $resu = array(
                        "status" => "0",
                        "msg" => "Error"
                    );
                }
            }
        }
        echo json_encode($resu);
    }
    public function auto_reassign()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('AutoAssign');
        $company = $this->AutoAssign->get_company();
        foreach ($company as $cmp) {
            $company_id = $cmp['company_id'];
            $tech       = $this->AutoAssign->auto_reassign($company_id);
            if ($tech == 1) {
                echo "Technicians Assigned";
            } else {
                echo "No Technicians available";
            }
        }
    }
    public function auto_escalate()
    {
        $resu = array();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('AutoAssign');
        $ticket_id = $this->input->post('ticket_id');
        $tech_id   = $this->input->post('tech_id');
        
        $tech = $this->AutoAssign->auto_escalate($ticket_id, $tech_id);
        if ($tech == 1) {
            $resu = array(
                "status" => "1",
                "result" => "Escalated"
            );
        } else {
            $resu = array(
                "status" => "0",
                "msg" => "No Technicians available"
            );
        }
        echo json_encode($resu);
    }
    public function auto_spare_approval()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('AutoAssign');
        $company = $this->AutoAssign->get_company();
        foreach ($company as $cmp) {
            $company_id = $cmp['company_id'];
            $result     = $this->AutoAssign->auto_spare_approval($company_id);
            if ($result == 1) {
                echo "Spare request accepted";
            } else {
                echo "Awaiting for Spare request";
            }
        }
    }
    public function auto_tech_task()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('AutoAssign');
        $tech_id = $this->input->post('tech_id');
        if ($tech_id != '') {
            $this->load->model('Punch_in');
            $company_i  = $this->Punch_in->get_company($tech_id);
            $company_id = $company_i;
            $result     = $this->AutoAssign->auto_tech_task1($company_id, $tech_id);
            if ($result == 1) {
                $date = date('d H');
                if ($date <= '01 02') {
                    $this->AutoAssign->auto_tech($company_id, $tech_id);
                }
                $resu = array(
                    "status" => "1",
                    "result" => "Tech task updated"
                );
            } else {
                $resu = array(
                    "status" => "0",
                    "result" => "Error!!!"
                );
            }
        } else {
            $company = $this->AutoAssign->get_company();
            foreach ($company as $cmp) {
                $company_id = $cmp['company_id'];
                $result     = $this->AutoAssign->auto_tech_task($company_id);
                if ($result == 1) {
                    $date = date('d H');
                    if ($date <= '01 02') {
                        $this->AutoAssign->auto_tech($company_id, $tech_id);
                    }
                    $resu = array(
                        "status" => "1",
                        "result" => "Tech task updated"
                    );
                } else {
                    $resu = array(
                        "status" => "1",
                        "result" => "No Tech available"
                    );
                }
                
            }
        }
        echo json_encode($resu);
    }
    
}