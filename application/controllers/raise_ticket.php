<!DOCTYPE html>
<html lang="en">
   <!--<![endif]-->
   <!-- BEGIN HEAD -->
   <head>
      <?php
         $company_id = $this->session->userdata('companyid');
         ?>
      <?php
         $company_id=$this->session->userdata('companyid');
         $region=$user['region'];$area=$user['area'];$location=$user['location'];
         include 'assets/lib/cssscript.php';
         ?>   
      <style>
         #errmsg
         {
         color: red;
         }  
         #errmsg1
         {
         color: red;
         }
        .error{          
            color: red;      
         }    
         .fa-big{
         font-size: 16px !important;
         }
         #span-button:hover
         { cursor: pointer !important;}
         .ui-autocomplete{
         z-index:999999 !important;
         }span.fileinput-new {
         color: #000 !important;
         }
         .datepicker > .datepicker_header > .icon-home {
    display: none !important;
}
.datepicker > .datepicker_header > .icon-close {
     display: none !important;
}
      </style>
   </head>
   <!-- END HEAD -->
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
      <div class="page-wrapper">
         <!-- BEGIN HEADER -->
         <?php
            include "assets/lib/header_callcord.php";
            ?>
         <!-- END HEADER -->
         <div class="page-container">
         <div class="page-sidebar-wrapper">
	 
	 
	 
	 
	          <?php include "assets/lib/callcord_sidebar.php"?>
		           </div>
         <div class="page-content-wrapper">
                  <div class="page-content">
          
               <div class="tab-pane" id="tab_2">
                  <div class="portlet box dark">
                     <div class="portlet-title">
                        <div class="caption">
                           Raise Ticket
                        </div>
                        <div class="tools">
                           <a href="javascript:;" class="reload"> </a>
                           <!--<a href="javascript:;" class="remove"> </a>-->
                        </div>
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" id="form_sample_3">
                           <div class="form-body">
                              <h3 class="form-section">Customer Info</h3>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4" style="padding-left: 0px !important;">
                                       Contact Number
                                       <span class="required"> * </span></label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-phone"></i>
                                             </span>
                                             <input type="text" name="contact_num" id="contact_num" data-required="1" class="form-control"  placeholder="Enter Contact Number to Search"/> 
                                             <!-- <span class="help-block"> This is inline help </span>-->
                                             <span class="input-group-btn">
                                             <button class="btn blue" type="button" id="search">Go!</button>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Customer ID
                                       <span class="required"> * </span></label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" id="cust_id" name="cust_id" required readonly/>
                                          <!--<span class="help-block"> Select your gender. </span>-->
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                              </div>
                              <!--/row-->
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Name
                                       <span class="required"> * </span></label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="cname" id="cname" data-required="1" readonly/>
                                          <!--<input type="text" class="form-control" placeholder="dd/mm/yyyy">--> 
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Email Address
                                       <span class="required" readonly> * </span></label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-envelope"></i>
                                             </span>
                                             <input type="text" class="form-control" name="e_mail" id="e_mail" placeholder="Email Address" readonly/>                                   
                                          </div>
                                          <!--<span class="help-block"> Select your gender. </span>-->
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                              </div>
                              <div class="row">
                                 <!--/span-->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4" style="padding-left: 0px !important;">
                                       Alternate Number
                                       </label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-phone"></i>
                                             </span>
                                             <input type="text" class="form-control" name="altnum" id="altnum" readonly/>
                                          </div>
                                          <!--<input type="text" class="form-control" placeholder="dd/mm/yyyy">--> 
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                              </div>
                              <!--/row-->
                              <!-- New Section-->
                              <h3 class="form-section">Address</h3>
                              <!--/row-->
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Door/ Plot no.</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="door" id="door" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Street/ Locality</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="street" id="street" data-required="1" readonly/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Town</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="town" id="town" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Land mark</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="land_mark" id="land_mark" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">City</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="city" id="city" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">State</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="state" id="state" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                              </div>
                              <!--/row-->
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Country</label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-globe"></i>
                                             </span>
                                             <input type="text" class="form-control" id="country" name="country"/ readonly>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Post Code</label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-map-marker"></i>
                                             </span>
                                             <input type="text" class="form-control" id="pin" name="pin"/ readonly>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                                 <!--/span-->
                              </div>
                              <!--/row-->
                              <h3 class="form-section" id="contract_div" style="display:none;">Contract Info</h3>
                              <div class="row">
                                 <div class="table-responsive" id="prod_table" style="display:none;">
                                    <table id="datatable11" class="table table-hover ">
                                       <thead>
                                          <tr>
                                             <th>Product Category</th>
                                             <th>Sub-Category</th>
                                             <th>Contract Type</th>
                                             <th>Expiry Date</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>
                                       <tbody id="dynamic_table">
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <!--/row-->
                           </div>
                           <div class="form-actions">
                              <div class="row">
                                 <div class="col-md-6 pull-right">
                                    <button type="reset" id="clear_contract" class="btn red-haze btn-outline btn-circle btn-md">Reset</button>
                                 </div>
                              </div>
                           </div>
                     </div>
                     </form>
                     <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
     </div>
         <!--container-->
         <!-- BEGIN FOOTER -->
         <?php
            include "assets/lib/footer.php";
            ?>
         <!-- END FOOTER -->
      </div>
      <!-- Modal Starts-->
      <div id="myModal1" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header" style="border:0">
                  <div class="error" style="display:none">
                     <label id="rowdata"></label>
                  </div>
               </div>
               <div class="modal-body">
                  <div class="portlet light bordered" id="form_wizard_1" style="margin-bottom:0">
                     <div class="portlet-title" style="margin-top: -27px !important;">
                        <div class="actions">
                           <button type="button" class="close" data-dismiss="modal" style="margin-top: 20px !important;">&times;</button>   
                        </div>
                     </div>
                     <div class="portlet-body form">
                        <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                           <div class="form-wizard">
                              <div class="form-body">
                                 <ul class="nav nav-pills nav-justified steps">
                                    <li class="active">
                                       <a href="#tab1" data-toggle="tab" class="step" aria-expanded="true">
                                       <span class="number"> 1 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i> Customer Info</span>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#tab2" data-toggle="tab" class="step">
                                       <span class="number"> 2 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i>Product Info</span>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#tab3" data-toggle="tab" class="step">
                                       <span class="number"> 3 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i>Confirm Details</span>
                                       </a>
                                    </li>
                                 </ul>
                                 <div id="bar" class="progress progress-striped" role="progressbar" style="height:10px !important">
                                    <div class="progress-bar progress-bar-success" style="width: 20%;"> </div>
                                 </div>
                                 <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                       <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <!--<div class="alert alert-success display-none">
                                       <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>-->
                                    <div class="tab-pane active" id="tab1">
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Customer details</h3>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Ticket ID
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="tick_id" id="tick_id" class="form-control form-control1" readonly required /> 
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Customer ID
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="cu_id" id="cu_id" class="form-control form-control1" required readonly/> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <!--/span-->
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Name
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="name" id="name" class="form-control form-control1" required readonly/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">Contact Number
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="number" id="number" class="form-control form-control1" required readonly/>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <!--/span-->
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Email Id
                                                <span class="required" aria-required="true"> * </span>  </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="email" id="email" class="form-control form-control1" required readonly/> 
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">
                                                Alternate-Number </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="anum" id="anum" class="form-control form-control1" />
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Address Details</h3>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Door/ Plot no.
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="doornum" id="doornum" class="form-control form-control1" required />
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Street/ Locality
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="address" id="address" class="form-control form-control1" required />
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Town
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="cus_town" id="cus_town" class="form-control form-control1" required />
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Land mark 
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="land_mrk" id="land_mrk" class="form-control form-control1"/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">City
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="addr" id="addr" class="form-control form-control1" required />
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Country
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <!-- <input type="text" name="ccountry" id="ccountry" class="form-control form-control1" required /> -->
                                                   <select class="form-control form-control1" id="ccountry" name="ccountry" required>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                       <!--/row-->
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">State
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <!-- <input type="text" name="cstate" id="cstate" class="form-control form-control1" required />-->
                                                   <select class="form-control form-control1" id="cstate" name="cstate" required>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Post Code
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" id="postcode" name="postcode" class="form-control form-control1" required /> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Provide Product Info</h3>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Product-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="cust_prodname" id="cust_prodname" class="form-control form-control1" required readonly/> 
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Sub-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="cust_catname" id="cust_catname" class="form-control form-control1" required readonly/> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row" style="display:none">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Product-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="cust_prod" id="cust_prod" class="form-control form-control1" required /> 
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Sub-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="cust_cat" id="cust_cat" class="form-control form-control1" required /> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Model No.
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="model_no" name="model_no" required readonly/>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Contract Type
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="contract_type" id="contract_type" class="form-control form-control1" required readonly/> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Serial No.
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="s_no" name="s_no">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Call Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <!--<input type="text" name="call_tag" id="call_tag" class="form-control form-control1" required /> -->
                                                <select class="form-control" name="call_tag" id="call_tag" required>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5" style="padding-left: 0% !important;">Preferred Date-time
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" size="16" required class="form-control" id="preferred_date" name="preferred_date">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Upload Image
                                             </label>
                                             <div class="col-md-3">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                   <div class="input-group input-small">
                                                      <div class="form-control uneditable-input1 input-fixed input-small2" data-trigger="fileinput">
                                                         <!--<i class="fa fa-file fileinput-exists"></i>&nbsp;-->
                                                         <span class="fileinput-filename"> </span>
                                                      </div>
                                                      <span class="input-group-addon btn default btn-file">
                                                      <span class="fileinput-new"> Select file </span>
                                                      <span class="fileinput-exists"> Change </span>
                                                      <input type="file" id="upload_image" name="upload_image"> </span>
                                                      <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!--<div class="row">
                                          <div class="form-group col-md-12 col-sm-12">
                                                 <label class="control-label col-md-3" >Problem Description
                                                     <span class="required" aria-required="true"> * </span>
                                                 </label>
                                                 <div class="col-md-9" style="margin-left: -30px !important;">
                                                   <textarea id="prob_desc" name="prob_desc" rows="3" cols="73" style="resize:none;"></textarea>
                                                 </div>
                                             </div>
                                          </div>-->
                                          <div class="row">
                                          <div class="form-group col-md-6 col-sm-12 products">
                                                            <label class="control-label col-md-5">Work Type
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                 <select name="work_type" id="work_type" class="form-control form-control1" tabindex="-1" aria-hidden="true" required >
                                                                   <option value="" selected disabled>Select Work Type</option>
                                                                    <?php
                                                                       
                                                                            foreach ($servicegroup->result() as $row1) {
                                                                        ?>
                                                                   <option value="<?php echo $row1->service_group_id; ?>"><?php echo $row1->service_group; ?></option>
                                                                   <?php } ?>
                                                                <!--   <option value="1">Installation</option>
                                                                   <option value="2">Maintenance</option>
                                                                   <option value="3">Break down</option>-->

                                                                </select>
                                                            </div>
                                                        </div>
                                          </div>
                                       <div class="row">
                                          <div class="form-group col-md-12 col-sm-12">
                                             <label class="control-label col-md-3" style="text-align: left;margin-left: -11px !important;">Problem Description
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-9" style="margin-left: -30px !important;">
                                                <textarea id="prob_desc" name="prob_desc" rows="3" cols="73" style="resize:none;" required></textarea>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row" style="display:none">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php
                                                   echo $this->session->userdata('companyid');
                                                   ?>" readonly>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane" id="tab3">
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Customer details</h3>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Ticket ID:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="tick_id"> </p>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Name:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="name"> </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Email ID:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="email"> </p>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Contact Number:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="number"> </p>
                                             </div>
                                          </div>
                                       </div>
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Product Info</h3>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Product-Category:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="cust_prodname"> </p>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Sub-Category:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="cust_catname"> </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Model No:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="model_no"> </p>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Contract Type :</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="contract_type"> </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-actions" style="border-top:1px solid #ddd !important">
                              <div class="row">
                                 <div class="col-md-offset-5 col-md-7">
                                    <a href="javascript:;" class="btn btn-circle red btn-outline button-previous disabled" style="display: none;">
                                    <i class="fa fa-angle-left"></i> Back </a>
                                    <a href="javascript:;" class="btn btn-circle blue btn-outline button-next"> Continue
                                    <i class="fa fa-angle-right"></i>
                                    </a>
                                    <a href="javascript:;" class="btn btn-circle green btn-outline button-submit" style="display: none;" id="raise_buttons"> Submit
                                    <i class="fa fa-check"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                     </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--/end Modal-->
      <div id="myModal3" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-body">
                  <div class="portlet light bordered" id="form_wizard_3" style="margin-bottom:0">
                     <div class="error" style="display:none">
                        <label id="rowdata_c"></label>
                     </div>
                     <div class="portlet-title">
                        <!--<div class="caption">
                           <i class=" icon-layers font-red"></i>
                           <span class="caption-subject font-red bold uppercase">Add User
                               <span class="step-title"> Step 1 of 2 </span>
                           </span>
                           </div>-->
                        <div class="actions">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>   
                        </div>
                     </div>
                     <div class="portlet-body form">
                        <form class="form-horizontal" action="#" id="submit_form_2" method="POST" novalidate="novalidate">
                           <div class="form-wizard">
                              <div class="form-body">
                                 <ul class="nav nav-pills nav-justified steps">
                                    <li class="active">
                                       <a href="#tab7" data-toggle="tab" class="step" aria-expanded="true">
                                       <span class="number"> 1 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i> Customer Info</span>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#tab8" data-toggle="tab" class="step">
                                       <span class="number"> 2 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i>Product Info</span>
                                       </a>
                                    </li>
                                 </ul>
                                 <div id="bar" class="progress progress-striped" role="progressbar" style="height:10px !important">
                                    <div class="progress-bar progress-bar-success" style="width: 50%;"> </div>
                                 </div>
                                 <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                       <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-none">
                                       <button class="close" data-dismiss="alert"></button> Your form validation is successful! 
                                    </div>
                                    <div class="tab-pane active" id="tab7" style="font-size: 13px;">
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important"></h3>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Customer ID
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="custid" id="custid" data-required="1" class="form-control" required readonly/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Customer Name
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="cusname" id="cusname" data-required="1" class="form-control" required/> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">Email Id
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="email" name="email" id="email" data-required="1" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">Contact Number
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="con_number" id="con_number" data-required="1" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">
                                                Alternate-Number </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="alter_num" id="alter_num" data-required="1" class="form-control" />
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Address Details</h3>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Door/ Plot no.<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <input type="text" name="plot_no" id="plot_no" data-required="1" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Street/ Locality<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <input type="text" name="street_name" id="street_name" data-required="1" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Town<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <input type="text" name="c_town" id="c_town" data-required="1" class="form-control" required/> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Land mark</label>
                                                <div class="col-md-8">
                                                   <input type="text" name="c_landmark" id="c_landmark" data-required="1" class="form-control"/>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">City<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <input type="text" name="city_name" id="city_name" data-required="1" class="form-control" required/> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Country<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="fa fa-globe"></i>
                                                      </span>
                                                      <select name="country_name" id="country_name" class="form-control" required> 
                                                      </select>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                       <!--/row-->
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">State<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <select name="state_name" id="state_name" data-required="1" class="form-control" required></select>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Post Code<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="fa fa-map-marker"></i>
                                                      </span>
                                                      <input type="text" name="pin_num" id="pin_num" class="form-control" required/> 
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <!--/span-->
                                       </div>
                                       <div class="row" style="display:none">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php
                                                   echo $this->session->userdata('companyid');
                                                   ?>" readonly>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane" id="tab8">
                                       <div class="row" style="border-bottom:1px solid #e2e2e2;margin-bottom: 2%;line-height: 0;">
                                          <div class="form-group col-md-10">
                                             <h3 class="block" style=" margin-left: 2% !important;">Provide Product Info</h3>
                                          </div>
                                          <div class="form-group col-md-2 pull-right">
                                             <a onClick="add_prod();" class="btn font-black tooltip-test" title="Add Product" style="margin-top:27%;"><i class="fa fa-plus-circle fa-2x"></i></a>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Product-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <select class="form-control" id="product_list" name="fields[0][]" required>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Sub-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <select class="form-control" id="cat_list" name="fields[0][]" required>
                                                   <option value="nocat" selected>Select Sub-Category</option>
                                                   <!--<option value="empty">Sub Category</option>-->
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Date of purchase
                                              <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input class="form-control form-control-inline input-mediumda date-picker" ata-date-
                                                   format="yyyy-mm-dd" size="10" type="text" id="dop" name="fields[0][]" required>                                            
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Contract Type
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <select class="form-control" id="contract_list" name="fields[0][]" required>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row" style="display:none !important">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Expiry Date
                                              <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control" id="expiry_list" name="fields[0][]"/required>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Model No.
                                              <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control" id="emp_id" name="fields[0][]">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Serial No.
                                              <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control" id="c_name" name="fields[0][]">
                                             </div>
                                          </div>
                                       </div>
                                       <!--<hr>-->
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-actions" style="border-top:1px solid #ddd !important">
                              <div class="row">
                                 <div class="col-md-offset-5 col-md-7">
                                    <a href="javascript:;" class="btn btn-circle red btn-outline button-previous disabled" style="display: none;">
                                    <i class="fa fa-angle-left"></i> Back </a>
                                    <a href="javascript:;" class="btn btn-circle blue btn-outline button-next"> Continue
                                    <i class="fa fa-angle-right"></i>
                                    </a>
                                    <a href="javascript:;" class="btn btn-circle green btn-outline button-submit" style="display: none;" id="customer_add"> Submit
                                    <i class="fa fa-check"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                     </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--add_confirm-->
      <div id="add_confirm" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
               </div>
               <div class="modal-body">
                  <form class="form-horizontal" id="add_subproduct">
                     <div class="form-group">
                        <div class="col-sm-12">
                           <p id="sla_detail_confirm"></p>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times">
                  </i> Close</button>
               </div>
            </div>
         </div>
      </div>
        <!--loading model-->
        <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->
      <?php
         include 'assets/lib/javascript.php';
         ?>   
     <script>         
         $(window).load(function(){       
          //$('.nav.navbar-nav').find('.open').removeClass( 'open' );
         $('.nav.navbar-nav').find('.open').removeClass('open');
         $('#service_raise_tkt').addClass('open');
         
         $('#ccountry').focus(function(){
         $('#ccountry').empty();
         populateCountries("ccountry", "cstate");
         });
         $('#country_name').focus(function(){
         $('#country_name').empty();
         populateCountries("country_name", "state_name");
         });
         });
      </script> 
      <script type="text/javascript">
         $(document).ready(function() {
         
                $('#preferred_date').appendDtpicker({
              "inline": false,
              "dateFormat": "YYYY-MM-DD hh:mm",
                                        "todayButton": true,
                                        "futureOnly": true,
                                         "closeOnSelected": true
          });
          
                 $("#dop").appendDtpicker({
              "inline": false,
              "dateFormat": "YYYY-MM-DD",
                                        "dateOnly": true,
                                        "maxDate": "+0d",
                                         "closeOnSelected": true
          });
          
          
         
              /* $("#postcode").on('keypress',(function (e) {
                             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                        $("#postcode").html('');
                                    $("#postcode").text("Numbers Only");
                       }
                  else {
                               if($(this).val().length>5){
                                     return false;
                               }
                               
                       }
                   })); */
         
                 /* commented on 09-08-2017
           $("#anum").on('keypress',(function (e) {
                             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                        $("#anum").html('');
                                    $("#anum").text("Numbers Only");
                       }
                  else {
                               if($(this).val().length>9){
                                     return false;
                               }
                               
                       }
                   })); */
         
                /*  $("#con_number").keydown(function (e) {
                                   // Allow: backspace, delete, tab, escape, enter and .
                      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                                  // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                       // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                                    // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }); */
         
                /*  $("#alter_num").keydown(function (e) {
                             // Allow: backspace, delete, tab, escape, enter and .
                      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                             // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                               // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                                 // let it happen, don't do anything
                         return;
                }
                         // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }); */
            
                /* $("#anum").keydown(function (e) {
                             // Allow: backspace, delete, tab, escape, enter and .
                      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                             // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                               // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                                 // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }); */
         
            var company_id="<?php echo $this->session->userdata('companyid');
            ?>";
          
            $.ajax({
              url: "<?php
            echo base_url();
            ?>" + "index.php?/controller_call/load_amctype",
              type: 'POST',
              data:{'company_id':company_id},
              success: function(data) {
              var result=JSON.parse(data);
                  console.log(result);
                  $('#contract_list').append('<option value="Contract Type" selected>Contract Type</option>');
                       for(i=0;i<result.length;i++)
                       {
                        console.log(result[i]['amc_type']);
                        var amctypedropdown=String(result[i]['amc_type']);
                      $('#contract_list').append('<option value="'+amctypedropdown+'">'+amctypedropdown+'</option>');
                       }
              }
            });
            
            $.ajax({
                url: "<?php
            echo base_url();
            ?>" + "index.php?/controller_call/modal_product",
                  type: 'POST',
                data:{'company_id':company_id},
                success: function(result) {
                var result=JSON.parse(result);
                      console.log(result);
                      //alert(result);
                      //return false;
                      $('#product_list').append('<option value="dummydata" selected>Product Category</option>');
                         for(i=0;i<result.length;i++)
                         {
                          $('#product_list').append('<option value='+result[i]['product_id']+'>'+result[i]['product_name']+'</option>');
                         }
                }
            });
          
            $.ajax({
                url: "<?php
            echo base_url();
            ?>" + "index.php?/controller_call/fet_details",
              type: 'POST',
                data:{'company_id':company_id},
              async:false,
              success: function(data) { 
                                var data=JSON.parse(data);
                console.log(data);
                $('#call_tag').append('<option selected disabled>Call Category</option>');
                     for(i=0;i<data.length;i++)
                     {
                         $('#call_tag').append('<option value='+data[i]['id']+'>'+data[i]['call']+'</option>');
                     }
                                }
                    });   
            
         });
         
                $('#contract_list').change(function() {
          var dop=$("#dop").val();
           var contract_type=$("#contract_list").val();
            var company_id="<?php echo $company_id;?>";
            var region="<?php echo $region;?>";    
          var area="<?php echo $area;?>";    
          $.ajax({
              url: "<?php
            echo site_url('controller_call/expiry_date');
            ?>",
              method: "POST",
              data : { 'dop' : dop, 'cont' : contract_type },
                success : function(data){
                  $('#expiry_list').html('');
                  console.log(data);
                                                     //   alert(data);
                  //var result = JSON.parse(data);
                          $('#expiry_list').val(data);
                    },
                error: function( error )
                                        {
                        alert( error );
                      }
              });
            
         });
          
         $('#product_list').change(function(){
              
              var prod=$("#product_list").val();
              //alert(prod);
              var company_id="<?php
            echo $this->session->userdata('companyid');
            ?>";
              //alert(company_id);
              $.ajax({
              url: "<?php
            echo site_url('controller_call/choose_cat');
            ?>",
              method: "POST",
               data : { 'prod' : prod, 'company_id' : company_id },
                success : function(data){
                  $('#cat_list').html('');
                  console.log(data);
                  var result = JSON.parse(data);
                  $('#cat_list').append('<option value="nocat" selected>Select Sub-Category</option>');
                  for(i=0;i<result.length;i++)
                         {
                          $('#cat_list').append('<option value='+result[i]['cat_id']+'>'+result[i]['cat_name']+'</option>');
                         }
                  },
                error: function( error )
                                        {
                        alert("Network error");
                      }
              });
            
            
            });
                  $('#postcode').on('change',(function(e) {
                     /* if($(this).val().length==0){
                                     alert ("Pincode cannot be Empty!");
                               } */
         
                    if($(this).val().length>0 && $(this).val().length<6){
                                     alert ("Pincode should have 6 digits");
                               }
                      else 
                        return false;
               })); 
         
              /* $('#email').on('change',(function(e) {
             var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                 var emailinput = $('#email').val();
            if (email_reg.test(emailinput) == false) {
              swal("Please enter a valid email");
            }
            }));*/
          
               $('#anum').on('change',(function(e) {
             $("#rowdata").empty();
                      var inputVal = $(this).val();
              var numericReg = /^[0-9?=.*!@#$%^&*]+$/;
              if(!numericReg.test(inputVal)) {
                //alert("Numeric characters only");
                 $("#rowdata").html('Provide proper alternate number !');
                  $(".error").show();
                   $("#myModal1").animate({scrollTop:0});
              }
             else{
                 $("#rowdata").empty();
               window.scrollTo(40, 60);
             }
                   /*  if($(this).val().length>0 && $(this).val().length<20){
                                     alert ("Provide Proper Alternate Number");
                               }
                       else 
                        return false; */
               })); 
              
               $('#con_number').on('change',(function(e) {
               $("#rowdata_c").empty();
                      var inputVal = $(this).val();
              var numericReg =/^[0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]{7,13}$/;
              if(!numericReg.test(inputVal)) {
                //alert("Numeric characters only");
                 $("#rowdata_c").html('Provide proper Contact number !');
                  $(".error").show();
                   $("#myModal3").animate({scrollTop:0});
              }
         
                     /*if($(this).val().length>0 && $(this).val().length<10){
                                     alert ("Provide Proper Contact Number");
                      }
                     else if($(this).val().length>10){
                                       alert ("Provide Proper Contact Number");
                      }
                       else {
                             return false;
                          } */
               }));
              $('#alter_num').on('change',(function(e) {
         
                       $("#rowdata_c").empty();
                      var inputVal = $(this).val();
              var testReg = /^[0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]{7,13}$/;
              if(!testReg.test(inputVal)) {
                //alert("Numeric characters only");
                 $("#rowdata_c").html('Provide proper Alternate Contact number !');
                  $(".error").show();
                   $("#myModal3").animate({scrollTop:0});
              }
            else{
              $("#rowdata_c").empty();
            }
            /*if($(this).val().length>0 && $(this).val().length<10){
                                     alert ("Provide Proper Alternate Number");
                      }
                     else if($(this).val().length>10){
                                       alert ("Provide Proper Alternate Number");
                      }
                       else {
                             return false;
                          } */
               }));
         
         
               /* $('#anum').on('change',(function(e) {
         
                     if($(this).val().length>0 && $(this).val().length<10){
                                     alert ("Provide Proper Alternate Number");
                      }
                     else if($(this).val().length>10){
                                       alert ("Provide Proper Alternate Number");
                      }
                       else {
                             return false;
                          }
               })); */
         
         $('#clear_contract').click(function() {
             $('#contract_div').css({'display':'none'});
             $('#prod_table').css({'display':'none'});
             $('#dynamic_table').html('');
          //contract_div
          //prod_table
          //dynamic_table
         });
          
         $('#search').click(function() {
          //var value=$.trim($("#contact_num").val());
           if($("#contact_num").is(":blank"))
           {
            swal("Contact Field Empty!")
           }
           else 
           {
             var contact_number=$('#contact_num').val();  
              $('#submit_form_2')[0].reset();
            var company_id="<?php
            echo $company_id;
            ?>";
            $.ajax({
              url: "<?php
            echo base_url();
            ?>" + "index.php?/controller_call/fetch",
              type: 'POST',
              data:{'contact_number':contact_number},
              //contentType: "application/json",
                dataType:"json",        
              success: function(info) {
                //info = JSON.stringify(info);
                if(info=='Provide Proper Mobile Number'){
                  swal("Oops!", "Provide Proper Mobile Number!", "warning")
                }
                else if(info=='No details!') {
                  swal({
                    title:"No details Found!",
                      text: " Would you like to add?",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Yes, Add !",
                      cancelButtonText: "No, cancel !",
                      closeOnConfirm: false,
                      closeOnCancel: false
                  },
                  function(isConfirm){
                    if (isConfirm) {
                      $.ajax({
            url: "<?php
            echo base_url();
            ?>" + "index.php?/controller_call/customer",
            type: 'POST',
            data:{'company_id':company_id},
            success: function(data) {
                $("#custid").empty();
              //alert(data);
                console.log(data);
                $("#custid").val(data);
               // alert(contact_number);
                $('#con_number').val(contact_number);
            //  $("#myModal3").modal("show");
            }
          });
          
                      $('#myModal3').modal('show');
                      swal.close();
                    } else {
                      swal.close();
                      $('#contact_num').val('');
                    }
                  });
                }
                else {
                  //var info= JSON.stringify(info);
                  var cus=info[0].customer;                   
                  var con_length=info.length-1;           
                  var cust=info[con_length].contract;     
                  var data1= cus;
                    for(k=0;k<data1.length;k++) {
                      $('#cust_id').val(data1[k]['customer_id']);
                      $('#cname').val(data1[k]['customer_name']);
                      $('#e_mail').val(data1[k]['email_id']);
                      $('#altnum').val(data1[k]['alternate_number']);
                        $('#door').val(data1[k]['door_num']);
                        $('#street').val(data1[k]['address']);
                        $('#town').val(data1[k]['cust_town']);
                        $('#land_mark').val(data1[k]['landmark']);
                        $('#city').val(data1[k]['city']);  
                        $('#state').val(data1[k]['state']);
                        $('#country').val(data1[k]['cust_country']);
                        $('#pin').val(data1[k]['pincode']);
                    }
                    //var parsing=JSON.parse(cust);
                    var cont= cust;
                       //var cont= $.makeArray(cont1);    
                      // console.log(cont);
                       $('#contract_div').css({'display':'block'});
                       $('#prod_table').css({'display':'block'});
                       $('#dynamic_table').html('');
                       for(i=0;i<cont.length;i++)
                       {  
                      
                       var replaceSpace=cont[i].address;
                        var address = replaceSpace.replace(/ /g, ":");  
                       var replaceSpace=cont[i].cust_town;
                        var addr = replaceSpace.replace(/ /g, ":"); 
                                                                         var land=cont[i].land_mark;
                        var location= land.replace(/ /g, ":");  
                                                                         var city=cont[i].city;
                        var city= city.replace(/ /g, ":");
                                                                         var state=cont[i].state;
                        var state= state.replace(/ /g, ":");  
                         
                                                                         var product_name=cont[i].product_name;
                        var product_name= product_name.replace(/ /g, ":");  
                         
                                                                         var cat_name=cont[i].cat_name;
                        var cat_name= cat_name.replace(/ /g, ":");  
                         
                                         var replaceSpace=cont[i].type_of_contract;
                        var contract = replaceSpace.replace(/ /g, ":");  var replaceSpace=cont[i].type_of_contract;
                        var contract = replaceSpace.replace(/ /g, ":");
                         var door_num=cont[i].door_num;
                        var door_num = door_num.replace(/ /g, ":"); 
                         var customer_name=cont[i].customer_name;
                        var customer_name = customer_name.replace(/ /g, ":"); 
                     	if(cont[i].model_no=="")
                                 {
                                    var model="-";
                                 }
                         if(cont[i].serial_no=="")
                                 {        
                                  var serial_no="-";
                                 }
                      if(cont[i].model_no!="")
                                                                     {
                                                                             var model=cont[i].model_no;
                        var model= model.replace(/ /g, ":");
                                                                     }
                                                                        if(cont[i].serial_no!="")
                                                                     {         var serial_no=cont[i].serial_no;
                        var serial_no= serial_no.replace(/ /g, ":");
                                                                     }
                      //var d = new Date();
                      var d = new Date();
         
                                                                        var month = d.getMonth()+1;
                                                                        var day = d.getDate();
         
                                                                    var output = d.getFullYear() + '-' +(month<10 ? '0' : '') + month + '-' +
                                                                               (day<10 ? '0' : '') + day;
                      //var str = $.datepicker.formatDate('yy-mm-dd', d);
         //alert(d);
               // alert(month); 
                //alert(day);
                //alert(output);  
               // alert(cont[i]['end_date']); 
              var expirydate= new Date(cont[i]['warrenty_expairy_date']); //dd-mm-YYYY
            var today = new Date();
          // myDate > today
          console.log(expirydate);
           console.log(expirydate>=today);
                      if(expirydate>=today)
                      {
                        $('#dynamic_table').append("<tr><td><input class='form-control' type='text' name='prod_"+i+"' required id='prod_"+i+"' value='"+cont[i]['product_name']+"' readonly></td><td><input class='form-control' type='text' name='cat_"+i+"' required id='cat_"+i+"' value='"+cont[i]['cat_name']+"' readonly></td><td><input class='form-control' type='text' name='amctype_"+i+"' required id='amctype_"+i+"' value='"+cont[i]['type_of_contract']+"' readonly></td><td><input class='form-control' type='text' name='date_"+i+"' id='date_"+i+"' value='"+cont[i]['end_date']+"' readonly></td><td><button type='button' class='btn btn-circle blue btn-outline tooltip-test' title='Click to Raise Ticket' onclick=renew_contracts('"+cont[i]['custid']+"','"+customer_name+"','"+cont[i]['contact']+"','"+cont[i]['email_id']+"','"+cont[i]['anumber']+"','"+door_num+"','"+address+"','"+addr+"','"+location+"','"+city+"','"+state+"','"+cont[i]['cust_country']+"','"+cont[i]['pincode']+"','"+cont[i]['product_id']+"','"+cont[i]['cat_id']+"','"+contract+"','"+product_name+"','"+cat_name+"','"+model+"','"+serial_no+"')>Raise Ticket</button></td></tr>");
                      }
                      else
                      {
                      //  var contract = 'On-Demand'; 
                           
          $('#dynamic_table').append("<tr><td><input class='form-control' type='text' name='prod_"+i+"' required id='prod_"+i+"' value='"+cont[i]['product_name']+"' readonly></td><td><input class='form-control' type='text' name='cat_"+i+"' required id='cat_"+i+"' value='"+cont[i]['cat_name']+"' readonly></td><td><input class='form-control' type='text' name='amctype_"+i+"' required id='amctype_"+i+"' value='"+cont[i]['type_of_contract']+"'readonly></td><td><input class='form-control' type='text' name='date_"+i+"' id='date_"+i+"' value='Contract Expired' readonly style='color:red !important;'></td><td><button type='button' class='btn btn-circle blue btn-outline' onclick=renew_contracts('"+cont[i]['custid']+"','"+customer_name+"','"+cont[i]['contact']+"','"+cont[i]['email_id']+"','"+cont[i]['anumber']+"','"+door_num+"','"+address+"','"+addr+"','"+location+"','"+city+"','"+state+"','"+cont[i]['cust_country']+"','"+cont[i]['pincode']+"','"+cont[i]['product_id']+"','"+cont[i]['cat_id']+"','"+contract+"','"+product_name+"','"+cat_name+"','"+model+"','"+serial_no+"')>Raise Ticket</button></td></tr>");
                      }
                        
                    }             
                }
              }
            });
          }
         });
         
         
      </script>        
      <script>    
         function renew_contracts(custid,name,contact,email_id,anumber,door_num,address,addr,location1,city,state,country,pincode,product_id,cat_id,contract,product_name,cat_name,model,serial)
          {
            var company_id="<?php
            echo $this->session->userdata('companyid');
            ?>";
            $.ajax({
              url: "<?php
            echo base_url();
            ?>" + "index.php?/controller_call/ticket",
              type: 'POST',
              data:{'company_id':company_id},
              success: function(data) {
                //alert("data");
                  console.log(data);
                  $("#tick_id").val(data);
              }
            });
              
            //$('#myModal1').modal('show');
            
            var replaceSpace=address;
            var address = replaceSpace.replace(/\:/g," ");  
            var door_num = door_num.replace(/\:/g," ");   
            var name = name.replace(/\:/g," "); 
            var location=addr;
            var location= location.replace(/\:/g," ");
                         var loc=location1;
            var loc= loc.replace(/\:/g," ");
                         var ccity=city;
            var ccity= ccity.replace(/\:/g," ");  
                         var state1=state;
            var state1= state1.replace(/\:/g," ");  
            var product_name=product_name;
            var product_name= product_name.replace(/\:/g," ");  
            var cat_name=cat_name;
            var cat_name= cat_name.replace(/\:/g," ");  
            var contract_type =contract;
            var contract_type= contract_type.replace(/\:/g," ");  
            var model_no =model;
            var model_no= model_no.replace(/\:/g," ");  
            
            var s_no =serial;
            var s_no= s_no.replace(/\:/g," ");  
            
            $('#cu_id').val(custid);
            $('#name').val(name);
            $('#number').val(contact);
            $('#email').val(email_id);
            $('#anum').val(anumber);
            $('#doornum').val(door_num);
            $('#address').val(address);
            $('#addr').val(ccity);
            $('#cus_town').val(location);
            $('#land_mrk').val(loc);
            //$('#cstate').val(state1);
            //$('#ccountry').val(country);
            $('#ccountry').append('<option value="'+country+'" selected >'+country+'</option>');
            $('#cstate').append('<option value="'+state1+'" selected >'+state1+'</option>');
            $('#postcode').val(pincode);
            $('#cust_prod').val(product_id);
            $('#cust_prodname').val(product_name);
            $('#cust_cat').val(cat_id);
            $('#cust_catname').val(cat_name);
            $('#contract_type').val(contract_type);
            $('#model_no').val(model_no);
            $('#s_no').val(s_no);
            
            $('#myModal1').modal('show'); 
              
            
          }
          
          
          $('#raise_buttons').click(function() {
              $('#rowdata').empty();
                var tick_id=  $('#tick_id').val();
              var companyid=  $('#companyid').val();
                var cust_id=  $('#cu_id').val();
                var cust_name=  $('#name').val();
                var mail=  $('#email').val();
                var contact=  $('#number').val();
                var alt_contact=  $('#anum').val();
                var doornum=  $('#doornum').val();
                var address=  $('#address').val();
                 var cus_town=  $('#cus_town').val();
                 var land_mrk=  $('#land_mrk').val();
                var addr=  $('#addr').val();
                var cstate=  $('#cstate').val();
                var ccountry=  $('#ccountry').val();
                var postcode=  $('#postcode').val();
                var cust_prod=  $('#cust_prod').val();
                var cust_cat=  $('#cust_cat').val();
                var contract_type=  $('#contract_type').val();
                var model_no=  $('#model_no').val();
                var s_no=  $('#s_no').val();
                var preferred_date=  $('#preferred_date').val();
                var call_tag=  $('#call_tag').val();
                 var prob_desc = $('#prob_desc').val();
              var companyid="<?php echo $this->session->userdata('companyid'); ?>";
              var work_type=$("#work_type").val();
              var ticket_image = $('#upload_image').prop('files')[0];
              if(ticket_image){
                  var ext = $('#upload_image').val().toString().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['jpeg', 'png', 'jpg']) == -1) {
                  $("#myModal1").modal('hide');
                  swal({
                    title:"Upload an Image File",
                    //text: "No file Chosen!",
                    type:"warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok.",
                    cancelButtonText: "Cancel !",
                    closeOnConfirm: false,
                    closeOnCancel: false
                 },
                function(isConfirm){
                  if (isConfirm) {
                      swal.close();
                    $("#myModal1").modal('show');
                    
                  } else {
                  swal.close();
                  }
                 });  
                }
               else {
             var region="<?php echo $region;?>";
             var area="<?php echo $area;?>";
                var form_data = new FormData();
                form_data.append('ticket_image', ticket_image);
                form_data.append('tick_id', tick_id);
                form_data.append('cust_id', cust_id);
                form_data.append('companyid', companyid);
                form_data.append('cust_name', cust_name);
                form_data.append('mail', mail);
                form_data.append('contact', contact);
                form_data.append('alt_contact', alt_contact);
                form_data.append('doornum', doornum);
                form_data.append('address', address);
                form_data.append('cus_town', cus_town);
                form_data.append('land_mrk', land_mrk);
                form_data.append('addr', addr);
                form_data.append('cstate', cstate);
                form_data.append('ccountry', ccountry);
                form_data.append('postcode', postcode);
                form_data.append('cust_prod', cust_prod);
                form_data.append('cust_cat', cust_cat);
                form_data.append('contract_type', contract_type);
                form_data.append('model_no', model_no);
                form_data.append('s_no', s_no);
                form_data.append('preferred_date', preferred_date);
                form_data.append('call_tag', call_tag);
                form_data.append('prob_desc', prob_desc);
                form_data.append('region', region);
                form_data.append('area', area);
                form_data.append('work_type',work_type);
                $('#Searching_Modal').modal('show');
                $.ajax({
                  url         :   "<?php
            echo base_url();
            ?>index.php?/controller_call/raise_newticket",
                  type        :   "POST",
                  data        :   form_data,// {action:'$funky'}
                  //datatype  :   "JSON", 
                  contentType :false,
                  processData : false,
                  cache   :false,  
                  success     :   function(data){
         $.ajax({
                  url         :   "<?php
            echo base_url();
            ?>index.php?/AutoNewAssign/autoassign",
                  type        :   "POST",
                  data        : {"company_id":companyid},
                  //datatype  :   "JSON", 
                  contentType :false,
                  processData : false,
                  cache   :false,  
                  success     :   function(data){
         }
         });
                           $('#Searching_Modal').modal('hide');
                             
                            if(data="Ticket has been Raised.")
                            {
                            $('#myModal1').modal('hide');
                            swal({
                                title: "Good job!",
                                text: data,
                                type: "success",
                                confirmButtonClass: "btn-primary",
                                confirmButtonText: "Ok.",
                                closeOnConfirm: false,
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                location.reload();
                              }
                              }); 
                            }
                            else{
                              $('#myModal1').modal('hide');
                              swal(data);
                              $('#myModal1').modal('show');
                            }
                        }
                   });
                
                 }
              }
              else{
            var region="<?php echo $region;?>";
             var area="<?php echo $area;?>";
             $('#Searching_Modal').modal('show');  
                $.ajax({
                  url   : "<?php
            echo site_url('controller_call/ticket_withoutimg');
            ?>",
                  method  :'POST',
                  data  :  $("#submit_form").serialize() + '&company_id=' + companyid + '&region=' + region + '&area=' + area,
                  success :   function(data){
         $.ajax({
                  url         :   "<?php
            echo base_url();
            ?>index.php?/AutoNewAssign/autoassign",
                  type        :   "POST",
                  data        : {"company_id":companyid},
                  //datatype  :   "JSON", 
                  contentType :false,
                  processData : false,
                  cache   :false,  
                  success     :   function(data){
         }
         });             $('#Searching_Modal').modal('hide');   
                        if(data="Ticket has been Raised.")
                        {
                         
                        $('#myModal1').modal('hide');
                          swal({
                              title: "Good job!",
                              text: data,
                              type: "success",
                              confirmButtonClass: "btn-primary",
                              confirmButtonText: "Ok.",
                              closeOnConfirm: false,
                          },
                          function(isConfirm) {
                              if (isConfirm) {
                              location.reload();
                              }
                                }); 
                        }
                        else {
                            $('#myModal1').modal('hide');
                            swal(data);
                            $('#myModal1').modal('show');
                        }
                      }
                });
              }
               });
          
          var counter=0;
          function add_prod(){  
            counter += 1;
            $("#tab8").append("<hr class='hr_"+ counter +"'><div class='row form-group fields_"+ counter + "' name='contract_"+ counter +"'><a class='pull-right' id='cancel_"+ counter +"' onclick='cancel_prod(id)'><i class='fa fa-times'></i></a><div class='row'><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Product-Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select class='form-control col-md-7 product_list' id='fields_"+ counter + "' name='fields["+ counter +"][]'></select></div></div> <div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Sub-Category<span class='required' aria-required='true'> * </span></label> <div class='col-md-7'><select class='form-control' id='cat_list' name=fields["+ counter +"][]><option selected value='nocat'>Select Sub-Category</option></select></div></div> </div> <div class='row'><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Date of purchase </label><div class='col-md-7'><input class='form-control form-control-inline input-mediumda date-picker' ata-date-format='yyyy-mm-dd' size='10' type='text' id='epd_date_"+counter+"' name='fields["+ counter +"][]'></div></div><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Contract Type<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select class='form-control contract_list' id='contract_"+ counter + "' name='fields["+ counter +"][]'><option selected value='contract'> Select Contract Type</option></select></div></div> </div> <div class='row' style='display:none !important'><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Expiry Date</label><div class='col-md-7'> <input type='text' class='form-control' id='expiry_list' name='fields["+ counter +"][]'/> </div> </div></div><div class='row'><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Model No.</label><div class='col-md-7'><input type='text' class='form-control' id='modelno' name='fields["+ counter +"][]'></div></div><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Serial No.</label><div class='col-md-7'><input type='text' class='form-control' id='se_no' name='fields["+ counter +"][]'></div></div></div></div>");        
            //$('.product_list').html(' ');
             $("#epd_date_"+counter).appendDtpicker({
                "inline": false,
                "dateFormat": "YYYY-MM-DD",
                                         "dateOnly": true,
                                         "pastOnly": true,
                                          "closeOnSelected": true
              });
            
            var company_id="<?php
            echo $this->session->userdata('companyid');
            ?>";
            $.ajax({
              url: "<?php
            echo base_url();
            ?>" + "index.php?/controller_call/modal_product",
              type: 'POST',
              data:{'company_id':company_id},
              success: function(result) {
                    console.log(result);
                    var result= JSON.parse(result);
                    $('#fields_'+counter).append('<option selected value="dummydata">Product Category</option>');
                       for(i=0;i<result.length;i++)
                       {
                        $('#fields_'+counter).append('<option value='+result[i]['product_id']+'>'+result[i]['product_name']+'</option>');
                       }
              }
            });
         
                    $(".contract_list").change(function(){
                var id1=$(this).attr('id');
              var id2=id1.split("_");         
              var id3=$('.fields_'+id2[1]).find( "#epd_date_"+id2[1] ).val();
              
              console.log(id3);
              
              var contrt=$(this).val(); 
                 //var findz= $("input[name='"+id1+"']");
                 //var fin=findz.find("#epd_date_"+counter).val();
                 //console.log(findz);
                 //console.log(fin);
              var company_id="<?php
            echo $this->session->userdata('companyid');
            ?>";
                //var finds=$('.'+id1).find('#expiry_list');
                 
              var finds=$('.fields_'+id2[1]).find( "#expiry_list");
              $.ajax({
                url: "<?php
            echo site_url('controller_call/expiry_date');
            ?>",
                method: "POST",
                data : { 'dop' : id3, 'cont' : contrt },
                  success : function(data){
                    
                    finds.html('');
                    console.log(data);
                    //alert(data);
                    //var result = JSON.parse(data);
                    finds.val(data);
                      },
                  error: function( error )
                                         {
                          alert( "Network error");
                        }
                });
            
            });
            
            $.ajax({
              url: "<?php
            echo base_url();
            ?>" + "index.php?/controller_call/load_amctype",
              type: 'POST',
              data:{'company_id':company_id},
              success: function(data) {
              var result=JSON.parse(data);
                $('#contract_'+counter).empty();
                  console.log(result);        
                  $('#contract_'+counter).append('<option selected value="contract">Select Contract Type</option>');
                       for(i=0;i<result.length;i++)
                       {
                      $('#contract_'+counter).append('<option value="'+result[i]['amc_type']+'">'+result[i]['amc_type']+'</option>');
                       }
              }
            });
             
          }
            
            
            $(".product_list").change(function(){
              var id=$(this).attr('id');
              var prod=$("#"+id).val();     
              var company_id="<?php
            echo $this->session->userdata('companyid');
            ?>";
              var finds=$('.'+id).find('#cat_list');
              finds.html('');
              $.ajax({
                url: "<?php
            echo site_url('controller_call/choose_cat');
            ?>",
                method: "POST",
                data : { 'prod' : prod, 'company_id' : company_id },
                success : function(data){           
                    //console.log(data);
                    var result = JSON.parse(data);
                    finds.append('<option selected value="nocat">Select Sub-Category</option>');
                    for(i=0;i<result.length;i++)
                           {
                            finds.append('<option value='+result[i]['cat_id']+'>'+result[i]['cat_name']+'</option>');
                           }
                      },
                  error: function( error )
                                         {
                          alert( error );
                        }
                });
            }); 
            
            //$("#epd_date_"+counter).datepicker();
            
            function cancel_prod(id){
                   //$('.'+id).remove();
                   var id=id.split("_");
                   //console.log(id[1]);
                   $('.fields_'+id[1]).remove();
                   $('.hr_'+id[1]).remove();
              }
          $('#customer_add').click(function() {
            var company_id="<?php
            echo $this->session->userdata('companyid');
            ?>";
            $.ajax({
              url   : "<?php
            echo site_url('controller_call/add_customerdetails');
            ?>",
              method  :'POST',
              data  :  $("#submit_form_2").serialize() + '&company_id=' + company_id,
              dataType : 'JSON',
              cache : false,
              success :   function(data){ 
                      //var data=JSON.parse(data);
                      $("#rowdata_c").empty();              
                      if(data=="Product, Sub-cateogry, Contract information are Mandatory."){
                        $("#rowdata_c").html(data);
                          $(".error").show();
                           $("#myModal3").animate({scrollTop:0});
                      }
                      else if(data=="Select date of purchase for product!"){
                        $("#rowdata_c").empty();
                        $("#rowdata_c").html(data);
                        $(".error").show();
                        $("#myModal3").animate({scrollTop:0});          
                      return false;
                      }
                      else if(data=="Email Id already exists for different user!"){
                                            $("#rowdata_c").empty();
                                            $("#rowdata_c").html(data);
                                            $(".error").show();
                                            $("#myModal3").animate({scrollTop:0});  
                                        }
                                        else if(data=="Contact Number already exists for different user!!"){
                                            $("#rowdata_c").empty();
                                            $("#rowdata_c").html(data);
                                            $(".error").show();
                                            $("#myModal3").animate({scrollTop:0});  
                                        }
                                        else if(data == "Model number or seriel number empty"){
                                          $("#rowdata_c").empty();
                                            $("#rowdata_c").html(data);
                                            $(".error").show();
                                            $("#myModal3").animate({scrollTop:0}); 
                                            return false;

                                        }
                      else{
                        $("#rowdata_c").empty();
                          $('#sla_detail_confirm').empty();
                          //console.log(data.length);
                          $('#myModal3').modal('hide');
                          $('#add_confirm').modal('show');
                          console.log(data.length);
                            for(i=0;i<data.length;i++){
                                 $('#sla_detail_confirm').append(data[i]+'<br><hr>');
                            }
                        }
                },
            });
          });
          
      </script>
      <script>
         /*var ajaxResult=[];
         $.ajax({
         url: "<?php
            echo base_url();
            ?>" + "index.php?/controller_call/fet_details",
         type: 'POST',
         async:false,
         success: function(data) { 
         data=JSON.parse(data);
         ajaxResult=[];
         ajaxResult=data;
         
         },
         });
         var res=ajaxResult; 
         //console.log(res);
         $('#call_tag').autocomplete({
          source: res
                 });*/
      </script>
   </body>
</html>
