<?php
//cretaed by: Aarthi - Mar 10th 2020 for KRA KPI report except ctpl calls
defined('BASEPATH') OR exit('No direct script access allowed');

class Kra_kpi_reports extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index()
	{
		if ($this->session->userdata('user_logged_in'))
		{
				
		$this->data['webPageheading'] = ' Engineers KRA KPI Report';
		$company_id = $this->session->userdata('companyid');
		$array = array('company_id'=>$company_id);
		// get technician list procedure
		$this->data['technician_all'] = $this->common->get_method($array,array('methodname'=>'GET_TECHNICIAN_ALL','sp_next_result'=>1));		
		$this->load->view('reports/kra_kpi_report',$this->data);
		}
		else
		{
			redirect(base_url());
		}
	}
	
	// report list
	public function kra_kpi_list_all()
	{
		if ($this->session->userdata('user_logged_in'))
		{
			$company_id = $this->session->userdata('companyid');
			$username= $this->session->userdata('session_username'); // manager profile username
			$startdate=$this->input->post('startdate');  // filter start date
			$sdate = str_replace('/', '-', $startdate); 
			$startdate = date('Y-m-d', strtotime($sdate));
			$enddate=$this->input->post('enddate');	// filter end date
			$edate = str_replace('/', '-', $enddate);
			$enddate = date('Y-m-d', strtotime($edate));
			$technician=$this->input->post('technician'); // technician list

			
			$draw = intval($this->input->get("draw"));
			$start = intval($this->input->get("start"));
			$length = intval($this->input->get("length"));


			$array = array(  // onload array
                'companyid'=>$company_id,
                'from_date'=>$startdate,
                'to_date'=>$enddate,
                'technician'=>$technician,
			);
			
			$kra_kpi_report = $this->common->kra_kpi_report_filter($array); // list query

			$data = array();
			$i=0;
			

			foreach($kra_kpi_report->result() as $row) {
				
				$technician_id = $row->technician_id;	

				$calc_data = array(   // array for actual and avarage calculations
					'companyid'=>$company_id,
					'from_date'=>$startdate,
					'to_date'=>$enddate,
					'technician_id'=>$technician_id
				);

				$end_to_end_difference = $this->common->end_to_end_difference($calc_data);	// End to end difference
				$avg_calls = $this->common->average_calls($calc_data); // Average calls per day
				$first_time_fix = $this->common->first_time_fix_calc($calc_data); // first time fix 
				$pm_calls_target_calc = $this->common->pm_calls_target_calc($calc_data,$username); // pm calls target
				$parts_per_serial_calc = $this->common->parts_per_serial_calc($calc_data); // parts per serial number
				$calls_per_serial_calc = $this->common->calls_per_serial_calc($calc_data); // calls per serial number
				$pm_call_target = $this->common->pm_call_target($company_id,$username); // target value in manager
				$mta_calc = $this->common->mta_calc($calc_data); // mta calc
				$mtr_calc = $this->common->mtr_calc($calc_data); // mtr calc
				$mtc_calc = $this->common->mtc_calc($calc_data); // mtc calc
				$kra_customer_feedback = $this->common->kra_customer_feedback($calc_data); // customer feedback
				
			
				$i=$i+1;
				$data[] = array(

					$i,
					'<b>'.$row->first_name.'</b> ( '.$row->employee_id.')', // technician first name alonmg with employee id

					'90%',  //E2E target value
					$end_to_end_difference[0], // E2e Actual value
					$end_to_end_difference[1],  // E2e Avarage value

					'4',   //Avg Calls/day target value
					$avg_calls[0],  // Avg calls/day Actual value
					$avg_calls[1],  // Avg Calls per day Average value

					'95%', // first time fix target value
					$first_time_fix[0], // first time fix actual value
					$first_time_fix[1],  // first time fix average value

					$pm_call_target, // PM Calls target value
					$pm_calls_target_calc[0],  // pm calls actual value
					$pm_calls_target_calc[1], // pm calls average value

					'<1.12', //parts per serial no target value
					$parts_per_serial_calc[0], // parts per serial no actual value
					$parts_per_serial_calc[1], // parts per serial no average value

					'1', // calls per serial no target value
					$calls_per_serial_calc[0], // calls per serial no actual value
					$calls_per_serial_calc[1], // calls per serial no average value

					'<= 4 Hrs', // mta target value
					$mta_calc[0], // mta actual value
					$mta_calc[1], // mta average value

					'<= 3 Hrs', // mtr target value
					$mtr_calc[0], // mtr actual value
					$mtr_calc[1], // mtr average value

					'<= 24 Hrs', // mtc target value
					$mtc_calc[0], // mtc actual value
					$mtc_calc[1], // mtc average value

					'90%', // feedback target value
					$kra_customer_feedback[0], // feedback actual value
					$kra_customer_feedback[1] // feedback average value


				);
			}
				$output = array(
					"draw" => $draw,
					"recordsTotal" => $kra_kpi_report->num_rows(),
					"recordsFiltered" => $kra_kpi_report->num_rows(),
					"data" => $data
				);

			echo json_encode($output);
		}
		else
		{
		redirect(base_url());
		}
	}

			
	
}
