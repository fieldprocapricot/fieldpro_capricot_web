<?php
//cretaed by: Aarthi - Nov 11th 2020 for ctpl reports in manager
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctpl_reports extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}
	
	public function index()
	{
		if ($this->session->userdata('user_logged_in')) //if user logged in
			{
				 
			$this->data['webPageheading'] = ' CTPL Report'; //page heading
			$company_id = $this->session->userdata('companyid'); //company id
		    $array = array('company_id'=>$company_id); //array to pass in the procedure
	 		$this->data['service_group'] = $this->common->get_method($array,array('methodname'=>'GET_SERVICE_GROUP_ALL','sp_next_result'=>1));	// service group procedure	
		 	// $array = array('company_id'=>$company_id);
	 		$this->data['product_all'] = $this->common->get_method($array,array('methodname'=>'GET_PRODUCT_ALL','sp_next_result'=>1));	 // product procedure	
			$this->load->view('reports/ctpl_report',$this->data); //ctpl reports view
			}
			else{
				redirect(base_url()); // app base url
			}
	}
    

	public function ctpl_report_all() 	//ctpl report data list
	{

		if ($this->session->userdata('user_logged_in')) //if user logged in
		{
			$company_id = $this->session->userdata('companyid'); //company id
			$startdate=$this->input->post('startdate'); //start date from request
			$sdate = str_replace('/', '-', $startdate); //string replace of start date
			$startdate = date('Y-m-d', strtotime($sdate)); //formated start date
			$enddate=$this->input->post('enddate');	 //end date from request
			$edate = str_replace('/', '-', $enddate); //string replace of end date
			$enddate = date('Y-m-d', strtotime($edate)); // formated end date
			$product=$this->input->post('product');	//product id from request
			$call_status=$this->input->post('call_status'); //call status from request
			 
			$draw = intval($this->input->get("draw"));
			$start = intval($this->input->get("start"));
			$length = intval($this->input->get("length"));

			$array = array(  //array for model call
				'companyid'=>$company_id,
				'from_date'=>$startdate,
				'to_date'=>$enddate,
				'product'=>$product,
				'call_status'=>$call_status,
			);

		 	$ctpl_report = $this->common->ctpl_report_filter($array); // fetch from model for ctpl datas
		  	$data = array();
			$i=0;

          	foreach($ctpl_report->result() as $row) {
			
				if($row->current_status=='0') //if current status '0' , then not assigned
				{
					$c_status= 'Not Assign';
				}
				else //if not, then status name from status table
				{
					$array = array('current_status'=>$row->current_status);
					$curr_status = $this->common->ticket_status($array); //fetch from model for ticket status
					$curstatus = $curr_status->row();
					$c_status= $curstatus->status_name; //status name
					$c_status = str_replace('_', ' ', $c_status); //replacing '_' by empty space
				}

				/// Date of completion wrt status
				if($row->current_status=='8' || $row->current_status=='12'){ //if completed or closed, then date should be available

					$completed_date = date('d/m/Y', strtotime($row->last_update));
				}
				else{ //else date is empty
					$completed_date = "-";
				}
	
				$i=$i+1;

				if($row->current_status=='8' || $row->current_status=='12') // TAT for completed and closed calls
				{
					$dteEnd   = new DateTime($row->raised_time); //ticket raised time
					$dteStart = new DateTime($row->ticket_end_time); // ticket end time
					$dteDiff  = $dteStart->diff($dteEnd);
					$completed_hrs = $dteDiff->format("%h"); // hrs difference
					$completed_mins = $dteDiff->format("%i"); // minutes diffrenece
					$total_tat = $completed_hrs.'hrs'.' '.$completed_mins.'mins'; // total time 
				}
				else if($row->current_status !='8' || $row->current_status !='12') // not completed and not closed
				{
					$dteEnd   = new DateTime($row->raised_time); //ticket raised time
					$dteStart = new DateTime(date('Y-m-d H:i:s')); // ticket accpetance time
					$dteDiff  = $dteStart->diff($dteEnd);
					$completed_hrs = $dteDiff->format("%h"); // hrs difference
					$completed_mins = $dteDiff->format("%i"); // minutes diffrenece
					$total_tat = $completed_hrs.'hrs'.' '.$completed_mins.'mins'; // total time 
				}

					if($row->invoice_date!='' || $row->invoice_date!=NULL)
					{
						$invoice_date=date('d/m/Y', strtotime($row->invoice_date));
					}
					else
					{
						$invoice_date='';
					}
					
					if($row->delivery_date!='' || $row->delivery_date!=NULL)
					{
						$delivery_date=date('d/m/Y', strtotime($row->delivery_date));
					}
					else
					{
						$delivery_date='';
					} 

					$activite_id = $row->activite_id; // activity id in fp activity table
					$service_id = $row->service_id;
					$tck_id = $row->ticket_id;
					if($service_id != null && $service_id == 2) // service id status in fp activity table
					{
						$activite_id  = 37; // primary id in fp categories table based on service group
					}
					else if($service_id != null && $service_id == 3)
					{
						$activite_id  = 38;
					}
					else if($service_id != null && $service_id == 4)
					{
						$activite_id  = 39;
					}
					else if($service_id != null && $service_id == 5)
					{
						$activite_id  = 40;
					}
					else if($service_id != null && $service_id == 6)
					{
						$activite_id  = 41;
					}
					else
					{
						$activite_id = '-';
					}

					$ap_value = $this->common->service_checklist_status($activite_id,$service_id,$tck_id); // to get ap value
					$apValue = $ap_value->row();
					$service_checklist= $apValue->ap_value;

					if ($service_checklist != null && $service_checklist == 0)
					{
						$machine_status = 'No'; // machine down status
					}
					else if ($service_checklist != null && $service_checklist == 1)
					{
						$machine_status = 'Yes';
					}
					else
					{
						$machine_status = '-';
					}
					
					$data[] = array(
							$i, 
							$row->ticket_id,
							$row->customer_name,
						//   '<b>'.$row->ticket_id.'</b> - '.$row->customer_name,
							$row->product_name,
							$row->cat_name,
							$row->serial_no,
							$row->town,
							$invoice_date,
							$delivery_date,
							date('d/m/Y', strtotime($row->raised_time)),
							$completed_date,
							$row->service_group,
							$row->cust_rating,
							$row->cust_feedback,
							$row->visit_count,
							$machine_status, // Machine Completely Down
							$row->vertical,
							$total_tat,
							$c_status
					);

				}
				

					$output = array(
							"draw" => $draw,
							"recordsTotal" => $ctpl_report->num_rows(),
							"recordsFiltered" => $ctpl_report->num_rows(),
							"data" => $data
						);
				
			 
					echo json_encode($output);
				}
				else
				{
					redirect(base_url());
				}
     }
	
}