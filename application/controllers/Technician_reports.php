<?php
//cretaed by: Aarthi - Nov 28th 2019
defined('BASEPATH') OR exit('No direct script access allowed');

class Technician_reports extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index()
	{
			 if ($this->session->userdata('user_logged_in'))
			 {
				 
		$this->data['webPageheading'] = ' Technician Report';
		
				
		
	$company_id = $this->session->userdata('companyid');
		
		      $array = array('company_id'=>$company_id);
	 $this->data['service_group'] = $this->common->get_method($array,array('methodname'=>'GET_SERVICE_GROUP_ALL','sp_next_result'=>1));		
		 $array = array('company_id'=>$company_id);
	 $this->data['product_all'] = $this->common->get_method($array,array('methodname'=>'GET_PRODUCT_ALL','sp_next_result'=>1));	
	 $this->data['technician_all'] = $this->common->get_method($array,array('methodname'=>'GET_TECHNICIAN_ALL','sp_next_result'=>1));		
			$this->load->view('reports/technician_report',$this->data);
		}
		else{
	 redirect(base_url());
		}
    }
    
//Technician report data list
	
	public function technician_report_all()
     {

		 if ($this->session->userdata('user_logged_in'))
		 {
			$company_id = $this->session->userdata('companyid');
			$startdate=$this->input->post('startdate');
			$sdate = str_replace('/', '-', $startdate);
 			$startdate = date('Y-m-d', strtotime($sdate));
			$enddate=$this->input->post('enddate');	
			$edate = str_replace('/', '-', $enddate);
 			$enddate = date('Y-m-d', strtotime($edate));
			$technician=$this->input->post('technician');	
			 
			$draw = intval($this->input->get("draw"));
			$start = intval($this->input->get("start"));
			$length = intval($this->input->get("length"));
		
			$array = array(
					'companyid'=>$company_id,
					'from_date'=>$startdate,
					'to_date'=>$enddate,
					'technician'=>$technician,
				);
		 	$tech_report = $this->common->technician_report_filter($array);	

			$data = array();
			$i=0;

          	foreach($tech_report->result() as $row) {
			
				$array = array('current_status'=>$row->current_status);
				$curr_status = $this->common->ticket_status($array);
				$curstatus = $curr_status->row();
				$c_status= $curstatus->status_name;
				$c_status = str_replace('_', ' ', $c_status);
				$cu_status = ucfirst($c_status); // ticket status

				$i=$i+1;

					$dteEnd   = new DateTime($row->ticket_start_time); //ticket start time
					$dteStart = new DateTime($row->ticket_end_time); // ticket end time
					$dteDiff  = $dteStart->diff($dteEnd);
					$completed_hrs = $dteDiff->format("%h"); // hrs difference
					$completed_mins = $dteDiff->format("%i"); // minutes diffrenece
					$total_time = $completed_hrs.'hrs'.' '.$completed_mins.'mins'; // total time 
				

				if($row->current_status=='8' || $row->current_status=='12') // TAT for completed and closed calls
				{
					$dteEnd   = new DateTime($row->raised_time); //ticket raised time
					$dteStart = new DateTime($row->ticket_end_time); // ticket end time
					$dteDiff  = $dteStart->diff($dteEnd);
					$completed_hrs = $dteDiff->format("%h"); // hrs difference
					$completed_mins = $dteDiff->format("%i"); // minutes diffrenece
					$total_tat = $completed_hrs.'hrs'.' '.$completed_mins.'mins'; // total time 
				}
				else if($row->current_status !='8' || $row->current_status !='12') // not completed and not closed
				{
					$dteEnd   = new DateTime($row->raised_time); //ticket start time
					$dteStart = new DateTime($row->acceptance_time); // ticket accpetance time
					$dteDiff  = $dteStart->diff($dteEnd);
					$completed_hrs = $dteDiff->format("%h"); // hrs difference
					$completed_mins = $dteDiff->format("%i"); // minutes diffrenece
					$total_tat = $completed_hrs.'hrs'.' '.$completed_mins.'mins'; // total time 
				}

			  
               	$data[] = array(
					$i, 
					$row->ticket_id,
					$row->customer_name,
					$row->first_name,
					$row->service_group,
					$row->product_name,
					$row->cat_name,
					$row->serial_no,
					$row->raised_time,
					$row->assigned_time,
					$row->acceptance_time,
					$row->ticket_start_time, 
					$row->start_time,
					$row->end_time,
					$row->ticket_end_time,
					$row->cust_rating,
					$row->company_feedback,
					$row->prob_desc,
					$row->resolution_summary,
					$total_time,
					$total_tat,
					$cu_status
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $tech_report->num_rows(),
                 "recordsFiltered" => $tech_report->num_rows(),
                 "data" => $data
            );
		
			 
		  echo json_encode($output);
		 }
			 else{
			 redirect(base_url());
			 }
     }
			
	
}