<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Summary_reports extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index()
	{
			 if ($this->session->userdata('user_logged_in'))
			 {
				 
		$this->data['webPageheading'] = ' Summary Report';
		
				
		
	$company_id = $this->session->userdata('companyid');
		
		      $array = array('company_id'=>$company_id);
	 $this->data['service_group'] = $this->common->get_method($array,array('methodname'=>'GET_SERVICE_GROUP_ALL','sp_next_result'=>1));		
		 $array = array('company_id'=>$company_id);
	 $this->data['product_all'] = $this->common->get_method($array,array('methodname'=>'GET_PRODUCT_ALL','sp_next_result'=>1));		
	

			$this->load->view('reports/summary_report',$this->data);
		}
		else{
	 redirect(base_url());
		}
	}
	
	public function summary_report_all()
     {

		 if ($this->session->userdata('user_logged_in'))
		 {
			  $company_id = $this->session->userdata('companyid');
				$startdate=$this->input->post('startdate');
			 $sdate = str_replace('/', '-', $startdate);
 $startdate = date('Y-m-d', strtotime($sdate));
				$enddate=$this->input->post('enddate');	
			  $edate = str_replace('/', '-', $enddate);
 $enddate = date('Y-m-d', strtotime($edate));
			 	$product=$this->input->post('product');	
			 $service_group=$this->input->post('service_group');
			 $call_status=$this->input->post('call_status');
			 $visit_call=$this->input->post('visit_call');
			 
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));
	
		//	Array ( [where_cond] => A.current_status IN (8) [from_date] => 2019-02-01 [to_date] => 2019-03-15 )  
	
		  $array = array(
			'companyid'=>$company_id,
		  'from_date'=>$startdate,
		  'to_date'=>$enddate,
		  'product'=>$product,
		  'service_group'=>$service_group,
		  'call_status'=>$call_status,
		  'visit_call'=>$visit_call
		);

		 $summaryss = $this->common->summary_report_filter($array);	
		  $data = array();
			 $i=0;
		

          foreach($summaryss->result() as $row) {
			
			
			  if($row->current_status=='0')
			  {
				  $c_status= 'Not Assign';
			  }
			  else
			  {
				   	$array = array('current_status'=>$row->current_status);
					$curr_status = $this->common->ticket_status($array);
					$curstatus = $curr_status->row();
				  $c_status= $curstatus->status_name;
				   $c_status = str_replace('_', ' ', $c_status);
			  }


			  /// Date of completion wrt status

			  if($row->current_status=='8' || $row->current_status=='12'){
				$completed_date = date('d/m/Y', strtotime($row->last_update));

			  }
			  else{
				$completed_date = "-";

			  }

	
				$i=$i+1;


				if($row->current_status=='8' || $row->current_status=='12') // TAT for completed and closed calls
				{
					$dteEnd   = new DateTime($row->raised_time); //ticket raised time
					$dteStart = new DateTime($row->ticket_end_time); // ticket end time
					$dteDiff  = $dteStart->diff($dteEnd);
					$completed_hrs = $dteDiff->format("%h"); // hrs difference
					$completed_mins = $dteDiff->format("%i"); // minutes diffrenece
					$total_tat = $completed_hrs.'hrs'.' '.$completed_mins.'mins'; // total time 
				}
				else if($row->current_status !='8' || $row->current_status !='12') // not completed and not closed
				{
					$dteEnd   = new DateTime($row->raised_time); //ticket start time
					$dteStart = new DateTime($row->acceptance_time); // ticket accpetance time
					$dteDiff  = $dteStart->diff($dteEnd);
					$completed_hrs = $dteDiff->format("%h"); // hrs difference
					$completed_mins = $dteDiff->format("%i"); // minutes diffrenece
					$total_tat = $completed_hrs.'hrs'.' '.$completed_mins.'mins'; // total time 
				}


			if($row->invoice_date!='' || $row->invoice_date!=NULL)
			{
				$invoice_date=date('d/m/Y', strtotime($row->invoice_date));
			}
			  else
			  {
				  $invoice_date='';
			  }
			  
			 if($row->delivery_date!='' || $row->delivery_date!=NULL)
			{
				$delivery_date=date('d/m/Y', strtotime($row->delivery_date));
			}
			  else
			  {
				  $delivery_date='';
			  } 

			  $activite_id = $row->activite_id; // activity id in fp activity table
			  $service_id = $row->service_id;
			  $tck_id = $row->ticket_id;
			if($service_id != null && $service_id == 2) // service id status in fp activity table
			{
				$activite_id  = 37; // primary id in fp categories table based on service group
			}
			else if($service_id != null && $service_id == 3)
			{
				$activite_id  = 38;
			}
			else if($service_id != null && $service_id == 4)
			{
				$activite_id  = 39;
			}
			else if($service_id != null && $service_id == 5)
			{
				$activite_id  = 40;
			}
			else if($service_id != null && $service_id == 6)
			{
				$activite_id  = 41;
			}
			else
			{
				$activite_id = '-';
			}

			$ap_value = $this->common->service_checklist_status($activite_id,$service_id,$tck_id); // to get ap value
			$apValue = $ap_value->row();
			$service_checklist= $apValue->ap_value;

			if ($service_checklist != null && $service_checklist == 0)
			{
				$machine_status = 'No'; // machine down status
			}
			else if ($service_checklist != null && $service_checklist == 1)
			{
				$machine_status = 'Yes';
			}
			else
			{
				$machine_status = '-';
			}
			
			  $data[] = array(
					$i, 
					$row->ticket_id,
					$row->technician_name,//   '<b>'.$row->ticket_id.'</b> - '.$row->customer_name,
					$row->customer_name,
					$row->product_name,
					$row->cat_name,
					$row->serial_no,
					$row->town,
					$invoice_date,
					$delivery_date,
					date('d/m/Y', strtotime($row->raised_time)),
					$completed_date,
					$row->service_group,
					$row->cust_rating,
					$row->cust_feedback,
					$row->company_feedback,
					$row->visit_count,
					$machine_status, // Machine Completely Down
					$row->vertical,
					$total_tat,
					$c_status
					
			 );

			  
          }
		

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $summaryss->num_rows(),
                 "recordsFiltered" => $summaryss->num_rows(),
                 "data" => $data
            );
		
			 
		  echo json_encode($output);
		 }
			 else{
			 redirect(base_url());
			 }
     }
			
	
}