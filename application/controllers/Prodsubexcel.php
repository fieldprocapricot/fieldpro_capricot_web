<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodsubexcel extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->model('common');	
		$this->load->model('capricotexcelimportexport');	
	
		
	}

	public function index()
	{
			
	}
	
	public function prodsubexcelexport()
	{
		$this->load->model('capricotexcelimportexport');	
		
					
					$dateof = date('Y-m-d');
			
					$array = array('dateof' => $dateof);
					$this->capricotexcelimportexport->prodsubexcel_fp_sap($array);
		
	}
	
			
	
}