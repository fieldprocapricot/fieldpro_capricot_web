<?php
ini_set('max_execution_time', 0);
defined('BASEPATH') OR exit('No direct script access allowed');
class controller_cust extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->library('email');
        $this->load->library('session');
        //$this->load->library('ffmpeglib');
        $this->load->helper(array(
            'url',
            'cookie'
        ));
        $this->load->database();
        
    }
    public function index()
    {
        $this->load->helper('url');
        $this->load->database();
    }
    	
	public function load_company()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Customer');
        $result = $this->Customer->company();
      //  $comp=json_encode($result);
		
		$json   = array(
                "status" => 1,
                "result" => $result
            );
		  echo json_encode($json);
    }
	
	
    public function load_product()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        } else {
            $this->load->model('Customer');
            $result = $this->Customer->load_product($company_id);
            $json   = array(
                "status" => 1,
                "result" => $result
            );
        }
        
        echo json_encode($json);
    }
	public function load_customer_details()
	{
		$this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $cust_id = $this->input->post('cust_id');
        if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        } else {
            $this->load->model('Customer');
            $result = $this->Customer->load_customer_details($company_id,$cust_id);
			$res=array();
			foreach($result as $r)
			{
				array_push($res,array("product"=>$r['product_name'],"category"=>$r['cat_name'],"model"=>$r['model_no'],"serial"=>$r['serial_no'],"contract_type"=>$r['type_of_contract'],"expiry_date"=>$r['warrenty_expairy_date']));
			}
			$json=array("status"=>1,"result"=>$res);
			
		}
	echo json_encode($json);
	}
    public function load_cust_product()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $cust_id = $this->input->post('cust_id');
        if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        } else {
            $this->load->model('Customer');
            $result = $this->Customer->load_cust_product($company_id,$cust_id);
            $json   = array(
                "status" => 1,
                "result" => $result
            );
        }
        
        echo json_encode($json);
    }
    public function load_category()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $product_id = $this->input->post('product_id');
        if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        } else if ($product_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Product ID"
            );
        } else {
            $this->load->model('Customer');
            $result = $this->Customer->load_category($company_id, $product_id);
            $json   = array(
                "status" => 1,
                "result" => $result
            );
        }
        echo json_encode($json);
    }
    public function load_cust_category()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $cust_id = $this->input->post('cust_id');
        $product_id = $this->input->post('product_id');
        if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        } else if ($product_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Product ID"
            );
        } else if ($cust_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Customer ID"
            );
        } else {
            $this->load->model('Customer');
            $result = $this->Customer->load_cust_category($company_id,$cust_id,$product_id);
            $json   = array(
                "status" => 1,
                "result" => $result
            );
        }
        echo json_encode($json);
    }
    public function load_model()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $cust_id = $this->input->post('cust_id');
        $product_id = $this->input->post('product_id');
        $cat_id = $this->input->post('cat_id');
        if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        } else if ($product_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Product ID"
            );
        } else if ($cust_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Customer ID"
            );
        } else if ($cat_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Sub Category ID"
            );
        } else {
            $this->load->model('Customer');
            $result = $this->Customer->load_model($cust_id,$company_id,$product_id,$cat_id);
            $json   = array(
                "status" => 1,
                "result" => $result
            );
        }
        echo json_encode($json);
    }
    public function load_call_tags()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        } else {
            $this->load->model('Customer');
            $result = $this->Customer->load_call_tags($company_id);
            $json   = array(
                "status" => 1,
                "result" => $result
            );
        }
        echo json_encode($json);
    }
    public function load_service_category()
    {
        $this->load->helper('url');
        $this->load->database();
        //$company_id=$this->input->post('company_id');
        //if($company_id=="")
        //{
        $json = array(
            "status" => 0,
            "msg" => "Please provide Company ID"
        );
        //}
        //else{
        $this->load->model('Customer');
        $result = $this->Customer->load_service_category();
        $json   = array(
            "status" => 1,
            "result" => $result
        );
        //}
        echo json_encode($json);
    }
    public function load_cust_service_category()
    {
        $this->load->helper('url');
        $this->load->database();
        $cust_id=$this->input->post('cust_id');
        $company_id=$this->input->post('company_id');
        if($cust_id=="")
        {
        $json = array(
            "status" => 0,
            "msg" => "Please provide Customer ID"
        );
        }
		else if($company_id=="")
		{
		$json = array(
            "status" => 0,
            "msg" => "Please provide Company ID"
        );	
		}
        else{
        $this->load->model('Customer');
        $result = $this->Customer->load_cust_service_category($cust_id,$company_id);
        $json   = array(
            "status" => 1,
            "result" => $result
        );
        }
        echo json_encode($json);
    }
    public function register()
    {
        $json = array();
        $this->load->helper('url');
        $this->load->database();
		  $this->load->model('Customer');
		
        $name       = $this->input->post('name');
        $emailid    = trim($this->input->post('emailid'));
        $contact_no = $this->input->post('contact_no');
        $alt_no     = $this->input->post('alt_no');
        $door_no    = $this->input->post('door_no');
        $street     = $this->input->post('street');
        $town       = $this->input->post('town');
        $city       = $this->input->post('city');
        $landmark   = $this->input->post('landmark');
        $region     = $this->input->post('region');
        $state      = $this->input->post('state');
        $country    = $this->input->post('country');
        $pincode    = $this->input->post('pincode');
        $company_id = $this->input->post('company_id');
        $product_id = $this->input->post('product_id');
        $cat_id     = $this->input->post('cat_id');
        $model_no   = $this->input->post('model_no');
        $serial_no  = $this->input->post('serial_no');
		$start_date = $this->input->post('start_date');  /////// (format –(mm/dd/yyyy)- 09/24/2018)
		$type_of_contract  = $this->input->post('type_of_contract');
		$contract_duration  = $this->input->post('contract_duration');
		$start_date =date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime("+".$contract_duration." months", strtotime($start_date))); 
        $warrenty_expairy_date = $end_date;
		
		$get_cust_emailid = $this->Customer->email_check_customer($emailid);

        if ($name == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Name"
            );
        } else if ($emailid == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Email ID"
            );
        } 
		 else if ($get_cust_emailid == '1' || $get_cust_emailid == 1) 
		 {
            $json = array(
                "status" => 0,
                "msg" => "Email ID Already Registered"
            );
        } 
		
		else if ($contact_no == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Contact Number"
            );
        }
		/*else if ($alt_no == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Alternative number"
            );
        } */
		
		else if ($door_no == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Door number"
            );
        } else if ($street == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Street"
            );
        } else if ($town == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Town"
            );
        } else if ($city == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide city"
            );
        }
      /*  else if ($region == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Region"
            );
        }*/
		else if ($state == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide State"
            );
        } else if ($country == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Country"
            );
        } else if ($pincode == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide pincode"
            );
        } else if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        } else if ($product_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Product ID"
            );
        } else if ($cat_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Sub-Category ID"
            );
        } else if ($model_no == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Model Number"
            );
        } else if ($serial_no == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Serial Number"
            );
        } else {
            
          
            $check_cust = $this->Customer->check_cust($emailid, $contact_no, $product_id, $cat_id, $model_no, $serial_no, $company_id);
            if (empty($check_cust)) {
                $get_cust_id = $this->Customer->get_cust_id($company_id);
                
                $data     = array(
                    "customer_name" => $this->input->post('name'),
                    "customer_id" => $get_cust_id,
                    "email_id" => $this->input->post('emailid'),
                    "contact_number" => $this->input->post('contact_no'),
                    "alternate_number" => $this->input->post('alt_no'),
                    "door_num" => $this->input->post('door_no'),
                    "address" => $this->input->post('street'),
                    "cust_town" => $this->input->post('town'),
                    "landmark" => $this->input->post('landmark'),
                    "region"   => $this->input->post('region'),
                    "city" => $this->input->post('city'),
                    "state" => $this->input->post('state'),
                    "cust_country" => $this->input->post('country'),
                    "pincode" => $this->input->post('pincode'),
                    "company_id" => $this->input->post('company_id'),
                    "product_serial_no" => $this->input->post('product_id'),
                    "component_serial_no" => $this->input->post('cat_id'),
                    "model_no" => $this->input->post('model_no'),
                    "serial_no" => $this->input->post('serial_no'),
					"type_of_contract" => $type_of_contract,
                    "contract_value" =>'',
                    "contract_duration"=>$contract_duration,
                    "warrenty_expairy_date" =>  $warrenty_expairy_date,
                    "start_date" => $start_date,
                    "end_date"=> $end_date
                );
                $add_cust = $this->Customer->register($data);
                $this->sendmail($emailid, $company_id, 'Customer');
                if ($add_cust == 1) {
                    $json = array(
                        "status" => 1,
                        "msg" => "Registered Successfully, Login Credential sent to your given email id!"
                    );
                } else {
                    $json = array(
                        "status" => 0,
                        "msg" => "Error!!!"
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Customer detail Already exists!!!"
                );
            }
        }
        echo json_encode($json);
    }
    public function sendmail_08_mar($email_id, $c_id, $role)
    {
        $this->load->helper('url');
        $this->load->database();
        $username = base_url() . "/index.php?/controller_admin/password_changes/?user=$email_id&company_id=$c_id&role=$role";
       /* $config   = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'Fieldpro@kaspontech.com',
            'smtp_pass' => 'Field@2012',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('Fieldpro@kaspontech.com', 'Fieldpro');
        $this->email->to($email_id);
        $this->email->subject('Login Credentials');
        $this->email->message($username);
        $this->email->set_newline("\r\n");
        $this->email->send();*/
        $this->load->library('email');
       $config['protocol']='smtp';
        $config['smtp_host']='email-smtp.us-east-1.amazonaws.com';
        $config['smtp_port']='465';
        $config['smtp_timeout']='30';
        $config['smtp_user']='AKIAQVP5N6THGN3WW5WY';
        $config['smtp_pass']='BDr3VEpIbSqqFBuzQ291YtrMRfufJgXQAiZDLNsHMXtD';
	    $config['smtp_crypto'] = 'ssl';
        $config['charset']='utf-8';
        $config['newline']="\r\n";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from('hwsupport@arkance.world', 'Arkance');
        $this->email->to($email_id);
        $this->email->subject('Arkance Login Credentials - Customer');
        $this->email->message($username);
        $this->email->send();
       
    }
	
   public function sendmail($email_id, $c_id, $role)
    {
        $this->load->helper('url');
		 $this->load->library('encrypt');
        $this->load->database();
    
          $alphabet = "abcdefghijklmnpqrstuwxyzABCDEFGHIJKLMNPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 6; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }

				$passwordplain = "";
				//$passwordplain  = rand(999999999,9999999999);
				$passwordplain  = implode($pass);
				$newpass['password'] = $this->encrypt->encode($passwordplain);
				$this->db->where('username', $email_id);
				$this->db->update('login', $newpass); 
				$mail_message='Dear '.$role.','. "<br>";
				$mail_message.='Thank you for registered in Arkance. Please find the access details <br> <b>Username : '.$email_id.'</b>'.'<br> <b>Password : '.$passwordplain.'</b><br>';
				$mail_message.='<br>';
				$mail_message.='<br>Thanks & Regards';
				$mail_message.='<br>Arkance Team'; 
        $this->load->library('email');
       $config['protocol']='smtp';
        $config['smtp_host']='email-smtp.us-east-1.amazonaws.com';
        $config['smtp_port']='465';
        $config['smtp_timeout']='30';
        $config['smtp_user']='AKIAQVP5N6THGN3WW5WY';
        $config['smtp_pass']='BDr3VEpIbSqqFBuzQ291YtrMRfufJgXQAiZDLNsHMXtD';
	    $config['smtp_crypto'] = 'ssl';
        $config['charset']='utf-8';
        $config['newline']="\r\n";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from('hwsupport@arkance.world', 'Arkance');
        $this->email->to($email_id);
        $this->email->subject('Arkance - Customer Registration');
        $this->email->message($mail_message);
        $this->email->send();
	/*   if ($this->email->send()) {
		     // $this->session->set_flashdata('msg','Password sent to your email!');
             $json1 = '';
             $json1 = array(
               "status" => 1,
               "msg" => "Password sent to your email!"
           );
			
		}
	   else
	   {
	   }*/
       
    }
    

    public function login()
    {
        $this->load->helper('url');
        $this->load->database();
        $username           = urldecode(urldecode($this->input->post('username')));

        $password           = $this->input->post('password');
        $device_token       = $this->input->post('device_token');
        $reg_id             = $this->input->post('reg_id');
        $encrypted_password = $password;
        $this->load->model('Customer');
        $result   = $this->Customer->check_login($username);
        $result_1 = $this->encrypt->decode($result);
		//echo 'User Password'.$this->input->post('password');
		//echo '</br>';
		//echo 'user-'.$username.'-pass-'.$result_1.'-given pass-'.$encrypted_password;
		//echo '</br>';
		//exit;
        $res      = array();
        if ($result_1 == $encrypted_password) {
            if ($device_token != '' || $reg_id != '') {
                $this->load->model('Customer');
                $result2 = $this->Customer->store_id($device_token, $reg_id, $username);
                if ($result2 == 1) {
                    $userid   = $this->Customer->get_userid($username);
                    $cust_details     = $this->Customer->get_username($username);
					foreach($cust_details as $cust)
					{
						$name=$cust['customer_name'];
						$door_no=$cust['door_num'];
						$street=$cust['address'];
						$town=$cust['cust_town'];
                        $landmark=$cust['landmark'];
                        $region = $cust['region'];
						$city=$cust['city'];
						$state=$cust['state'];
						$country=$cust['cust_country'];
						$pincode=$cust['pincode'];
						$alternate_number=$cust['alternate_number'];
					}
                    $mobile   = $this->Customer->get_mobile($username);
                    $location = $this->Customer->get_location($username);
                    $company_id = $this->Customer->get_company($username);
                    $res      = array(
                        "msg" => "Logged in Successfully!!!",
                        "cust_id" => $userid,
                        "cust_name" => $name,
                        "emailid" => str_replace("\n","",$username),
                        "mobile" => $mobile,
                        "company_id" => $company_id,
                        "door_no" => $door_no,
                        "street" => $street,
                        "town" => $town,
                        "landmark" => $landmark,
                        "region" => $region,
                        "city" => $city,
                        "state" => $state,
                        "country" => $country,
                        "pincode" => $pincode,
						"alternate_number"=>$alternate_number
                    );
                    $json     = array(
                        "status" => 1,
                        "result" => $res
                    );
                } else {
                    $json = array(
                        "status" => 0,
                        "msg" => "Error storing device token/Register ID!!!"
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide Device Token/Register ID!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide correct details!!!"
            );
        }
        echo json_encode($json);
    }
	public function raise_amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('New_amc');
        $company_id    = $this->input->post('company_id');
        $ticket_id    = $this->input->post('modal_ticket');
        $amc_id    = $this->New_amc->select_amc1($company_id);
        $myStr      = $this->input->post('contract_type');
        $result     = substr($myStr, 0, 4);
        $gen_amc_id = $result . '_' . $amc_id;
        
        $ticket_id     = $this->New_amc->tick_id($company_id);
        $cust_id       = $this->input->post('cust_id');
        $cust_name     = $this->input->post('name');
        $mail          = $this->input->post('emailid');
        $contact_no    = $this->input->post('contact_no');
        $alt_contact   = $this->input->post('alt_no');
        $door_no       = $this->input->post('door_no');
        $street        = $this->input->post('street');
        $town          = $this->input->post('town');
        $land_mrk      = $this->input->post('landmark');
        $city          = $this->input->post('city');
        $state         = $this->input->post('state');
        $country       = $this->input->post('country');
        $pincode       = $this->input->post('pincode');
        $product_id    = $this->input->post('product_id');
        $cat_id        = $this->input->post('cat_id');
        $model_no        = $this->input->post('model_no');
        $call_type = $this->input->post('contract_type');
        $qty         = $this->input->post('quantity');
        $service     = $this->input->post('pref_date');
        $time        = $this->input->post('pref_time');
        $tim         = implode('', explode(' ', $time));
        $time1       = date("H:i:s", strtotime($tim));
        $date_time2  = $service . ' ' . $time1;
        $date_time1  = date("Y-m-d H:i:s", strtotime((string) $date_time2));
        
        $raised_time = new DateTime();
        $raised_time = $raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
        $raised_time = $raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
        $raised_time = $raised_time->format('Y-m-d H:i:s');
        $addr     = $street . ',' . $town . ',' . $city . ',' . $state . ',' . $country;
        $latitude=0;
		$longitude=0;
        $geo         = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addr) . '&sensor=false');
        // Convert the JSON to an array
        $geo         = json_decode($geo, true);
        if ($geo['status'] == 'OK') {
            // Get Lat & Long
            $latitude  = $geo['results'][0]['geometry']['location']['lat'];
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
        }
        if (($latitude <= 0 || $longitude <= 0)) {
            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($street) . '&sensor=false');
            // Convert the JSON to an array
            $geo = json_decode($geo, true);
            if ($geo['status'] == 'OK') {
                // Get Lat & Long
                $latitude  = $geo['results'][0]['geometry']['location']['lat'];
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
            }
        }
            
		if (!empty($cust_id))
		{
			if (!empty($ticket_id))
			{
				if (!empty($door_no)) 
				{
					if (!empty($street)) 
					{
						if (!empty($town)) 
						{
							if (!empty($city)) 
							{
								if (!empty($state)) 
								{
									if (!empty($country))
									{
										if (!empty($product_id)) 
										{
											if (!empty($cat_id)) 
											{
												if (!empty($call_type))
												{
													if (!empty($service)) 
													{
														if (!empty($time)) 
														{
															$cdata  = array(
																'ticket_id' => $ticket_id,
																'amc_id' => $gen_amc_id,
																'cust_id' => $cust_id,
																'contact_no' => $contact_no,
																'alternate_no' => $alt_contact,
																'door_no' => $door_no,
																'address' => $addr,
																'location' => $addr,
																'town' => $state,
																'latitude' => $latitude,
																'longitude' => $longitude,
																'country' => $country,
																'pincode' => $pincode,
																'product_id' => $product_id,
																'cat_id' => $cat_id,
																'model' => $model_no,
																'serial_no' => "",
																'call_type' => $call_type,
																'quantity' => $qty,
																'company_id' => $company_id,
																'raised_time' => $raised_time,
																'last_update' =>$raised_time,
																'cust_preference_date' => $date_time1
															);
															$insert = $this->New_amc->raise_newticket($cdata);
															if ($insert)
															{
																$company_id = $this->input->post('company');
																$data       = array();
																$this->load->model('AutoAssign');
																$address   = $street;
																$location  = $street;
																$product1  = $product_id;
																$category1 = $cat_id;
																$this->db->select('*');
																$this->db->from('sla_combination');
																$this->db->where('company_id', $company_id);
																$this->db->group_start();
																$this->db->like('product', 'all');
																$this->db->group_end();
																$query  = $this->db->get();
																$result = $query->result_array();
																if (!empty($result)) {
																	$product = 'all';
																} else {
																	$product = $product_id;
																}
																$this->db->select('*');
																$this->db->from('sla_combination');
																$this->db->where('company_id', $company_id);
																$this->db->group_start();
																$this->db->like('category', 'all');
																$this->db->group_end();
																$query1  = $this->db->get();
																$result1 = $query1->result_array();
																if (!empty($result1)) {
																	$category = 'all';
																} else {
																	$category = $cat_id;
																}
																$this->db->select('*');
																$this->db->from('sla_combination');
																$this->db->where('company_id', $company_id);
																$this->db->group_start();
																$this->db->like('cust_category', 'all');
																$this->db->group_end();
																$query2  = $this->db->get();
																$result2 = $query2->result_array();
																if (!empty($result2)) {
																	$cust_category = 'all';
																} else {
																	$cust_category = "";
																}
																$call_tag="";
																$this->db->select('*');
																$this->db->from('sla_combination');
																$this->db->where('company_id', $company_id);
																$this->db->group_start();
																$this->db->like('call_category', 'all');
																$this->db->group_end();
																$query3  = $this->db->get();
																$result3 = $query3->result_array();
																if (!empty($result3)) {
																	$call_category = 'all';
																} else {
																	$call_category = $call_tag;
																}
																$this->db->select('*');
																$this->db->from('sla_combination');
																$this->db->where('company_id', $company_id);
																$this->db->group_start();
																$this->db->like('service_category', 'all');
																$this->db->group_end();
																$query4  = $this->db->get();
																$result4 = $query4->result_array();
																if (!empty($result4)) {
																	$service_category = 'all';
																} else {
																	$service_category = $call_type;
																}
																$where8 = array(
																	'company_id' => $company_id,
																	'product' => $product,
																	'category' => $category,
																	'cust_category' => $cust_category,
																	'service_category' => $service_category,
																	'call_category' => $call_category
																);
																$this->db->select('ref_id');
																$this->db->from('sla_combination');
																$this->db->where($where8);
																$query5  = $this->db->get();
																$result5 = $query5->result_array();
																if (!empty($result5)) 
																{
																	$ref_id = $result5[0]['ref_id'];
																	$this->db->select('resolution_time,response_time,priority_level');
																	$this->db->from('sla_mapping');
																	$this->db->where('ref_id', $ref_id);
																	$query6  = $this->db->get();
																	$result6 = $query6->result_array();
																	if (!empty($result6))
																	{
																		$resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
																		$response   = date('H:i:s', strtotime($result6[0]['response_time']));
																		$priority   = date('H:i:s', strtotime($result6[0]['priority_level']));
																		$this->load->model('AutoAssign');
																		$tech = $this->AutoAssign->update_priority($priority, $ticket_id);
																		
																		
																		// Get JSON results from this request
																		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
																		// Convert the JSON to an array
																		$geo = json_decode($geo, true);
																		if ($geo['status'] == 'OK') 
																		{
																			// Get Lat & Long
																			$latitude  = $geo['results'][0]['geometry']['location']['lat'];
																			$longitude = $geo['results'][0]['geometry']['location']['lng'];
																			if ($latitude <= 0 || $longitude <= 0)
																			{
																				$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($location) . '&sensor=false');
																				// Convert the JSON to an array
																				$geo = json_decode($geo, true);
																				if ($geo['status'] == 'OK') 
																				{
																					// Get Lat & Long
																					$latitude  = $geo['results'][0]['geometry']['location']['lat'];
																					$longitude = $geo['results'][0]['geometry']['location']['lng'];
																					$this->load->model('AutoAssign');
																					$tech = $this->AutoAssign->get_nearbylocation($latitude, $longitude, $product1, $category1);
																					if (!empty($tech))
																					{
																						foreach ($tech as $row1) 
																						{
																							$this->load->model('AutoAssign');
																							$result = $this->AutoAssign->assign($row['ticket_id'], $row1['technician_id'], $resolution, $response, $priority);
																							if ($result == 1) 
																							{
																								$res1 = $this->AutoAssign->update_tech_task($row1['technician_id'], $row1['today_task_count']);
																							}
																							
																						}
																					}
																				}
																			} 
																			else 
																			{
																				$this->load->model('AutoAssign');
																				$tech = $this->AutoAssign->get_nearbylocation($latitude, $longitude, $product1, $category1);
																				if (!empty($tech)) 
																				{
																					foreach ($tech as $row1) 
																					{
																						$this->load->model('AutoAssign');
																						$result = $this->AutoAssign->assign($row['ticket_id'], $row1['technician_id'], $resolution, $response, $priority);
																						if ($result == 1) 
																						{
																							$res1 = $this->AutoAssign->update_tech_task($row1['technician_id'], $row1['today_task_count']);
																						}
																						
																					}
																				}
																			}
																		}
																	}
																}
																$res=  "Raised Successfully";
															}
														}
														else 
														{
														   $res=  "Select Preferred Time of visit";
														}
													} 
													else
													{
															$res=  "Select Preferred Date of visit";
													}
												}
												else 
												{
													$res=  "Service category is mandatory";
												}
											} 
											else
											{
												$res=  "Provide Sub-Category to Raise Ticket";
											}
										}
										else
										{
											$res=  "Provide Product to Raise Ticket";
										}
									}
									else 
									{
										$res=  "Country field is Empty";
									}
								}
								else 
								{
									$res=  "State field is Empty";
								}
							}
							else 
							{
								$res=  "City field is Empty";
							}
						}
						else
						{
							$res=  "Provide town fields";
						}
					}
					else 
					{
						$res=  "Provide street";
					}
				}
				else
				{
					$res=  "Provide Door no ";
				}
			}
			else
			{
				$res=  "Ticket id mandatory";
			}
		}
		else 
		{
			$res= "Customer id mandatory";
		}
		if($res=="Raised Successfully")
		{
			$json=array("status"=>1,"msg"=>$res);
		}
		else
		{
			$json=array("status"=>0,"msg"=>$res);
		}
		echo json_encode($json);
    }
    public function raise_tkt()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Customer');
        $this->load->model('New_amc');
        $res="";
        $response="";
		$json=array();
        $company_id    = $this->input->post('company_id');
        $ticket_id     = $this->New_amc->tick_id($company_id);
        $cust_id       = $this->input->post('cust_id');
        $cust_name     = $this->input->post('name');
        $mail          = $this->input->post('emailid');
        $contact_no    = $this->input->post('contact_no');
        $alt_contact   = $this->input->post('alt_no');
        $door_no       = $this->input->post('door_no');
        $street        = $this->input->post('street');
        $town          = $this->input->post('town');
        $land_mrk      = $this->input->post('landmark');
        $city          = $this->input->post('city');
        $state         = $this->input->post('state');
        $country       = $this->input->post('country');
		$serial_no       = $this->input->post('serial_no');
        $pincode       = $this->input->post('pincode');
        $product_id    = $this->input->post('product_id');
        $cat_id        = $this->input->post('cat_id');
        $model_no        = $this->input->post('model_no');
        $call_type = $this->input->post('contract_type');
        $call_tag      = $this->input->post('call_tag');
        $prob_desc     = $this->input->post('prob_desc');
        $region = $this->input->post('region');
        $work_type = $this->input->post('work_type');
        
            
        if(isset($_FILES["ticket_image"]['name']) )
		{
            
            $upload_contents = $_FILES["ticket_image"];
            $upload_content  = $_FILES["ticket_image"]["name"];
            $baseurl = base_url();

		$image=$_FILES["ticket_image"];
		$output_dir = "assets/images/";   
$url=$baseurl.$output_dir.$ticket_id.$upload_content;
move_uploaded_file($_FILES["ticket_image"]["tmp_name"],$output_dir.$ticket_id.$upload_content);   
        } else {
            $url = "";
        }
		$street = str_replace("&",' ', $street);
		$street = preg_replace('/[^A-Za-z0-9\-]/',' ', $street); 
     $new_addr= $street . ',' . $town . ',' . $city . ',' . $state . ',' . $country;
		
		//$new_addr= $addr.','.$lmrk.','.$c_town.','.$loc; 
        $service     = $this->input->post('pref_date');
        $time        = $this->input->post('pref_time');
        $tim         = implode('', explode(' ', $time));
        $time1       = date("H:i:s", strtotime($tim));
        $date_time2  = $service . ' ' . $time1;
        $date_time1  = date("Y-m-d H:i:s", strtotime((string) $date_time2));
        $raised_time = new DateTime();
        $raised_time = $raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
        $raised_time = $raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
        $raised_time = $raised_time->format('Y-m-d H:i:s');
		$latitude=0;
		$longitude=0;
	
		  $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI');
      $geo = json_decode($geo, true); 
		
		  if ($geo['status'] == 'OK') {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];   
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
         }
         else{
            $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI');
            $geo = json_decode($geo, true); 
            if ($geo['status'] == 'OK') {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];   
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
         } 
			 else
			 {
				$latitude = $geo['results'][0]['geometry']['location']['lat'];   
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
			 }
		 }
    /*    $geo         = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
        // Convert the JSON to an array
        $geo         = json_decode($geo, true);
        if ($geo['status'] == 'OK') {
            // Get Lat & Long
            $latitude  = $geo['results'][0]['geometry']['location']['lat'];
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
        }
        if (($latitude <= 0 || $longitude <=0)) {
            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($street) . '&sensor=false');
            // Convert the JSON to an array
            $geo = json_decode($geo, true);
            if ($geo['status'] == 'OK') {
                // Get Lat & Long
                $latitude  = $geo['results'][0]['geometry']['location']['lat'];
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
            }
        }*/
        
        if (!empty($cust_id)) {
            if (!empty($ticket_id)) {
                if (!empty($door_no)) {
                    if (!empty($street)) {
                        if (!empty($town)) {
                            if (!empty($city)) {
                                if (!empty($state)) {
                                    if (!empty($country)) {
                                        if (!empty($product_id)) {
                                            if (!empty($cat_id)) {
                                                if (!empty($call_tag)) {
                                                    if (!empty($call_type)) {
                                                        if (!empty($prob_desc)) {
                                                            if (!empty($service)) {
                                                                if (!empty($time)) {
                                                                    
                                                                    $my_date1   = implode('-', array_reverse(explode('/', $service)));
                                                                    $tim        = implode('', explode(' ', $time));
                                                                    $time1      = date("H:i:s", strtotime($tim));
                                                                    $date_time2 = $service . ' ' . $time1;
                                                                    $date_time1 = date("Y-m-d H:i:s", strtotime((string) $date_time2));
                                                                    $rdata      = array(
                                                                        'cust_id' => $cust_id,
                                                                        'ticket_id' => $ticket_id,
                                                                        'product_id' => $product_id,
                                                                        'cat_id' => $cat_id,
																		'model' => $model_no,
                                                                        'contact_no' => $contact_no,
                                                                        'call_type' => $call_type,
                                                                        'door_no' => $door_no,
                                                                        'address' => $street,
                                                                        'latitude' => $latitude,
                                                                        'longitude' => $longitude,
                                                                        'location' => $street,
                                                                        'town' => $town,
                                                                        'city' => $city,
                                                                        'state' => $state,
                                                                        'country' => $country,
                                                                        'region'=> $region,
																		'serial_no'=> $serial_no,
																		'pincode'=> $pincode,
                                                                        'prob_desc' => $prob_desc,
                                                                        'call_tag' => $call_tag,
                                                                        'work_type'=>$work_type,
                                                                        'image' => $url,
                                                                        'raised_time' => $raised_time,
																		'last_update' =>$raised_time,
																		'cust_tkt_raised' =>1,
                                                                        'company_id' => $this->input->post('company_id'),
                                                                        'cust_preference_date' => $date_time1
                                                                    );
                                                                    $this->load->model('New_amc');
                                                                    $insert1 = $this->New_amc->raise_data($rdata);
                                                                    if ($insert1 == 1) {

                                                                        $company_id = $this->input->post('company_id');
                                                                        $data       = array();
                                                                        $this->load->model('AutoAssign');
                                                                        $address   = $street;
                                                                        $location  = $street;
                                                                        $product1  = $product_id;
                                                                        $category1 = $cat_id;
                                                                        $this->db->select('*');
                                                                        $this->db->from('sla_combination');
                                                                        $this->db->where('company_id', $company_id);
                                                                        $this->db->group_start();
                                                                        $this->db->like('product', 'all');
                                                                        $this->db->group_end();
                                                                        $query  = $this->db->get();
                                                                        $result = $query->result_array();
                                                                        if (!empty($result)) {
                                                                            $product = 'all';
                                                                        } else {
                                                                            $product = $product_id;
                                                                        }
                                                                        $this->db->select('*');
                                                                        $this->db->from('sla_combination');
                                                                        $this->db->where('company_id', $company_id);
                                                                        $this->db->group_start();
                                                                        $this->db->like('category', 'all');
                                                                        $this->db->group_end();
                                                                        $query1  = $this->db->get();
                                                                        $result1 = $query1->result_array();
                                                                        if (!empty($result1)) {
                                                                            $category = 'all';
                                                                        } else {
                                                                            $category = $cat_id;
                                                                        }
                                                                        $this->db->select('*');
                                                                        $this->db->from('sla_combination');
                                                                        $this->db->where('company_id', $company_id);
                                                                        $this->db->group_start();
                                                                        $this->db->like('cust_category', 'all');
                                                                        $this->db->group_end();
                                                                        $query2  = $this->db->get();
                                                                        $result2 = $query2->result_array();
                                                                        if (!empty($result2)) {
                                                                            $cust_category = 'all';
                                                                        } else {
                                                                            $cust_category = "";
                                                                        }
                                                                        $this->db->select('*');
                                                                        $this->db->from('sla_combination');
                                                                        $this->db->where('company_id', $company_id);
                                                                        $this->db->group_start();
                                                                        $this->db->like('call_category', 'all');
                                                                        $this->db->group_end();
                                                                        $query3  = $this->db->get();
                                                                        $result3 = $query3->result_array();
                                                                        if (!empty($result3)) {
                                                                            $call_category = 'all';
                                                                        } else {
                                                                            $call_category = $call_tag;
                                                                        }
                                                                        $this->db->select('*');
                                                                        $this->db->from('sla_combination');
                                                                        $this->db->where('company_id', $company_id);
                                                                        $this->db->group_start();
                                                                        $this->db->like('service_category', 'all');
                                                                        $this->db->group_end();
                                                                        $query4  = $this->db->get();
                                                                        $result4 = $query4->result_array();
                                                                        if (!empty($result4)) {
                                                                            $service_category = 'all';
                                                                        } else {
                                                                            $service_category = $call_type;
                                                                        }
                                                                        $where8 = array(
                                                                            'company_id' => $company_id,
                                                                            'product' => $product,
                                                                            'category' => $category,
                                                                            'cust_category' => $cust_category,
                                                                            'service_category' => $service_category,
                                                                            'call_category' => $call_category
                                                                        );
                                                                        $this->db->select('ref_id');
                                                                        $this->db->from('sla_combination');
                                                                        $this->db->where($where8);
                                                                        $query5  = $this->db->get();
                                                                        $result5 = $query5->result_array();
                                                                        if (!empty($result5)) {
                                                                            $ref_id = $result5[0]['ref_id'];
                                                                            $this->db->select('resolution_time,response_time,priority_level');
                                                                            $this->db->from('sla_mapping');
                                                                            $this->db->where('ref_id', $ref_id);
                                                                            $query6  = $this->db->get();
                                                                            $result6 = $query6->result_array();
                                                                            
                                                                            if (!empty($result6)) {
                                                                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                                                                $response   = date('H:i:s', strtotime($result6[0]['response_time']));
                                                                                $priority   = date('H:i:s', strtotime($result6[0]['priority_level']));
                                                                                $this->load->model('AutoAssign');
                                                                                $tech = $this->AutoAssign->update_priority($priority, $ticket_id);
                                                                                
                                                                                
                                                                                // Get JSON results from this request
                                                                                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
                                                                                // Convert the JSON to an array
                                                                                $geo = json_decode($geo, true);
                                                                                if ($geo['status'] == 'OK') {
                                                                                    // Get Lat & Long
                                                                                    $latitude  = $geo['results'][0]['geometry']['location']['lat'];
                                                                                    $longitude = $geo['results'][0]['geometry']['location']['lng'];
                                                                                    if ($latitude <= 0 || $longitude <= 0) {
                                                                                        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($location) . '&sensor=false');
                                                                                        // Convert the JSON to an array
                                                                                        $geo = json_decode($geo, true);
                                                                                        if ($geo['status'] == 'OK') {
                                                                                            // Get Lat & Long
                                                                                            $latitude  = $geo['results'][0]['geometry']['location']['lat'];
                                                                                            $longitude = $geo['results'][0]['geometry']['location']['lng'];
                                                                                            $this->load->model('AutoAssign');
                                                                                            $tech = $this->AutoAssign->get_nearbylocation($latitude, $longitude, $product1, $category1);
                                                                                            if (!empty($tech)) {
                                                                                                foreach ($tech as $row1) {
                                                                                                    $this->load->model('AutoAssign');
                                                                                                    $result = $this->AutoAssign->assign($row['ticket_id'], $row1['technician_id'], $resolution, $response, $priority);
                                                                                                    if ($result == 1) {
                                                                                                        $res1 = $this->AutoAssign->update_tech_task($row1['technician_id'], $row1['today_task_count']);
                                                                                                    }
                                                                                                    
                                                                                                }
                                                                                            }
                                                                                            
                                                                                        }
                                                                                    } else {
                                                                                        $this->load->model('AutoAssign');
                                                                                        $tech = $this->AutoAssign->get_nearbylocation($latitude, $longitude, $product1, $category1);
                                                                                        if (!empty($tech)) {
                                                                                            foreach ($tech as $row1) {
                                                                                                $this->load->model('AutoAssign');
                                                                                                $result = $this->AutoAssign->assign($row['ticket_id'], $row1['technician_id'], $resolution, $response, $priority);
                                                                                                if ($result == 1) {
                                                                                                    $res1 = $this->AutoAssign->update_tech_task($row1['technician_id'], $row1['today_task_count']);
                                                                                                }
                                                                                                
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                
                                                                            }
                                                                        }

                                                                       
                                                                        if($work_type == 9) //ctpl worktype
                                                                        {
                                                                        //    $email       = $this->session->userdata('session_username'); //service desk username
                                                                           $email = 'sysadmin@arkance.world';
                                                                           $this->load->model('Admin_model');
                                                                           $this->load->model('model_service');
                                                                           $user         = $this->Admin_model->get_details_user($email, $company_id);
                                                                           $employee_id  = $user['employee_id'];
                                                                           $user_id  = $user['user_id']; 
                                                                           $ctpl_technician = $this->New_amc->ctpl_call_tech($email); // get the assigned technician for ctpl calls from user
                                                                            $technician_id = $ctpl_technician['ctpl_technician']; //ctpl tech id
                                                      
                                                                           if($technician_id == "" || $technician_id == NULL )
                                                                           {
                                                                              $response = "No CTPL technicians found.";
                                                                           }
                                                                           else{
                                                      
                                                                              $tech_data = $this->model_service->get_technican_data($technician_id); //get tech datas from tech id
                                                                              
                                                                               $tech_task_count = $tech_data['today_task_count']; // tech task count
                                                                                
                                                                               $update_tasks_count = $this->model_service->change_tech($company_id, $technician_id, $ticket_id, $tech_task_count); //update task count in technician table
                                                                              if ($update_tasks_count == 1) {
                                                                                
                                                                                 $assign_tkt = $this->model_service->tkt_ass($company_id, $technician_id, $ticket_id, $tech_task_count, $product_id, $cat_id, $employee_id);
                                                                  
                                                                                 if($assign_tkt == 1)
                                                                                 {
                                                                                    
                                                                                    $notify = "The new ticket assigned to you - ".$ticket_id; //notiification msg
                                                                                    $role   = "Technician";
                                                                                    $key    = "new";
                                                                                    $notify_tech = $this->model_service->update_notify($notify,$company_id,$role,$technician_id,$key);
                                                                  
                                                                                    // $this->load->model('Cronfunctions_model');
                                                                                    // $this->Cronfunctions_model->notify_ctpl_tickets();
                                                      
                                                                                    $response="Ticket Assigned Successfully";


                                                                                      //Email for ticket creation without image
                                                                                        $logo = base_url() .'assets/layouts/layout/img/logos.png';
                                                                                        $cust_email=$mail; 

                                                                                    $emailcontent=" <!doctype html>
                                                                                                    <html>
                                                                                                    <head>
                                                                                                    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                                                                                                    
                                                                                                    <body>
                                                                                                    <p>Dear Customer,</p>
                                                                                                    <p>Welcome to Arkance.</p>
                                                                                                    <p>The ticket has been created and technician assigned successfully.Kindly find the details below</p>
                                                                                                    <p>Ticket Id: <b>$ticket_id</b> </p>
                                                                                                    <p>Problem description: <b>$prob_desc</b> </p>
                                                                                                    <p>Contract Type: <b>$call_type</b> </p>
                                                                                                    <p> </p>
                                                                                                    If any queries please mail to : sysadmin@arkance.world
                                                                                                    <p> </p>
                                                                                                    <p>Thanks & regards,</p> <p>Arkance Team.</p>

                                                                                                    </body>
                                                                                                    </html>"; 

                                                        
                                                                                    $config   = Array(
                                                                                        'protocol' => 'smtp',
                                                                                        'smtp_host' => 'email-smtp.us-east-1.amazonaws.com',
                                                                                        'smtp_port' => '465',
                                                                                        'smtp_timeout' => '30',
                                                                                        'smtp_user' => 'AKIAQVP5N6THGN3WW5WY',
                                                                                        'smtp_pass' => 'BDr3VEpIbSqqFBuzQ291YtrMRfufJgXQAiZDLNsHMXtD',
                                                                                        'mailtype' => 'html',
                                                                                        'smtp_crypto' => 'ssl',
                                                                                        'wordwrap' => TRUE,
                                                                                        'charset' => 'utf-8'
                                                                                );

                                                                            $this->email->initialize($config);
                                                                            $this->load->library('email');
                                                                            $this->email->from('hwsupport@arkance.world', 'Arkance');
                                                                            $this->email->to($cust_email); 
                                                                            $this->email->cc('sysadmin@arkance.world');
                                                                            $this->email->subject('Arkance - New Ticket Assigned -' .$ticket_id);
                                                                            $this->email->set_crlf( "\r\n" );
                                                                            $this->email->set_newline("\r\n"); 
                                                                            $this->email->message($emailcontent);
                                                                            $result = $this->email->send();  
                                                                                 
                                                                  
                                                                                 }
                                                                                 else
                                                                                 {
                                                                                    $response="Ticket not assigned";
                                                                                 }
                                                                  
                                                                                
                                                                  
                                                                              }
                                                                              else
                                                                              {
                                                                                  $response="Task count not updated";
                                                                              }
                                                      
                                                                           }
                                                               
                                                                         
                                                               
                                                                         
                                                                        }
                                                      
                                                                        else{
                                                                            $response="Raised Successfully";

                                                                                                                                 //Email for ticket creation without image
                                $logo = base_url() .'assets/layouts/layout/img/logos.png';
                                $cust_email=$mail; 

                             $emailcontent=" <!doctype html>
                                            <html>
                                            <head>
                                            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                                            
                                            <body>
                                            <p>Dear Customer,</p>
                                            <p>Welcome to Arkance.</p>
                                            <p>Your ticket has been raised successfully.Kindly find the details below</p>
                                            <p>Ticket Id: <b>$ticket_id</b> </p>
                                            <p>Problem description: <b>$prob_desc</b> </p>
                                            <p>Contract Type: <b>$call_type</b> </p>
                                            <p> </p>
                                            If any queries please mail to : hwsupport@arkance.world
                                            <p> </p>
                                            <p>Thanks & regards,</p> <p>Arkance Team.</p>

                                            </body>
                                            </html>"; 

  
                             $config   = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'email-smtp.us-east-1.amazonaws.com',
                                'smtp_port' => '465',
                                'smtp_timeout' => '30',
                                'smtp_user' => 'AKIAQVP5N6THGN3WW5WY',
                                'smtp_pass' => 'BDr3VEpIbSqqFBuzQ291YtrMRfufJgXQAiZDLNsHMXtD',
                                'mailtype' => 'html',
                                'smtp_crypto' => 'ssl',
                                'wordwrap' => TRUE,
                                'charset' => 'utf-8'
                          );

                       $this->email->initialize($config);
                       $this->load->library('email');
                       $this->email->from('hwsupport@arkance.world', 'Arkance');
                       $this->email->to($cust_email); 
                       $this->email->subject('Arkance - New Ticket Raised -' .$ticket_id);
                       $this->email->set_crlf( "\r\n" );
                       $this->email->set_newline("\r\n"); 
                       $this->email->message($emailcontent);
                       $result = $this->email->send();  


                                                                        }




                                                                        
                                                                        //$response="Raised Successfully";
                                                                    }
                                                                } else {
                                                                    $response=  "Select Preferred Time of visit";
                                                                }
                                                            } else {
                                                                $response=  "Select Preferred Date of visit";
                                                            }
                                                        } else {
                                                            $response=  "Fill Problem Description";
                                                        }
                                                    }
                                                    
                                                    else {
                                                        $response=  "Service category is mandatory";
                                                    }
                                                } else {
                                                    $response=  "Call Category is mandatory";
                                                }
                                            } else {
                                                $response=  "Provide Sub-Category to Raise Ticket";
                                            }
                                        } else {
                                            $response=  "Provide Product to Raise Ticket";
                                        }
                                    } else {
                                        $response=  "Country field is Empty";
                                    }
                                } else {
                                    $response=  "State field is Empty";
                                }
                            } else {
                                $response=  "City field is Empty";
                            }
                        } else {
                            $response=  "Provide town fields";
                        }
                    } else {
                        $response=  "Provide street";
                    }
                } else {
                    $response=  "Provide Door no ";
                }
            } else {
                $response=  "Ticket id mandatory";
            }
        } else {
            $response= "Customer id mandatory";
        }
		if($response=="Raised Successfully") 
		{
		    $json_result=array(
                "status"=>1,
                "msg"=>$response
            );
		}
		else if($response=="Ticket Assigned Successfully")
		{
            $json_result=array(
            "status"=>2,
            "msg"=>$response
            );
        }
        else
        {
            $json_result=array(
                "status"=>0,
                "msg"=>$response
            );
        }
      echo json_encode($json_result);
    }

    public function load_tickets()
    {
       
        $this->load->helper('url');
        $this->load->database();
        $cust_id = $this->input->post('cust_id');
        $company_id = $this->input->post('company_id');
        $final_result    = array();
       
        if ($cust_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Customer ID"
            );
        }
        else if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        } else {
            $this->load->model('Customer');
            $result = $this->Customer->load_tickets($cust_id,$company_id);
            foreach($result as $row)
            {

                $status = $row['status_name'];
                // $staus_name = str_replace("_"," ",$status);
                // $tkt_status = ucwords($staus_name);

                if($status == 'newly_assigned_tkt')
                {
                    $tkt_status = 'Newly Assigned Ticket';
                }
		else if($status == 'newly_raised_ticket')
                {
                    $tkt_status = 'Newly Raised Ticket';
                }
                else if($status == 'accepted')
                {
                    $tkt_status = 'Accepted';
                }
                else if($status == 'reject')
                {
                    $tkt_status = 'Rejected';
                }
                else if($status == 'travel_start_status')
                {
                    $tkt_status = 'Travel Started';
                }
                else if($status == 'travel_end_status')
                {
                    $tkt_status = 'Travel Completed';
                }
                else if($status == 'ticket_start')
                {
                    $tkt_status = 'Ticket Start';
                }
                else if($status == 'completed')
                {
                    $tkt_status = 'Ticket Completed';
                }
                else if($status == 'escalated')
                {
                    $tkt_status = 'Ticket Escalated';
                }
                else if($status == 'work_in_progress')
                {
                    $tkt_status = 'Work in Progress';
                }
                else if($status == 'spare_accepted')
                {
                    $tkt_status = 'Spare Accepted';
                }
                else if($status == 'closed')
                {
                    $tkt_status = 'Ticket Closed';
                }
                else if($status == 'reimbursement')
                {
                    $tkt_status = 'Reimbursement';
                }
                else if($status == 'escalate')
                {
                    $tkt_status = 'Ticket Escalated';
                }
                else if($status == 'spare_request')
                {
                    $tkt_status = 'Spare Requested';
                }
                else if($status == 'spare_rejected')
                {
                    $tkt_status = 'Spare Rejected';
                }
                else if($status == 'spare_delivered')
                {
                    $tkt_status = 'Spare Delivered';
                }
                else if($status == 'otp_verified')
                {
                    $tkt_status = 'Completed';
                }
                else{

                }

                $data = array(
                    'ticket_id' => $row['ticket_id'],
                    'tech_name' => $row['tech_name'],
                    'contact_number' => $row['contact_number'],
                    'product_name' => $row['product_name'],
                    'cat_name' => $row['cat_name'],
                    'contact_no' => $row['contact_no'],
                    'door_no' => $row['door_no'],
                    'street' => $row['street'],
                    'town' => $row['town'],
                    'city' => $row['city'],
                    'state' => $row['state'],
                    'country' => $row['country'],
                    'landmark' => $row['landmark'],
                    'pincode' => $row['pincode'],
                    'prob_desc' => $row['prob_desc'],
                    'call_category' => $row['call_category'],
                    'service_category' => $row['service_category'],
                    'cust_preference_date' => $row['cust_preference_date'],
                    'raised_time' => $row['raised_time'],
                    'total_amount' => $row['total_amount'],
                    'bill_no' => $row['bill_no'],
                    'ticket_start_time' => $row['ticket_start_time'],
                    'ticket_end_time' => $row['ticket_end_time'],
                    'serial_no' => $row['serial_no'],
                    'model' => $row['model'],
                    'status_name' => $tkt_status,
                    'service_group' => $row['service_group'],
                );
                
                array_push($final_result,
                    $data
                    );
               
            }

            $json   = array(
                "status" => 1,
                "result" => $final_result
            );

        }

        echo json_encode($json);
        
       
    }
	
    public function add_product()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $cust_id = $this->input->post('cust_id');
        $product_id = $this->input->post('product_id');
        $cat_id = $this->input->post('cat_id');
        $model_no = $this->input->post('model_no');
        $serial_no = $this->input->post('serial_no');
        $contract_type = $this->input->post('contract_type');
        $purchase_date = $this->input->post('purchase_date');
        $retailer_name = $this->input->post('retailer_name');
		
		$start_date = $this->input->post('start_date');  /////// (format –(mm/dd/yyyy)- 09/24/2018)
		$contract_duration  = $this->input->post('contract_duration');
		$start_date =date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime("+".$contract_duration." months", strtotime($start_date))); 
        $warrenty_expairy_date = $end_date;
        if ($cust_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Customer ID"
            );
        }
        else if ($company_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Company ID"
            );
        }
        else if ($product_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Product ID"
            );
        }
        else if ($cat_id == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Category ID"
            );
        }
        else if ($model_no == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Model Number"
            );
        }
        else if ($serial_no == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Serial Number"
            );
        }
        else if ($contract_type == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide service Category"
            );
        }
        else if ($purchase_date == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide service Category"
            );
        }
        else if ($retailer_name == "") {
            $json = array(
                "status" => 0,
                "msg" => "Please provide service Category"
            );
        }else {
            $this->load->model('Customer');
            $result = $this->Customer->add_product($cust_id,$company_id,$product_id,$cat_id,$model_no,$serial_no,$contract_type,$purchase_date,$retailer_name,$start_date,$end_date,$contract_duration,$warrenty_expairy_date);
            $json   = array(
                "status" => 1,
                "result" => $result
            );
        }
        
        echo json_encode($json);
    }
	
	
}