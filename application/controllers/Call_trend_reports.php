<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Call_trend_reports extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index()
	{
			 if ($this->session->userdata('user_logged_in'))
			 {
				 
		$data['webPageheading'] = ' Open / Closed - Call Trend Graph';
		  $this->load->model('Admin_model');  
	 $sess_username=$this->session->userdata('session_username');
     $company_id= $this->session->userdata('companyid');
            $user = $this->Admin_model->get_details_user($sess_username,$company_id); 
          $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];	

	 $data['user']=$this->Admin_model->get_details_user($sess_username,$company_id,$region,$area);
			$this->load->view('reports/call_trend_report',$data);
		}
		else{
	 redirect(base_url());
		}
	}
	
    public function call_trend()
    {
        
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
		
	 $close_array = array('company_id'=>$company_id,'status'=>1);
	 $closed = $this->common->get_method($close_array,array('methodname'=>'GET_CALL_TREND_REPORT','sp_next_result'=>1));	///// 1 - Closed ticket count and week number
		
		$close_key=array();
		$close_value=array();
		  foreach($closed->result() as $row) {
			 // print_r($row);
			  $close_key[]=$row->weekno;
			  $close_value[]=$row->close_tkt;
		  }
	
	$open_array = array('company_id'=>$company_id,'status'=>2);
	 $opened = $this->common->get_method($open_array,array('methodname'=>'GET_CALL_TREND_REPORT','sp_next_result'=>1));	///// 2 - Opened ticket count and week number
		
		$open_key=array();
		$open_value=array();
		  foreach($opened->result() as $row1) {
			// print_r($row1);
			  $open_key[]=$row1->weekno;
			  $open_value[]=$row1->open_tkt;
		  }
			//echo $row1->open_tkt;
		//die;
	//	echo "open";
	//	print_r($value1);
	//	die;
		
			$log_array = array('company_id'=>$company_id,'status'=>3);
	 $call_log = $this->common->get_method($log_array,array('methodname'=>'GET_CALL_TREND_REPORT','sp_next_result'=>1));	///// 2 - Call logged ticket count and week number
		
		$log_key=array();
		$log_value=array();
		  foreach($call_log->result() as $row2) {
			// print_r($row1);
			  $log_key[]=$row2->weekno;
			  $log_value[]=$row2->call_log_tkt;
		  }
		
		
	
		$categoryarray=array_merge($close_key,$open_key,$log_key);
		$categoryarray=array_unique($categoryarray);
		//
		sort($categoryarray);
		$category=[];
	//	$category[]=$categoryarray[0];
		$data_opencall=[];
		$data_closecall=[];
		$data_logcall=[];
		if(count($categoryarray)>0)
		{
			for($i=$categoryarray[0];$i<=$categoryarray[count($categoryarray)-1];$i++)
			{
				$category[]="Wk ".$i;
				
				if(in_array($i, $close_key))
					{
					 $arrayindexsearch=array_keys($close_key,$i);
						$arrayindex1=$arrayindexsearch[0];
						$data_closecall[]=(int)$close_value[(int)$arrayindex1];
						
					}
					else
					{
						$data_closecall[]=0;
						
					}

			  if(in_array($i, $open_key))
					{
						$arrayindexsearch=array_keys($open_key,$i);
						$arrayindex=$arrayindexsearch[0];
						$data_opencall[]=(int)$open_value[(int)$arrayindex];
					}
					else
					{
						$data_opencall[]=0;
					}
				
			
				  if(in_array($i, $log_key))
					{
						$arrayindexsearch=array_keys($log_key,$i);
						$arrayindex=$arrayindexsearch[0];
						$data_logcall[]=(int)$log_value[(int)$arrayindex];
					}
					else
					{
						$data_logcall[]=0;
					}	
				
				
			}
		
		}
		

		
		$data=array();
		$data[]=array('name'=>'Open Calls','type'=>'spline','data'=>$data_opencall);
		$data[]=array('name'=>'Closed Calls','type'=>'spline','data'=>$data_closecall);
		$data[]=array('name'=>'Call Logged week','type'=>'spline','data'=>$data_logcall);
		//$data[]=array('name'=>'Linear','linkedTo'=>'primary','showInLegend'=>'true','enableMouseTracking'=>'false','type'=>'trendline','algorithm'=>'linear','data'=>$data_opencall);
		//$data[]=array('name'=>'Linear','type'=>'trendline','showInLegend'=>'true','enableMouseTracking'=>'false','dashStyle'=>'dash','data'=>[0,9],[27,9], 'marker'=>array('enabled'=>'false'));
		$data=array("category"=>$category,"data"=>$data);
    /*   $this->load->model('model_service');
       $result = $this->model_service->call_trend($company_id, $filter, $region, $area, $location);*/
        echo json_encode($data);
    }
  		
	
}