<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_rate_reports extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index()
	{
			 if ($this->session->userdata('user_logged_in'))
			 {
				 
		$this->data['webPageheading'] = 'Customer Service Rating';
		
	$company_id = $this->session->userdata('companyid');
		

			$this->load->view('reports/customer_rate_report',$this->data);
		}
		else{
	 redirect(base_url());
		}
	}
	
	public function cust_rate_report_all()
     {

		 if ($this->session->userdata('user_logged_in'))
		 {
				
			 
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));
		 $company_id = $this->session->userdata('companyid');
		 $startdate=$this->input->post('startdate');
			 $sdate = str_replace('/', '-', $startdate);
 $startdate = date('Y-m-d', strtotime($sdate));
				$enddate=$this->input->post('enddate');	
			  $edate = str_replace('/', '-', $enddate);
 $enddate = date('Y-m-d', strtotime($edate));
	
          $array = array('companyid'=>$company_id,'from_date'=>$startdate,'to_date'=>$enddate);
		  $results = $this->common->get_method($array,array('methodname'=>'GET_CUSTOMER_RATE_REPORT','sp_next_result'=>0));
		  $cust=$results->row();		 
		  $data = array();
			 $i=0;
			 $tot_1=$cust->one_five+$cust->six_eight+$cust->nine_ten+$cust->non_rate;
			$percent1to5 = $cust->one_five * 100 / $cust->tot_cust;
			$percent6to8 = $cust->six_eight * 100 / $cust->tot_cust;
			$percent9to10 = $cust->nine_ten * 100 / $cust->tot_cust;
			$percentnorate = $cust->non_rate * 100 / $cust->tot_cust;
				$fcolm=array('<b>1 to 5</b>','<b>6 to 8</b>','<b>9 to 10</b>','<b>Customer Not Rated</b>');
			 $fcolmvalue=array($cust->one_five,$cust->six_eight,$cust->nine_ten,$cust->non_rate);
			 $fcolmperct=array($percent1to5,$percent6to8,$percent9to10,$percentnorate);
          for($h=0;$h<=3;$h++) {
		
			$tot_1+='';
               $data[] = array(
				  $fcolm[$i], 
				  $fcolmvalue[$i],
				 number_format((float)$fcolmperct[$i], 2, '.', '').'%'
				  
               );
			  $i=$i+1;
          }
			if($tot_1=='')
			{
				$tot_1='0';
			}
			  else
			  {
				  $tot_1=$tot_1;
			  }
			 $tot_2='';
			  $data[]=array('<b>Total Calls</b>',$tot_1,$tot_2);

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $results->num_rows(),
                 "recordsFiltered" => $results->num_rows(),
                 "data" => $data
            );
		
		  echo json_encode($output);
		 }
			 else{
			 redirect(base_url());
			 }
     }
			
	
}