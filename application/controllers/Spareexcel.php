<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spareexcel extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->model('common');	
		$this->load->model('capricotexcelimportexport');	
	
		
	}

	public function index()
	{
			
	}
	
	public function spareexcelexport()
	{
		$this->load->model('capricotexcelimportexport');	
		
					
					$dateof = date('Y-m-d');
			
					$array = array('dateof' => $dateof);
					$this->capricotexcelimportexport->spareexcel_fp_sap($array);
		
	}
	
public function spareexcelimport()
    {

	 $this->load->model('Admin_model');
	$this->load->library('excel');
	$this->load->model('capricotexcelimportexport');	
$companyid='company51';
		
		$date = date("d-m-Y");
	$path='assets/excel/spare/sap/spare_'.$date.'.xlsx';

	
		$inputFileName = $path; 
$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet

$cc=1;	
for($i=2;$i<=$arrayCount;$i++)
{   
	 $product=$allDataInSheet[$i]["I"];
   $sub_product=$allDataInSheet[$i]["J"];
 $check_prod=$this->Admin_model->check_prod($product);
if($check_prod != 0){
$check_produ=$this->Admin_model->check_produ($sub_product,$check_prod);
if($check_produ !=0 ){	
	if($allDataInSheet[$i]["C"]!='' || $allDataInSheet[$i]["D"]!='')
	{
	 $spare_code=$allDataInSheet[$i]["A"];	
	 $spare_desc=$allDataInSheet[$i]["B"];		
	 $spare_name=$allDataInSheet[$i]["C"];	
	 $spare_model=$allDataInSheet[$i]["D"];
   $spare_source=$allDataInSheet[$i]["E"];	
   $price_c=$allDataInSheet[$i]["F"];	
   $qty_purchase_c=$allDataInSheet[$i]["G"];	
   $qty_used_c=$allDataInSheet[$i]["H"];	
   $prod_id=$check_prod['product_id'];
   $sub_cat=$check_produ['cat_id'];
   $purchase_date=$allDataInSheet[$i]["K"];		
    $expire_date=$allDataInSheet[$i]["L"];		
		
		$price=number_format($price_c,2,",",".");
		$qty_purchase=round($qty_purchase_c);
		$qty_used=round($qty_used_c);
		
		/*	
			$date='08-04-2019';
				$test = strtotime($date);
	         echo  $test = date('Y-m-d', $test);
			 */	
  	$p_date = strtotime($purchase_date);
	$p_date = date('Y-m-d', $p_date);
	
		$ex_date = strtotime($expire_date);
	$ex_date = date('Y-m-d', $ex_date);	
		
 $check_spare=$this->capricotexcelimportexport->check_spare($spare_code);
if($check_spare == 0){
        $datass = $this->Admin_model->spareid_check();
        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
        $datass = $datass + $cc;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $spare_id = "Spare_" . $datass;	
	
	$data=array('spare_id'=>$spare_id, 'spare_code'=>$spare_code, 'spare_desc'=>$spare_desc, 'spare_name'=>$spare_name, 'spare_modal'=>$spare_model, 'image'=>'', 'spare_source'=>$spare_source, 'price'=>$price, 'quantity_purchased'=>$qty_purchase, 'quantity_used'=>$qty_used, 'product'=>$prod_id, 'category'=>$sub_cat, 'p_date'=>$p_date, 'ep_date'=>$ex_date, 'expiry_date'=>$ex_date,'company_id'=>$companyid);

$this->capricotexcelimportexport->integeration_insert_spare($data);
//echo 'insert';	
//print_r($data);
}
else
{
$data=array('spare_desc'=>$spare_desc, 'spare_name'=>$spare_name, 'spare_modal'=>$spare_model, 'image'=>'', 'spare_source'=>$spare_source, 'price'=>$price, 'quantity_purchased'=>$qty_purchase, 'quantity_used'=>$qty_used, 'product'=>$prod_id, 'category'=>$sub_cat, 'p_date'=>$p_date, 'ep_date'=>$ex_date, 'expiry_date'=>$ex_date);	

$this->capricotexcelimportexport->integeration_edit_spare($data,$spare_code);	
	
//echo 'edit';		
}

	}
}
}
	$cc++;	
}	
		
		
		
	}

	
			
	
}