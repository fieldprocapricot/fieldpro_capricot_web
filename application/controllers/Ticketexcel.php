<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('memory_limit', '256M');
class Ticketexcel extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->model('common');	
	
	
		
	}

	public function index()
	{
			
	}
	
	public function ticketexcelexport()
	{
		$this->load->model('capricotexcelimportexport');	
		
					
					$dateof = date('Y-m-d');
			
					$array = array('dateof' => $dateof);
					$this->capricotexcelimportexport->ticketexcel_fp_sap($array);
		
	}

public function ticketexcelexport_before3pm()
	{
		$this->load->model('capricotexcelimportexport');	
		
	//$dateof = date('2020-01-01 00:00:00');
	
	//$currdate =  date('2020-03-31 23:59:59');
	$dateof = date('Y-m-d 00:00:00');
	
	$currdate =  date('Y-m-d 15:00:00');
	
	
			
					$array = array('dateof' => $dateof,'currdate' => $currdate,'export_status' => 1);
					$this->capricotexcelimportexport->ticketexcel_fp_sap($array);
		
	}
		
public function ticketexcelexport_after3pm()
	{
		$this->load->model('capricotexcelimportexport');	
		
					
					$dateof = date('Y-m-d 15:00:00');
	                $currdate = date('Y-m-d 23:00:00');
			
					$array = array('dateof' => $dateof,'currdate' => $currdate,'export_status' => 2);
					$this->capricotexcelimportexport->ticketexcel_fp_sap($array);
		
	}
				
	public function database_backup()
	{
			$this->load->dbutil();
			$this->load->library('zip');
			$db_format=array('format'=>'zip','tables' => array('admin_user','all_tickets','amc_price','amc_type','attendance','avaliblity','billing','billingfor_sla','call_tag','call_tagging','category_details','certificate','company','country','customer','customer_contract','fp_service_activities','fp_service_category','impress_consumed_spare','imprest_spare_approved_qty','knowledge_base','leaderboard','login','notification','personal_spare','priority','product_management','quiz','reimbursement','reimbursement_request','reward','score_reward','score_update','service_disc','service_group','sla','sla_combination','sla_mapping','spare','spare_loc','spare_location','spare_transfer','states','status','technician','technician_hierarchy','ticket_history','training','training_link','training_score','user'),'filename'=>'fieldpro_capricot.sql');
			//var_dump($db_format);
			$backup=$this->dbutil->backup($db_format);
			$dbname='backup-on-'.date('Y-m-d').'.zip';
			$save='assets/db_backup/'.$dbname;
			$this->load->helper('file');
			write_file($save,$backup);
			$this->load->helper('download');
			force_download($dbname,$backup);
	}	
}
