<?php
//cretaed by: Aarthi - Nov 28 2020 for CTPL KRA KPI List
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctpl_kra_kpi_reports extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
		$this->load->model('New_amc');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index()
	{
		if ($this->session->userdata('user_logged_in'))
		{	
			$this->data['webPageheading'] = 'CTPL KRA KPI Report'; // Page Heading
			$this->load->view('reports/ctpl_kra_kpi_report'); //view page

		}
		else
		{
			redirect(base_url());
		}
	}
	
	// report list
	public function ctpl_kra_kpi_list_all() //data table list
	{
		if ($this->session->userdata('user_logged_in'))
		{
			$company_id = $this->session->userdata('companyid'); //company id
			$username= $this->session->userdata('session_username'); // manager profile username
			$startdate=$this->input->post('startdate');  // filter start date
			$sdate = str_replace('/', '-', $startdate); 
			$startdate = date('Y-m-d', strtotime($sdate));
			$enddate=$this->input->post('enddate');	// filter end date
			$edate = str_replace('/', '-', $enddate);
			$enddate = date('Y-m-d', strtotime($edate));
			$get_ctpl_tech = $this->New_amc->ctpl_call_tech($username); //fetch technician data from ctpl list
			$technician=$get_ctpl_tech['ctpl_technician']; // ctpl technician id

			$draw = intval($this->input->get("draw"));
			$start = intval($this->input->get("start"));
			$length = intval($this->input->get("length"));


			$array = array(  // onload datatable array
                'companyid'=>$company_id,
                'from_date'=>$startdate,
                'to_date'=>$enddate,
                'technician'=>$technician,
			);
			
			$kra_kpi_report = $this->common->ctpl_kra_kpi_report_filter($array); // datalist query from model

			$data = array();
			$i=0;

			foreach($kra_kpi_report->result() as $row) {
				
				$technician_id = $row->technician_id;	//technician_id from model

				$calc_data = array(   // array for actual and avarage calculations
					'companyid'=>$company_id,
					'from_date'=>$startdate,
					'to_date'=>$enddate,
					'technician_id'=>$technician_id
				);

				$ctpl_mtr_calc = $this->common->ctpl_mtr_calc($calc_data); // mtr calc
				$ctpl_mtc_calc = $this->common->ctpl_mtc_calc($calc_data); // mtc calc
				$kra_customer_feedback = $this->common->kra_customer_feedback($calc_data); // customer feedback
			
				$i=$i+1;
				$data[] = array(

					$i,
					'<b>'.$row->first_name.'</b> ( '.$row->employee_id.')', // technician first name alonmg with employee id

					'<= 2 Hrs', // mtr target value
					$ctpl_mtr_calc[0], // mtr actual value
					$ctpl_mtr_calc[1], // mtr average value

					'<= 24 Hrs', // mtc target value
					$ctpl_mtc_calc[0], // mtc actual value
					$ctpl_mtc_calc[1], // mtc average value

					'90%', //customer feedback target value
					$kra_customer_feedback[0], //customer feedback actual value
					$kra_customer_feedback[1] //customer feedback average value


				);
			}
				$output = array(
					"draw" => $draw,
					"recordsTotal" => $kra_kpi_report->num_rows(),
					"recordsFiltered" => $kra_kpi_report->num_rows(),
					"data" => $data
				);

			echo json_encode($output);
		}
		else
		{
		redirect(base_url());
		}
	}

			
	
}
