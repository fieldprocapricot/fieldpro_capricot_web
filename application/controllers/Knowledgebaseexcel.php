<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Knowledgebaseexcel extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->model('common');	
	
	
		
	}

	public function index()
	{
			
	}
	
	public function knowledgebaseexcelexport()
	{
		$this->load->model('capricotexcelimportexport');	
		
					
					$dateof = date('Y-m-d');
		
					$array = array('dateof' => $dateof);
					$this->capricotexcelimportexport->knowledgebaseexcel_fp_sap($array);
		
	}
	
public function knowledgebaseexcelimport()
    {
	$this->load->model('capricotexcelimportexport');	
	 $this->load->model('Admin_model');
	$this->load->model('common');	
	$this->load->library('excel');
$companyid='company51';
		
		$date = date("d-m-Y");
		 $path='assets/excel/knowledgebase/sap/knowledgebase_'.$date.'.xlsx';
	
		$inputFileName = $path; 
$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
	
for($i=2;$i<=$arrayCount;$i++)
{   
	 $product=$allDataInSheet[$i]["A"];
   $sub_product=$allDataInSheet[$i]["B"];
 $check_prod=$this->Admin_model->check_prod($product);
if($check_prod != 0){
$check_produ=$this->Admin_model->check_produ($sub_product,$check_prod);
if($check_produ !=0 ){	
	if($allDataInSheet[$i]["C"]!='' || $allDataInSheet[$i]["D"]!='')
	{
	 $prod_id=$check_prod['product_id'];
	 $sub_cat=$check_produ['cat_id'];
   $problem=$allDataInSheet[$i]["C"];
   $solution=$allDataInSheet[$i]["D"];
   $techid=$allDataInSheet[$i]["E"];	
   $region=$allDataInSheet[$i]["F"];	
   $area=$allDataInSheet[$i]["G"];	
   $status=$allDataInSheet[$i]["H"];	
	
	$data=array('product'=>$prod_id, 'category'=>$sub_cat, 'sub_category'=>$sub_cat, 'problem'=>$problem, 'solution'=>$solution, 'image'=>'', 'video'=>'', 'technician_id'=>$techid, 'company_id'=>$companyid, 'region'=>$region, 'area'=>$area, 'status'=>$status);
	//	print_r($data);
$this->capricotexcelimportexport->integeration_knowledge($data);	
	}
	else
	{
	}
}
}
}	
		
		
}

			
	
}