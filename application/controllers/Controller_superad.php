<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class controller_superad extends CI_Controller {
	 public function __construct()
	{
	parent:: __construct();
	$this->load->library('encrypt');
	$this->load->library('email');
	$this->load->library('session');
	$this->load->helper(array('url','cookie'));
	$this->load->database();
	$this->load->model('Super_admin');	
	$this->load->model('Admin_model');
	}
	public function index()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		if($this->session->userdata('user_logged_in'))
		{
			$data['record']=$this->Super_admin->company_count();
	        $data['renew']=$this->Super_admin->renew_count();
			$data['service']=$this->Super_admin->count_s();
                //$data['service']=$this->Super_admin->counts();
		//$data['tech']=$this->Super_admin->count1();
                $data['tech']=$this->Super_admin->count_t();

			$this->load->view('superad_dashboard',$data);
		}
		else
	    {
		    redirect('login/login');
	    }

	}

	/*
	Removing SLA
	*/
   public function delet_sla()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $ref_id   = $this->input->post('ref_id');
        $company_id   = $this->input->post('company_id');
        $data = $this->Super_admin->delet_sla($ref_id,$company_id);
		if($data==1){
        $data1= $this->Super_admin->delet_sla_map($ref_id,$company_id);
		if($data1==1)
		{
			echo "Deleted Successfully!";
		}
		else
		{
			echo "Error while deleting !";
		}
		}
        
		else
		{
			echo "Error while deleting !";
		}
	}
	
	/*
	Edit the existing sla details
	*/
	public function submit_edit_sla()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Super_admin');
        $company_id     = $this->input->post('company_id');
        $ref_id     = $this->input->post('ref_id');
        $response       = $this->input->post('response');
        $resolution     = $this->input->post('resolution');
        $acceptance     = $this->input->post('acceptance');
        $sla_compliance = $this->input->post('sla_compliance');
        $mttr           = $this->input->post('mttr');
        $result = $this->Super_admin->submit_edit_sla($company_id, $ref_id, $response, $resolution, $acceptance, $sla_compliance, $mttr);
		if($result==1)
		{
			echo "SLA Updated Successfully";
		}
		else
		{
			echo "Error in Update!";
		}
	}
	public function user_profile()
	{
		$this->load->view('sample');
	}

	//Displalying the new comapany add form
	public function new_subscription()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		if($this->session->userdata('user_logged_in'))
		{
			$this->load->view('enroll_new');
		}
		else
	    {
		    redirect('login/login');
	    }
	}
	public function manage_subscription()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		if($this->session->userdata('user_logged_in'))
		{
			$this->load->view('renew');
		}
		else
	    {
		    redirect('login/login');
	    }	 
	}
   

	//displaying the product page
	public function product_master()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		if($this->session->userdata('user_logged_in'))
		{
			$data['company']= $this->Super_admin->getcompany_ss();
			$data['record']=$this->Super_admin->getproduct();
			//print_r($data);
			$this->load->view('product_master',$data);
		}
		else
	    {
		    redirect('login/login');
	    }
	}



	/*
	 displaying the sla_mapping
	*/
	public function sla_mapping()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		if($this->session->userdata('user_logged_in'))
		{
			$data['company']=$this->Super_admin->getcompany_ss();
			$data['priority_level']=$this->Super_admin->priority_level();
			$data['get_sla']=$this->Super_admin->sla_get();
			$this->load->view('sla_page',$data); 
		}
		else
	    {
		    redirect('login/login');
	    }	
	}

        
	public function getproduct(){		
		$data_company=$this->Super_admin->getcompany_s(); 
		$nodes = array();
		foreach($data_company as $val)
		{
			array_push($nodes,array('id'=>$val['company_id'],'parent'=>"#",'text'=>$val['company_name'],'icon'=>$val['company_logo']));
			$company=$val['company_id'];
			$data=$this->Super_admin->getproduct_s($company);
			foreach($data as $val_product){
				array_push($nodes,array('id'=>$val_product['product_id'],'parent'=>$val_product['company_id'],'text'=>$val_product['product_name'],'icon'=>$val_product['product_image']));
				$prod_id_1=$val_product['product_id'];
				$data_category=$this->Super_admin->getcategory_s($prod_id_1);
				 foreach($data_category as $val_category){
					array_push($nodes,array('id'=>$val_category['cat_id'],'parent'=>$val_category['prod_id'],'text'=>$val_category['cat_name'],'icon'=>$val_category['cat_image'])); 
				}
			} 
		}
		echo json_encode($nodes);
	}	
	

	/* Listing the billing, product, priority informations */
	public function view_existingbilling(){
		
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$company=$this->input->post('company_id');
		$data=$this->Super_admin->view_existingbilling($company);
		echo json_encode($data);	
	
	}
	
	public function treeview(){
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$company=$this->input->post('company');
		$data=$this->Super_admin->sla_get1($company);
		print_r(json_encode($data));
	}

	/*Check SLA compination among company, product, sub products */
       public function check_combination()
      {
           $this->load->helper('url');
	   $this->load->database();
	   $this->load->model('Super_admin');
	   $comp =$this->input->post('company_name');
	   $prio= $this->input->post('priority');	
	   $prod= $this->input->post('product');	
           $cat= $this->input->post('category');
	   $data=$this->Super_admin->check_combo($comp,$prio,$prod,$cat);
	   return $data;
	  }
	  
	  /*Update the SLA details */
	 public function update_sla()
      {
           $this->load->helper('url');
	   $this->load->database();
	   $this->load->model('Super_admin');
	   $comp =$this->input->post('comp_id');	
	   $prod= $this->input->post('p_name');	
           $cat= $this->input->post('c_name');
           $sp1= $this->input->post('sla_P1');
          		 $ep1= $this->input->post('elap_P1');
           $sp2= $this->input->post('sla_P2');
           		$ep2= $this->input->post('elap_P2');
           $sp3= $this->input->post('sla_P3');
          		 $ep3= $this->input->post('elap_P3');
           $sp4= $this->input->post('sla_P4');
          		 $ep4= $this->input->post('elap_P4');
		  
	   $data=$this->Super_admin->update_sla($comp,$prod,$cat,$sp1,$ep1,$sp2,$ep2,$sp3,$ep3,$sp4,$ep4);
	 
		  if($data==1){
			  echo "updated";
		  }
		  else{
			  echo "error occured";
		  }
	  }
	  
	  /* 
	  
	 Display Billling details
	  */
	public function billing_details()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		if($this->session->userdata('user_logged_in'))
		{
			$this->load->view('billing_details');	
		}
		else
	    {
		    redirect('login/login');
	    }		
	}
	public function test_sla()
	{
		$this->load->helper('url');
        $this->load->database();
		$this->load->model('Super_admin');
		$data['records']=$this->Super_admin->getproduct();
            //$data['records2']=$this->Admin_model->getproduct();
		$data['records2']=$this->Super_admin->getsubproduct();
		$data['records_spare']=$this->Super_admin->getspare();  
		$data['record']=$this->Super_admin->getproduct();
		$this->load->view('test_sla', $data);
	}
	public function product_display()
	{
	    $this->load->helper('url');
		$this->load->database(); 
		$this->load->model('Super_admin');
		$data['records']=$this->Super_admin->getproduct();
            //$data['records2']=$this->Admin_model->getproduct();
		$data['records2']=$this->Super_admin->getsubproduct();
		$data['records_spare']=$this->Super_admin->getspare();  
		$data['record']=$this->Super_admin->getproduct();
		$this->load->view('product_view', $data); 
                
	}

	/*Load product category */
     public function load_product()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$id=$this->input->post('company_id');
		$data=$this->Super_admin->load_product($id);
		return $data;
	}

	/*Load product sub category */
	public function load_subcategory()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$id=$this->input->post('prod_id');
		$data=$this->Super_admin->load_subcategory($id);
		print_r(json_encode($data));
	}

	/*Displaying the priority list */
       public function load_priority()
	{
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		$priority =$this->Super_admin->load_priority();
		return $priority;
	}

      //get the company license details.
      public function get_licence(){
		 $this->load->helper('url');
		 $this->load->database();
		 $this->load->model('Super_admin');
		 $company_id=$this->input->post('company_id');
		 $data=$this->Super_admin->get_licence($company_id);
		 print_r(json_encode($data));
	}


      public function renew_ondemand()
	{
		 $this->load->helper('url');
		    $this->load->database();
		    $this->load->model('Super_admin');
			$companyid=$this->input->post('companyid');
			$rdate=$this->input->post('renewaldate');
			$ext_date=$this->input->post('extra_date');
		        $newDate = date('Y-m-d', strtotime($rdate. " + {$ext_date} days"));
			
			$datas1=$this->Super_admin->renew_ondemand($companyid,$newDate);
			print_r(json_encode($datas1));
	}
	   //Update the license validity
       public function insert_adminlicence(){
		    $this->load->helper('url');
		    $this->load->database();
		    $this->load->model('Super_admin');
			$id=$this->input->post('admin_id');
			$comp_id=$this->input->post('comp_id');
			$data=array(
				'service_desk'=>$this->input->post('total_service'),
				'technicians'=>$this->input->post('total_techni'),
			);
			$datas=$this->Super_admin->insert_adminlicence($data,$id,$comp_id);
			print_r(json_encode($datas));
		}


	public function productid_check(){
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Super_admin');
			$input=$this->input->post('comp');
			if(!empty($input))
				{
					$datass=$this->Super_admin->productid_check($input);
					$datass=intval(preg_replace('/[^0-9]+/', '', $datass), 10); 
					$datass=$datass+1;          
					$datass =str_pad($datass,4,"0",STR_PAD_LEFT);
					$datass="product_".$datass;
					echo $datass;
				}
			else {
			       echo "Choose Company to add Product";
			   }
       } 
	public function get_subproduct_details(){
		 $this->load->helper('url');
            $this->load->database();
            $this->load->model('Super_admin');
			$id=$this->input->post('id');
		    $data=$this->Super_admin->get_subproduct_details($id);
		    print_r(json_encode($data));
	}
	public function onload()
	{
		$this->load->helper('url');
        $this->load->database();
		$this->load->model('Super_admin');
		$result=$this->Super_admin->fetch(); 
		return $result;
	}
	public function tech_count()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$count=$this->Super_admin->count1();
		return $count;
	}
	public function service_count()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$count_s=$this->Super_admin->counts();
		return $count_s;
	}
	public function company_count()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$count_c=$this->Super_admin->company_count();
		return $count_c;
	}
	public function renew_count()
	{   
          $date = date('Y-m-d');
		$rdate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') +3, date('Y')));
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$count_r=$this->Super_admin->renew_count($rdate,$date);
		return $count_r;
	}
    public function display_company()
    {
        $this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$result=$this->Super_admin->display_company();    
            return $result;
    }
	public function list_prod()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$company = $this->input->post('com');
		$final= $this->Super_admin->list_prod($company);    
        return $final;
	}   
	public function priority()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$company = $this->input->post('com');
		$filter = $this->input->post('filt');
		$final= $this->Super_admin->priority($company);  
        return $final;
	}	
	public function download_sampletemplate($filename = NULL){
		$this->load->helper('url');
		// load download helder
		$this->load->helper('download');
		// read file contents
		$data = file_get_contents(base_url('/assets/templates/'.$filename));
		force_download($filename, $data);
	}


	//add new product
	public function insertproduct()
    {
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Super_admin');      
            $product_id=$this->input->post('product_id');
            $product_name=$this->input->post('product_name');
            $company=$this->input->post('company'); 
            $product_modal=$this->input->post('product_modal'); 
            $product_desc=$this->input->post('product_desc');   
            
			    $product_image=$_FILES["product_image"];
                $output_dir = "./assets/images/";                   
                $fileName = $_FILES["product_image"]["name"];
                $baseurl = base_url().'assets/images/';
                $data=array(
                    'product_id'=>$this->input->post('product_id'),
                    'product_name'=>$this->input->post('product_name'),
                    'product_image'=>$baseurl.$product_id,
                    'company_id'=>$this->input->post('company'),
                    'product_modal'=>$this->input->post('product_modal'),
                    'product_desc'=>$this->input->post('product_desc'),
                );
                $data['records']=$this->Super_admin->insertproduct($data);
                if($data['records']==1){
                    move_uploaded_file($_FILES["product_image"]["tmp_name"],$output_dir.$product_id);               
                    echo "product added Successfully";
                    }
				else{
                    echo "product not added, kindly try again";
					}
		   }
		   
		   /*Bulk upload sla details */
	public function bulk_slabill()
	{
		$result="";
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');			
		$this->load->library('PHPExcel');
		$handle = $_FILES["file-upload"]['tmp_name'];
		$filename = $_FILES["file-upload"]["name"];		
		$pieces = explode(".", $filename);
		if($pieces[1]== "xlsx"){
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');//2007PHP Excel_IOFactory
			$objPHPExcel = $objReader->load($handle);
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestDataColumn();
			function toNumber($dest)
				{
					if ($dest)
						return ord(strtolower($dest)) - 96;
					else
						return 0;
				}
				function myFunction($s,$x,$y){
				 $x = toNumber($x);
				 return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
				}
				for ($getContent = 2; $getContent <= $highestRow; $getContent++)
				{
					$rowData = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
					$content[] = $rowData;			
				}	
				$e=2;
			  for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++)
				{	
				
				$sheetData = $objPHPExcel->getActiveSheet();	
			    $company_id = myFunction($sheetData,0,$e);
				$product = myFunction($sheetData,'A',$e);
				$category = myFunction($sheetData,'B',$e);
				$priority = myFunction($sheetData,'C',$e);
				$sla_amount = myFunction($sheetData,'D',$e);
				$elapsed_amount = myFunction($sheetData,'E',$e);
					
			 $check_bulkcombo =$this->Super_admin->check_bulkcombo($company_id,$product,$category,$priority); 

			  if($check_bulkcombo=="1") {
				//print_r($sla_amount);
$datas=$content[$makeInsert][0];
				$data=array(
					'company_id'=>$datas[0],
					'product'=>$datas[1],
					'category'=>$datas[2],
					'priority'=>$datas[3],
					'sla_amount'=>$datas[4],
					'elapsed_amount'=>$datas[5]
				);	
	$valueing=$this->Super_admin->bulk_slabill($company_id,$product,$category,$priority,$sla_amount,$elapsed_amount,$data);
				if($valueing=="1"){
					$result[].= "Sla bill added for ".$product. " & ".$category . " for " .$company_id;
				 }

                              else if($valueing=="Sorry, the Sub-Category" .$category. "does not exist in" .$company_id)
                                 {
                                      $result[].= "Sorry, the Sub-Category" .$category. "does not exist in" .$company_id;
                                 }

                               else if($valueing=="Sorry, the Product" .$product. "does not exist in" .$company_id)
                                 {
                                      $result[].= "Sorry, the Product  " .$product . " does not exist in  " .$company_id;
                                 }

                               else if($valueing=="Sorry, Company" .$company_id . "does not exist")
                                 {
                                      $result[].= "Sorry, Company" .$company_id . "does not exist";
                                 }

                              else{
				      $result[].= "Sla bill not added for ".$product. " & ".$category . "for" .$company_id;
				  }
			
		         }
                 else if($check_bulkcombo=="Sorry, the Sub-Category does not exist")
                  {
                    $result[].= "Sorry, the Sub-Category".$category. "does not exist in" .$company_id;
                   }
             else if($check_bulkcombo=="Sorry, the product-Category does not exist")
              {
                      $result[].= "Sorry, the Product  " .$product. " does not exist in " .$company_id;
               }
            else if($check_bulkcombo=="Sorry, Company" .$company_id . "does not exist")
		{
                       $result[].= "Sorry, Company " .$company_id . " does not exist";
                }
             else {
                       // $result[].= "Duplication occured for";
			$result[].= "Duplication occured for ".$product. " &  ".$category . " for Company ".$company_id;
		}
              $e++;
	  }
	echo json_encode($result);
	}		
	else if($pieces[1]== "xls"){
		$result="";	
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load($handle);
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestDataColumn();
			function toNumber($dest)
				{
					if ($dest)
						return ord(strtolower($dest)) - 96;
					else
						return 0;
				}
				function myFunction($s,$x,$y){
				 $x = toNumber($x);
				 return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
				}
				for ($getContent = 2; $getContent <= $highestRow; $getContent++)
				{
					$rowData = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
					$content[] = $rowData;			
				}	
				$e=2;
				
			  for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++)
				{	
					
				$sheetData = $objPHPExcel->getActiveSheet();	
			    $company_id = myFunction($sheetData,0,$e);
				$product = myFunction($sheetData,'A',$e);
				$category = myFunction($sheetData,'B',$e);
				$priority = myFunction($sheetData,'C',$e);
				$sla_amount = myFunction($sheetData,'D',$e);
				$elapsed_amount = myFunction($sheetData,'E',$e);
					
				$check_bulkcombo =$this->Super_admin->check_bulkcombo($company_id,$product,$category,$priority); 
			  if($check_bulkcombo==1) {
				//print_r($sla_amount);
$datas=$content[$makeInsert][0];
				$data=array(
					'company_id'=>$datas[0],
					'product'=>$datas[1],
					'category'=>$datas[2],
					'priority'=>$datas[3],
					'sla_amount'=>$datas[4],
					'elapsed_amount'=>$datas[5]
				);	
				$valueing=$this->Super_admin->bulk_slabill($data);
				if($valueing){
					$result[].= "Sla bill added for ".$product. " & ".$category . $company_id;
				}else{
					$result[].= "Sla bill not added for ".$product. " & ".$category;
				}
		          }
		else {
			$result[].= "Duplication occured for".$product. " & ".$category .$company_id;
		}
            $e++;
	  }
	echo json_encode($result);	
	}
	
	}

	/*Save SLA amount */
      public function submit_amount()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
			   $comp =$this->input->post('company_name');
			   $prio= $this->input->post('priority');	
			   $prod= $this->input->post('product');	
			   $cat= $this->input->post('category');	
			   $sla_amt= $this->input->post('sla_amt');
			   $elapsed_amt= $this->input->post('elapsed_amt');
		   if( !empty($comp) && !empty($prod) && !empty( $sla_amt) && !empty( $elapsed_amt))
		   {
				$this->load->library('form_validation');
				$this->form_validation->set_rules('sla_amt', 'Number', 'required|regex_match[/^[1-9][0-9][0-9]*$/]');
		$this->form_validation->set_rules('elapsed_amt', 'Number', 'required|regex_match[/^[1-9][0-9][0-9]*$/]');
		if ($this->form_validation->run() == FALSE)
		{
			echo "Provide Proper Inputs for the Fields";
		}
		else{
			if(!empty($cat)){
			$data=array(
						'company_id'=>$comp ,
						'priority'=>$prio ,
						'product'=>$prod,
						'category'=>$cat,
						'sla_amount'=>$sla_amt,
						'elapsed_amount'=>$elapsed_amt
					  );
			}
			else{
				$data=array(
						'company_id'=>$comp ,
						'priority'=>$prio ,
						'product'=>$prod,
						'sla_amount'=>$sla_amt,
						'elapsed_amount'=>$elapsed_amt
					  );
			}
                       $combo=array(
                                       'company_id'=>$comp ,
				       'priority'=>$prio ,
				       'product'=>$prod,
				       'category'=>$cat
                                   );
			
		$insert1 = $this->Super_admin->submit_amount($data,$combo);
		if($insert1==1){
				echo "inserted.";
			      }
			}
		}
		else {
			echo "All Fields are Mandatory";
		}
	}



	//store the company logo image
	public function imageof()
	{		
		
		        $product_image1  = $_FILES["fileName"];
                $output_dir      = "./assets/upload/";
                $fileName        = $_FILES["fileName"]["name"];
                $baseurl         = base_url() . 'assets/images/';

                move_uploaded_file($_FILES["fileName"]["tmp_name"],$output_dir.$fileName);
                echo $fileName;
		
	}
         
	
     
	public function renewal_date()
	{
	  $years =$this->input->post('yr');
	  $ext_date =$this->input->post('date');
  
          if($years=="category1")
              {
                $date1 = strtotime(date("Y-m-d", strtotime($ext_date)) . " +".$yr."years");
		$date1 = strtotime(date("Y-m-d", strtotime($date1)) . " +".$mon."month");
		$date2 = date('Y-m-d',$date1);
		echo $date2;
             }
	  else if($years=="category2")
              {

                   $yr=0;
                   $mon=3;
		 //$date = date("Y-m-d");
	        //$date = date('Y-m-d', strtotime('+'.$yr.' years'));
               
                $time = strtotime($ext_date);
                $CurrentDate = date('Y-m-d',$time);
	        $date = new DateTime($CurrentDate);
                $date->modify('3 month');
		
                echo $date->format('Y-m-d');
                
               
             }
        else if($years=="category3")
              {
                   $yr=0;
                   $mon=6;
		 $time = strtotime($ext_date);
                $CurrentDate = date('Y-m-d',$time);
	        $date = new DateTime($CurrentDate);
                $date->modify('6 month');
		
                echo $date->format('Y-m-d');
             }
     else 
              {
                   $yr=1;
                   $mon=0;
		 $time = strtotime($ext_date);
                $CurrentDate = date('Y-m-d',$time);
	        $date = new DateTime($CurrentDate);
                $date->modify('+1 year');
		
                echo $date->format('Y-m-d');
             }
      
	}
  public function renewal_date1()
	{
	  $years =$this->input->post('yr');
         $mydate=  date('Y-m-d');
  
          if($years=="category1")
              {
         	//$days = floor(($years * 365*60*60*24 +$months*30*60*60*24)/ (60*60*24)); 
		//$g=$days;
		//$dayYmd= date("Y-m-d");
		/* $date = date("Y-m-d");
		// echo $date = strtotime(date("Y-m-d", strtotime($date)) . " +".$months." months");
		//$d = strtotime($dayYmd);
		//$cc = 24*60*60*$g + 60*60 + $d;
		$result= date("Y-m-d",$date);   
		echo  $result; */
                   $yr=0;
                   $mon=1;
		 //$date = date("Y-m-d");
		// $date = date('Y-m-d', strtotime('+'.$yr.' years'));
                $time = strtotime($mydate);
                $CurrentDate = date('Y-m-d',$time);
	        $date = new DateTime($CurrentDate);
                $date->modify('1 month');
		
                echo $date->format('Y-m-d');
             }
	  else if($years=="category2")
              {

                   $yr=0;
                   $mon=3;
		 //$date = date("Y-m-d");
	        //$date = date('Y-m-d', strtotime('+'.$yr.' years'));
               
                $time = strtotime($mydate);
                $CurrentDate = date('Y-m-d',$time);
	        $date = new DateTime($CurrentDate);
                $date->modify('3 month');
		
                echo $date->format('Y-m-d');
                
               
             }
        else if($years=="category3")
              {
                   $yr=0;
                   $mon=6;
		 $time = strtotime($mydate);
                $CurrentDate = date('Y-m-d',$time);
	        $date = new DateTime($CurrentDate);
                $date->modify('6 month');
		
                echo $date->format('Y-m-d');
             }
     else 
              {
                   $yr=1;
                   $mon=0;
		 $time = strtotime($mydate);
                $CurrentDate = date('Y-m-d',$time);
	        $date = new DateTime($CurrentDate);
                $date->modify('+1 year');
		
                echo $date->format('Y-m-d');
             }
      
	}
       public function sendof(){
	
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$variable=$this->Super_admin->company_id();
		$base_url='assets/upload/';
		  $image =$this->input->post('data');
		  $image_url=$base_url.$image;
                  $comp =$this->input->post('company_name');
                  $con= $this->input->post('contact');	
                  $flat_no= $this->input->post('flat_no');
                  $street= $this->input->post('street');
                  $addr= $this->input->post('comp_addr');
                  $state= $this->input->post('state');
                  $country=  $this->input->post('country');
                  $pincode=  $this->input->post('post');
                 // $admin_empid= $this->input->post('admin_empid');
                  $adfn= $this->input->post('fname');
                  $adln= $this->input->post('lname');
                  $mail= $this->input->post('ad_email');
                  $cont= $this->input->post('contact_num');
                  $serv= $this->input->post('no_servd');
                  $tech= $this->input->post('no_tech');
                  $subs_plan= $this->input->post('time_yr');
				  $renew= $this->input->post('renew_date');

				  $empid_count  = $this->Super_admin->get_employeeid_count();
				  $admin_empid = '000'.$empid_count ++;
				 

                 if($subs_plan=='category1'){
                        $subsplan="1 Month Trial";
                  }
                  else if($subs_plan=='category2'){
                        $subsplan="Quarterly Subscription";
                  }
                  else if($subs_plan=='category3'){
                        $subsplan="Half-Yearly Subscription";
                  }
                  else{
                        $subsplan="Annual Subscription";
                  }
                  $date  = date("Y-m-d");
       if( !empty($comp) && !empty($mail) && !empty($con) && !empty($flat_no) && !empty($street) && !empty($state) && !empty($country)  && !empty($admin_empid) && !empty($adfn) && !empty($adln) && !empty($mail) && !empty($cont) && !empty($serv) && !empty($tech) && !empty( $subs_plan) && !empty( $pincode))
      {
			  $this->load->library('form_validation');
		
		$this->form_validation->set_rules('ad_email', 'Email', 'required|valid_email');
		//$this->form_validation->set_rules('contact', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');
		//$this->form_validation->set_rules('contact_num', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');
		if ($this->form_validation->run() == FALSE)
		{
			echo "Provide Proper Mobile Number or/and Email Id";
		}
		else {
			
				$empid_check  = $this->Super_admin->empid_check($admin_empid);
                $email_check  = $this->Super_admin->useremail_check($mail);
                $mobile_check = $this->Super_admin->usernumber_check($con);

                if ($empid_check == 1) {
                    echo "Employee id is already placed, Kindly check it";
                } 
                else {
                    if ($email_check == 1) {
                        echo "Admin Email is already placed, Kindly check it";
                    } 
                    else {
                        if ($mobile_check == 1) {
                            echo "Contact number is already placed, Kindly check it";
                        } 
                        else {
                        	 $check_company= $this->input->post('company_name');
							 $check_contact= $this->input->post('contact');	
						     $check_mail= $this->input->post('ad_email');
				             $check_adcontact= $this->input->post('contact_num');
							
							$date  = date("Y-m-d");
							if(!empty($image))
			                 {
								$cdata1=array(
								    'company_id'=>$variable,
									'company_name'=>$comp,
									'company_logo'=>$image_url,
									'company_contact'=> $con,
									'building_no'=> $flat_no,
									'base_address'=> $street,
									'town' => $this->input->post('company_town'),
									'location' => $this->input->post('landmark'),
									'city'=>$addr,
									'state'=> $state,
									'country'=> $country,
									'pincode'=>$pincode,
									'subscription_plan'=>$subsplan,
									'start_date'=>$date,
									'renewal_date'=>$renew
								);
								$adata1=array(
									'company_id'=>$variable,
									'company_name'=>$comp,
									'ad_fname'=>$adfn,
									'ad_lname'=> $adln,
									'ad_mailid'=>$mail,
									'ad_contact'=> $con,
									'service_desk'=>$serv,
									'technicians'=>$tech,
									'subs_plan'=> $subsplan,
									'start_date'=>$date,
									'renewal_date'=>$renew);
			                 }

			                 $datass          = $this->Admin_model->userid_check();
					        $datass          = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
					        $datass          = $datass + 1;
					        $datass          = str_pad($datass, 4, "0", STR_PAD_LEFT);
					        $company_id      = $variable;
					        $datass          = $company_id . "_" . "User_" . $datass;

				            $user_table = array(
				                                'employee_id' => $admin_empid,
				                                'user_id' => $datass,
				                                'company_id' => $variable,
				                                'companyname' => $comp,
				                                'first_name' => $adfn,
				                                'last_name' => $adln,
				                                'email_id' => $mail,
				                                'contact_number' => $con,
				                                'flat_no' => $flat_no,
				                                'street' => $street,
				                                'city' => $addr,
				                                'state' => $state,
				                                'location' => $this->input->post('company_town'),
				                                'area' => $this->input->post('company_town'),
				                                'role' => 'Admin',
				                                'town' => $this->input->post('company_town'),
				                                'landmark' => $this->input->post('landmark'),
				                                'country' => $country,
				                                'pincode' => $pincode
				                          );
				            //print_r($user_table);
				            //die(); 
                   
								$testing = $this->Super_admin->testing_combo($check_company,$check_contact,$check_mail,$check_adcontact);
								if($testing==1) 
								{
					                       echo "Duplication occured";
							   
						        }
			
								else {
									$user_insert = $this->Super_admin->user_data($user_table);
									if($user_insert==1){
										$insert1 = $this->Super_admin->comp_data1($cdata1);
										if($insert1==1){
										   $value = $this->Super_admin->admin_data($adata1);
											if($value==1){
											  echo $variable;
											}
										}
									}									
		                        }
                    }
                }			              
            
			}
			
		}
	}
    else{
		echo "All Fields are Mandatory";
	} 
    }	

public function sendof1(){
	$cdata1=array();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$variable=$this->Super_admin->company_id();
		
                  $comp =$this->input->post('company_name');
                  $con= $this->input->post('contact');	
                  $flat_no= $this->input->post('flat_no');
                  $street= $this->input->post('street');
                  $addr= $this->input->post('comp_addr');
                  $state= $this->input->post('state');
                  $country=  $this->input->post('country');
                  $pincode=  $this->input->post('post');
                  $admin_empid= $this->input->post('admin_empid');
                  $adfn= $this->input->post('fname');
                  $adln= $this->input->post('lname');
                  $mail= $this->input->post('ad_email');
                  $cont= $this->input->post('contact_num');
                  $serv= $this->input->post('no_servd');
                  $tech= $this->input->post('no_tech');
                  $subs_plan= $this->input->post('time_yr');
                  $renew= $this->input->post('renew_date');
                 if($subs_plan=='category1'){
                        $subsplan="1 Month Trial";
                  }
                  else if($subs_plan=='category2'){
                        $subsplan="Quarterly Subscription";
                  }
                  else if($subs_plan=='category3'){
                        $subsplan="Half-Yearly Subscription";
                  }
                  else{
                        $subsplan="Annual Subscription";
                  }
                  $date  = date("Y-m-d");
       if( !empty($comp) && !empty($mail) && !empty($con) && !empty($flat_no) && !empty($street) && !empty($state) && !empty($country)  && !empty($admin_empid) && !empty($adfn) && !empty($adln) && !empty($mail) && !empty($cont) && !empty($serv) && !empty($tech) && !empty( $subs_plan) && !empty( $pincode))
      {
			  $this->load->library('form_validation');
		
		$this->form_validation->set_rules('ad_email', 'Email', 'required|valid_email');
		//$this->form_validation->set_rules('contact', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');
		//$this->form_validation->set_rules('contact_num', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');
		if ($this->form_validation->run() == FALSE)
		{
			echo "Provide Proper Mobile Number or/and Email Id";
		}
		else {
			
				$empid_check  = $this->Super_admin->empid_check($admin_empid);
                $email_check  = $this->Super_admin->useremail_check($mail);
                $mobile_check = $this->Super_admin->usernumber_check($con);

                if ($empid_check == 1) {
                    echo "Employee id is already placed, Kindly check it";
                } 
                else {
                    if ($email_check == 1) {
                        echo "Admin Email is already placed, Kindly check it";
                    } 
                    else {
                        if ($mobile_check == 1) {
                            echo "Contact number is already placed, Kindly check it";
                        } 
                        else {
                        	 $check_company= $this->input->post('company_name');
							 $check_contact= $this->input->post('contact');	
						     $check_mail= $this->input->post('ad_email');
				             $check_adcontact= $this->input->post('contact_num');
							
							$date  = date("Y-m-d");
							if(empty($image))
			                 {
								$cdata1=array(
								    'company_id'=>$variable,
									'company_name'=>$comp,
									'company_contact'=> $con,
									'building_no'=> $flat_no,
									'base_address'=> $street,
									'town' => $this->input->post('company_town'),
									'location' => $this->input->post('landmark'),
									'city'=>$addr,
									'state'=> $state,
									'country'=> $country,
									'pincode'=>$pincode,
									'subscription_plan'=>$subsplan,
									'start_date'=>$date,
									'renewal_date'=>$renew
								);
								$adata1=array(
									'company_id'=>$variable,
									'company_name'=>$comp,
									'ad_fname'=>$adfn,
									'ad_lname'=> $adln,
									'ad_mailid'=>$mail,
									'ad_contact'=> $con,
									'service_desk'=>$serv,
									'technicians'=>$tech,
									'subs_plan'=> $subsplan,
									'start_date'=>$date,
									'renewal_date'=>$renew,
								    'blocked_status'=>0);
			                 }
                                                   

			                 $datass          = $this->Admin_model->userid_check();
					        $datass          = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
					        $datass          = $datass + 1;
					        $datass          = str_pad($datass, 4, "0", STR_PAD_LEFT);
					        $company_id      = $variable;
					        $datass          = $company_id . "_" . "User_" . $datass;

				            $user_table = array(
				                                'employee_id' => $admin_empid,
				                                'user_id' => $datass,
				                                'company_id' => $variable,
				                                'companyname' => $comp,
				                                'first_name' => $adfn,
				                                'last_name' => $adln,
				                                'email_id' => $mail,
				                                'contact_number' => $con,
				                                'flat_no' => $flat_no,
				                                'street' => $street,
				                                'city' => $addr,
				                                'state' => $state,
				                                'location' => $this->input->post('company_town'),
				                                'area' => $this->input->post('company_town'),
				                                'role' => 'Admin',
				                                'town' => $this->input->post('company_town'),
				                                'landmark' => $this->input->post('landmark'),
				                                'country' => $country,
				                                'pincode' => $pincode
				                          );
				            //print_r($user_table);
				            //die(); 
                   
								$testing = $this->Super_admin->testing_combo($check_company,$check_contact,$check_mail,$check_adcontact);
								if($testing==1) 
								{
					                       echo "Duplication occured";
							   
						        }
			
								else {
									$user_insert = $this->Super_admin->user_data($user_table);
									if($user_insert==1){					
										$insert1 = $this->Super_admin->comp_data1($cdata1);
										if($insert1==1){
										   $value = $this->Super_admin->admin_data($adata1);
											if($value==1){
											  echo $variable;
											}
										}
									}									
		                        }
                    }
                }			              
            
			}
			
		}
	}
    else{
		echo "All Fields are Mandatory";
	} 
}

//send email notification after company registration
public function send_mail(){
   
     $this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
                          $login_url=base_url()."index.php?/Login/login";

                        $this->load->helper('string');
                       $password= random_string('alnum',8);
		$to = $this->input->post('ad_email');
		$variable=$this->input->post('data');
                  $comp =$this->input->post('company_name');
                 $adfn= $this->input->post('fname');				 
     	
		
 		
       	$enc_password1= $this->encrypt->encode($password);
		$adata1=array(
						'companyid'=>$variable,
						'companyname'=>$comp,
						'username'=>$to,
						'password'=> $enc_password1,
						'user_type'=>'Admin'
					);
						 $value = $this->Super_admin->login_data($adata1);
	$message = "
	<!doctype html>
	<html>
	<head>
	<meta name='viewport' content='width=device-width' />
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<style>
	p,
	ul,
	ol {
	font-family: sans-serif;
	font-size: 14px;
	font-weight: normal;
	margin: 0;
	Margin-bottom: 15px; }
	p li,
	ul li,
	ol li {
	list-style-position: inside;
	margin-left: 5px; }
	a {
	color: #3498db;
	text-decoration: underline; }
	</style>
	<body>
	<p>Dear <b>$adfn</b> & Team,</p>
	<p>Welcome to FieldPro</b></p>
	<p>Your Login Credentials are </p> 
	<p>URL <strong><a href='$login_url'>Click here to login</a></strong></p>
	<p>Username <strong>$to</strong></p>
	<p>Password <strong>$password</strong></p>
	<p>Best regards,</p> <p>FieldPro Team.</p>Kaspon Techworks.

	</body>
	</html>
	";

	$this->load->library('email');
$config['protocol']='smtp';
        $config['smtp_host']='smtp.office365.com';
        $config['smtp_port']='587';
        $config['smtp_timeout']='30';
        $config['smtp_user']='hwsupport@capricot.com';
        $config['smtp_pass']='Support14';
	    $config['smtp_crypto'] = 'tls';
        $config['charset']='utf-8';
        $config['newline']="\r\n";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
$this->email->initialize($config);
$this->email->from('hwsupport@capricot.com', 'Capricot');
$this->email->to($to);
$this->email->subject('New Company Registration');
$this->email->message($message);
$this->email->send();

	// $config = Array(
	// 				'protocol' => 'smtp',
	// 						'smtp_host' => 'ssl://smtp.googlemail.com',
	// 						'smtp_port' => 465,
	// 						'smtp_user' => 'kaspondevelopers@gmail.com',
	// 						'smtp_pass' => 'Kaspon@123',
	// 						'mailtype' => 'html',
	// 						'charset' => 'iso-8859-1'
	// 				);
	// 		$this->load->library('email', $config);
	// 		$this->email->set_newline("\r\n");
	// 		$this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
	// 		$this->email->to($to);
	// 		$this->email->subject('New Subscription regd');
	// 		$this->email->message($message);
	// 		//$this->email->set_newline("\r\n");
	// 		//$this->email->send();
	// 	  $this->email->send();

	// // Will only print the email headers, excluding the message subject and body
 //       echo  $this->email->print_debugger(array('headers'));
	//mail($to,$subject,$message,$headers);
				// if (mail($to, $subject, $message, $headers)) {
				// 	echo "Your Password has been sent to your mail";
				// }
			 	// 	else {
				// 	echo "Your are not an Registered User";
				// }
  }
	public function makeid()
	{
		$this->load->helper('string');
		echo random_string('alnum',8);

	}
	public function calc_ren()
	{
		$my_date=date("Y-m-d");
		$months=$this->input->post('month');
		//echo $this->input->post('amc_yr');
		$years=$this->input->post('year');
		//echo $this->input->post('amc_months');
		$days = floor(($years * 365*60*60*24 +$months*30*60*60*24)/ (60*60*24)); 
		$g=$days;
		$dayYmd= date("d-m-Y");
		$d = strtotime($dayYmd);
		$cc = 24*60*60*$g + 60*60 + $d;
		$result = date("Y-m-d",$cc); 
		echo $result;
	}
	public function calculate()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$revenue=$this->Super_admin->revenue_count();
		return $revenue;
		
	}
	
	public function calculate_count()
	{
		$this->load->helper('url');
		$this->load->database();
		$input= $this->input->post('input');
		$this->load->model('Super_admin');
		$count=$this->Super_admin->user_details($input);
		return $count;
		
	}
	public function pie_chart()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$answer=$this->Super_admin->load_chart();
		return $answer;
		
	} 
	public function fetch_company(){
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		$id= $this->input->post('id');
		$value=$this->Super_admin->fetch_company($id);
		print_r(json_encode($value));
	}
	public function send_renewmail(){
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		$reciever= $this->input->post('reciever');
		$company= $this->input->post('company');
		$renew= $this->input->post('renew');
		$from_email= 'ramyariotz22@gmail.com';
			$config = Array(
				'mailtype'  => 'html', 
				'charset'   => 'utf8'
			);
			$message =array(
			'com' => $this->input->post('company'),
			'ren' => $this->input->post('renew'),
			'rec' => $this->input->post('reciever')
		);
		$this->load->library('email',$config); 
			$this->email->from($from_email, 'Ramya'); 
			$this->email->to($reciever);
			$this->email->subject('Email Test');
			$body = $this->load->view('renewmail.php',$message,TRUE);	
			$this->email->message($body);
			$this->email->send();
	}
       
       public function fetch_renewdate()
       {
           $this->load->helper('url');
           $this->load->database();				
	   $this->load->model('Super_admin');
           $id= $this->input->post('company');
           $ren_info=$this->Super_admin->fetch_renewdate($id);
           echo json_encode($ren_info);
	   }
	   
	   //renew the company information
	public function renew_info(){
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		$name= $this->input->post('cmodal_name');
		$tech= $this->input->post('cmodal_serv');
		$serv= $this->input->post('cmodal_tech');
		$subs_plan= $this->input->post('time_yr');
		$renew= $this->input->post('cust_id_renew');
		$mydate=  date('Y-m-d');
		  if(  !empty($tech) && !empty($serv) && !empty($subs_plan))
		 {
				$company_info=$this->Super_admin->renew_info($name,$tech,$serv,$subs_plan,$renew,$mydate);
			if($company_info==1){
				echo"inserted";
				}
			else{
			echo "Error occured in Renewal.";
				}
		 }
		 else {
			echo "All Fields are Mandatory.";
		 }
	}

    //blocking the specific company
	public function set_block(){
		$this->load->helper('url');
		$this->load->database();	
		$admin_id= $this->input->post('id');		
		$this->load->model('Super_admin');
		$value=$this->Super_admin->set_block($admin_id);
		if($value=="updated"){
			echo"updated";
			}
		else{
			//echo "error";
echo $value;
		 }
	}

  // Unblocking the company
	public function set_unblock(){
		$this->load->helper('url');
		$this->load->database();	
		$admin_id= $this->input->post('id');		
		$this->load->model('Super_admin');
		$value=$this->Super_admin->set_unblock($admin_id);
		if($value==1){
			echo"updated";
			}
		else{
			echo "error";
		 }
	}

	
	public function sample(){
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		$run =$this->Super_admin->company_id();
		return $run;
	}


    //send notificatio to admin user
	public function notification(){
                $date= date('Y-m-d');
		$rdate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') +7, date('Y')));
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$notify=$this->Super_admin->notification($rdate,$date);
		return $notify;
	}


        public function noti_count(){
                $date= date('Y-m-d');
		$rdate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') +7, date('Y')));
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$notify=$this->Super_admin->noti_count($rdate,$date);
		echo $notify;
	}
	public function auto_block(){
		$today=date('Y-m-d');
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		$block =$this->Super_admin->auto_block($today);
		echo $block;
	}
	
		public function slaa()
	{	
	$first_day_of_the_week = 'Monday';
		$start_of_the_week     = date('Y-m-d 00:00:00',strtotime("Last $first_day_of_the_week"));
			if ( strtolower(date('l')) === strtolower($first_day_of_the_week) )
			{
				$start_of_the_week = date('Y-m-d 00:00:00',strtotime('today'));
			}
			 $start_of_the_week ; 	                                //Current Week to date
			 $month= date("Y-m-01 00:00:00");                        /*Month to Date*/
			 $quart = date("Y-m-01 00:00:00", strtotime("-2 month")); //Quarter To Date
			$current_month=date('M'); 
			 $start_of_yr  = date('Y-01-01 00:00:00',strtotime("today")); 	//April 1st calculation
			
		$today = new DateTime();
		$today=$today->setTimezone(new DateTimezone('Asia/Kolkata'));
		$today=$today->setTimezone(new DateTimezone('Asia/Kolkata'));
		$today=$today->format('Y-m-d H:i:s'); //today
        $this->load->helper('url');
		$this->load->database();	
        $company_id= $this->input->post('comp');
		$filter= $this->input->post('filter');
		$this->load->model('Super_admin');
		$sla_value=$this->Super_admin->slaa($company_id,$filter,$today,$start_of_the_week ,$month,$quart,$start_of_yr);
		return $sla_value;
	}
	public function dashboard()
	{
		$today = new DateTime();
		$today=$today->setTimezone(new DateTimezone('Asia/Kolkata'));
		 $today=$today->format('Y-m-d H:i:s');
		 $month= date("Y-m-01 00:00:00");
		$quart = date("Y-m-01  00:00:00", strtotime("-2 month")); 
		$current_month=date('M');//$current_month='Apr';
			if($current_month=='Jan' || $current_month=='Feb'|| $current_month=='Mar')
			{
			 $start_of_yr  = date('Y-04-01  00:00:00',strtotime("-1 year")); 	//April 1st calculation
			}
			else
			{
			$start_of_yr  = date('Y-04-01  00:00:00',strtotime("today")); 	//April 1st calculation
			}
		 $maxDays=date($current_month);
		 $currentDayOfMonth=date('j');

		$this->load->helper('url');
		$this->load->database();
		$filter = $this->input->post('filter');	
			$this->load->model('Super_admin');
			//$this->load->model('model');
			$chart =$this->Super_admin->dashboard1($today,$month,$quart,$start_of_yr,$filter);
		   return $chart;
	}
        public function revenue_graph()
	{
		$this->load->helper('url');
		$this->load->database();
		$filter = $this->input->post('filter');	
		$company = $this->input->post('comp');	
		$this->load->model('Super_admin');
		$revenue='';
		$revenue =$this->Super_admin->revenue_graph($filter,$company);
		return $revenue;
	}

	public function select_company() {
	
		$first_day_of_the_week = 'Monday';
		$start_of_the_week     = date('Y-m-d',strtotime("Last $first_day_of_the_week"));
		if ( strtolower(date('l')) === strtolower($first_day_of_the_week) )
		{
			$start_of_the_week = date('Y-m-d',strtotime('today'));
		}
		$start_of_the_week ; 	//Current Week to date
		$date = date('Y-m-d 00:00:00' , strtotime('-1 year')); //previous yr current date
		
		$month= date("Y-m-01 00:00:00"); /*Month to Date*/
		$prev_month= date('Y-m-01 00:00:00', strtotime('-1 year')); //Month to Date for previous yr
	
		$quart = date("Y-m-01 00:00:00", strtotime("-2 month")); //Quarter To Date
		$prev_quart = date("Y-m-01 00:00:00", strtotime("-2 month",strtotime($date))); //Previous Quarter to Date
		
		$current_month=date('M');//$current_month='Apr';
		if($current_month=='Jan' || $current_month=='Feb'|| $current_month=='Mar')
		{
			$start_of_yr  = date('Y-04-01 00:00:00',strtotime("-1 year"));  //April 1st calculation
			$prevstart_of_yr  = date('Y-04-01 00:00:00',strtotime("-1 year",strtotime($date)));   //previous April 1st calculation	
		}
		else
		{
			 $start_of_yr  = date('Y-04-01 00:00:00',strtotime("today")); 	//April 1st calculation
			 $prevstart_of_yr  = date('Y-04-01 00:00:00',strtotime("today",strtotime($date))); 	//April 1st calculation
		}
		
		$today = new DateTime();
		$today=$today->setTimezone(new DateTimezone('Asia/Kolkata'));
		$today=$today->setTimezone(new DateTimezone('Asia/Kolkata'));
		$today=$today->format('Y-m-d H:i:s');// today
        $this->load->helper('url');
		$this->load->database();	
        $company_id= $this->input->post('comp');		
		$this->load->model('Super_admin');
		$company=$this->Super_admin->select_company($company_id,$today,$month,$quart,$start_of_yr,$date,$prev_month,$prev_quart,$prevstart_of_yr);
		return $company;  
	 }
	public function total_subscription() 
	{
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		$subscription =$this->Super_admin->total_subscription();
		return $subscription; 
	}
	public function subscribed_tech()
	{
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		$company_id= $this->input->post('company');
		$subs_tech =$this->Super_admin->subscribed_tech($company_id);
		return $subs_tech; 
	}

	/* Displaying the company list */
	public function choose_company() {
			//calculates next coimng monday(ie) starting day of week
		/* $date=date('Y-m-d', strtotime('2016-12-30'));
		echo $monday = date('Y-m-d',strtotime('monday', strtotime($date))); */
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		$company =$this->Super_admin->choose_company();
		return $company; 
	
	}
	public function check()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		/* $product = $this->input->post('product');
			$category = $this->input->post('category');	
			$customer = $this->input->post('customer');
			$contract = $this->input->post('contract');
			$call_cat = $this->input->post('txtPassportNumber0');
			echo $call_cat;
			$keywords = explode(',', $call_cat); */
			$company_name =  $this->input->post('company_name');
			$priority=  $this->input->post('priority');
			$insert1 = $this->Super_admin->check($company_name,$priority);
			return $insert1;
	}
		public function add_sla()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
				$company_name =  $this->input->post('company_name');
				$editable =  $this->input->post('data');
				$priority = $this->input->post('priority');
				$rhours = $this->input->post('rhours');	
				$rminute = $this->input->post('rminute');	
				$rseconds = $this->input->post('rseconds');
				$resp_time=$rhours.':'.$rminute.':'.$rseconds;
				//$resp_time=date('',strtotime($restime));
				$hours = $this->input->post('hours');	
				$minute = $this->input->post('minute');	
				$seconds = $this->input->post('seconds');
				$resolution=$hours.':'.$minute.':'.$seconds;
				//$resp_time=date('H:i:s',strtotime($restime));
				//$resolution=date('H:i:s',strtotime($resoltime));
				$mttr_target = $this->input->post('mttr_target');
				$sla_target = $this->input->post('service_reven');
		
		if(!empty($company_name))
		{
		if(!empty($priority))
		{
			if((!empty($rhours)) || (!empty($rminute)) || (!empty($rseconds)))
			{
				if((!empty($hours)) || (!empty($minute)) || (!empty($seconds)))
				{
					if(!empty($mttr_target))
					{
					 if(!empty($sla_target)) {
						$product = $this->input->post('for_prod');
						$category = $this->input->post('for_cat');	
						$customer = $this->input->post('for_cust');
						$contract = $this->input->post('for_cont_type');
						$call_cat = $this->input->post('txtPassportNumber0');
						echo $call_cat;
						$keywords = explode(',', $call_cat);
				if(	empty($call_cat))
				{
					if($product=='')
					{
					$product = 'all';
					}
					if($category=='')
					{
					$category = 'all';
					}
					if($customer=='')
					{
					$customer = 'all';
					}
					if($contract=='')
					{
					$contract = 'all';
					}
					if($call_cat=='')
					{
					$call_cat = 'all';
					}
				
				$adata1=array(
                        'company_id'=>$company_name,
						'priority_level'=>$priority,
						'response_time'=>$resp_time,
						'resolution_time'=>$resolution,
						'mttr_target'=> $mttr_target,
						'SLA_Compliance_Target'=>$sla_target,
						'editable_rights'=>$editable
						);
				$adata2=array(
                        'company_id'=>$company_name,
						'priority_level'=>$priority,
						'product'=>$product,
						'category'=>$category,
						'cust_category'=>$customer,
						'call_category'=> $call_cat,
						'service_category'=>$contract
						);
			
					$insert1 = $this->Super_admin->add_sla1($adata2);
						if($insert1=="Already Exist!")
							{
								echo "This combination already Already Exist";
							}
						else{
							$ins = $this->Super_admin->add_sla($adata1,$insert1);
								if($ins==1)
								{
								echo "Inserted.";
								}	
					}
			}
			else
			{
				if(empty($product)){
					$product = 'all';
				}
				if(empty($category)){
					$category = 'all';
				}
				if(empty($customer)){
					$customer = 'all';
				}
				if(empty($contract)){
					$contract = 'all';
				}
				if(empty($call_cat)){
					$call_cat = 'all';
				}
				foreach($keywords as $key){
				
				$adata1=array(
                        'company_id'=>$company_name,
						'priority_level'=>$priority,
						'response_time'=>$resp_time,
						'resolution_time'=>$resolution,
						'mttr_target'=> $mttr_target,
						'SLA_Compliance_Target'=>$sla_target
						);
				$adata2=array(
                        'company_id'=>$company_name,
						'priority_level'=>$priority,
						'product'=>$product,
						'category'=>$category,
						'cust_category'=>$customer,
						'call_category'=> $key,
						'service_category'=>$contract
						);
			
					$insert1 = $this->Super_admin->add_sla1($adata2);
							if($insert1=="Already Exist!")
								{
								echo "This combination already Already Exist";
								}
							else{
							$ins = $this->Super_admin->add_sla($adata1,$insert1);
								if($ins==1)
								{
								echo "Inserted.";
							}	
						}
						}	
					 }
					 }
					 else{
						echo "Set SLA_Compliance_Target";
					 }
					}
					else {
						echo "Set MTTR Target.";
					}
				}
				else {
			echo "Set Resolution Time.";
			}
			}
			else {
			echo "Set Response Time.";
		}
		}
		else {
		echo "Prefer Priority level for SLA.";
		}
		}
		else{
			echo "Choose any Company to Map SLA.";
			}
		
	}
	public function search_state()
	 {   
		$this->load->helper('url');
		$this->load->database();				
		$this->load->model('Super_admin');
		echo $s=$this->input->post('term');
		$run =$this->search_state($s);
		if(count($run) > 0) {
		    foreach($run as $pr)
			   $arr_result[]= $pr->state;
			  echo json_encode($arr_result);
		}
	}
	public function get_details_sla(){
		$result;
		$product_id=$this->input->post('prod_id');
		$category_id=$this->input->post('cate_id');		
		$company_id=$this->input->post('company_id');		
		$priority=$this->input->post('priority');
		$reso_hour=$this->input->post('reso_hour');		
		$reso_hour=str_pad($reso_hour,2,"0",STR_PAD_LEFT);
		$reso_minute=$this->input->post('reso_minute');
		$reso_minute=str_pad($reso_minute,2,"0",STR_PAD_LEFT);
		$resolution_time=$reso_hour.':'.$reso_minute.':'.'00';
		$res_hour=$this->input->post('res_hour');		
		$res_hour=str_pad($res_hour,2,"0",STR_PAD_LEFT);
		$res_minute=$this->input->post('res_minute');
		$res_minute=str_pad($res_minute,2,"0",STR_PAD_LEFT);
		$response_time=$res_hour.':'.$res_minute.':'.'00';
		$acc_hour=$this->input->post('acc_hour');
		$acc_hour=str_pad($acc_hour,2,"0",STR_PAD_LEFT);
		$acc_minute=$this->input->post('acc_minute');
		$acc_minute=str_pad($acc_minute,2,"0",STR_PAD_LEFT);
		$acceptance_time=$acc_hour.':'.$acc_minute.':'.'00';		
		$sla_target=$this->input->post('sla_target');
		$mttr_target_hour=$this->input->post('mttr_target_hour');
		$mttr_target_hour=str_pad($mttr_target_hour,2,"0",STR_PAD_LEFT);
		$mttr_target_minute=$this->input->post('mttr_target_minute');
		$mttr_target_minute=str_pad($mttr_target_minute,2,"0",STR_PAD_LEFT);
		$mttr_target=$mttr_target_hour.':'.$mttr_target_minute.':'.'00';
		$editable_right=$this->input->post('editable_right');
		//$product_id= explode(",", $product_id);
		//print_r(json_encode(count($product_id[])));
		//echo "<pre>"; print_r($product_id);
		//$prod=explode(',', $product_id);
		if(!empty($priority) && !empty($reso_hour) && !empty($reso_minute) && !empty($res_hour) && !empty($res_minute) && !empty($acc_hour) && !empty($acc_minute) && !empty($sla_target) && !empty($mttr_target_hour) && !empty($mttr_target_minute) && !empty($editable_right)){			
			$prod=explode(",",$product_id);
			$cate=explode(",",$category_id);
			for($i=0;$i<count($prod);$i++){	
				$arr=array();
				$datas=$this->Super_admin->get_subcate($prod[$i]);
				$oneDimensionalArray = array_map('current', $datas);
				$arr= array_intersect($oneDimensionalArray,$cate);
				for($j=0;$j<count($arr);$j++){					
					$ref_id=$this->Super_admin->get_reference_id($company_id);
					$data_combination=array(
							'product'=>$prod[$i],
							'category'=>$arr[$j],
							'ref_id'=>$ref_id,
							'company_id'=>$company_id,
							'priority_level'=>$priority
					);
					$data_mapping=array(
							'ref_id'=>$ref_id,
							'company_id'=>$company_id,
							'priority_level'=>$priority,
							'response_time'=>$response_time,
							'resolution_time'=>$resolution_time,
							'acceptance_time'=>$acceptance_time,
							'SLA_Compliance_Target'=>$sla_target,
							'mttr_target'=>$mttr_target,
					);
					$data_edit=array(							
							'editable_rights'=>$editable_right,
					);
					$insert_slac=$this->Super_admin->insert_slac($data_combination,$data_mapping,$data_edit,$company_id);
					if($insert_slac==1){
						$result[]="SLA Combination of the Product category ".$prod[$i]." and Sub Category ".$arr[$j]." is successfully mapped";
					}else{
						$result[]="SLA Combination of the Product category ".$prod[$i]." and Sub Category ".$arr[$j]." is not mapped";
					}
				}
			}
		}else{
			$result="All Fields are Mandatory";
		}
		print_r($result);
	}	
	public function get_parent_node(){
		$selected=$this->input->post('selected');
		print_r($selected);
	}
	

	//get product's informations
	public function getdetails_product()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$data=$this->input->post('id');
		$data=$this->Super_admin->getdetails_product($data);
		print_r(json_encode($data));
	}
	
	public function deleteproduct()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $datas=$this->Admin_model->del_prod($id);
		 $checkdatas=$this->Admin_model->check_aval_prod($id);
		if($checkdatas == 1){
      // echo "Product already mapped to existing customer!";
			$data=2;
		 echo $data;	
         }
       else{ 
        if($datas){
            $data = $this->Admin_model->deleteproduct($id);
            echo $data;
        }
	   }
    }
	public function getdetails_subproduct()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$data=$this->input->post('id');
		$data=$this->Super_admin->getdetails_subproduct($data);
		print_r(json_encode($data)); 
	}	

	//delete the product
	public function deletesubproduct()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Super_admin');
		$this->load->model('Admin_model');
		$id=$this->input->post('id');
		 $checkdatas=$this->Admin_model->check_aval_sub_prod($id);
		if($checkdatas == 1){
      // echo "Sub Product already mapped to existing customer!";
			$data=2;
		 echo $data;	
         }
       else{ 
		$data=$this->Super_admin->deletesubproduct($id);
		echo $data;
	   }
	}


	/*
	 fetching the sla details
	*/
	function sla_timing(){
		$ref_id=$this->input->post('id');
		$this->load->model('Super_admin');
		$data=$this->Super_admin->view_priority($ref_id);
		print_r(json_encode($data));
	}

  /* Getting the company informations  */
   public function get_details_company(){
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Super_admin');
            $datas=$this->Super_admin->get_details_company();
            echo json_encode($datas);
		}
		
            //retrive the company list
            public function get_details_company1(){
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Super_admin');
            $datas=$this->Super_admin->get_details_company1();
            echo json_encode($datas);
		}
		

        public function get_product_company(){
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Super_admin');
            $company_id=$this->input->post('company_id');
            $datas=$this->Super_admin->get_product_company($company_id);
            echo json_encode($datas);
		}
		
     //displaying the product list for specific company
       public function get_product_company1()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Super_admin');
        $company_id = $this->input->post('company_id');
        $datas      = $this->Super_admin->get_product_company1($company_id);
        echo json_encode($datas);
	}
	
/*Get the sub product list based on company and product */
        public function get_subproduct_company(){
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Super_admin');
            $prod_id=$this->input->post('prod_id');
            $datas=$this->Super_admin->get_subproduct_company($prod_id);
            echo json_encode($datas);
		}
		
 //get the sub product based on company and products.
    public function get_subproduct_company1()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Super_admin');
        $prod_id = $this->input->post('prod_id');
        $datas   = $this->Super_admin->get_subproduct_company1($prod_id);
        echo json_encode($datas);
	}
	

        public function testing(){
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Super_admin');
            $this->load->view('testing');
        }
		
		/* 
		Save SLA
		*/
        public function submit_sla(){
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Super_admin');
            $company_id=$this->input->post('company_id');
            $product_id=$this->input->post('product_id');
            $cat_id=$this->input->post('cat_id');
            $priority=$this->input->post('priority');
			$response_day=$this->input->post('response_day');
            $response=$this->input->post('response');
			$resolution_day=$this->input->post('resolution_day');
            $resolution=$this->input->post('resolution');
			$acceptance_day=$this->input->post('acceptance_day');
            $acceptance=$this->input->post('acceptance');
            $sla_compliance=$this->input->post('sla_compliance');
            $mttr=$this->input->post('mttr');
            $rights=$this->input->post('rights');
	        $selectedcategory=$this->input->post('selectedcategory');
/*			
			if($company_id=="" && $product_id!="")
			{
				$company_id=$this->Super_admin->select_com($product_id);
			}
			if($company_id=="" && $product_id=="")
			{
				$company_id=$this->Super_admin->select_com_with_cat($cat_id);
			}
			if($product_id=="")
			{
				$product_id=$this->Super_admin->select_pro($cat_id);
			}
*/
			$type=$this->Super_admin->select_table_type($selectedcategory); 
			if($type=="company")
			{

			}
			else if($type=="product"){
				$product_id=$selectedcategory;
			}
			else if($type=="category"){
				$product_id=$this->Super_admin->select_pro($selectedcategory);
				$cat_id=$selectedcategory;
			}

			

            $datas=$this->Super_admin->select_sla_comb($company_id,$product_id,$cat_id,$priority);
            if($datas!=0)
			{
				$result=$this->Super_admin->submit_sla($company_id,$priority,$response_day,$response,$resolution_day,$resolution,$acceptance_day,$acceptance,$sla_compliance,$mttr,$datas,$rights);
				if($result==1)
				{
					echo "SLA Updated Successfully";
				}
				else
				{
					echo "Error";
				}
			}
			else
			{
				echo "Already Exist";
			}
        }
		
        public function check_slas(){
        	$prod_id=$this->input->post('product_id');
        	$id=$this->Super_admin->sla_company_id1($prod_id);
        	$datas=$this->Super_admin->check_slas($id,$prod_id);
        	echo json_encode($datas);
        }        
        public function check_slas1(){        	
        	$prod_id=$this->input->post('prod_id');
        	$id=$this->Super_admin->sla_company_id($prod_id);
        	$datas=$this->Super_admin->check_slas1($id,$prod_id);
        	echo json_encode($datas);
        }        
        public function check_slas2(){        	
        	$id=$this->input->post('company_id');
        	$datas=$this->Super_admin->check_slas2($id);
        	echo json_encode($datas);
		}
		
		/*Check sla is already checked or not */
        public function che_ck(){
        	$id=$this->input->post('company_id');
        	$delete_rec=$this->Super_admin->delete_rec($id);
        	if($delete_rec){
        		foreach($delete_rec as $d){
        			$dele=$this->Super_admin->dele($d);
        			if($dele == 1){
        				$dele=$this->Super_admin->deles($d);
        				echo $dele;      				
        			}
        		}
        	}        	
        }
        public function chec_k(){
        	$id=$this->input->post('company_id');
        	$delete_rec=$this->Super_admin->delete_recds($id);
        	if($delete_rec){
        		foreach($delete_rec as $d){
        			$dele=$this->Super_admin->dele_s($d);
        			if($dele == 1){
        				$dele=$this->Super_admin->deles_s($d);
        				echo $dele;     				
        			}
        		}
        	}        	
		}
/*To check SLA is already set for to company on this Priority*/		
  public function check_existslas2()
        {
        	$companyid=$this->input->post('company_id');
        	$priority=$this->input->post('priority');
        	$check_existslas2=$this->Super_admin->check_existslas2($companyid,$priority);
        	if($check_existslas2==true||$check_existslas2=="true")
        	{
        		echo "true";
        	}
        	else
        	{
        		echo "false";
        	}
        	
		}
		
//retrive the company
public function fetch_renewcompany(){
$this->load->helper('url');
$this->load->database(); 
$this->load->model('Super_admin');
$id= $this->input->post('id');
$value=$this->Super_admin->fetch_renewcompany($id);
print_r(json_encode($value));
}
	

 }
