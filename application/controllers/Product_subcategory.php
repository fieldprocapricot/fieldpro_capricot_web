<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_subcategory extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('common');	
	
		 if ($this->session->userdata('user_logged_in'))
        {

		}
		else{
			  redirect(base_url());
		}
		
	}

	public function index($id)
	{
			 if ($this->session->userdata('user_logged_in'))
			 {
		$companyid = $this->session->userdata('companyid');
		$this->load->model('Admin_model');	
		$this->data['webPageheading'] = $_GET['name'].' - Product';
		$this->data['employee_id'] = $id;		
		$this->data['employee_name'] = $_GET['name'];		
		$this->data['product_list']   = $this->Admin_model->getproduct($companyid);	
		$this->data['servicegroup'] = $this->Admin_model->get_servicegroup($companyid);		 
			$this->load->view('product_subcategory/product_subcategory',$this->data);
		}
		else{
	 redirect(base_url());
		}
	}
	
	public function getdetails_productcategory($id)
     {
	
		 if ($this->session->userdata('user_logged_in'))
		 {
		$this->load->model('Admin_model');		
			 
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));
		
         $results = $this->Admin_model->productsubproduct_view($id);
		 $product=$results->result_array();	 
	// print_r($product);
	//die; 
          $data = array();
			 $i=0;
			
          foreach($product as $row) {
			 
			  $button="";
					
			 //   $button = "<button class='btn btn-circle blue btn-outline btn-sm'  onclick='edit_details(".$row['id'].")'><i class='fa fa-pencil' aria-hidden='true'></i></button>";

			    $button = $button . "<button class='btn btn-circle red btn-outline btn-sm'  onclick='delete_details(" .$row['id']. ")'><i class='fa fa-trash' aria-hidden='true'></i></button>";
			
				$i=$i+1;
			
               $data[] = array(
				   $i,
				    $row['product_name'],
				    $row['cat_name'],
				   $row['skill_level'],
           		  $button
               );
          }
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $results->num_rows(),
                 "recordsFiltered" => $results->num_rows(),
                 "data" => $data
            );
		
		  echo json_encode($output);
		 }
			 else{
			 redirect(base_url());
			 }
     }
			
	
	public function add_tech_product_category()
    {
		if ($this->input->is_ajax_request()) {
			 if ($this->session->userdata('user_logged_in'))
			{
				$tech_list_id = $this->input->post('tech_list_id');
				$employee_id = $this->input->post('employee_id');
				$employee_name = $this->input->post('employee_name');
				$product_id = $this->input->post('product_id');
				$subcategory =$this->input->post('subcategory');
				$skill_level=$this->input->post('skill_level'); 
				$work_type=$this->input->post('work_type');  
				$delete = 0;
				 
				  $array = array('employeeid'=>$employee_id); 
		 $profile = $this->common->get_method($array,array('methodname'=>'GET_TECHNICIAN','sp_next_result'=>1));	
		$tech=$profile->row();
				 
				
				$array = array(
'tech_list_id' => $tech_list_id,
'employee_id' => $employee_id, 
'technician_id' => $tech->technician_id, 					
'employee_name'=>$employee_name, 
'product_id'=>$product_id,
'subcategory'=>$subcategory, 
'skill_level'=>$skill_level, 
'work_type'=>$work_type, 
'first_name' => $tech->first_name,
'last_name' => $tech->last_name,
'image' => $tech->image,
'email_id' => $tech->email_id,
'contact_number' => $tech->contact_number,
'alternate_number' => $tech->alternate_number,
'flat_no' => $tech->flat_no,
'street' => $tech->street,
'city' => $tech->city,
'state' => $tech->state,
'region' => $tech->region,
'location' => $tech->location,
'area' => $tech->area,
'role' => $tech->role,
'current_location' => $tech->current_location,
'current_lat' => $tech->current_lat,
'current_long' => $tech->current_long,
'availability' => $tech->availability,
'today_task_count' => $tech->today_task_count,
'task_count' => $tech->task_count,
'company_id' => $tech->company_id,
'companyname' => $tech->companyname,
'blocked_status' => $tech->blocked_status,
'town' => $tech->town,
'landmark' => $tech->landmark,
'country' => $tech->country,
'pincode' => $tech->pincode);

// print_r($array);
// die;
				
			$query = $this->common->create_method($array,array('methodname'=>'CREATE_UPDATE_TECH_PRODUCT_CATEGORY','sp_next_result'=>0,'outPosition'=>'last','out'=>'p_cateid'));
				if($query){
					$row = $query->row();
					     
						$cid = $row->p_cateid;
					if($cid==0)
					{
					echo json_encode(array('status'=>'true','msg'=>'Saved Successfully'));
					}
					else
					{
					echo json_encode(array('status'=>'true','msg'=>'Updated Successfully'));
					}
				}
				else{
					echo json_encode(array('status'=>'problem','msg'=>'Something went problem. Please try again.'));
				}
			}
			else{
				echo json_encode(array('status'=>'sessionout'));
			}
		}
		else{
			exit('No direct script access allowed');
		}
		
	}	
		
public function edit_category()
	{
				 if ($this->session->userdata('user_logged_in'))
				 {
						
			$cate_id = $this->input->post('cate_id');
				
			  $array = array('cate_id'=>$cate_id); 
		 $cate = $this->common->get_method($array,array('methodname'=>'GET_SERVICE_CATEGORY','sp_next_result'=>0));	
		$scate=$cate->row();
		
		  print_r(json_encode($scate));		
	
		}
		else{
			echo 'sessionout';
		}
	}

	public function delete_productcategory()
	{
		if ($this->input->is_ajax_request()) {
		 if ($this->session->userdata('user_logged_in'))
		 {
			
	
	$tech_list_id = $this->input->post('tech_list_id');
	
				$array = array('tech_list_id' => $tech_list_id);
                  
				
			$query = $this->common->create_method($array,array('methodname'=>'DELETE_TECH_PRODUCT_CATEGORY','sp_next_result'=>0,'outPosition'=>'last','out'=>'p_prodcateid'));
			if($query){
					$row = $query->row();
					     
						$cateid = $row->p_cateid;
				
					echo json_encode(array('status'=>'true','msg'=>"Deleted Successfully"));
				}
				else{
					echo json_encode(array('status'=>'problem','msg'=>'Something went problem. Please try again.'));
				}
					}
			else{
				echo json_encode(array('status'=>'sessionout'));
			}
		}
		else{
			exit('No direct script access allowed');
		}
	}
	
	
	
}