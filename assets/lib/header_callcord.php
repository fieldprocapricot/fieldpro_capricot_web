                    <div class="page-header navbar navbar-fixed-top">
                        <div class="page-header-inner">

                            <!-- BEGIN LOGO -->
								<div class="page-logo">
                        <a id="index" href="<?php echo base_url()?>">
                 <?php $company= $this->session->userdata('companylogo'); ?>								
                          <?php
							if($company!='')
							{
							?>
									<img src="<?php echo $company; ?>" alt="" style="width: 184px;height: 36px;" class="logo-default">
							<?php
							}
							else
							{
							?>
							 <img src="assets/layouts/layout/img/logos.png" alt="" class="logo-default"> 
							<?php
							}
							?>
									</a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                             <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                            <!-- END SEARCH -->
                            <!-- BEGIN TOPBAR ACTIONS -->
							
							<div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout/img/men.jpg" />
                                    <span class="username username-hide-on-mobile">  <?php echo $this->session->userdata('role');?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu-v2 header-logout" role="menu">
                                    <li>
                                        <a href="<?php echo site_url('Login/call_myprofile');?>">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
      		        <li>
                                       <a href="<?php echo site_url('login/callpassword_changes');?>">
                                                <i class="icon-key"></i> Change Password </a>
                                    </li>
                                    <li>
                                       <a href="<?php echo site_url('Login/logout');?>">
                                                <i class="fa fa-sign-out"></i> Log Out </a>
                                    </li>
                                                                    </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->

                        </ul>
                    </div>
					
                            
                            <!-- END TOPBAR ACTIONS -->
                        </div>
                        <!-- BEGIN HEADER MENU -->
                        
                        <!-- END HEADER MENU -->
                    </div>
                    <!--/container-->
					<div class="clearfix"> </div>