<div class="page-sidebar navbar-collapse collapse">
                            <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" >
							<li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
								<li class="nav-item start " id="man_dashboard">
                                    <a href="<?php echo site_url('controller_manager');?>" class="nav-link nav-toggle">
                                        <i class="icon-home"></i>
										<span class="title"> Dashboard</span>
										 </a>
										
                                </li>
								<li id="man_leader" class="nav-item">
                                    <a href="<?php echo site_url('controller_manager/leaderboard');?>" class="nav-link nav-toggle">
                                        <i class="icon-badge"></i>
										<span class="title">Leaderboard</span>
										 </a>
                                </li>								
                                <li id='man_spare' class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <i class="fa fa-ticket"></i><span class="title"> Tickets </span>
										<span class="arrow"></span> </a>
                                    <ul class="sub-menu">
                                        <li id='man_open' class="nav-item">
                                            <a href="<?php echo site_url('controller_manager/assigned_tickets');?>" class="nav-link">
                                                <span class="title"> New Tickets </span> </a>
                                        </li>
                                        <li id='man_ongoing' class="nav-item">
                                            <a href="<?php echo site_url('controller_manager/ongoing');?>" class="nav-link">
                                                <span class="title">Ongoing Tickets </span></a>
                                        </li>
                                        <li id='man_completed' class="nav-item">
                                             <a href="<?php echo site_url('controller_manager/completed');?>" class="nav-link">
                                                <span class="title">Completed Tickets </span></a>
                                        </li>
                                    </ul>
									</li>
									
								<li id="sla_management" class="nav-item">
                                   <!-- <a href="<?php //echo site_url('controller_manager/addsla');?>" class="nav-link nav-toggle  -->
									 <a href="<?php echo site_url('controller_admin/sla');?>" class="nav-link nav-toggle">
                                        <i class="icon-directions"></i> <span class="title">SLA Mapping </span> </a>
                                </li>
								<li id='man_spares' class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <i class="icon-wrench"></i> <span class="title"> Spare Request </span>
										<span class="arrow"></span> </a>
									<ul class="sub-menu">
                                        <li id='man_spare_re' class="nav-item">
                                            <a href="<?php echo site_url('controller_manager/Spare_request');?>" class="nav-link">
                                               <span class="title"> Spare Request</span> </a>
                                        </li>
                                        <li id='man_spare_im' class="nav-item">
                                            <a href="<?php echo site_url('controller_manager/impressed_spare');?>" class="nav-link">
                                               <span class="title"> Imprest Spare Request</span> </a>
                                        </li>
										<li id="spare_transfer" class="nav-item">
                                   			 <a href="<?php echo site_url('controller_manager/spare_transfer');?>" class="nav-link">
                                       		 <span class="title"> Spare Transfer Request</span> </a>
                              		  </li>
                                    </ul>
                                </li>
								
									<li id="man_contract" class="nav-item">
                                    <a href="<?php echo site_url('controller_manager/Manager_Contract');?>" class="nav-link nav-toggle">
                                        <i class="icon-docs"></i> <span class="title">New Contracts</span></a>
                                </li>
								<li id="man_reim" class="nav-item">
                                    <a href="<?php echo site_url('controller_manager/reimbursement');?>" class="nav-link nav-toggle">
                                        <i class="fa fa-money"></i> <span class="title">Reimbursement</span></a>
                                </li>
								<li class="nav-item" id="training_type">
                  <a href="<?php echo site_url('training_scrore/index');?>" class="nav-link nav-toggle">
                  <i class="icon-badge"></i>
				  <span class="title">Training Score</span>
				  </a>
               </li>
                              <!-- <li id="man_report" class="nav-item">
                                    <a href="<?php echo site_url('controller_manager/report');?>" class="nav-link nav-toggle">
									<i class="icon-doc"></i> <span class="title">Reports</span></a>
                                </li> -->
								
									<li id='man_spares' class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <i class="icon-doc"></i> <span class="title"> Reports </span>
										<span class="arrow"></span> </a>
									<ul class="sub-menu">
										 <li id='man_spare_re' class="nav-item">
                                            <a href="<?php echo site_url('controller_manager/report');?>" class="nav-link">
                                               <span class="title"> Attendance Report</span> </a>
                                        </li>
                                        <li id='man_spare_re' class="nav-item">
                                            <a href="<?php echo site_url('call_reports/index');?>" class="nav-link">
                                               <span class="title"> Call Report</span> </a>
                                        </li> 
                                     <li id='man_spare_im' class="nav-item">
                                            <a href="<?php echo site_url('summary_reports/index');?>" class="nav-link">
                                               <span class="title"> Summary Report</span> </a>
                                        </li>
                                        <li id='man_spare_re' class="nav-item">
                                            <a href="<?php echo site_url('technician_reports/index');?>" class="nav-link">
                                               <span class="title"> Technician Report</span> </a>
                                        </li>
                                        <li id="man_spare_re" class="nav-item">
                                   			 <a href="<?php echo site_url('kra_kpi_reports/index');?>" class="nav-link">
                                       		 <span class="title">Engineers KRA KPI</span> </a>
                              		  </li>
										<li id="man_spare_re" class="nav-item">
                                   			 <a href="<?php echo site_url('aging_reports/index');?>" class="nav-link">
                                       		 <span class="title">Aging Report</span> </a>
                              		  </li>
									  	<li id="man_spare_re" class="nav-item">
                                   			 <a href="<?php echo site_url('customer_rate_reports/index');?>" class="nav-link">
                                       		 <span class="title">Customer Rate Report</span> </a>
                              		  </li>
                                        
                                    </ul>
                                </li>
								
								
								<li id='man_metrics' class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <i class="icon-wrench"></i> <span class="title"> Configuration </span>
										<span class="arrow"></span></a>
									<ul class="sub-menu">
                                        <li id='man_metrics' class="nav-item perf-metrics">
                                            <a href="<?php echo site_url('controller_manager/service_desk');?>" class="nav-link">
                                                <span class="title">Performance Metrix </span> </a>
                                        </li>
                                        <li id='man_serv' class="nav-item">
                                            <a href="<?php echo site_url('controller_manager/score');?>" class="nav-link">
                                               <span class="title"> Configuration </span></a>
                                        </li>
                                    </ul>
                                    <li id='aaa' class="nav-item">
                                    <a href="<?php echo site_url('controller_manager/manager_notifications');?>" class="nav-link nav-toggle">
                                        <i class="icon-bell"></i> <span class="title"> Notifications </span>
                                        </a>
                                </li>
                                </li>
			
                                        
                            </ul>
                        </div>